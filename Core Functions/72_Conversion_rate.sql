
DELIMITER //


CREATE OR REPLACE FUNCTION bi.sfunc_to_conversion_leads(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_to_conversion_leads';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.conversion_leads_per_polygon;
CREATE TABLE bi.conversion_leads_per_polygon as 


SELECT

	t2.Country as Country,
	t2.polygon,
	CAST(t2.year as integer),
	CAST(t2.month as integer),
	t2.first_day_of_month,
	
	t2.Total_leads,
	t2.Touched as Touched,
	
	-- (t2.Touched/t2.Total_leads) as Share_touched,
	CASE WHEN t2.Reached > 0 THEN (t2.Reached/t2.Touched) ELSE 0 END as Share_Reached,
	CASE WHEN t2.Reached > 0 THEN (t2.Rejected/t2.Touched) ELSE 0 END as Share_Rejected,
	CASE WHEN t2.Touched > 0 THEN (t2.Documents_before_contract/t2.Touched) ELSE 0 END as Share_Documents_before_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_to_be_scheduled/t2.Touched) ELSE 0 END as Share_Interview_to_be_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_scheduled/t2.Touched) ELSE 0 END as Share_Interview_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_contract/t2.Touched) ELSE 0 END as Share_Waiting_for_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.After_interview/t2.Touched) ELSE 0 END as Share_After_interview,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_greenlight/t2.Touched) ELSE 0 END as Share_Waiting_for_greenlight,
	CASE WHEN t2.Touched > 0 THEN (t2.Converted/t2.Touched) ELSE 0 END as Share_Converted_touched,
	CASE WHEN t2.Total_leads > 0 THEN (t2.Converted/t2.Total_leads) ELSE 0 END as Share_Converted_total,
	
	t2.Converted as Converted
	
	
FROM


	(SELECT
	
		t1.countrycode as Country,
		t1.polygon,
		CAST(t1.year as integer),
		CAST(t1.month as integer),
		first_day_of_month,
		
		t1.C0::numeric as Total_leads,
		(t1.C1 + t1.C1bis + t1.C2 + t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9 + t1.C10)::numeric as New_leads,
		(t1.C0 - t1.C1)::numeric as Touched,
		(t1.C0 - t1.C1 - t1.C2)::numeric as Reached,
		(t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Documents_before_contract,
		(t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_to_be_scheduled,
		(t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_scheduled,
		(t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Waiting_for_contract,
		(t1.C7 + t1.C8 + t1.C9)::numeric as After_interview,
		(t1.C7 + t1.C9)::numeric as Waiting_for_greenlight,
		
		(t1.C9)::numeric as Converted,
		(t1.C10)::numeric as Rejected
		
	
	FROM
	
		
		(SELECT
		
			a.countrycode,
			a.delivery_area__c as polygon,
			EXTRACT(year from a.createddate) as year,
			EXTRACT(month from a.createddate) as month,
			MIN(a.createddate::date) first_day_of_month,
		
			COUNT(a.createddate) as C0,
		
			SUM(CASE WHEN a.status IN ('New lead','New leads') THEN 1 ELSE 0 END) as C1,
			SUM(CASE WHEN a.status IN ('QA call', 'Hold', 'Double Entry', 'Avoid', 'Contact Later') THEN 1 ELSE 0 END) as C1bis,
			SUM(CASE WHEN a.status IN ('First contact outstanding', 'Never reached') THEN 1 ELSE 0 END) as C2,
			SUM(CASE WHEN a.status IN ('Documents before contract') THEN 1 ELSE 0 END) as C3,
			SUM(CASE WHEN a.status IN ('Interview to be scheduled') THEN 1 ELSE 0 END) as C4,
			SUM(CASE WHEN a.status IN ('Interview scheduled') THEN 1 ELSE 0 END) as C5,
			SUM(CASE WHEN a.status IN ('Waiting for contract') THEN 1 ELSE 0 END) as C6,
			SUM(CASE WHEN a.status IN ('Waiting for greenlight') THEN 1 ELSE 0 END) as C7,
			SUM(CASE WHEN a.status IN ('Documents after interview', 'Contract ready') THEN 1 ELSE 0 END) as C8,
			SUM(CASE WHEN a.status IN ('Contact/Account created') THEN 1 ELSE 0 END) as C9,
			SUM(CASE WHEN a.status IN ('Rejected') THEN 1 ELSE 0 END) as C10,
			COUNT(a.converteddate) as C11
		
		FROM
		
			salesforce.lead a
				
				
		WHERE
		
			a.createddate > '2016-06-01'
			AND a.delivery_area__c IS NOT NULL
			AND a.countrycode IN ('DE', 'AT', 'NL', 'CH')
			AND a.delivery_area__c IN ('at-vienna', 'ch-basel', 'ch-bern', 'ch-geneva', 'ch-lausanne', 'ch-lucerne', 'ch-stgallen', 'ch-zurich', 'de-berlin', 'de-bonn', 'de-cologne', 'de-dusseldorf', 'de-essen', 'de-frankfurt', 'de-hamburg', 'de-mainz', 'de-manheim', 'de-munich', 'de-nuremberg', 'de-stuttgart', 'nl-amsterdam', 'nl-hague')
				
				
		GROUP BY
		
			a.countrycode,
			a.delivery_area__c,
			year,
			month
				
		
		ORDER BY 
		
			a.countrycode asc,
			a.delivery_area__c asc,
			month asc
			
			) t1
			
				) t2;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.conversion_leads_per_country;
CREATE TABLE bi.conversion_leads_per_country as 


SELECT

	t2.Country as Country,
	CAST(t2.year as integer),
	CAST(t2.month as integer),
	t2.first_day_of_month,
	
	t2.Total_leads,
	t2.Touched as Touched,
	
	-- (t2.Touched/t2.Total_leads) as Share_touched,
	CASE WHEN t2.Reached > 0 THEN (t2.Reached/t2.Touched) ELSE 0 END as Share_Reached,
	CASE WHEN t2.Reached > 0 THEN (t2.Rejected/t2.Touched) ELSE 0 END as Share_Rejected,
	CASE WHEN t2.Touched > 0 THEN (t2.Documents_before_contract/t2.Touched) ELSE 0 END as Share_Documents_before_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_to_be_scheduled/t2.Touched) ELSE 0 END as Share_Interview_to_be_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_scheduled/t2.Touched) ELSE 0 END as Share_Interview_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_contract/t2.Touched) ELSE 0 END as Share_Waiting_for_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.After_interview/t2.Touched) ELSE 0 END as Share_After_interview,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_greenlight/t2.Touched) ELSE 0 END as Share_Waiting_for_greenlight,
	CASE WHEN t2.Touched > 0 THEN (t2.Converted/t2.Touched) ELSE 0 END as Share_Converted_touched,
	CASE WHEN t2.Total_leads > 0 THEN (t2.Converted/t2.Total_leads) ELSE 0 END as Share_Converted_total,
	
	t2.Converted as Converted
	
	
FROM


	(SELECT
	
		t1.countrycode as Country,
		CAST(t1.year as integer),
		CAST(t1.month as integer),
		first_day_of_month,
		
		t1.C0::numeric as Total_leads,
		(t1.C1 + t1.C1bis + t1.C2 + t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9 + t1.C10)::numeric as New_leads,
		(t1.C0 - t1.C1)::numeric as Touched,
		(t1.C0 - t1.C1 - t1.C2)::numeric as Reached,
		(t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Documents_before_contract,
		(t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_to_be_scheduled,
		(t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_scheduled,
		(t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Waiting_for_contract,
		(t1.C7 + t1.C8 + t1.C9)::numeric as After_interview,
		(t1.C7 + t1.C9)::numeric as Waiting_for_greenlight,
		
		(t1.C9)::numeric as Converted,
		(t1.C10)::numeric as Rejected
		
	
	FROM
	
		
		(SELECT
		
			a.countrycode,
			EXTRACT(year from a.createddate) as year,
			EXTRACT(month from a.createddate) as month,
			MIN(a.createddate::date) first_day_of_month,
		
			COUNT(a.createddate) as C0,
		
			SUM(CASE WHEN a.status IN ('New lead','New leads') THEN 1 ELSE 0 END) as C1,
			SUM(CASE WHEN a.status IN ('QA call', 'Hold', 'Double Entry', 'Avoid', 'Contact Later') THEN 1 ELSE 0 END) as C1bis,
			SUM(CASE WHEN a.status IN ('First contact outstanding', 'Never reached') THEN 1 ELSE 0 END) as C2,
			SUM(CASE WHEN a.status IN ('Documents before contract') THEN 1 ELSE 0 END) as C3,
			SUM(CASE WHEN a.status IN ('Interview to be scheduled') THEN 1 ELSE 0 END) as C4,
			SUM(CASE WHEN a.status IN ('Interview scheduled') THEN 1 ELSE 0 END) as C5,
			SUM(CASE WHEN a.status IN ('Waiting for contract') THEN 1 ELSE 0 END) as C6,
			SUM(CASE WHEN a.status IN ('Waiting for greenlight') THEN 1 ELSE 0 END) as C7,
			SUM(CASE WHEN a.status IN ('Documents after interview', 'Contract ready') THEN 1 ELSE 0 END) as C8,
			SUM(CASE WHEN a.status IN ('Contact/Account created') THEN 1 ELSE 0 END) as C9,
			SUM(CASE WHEN a.status IN ('Rejected') THEN 1 ELSE 0 END) as C10,
			COUNT(a.converteddate) as C11
		
		FROM
		
			salesforce.lead a
				
				
		WHERE
		
			a.createddate > '2016-06-01'
			AND a.delivery_area__c IS NOT NULL
			AND a.countrycode IN ('DE', 'AT', 'NL', 'CH')
			AND a.delivery_area__c IN ('at-vienna', 'ch-basel', 'ch-bern', 'ch-geneva', 'ch-lausanne', 'ch-lucerne', 'ch-stgallen', 'ch-zurich', 'de-berlin', 'de-bonn', 'de-cologne', 'de-dusseldorf', 'de-essen', 'de-frankfurt', 'de-hamburg', 'de-mainz', 'de-manheim', 'de-munich', 'de-nuremberg', 'de-stuttgart', 'nl-amsterdam', 'nl-hague')
				
				
		GROUP BY
		
			a.countrycode,
			a.delivery_area__c,
			year,
			month
				
		
		ORDER BY 
		
			a.countrycode asc,
			a.delivery_area__c asc,
			month asc
			
			) t1
			
				) t2;


-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.rejection_reasons_per_polygon;
CREATE TABLE bi.rejection_reasons_per_polygon as 


SELECT

	Table1.countrycode as Country,
	Table1.polygon,
	CAST(Table1.year as integer),
	CAST(Table1.month as integer),
	first_day_of_month,
	CAST(Table1.Rejected as numeric),
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Availability as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Availability,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Distance as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Distance,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Personal_qualities as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Personal_qualities,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Professional_skills as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Professional_skills,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Contractual_reasons as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Contractual_reasons,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Student as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Student,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Other as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Other


FROM

	(SELECT

		a.countrycode,
		a.delivery_area__c as polygon,
		EXTRACT(year from a.createddate) as year,
		EXTRACT(month from a.createddate) as month,
		MIN(a.createddate::date) first_day_of_month,

		
		SUM(CASE WHEN (a.rejection_reason__c IS NULL) THEN 0 ELSE 1 END) as Rejected,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Availability') THEN 1 ELSE 0 END) as Availability,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Distance') THEN 1 ELSE 0 END) as Distance,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('No Work Permit', 'Soft Skills', 'Failed background check', 'Punctuality / reliability', 'No show', 'Appearance', 'Not reached anymore') THEN 1 ELSE 0 END) as Personal_qualities, 

		SUM(CASE WHEN a.rejection_reason__c IN ('Language skills', 'Cleaning skills') THEN 1 ELSE 0 END) as Professional_skills,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Earnings too low', 'Found other Full-time job', 'Wants full-time', 'Minijob') THEN 1 ELSE 0 END) as Contractual_Reasons,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Student') THEN 1 ELSE 0 END) as Student,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Other (add note)') THEN 1 ELSE 0 END) as Other


	FROM

		salesforce.lead a
		
		
	WHERE

		a.createddate > '2016-06-01'
		AND a.delivery_area__c IS NOT NULL
		
		
	GROUP BY

		a.countrycode,
		a.delivery_area__c,
		year,
		month

	ORDER BY 

		a.countrycode asc,
		a.delivery_area__c asc,
		month asc,
		Rejected asc,
		Availability asc,
		Distance asc,
		Personal_qualities asc,
		Professional_skills asc,
		Contractual_reasons asc,
		Student asc,
		Other asc

		) as Table1

GROUP BY

	Country,
	Table1.polygon,
	Table1.year,
	Table1.month,
	first_day_of_month,
	Table1.Rejected,
	Table1.Availability,
	Table1.Distance,
	Table1.Personal_qualities,
	Table1.Professional_skills,
	Table1.Contractual_reasons,
	Table1.Student,
	Table1.Other
	
ORDER BY

	Country, polygon asc, month asc, Rejected asc, Availability asc, Distance asc, Personal_qualities asc, Professional_skills asc, Contractual_reasons asc, Student asc, Other asc;


-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.rejection_reasons_per_country;
CREATE TABLE bi.rejection_reasons_per_country as 


SELECT

	Table1.countrycode as Country,
	CAST(Table1.year as integer),
	CAST(Table1.month as integer),
	first_day_of_month,
	CAST(Table1.Rejected as numeric),
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Availability as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Availability,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Distance as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Distance,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Personal_qualities as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Personal_qualities,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Professional_skills as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Professional_skills,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Contractual_reasons as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Contractual_reasons,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Student as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Student,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Other as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Other


FROM

	(SELECT

		a.countrycode,
		EXTRACT(year from a.createddate) as year,
		EXTRACT(month from a.createddate) as month,
		MIN(a.createddate::date) first_day_of_month,

		
		SUM(CASE WHEN (a.rejection_reason__c IS NULL) THEN 0 ELSE 1 END) as Rejected,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Availability') THEN 1 ELSE 0 END) as Availability,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Distance') THEN 1 ELSE 0 END) as Distance,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('No Work Permit', 'Soft Skills', 'Failed background check', 'Punctuality / reliability', 'No show', 'Appearance', 'Not reached anymore') THEN 1 ELSE 0 END) as Personal_qualities, 

		SUM(CASE WHEN a.rejection_reason__c IN ('Language skills', 'Cleaning skills') THEN 1 ELSE 0 END) as Professional_skills,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Earnings too low', 'Found other Full-time job', 'Wants full-time', 'Minijob') THEN 1 ELSE 0 END) as Contractual_Reasons,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Student') THEN 1 ELSE 0 END) as Student,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Other (add note)') THEN 1 ELSE 0 END) as Other


	FROM

		salesforce.lead a
		
		
	WHERE

		a.createddate > '2016-06-01'
		AND a.delivery_area__c IS NOT NULL
		
		
	GROUP BY

		a.countrycode,
		a.delivery_area__c,
		year,
		month

	ORDER BY 

		a.countrycode asc,
		a.delivery_area__c asc,
		month asc,
		Rejected asc,
		Availability asc,
		Distance asc,
		Personal_qualities asc,
		Professional_skills asc,
		Contractual_reasons asc,
		Student asc,
		Other asc

		) as Table1

GROUP BY

	Country,
	Table1.year,
	Table1.month,
	first_day_of_month,
	Table1.Rejected,
	Table1.Availability,
	Table1.Distance,
	Table1.Personal_qualities,
	Table1.Professional_skills,
	Table1.Contractual_reasons,
	Table1.Student,
	Table1.Other
	
ORDER BY

	Country, month asc, Rejected asc, Availability asc, Distance asc, Personal_qualities asc, Professional_skills asc, Contractual_reasons asc, Student asc, Other asc;







-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'