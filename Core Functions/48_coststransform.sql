CREATE OR REPLACE FUNCTION bi.daily$coststransform (crunchdate date) RETURNS void AS 

$BODY$

DECLARE 
function_name varchar := 'bi.daily$coststransform';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;


BEGIN

  DROP TABLE IF EXISTS bi.coststransform;

  CREATE TABLE bi.coststransform AS -- SEM IS OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'SEM'::text as channel,
      SUM(cpa.sem_cost + cpa.sem_discount) as cost,
      SUM(cpa.sem_acq) as acquisitions,
      '177'::numeric as cpa_target_global, --ok
      '26430'::numeric as budget_target_global, --ok
      '170'::numeric as cpa_target_at, --ok
      '3230'::numeric as budget_target_at, --ok
      '190'::numeric as cpa_target_de, --ok
      '11200'::numeric as budget_target_de, --ok
      '170'::numeric as cpa_target_ch, --ok
      '12000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;

  INSERT INTO bi.coststransform -- SEO is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'SEO'::text as channel,
      SUM(cpa.seo_discount) as cost,
      SUM(cpa.seo_acq) as acquisitions,
      '7'::numeric as cpa_target_global, --ok
      '2000'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '5'::numeric as cpa_target_de, --ok
      '1000'::numeric as budget_target_de, --ok
      '16'::numeric as cpa_target_ch, --ok
      '1000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;

  INSERT INTO bi.coststransform -- Brand Marketing is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Brand Marketing'::text as channel,
      SUM( cpa.sem_brand_discount + cpa.sem_brand_cost  /* + (cpa.tvcampaign) */ + cpa.offline_cost + cpa.offline_discount + cpa.seo_brand_discount ) as cost,
      SUM( cpa.dti_acq + cpa.sem_brand_acq + cpa.seo_brand_acq + cpa.offline_acq ) as acquisitions,
      '29'::numeric as cpa_target_global, --ok
      '17600'::numeric as budget_target_global, --ok
      '7'::numeric as cpa_target_at, --ok
      '600'::numeric as budget_target_at, --ok
      '24'::numeric as cpa_target_de, --ok
      '7000'::numeric as budget_target_de, --ok
      '43'::numeric as cpa_target_ch, --ok
      '10000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


  INSERT INTO bi.coststransform -- Facebook is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Facebook'::text as channel,
      SUM(cpa.facebook_cost + cpa.facebook_discount + cpa.facebook_organic_discount) as cost,
      SUM(cpa.facebook_acq + cpa.facebook_organic_acq) as acquisitions,
      '180'::numeric as cpa_target_global, --ok
      '27100'::numeric as budget_target_global, --ok
      '160'::numeric as cpa_target_at, --ok
      '4500'::numeric as budget_target_at, --ok
      '190'::numeric as cpa_target_de, --ok
      '12600'::numeric as budget_target_de, --ok 
      '180'::numeric as cpa_target_ch, --ok
      '10000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok


    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


  INSERT INTO bi.coststransform -- Display is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Display'::text as channel,
      SUM(cpa.display_cost + cpa.display_discount) as cost,
      SUM(cpa.display_acq) as acquisitions,
      '134'::numeric as cpa_target_global, --ok
      '8975'::numeric as budget_target_global, --ok
      '43'::numeric as cpa_target_at, --ok
      '1000'::numeric as budget_target_at, --ok
      '190'::numeric as cpa_target_de, --ok
      '2975'::numeric as budget_target_de, --ok
      '180'::numeric as cpa_target_ch, --ok
      '5000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok


    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


  INSERT INTO bi.coststransform -- Vouchers is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Voucher Campaigns'::text as channel,
      SUM(cpa.vouchers_cost) as cost,
      SUM(cpa.vouchers_acq) as acquisitions,
      '21'::numeric as cpa_target_global, --ok
      '1000'::numeric as budget_target_global, --ok
      '34'::numeric as cpa_target_at, --ok
      '100'::numeric as budget_target_at, --ok
      '20'::numeric as cpa_target_de, --ok
      '900'::numeric as budget_target_de, --ok
      '0'::numeric as cpa_target_ch, --ok
      '0'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


    INSERT INTO bi.coststransform -- Overall is in progress
      SELECT

        cpa.date as dates,
        cpa.locale as locale,
        'Overall'::text as channel,

        SUM(
        cpa.sem_cost
        +cpa.sem_discount

        +cpa.sem_brand_cost
        +cpa.sem_brand_discount

        +cpa.display_cost
        +cpa.display_discount

        +cpa.facebook_cost
        +cpa.facebook_discount

        +cpa.offline_cost
        +cpa.offline_discount

        +cpa.vouchers_cost

        +cpa.youtube_cost
        +cpa.youtube_discount

        ) as cost,

        SUM(cpa.all_acq) as acquisitions,
        
        '64'::numeric as cpa_target_global, --ok
        '83105'::numeric as budget_target_global, --ok
        '53'::numeric as cpa_target_at, --ok
        '9430'::numeric as budget_target_at, --ok
        '53'::numeric as cpa_target_de, --ok
        '35675'::numeric as budget_target_de, --ok
        '85'::numeric as cpa_target_ch, --ok
        '38000'::numeric as budget_target_ch, --ok
        '0'::numeric as cpa_target_nl, --ok
        '0'::numeric as budget_target_nl --ok

      FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'