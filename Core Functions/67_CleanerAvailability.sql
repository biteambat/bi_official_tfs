
CREATE OR REPLACE FUNCTION bi.daily$cleaneravailability(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$cleaneravailability';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


DROP TABLE IF EXISTS bi.availability_cleaner;
CREATE TABLE bi.availability_cleaner as 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-02' as date) as date,
	CAST(split_part(availability_monday__c,'-',1) as time) as start_availability,
	CAST(split_part(availability_monday__c,'-',2) as time) as end_availability
FROM
	salesforce.account
WHERE
	(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
	and Status__c in ('ACTIVE','BETA');

INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-03' as date) as day,
	CAST(split_part(availability_tuesday__c,'-',1) as time) as start_availability,
	CAST(split_part(availability_tuesday__c,'-',2) as time) as end_availability
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-04' as date) as day,
	CAST(split_part(availability_wednesday__c,'-',1) as time) as start_availability_wed,
	CAST(split_part(availability_wednesday__c,'-',2) as time) as end_availability_wed
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
		status__c,
		type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-05' as date) as day,
	CAST(split_part(availability_thursday__c,'-',1) as time) as start_availability_thu,
	CAST(split_part(availability_thursday__c,'-',2) as time)as end_availability_thu
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
		status__c,
		type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-06' as date) as day,
	CAST(split_part(availability_friday__c,'-',1) as time) as start_availability_fr,
	CAST(split_part(availability_friday__c,'-',2) as time) as end_availability_fr
FROM
	salesforce.account
WHERE
	(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
	and Status__c in ('ACTIVE','BETA');
	
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
		status__c,
		type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-07' as date) as day,
	CAST(split_part(availability_saturday__c,'-',1) as time) as start_availability_sa,
	CAST(split_part(availability_saturday__c,'-',2) as time) as end_availability_sa
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
	and Status__c in ('ACTIVE','BETA');
	
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	type__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-08' as date) as day,
	CAST(split_part(availability_sunday__c,'-',1) as time) as start_availability_sun,
	CAST(split_part(availability_sunday__c,'-',2) as time) end_availability_sun
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');

DROP TABLE IF EXISTS bi.availabilitymuffin_temp1;
CREATE TABLE bi.availabilitymuffin_temp1 as 
SELECT
	*
FROM
	bi.availability_cleaner,
	unnest(array[0,1,2,3,4,5,6]) as day,
	unnest(array[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) as hour
	;

DROP TABLE IF EXISTS bi.cleaner_available_hourly;
CREATE TABLE bi.cleaner_available_hourly as 
SELECT
	city,
	date,
	zipcode,
	locale,
	hour,
	type__c,
	status__c,
	SUM(CASE WHEN EXTRACT(DOW FROM date) = DAY AND HOUR >= EXTRACT(HOUR FROM start_availability) and hour < EXTRACT(HOUR FROM end_availability) THEN 1 ELSE 0 END) as Jobs
FROM
	bi.availabilitymuffin_temp1
GROUP BY
	city,
	zipcode,
	locale,
	date,
	hour,
	status__c,
	type__c;

DROP TABLE IF EXISTS bi.availabilitymuffin_temp1;

-- job calendar Current


DROP TABLE IF EXISTS bi.muffin_cleaner;
CREATE TABLE bi.muffin_cleaner as 
SELECT
	sfid,
	name,
	createddate::date as onboarding_date,
	type__c,
	CASE WHEN hr_contract_weekly_hours_min__c is null THEN hr_contract_weekly_hours_max__c ELSE hr_contract_weekly_hours_min__c END  as Contract_Hours,
	Status__c as Status
FROM
	Salesforce.Account
WHERE
	type__c in ('cleaning-b2c','cleaning-b2b');



DROP TABLE IF EXISTS bi.muffin_order_distribution;
CREATE TABLE bi.muffin_order_distribution as 
SELECT
	City,
	contact__c,
	status,
	order_type,
	LEFT(Locale__c,2) as locale,
	LEFT(ShippingPostalCode,3) as zipcode,
	Effectivedate::date as date,
	EXTRACT(DOW FROM Effectivedate) as weekday,
	EXTRACT(HOUR FROM Order_Start__c)+2 as Start_time,
	EXTRACT(HOUR FROM Order_End__c)+2 as End_time
FROM
	bi.orders
WHERE
	Effectivedate::date between '2016-08-15'::date and current_date::date
	and test__c = '0';

DROP TABLE IF EXISTS bi.muffin_order_distribution_v2;
CREATE TABLE bi.muffin_order_distribution_v2 as 
SELECT
	*
FROM
	bi.muffin_order_distribution,
	unnest(array[0,1,2,3,4,5,6]) as day,
	unnest(array[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) as hour;

DROP TABLE IF EXISTS bi.muffin_order_distribution_v3;
CREATE TABLE bi.muffin_order_distribution_v3 as 	
SELECT
	city,
	Status,
	order_type,
	locale,
	date::date order_date,
	EXTRACT(WEEK FROM date::date) as CW,
	EXTRACT(DOW FROM date::date) as Weekday,
	hour,
	SUM(CASE WHEN EXTRACT(DOW FROM date) = DAY AND HOUR >= bi.muffin_order_distribution_v2.Start_time and hour < bi.muffin_order_distribution_v2.End_time THEN 1 ELSE 0 END) as Orders
FROM
	bi.muffin_order_distribution_v2
GROUP BY
	CW,
	zipcode,
	Status,
	order_type,
	locale,
	weekday,
	order_date,
	city,
	hour;

DROP TABLE IF EXISTS bi.workcalendar;
DROP TABLE IF EXISTS bi.utalization_temp12;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


END;

$BODY$ LANGUAGE 'plpgsql'