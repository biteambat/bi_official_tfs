CREATE OR REPLACE FUNCTION bi.daily$forecast_functions(crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$forecast_functions';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------- 03_acquisitionsforecast

DROP TABLE IF EXISTS bi.acquisition_kpi;
CREATE TABLE bi.acquisition_kpi as 
SELECT
	LEFT(locale__c,2)::text as locale,
	'Actuals'::text as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE a.Marketing_Channel END as Channel,
	COUNT(1) as value
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and test__c = '0'
	and order_type = '1'
	and order_creation__c::date = customer_creation_date::date
	and (EXTRACT(MONTH FROM Order_Creation__c::date) = EXTRACT(MONTH FROM  current_date) and EXTRACT(YEAR FROM Order_Creation__c::date) = EXTRACT(YEAR FROM  current_date))
	and status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
GROUP BY
	locale,
	Channel;

INSERT INTO bi.acquisition_kpi	
SELECT
	LEFT(locale__c,2) as locale,
	'Runrate per Day' as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
	CAST(COUNT(1)/9 as decimal) as Acquisitions
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and test__c = '0'
	and order_type = '1'
	and order_creation__c::date = customer_creation_date::date
	and Order_Creation__c between (cast(current_date as date)- interval '9 days') and (cast(current_date as date)- interval '1 days')
	and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
	locale,
	Channel;

INSERT INTO bi.acquisition_kpi
SELECT
	a.locale,
	'Forecast' as kpis,
	CASE WHEN a.channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE a.channel END as Channel,
	acquisitions+(b.runrate*(month_days-days)) as value
FROM	
(SELECT
	LEFT(locale__c,2) as locale,
	'Actuals' as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
						  DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
  	Extract(day from current_date-1) as days,
	COUNT(1) as Acquisitions
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and order_type = '1'
	and order_creation__c::date = customer_creation_date::date
	and test__c = '0'
	and (EXTRACT(MONTH FROM Order_Creation__c::date) = EXTRACT(MONTH FROM  current_date) and EXTRACT(YEAR FROM Order_Creation__c::date) = EXTRACT(YEAR FROM  current_date))
	and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
	locale,
	kpi,
	channel,
	days,
	month_days) as a
LEFT JOIN
	(SELECT
	LEFT(locale__c,2) as locale,
	'Runrate per Day' as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
	round(CAST(CAST(COUNT(1) as decimal)/15 as decimal),2) as runrate
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and order_type = '1'
	and test__c = '0'
	and Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days')
	and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
	and order_creation__c::date = customer_creation_date::date
GROUP BY
	locale,
	channel) as b
ON
	a.locale = b.locale and a.channel = b.channel;
	
INSERT INTO bi.acquisition_kpi Values('at','Target','Facebook','28');
INSERT INTO bi.acquisition_kpi Values('at','Target','SEO','19');
INSERT INTO bi.acquisition_kpi Values('at','Target','Brand Marketing','86');
INSERT INTO bi.acquisition_kpi Values('at','Target','SEM','19');
INSERT INTO bi.acquisition_kpi Values('at','Target','Voucher Campaigns','3');
INSERT INTO bi.acquisition_kpi Values('at','Target','Display','23');
INSERT INTO bi.acquisition_kpi Values('at','Target','Unattributed','0');

INSERT INTO bi.acquisition_kpi Values('ch','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Voucher Campaigns','0');
INSERT INTO bi.acquisition_kpi Values('ch','Target','SEM','71');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Brand Marketing','233');
INSERT INTO bi.acquisition_kpi Values('ch','Target','SEO','61');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Facebook','56');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Newsletter','0');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Display','28');

INSERT INTO bi.acquisition_kpi Values('de','Target','Brand Marketing','292');
INSERT INTO bi.acquisition_kpi Values('de','Target','Display','16');
INSERT INTO bi.acquisition_kpi Values('de','Target','SEM','59');
INSERT INTO bi.acquisition_kpi Values('de','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('de','Target','Voucher Campaigns','45');
INSERT INTO bi.acquisition_kpi Values('de','Target','SEO','199');
INSERT INTO bi.acquisition_kpi Values('de','Target','Newsletter','0');
INSERT INTO bi.acquisition_kpi Values('de','Target','Facebook','66');

INSERT INTO bi.acquisition_kpi Values('nl','Target','SEM','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Brand Marketing','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Display','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Voucher Campaigns','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','SEO','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Facebook','0');


-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------- 41_forecastkpis


	DROP TABLE IF EXISTS bi.forecast_kpisdashboard;

	CREATE TABLE bi.forecast_kpisdashboard AS

	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------

	-- PART 1: CURRENT VALUES : DONE

	-- Total GMV current month (except CANCELLED FAKED and MISTAKE)

		SELECT
		left(o.locale__c,2)::text as locale,
		'Total GMV Current month'::text as kpi,
		ROUND(SUM(o.gmv_eur)::numeric,0) as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Total orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Orders # Current month'::text as kpi,
		COUNT(o.order_id__c)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;


	-- Acquisition GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Acquisition GMV Current month'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '1'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;

	-- Acquisition orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Acquisitions # Current month'::text as kpi,
		COUNT(o.order_id__c)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '1'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;

	-- Rebooking GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Rebookings GMV Current month'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Rebooking orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Rebookings # Current month'::text as kpi,
		COUNT(o.order_id__c)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;

	-- RRM1 current month


	-- Global CPA current month

	INSERT INTO bi.forecast_kpisdashboard -- DONT FORGET TO ERASE THE DISCOUNT COSTS THAT SHOULD NOT BE INCLUDED IN THE CPA CALCULATION :)
		
		SELECT

		cpa.locale::text as locale,
		'Current CPA'::text as kpi,
		SUM(
			cpa.sem_brand
			+cpa.sem_non_brand
			+cpa.offline_marketing
			+cpa.facebook 
			+(cpa.gdn) 
			+(cpa.criteo) 
			+(cpa.sociomantic) 
			+(cpa.coops) 
			+cpa.other_voucher
			--+(cpa.vouchercosts)
			+cpa.tvcampaign
			+cpa.deindeal_voucher
			+cpa.seo)
		/
		SUM(cpa.all_acquisitions)::numeric as value

		--SUM([Sem Brand]+[Sem Non Brand]+[Offline Marketing]+[Facebook]+[Sociomantic]+[Criteo]+[Gdn]+[Gpdd Voucher]+[Coops]+[Other Voucher]+[Vouchercosts]+[Tvcampaign]+[Seo acq])/SUM([All Acquisitions])

		FROM bi.cpacalclocale cpa

		WHERE extract(month from cpa.date) = extract(month from current_date)
			and extract(year from cpa.date) = extract(year from current_date)

		GROUP BY locale, kpi

		ORDER BY locale asc
	;

	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------

	-- PART 2 : FORECASTS EOM 

	-- Forecast Total GMV Current month

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		f.locale::text as locale,
		'Forecast - Total GMV Current month'::text as kpi,
		ROUND(SUM(f.acquisition_forecast + f.rebookings_forecast)::numeric,0) as value

		FROM
			bi.booking_forecast_locale f

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Forecast Total orders current month (except CANCELLED FAKED and MISTAKE)


	-- Forecast Acquisition GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		f.locale::text as locale,
		'Forecast - Acquisition GMV Current month'::text as kpi,
		SUM(f.acquisition_forecast)::numeric as value

		FROM
			bi.booking_forecast_locale f

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Forecast Acquisition orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		a.locale::text as locale,
		'Forecast - Acquisition # Current month'::text as kpi,
		SUM(CASE WHEN a.kpi = 'Forecast' THEN a.value END)::numeric as value

		FROM
			bi.acquisition_kpi a

		GROUP BY
			locale

		ORDER BY
			locale asc

	;

	-- Forecast Rebooking GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		f.locale::text as locale,
		'Forecast - Rebookings GMV Current month'::text as kpi,
		SUM(f.rebookings_forecast)::numeric as value

		FROM
			bi.booking_forecast_locale f

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;


	-- Forecast B2B GMV current month


	-- Forecast RRM1 current month

		INSERT INTO bi.forecast_kpisdashboard

		SELECT 
		rr.locale::text as locale,
		'RR M1 Forecast'::text as kpi,
		rr.rr_m1_forecast as value

		FROM bi.rr_forecast rr

	;

	-- Forecast RRM3 current month

	-- Forecast RRM6 current month


	-- Forecast Global CPA current month

	INSERT INTO bi.forecast_kpisdashboard

		SELECT

		locale::text as locale,
		'Forecast CPA EOM'::text as kpi,

		((SUM(case when channel = 'Overall' and extract(month from dates) = extract(month from current_date) and extract(year from dates) = extract(year from current_date) then cost else NULL end) 
		/ extract(day from max(dates))
		* DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))
		/
		(SUM(case when channel = 'Overall' and extract(month from dates) = extract(month from current_date) and extract(year from dates) = extract(year from current_date) then acquisitions else NULL end) 
		/ extract(day from max(dates))
		* DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)))::numeric as value

		FROM bi.coststransform 

		WHERE channel = 'Overall'

		GROUP BY locale

		ORDER BY locale asc

	;
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------

	-- PART 3: TARGETS : DONE

	-- Setting acquisitions GMV targets per country

	
/*	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'60000'::numeric as value
	;
*/	
	-- Setting acquisitions # targets per country

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'178'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'677'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'448'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'0'::numeric as value
	;

	-- Setting Rebookings GMV targets per country
/*
	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'60000'::numeric as value
	;
*/
	-- Setting Rebookings # targets per country

/*	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Rebookings # Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Rebookings # Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Rebookings # Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Rebookings # Target'::text as kpi,
		'60000'::numeric as value
	;
*/
	-- Setting Total GMV target for all countries

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	-- Setting Total # target for all countries
/*
	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Total # orders Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Total # orders Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Total # orders Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Total # orders Target'::text as kpi,
		'60000'::numeric as value
	;
*/

	-- Setting CPA Targets for all countries 

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'CPA Target'::text as kpi,
		'69'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'CPA Target'::text as kpi,
		'52'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'CPA Target'::text as kpi,
		'91'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'CPA Target'::text as kpi,
		'0'::numeric as value
	;

	-- Setting RR M1 Targets for all countries

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'RR M1 Target'::text as kpi,
		'20'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'RR M1 Target'::text as kpi,
		'35'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'RR M1 Target'::text as kpi,
		'36'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'RR M1 Target'::text as kpi,
		'31'::numeric as value
	;

	-- acquisitions GMV targets per country
	-- acquisitions # targets per country

	-- Rebookings GMV targets per country
	-- Rebookings # targets per country

	-- Total GMV target for all countries
	-- Total # target for all countries

	-- CPA Targets for all countries 
	-- RR M1 Targets for all countries

	INSERT INTO bi.forecast_kpisdashboard

		SELECT

		locale::text as locale,
		'GMV Daily Run Rate'::text as kpi,
		round(max(value) / Extract(day from current_date)::numeric,0) as value

		FROM bi.forecast_kpisdashboard

		WHERE kpi = 'Total GMV Current month'

		GROUP BY locale, kpi

		ORDER BY locale asc

	;

		INSERT INTO bi.forecast_kpisdashboard

			SELECT

			locale::text as locale,
			'% on target GMV €'::text as kpi,
			ROUND(max(value)::numeric / (CASE WHEN locale = 'at' THEN 40433 WHEN locale = 'de' THEN 698468.54 WHEN locale = 'ch' THEN 222890 WHEN locale = 'nl' THEN 24994 END)*100,2) as value

			FROM bi.forecast_kpisdashboard

			WHERE kpi = 'Forecast - Total GMV Current month'

			GROUP BY locale, kpi

			ORDER BY locale asc

	;


		INSERT INTO bi.forecast_kpisdashboard

			SELECT

			locale::text as locale,
			'% on target Acquisitions #'::text as kpi,
			ROUND(max(value)::numeric / (CASE WHEN locale = 'at' THEN 260 WHEN locale = 'de' THEN 2650 WHEN locale = 'ch' THEN 680 WHEN locale = 'nl' THEN 187 END)*100,2)  as value

			FROM bi.forecast_kpisdashboard

			WHERE kpi = 'Forecast - Acquisition # Current month'

			GROUP BY locale, kpi

			ORDER BY locale asc
	;

-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------- 46_RR_forecastperlocale

	DROP TABLE IF EXISTS bi.temp_acquisitions_locale_L30D;
	CREATE TABLE bi.temp_acquisitions_locale_L30D AS

		SELECT
		left(o.locale__c,2) as locale,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.order_creation__c >= (current_date - interval '30 days') THEN 1 ELSE NULL END) as locale_acquisitions_L30D,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.order_creation__c >= (current_date - interval '90 days') AND o.order_creation__c < (current_date - interval '60 days') THEN 1 ELSE NULL END) as locale_acquisitions_L60to90D

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND order_type = '1'
			AND o.marketing_channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND o.order_creation__c >= (current_date - interval '90 days')

		GROUP BY left(o.locale__c,2)

		ORDER BY left(o.locale__c,2)

	;

	-- avg RR L30D per channel and locale

	DROP TABLE IF EXISTS bi.temp_RR_L30D;
	CREATE TABLE bi.temp_RR_L30D AS

		SELECT
			r.locale as locale,
			r.channel as channel,
			'p1'::text as period,
			SUM(r.cohort_return_rate * r.acquisitions)/SUM(r.acquisitions) as rr_avg_L15D

		FROM bi.retention_marketing r
		WHERE r.date < current_date and r.date >= (current_date - interval '15 days')
			AND r.kpi_type = 'Channel analysis'
			AND r.channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND r.acquisitions > '0'
			AND r.period = 'p1'
		GROUP BY r.locale, r.channel, period
		ORDER BY r.locale asc, r.channel asc

	;

	INSERT INTO bi.temp_RR_L30D
		
		SELECT
			r.locale as locale,
			r.channel as channel,
			'p3'::text as period,
			SUM(r.cohort_return_rate * r.acquisitions)/SUM(r.acquisitions) as rr_avg_L15D

		FROM bi.retention_marketing r
		WHERE r.date < current_date and r.date >= (current_date - interval '15 days')
			AND r.kpi_type = 'Channel analysis'
			AND r.channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND r.acquisitions > '0'
			AND r.period = 'p3'
		GROUP BY r.locale, r.channel, period
		ORDER BY r.locale asc, r.channel asc

	;

	-- Channel share L30D ====> ONLY FOR RR P1 THEN !!!!

	DROP TABLE IF EXISTS bi.temp_acquisition_share_L30D;
	CREATE TABLE bi.temp_acquisition_share_L30D AS
		
		SELECT

			left(o.locale__c,2) as locale,
			CASE WHEN o.marketing_channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE o.marketing_channel END as channel,
			SUM(CASE WHEN o.order_creation__c >= (current_date - interval '30 days') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END)::numeric/AVG(a.locale_acquisitions_L30D)::numeric as channel_share_L30D,
			SUM(CASE WHEN o.order_creation__c >= (current_date - interval '90 days') AND o.order_creation__c < (current_date - interval '60 days') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END)::numeric/AVG(a.locale_acquisitions_L60to90D)::numeric as channel_share_L60to90D

		FROM bi.orders o

		JOIN bi.temp_acquisitions_locale_L30D a ON a.locale = left(o.locale__c,2)

		WHERE o.test__c = '0'
			AND o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND order_type = '1'
			AND o.marketing_channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND o.order_creation__c >= (current_date - interval '90 days')

		GROUP BY left(o.locale__c,2), CASE WHEN o.marketing_channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE o.marketing_channel END

		ORDER BY locale asc, channel asc

	;

	DROP TABLE IF EXISTS bi.temp_rr_forecast_calculation;

	CREATE TABLE bi.temp_rr_forecast_calculation AS 

		SELECT
			t1.locale,
			t1.channel,
			t1.channel_share_L30D,
			t1.channel_share_L60to90D,
			t2.rr_avg_L15D,
			t3.rr_avg_L15D as rr_avg_L60to90D

		FROM bi.temp_acquisition_share_L30D t1

		JOIN bi.temp_RR_L30D t2 ON t1.locale = t2.locale
			AND t1.channel = t2.channel 
			AND t2.period = 'p1'

		JOIN bi.temp_RR_L30D t3 ON t1.locale = t3.locale
			AND t1.channel = t3.channel 
			AND t3.period = 'p3'

	;

	DROP TABLE IF EXISTS bi.rr_forecast;
	CREATE TABLE bi.rr_forecast AS

		SELECT
			f.locale,
			SUM(f.channel_share_L30D*f.rr_avg_L15D) as rr_m1_forecast,
			SUM(f.channel_share_L60to90D*f.rr_avg_L60to90D) as rr_m2_forecast
		FROM bi.temp_rr_forecast_calculation f

		GROUP BY f.locale

		ORDER BY f.locale asc

	;

	DROP TABLE IF EXISTS bi.temp_acquisitions_locale_L30D;
	DROP TABLE IF EXISTS bi.temp_RR_L30D;
	DROP TABLE IF EXISTS bi.temp_acquisition_share_L30D;
	DROP TABLE IF EXISTS bi.temp_rr_forecast_calculation;


-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------- 51_invoiced_gmv_forecast


	DROP TABLE IF EXISTS bi.invoiced_gmv_forecast_polygon;
	CREATE TABLE bi.invoiced_gmv_forecast_polygon AS

		SELECT
			LEFT(o.locale__c,2) as locale,

			o.polygon as city_polygon,
		MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) )) as days_left_currMonth,

			SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END) as invoiced_gmv_currmonth,

			(SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND 	 (o.effectivedate::date <= current_date::date - 1) AND (o.effectivedate::date > current_date::date - 16)  THEN o.gmv_eur ELSE 0 END)/15)::numeric as rr_L15D,




			(SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END))
				+ 

			((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 16) THEN o.gmv_eur ELSE 0 END)/15)::numeric)
				*
				(MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) ))) 

			as forecast,




			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END) as invoiced_acq_currmonth,

			(SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate < current_date - 1) AND (o.effectivedate >= current_date - 16)THEN 1 ELSE 0 END)/15)::numeric as rr_L15D_acq,

			(SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END)) 
				+ ((SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate < current_date - 1) AND (o.effectivedate >= current_date - 16)THEN 1 ELSE 0 END)/15)::numeric) 
				* (MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-2) )))

			as forecast_invoiced_acquisitions,



			CASE WHEN (EXTRACT (DAY from current_date)) > '10' THEN

								((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END))
									+ 

								((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 16) THEN o.gmv_eur ELSE 0 END)/15)::numeric)
									*
									(MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) ))) )

			ELSE


								(((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 31) THEN o.gmv_eur ELSE 0 END)/30)::numeric)
									*
									(    DATE_PART('days', DATE_TRUNC('month', current_date::date) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)))

			END


			as adjusted_forecast


		FROM bi.orders o

		WHERE o.test__c = '0' and order_type = '1' and (LEFT(o.locale__c,2) = LEFT(o.polygon,2) OR o.polygon IS NULL)

		GROUP BY LEFT(o.locale__c,2), o.polygon

		ORDER BY locale asc, city_polygon asc
	;


-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------


	DELETE FROM bi.invoiced_gmv_locked_polygon WHERE forecast_date = current_date;

	INSERT INTO bi.invoiced_gmv_locked_polygon

		SELECT
			current_date::date as forecast_date,
			i.locale as locale,
			i.city_polygon as city_polygon,
			i.forecast as forecast
		FROM bi.invoiced_gmv_forecast_polygon i

	;





	DELETE FROM bi.adjusted_forecast_comparison WHERE forecast_date = current_date;

	INSERT INTO bi.adjusted_forecast_comparison

		SELECT
			current_date::date as forecast_date,
			i.locale as locale,
			i.city_polygon as city_polygon,
			i.forecast as old_forecast,
			i.adjusted_forecast as new_forecast
		FROM bi.invoiced_gmv_forecast_polygon i

	;





	DELETE FROM bi.invoiced_acq_locked_polygon WHERE forecast_date = current_date;

	INSERT INTO bi.invoiced_acq_locked_polygon

		SELECT
			current_date::date as forecast_date,
			i.locale::text as locale,
			i.city_polygon::text as city_polygon,
			i.forecast_invoiced_acquisitions::numeric as forecast
		FROM bi.invoiced_gmv_forecast_polygon i

	;

	DROP TABLE IF EXISTS bi.temp_supplies_forecast;
	CREATE TABLE bi.temp_supplies_forecast AS 

		SELECT
			t1.locale,
			t1.polygon,
			SUM(t1.supply_revenue) as supply_forecast

		FROM 
			(SELECT
			 left(t1.locale__c,2) as locale,
			 t1.polygon,
			 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
			 bi.orders t1
			LEFT JOIN
			 Salesforce.Opportunity t2
			ON
			 (t2.sfid = t1.opportunityid)
			WHERE
			 type = 'cleaning-b2b'
			 and status NOT LIKE ('%CANCELLED%')
			 and EXTRACT(MONTH from Effectivedate) = EXTRACT(MONTH from current_date)
			 and EXTRACT(YEAR from Effectivedate) = EXTRACT(YEAR from current_date)
			GROUP BY
			 t2.name,
			 contact__c,
			 contact_name__c,
			 left(t1.locale__c,2),
			 t1.polygon) t1

		GROUP BY t1.locale, t1.polygon

	;

	DROP TABLE IF EXISTS bi.temp_b2b_bookings_forecast;

	CREATE TABLE bi.temp_b2b_bookings_forecast AS

		SELECT
			left(o.locale__c,2) as locale,

			o.polygon as polygon,

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)) as days_left_currMonth,

			SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END) as actual_sales_currMonth,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
			THEN o.gmv_eur ELSE 0 END)
			/
			14) as run_rate_L14D,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
			THEN o.gmv_eur ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as bookings_forecast,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND o.acquisition_new_customer__c = '1'
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
						AND o.acquisition_new_customer__c = '1'
			THEN o.gmv_eur ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as acquisition_forecast,


			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND o.acquisition_new_customer__c = '0'
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
						AND o.acquisition_new_customer__c = '0'
			THEN o.gmv_eur ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as rebookings_forecast

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.order_type = '2'
			AND o.status not like ('%CANCELLED%')

		GROUP BY left(o.locale__c,2), o.polygon

		ORDER BY locale asc, o.polygon asc

	;

	DROP TABLE IF EXISTS bi.b2b_forecast;

	CREATE TABLE bi.b2b_forecast AS

		SELECT

			t1.locale,
			t1.polygon,
			t1.acquisition_forecast::numeric as acquisition_forecast,
			t1.rebookings_forecast::numeric as rebookings_forecast,
			t1.bookings_forecast::numeric as bookings_forecast,
			t2.supply_forecast::numeric as supply_forecast

		FROM bi.temp_b2b_bookings_forecast t1

		LEFT JOIN bi.temp_supplies_forecast t2 ON (t1.locale = t2.locale AND t1.polygon = t2.polygon)

	;

	DELETE FROM bi.locked_forecast_history_B2B WHERE date = current_date::date;

	INSERT INTO bi.locked_forecast_history_B2B

		SELECT

			current_date::date as date,
			t1.locale::text,
			t1.acquisition_forecast::numeric as acquisition_forecast,
			t1.rebookings_forecast::numeric as rebookings_forecast,
			t1.bookings_forecast::numeric as bookings_forecast,
			t2.supply_forecast::numeric as supply_forecast

		FROM bi.temp_b2b_bookings_forecast t1

		JOIN bi.temp_supplies_forecast t2 ON (t1.locale = t2.locale)

	;

	DROP TABLE IF EXISTS bi.invoiced_forecast_b2c_b2b;
	CREATE TABLE bi.invoiced_forecast_b2c_b2b AS

		SELECT
			f.locale,
			f.city_polygon,
			SUM(f.forecast) as invoiced_gmv_forecast,
			(CASE WHEN (AVG(b.bookings_forecast + b.supply_forecast)) IS NOT NULL THEN (AVG(b.bookings_forecast + b.supply_forecast))
				ELSE 0 END)
			as b2b_forecast,
			SUM(f.forecast) + (CASE WHEN (AVG(b.bookings_forecast + b.supply_forecast)) IS NOT NULL THEN (AVG(b.bookings_forecast + b.supply_forecast)) ELSE 0 END) 
			as b2c_b2b_invoiced_forecast,
			CASE WHEN f.city_polygon = 'at-vienna' THEN 23274
				 WHEN f.city_polygon = 'ch-zurich' THEN 263234
				 WHEN f.city_polygon = 'nl-amsterdam' THEN 0 
				 WHEN f.city_polygon = 'de-berlin' then 255451
				 ELSE 0 END as b2c_target,
			CASE WHEN f.city_polygon = 'at-vienna' THEN 0
				 WHEN f.city_polygon = 'ch-zurich' THEN 49326
				 WHEN f.city_polygon = 'nl-amsterdam' THEN 26159
				 WHEN f.city_polygon = 'de-berlin' THEN 105147
				 ELSE 0 END as b2b_target

		FROM bi.invoiced_gmv_forecast_polygon f

			LEFT JOIN bi.b2b_forecast b ON f.locale = b.locale 
				AND f.city_polygon = b.polygon

		GROUP BY f.locale, f.city_polygon

	;



	DROP TABLE IF EXISTS bi.temp_b2b_bookings_forecast;
	DROP TABLE IF EXISTS bi.temp_supplies_forecast;




-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.working_days_newforecast;
CREATE TABLE bi.working_days_newforecast AS

	SELECT
				LEFT(o.locale__c,2) as locale,

				o.polygon as city_polygon,

				(SELECT count(*)-1 as wds_gone_currMonth
				FROM generate_series(0, (current_date::date - (current_date - interval '15 days')::date::date)) i
				WHERE date_part('dow', (current_date - interval '15 days')::date::date + i) NOT IN (0,6))
				as workingdays_in_L15D,

				((SELECT count(*) 
				FROM generate_series(0, (((date_trunc('MONTH', current_date::date) + INTERVAL '1 MONTH - 1 day')::date) - current_date::date::date)) i
				WHERE date_part('dow', current_date::date::date + i) NOT IN (0,6)))		
				as workingdays_left_currMonth,

				SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and current_date::date - interval '1 day') THEN o.gmv_eur_net ELSE 0 END) 
				as invoiced_gmv_L14D,

				SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur_net ELSE 0 END) 
				as invoiced_gmv_currmonth,

				SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and current_date::date - interval '1 day') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE 0 END) 
				as invoiced_acq_L14D,

				SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END) 
				as invoiced_acq_currmonth,





				 (SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur_net ELSE 0 END))

				 + 

				 (
				 	(((SELECT count(*) 
				FROM generate_series(0, (((date_trunc('MONTH', current_date::date) + INTERVAL '1 MONTH - 1 day')::date) - current_date::date::date)) i
				WHERE date_part('dow', current_date::date::date + i) NOT IN (0,6)))	)

				 	*

				 	((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and current_date::date - interval '1 day')  THEN o.gmv_eur_net ELSE 0 END))/((SELECT count(*)-1 as wds_gone_currMonth
				FROM generate_series(0, (current_date::date - (current_date - interval '15 days')::date::date)) i
				WHERE date_part('dow', (current_date - interval '15 days')::date::date + i) NOT IN (0,6))))
				 )

				as forecast_invoiced_gmv,



				 (SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END))

				 + 

				 (
				 	(	((SELECT count(*) 
				FROM generate_series(0, (((date_trunc('MONTH', current_date::date) + INTERVAL '1 MONTH - 1 day')::date) - current_date::date::date)) i
				WHERE date_part('dow', current_date::date::date + i) NOT IN (0,6))))

				 	*

				 	(
				 	
				 	(SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and 				current_date::date - interval '1 day') AND o.acquisition_new_customer__c = '1' and date_part('dow',Effectivedate::date) not in ('0','6') THEN 1 ELSE 0 END))
				 	
				 	
				 	/((SELECT count(*)-1 as wds_gone_currMonth
				FROM generate_series(0, (current_date::date - (current_date - interval '15 days')::date::date)) i
				WHERE date_part('dow', (current_date - interval '15 days')::date::date + i) NOT IN (0,6))))
				 )

				as forecast_invoiced_acquisitions 




			FROM bi.orders o

			WHERE o.test__c = '0' and order_type = '1' and (LEFT(o.locale__c,2) = LEFT(o.polygon,2) OR o.polygon IS NULL)

			GROUP BY LEFT(o.locale__c,2), o.polygon

			ORDER BY locale asc, city_polygon asc

;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
  
 END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;