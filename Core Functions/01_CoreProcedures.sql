
CREATE OR REPLACE FUNCTION bi.daily$01_coreprocedures(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$01_coreprocedures';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

-- CPA Calculation

DROP TABLE IF EXISTS bi.VoucherCosts;
CREATE TABLE bi.VoucherCosts as 
SELECT
	CAST(Order_Creation__c as DATE) as OrderDate,
	LEFT(Locale__c,2) as locale,
	Voucher__c,
	CASE 
	WHEN Voucher__c = 'TIGERRAUSCH20' THEN COUNT(DISTINCT(Order_Id__C))*7 
	WHEN Voucher__c = 'SPARTIGER20' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'IMMERSAUBER15' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'PUTZENLASSEN22' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'CLEANPONY' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'PUTZCHECKER14' THEN COUNT(DISTINCT(Order_Id__c))*22
	WHEN Voucher__c = 'TIGER4YOU' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'TIGER1H' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'TIGERSTUFF' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'TIGERFITO20' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'TIGEREF15' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'CPRTGR2320' THEN COUNT(DISTINCT(Order_Id__c))*15
	ELSE COUNT(DISTINCT(Order_Id__c))*7
	END as VoucherCosts
FROM
	Salesforce.Order t1
WHERE
	(Voucher__c in ('TIGERRAUSCH20','SPARTIGER20','TLCTIGER15','PUTZENLASSEN22','IMMERSAUBER15','TIGERGM','CLEANPONY','TIGER4YOU','CPRTGR2320','TIGER1H','TIGERSTUFF','TIGERFITO20','TIGEREF15') or Voucher__c like '%CRB%')
	and t1.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
GROUP BY
	CAST(Order_Creation__c as DATE),
	locale,
	Voucher__c;

DROP TABLE IF EXISTS bi.VoucherCosts2;
CREATE TABLE bi.VoucherCosts2 as 	
SELECT
	orderdate,
	locale,
	sum(CASE WHEN vouchercosts is null then 0 else vouchercosts end) as VoucherCosts
FROM
	bi.VoucherCosts t3
GROUP BY
	orderdate,
	locale;
  
DROP TABLE IF EXISTS bi.MarketingCostsTableau;
CREATE TABLE bi.MarketingCostsTableau as 
SELECT
  Date(t1.Orderdate) as Date,
  t1.*,
  CASE WHEN t4.VoucherCosts is null THEN 0 ELSE t4.VoucherCosts END as VoucherCosts,
  (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN (22*0.6) ELSE 0 END) as GPDD_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as Other_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Acquisition_New_Customer__c = '0' THEN Discount__c ELSE 0 END) as Other_Voucher_reb,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Acquisitions,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' and t2.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') THEN 1 ELSE 0 END) as Valid_Acquisitions,
  Count(1) as All_Orders,
  SUM(Order_Duration__c) as Hours
FROM
  bi.MarketingCostsImport t1
JOIN
  Salesforce.Order t2
ON
  (CAST(t1.Orderdate  as DATE) = CAST(t2.Order_Creation__c as DATE) and t2.Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') and test__c = '0' )
LEFT JOIN
	bi.VoucherCosts2 t4
ON
	(CAST(t1.Orderdate  as DATE) = t4.orderdate)
GROUP BY
  Date(t1.Orderdate),
  sem_non_brand,
  sem_brand,
  display,
  seo,
  seo_brand,
  facebook,
  offline_marketing,
  tvcampaign,
  youtube,
  t1.coops,
  VoucherCosts;

DROP TABLE IF EXISTS bi.cpacalclocale;
CREATE TABLE bi.cpacalclocale as 
SELECT
  CAST(Order_Creation__C as Date)  as Date,
  sum(CASE WHEN Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END)  as All_Acquisitions,
  sum(CASE WHEN Acquisition_New_Customer__c = '1' and t2.status not in ('CANCELLED NOT THERE YET','CANCELLED NO MANPOWER') THEN 1 ELSE 0 END)  as All_Acquisitions_Non_NTY,
  COUNT(1) as Orders,
  SUM(CASE WHEN Marketing_Channel = 'DTI' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as DTI_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEO Brand' and Acquisition_New_Customer__c = '1'THEN 1 ELSE 0 END) as SEOBrand_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEM' and Acquisition_New_Customer__c = '1'  THEN 1 ELSE 0 END) as SEM_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEM Brand' and Acquisition_New_Customer__c = '1'  THEN 1 ELSE 0 END) as SEM_Brand_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Display' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Display_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Facebook' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 end) as Facebook_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEO' and Acquisition_New_Customer__c = '1'  THEN 1 ELSE 0 END) as SEO_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Voucher Campaigns' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Vouchercampaigns_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Brand Marketing Offline' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as BrandMktgOffline_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEM' and Acquisition_New_Customer__c = '1' and status not like '%CANCELLED%' THEN Discount__c ELSE 0 END) as SEM_Discount,
  SUM(CASE WHEN Marketing_Channel = 'SEM Brand' and Acquisition_New_Customer__c = '1' and status not like '%CANCELLED%' THEN Discount__c ELSE 0 END) as SEMBrand_Discount,
  SUM(CASE WHEN Marketing_Channel = 'Display' and Acquisition_New_Customer__c = '1' and status not like '%CANCELLED%' THEN Discount__c ELSE 0 END) as Display_Discout,
  SUM(CASE WHEN Marketing_Channel = 'Facebook' and Acquisition_New_Customer__c = '1' and status not like '%CANCELLED%' THEN Discount__c ELSE 0 END) + SUM(CASE WHEN Marketing_Channel = 'Facebook' and Acquisition_New_Customer__c = '1' and voucher__c = 'FBKIT' and status not like ('%CANCELLED%') THEN '20'::numeric ELSE 0 END) as Facebook_Discount,
  SUM(CASE WHEN Marketing_Channel = 'Voucher Campaigns' and Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Acquisition_New_Customer__c = '1' and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%DEINDEAL%' and Voucher__c not like '%DEAL%' THEN Discount__c ELSE 0 END)+(SUM(CASE WHEN Marketing_Channel = 'Voucher Campaigns' and Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Acquisition_New_Customer__c = '1' and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') THEN (Discount__c*0.8)-(22*0.6) ELSE 0 END)) as Vouchercampaigns_Discount,
  SUM(CASE WHEN Marketing_Channel = 'Brand Marketing Offline' and Acquisition_New_Customer__c = '1' and status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') THEN Discount__c ELSE 0 END) as BrandMktgOffline_Discount,
  (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-(22*0.6) as GPDD_Voucher,
  CASE WHEN SUM(CASE WHEN (Voucher__c like '%DEINDEAL%' or Voucher__c like '%DEAL%') and Status not like '%CANCELLED%' THEN 1 ELSE 0 END) > 0 THEN (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%DEINDEAL%' or Voucher__c like '%DEAL%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-(80*0.65) ELSE 0 END as Deindeal_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Voucher__c not like '%DEINDEAL%' and Voucher__c not like '%DEAL%' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END ) as Other_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Voucher__c not like '%DEINDEAL%' and voucher__c::text not in ('FRUEHLING20','FRUEHJAHR20','SAUBER20','GLANZ20','PUTZ20','STEUER20','HAUS20','TECH20','RECHT20','ELTERN20','HAPPY20','BER20','BOOTE20','CT20','BOGLAR20','BOGLFZ20','SAUBER15','PROPER20','FDFLY1J','FDFLY2J','FDFLY3J','FDFLY4J','FDFLY5J','FDFLY6J','FDFLY7J','FDFLY8J','FDFLY9J','FDFLYAJ','FDFLYBJ','FDFLYCJ','FDFLYDJ','FDFLYEJ','FDFLYFJ','FDFLYGJ','FDFLYHJ','FDFLYKJ','FDFLYLJ','PRC1X','PRC2X','PRC3X','PRC4X','PRC5X','PRC6X','PRC7X','PRC8X','PRC9X','PRC1Y','PRC2Y','PRC3Y','PRC4Y','PRC5Y','PRC6Y','PRC7Y','PRC8Y','PRC9Y','PRC1Z','PRC2Z','PRC3Z','PRC4Z','PRC5Z','PRC6Z','PRC7Z','PRC8Z','PRC9Z','SEE35','MAG35','LUZERN35','ZÜRICHSAUBER','STGALLEN35','BERN35','BASEL35','GENEVA35','LAUSANNE35','TRAM35','BATFR75','TIGER75','ZÜRICH35','20MIN50','FUW50','FEM50','TUTTI50','PUTZ35','TAG50','TRIBUNE50','20MIN70','RAB50','PUTZ40','BLATT50','WINTI35','TUTTI35','FRIDAY50','ENCORE50','BILAN50','BILAN70','ENCORE70','TRIBUNE70','FRIDAY70','TAG70','BLATT70','FEM70','ANIBIS50','EBAY50','SCHOON1H','PEPER20','WEWORK20','LZYCMP2AH','VRIEND20','OOSTER20','MODELLISTID20','HUIS20','HE20','PLLEK20','YACHT20','HAYS20','APPLIFIED20','RBI20','AUTO20','UVA20','PAPABUBBLE20','SPARK20','WORKON20','TONY20','TONYTIGER20','SYMPHONY20','FRIS20','AMS15','AMS20','MOVE20') and Acquisition_New_Customer__c = '0' THEN Discount__c ELSE 0 END ) as Other_Voucher_reb,
  t1.*,
  CASE WHEN t3.VoucherCosts is null THEN 0 ELSE t3.VoucherCosts END as VoucherCosts
FROM
     bi.marketingspending_locale t1
 JOIN
  bi.orders t2
ON
  (CAST(Order_Creation__C as Date) = CAST(t1.Orderdate as date) and t2.Order_Creation__c::date = t2.customer_creation_date::date and t1.locale = LEFT(t2.locale__c,2) and order_type = '1') 
LEFT JOIN
	bi.VoucherCosts2 t3
ON
	(CAST(Order_Creation__C as Date) = t3.orderdate and t1.locale = t3.locale)
WHERE
 	t2.Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')  and test__c = '0'
GROUP BY
  CAST(Order_Creation__C as Date),
  CAST(t1.Orderdate as date),
 sem_non_brand,
  sem_brand,
  seo,
  criteo,
  sociomantic,
  gdn,
  facebook,
  offline_marketing,
  tvcampaign,
  youtube,
  t1.coops,
  t1.locale,
  VoucherCosts;

DROP TABLE IF EXISTS bi.cpa_by_locale;
CREATE TABLE bi.cpa_by_locale as 
SELECT
  t1.*,
  CASE WHEN t4.VoucherCosts is null THEN 0 ELSE t4.VoucherCosts END as VoucherCosts,
  (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN (22*0.6) ELSE 0 END) as GPDD_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Voucher__c not like '%DEINDEAL%' and Voucher__c not like '%DEAL%' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as Other_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Acquisition_New_Customer__c = '0' THEN Discount__c ELSE 0 END) as Other_Voucher_reb,
      (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%DEINDEAL%' or Voucher__c like '%DEAL%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-(89*0.6) as Deindeal_Voucher,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Acquisitions,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' and t2.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') THEN 1 ELSE 0 END) as Valid_Acquisitions,
  Count(1) as All_Orders,
  SUM(Order_Duration__c) as Hours
FROM
  bi.marketingspending_locale t1
JOIN
  Salesforce.Order t2
ON
  (CAST(t1.Orderdate  as DATE) = CAST(t2.Order_Creation__c as DATE) and t2.Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') and test__c = '0' and LEFT(t2.locale__c,2) = t1.locale)
LEFT JOIN
	bi.VoucherCosts2 t4
ON
	(CAST(t1.Orderdate  as DATE) = t4.orderdate and t1.locale = t4.locale)
GROUP BY
  Date(t1.Orderdate),
  sem_non_brand,
  sem_brand,
  seo,
  criteo,
  sociomantic,
  gdn,
  facebook,
  offline_marketing,
  tvcampaign,
  youtube,
  t1.coops,
  t1.locale,
  VoucherCosts;

DROP TABLE IF EXISTS bi.VoucherCosts;

DROP TABLE IF EXISTS bi.MarketingVouchers;
CREATE TABLE bi.MarketingVouchers as 
SELECT
  CAST(Order_Creation__c as DATE)  as OrderDate,
  CASE WHEN Acquisition_New_Customer__c = '1' THEN 'Acquisitions' ELSE 'Rebookings' END as Ordertype,
  LEFT(Locale__c,2) as Locale,
  CASE 
  WHEN Voucher__c like '%CDD%' or Voucher__c like '%CGP%' THEN 'GROUPON'
  WHEN Voucher__C like 'CRB%' THEN 'RUBLYS'
  ELSE Voucher__c
  END  as Voucher,
  count(1)
FROM
  Salesforce.Order
WHERE
  Voucher__c is not null
  and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') and test__c = '0' 
GROUP BY
   Date(Order_Creation__c),
   Locale__c,
   OrderType,
   Voucher;

DROP TABLE IF EXISTS bi.VoucherAcquisitions;
CREATE TABLE bi.VoucherAcquisitions as 
SELECT
  CAST(Order_Creation__c as Date) as OrderDate,
  CASE WHEN Acquisition_New_Customer__c = '1' THEN 'Acquisitions' ELSE 'Rebookings' END as Ordertype,
  LEFT(Locale__c,2) as Locale,
  CASE
  WHEN Voucher__c like '%CGP%' THEN 'Groupon'
  WHEN Voucher__c like '%CDD%' THEN 'DailyDeal'
  WHEN Voucher__c like '%CLM%' THEN 'Limango'
  WHEN Voucher__c like '%CRB%' THEN 'Rublys'
  WHEN Voucher__c like '%CM%' THEN 'CM'
  ELSE Voucher__c 
  END as Voucher,
  Count(1) as Order_Count
FROM
  Salesforce.Order
Where
  Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  and (Voucher__c is not null and Voucher__c != '')
GROUP BY
  CAST(Order_Creation__c as Date),
  locale,
  Ordertype,
  Voucher;
 
-- GMV Forecast


DROP TABLE IF EXISTS bi.booking_forecast_1;
CREATE TABLE bi.booking_forecast_1 as 
SELECT
	LEFT(locale__c,2) as locale,
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/14),0) as Acquisition_Runrate_Per_Day,
		round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/14),0) as Rebookings_Runrate_Per_Day,
	  DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
   Extract(day from current_date-1) as days
FROM
	bi.orders
WHERE
	Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  	AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days')
	and order_type = '1' 
GROUP BY
	LEFT(locale__c,2);

DROP TABLE IF EXISTS bi.booking_forecast_2;
CREATE TABLE bi.booking_forecast_2 as 
SELECT
	LEFT(locale__c,2) as locale,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN GMV_Eur ELSE 0 END) as Acquisition_hours_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN GMV_Eur ELSE 0 END) as Rebookings_hours_this_month
FROM
	bi.orders
WHERE
	 Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','NOSHOW CUSOTMER','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  	 AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM ORder_Creation__c::date) and EXTRACT(Year FROM ORder_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
  	 AND order_type = '1'
GROUP BY
	left(locale__c,2); 
 
DROP TABLE IF EXISTS bi.booking_forecast_locale;
CREATE TABLE bi.booking_forecast_locale as 
SELECT
	t1.locale,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days))  Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)) as rebookings_Forecast,
  t3.acquisition_forecast as b2b_acquisitions_forecast,
  t3.rebookings_forecast as b2b_rebookings_forecast,
  t3.supply_forecast as b2b_supply_forecast,
  (t3.bookings_forecast + t3.supply_forecast) as b2b_forecast
FROM
	bi.booking_forecast_2 t1
left join
	bi.booking_forecast_1 t2
ON 
	(t1.locale = t2.locale)
left join
 	(SELECt
	locale,
	SUM(acquisition_forecast) as acquisition_forecast,
	SUM(rebookings_forecast) as rebookings_forecast,
	SUM(bookings_forecast) as bookings_forecast,
	Sum(supply_forecast) as supply_forecast
FROM
 bi.b2b_forecast 
GROUP BY
	locale) as t3
ON
	(t1.locale = t3.locale)
WHERE
	t1.locale in ('ch','at')
GROUP BY
	t1.locale,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
	Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	Rebookings_Forecast,
  month_days,
  days,
  t3.acquisition_forecast,
  t3.rebookings_forecast,
  t3.bookings_forecast,
  t3.supply_forecast;

INSERT INTO bi.booking_forecast_locale
SELECT
	t1.locale,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days)*1.0)  Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)*1.0) as rebookings_Forecast,
  t3.acquisition_forecast as b2b_acquisitions_forecast,
  t3.rebookings_forecast as b2b_rebookings_forecast,
  t3.supply_forecast as b2b_supply_forecast,
  (t3.bookings_forecast + t3.supply_forecast) as b2b_forecast
FROM
  bi.booking_forecast_2 t1
left join
  bi.booking_forecast_1 t2
ON 
  (t1.locale = t2.locale)
left join
 	(SELECt
	locale,
	SUM(acquisition_forecast) as acquisition_forecast,
	SUM(rebookings_forecast) as rebookings_forecast,
	SUM(bookings_forecast) as bookings_forecast,
	Sum(supply_forecast) as supply_forecast
FROM
 bi.b2b_forecast 
GROUP BY
	locale) as t3
ON (t1.locale = t3.locale)
WHERE
	t1.locale in ('de')
GROUP BY
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  Rebookings_Forecast,
  month_days,
  days,
  t3.acquisition_forecast,
  t3.rebookings_forecast,
  t3.bookings_forecast,
  t3.supply_forecast;


INSERT INTO bi.booking_forecast_locale
SELECT
	t1.locale,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days))  Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)) as rebookings_Forecast,
  t3.acquisition_forecast as b2b_acquisitions_forecast,
  t3.rebookings_forecast as b2b_rebookings_forecast,
  t3.supply_forecast as b2b_supply_forecast,
  (t3.bookings_forecast + t3.supply_forecast) as b2b_forecast
FROM
  bi.booking_forecast_2 t1
left join
  bi.booking_forecast_1 t2
ON 
  (t1.locale = t2.locale)
left join
 	(SELECt
	locale,
	SUM(acquisition_forecast) as acquisition_forecast,
	SUM(rebookings_forecast) as rebookings_forecast,
	SUM(bookings_forecast) as bookings_forecast,
	Sum(supply_forecast) as supply_forecast
FROM
 bi.b2b_forecast 
GROUP BY
	locale) as t3
ON (t1.locale = t3.locale)
WHERE
	t1.locale = 'nl'
GROUP BY
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  Rebookings_Forecast,
  month_days,
  days,
  t3.acquisition_forecast,
  t3.rebookings_forecast,
  t3.bookings_forecast,
  t3.supply_forecast;

DROP TABLE IF EXISTS bi.booking_forecast_1;
DROP TABLE IF EXISTS bi.booking_forecast_2; 

DELETE FROM bi.locked_forecast_history WHERE date = current_date;
INSERT INTO bi.locked_forecast_history
  SELECT
    current_date as date,
    t1.locale as locale,
    (t1.acquisition_forecast + t1.rebookings_Forecast) as forecast
  FROM bi.booking_forecast_locale t1

;

-- International Cockpit

DROP TABLE IF EXISTS bi.date;
CREATE TABLE bi.date as 
SELECT
	Order_Creation__c::date as date,
	locale	
FROM
	bi.orders t1,
	unnest(array['de','ch','at','nl']) as locale
GROUP BY
	date,
	locale;

DROP TABLE IF EXISTS bi.international_cockpit_temp1;
CREATE TABLE bi.international_cockpit_temp1 as 
SELECT
	a.date,
	a.locale,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN 1 ELSE 0 END) as all_nc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN 1 ELSE 0 END) as all_rc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN Order_Duration__c ELSE 0 END) as all_nc_hours,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN Order_Duration__c ELSE 0 END) as all_rc_hours,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN 1 ELSE 0 END) as valid_nc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN 1 ELSE 0 END) as valid_rc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN order_Duration__c ELSE 0 END) as valid_nc_hours,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN order_duration__c ELSE 0 END) as valid_rc_hours,
	SUM(CASE WHEN Status in ('ALLOCATION PAUSED','INVOICED','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO INVOICE','PENDING TO START','PENDING VALIDATION','WAITING FOR ACCEPTANCE','WAITING FOR RESCHEDULE','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN gmv_eur ELSE 0 END) as valid_gmv,
	SUM(CASE WHEN Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN gmv_eur ELSE 0 END) as all_gmv
FROM
	bi.date a
LEFT JOIN
	bi.orders as b
ON
	(a.date = b.order_Creation__c::date and a.locale = LEFT(b.locale__c,2) and test__c = '0' and order_type = '1')
GROUP BY
	date,
	locale;

DROP TABLE IF EXISTS bi.international_cockpit_temp2;
CREATE TABLE bi.international_cockpit_temp2 as 
SELECT
	date,
	locale,
	SUM(Cleaners_inactive) as cleaner_inactive,
	SUM(Cleaners_active) as Cleaner_active
FROM
	bi.cleaneractivity_change
GROUP BY
	date,
	locale;

DROP TABLE IF EXISTS bi.international_cockpit_temp3;
CREATE TABLE bi.international_cockpit_temp3 as 
SELECT
	created_at::date as date,
	LEFT(REPLACE(CAST(professional_json->'Locale__c' as varchar),'"',''),2) as locale,
	COUNT(1) as Offboardings
FROM
	events.sodium
WHERE
	event_name in ('Account Event:LEFT','Account Event:TERMINATED')
GROUP BY
	date,
	locale;
	
DROP TABLE IF EXISTS bi.international_cockpit_temp4;
CREATE TABLE bi.international_cockpit_temp4 as 
SELECT
	CASE WHEN type__c = '60' then hr_contract_start__c::date ELSE createddate::date END as date,
	LEFT(locale__C,2) as locale,
	COUNT(1) as Onboardings
FROM
	salesforce.account
WHERE
	test__c = '0'
	and Status__c != 'SUBCONTRACTOR' and Status__c != ''
GROUP BY
	date,
	locale;    
    
DROP TABLE IF EXISTS bi.international_cockpit_temp5;
CREATE TABLE bi.international_cockpit_temp5 as  
SELECT
	date,
   	locale,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(all_acq) as Acquisitions
FROM
    bi.cpacalcpolygon
GROUP BY
    locale,
    date;

DROP TABLE IF EXISTS bi.international_cockpit_temp9;
CREATE TABLE bi.international_cockpit_temp9 as 	
SELECT
	MIN(Effectivedate::Date) as Date,
	LEFT(Locale__c,2) as locale,
	to_Char(Effectivedate::Date,'YYYY-WW') as Week,
	COUNT(DISTINCT(Professional__c)) as Distinct_Cleaner,
	SUM(Order_Duration__c) as Hours
FROM
	bi.orders
WHERE
	Status = 'INVOICED'
	and order_type = '1'
GROUP BY
	Week,
	locale;

-- DROP TABLE IF EXISTS bi.sourcelead;
-- CREATE TABLE bi.sourcelead as
-- SELECT
	-- created_at::date as date,
	-- LEFT(locale,2) as locale,
	-- COUNT(1) as Leads
-- FROM
	-- main.lead
-- WHERE
	-- created_at::date >= '2015-01-01'
-- GROUP BY
	-- created_at::date,
	-- LEFT(locale,2);




-- DRoP TABLE IF EXISTS bi.leadbysource_2; 
-- CREATE TABLE bi.leadbysource_2 as 	
-- SELECT
	-- date,
	-- locale,
	-- SUM(Leads) as Leads
-- FROM
	-- bi.sourcelead
-- GROUP BY
	-- date,
	-- locale;

-- DROP TABLE IF EXISTS bi.leadcosts2;
-- CREATE TABLE bi.leadcosts2 as 
-- SELECT
	-- date as date,
	-- lower(locale) as locale,
	-- SUM(Costs) as Costs
-- FROM
	-- bi.LeadCosts
-- GROUP BY
	-- date,
	-- lower(locale);
	
-- DROP TABLE IF EXISTS bi.international_cockpit_temp8;
-- CREATE TABLE bi.international_cockpit_temp8 as 
-- SELECT
	-- t2.date::date as date,
	-- t2.locale,
	-- sum(costs) as costs,
	-- SUM(t1.leads) as leads
-- FROM
	-- bi.leadcosts2 t2
-- LEFT JOIN
	-- bi.leadbysource_2 t1
-- ON
	-- (t1.date::date = t2.date::date and t1.locale = t2.locale)
-- GROUP BY
	-- t2.date,
	-- t2.locale;

DROP TABLE IF EXISTS bi.leadcosts2;
DRoP TABLE IF EXISTS bi.leadbysource_2; 
DROP TABLE IF EXISTS bi.sourcelead;

DROP TABLE IF EXISTS bi.international_cockpit;
CREATE TABLE bi.international_cockpit as 
SELECT
	t1.*,
	t3.cleaner_inactive,
	t3.cleaner_active,
	CASE WHEN t4.onboardings is null then 0 else t4.onboardings END as onboardings,
	CASE WHEN t5.offboardings is null then 0 else t5.offboardings END as offboardings,
	t6.costs,
	t6.acquisitions,
  t12.leads as leads_verification,
  t12.onboardings as onboardings_verification,
  t12.costs as lead_costs,
  rebookings_retention_m3,
  acquisitions_retention_m3,
  rebookings_retention_m1,
  acquisitions_retention_m1
FROM
	bi.international_cockpit_temp1 t1 
LEFT JOIn
	bi.international_cockpit_temp2 t3
ON
	(t1.locale = t3.locale and t1.date = t3.date)
LEFT JOIN
	bi.international_cockpit_temp3 t5
ON
	(t1.locale = t5.locale and t1.date = t5.date)
LEFT JOIN
	bi.international_cockpit_temp4 t4
ON
	(t1.locale = t4.locale and t1.date = t4.date)
LEFT JOIN
	bi.international_cockpit_temp5 t6
ON
	(t1.locale = t6.locale and t1.date = t6.date)
LEFT JOIN
	(SELECT
	acquisition_date + Interval '60 days' as date,
	locale,
	CAST(SUM(rebooking_m1) as decimal) as rebookings_retention_m1,
	COUNT(DISTINCT(customer_id)) as acquisitions_retention_m1
FROM
	bi.retention_data
GROUP BY
	date,
	locale) t8
ON
	(t1.locale = t8.locale and t1.date = t8.date)
LEFT JOIN
	(SELECT
	acquisition_date + Interval '120 days' as date,
	locale,
	CAST(SUM(rebooking_m3) as decimal) as rebookings_retention_m3,
	COUNT(DISTINCT(customer_id)) as acquisitions_retention_m3
FROM
	bi.retention_data
GROUP BY
	date,
	locale) t9
ON
	(t1.locale = t9.locale and t1.date = t9.date)
LEFT JOIN 
  (SELECT
	date,
	locale,
	sum(classifieds_costs+sem_costs+fb_costs) as costs,
	sum(leads) as leads,
	sum(onboardings) as onboardings
FROM
	bi.cleaners_costsperonboarding_polygon
GROUP BY
	date,
	locale) as t12
ON (t1.locale = t12.locale and t1.date = t12.date)

WHERE
	t1.date >= '2015-01-01';

DROP TABLE IF EXISTS bi.international_cockpit_temp1;
DROP TABLE IF EXISTS bi.international_cockpit_temp2;
DROP TABLE IF EXISTS bi.international_cockpit_temp3;
DROP TABLE IF EXISTS bi.international_cockpit_temp4;
DROP TABLE IF EXISTS bi.international_cockpit_temp5;


DROP TABLE IF EXISTS  bi.trial_kpis;
CREATE TABLE bi.trial_kpis as 
SELECT
	t2.date as date,
	LEFT(locale,2) as locale,
	CAST('trial customer' as varchar) as kpi,
	count(distinct(t1.customer_Id__c)) as value
FROM
(SELECT
	customer_id__c,
	LEFT(locale__c,2) as locale,
	SUM(CASE WHEN (acquisition_channel__c = 'web' and (recurrency__c = '0' or recurrency__c is null) and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null)) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
GROUP BY
	customer_id__c,
	locale
HAVING
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
	(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null)and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t2
ON
	(t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
		(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t3
ON
	(t1.customer_id__c = t3.customer_id__c) 
GROUP BY
	t1.locale,
	t2.date;
	
INSERT INTO bi.trial_kpis
SELECT
	t2.date as date,
	t1.locale,
	'subscriber CVR' as kpi,
	count(distinct(t1.customer_Id__c)) as unique_customer
FROM
(SELECT
	customer_id__c,
	LEFT(t1.locale__c,2) as locale,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
GROUP BY
	customer_id__c,
	locale
HAVING
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
	(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t2
ON
	(t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
		(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t3
ON
	(t1.customer_id__c = t3.customer_id__c) 
WHERE
	t3.date is not null
GROUP BY
	t1.locale,
	t2.date;
	
INSERT INTO bi.trial_kpis
  SELECT
	t2.date as date,
	t1.locale,
	'Trials CVR' as kpi,
	count(distinct(t1.customer_Id__c)) as unique_customer
FROM
(SELECT
	customer_id__c,
	LEFT(t1.locale__c,2) as locale,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
GROUP BY
	customer_id__c,
	locale
HAVING
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
	(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t2
ON
	(t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
		(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t3
ON
	(t1.customer_id__c = t3.customer_id__c) 
GROUP BY
	t1.locale,
	t2.date;


INSERT INTO bi.trial_kpis
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	'All Website Orders' as All_Orders,
	SUM(CASE WHEN acquisition_channel__c = 'web' THEN 1 ELSE 0 END) as all_website_orders
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
GROUP BY
	date,
	locale;

INSERT INTO bi.trial_kpis	
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	'All Website Trial Orders' as All_Orders,
	SUM(CASE WHEN acquisition_channel__c = 'web' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (recurrency__c = '0' or recurrency__c is null) THEN 1 ELSE 0 END) as all_website_orders
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
GROUP BY
	date,
	locale;
	
INSERT INTO bi.trial_kpis	
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	'All Website Trial Acquisitions' as All_Orders,
	SUM(CASE WHEN acquisition_channel__c = 'web' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (recurrency__c = '0' or recurrency__c is null) THEN 1 ELSE 0 END) as all_website_orders
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and acquisition_New_customer__c = '1'
GROUP BY
	date,
	locale;
	
INSERT INTO bi.trial_kpis
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	'All Website Acquisitions' as All_Orders,
	SUM(CASE WHEN acquisition_channel__c = 'web' THEN 1 ELSE 0 END) as all_website_orders
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and acquisition_new_customer__c = '1'
GROUP BY
	date,
	locale;
	
INSERT INTO bi.trial_kpis
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	CASE 
	WHEN recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and acquisition_channel__c in ('web','ios','android') THEN 'Funnel Trials'
	WHEN (IP__c = '62.72.67.58' or acquisition_channel__c = 'salesforce') and acquisition_tracking_id__c = 'express-checkout' THEN 'Last Minute CM'
	WHEN IP__c != '62.72.67.58' and acquisition_tracking_id__c = 'express-checkout' and acquisition_channel__c in ('web','ios','android')  THEN 'Last Minute Customer'
	ELSE 'Other Orders' END as types,
	
	COUNT(DISTINCT(Order_Id__c)) as all_website_orders
FROM
	bi.orders t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and order_type = '1'
	and recurrency__c = '0'
GROUP BY
	date,
	locale,
	types;
	
-- Funnel Availability History

DELETE FROM bi.funnel_availability_history WHERE date = current_Date::Date;

INSERT INTO bi.funnel_availability_history
SELECT
	current_date::date as date,
	*
FROM
	bi.funnel_availability;


-- Facebook Leadgen Calc

DROP TABLE IF EXISTS bi.fb_voucher_acquisitions;
CREATE TABLE bi.fb_voucher_acquisitions AS

SELECT

	o.order_id__c as order_id,
	order_creation__c::timestamp::date as order_created,
	left(o.delivery_area,2) as locale,
	replace(o.delivery_area,'+','') as city,
	CASE WHEN (o.acquisition_channel_params__c like ('%"utm_source":"facebook"%') and o.acquisition_channel_params__c like ('%"utm_medium":"email"%')) THEN 'Facebook - after emailing'
		WHEN (o.acquisition_channel_params__c like ('%"utm_source":"facebook"%') and o.acquisition_channel_params__c like ('%"utm_medium":"display"%')) THEN 'Facebook - display ad'
		WHEN (o.acquisition_channel_params__c like '{}' or o.acquisition_channel_params__c like ('{"ref":"bookatiger.de"}') or o.acquisition_channel_params__c like ('{"ref":"bookatiger.nl"}') or o.acquisition_channel_params__c like ('{"ref":"bookatiger.at"}') or o.acquisition_channel_params__c like ('{"ref":"bookatiger.ch"}')) THEN 'DTI'
		WHEN (o.acquisition_channel_params__c like ('{"clid":"goob","gkey":"bookatiger","gclid":"%"}') or (o.acquisition_channel_params__c like ('{"clid":"goog","gclid":"%"}'))) THEN 'SEM'
		ELSE ('Other') END as acquisition_tracking,

    CASE WHEN o.voucher__c like ('FBML%') THEN 'Facebook - after emailing'
      WHEN o.voucher__c NOT like ('FBML%') THEN 'Facebook - display ad'
    END as acquisition_tracking_2,
    
	o.voucher__c as voucher,
	acquisition_channel_params__c as acquisition_tag

FROM bi.orders o

WHERE o.marketing_channel = 'Facebook'
	and o.voucher__c in ('WISCHMOP3C','SPRINGFB20','SPRINGFB50','SPRINGFB3P','WISCH1FB','RABATT20','FBPRIMA20','FBPUTZCH40','FBPUTZ20','TIGER1FB','FBTIGER15','FBPUTZ15','FBTIGER50','FBTIGER20','STAR50','FBTIGER30','FBTIGER40','FBTIGER15','FBTIG70','FBML15','FBML20','FBML25','FBML30','FBML50','FBML70')
	and o.test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and o.acquisition_new_customer__c = '1'
  	and order_type = '1'

ORDER BY order_created desc
;

-- FRF Program

DROP TABLE IF EXISTS bi.frf_tracking_1;
CREATE TABLE bi.frf_tracking_1 as 
SELECT
	Order_Id__c,
	referred_by__c,
	Order_Creation__c,
	Voucher__c,
	status
FROM
	salesforce.order
WHERE
	Voucher__c = 'FRF'
	and referred_by__c is not null;

DROP TABLE IF EXISTS bi.frf_tracking;
CREATE TABLE bi.frf_tracking as 
SELECT
	t2.sfid as salesforce_id,
	t2.name as Referrer,
	SUM(CASE WHEN t1.status not like '%CANCELLED%' THEN 1 ELSE 0 END) as Valid_Referred_Orders,
	SUM(CASE WHEN t1.status like '%CANCELLED%' THEN 1 ELSE 0 END) as Cancelled_Referred_Orders,
	SUM(CASE WHEN t1.status not like '%CANCELLED%' THEN 1 ELSE 0 END) +SUM(CASE WHEN t1.status like '%CANCELLED%' THEN 1 ELSE 0 END) as All_Orders
FROM
	bi.frf_tracking_1 t1
JOIN
	salesforce.contact t2
ON
	(t1.referred_by__c = t2.sfid)
GROUP BY
	t2.sfid,
	t2.name;

DROP TABLE IF EXISTS bi.frf_tracking_1;

-- LAST100D_Acquisitions

	
	DROP TABLE IF EXISTS bi.acquisitions_last100D_channel;

	CREATE TABLE bi.acquisitions_last100D_channel AS

		SELECT 

		o.order_creation__c::timestamp::date as orderdate,
		left(o.locale__c,2) as country,
		o.marketing_channel as marketing_channel,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' and order_creation__c::date = customer_creation_date::date THEN 1 ELSE NULL END) as acquisitions,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status LIKE ('%CANCELLED%') and order_creation__c::date = customer_creation_date::date THEN 1 ELSE NULL END) as cancelled_acquisitions

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND o.order_creation__c >= Current_date - interval '100 days'
			AND order_creation__c::timestamp::date < current_date 
			and order_type = '1'

		GROUP BY orderdate, country, marketing_channel

		ORDER BY orderdate desc, country asc, marketing_channel asc

	;

-- Order events

DROP TABLE IF EXISTS main.order_events;
CREATE TABLE main.order_events as

select
		  created_at
		, replace(event_name, 'Order Event:','') as order_event
		, order_json::json->>'Id' as Order_ID
		, order_json->>'EffectiveDate' as Order_Start_Date
from events.sodium
where substring(event_name,1,12) = 'Order Event:';


-- Costs per Likelie

DROP TABLE IF EXISTS bi.temp_b2c_likelies;
CREATE TABLE bi.temp_b2c_likelies AS	

	SELECT		

		t.createddate::date as likeli_creation,

		left(t.locale__c,2) as locale,

		CASE 
		        
		        WHEN (t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		        THEN 'SEM'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'SEO'::text
		       	            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN ((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
			            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
			            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text)
					
					OR ((t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text)

					OR ((t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text))

				THEN 'Brand Marketing'::text
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		COUNT(1) as nb_likelies

	FROM
	Salesforce.likeli__c t

	WHERE
	t.createddate::date >= '2016-03-01'
	AND t.acquisition_tracking_id__c = 'appbooking'
	
	GROUP BY locale, likeli_creation, source_channel
	ORDER BY likeli_creation desc, locale asc, source_channel asc

;

DROP TABLE IF EXISTS bi.b2c_cost_per_likelies;
CREATE TABLE bi.b2c_cost_per_likelies AS

	SELECT 
		t1.likeli_creation as date,
		t1.locale,
		t1.source_channel,
		t1.nb_likelies,
		t2.cost

	FROM bi.temp_b2c_likelies t1
		JOIN bi.coststransform t2
			ON t1.likeli_creation = t2.dates
			AND t1.locale = t2.locale
			AND t1.source_channel = t2.channel
			AND t1.source_channel IN ('Brand Marketing','SEM','SEO','Display','Facebook')


;

DROP TABLE IF EXISTS bi.b2c_likelies_conversion;

CREATE TABLE bi.b2c_likelies_conversion AS

	SELECT
	c.*,
	SUM(a.acquisitions) as nb_acquisitions,
	ROUND(SUM(a.acquisitions)::numeric/c.nb_likelies,4) as conversion_ratio

	FROM bi.b2c_cost_per_likelies c
		
		JOIN bi.acquisitions_last100d_channel a 
			ON c.date = a.orderdate
			AND c.locale = a.country
			AND c.source_channel = (CASE WHEN a.marketing_channel in ('DTI','SEM Brand','SEO Brand') THEN 'Brand Marketing'
															ELSE a.marketing_channel END)
			AND c.date < current_date
			AND a.acquisitions::numeric < c.nb_likelies

	WHERE c.nb_likelies > 5

	GROUP BY c.date, c.locale, c.source_channel, c.nb_likelies, c.cost

	ORDER BY c.date desc, c.locale asc, c.source_channel asc

;

DROP TABLE IF EXISTS bi.temp_b2c_likelies;

-- CPA with trials


DROP TABLE IF EXISTS bi.temp_cpatrials_1;
CREATE TABLE bi.temp_cpatrials_1 AS

SELECT
	*
FROM
	 (SELECT
		left(locale__c,2) as co,
		order_creation__c::date as creation_date,
		count(distinct(customer_id__c)) as New_CustoID,
		sum(case when recurrency__c = '0' THEN order_duration__c ELSE 0 END) as Trial_hours,
		count(distinct(case when recurrency__c > '0' THEN customer_id__c ELSE NULL END)) as New_CustoID_no_trial

	FROM
		bi.orders

	WHERE
		order_creation__c::date >= '2016-07-01'
	 	and acquisition_new_customer__c = '1'
	 	and test__c = '0'
	 	and status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')

	GROUP BY
		co,
		creation_date) as t1

	
	LEFT JOIN

	(SELECT
		orderdate as DateofOrder,
		locale as countr,
		sum(sem_non_brand + sem_brand + seo + facebook + criteo + sociomantic + gdn + youtube + coops + tvcampaign + offline_marketing + other_voucher + deindeal_voucher) as Mkg_Costs

	FROM
		bi.cpacalclocale

	WHERE
		orderdate between '2016-07-01' and '2016-09-18'

	GROUP BY
		DateofOrder,
		countr) as t2 


	ON (t1.Creation_date::date = t2.DateofOrder::date and t1.co = t2.countr);


DROP TABLE IF EXISTS bi.temp_cpatrials_2;
CREATE TABLE bi.temp_cpatrials_2 AS

	SELECT
		countr as Country,
		creation_date as Date,
		sum(New_CustoID) as New_Subs,
		sum(New_CustoID_no_trial) as New_Subs_no_trial,
		sum(Mkg_Costs) as Total_costs_Mkg,
		12,25 * sum(Trial_hours) as Trial_Costs
		
	FROM bi.temp_cpatrials_1

	GROUP BY
		Country,
		Date,
		New_CustoID

;

DROP TABLE IF EXISTS bi.cpa_w_trials;
CREATE TABLE bi.cpa_w_trials AS

	SELECT
		Country,
		date::date as date,
		sum(New_Subs) as NS,
		sum(New_Subs_no_trial) as NSNT,
		sum(Total_costs_Mkg) as MkgCosts,
		sum(Trial_Costs) as MkgCosts_w_Trial

	FROM bi.temp_cpatrials_2

	GROUP BY
		Country,
		date

;

DROP TABLE IF EXISTS bi.temp_cpatrials_1;
DROP TABLE IF EXISTS bi.temp_cpatrials_2;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;

$BODY$ LANGUAGE 'plpgsql'