CREATE OR REPLACE FUNCTION bi.daily$mfunc_core(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$mfunc_core';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	PERFORM bi.mfunc_gpm(crunchdate);
	PERFORM bi.mfunc_retention(crunchdate);
	PERFORM bi.mfunc_city_overview(crunchdate);

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'