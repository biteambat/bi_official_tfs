DELIMITER //


CREATE OR REPLACE FUNCTION bi.sfunc_gpm_subcontractors(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_gpm_subcontractors';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN 

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.account_temp;
CREATE TABLE bi.account_temp as 

SELECT  

   t1.name,
   t1.company_name__c,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.delivery_areas__c,
   CASE WHEN t1.company_name__c LIKE '%Ali Kocahal%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Akzent Facility Services GmbH%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Sadettin Akal%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%ATIS Gebäudereinigung de- Cologne%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%blitzgebäudereinigung de-Hamburg%' THEN 'de-hamburg'
		  WHEN t1.company_name__c LIKE '%Kostic%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Crystal Facility Service GmbH de-berlin%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Herr Kovacs%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Jefa Gebäudereinigung%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%KNM Gebäudeservice GmbH%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%Medical Clean Management GmbH de-munich%' THEN 'de-munich'
		  WHEN t1.company_name__c LIKE '%Dordevic%' THEN 'de-munich'
		  WHEN t1.company_name__c LIKE '%Stamm´s Haus & Hof Service%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%TMG Gebäudereinigung und Service de-stuttgart%' THEN 'de-stuttgart'
		  WHEN t1.company_name__c LIKE '%1636583753%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Kovacs%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Korkmaz%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Czerwinski%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%Kordian%' THEN 'de-frankfurt'
	ELSE t1.delivery_areas__c
	END as new_areas,
	t1.longitude__c,
	t1.latitude__c
     
FROM
 
   Salesforce.account t1
WHERE
	t1.type__c = 'cleaning-b2b'
      
GROUP BY
 
   t1.name,
   t1.company_name__c,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.delivery_areas__c,
   t1.longitude__c,
	t1.latitude__c,
   new_areas;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.cleaner_geoloc_temp;
CREATE TABLE bi.cleaner_geoloc_temp as 

SELECT  

   t1.name,
   t1.sfid,
   t1.parentid,
   t1.new_areas,
   t1.pph__c,
   CASE WHEN t1.new_areas IS NOT NULL THEN t1.new_areas ELSE t2.key__c END as delivery_area,
   ST_Intersects( 
   ST_SetSRID(ST_Point(t1.longitude__c::float, t1.latitude__c::float), 4326), 
   ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
     
FROM
 
   bi.account_temp t1,
   salesforce.delivery_area__c t2
      
GROUP BY
 
   t1.name,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.new_areas,
	delivery_area,
	flag_polygon
     
HAVING
 
    ST_Intersects(
    ST_SetSRID(ST_Point(t1.longitude__c::float, t1.latitude__c::float), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true
    
UNION

	SELECT  

   t1.name,
   t1.sfid,
   t1.parentid,
   t1.new_areas,
   t1.pph__c,
   t1.new_areas,
 	TRUE as flag_polygon
     
FROM
 
   bi.account_temp t1
      
GROUP BY
 
   t1.name,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.new_areas,
	flag_polygon
     
HAVING
     	t1.New_areas IS NOT NULL;
	 

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


-- CLEANER STATS TEMP

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.name,
	t1.sfid as cleanerid,
	t1.parentid,
	t2.company_name__c,
	t2.parentid as sub_id,
	t2.sfid,
	t2.status__c,
	t1.delivery_area as delivery_areas,
	t2.type__c,
	t2.hr_contract_start__c::date as contract_start,
	t2.hr_contract_end__c::date as contract_end,
	0 as weekly_hours,
	t2.pph__c as pph_subcontractor,
	t1.pph__c as pph_cleaner
FROM
	 bi.cleaner_geoloc_temp t1
LEFT JOIN
	Salesforce.Account t2
ON
	(t1.parentid = t2.sfid)
WHERE 

	(t1.name NOT LIKE '%BAT%' AND t1.name NOT LIKE '%BOOK%') 
	AND t2.test__c = '0' 
	AND t2.name NOT LIKE '%test%' AND t2.name NOT LIKE '%Test%' AND t2.name NOT LIKE '%TEST%'
	AND t2.type__c = 'cleaning-b2b'
	AND t1.name NOT IN ('B2BS FreshFolds');
	
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
CREATE TABLE bi.gmp_per_cleaner_temp as 

SELECT

	t1.professional__c,
	t1.delivery_area__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN GMV__c
	         WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN GMV__c*1.19 
				ELSE 0 END) as GMV,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN GMV__c ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN GMV__c*1.19 ELSE 0 END) as b2b_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN order_Duration__c ELSE 0 END) + SUM(CASE WHEN (type = 'cleaning-b2b') AND Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2c_hours,
	SUM(CASE WHEN (type = 'cleaning-b2b') AND Status IN  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2b_hours
		
FROM

	Salesforce.order t1
	
WHERE

	t1.professional__c IS NOT NULL
	
GROUP BY

	t1.professional__c,
	t1.delivery_area__c,
	date;
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GMP PER CLEANER V1 TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
CREATE TABLE bi.gmp_per_cleaner_v1_temp as 

SELECT

	t1.name,
	t1.cleanerid as sfid,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	t2.gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours,
	CASE WHEN t1.pph_cleaner IS NOT NULL THEN t1.pph_cleaner ELSE t1.pph_subcontractor END as pph__c
	
FROM

	bi.cleaner_Stats_temp t1
	
LEFT JOIN

	bi.gmp_per_cleaner_temp t2
	
ON

	(t1.cleanerid = t2.professional__c);

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER V2

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_temp as 

SELECT 

	t2.*, 
	GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
	CASE
	WHEN year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
	WHEN to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
	ELSE calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
	END AS days_worked
	
FROM (

	SELECT	
	
	   to_char(date,'YYYY-MM') as Year_Month,
	   MIN(date) as mindate,
	   t1.name,
	   t1.sfid,
		t1.sfid as professional__c,
		t1.pph__c,
		delivery_areas as delivery_area,
		DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
		contract_start,
		contract_end,
		type__c,
		SUM(hours) as worked_hours,
		CAST(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
		SUM(GMV) as GMV,
		SUM(GMV/1.19) as total_revenue,
		SUM(B2C_GMV/1.19) as B2C_Revenue,
		SUM(B2B_GMV/1.19) as B2B_Revenue,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2b_share,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2c_share,
		MAX(Weekly_hours) as weekly_hours,
		MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL - DATE_TRUNC('month', DATE))/7 )as monthly_hours
		
	FROM
	
		bi.gmp_per_cleaner_v1_temp t1
		
	WHERE
	
		date >= '2016-01-01'
		
	GROUP BY
	
	   Year_Month,
		professional__c,
		t1.sfid,
		t1.name,
		t1.pph__c,
		contract_start,
		days_of_month,
		type__c,
		contract_end,
		delivery_area,
		calc_end
		) as t2;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
   professional__c,
   pph__c,
   delivery_area,
   worked_hours,
   type__c,
   contract_start,
   contract_end,
   monthly_hours,
   weekly_hours,
   days_worked,
   days_of_month,
   MIN(mindate) as mindate,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	worked_hours*pph__c as salary_payed,
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV/1.19) as Revenue,
	SUM(GMV/1.19) - SUM(worked_hours)*pph__c  as gp,
	CASE WHEN SUM(GMV/1.19) > 0 THEN (SUM(GMV/1.19) - SUM(worked_hours)*pph__c)/SUM(GMV/1.19) ELSE 0 END as gpm_subcontractor

FROM

     bi.gpm_per_cleaner_v2_temp
     
WHERE

    LEFT(delivery_area,2) IN ('de', 'nl')
    AND days_worked > '0'
    
GROUP BY
    year_month,
    professional__c,
    pph__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    days_worked,
    worked_hours;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_subcontractor;
CREATE TABLE bi.gpm_subcontractor as

SELECT

	t2.name as name_account,
	t1.*

FROM

	bi.gpm_per_cleaner_v3_temp_temp t1
	
LEFT JOIN
	Salesforce.Account t2	
ON
	(t1.professional__c = t2.sfid)
	
LEFT JOIN
	bi.KPI_Performance t5	
ON
	(t1.professional__c = t5.sfid)
	
ORDER BY

	name_account;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.account_temp;
DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;
DROP TABLE IF EXISTS bi.cleaner_geoloc_temp;



-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


END;

$BODY$ LANGUAGE 'plpgsql'