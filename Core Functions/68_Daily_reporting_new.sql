CREATE OR REPLACE FUNCTION bi.daily$daily_reporting(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$daily_reporting';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

DROP TABLE IF EXISTS bi.daily_reporting;
CREATE TABLE bi.daily_reporting AS

	SELECT

		1::numeric as id,
		LEFT(o.locale__c,2) as locale,

		(current_date - 1)::date as date,


		SUM(CASE WHEN 
			(o.effectivedate = (current_date-1)::date 
			AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			) 
		THEN o.gmv_eur_net ELSE 0 END)::numeric

		as net_executed_revenue,


		CASE WHEN (				SUM(CASE WHEN 
					(o.effectivedate = (current_date-8)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
					) 
				THEN o.gmv_eur_net ELSE 0 END)

				)::numeric

		> 0 THEN


				(
					(SUM(CASE WHEN 
					(o.effectivedate = (current_date-1)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
					) 
				THEN o.gmv_eur_net ELSE 0 END)

				-

				SUM(CASE WHEN 
					(o.effectivedate = (current_date-8)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
					
				THEN o.gmv_eur_net ELSE 0 END))

				/

				SUM(CASE WHEN 
					(o.effectivedate = (current_date-8)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
					) 
				THEN o.gmv_eur_net ELSE 0 END)

				)::numeric 

		ELSE NULL END

		as pcent_var_1,


		CASE WHEN (			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric) > 0 

		THEN

			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric 

		ELSE NULL END

		as percent_cancellations,


		CASE WHEN (			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))) > 0

		THEN

			(((1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric
		-
			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-8)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-8)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric)

		/

			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric
			)

		ELSE NULL END

		as pcent_var_2,



		COUNT(DISTINCT CASE WHEN 
			(o.order_creation__c::date = (current_date-1)::date 
			AND o.acquisition_new_customer__c = '1'
			and customer_creation_date::date = order_creation__c::Date
			AND o.order_type = '1'
			AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')) 
		THEN o.sfid ELSE NULL END)

		as total_acquisitions_b2c,



		CASE WHEN ((COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-8)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END))::numeric) > 0 

		THEN

			(COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)
		-
			COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-8)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END))
		/
			(COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-8)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END))::numeric

		ELSE NULL END

		as pcent_var_3,


		CASE WHEN (COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)::numeric) > 0


		THEN

			(COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.recurrency__c = '0'
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)::numeric)

		ELSE NULL END

		as percent_trial_share_b2c,


		CASE WHEN ((COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)) > 0 

			AND (COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-1)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric) > 0)

		THEN (

			CASE WHEN (			(COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric)
				) > 0 



			THEN



				((COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-1)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-1)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric)
			-
				(COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric))
			/
				(COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric)

			ELSE NULL END
		)

		ELSE NULL END

		as pcent_var_4


	FROM bi.orders o

	WHERE o.test__c = '0'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')

	GROUP BY LEFT(o.locale__c,2)

	ORDER BY locale asc

;







DROP TABLE IF EXISTS bi.daily_reporting_all;
CREATE TABLE bi.daily_reporting_all AS

	SELECT

		2::numeric as id,

		'All'::text as locale,

		(current_date - 1)::date as date,


		SUM(CASE WHEN 
			(o.effectivedate = (current_date-1)::date 
			AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			) 
		THEN o.gmv_eur_net ELSE 0 END)::numeric

		as net_executed_revenue,


		CASE WHEN (				SUM(CASE WHEN 
					(o.effectivedate = (current_date-8)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
					) 
				THEN o.gmv_eur_net ELSE 0 END)

				)::numeric

		> 0 THEN


				(
					(SUM(CASE WHEN 
					(o.effectivedate = (current_date-1)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
					) 
				THEN o.gmv_eur_net ELSE 0 END)

				-

				SUM(CASE WHEN 
					(o.effectivedate = (current_date-8)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
					
				THEN o.gmv_eur_net ELSE 0 END))

				/

				SUM(CASE WHEN 
					(o.effectivedate = (current_date-8)::date 
					AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
					) 
				THEN o.gmv_eur_net ELSE 0 END)

				)::numeric 

		ELSE NULL END

		as pcent_var_1,


		CASE WHEN (			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric) > 0 

		THEN

			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric 

		ELSE NULL END

		as percent_cancellations,


		CASE WHEN (			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))) > 0

		THEN

			(((1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric
		-
			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-8)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-8)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric)

		/

			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date 
				AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.effectivedate = (current_date-1)::date) 
			THEN o.sfid ELSE NULL END)::numeric))::numeric
			)

		ELSE NULL END

		as pcent_var_2,



		COUNT(DISTINCT CASE WHEN 
			(o.order_creation__c::date = (current_date-1)::date 
			AND o.acquisition_new_customer__c = '1'
			and customer_creation_date::date = order_creation__c::Date
			AND o.order_type = '1'
			AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')) 
		THEN o.sfid ELSE NULL END)

		as total_acquisitions_b2c,



		CASE WHEN ((COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-8)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END))::numeric) > 0 

		THEN

			(COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)
		-
			COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-8)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END))
		/
			(COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-8)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END))::numeric

		ELSE NULL END

		as pcent_var_3,


		CASE WHEN (COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)::numeric) > 0


		THEN

			(COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.recurrency__c = '0'
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.order_creation__c::date = (current_date-1)::date 
				AND o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)::numeric)

		ELSE NULL END

		as percent_trial_share_b2c,


		CASE WHEN ((COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)) > 0 

			AND (COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-1)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric) > 0)

		THEN (

			CASE WHEN (			(COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric)
				) > 0 



			THEN



				((COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-1)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-1)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric)
			-
				(COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric))
			/
				(COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.recurrency__c = '0'
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)
				/
				COUNT(DISTINCT CASE WHEN 
					(o.order_creation__c::date = (current_date-8)::date 
					AND o.acquisition_new_customer__c = '1'
					and customer_creation_date::date = order_creation__c::Date
					AND o.order_type = '1') 
				THEN o.sfid ELSE NULL END)::numeric)

			ELSE NULL END
		)

		ELSE NULL END

		as pcent_var_4


	FROM bi.orders o

	WHERE o.test__c = '0'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')

;

DROP TABLE IF EXISTS bi.daily_reporting_temp;
CREATE TABLE bi.daily_reporting_temp AS

SELECT * FROM

	(SELECT * FROM bi.daily_reporting_all

	UNION

	SELECT * FROM bi.daily_reporting) as t1

ORDER BY id asc, locale asc

;

DROP TABLE IF EXISTS bi.daily_reporting_cpa;
CREATE TABLE bi.daily_reporting_cpa AS

	SELECT

		1::numeric as id,
		locale::text as locale,
		(current_date - interval '1 day')::date as date,
		CASE WHEN SUM(CASE WHEN date = (current_date - interval '1 day') THEN all_acq ELSE 0 END)::numeric > 0 THEN

			SUM(CASE WHEN date = (current_date - interval '1 day') THEN
					display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount
					ELSE 0 END)::numeric
				/
			SUM(CASE WHEN date = (current_date - interval '1 day') THEN all_acq ELSE 0 END)::numeric 
		ELSE NULL END
		as cpa,

		CASE WHEN SUM(CASE WHEN date = (current_date - interval '8 days') THEN all_acq ELSE 0 END)::numeric > 0 THEN

			SUM(CASE WHEN date = (current_date - interval '8 days') THEN
					display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount
					ELSE 0 END)::numeric
				/
			SUM(CASE WHEN date = (current_date - interval '8 days') THEN all_acq ELSE 0 END)::numeric 
		ELSE NULL END
		as cpa_prevweek

	FROM bi.cpacalcpolygon

	GROUP BY locale
	ORDER BY locale asc 
;

DROP TABLE IF EXISTS bi.daily_reporting_cpa_all;
CREATE TABLE bi.daily_reporting_cpa_all AS

	SELECT

		2::numeric as id,
		'All'::text as locale,
		(current_date - interval '1 day')::date as date,
		CASE WHEN SUM(CASE WHEN date = (current_date - interval '1 day') THEN all_acq ELSE 0 END)::numeric > 0 THEN

			SUM(CASE WHEN date = (current_date - interval '1 day') THEN
					display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount
					ELSE 0 END)::numeric
				/
			SUM(CASE WHEN date = (current_date - interval '1 day') THEN all_acq ELSE 0 END)::numeric 
		ELSE NULL END
		as cpa,

		CASE WHEN SUM(CASE WHEN date = (current_date - interval '8 days') THEN all_acq ELSE 0 END)::numeric > 0 THEN

			SUM(CASE WHEN date = (current_date - interval '8 days') THEN
					display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount
					ELSE 0 END)::numeric
				/
			SUM(CASE WHEN date = (current_date - interval '8 days') THEN all_acq ELSE 0 END)::numeric 
		ELSE NULL END
		as cpa_prevweek

	FROM bi.cpacalcpolygon
;

DROP TABLE IF EXISTS bi.daily_reporting_cpa_temp;
CREATE TABLE bi.daily_reporting_cpa_temp AS

SELECT * FROM

	(SELECT * FROM bi.daily_reporting_cpa_all

	UNION

	SELECT * FROM bi.daily_reporting_cpa) as t1

ORDER BY id asc, locale asc

;

DROP TABLE IF EXISTS bi.daily_reporting_cpa_temp2;
CREATE TABLE bi.daily_reporting_cpa_temp2 AS 
	SELECT 

		id,
		locale,
		date,
		cpa,
		CASE WHEN cpa_prevweek::numeric > 0 THEN 
			(cpa::numeric-cpa_prevweek)/cpa_prevweek::numeric
		ELSE NULL END as pcent_var_5

	FROM bi.daily_reporting_cpa_temp

	ORDER BY id asc, locale asc
;

DROP TABLE IF EXISTS bi.daily_reporting_auto;
CREATE TABLE bi.daily_reporting_auto AS

	SELECT 
		t1.*,
		t2.cpa as cpa,
		t2.pcent_var_5 as pcent_var_5

	FROM bi.daily_reporting_temp t1
		LEFT JOIN bi.daily_reporting_cpa_temp2 t2 ON t1.id = t2.id AND t1.locale = t2.locale AND t1.date = t2.date

;

DROP TABLE IF EXISTS bi.daily_reporting_cpa_temp2;
DROP TABLE IF EXISTS bi.daily_reporting_cpa_temp;
DROP TABLE IF EXISTS bi.daily_reporting_cpa_all;
DROP TABLE IF EXISTS bi.daily_reporting_cpa;
DROP TABLE IF EXISTS bi.daily_reporting;
DROP TABLE IF EXISTS bi.daily_reporting_temp;
DROP TABLE IF EXISTS bi.daily_reporting_all;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_weekly_rep_1;
CREATE TABLE bi.temp_weekly_rep_1 AS

	SELECT
		LEFT(o.locale__c,2) as locale,

		EXTRACT(YEAR from o.effectivedate) as yearnum,

		EXTRACT(WEEK from o.effectivedate) as weeknum,

		SUM(CASE WHEN 
			(o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			) 
		THEN o.gmv_eur_net ELSE 0 END)::numeric

		as net_executed_revenue,


		CASE WHEN (COUNT(DISTINCT o.sfid)::numeric) > 0 

		THEN

			(1-
			(COUNT(DISTINCT CASE WHEN 
				(o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')) 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT o.sfid)::numeric))::numeric 

		ELSE NULL END

		as percent_cancellations


	FROM bi.orders o

	WHERE o.test__c = '0'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
		AND EXTRACT(WEEK from o.effectivedate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(WEEK from o.effectivedate) >= (EXTRACT(WEEK from current_date) - 2)
		AND EXTRACT(YEAR from o.effectivedate) = EXTRACT(YEAR from current_date)

	GROUP BY EXTRACT(WEEK from o.effectivedate), EXTRACT(YEAR from o.effectivedate), LEFT(o.locale__c,2)

	ORDER BY locale asc, weeknum desc


;

DROP TABLE IF EXISTS bi.temp_weekly_rep_2;
CREATE TABLE bi.temp_weekly_rep_2 AS

	SELECT
		LEFT(o.locale__c,2) as locale,

		EXTRACT(YEAR from o.order_creation__c) as yearnum,

		EXTRACT(WEEK from o.order_creation__c) as weeknum,



		COUNT(DISTINCT CASE WHEN 
			(o.acquisition_new_customer__c = '1'
			and customer_creation_date::date = order_creation__c::Date
			AND o.order_type = '1') 
		THEN o.sfid ELSE NULL END)

		as total_acquisitions_b2c,



		CASE WHEN (COUNT(DISTINCT CASE WHEN 
				(o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)::numeric) > 0


		THEN

			(COUNT(DISTINCT CASE WHEN 
				(o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.recurrency__c = '0'
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)
			/
			COUNT(DISTINCT CASE WHEN 
				(o.acquisition_new_customer__c = '1'
				and customer_creation_date::date = order_creation__c::Date
				AND o.order_type = '1') 
			THEN o.sfid ELSE NULL END)::numeric)

		ELSE NULL END

		as percent_trial_share_b2c

	FROM bi.orders o

	WHERE o.test__c = '0'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
		AND EXTRACT(WEEK from o.order_creation__c) < EXTRACT(WEEK from current_date)
		AND EXTRACT(WEEK from o.order_creation__c) >= (EXTRACT(WEEK from current_date) - 2)
		AND EXTRACT(YEAR from o.order_creation__c) = EXTRACT(YEAR from current_date)

	GROUP BY EXTRACT(WEEK from o.order_creation__c), EXTRACT(YEAR from o.order_creation__c), LEFT(o.locale__c,2)

	ORDER BY locale asc, weeknum desc


;


DROP TABLE IF EXISTS bi.temp_weekly_rep_3;
CREATE TABLE bi.temp_weekly_rep_3 AS

	SELECT
		LEFT(m.delivery_area, 2) as locale,
		EXTRACT(WEEK FROM m.mindate) as weeknum,
		EXTRACT(YEAR FROM m.mindate) as yearnum,
		SUM(m.revenue - m.salary_payed)/SUM(m.salary_payed) as gpm

	FROM bi.gpm_weekly_cleaner m

	WHERE EXTRACT(WEEK FROM m.mindate) < EXTRACT(WEEK FROM current_date)
		AND EXTRACT(WEEK FROM m.mindate) >= (EXTRACT(WEEK FROM current_date) - 2)
		AND EXTRACT(YEAR FROM m.mindate) = EXTRACT(YEAR FROM current_date)

	GROUP BY LEFT(m.delivery_area, 2), EXTRACT(WEEK FROM m.mindate), EXTRACT(YEAR FROM m.mindate)

	ORDER BY locale asc, weeknum desc

;


DROP TABLE IF EXISTS bi.temp_weekly_rep_4;
CREATE TABLE bi.temp_weekly_rep_4 AS

	SELECT
		LEFT(o.locale__c,2) as locale,
		EXTRACT(WEEK FROM o.effectivedate) as weeknum,
		EXTRACT(YEAR FROM o.effectivedate) as yearnum,
		SUM(o.gmv_eur_net  - 25.03*o.order_duration__c)/SUM(o.gmv_eur_net) as gpm

	FROM bi.orders o

	WHERE o.test__c = '0'
		AND o.status in ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
		AND EXTRACT(WEEK FROM o.effectivedate) < EXTRACT(WEEK FROM current_date)
		AND EXTRACT(WEEK FROM o.effectivedate) >= (EXTRACT(WEEK FROM current_date) - 2)
		AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
		AND LEFT(o.locale__c,2) = 'ch'

	GROUP BY LEFT(o.locale__c,2), EXTRACT(WEEK FROM o.effectivedate),EXTRACT(YEAR FROM o.effectivedate)

	ORDER BY locale asc, weeknum desc

;


DROP TABLE IF EXISTS bi.temp_weekly_rep_5;
CREATE TABLE bi.temp_weekly_rep_5 AS

	SELECT
		locale as locale,
		EXTRACT(WEEK FROM date) as weeknum,
		EXTRACT(YEAR FROM date) as yearnum,

		CASE WHEN SUM(all_acq)::numeric > 0 THEN

			SUM(display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount)::numeric
				/
			SUM(all_acq)::numeric

		ELSE NULL END
		as cpa

	FROM bi.cpacalcpolygon

	GROUP BY locale, weeknum, yearnum

	HAVING MIN(date)::date > (current_date - interval '3 weeks')::date

	ORDER BY locale asc, yearnum desc, weeknum asc

;


DROP TABLE IF EXISTS bi.weekly_reporting;
CREATE TABLE bi.weekly_reporting AS

	SELECT 
		t1.*,
		t2.total_acquisitions_b2c,
		t2.percent_trial_share_b2c,
		CASE WHEN t1.locale = 'ch' THEN t4.gpm
			 WHEN t1.locale = 'at' THEN 0.2
		ELSE t3.gpm END,
		t5.cpa
	FROM bi.temp_weekly_rep_1 t1
	JOIN bi.temp_weekly_rep_2 t2
		ON t1.locale = t2.locale
			AND t1.weeknum = t2.weeknum
			AND t1.yearnum = t2.yearnum
	LEFT JOIN bi.temp_weekly_rep_3 t3
		ON t1.locale = t3.locale
			AND t1.weeknum = t3.weeknum
			AND t1.yearnum = t3.yearnum
	LEFT JOIN bi.temp_weekly_rep_4 t4
		ON t1.locale = t4.locale
			AND t1.weeknum = t4.weeknum
			AND t1.yearnum = t4.yearnum
	LEFT JOIN bi.temp_weekly_rep_5 t5
		ON t1.locale = t5.locale
			AND t1.weeknum = t5.weeknum
			AND t1.yearnum = t5.yearnum
	ORDER BY t1.locale asc, t1.weeknum desc
;




DROP TABLE IF EXISTS bi.temp_weekly_rep_1;
DROP TABLE IF EXISTS bi.temp_weekly_rep_2;
DROP TABLE IF EXISTS bi.temp_weekly_rep_3;
DROP TABLE IF EXISTS bi.temp_weekly_rep_4;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--New weekly report, older part of the function should be deleted.

DROP TABLE IF EXISTS bi.weekly_reporting_temp;
CREATE TABLE bi.weekly_reporting_temp AS

	SELECT

		t2.locale,
		SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.gmv_net_lw ELSE 0 END) as gmv_eur_net,
		
		(
		SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.gmv_net_lw ELSE 0 END)
		-
		AVG(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.gmv_net_lw ELSE NULL END)
		)
		/
		AVG(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.gmv_net_lw ELSE NULL END)::numeric
		as pcent_var1,



		SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE 0 END) as b2c_acquisitions,
		
		(
		SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE 0 END)
		-
		AVG(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE NULL END)
		)
		/
		AVG(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE NULL END)::numeric
		as pcent_var2,



		SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_trial_acquisitions ELSE 0 END)/SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE 0 END)::numeric as trial_acq_pcent,

		(
		(SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_trial_acquisitions ELSE 0 END)/SUM(CASE WHEN t2.week = (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE 0 END))
		-
		(SUM(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_trial_acquisitions ELSE 0 END)/SUM(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE NULL END)::numeric)
		)
		/	
		(SUM(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_trial_acquisitions ELSE 0 END)/SUM(CASE WHEN t2.week != (EXTRACT(WEEK FROM current_date) - 1) THEN t2.total_acquisitions ELSE NULL END)::numeric)

		as pcent_var3

	FROM

			(SELECT
				LEFT(t1.locale__c, 2) as locale,
				EXTRACT(YEAR FROM t1.effectivedate) as year,
				EXTRACT(WEEK FROM t1.effectivedate) as week,
				MIN(effectivedate) as mindate,
				SUM(t1.gmv_eur_net) as gmv_net_lw,
				SUM(CASE WHEN t1.acquisition_new_customer__c = '1' AND t1.order_type = '1' THEN 1 ELSE 0 END) as total_acquisitions,	
				SUM(CASE WHEN t1.acquisition_new_customer__c = '1' AND t1.order_type = '1' AND t1.recurrency__c = '0' THEN 1 ELSE 0 END) as total_trial_acquisitions

			FROM bi.orders t1

			WHERE t1.test__c = '0'
				AND t1.status IN ('INVOICED','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
				AND t1.order_type = '1'
				AND t1.effectivedate >= (current_date-interval '37 days')
				AND EXTRACT(WEEK FROM t1.effectivedate) != EXTRACT(WEEK FROM current_date)
				AND (EXTRACT(YEAR FROM t1.effectivedate)::text || '-' || EXTRACT(WEEK FROM t1.effectivedate)::text) != (EXTRACT(YEAR FROM current_date)::text || '-52')

			GROUP BY LEFT(t1.locale__c, 2), EXTRACT(YEAR FROM t1.effectivedate), EXTRACT(WEEK FROM t1.effectivedate)

			HAVING
			(MIN(effectivedate) < current_date)
			AND
			(MIN(effectivedate) >= '2016-12-12')

	ORDER BY locale asc, mindate desc) as t2

	GROUP BY t2.locale

	ORDER BY t2.locale asc

;

DROP TABLE IF EXISTS bi.weekly_reporting_cpatemp;
CREATE TABLE bi.weekly_reporting_cpatemp AS

	SELECT
		t3.locale,
		t3.cpa as cpa,
		(t3.cpa - t3.cpa_l4w)/t3.cpa_l4w::numeric as pcent_var4

	FROM

		(SELECT
			locale as locale,

			SUM(CASE WHEN EXTRACT(WEEK FROM date) = (EXTRACT(WEEK FROM current_date)-1) THEN
			display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount
			ELSE 0 END)::numeric
				/
			SUM(CASE WHEN EXTRACT(WEEK FROM date) = (EXTRACT(WEEK FROM current_date)-1) THEN
			all_acq
			ELSE 0 END)::numeric as cpa,

			SUM(CASE WHEN EXTRACT(WEEK FROM date) >= (EXTRACT(WEEK FROM current_date)-5) AND EXTRACT(WEEK FROM date) != (EXTRACT(WEEK FROM current_date)-1) THEN
				display_cost+display_discount+facebook_cost+facebook_discount+facebook_organic_discount+newsletter_discount+offline_cost+offline_discount+sem_brand_cost+sem_brand_discount+sem_cost+sem_discount+seo_discount+seo_brand_discount+vouchers_cost+youtube_cost+youtube_discount
				ELSE 0 END)::numeric
					/
			SUM(CASE WHEN EXTRACT(WEEK FROM date) >= (EXTRACT(WEEK FROM current_date)-5) AND EXTRACT(WEEK FROM date) != (EXTRACT(WEEK FROM current_date)-1) THEN
			all_acq
			ELSE 0 END)::numeric as cpa_l4w

		FROM bi.cpacalcpolygon

		GROUP BY locale

		ORDER BY locale asc) as t3

;

DROP TABLE IF EXISTS bi.weekly_reporting_gpmtemp;
CREATE TABLE bi.weekly_reporting_gpmtemp AS

	SELECT

		locale,
		(gp/revenue) as gpm,


		(
			(gp/revenue)
		-
			(gp_l4w/revenue_l4w)
		)
		/
		(abs(gp_l4w/revenue_l4w)) as pcent_var5

	FROM
	(
	SELECT
		LEFT(delivery_area,2) as locale,
		SUM(CASE WHEN EXTRACT(WEEK FROM mindate) = (EXTRACT(WEEK FROM current_date)-1) THEN t1.gp ELSE 0 END) as gp,
		SUM(CASE WHEN EXTRACT(WEEK FROM mindate) = (EXTRACT(WEEK FROM current_date)-1) THEN t1.revenue ELSE 0 END)::numeric as revenue,
		SUM(CASE WHEN EXTRACT(WEEK FROM mindate) != (EXTRACT(WEEK FROM current_date)-1) THEN t1.gp ELSE 0 END) as gp_l4w,
		SUM(CASE WHEN EXTRACT(WEEK FROM mindate) != (EXTRACT(WEEK FROM current_date)-1) THEN t1.revenue ELSE 0 END)::numeric as revenue_l4w

	FROM bi.gpm_weekly_cleaner t1

	WHERE mindate > '2016-12-12'
				AND mindate < current_date
	GROUP BY LEFT(delivery_area,2)

	ORDER BY locale asc
	) as t1

	UNION

		(SELECT
			'at' as locale,
			0.2::numeric as gpm,
			0.00::numeric as pcent_var5)

	UNION

		(SELECT
			LEFT(o.locale__c,2) as locale,

			SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net  - 25.03*o.order_duration__c ELSE 0 END)
			/
			SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net ELSE 0 END) as gpm,

			(
			(SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net  - 25.03*o.order_duration__c ELSE 0 END)
			/
			SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net ELSE 0 END))
			 -
			(SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) != (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net  - 25.03*o.order_duration__c ELSE 0 END)
			/
			SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) != (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net ELSE 0 END))
			)
			/
			(SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) != (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net  - 25.03*o.order_duration__c ELSE 0 END)
			/
			SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) != (EXTRACT(WEEK FROM current_date)-1) THEN o.gmv_eur_net ELSE 0 END)) as pcent_var6

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND o.status in ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
			AND LEFT(o.locale__c,2) = 'ch'
			AND o.effectivedate < current_date
			AND o.effectivedate >= '2016-12-12'

		GROUP BY LEFT(o.locale__c,2)

		ORDER BY locale asc)

	;

DROP TABLE IF EXISTS bi.weekly_reporting_new;
CREATE TABLE bi.weekly_reporting_new AS

	SELECT
		t1.*,
		t3.gpm,
		t3.pcent_var5,
		t2.cpa,
		t2.pcent_var4
	FROM bi.weekly_reporting_temp t1
	JOIN bi.weekly_reporting_cpatemp t2 ON t1.locale = t2.locale
	JOIN bi.weekly_reporting_gpmtemp t3 ON t1.locale = t3.locale

;

DROP TABLE IF EXISTS bi.weekly_reporting_temp;
DROP TABLE IF EXISTS bi.weekly_reporting_cpatemp;
DROP TABLE IF EXISTS bi.weekly_reporting_gpmtemp;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$
LANGUAGE plpgsql VOLATILE