CREATE OR REPLACE FUNCTION bi.daily$15_order_areas(crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$15_order_areas';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


truncate bi.order_areas;

INSERT INTO bi.order_areas
select
	DISTINCT ON(Order_Id__c) Order_Id__c,
	ST_Distance(Shipping_Point, Center_Point) as distance,
	CASE WHEN ST_Distance(Shipping_Point, Center_Point) < effective_radius__c*1000 THEN key__c ELSE 'No Match' END as efficiency_area,
	CASE WHEN ST_Distance(Shipping_Point, Center_Point) < radius__c*1000 THEN key__c ELSE 'No Match' END as delivery_area,
	effective_radius__c,
	radius__c
from
	(select
		Order_Id__c,
		Effectivedate,
		Locale__c,
		shippinglongitude,
		shippinglatitude,
		ST_GeogFromText('SRID=4326;POINT(' || shippinglongitude || ' ' || shippinglatitude || ')') as Shipping_Point,
		status
	from Salesforce.Order
	where Effectivedate::date >= '2016-01-01'
	) t1,
	(select 
		x.*,
		ST_GeogFromText('SRID=4326;POINT(' ||  center__longitude__s|| ' ' || center__latitude__s || ')') as Center_Point
	from Salesforce.delivery_area__c x
	) t2
	where ST_DWithin(Shipping_Point, Center_Point,radius__c*1000)
ORDER BY Order_Id__c, ST_Distance(Shipping_Point, Center_Point)
;

DROP TABLE IF EXISTS bi.orders_in_polygon;
CREATE TABLE bi.orders_in_polygon as 
SELECT  
    Order_Id__c,
    Shippingcity,
    key__c,
ST_Intersects( 
    ST_SetSRID(ST_Point(shippinglongitude, shippinglatitude), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(delivery_area__c.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
FROM
    Salesforce.Order,
    salesforce.delivery_area__c
WHERE
     Order_Creation__c  >= '2016-01-01'
GROUP BY
        Order_Id__c,
    Shippingcity,
    key__c,
    flag_polygon
HAVING
    ST_Intersects( 
    ST_SetSRID(ST_Point(shippinglongitude, shippinglatitude), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(delivery_area__c.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true;

DELETE FROM bi.orders_location;

INSERT INTO bi.orders_location 
SELECT
	t1.order_id__c,
	t2.delivery_area,
	t2.efficiency_area,
	t3.key__c as polygon
FROM
    Salesforce.Order t1
LEFT JOIN
	bi.order_areas t2
ON
	(t1.order_id__c = t2.order_id__c)
LEFT JOIn
	bi.orders_in_polygon t3
ON
	(t1.order_id__c = t3.order_id__c);
	
	
end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$
LANGUAGE 'plpgsql'