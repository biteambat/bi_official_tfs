DELIMITER //

CREATE OR REPLACE FUNCTION bi.daily$onboarding_kpis(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$onboarding_kpis';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
------------------------------------------------------

DROP TABLE IF EXISTS bi.onboarding_kpis;
CREATE TABLE bi.onboarding_kpis AS

	SELECT

		EXTRACT(YEAR FROM to_date(t1.year_month,'YYYY-MM-DD'))::int AS year,
		EXTRACT(MONTH FROM to_date(t1.year_month,'YYYY-MM-DD'))::int AS month,
		t1.delivery_area::text as delivery_area,
		'Cleaners on platform'::text as kpi,
		SUM(CASE WHEN to_char(t1.contract_start,'YYYY-MM') <= t1.year_month and to_char(t1.contract_end,'YYYY-MM') >= t1.year_month THEN 1 ELSE 0 END) AS value

	FROM bi.margin_per_cleaner t1

	GROUP BY year, month, t1.delivery_area, kpi

	ORDER BY year desc, month desc, delivery_area asc, kpi asc

;

INSERT INTO bi.onboarding_kpis

	SELECT

		EXTRACT(YEAR FROM a.hr_contract_start__c) AS year,
		EXTRACT(MONTH FROM a.hr_contract_start__c) AS month,
		a.delivery_areas__c AS delivery_area,
		'Actual onboardings'::text AS kpi,
		COUNT(DISTINCT a.sfid) AS value

	FROM salesforce.account a

	WHERE a.status__c in ('ACTIVE','BETA') AND hr_contract_start__c IS NOT NULL and type__c != 'cleaning-b2b'

	GROUP BY year, month, delivery_area, kpi

	ORDER BY year desc, month desc, delivery_area asc, kpi asc

;


INSERT INTO bi.onboarding_kpis

	SELECT

		year AS year,
		month AS month,
		delivery_area AS delivery_area,
		'Onboarding target'::text AS kpi,
		onboarding_target AS value

	FROM bi.onboarding_targets

;


-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.Potential_Onboardings;
CREATE TABLE bi.Potential_Onboardings AS


	SELECT

		t4.citya as Polygon1,
		t4.cop as Professionals_platform,
		t3.Potential_onboardings,
		t3.Ending_Last_30days,
		t3.Ending_Next_30days,
		t3.Starting_Last_30days,
		t3.Starting_Next_30days,
		t3.Netto_60days
	
	FROM
	
		(SELECT
		
		Polygon1,
		Professionals_platform,
		Potential_onboardings,
		Ending_Last_30days,
		Ending_Next_30days,
		Starting_Last_30days,
		Starting_Next_30days,
		Netto_60days
		
		FROM
		
			(SELECT
		
		        a.delivery_area__c as Polygon1,
		        SUM(CASE WHEN a.status IN ('Interview scheduled', 'Waiting for contract', 'Documents after interview', 'Contract ready', 'Waiting for greenlight') THEN 1 ELSE 0 END) as Potential_onboardings
		
		    FROM salesforce.lead a
		
		    WHERE 
		
		        a.lastmodifieddate > current_date - 30
		        AND a.delivery_area__c NOT IN ('de-potsdam', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		
		    GROUP BY
		    
		        a.delivery_area__c
		    
		    ORDER BY
		
		        a.delivery_area__c asc) as t1
		        
		LEFT JOIN
		
		    (SELECT
		    
		        b.delivery_areas__c as Polygon2,
		
		        SUM(CASE WHEN hr_contract_end__c::date >= current_date::date - 30 AND hr_contract_end__c::date <= current_date::date THEN 1 ELSE 0 END) as Ending_Last_30days,
		        SUM(CASE WHEN hr_contract_end__c::date <= current_date::date + 30 AND hr_contract_end__c::date > current_date::date THEN 1 ELSE 0 END) as Ending_Next_30days,
		        SUM(CASE WHEN hr_contract_start__c::date >= current_date::date - 30 AND hr_contract_start__c::date <= current_date::date THEN 1 ELSE 0 END) as Starting_Last_30days,  
		        SUM(CASE WHEN hr_contract_start__c::date <= current_date::date + 30 AND hr_contract_start__c::date > current_date::date THEN 1 ELSE 0 END) as Starting_Next_30days,  
		        (SUM(CASE WHEN hr_contract_start__c::date >= current_date::date - 30 AND hr_contract_start__c::date <= current_date::date THEN 1 ELSE 0 END) + SUM(CASE WHEN hr_contract_start__c::date <= current_date::date + 30 AND hr_contract_start__c::date > current_date::date THEN 1 ELSE 0 END) - SUM(CASE WHEN hr_contract_end__c::date >= current_date::date - 30 AND hr_contract_end__c::date <= current_date::date THEN 1 ELSE 0 END) - SUM(CASE WHEN hr_contract_end__c::date <= current_date::date + 30 AND hr_contract_end__c::date > current_date::date THEN 1 ELSE 0 END)) as Netto_60days,
		        COUNT(DISTINCT CASE WHEN (b.status__c IN ('BETA', 'ACTIVE')) AND (hr_contract_start__c <= current_date::date OR delivery_areas__c IN ('at-vienna')) THEN b.sfid ELSE NULL END)  as Professionals_platform
		
		    FROM bi.account b
		
		    GROUP BY
		
		      b.delivery_areas__c
		    
		    ORDER BY
		    
		      b.delivery_areas__c asc) as t2
		        
		ON
		
		    (t1.Polygon1 = t2.Polygon2)) as t3
		    
	LEFT JOIN
	
		(SELECT
		
			EXTRACT(MONTH FROM max_date::date) as Month,
			EXTRACT(YEAR FROM max_date::date) as Year,
			MIN(max_date::date) as mindate,
			delivery_areas__c as citya,
			CAST('COP Cleaner' as text) as kpi,
			CAST('-' as text) as breakdown,
			COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date < hr_contract_end__c THEN sfid ELSE null END)) as cop
			
		FROM
		
		(SELECT
			TO_CHAR(date,'YYYY-MM') as Month,
			MAX(Date) as max_date
		FROM
			(select i::date as date from generate_series('2016-01-01', 
		  current_date::date, '1 day'::interval) i) as dateta
		  GROUP BY
		 	Month) as a,
		 	(SELECT
		 		a.*
		 	FROM
		   Salesforce.Account a
		    JOIN
		        Salesforce.Account t2
		    ON
		        (t2.sfid = a.parentid)
		
		    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
			and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
			
		 GROUP BY
		 	Year,
		 	Month,
		 	max_date::date,
		 	delivery_areas__c) as t4
		 	
	ON
	
		(t3.Polygon1 = t4.citya) 
		
	WHERE
	
		
		t3.Polygon1 = t4.citya
		AND t4.mindate = current_date;
	


-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$
LANGUAGE plpgsql VOLATILE