CREATE OR REPLACE FUNCTION bi.daily$piwik_visits(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$piwik_visits';
start_time timestamp := clock_timestamp() + interval '2 hours';

end_time timestamp;
duration interval;

BEGIN

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_visits_polygon_matching;
CREATE TABLE bi.temp_visits_polygon_matching AS

	SELECT  
	    t1.visit_id,
	    t1.city,
	    t2.key__c,
	ST_Intersects( 
	    ST_SetSRID(ST_Point(t1.longitude::float, t1.latitude::float), 4326), 
	    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
	FROM
	    external_data.piwik_visits t1,
	    salesforce.delivery_area__c t2
	WHERE
	     t1.server_date  >= '2016-11-25'::date
	GROUP BY
	    t1.visit_id,
	    t1.city,
	    t2.key__c,
	    flag_polygon
	HAVING
	    ST_Intersects(
	    ST_SetSRID(ST_Point(t1.longitude::float, t1.latitude::float), 4326), 
	    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true
;

DROP TABLE IF EXISTS bi.temp_etl_piwik_visits;
CREATE TABLE bi.temp_etl_piwik_visits AS

	SELECT DISTINCT
	t1.visit_id,
	t1.visitor_id,
	t2.transaction_id,
	t1.locale,
	t1.region,
	t1.city,
	CASE WHEN t3.key__c is NOT NULL THEN t3.key__c::text ELSE t1.locale::text || '-other'::text END as polygon,
	t1.server_date::date,
	t1.firstaction_unixtime,
	t1.visit_duration_seconds,
	t1.visitor_type,
	t1.referrer_url,
	t1.referrer_search_url,
	t1.utm_source,
	t1.utm_medium,
	t1.utm_campaign,
	t1.utm_content,
	t1.utm_term,
	t1.clid,
	t1.voucher_code,
	t2.transaction_total,
	t2.discount,
	CASE WHEN (t1.utm_source::text || t1.utm_medium::text || t1.utm_campaign::text || t1.utm_content::text || t1.utm_term::text || t1.clid::text || t1.voucher_code::text) = '' OR
			  (t1.utm_source::text || t1.utm_medium::text || t1.utm_campaign::text || t1.utm_content::text || t1.utm_term::text || t1.clid::text || t1.voucher_code::text) IS NULL THEN

		(CASE
	    WHEN t1.referrer_url like '%indeed%' or t1.referrer_url like '%apply.bookatiger.com%' or t1.referrer_url like '%careerjet%' or t1.referrer_url like '%aktuelle-jobs%' or t1.referrer_url like '%adzuna%' THEN 'Job Traffic'
	    WHEN t1.referrer_url like '%clid=goog%' THEN 'SEM'
	    WHEN t1.referrer_url like '%clid=goob%' THEN 'SEM Brand'
	    WHEN (t1.referrer_url like '%google%' OR t1.referrer_search_url like '%google%')
			OR (t1.referrer_url like '%yahoo%' OR t1.referrer_search_url like '%yahoo%')
			OR (t1.referrer_url like '%bing%' OR t1.referrer_search_url like '%bing%')
			OR (t1.referrer_url like '%msn%' OR t1.referrer_search_url like '%msn%')
		AND t1.referrer_url not like '%clid=goob%' and t1.referrer_url not like '%clid=goog%' and t1.referrer_url not like '%utm_medium=cpc%' THEN 'SEO'
	    WHEN t1.referrer_url like '%clid=dsp%' OR t1.referrer_url like '%gdtiger20%' or referrer_url like '%GDTIGER20%' THEN 'Display'
	    WHEN t1.referrer_url like '%fbtiger20%' or referrer_url like '%FBTIGER20%' or referrer_url like '%FBTIGER50%' or referrer_url like '%batfb%' THEN 'Facebook'
	    WHEN t1.referrer_url like '%facebook%' or referrer_url like '%instagram%' THEN 'Facebook Organic'
	    WHEN t1.referrer_url like '%freenet%' or referrer_url like '%outlook%' or referrer_url like '%deref-web.de%' or referrer_url like '%gmx%' or referrer_url like '%t-online%' or referrer_url like '%bluewin%' THEN 'Newsletter'
	    WHEN t1.referrer_url not like '%bookatiger.%' AND t1.referrer_url IS NOT NULL THEN 'Referral'
	    WHEN t1.referrer_url like '%bookatiger.%' OR t1.referrer_url IS NULL THEN 'DTI'
	    ELSE 'Unattributed' END)

	ELSE
		(CASE 
			WHEN t1.clid = 'goog' OR t1.utm_campaign like ('B2C\\___-__\\_SM%') THEN 'SEM'
			WHEN t1.clid = 'goob' OR t1.utm_campaign like ('B2C\\___-__\\_SB%') THEN 'SEM Brand'
			WHEN (t1.utm_source = 'facebook' AND utm_medium = 'cpc') OR t1.utm_campaign like ('B2C\\___-__\\_FB%') THEN 'Facebook'
			WHEN t1.clid = 'dsp' 
				OR t1.utm_campaign like ('B2C\\___-__\\_DS%')
				OR t1.clid = 'dsp'
				OR (t1.utm_source = 'linkedin' AND t1.utm_medium = 'cpc')
				OR (t1.utm_source = 'SRBL4SWyWM' AND t1.utm_medium = 'cpc')
				OR (t1.utm_source = 'xing' AND t1.utm_medium = 'cpc')
			THEN 'Display'
			WHEN t1.clid in ('afnt', 'afcp') OR t1.utm_campaign like ('B2C\\___-__\\_AF%') THEN 'Affiliates/Coops'
			WHEN t1.clid = 'vouc' OR t1.utm_campaign like ('B2C\\___-__\\_VD%') THEN 'Voucher Campaigns'
			WHEN t1.clid like ('jobp%') OR t1.utm_campaign like ('TIG\\___-__\\JP%') THEN 'Job Traffic'
			WHEN (t1.utm_source IS NULL OR t1.utm_source = '')
				AND (t1.utm_medium IS NULL OR t1.utm_medium = '')
				AND (t1.utm_campaign IS NULL OR t1.utm_campaign = '')
				AND (t1.utm_content IS NULL OR t1.utm_content = '')
				AND (t1.clid IS NULL OR t1.clid = '')
			THEN (CASE WHEN t1.referrer_search_url IS NULL OR t1.referrer_search_url = '' THEN 'DTI' ELSE 'SEO' END)
			WHEN t1.clid = 'email' OR t1.utm_campaign like ('B2C\\______\\_EM%') OR (t1.utm_source = 'mcloud' AND t1.utm_medium = 'email') THEN 'Newsletter'
			ELSE 'Unattributed'
		END)
	END
	as marketing_channel

	FROM external_data.piwik_visits t1
	LEFT JOIN external_data.piwik_actions t2
		ON t1.visit_id = t2.visit_id
		AND t2.transaction_id IS NOT NULL
		AND t2.transaction_id != ''
	LEFT JOIN bi.temp_visits_polygon_matching t3
		ON t1.visit_id = t3.visit_id

	WHERE t1.server_date::date >= '2016-11-25'::date
		AND t1.visit_ip NOT IN ('62.72.67.58', '81.62.149.109', '212.78.169.180')

	ORDER BY t1.server_date::date desc, t1.visit_id asc
;

--------------------------------------------------------------------------------------------------------- A) ATTRIBUTION MODELLING

DELETE FROM bi.trig_piwik_datacleanup;
INSERT INTO bi.trig_piwik_datacleanup

	SELECT * 

	FROM bi.temp_etl_piwik_visits
;

DELETE FROM bi.trig_transaction_attribution;
INSERT INTO bi.trig_transaction_attribution

	SELECT * FROM bi.trig_visits_data_cleanup()

;

DROP TABLE IF EXISTS bi.etl_piwik_visits;
CREATE TABLE bi.etl_piwik_visits AS

	SELECT * FROM bi.trig_visits_transaction_matching()

;


--------------------------------------------------------------------------------------------------------- B) FUNNEL CONVERSION

DROP TABLE IF EXISTS bi.piwik_funnel_conversion;
CREATE TABLE bi.piwik_funnel_conversion AS

    SELECT
        t1.server_date as date,
        LEFT(t1.polygon, 2) as locale,
        t1.polygon,
        t1.visitor_type,
        t1.marketing_channel,
        COUNT(DISTINCT t1.visit_id) as total_visits,
        COUNT(DISTINCT CASE WHEN t1.visits_step_1 > 0 THEN t1.visit_id ELSE NULL END) as total_visits_s1,
        COUNT(DISTINCT CASE WHEN t1.visits_step_2 > 0 THEN t1.visit_id ELSE NULL END) as total_visits_s2,
        COUNT(DISTINCT CASE WHEN t1.visits_step_3 > 0 THEN t1.visit_id ELSE NULL END) as total_visits_s3,
		COUNT(DISTINCT t1.visitor_id) as total_visitors,
        COUNT(DISTINCT CASE WHEN t1.visitors_step_1 > 0 THEN t1.visit_id ELSE NULL END) as total_visitors_s1,
        COUNT(DISTINCT CASE WHEN t1.visitors_step_2 > 0 THEN t1.visit_id ELSE NULL END) as total_visitors_s2,
        COUNT(DISTINCT CASE WHEN t1.visitors_step_3 > 0 THEN t1.visit_id ELSE NULL END) as total_visitors_s3,
        COUNT(DISTINCT CASE WHEN t1.transactions > 0 THEN t1.visit_id ELSE NULL END) as total_transactions

    FROM

	        (SELECT

	            t1.visit_id,
				t2.visitor_id,
	            t1.server_date,
	            t2.polygon,
				t2.visitor_type,
				t2.marketing_channel,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'Funnel / Step1' THEN t1.visit_id ELSE NULL END) as visits_step_1,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'Funnel / Step2' THEN t1.visit_id ELSE NULL END) as visits_step_2,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'Funnel / Step3' THEN t1.visit_id ELSE NULL END) as visits_step_3,

	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'Funnel / Step1' THEN t2.visitor_id ELSE NULL END) as visitors_step_1,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'Funnel / Step2' THEN t2.visitor_id ELSE NULL END) as visitors_step_2,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'Funnel / Step3' THEN t2.visitor_id ELSE NULL END) as visitors_step_3,

	            COUNT(DISTINCT CASE WHEN 
	                    t1.transaction_id IS NOT NULL 
	                    AND t1.transaction_id <> ''
	                    AND t1.transaction_total IS NOT NULL 
	                    AND t1.transaction_total > 0
	                THEN t1.transaction_id ELSE NULL END) as transactions

	        FROM external_data.piwik_actions t1

					JOIN bi.etl_piwik_visits t2 ON t1.visit_id = t2.visit_id

	        WHERE t1.server_date >= '2016-11-25'

	        GROUP BY t1.visit_id, t1.server_date, t2.polygon, t2.visitor_type,	t2.marketing_channel, t2.visitor_id
	        ORDER BY t1.server_date desc, t1.visit_id asc, t2.polygon asc, t2.visitor_type asc, t2.marketing_channel asc) 

        as t1

    GROUP BY server_date, LEFT(t1.polygon, 2), polygon, visitor_type, marketing_channel

    ORDER BY server_date desc, locale asc, polygon asc, visitor_type asc, marketing_channel asc

;


--------------------------------------------------------------------------------------------------------- Z) DELETING TEMP TABLES

DROP TABLE IF EXISTS bi.temp_visits_polygon_matching;
DROP TABLE IF EXISTS bi.temp_etl_piwik_visits;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

-- Filters: Only users which really had a transaction. <=> Users for which COUNTD(transaction_id) > 0 &&& Channel NOT IN ('Job traffic').

DROP TABLE IF EXISTS bi.temp_attribution_sample;
CREATE TABLE bi.temp_attribution_sample AS
	SELECT

			t2.*
		FROM

			(
			(SELECT 
				visitor_id,
				COUNT(DISTINCT transaction_id) as ntransactions
			FROM 
				bi.etl_piwik_visits
			GROUP BY visitor_id) as t1

			JOIN bi.etl_piwik_visits t2 
			ON t1.ntransactions > 0 
			AND t1.visitor_id = t2.visitor_id 
			AND t2.attributed_transaction IS NOT NULL
			AND t2.transaction_timestamp IS NOT NULL
			AND t2.converting_visit IS NOT NULL

			)

	ORDER BY t2.visitor_id asc, t2.visit_num asc, t2.firstaction_unixtime asc

;

DROP TABLE IF EXISTS bi.customer_journey;
CREATE TABLE bi.customer_journey AS

	SELECT
		COUNT(DISTINCT t3.visitor_id)::numeric as nb_orders,
		t3.chan_s1 as chan_s1,
		t3.chan_s2 as chan_s2,
		t3.chan_s3 as chan_s3,
		t3.chan_s4 as chan_s4,
		t3.chan_s5 as chan_s5,
		t3.chan_s6 as chan_s6,
		t3.chan_s7 as chan_s7,
		t3.chan_s8 as chan_s8,
		t3.chan_s9 as chan_s9,
		t3.chan_s10 as chan_s10,
		t3.chan_conv as chan_conv,
		(CASE WHEN t3.chan_s1 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s2 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s3 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s4 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s5 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s6 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s7 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s8 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s9 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s10 != '' THEN 1 ELSE 0 END) as n_visits,
		(CASE WHEN (t3.chan_s1 || ' ' || t3.chan_s2 || ' ' || t3.chan_s3 || ' ' || t3.chan_s4 || ' ' || t3.chan_s5 || ' ' || t3.chan_s6 || ' ' || t3.chan_s7 || ' ' || t3.chan_s8 || ' ' || t3.chan_s9 || ' ' || t3.chan_s10) LIKE ('%SEM%')
			OR (t3.chan_s1 || ' ' || t3.chan_s2 || ' ' || t3.chan_s3 || ' ' || t3.chan_s4 || ' ' || t3.chan_s5 || ' ' || t3.chan_s6 || ' ' || t3.chan_s7 || ' ' || t3.chan_s8 || ' ' || t3.chan_s9 || ' ' || t3.chan_s10) LIKE ('%Display%')
			OR (t3.chan_s1 || ' ' || t3.chan_s2 || ' ' || t3.chan_s3 || ' ' || t3.chan_s4 || ' ' || t3.chan_s5 || ' ' || t3.chan_s6 || ' ' || t3.chan_s7 || ' ' || t3.chan_s8 || ' ' || t3.chan_s9 || ' ' || t3.chan_s10) LIKE ('%Affiliates%')
			OR (t3.chan_s1 || ' ' || t3.chan_s2 || ' ' || t3.chan_s3 || ' ' || t3.chan_s4 || ' ' || t3.chan_s5 || ' ' || t3.chan_s6 || ' ' || t3.chan_s7 || ' ' || t3.chan_s8 || ' ' || t3.chan_s9 || ' ' || t3.chan_s10) LIKE ('%Facebook%')
		THEN 'Perf Mktg' ELSE 'No Perf Mktg' END)::text as paid_or_not,		
		NULL::numeric as o_sem,
		NULL::numeric as o_semb,
		NULL::numeric as o_seo,
		NULL::numeric as o_dti,
		NULL::numeric as o_aff,
		NULL::numeric as o_dis,
		NULL::numeric as o_fb,
		NULL::numeric as o_unatt
	FROM
		(SELECT
			t1.visitor_id,
			MAX(t1.chan_s1) as chan_s1,
			MAX(t1.chan_s2) as chan_s2,
			MAX(t1.chan_s3) as chan_s3,
			MAX(t1.chan_s4) as chan_s4,
			MAX(t1.chan_s5) as chan_s5,
			MAX(t1.chan_s6) as chan_s6,
			MAX(t1.chan_s7) as chan_s7,
			MAX(t1.chan_s8) as chan_s8,
			MAX(t1.chan_s9) as chan_s9,
			MAX(t1.chan_s10) as chan_s10,
			MAX(t1.chan_conv) as chan_conv

		FROM
		(
			SELECT
				visitor_id,
				visit_num,
				CONCAT(CASE WHEN visit_num = 1 THEN marketing_channel ELSE NULL END) as chan_s1,
				CONCAT(CASE WHEN visit_num = 2 THEN marketing_channel ELSE NULL END) as chan_s2,
				CONCAT(CASE WHEN visit_num = 3 THEN marketing_channel ELSE NULL END) as chan_s3,
				CONCAT(CASE WHEN visit_num = 4 THEN marketing_channel ELSE NULL END) as chan_s4,
				CONCAT(CASE WHEN visit_num = 5 THEN marketing_channel ELSE NULL END) as chan_s5,
				CONCAT(CASE WHEN visit_num = 6 THEN marketing_channel ELSE NULL END) as chan_s6,
				CONCAT(CASE WHEN visit_num = 7 THEN marketing_channel ELSE NULL END) as chan_s7,
				CONCAT(CASE WHEN visit_num = 8 THEN marketing_channel ELSE NULL END) as chan_s8,
				CONCAT(CASE WHEN visit_num = 9 THEN marketing_channel ELSE NULL END) as chan_s9,
				CONCAT(CASE WHEN visit_num = 10 THEN marketing_channel ELSE NULL END) as chan_s10,
				CONCAT(CASE WHEN transaction_id IS NOT NULL THEN marketing_channel ELSE NULL END) as chan_conv

			FROM bi.temp_attribution_sample

			GROUP BY visitor_id, visit_num, marketing_channel, transaction_id

			ORDER BY visitor_id asc, visit_num asc
		) as t1

		GROUP BY t1.visitor_id) 

	as t3

	GROUP BY 
		t3.chan_s1,
		t3.chan_s2,
		t3.chan_s3,
		t3.chan_s4,
		t3.chan_s5,
		t3.chan_s6,
		t3.chan_s7,
		t3.chan_s8,
		t3.chan_s9,
		t3.chan_s10,
		t3.chan_conv
	ORDER BY nb_orders desc
;

DELETE FROM bi.trig_channels_attribution;
INSERT INTO bi.trig_channels_attribution

	SELECT
		t3.order_date,
		COUNT(DISTINCT t3.visitor_id)::numeric as nb_orders,
		t3.chan_s1 as chan_s1,
		t3.chan_s2 as chan_s2,
		t3.chan_s3 as chan_s3,
		t3.chan_s4 as chan_s4,
		t3.chan_s5 as chan_s5,
		t3.chan_s6 as chan_s6,
		t3.chan_s7 as chan_s7,
		t3.chan_s8 as chan_s8,
		t3.chan_s9 as chan_s9,
		t3.chan_s10 as chan_s10,
		t3.chan_conv as chan_conv,
		(CASE WHEN t3.chan_s1 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s2 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s3 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s4 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s5 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s6 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s7 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s8 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s9 != '' THEN 1 ELSE 0 END + CASE WHEN t3.chan_s10 != '' THEN 1 ELSE 0 END) as n_visits,
		NULL::numeric as o_sem,
		NULL::numeric as o_semb,
		NULL::numeric as o_seo,
		NULL::numeric as o_dti,
		NULL::numeric as o_aff,
		NULL::numeric as o_dis,
		NULL::numeric as o_fb,
		NULL::numeric as o_unatt
	FROM
		(SELECT
			MAX(t1.order_date) as order_date,
			t1.visitor_id,
			MAX(t1.chan_s1) as chan_s1,
			MAX(t1.chan_s2) as chan_s2,
			MAX(t1.chan_s3) as chan_s3,
			MAX(t1.chan_s4) as chan_s4,
			MAX(t1.chan_s5) as chan_s5,
			MAX(t1.chan_s6) as chan_s6,
			MAX(t1.chan_s7) as chan_s7,
			MAX(t1.chan_s8) as chan_s8,
			MAX(t1.chan_s9) as chan_s9,
			MAX(t1.chan_s10) as chan_s10,
			MAX(t1.chan_conv) as chan_conv

		FROM
		(
			SELECT
				MAX(CASE WHEN transaction_id IS NOT NULL THEN server_date ELSE NULL END) as order_date,
				visitor_id,
				visit_num,
				CONCAT(CASE WHEN visit_num = 1 THEN marketing_channel ELSE NULL END) as chan_s1,
				CONCAT(CASE WHEN visit_num = 2 THEN marketing_channel ELSE NULL END) as chan_s2,
				CONCAT(CASE WHEN visit_num = 3 THEN marketing_channel ELSE NULL END) as chan_s3,
				CONCAT(CASE WHEN visit_num = 4 THEN marketing_channel ELSE NULL END) as chan_s4,
				CONCAT(CASE WHEN visit_num = 5 THEN marketing_channel ELSE NULL END) as chan_s5,
				CONCAT(CASE WHEN visit_num = 6 THEN marketing_channel ELSE NULL END) as chan_s6,
				CONCAT(CASE WHEN visit_num = 7 THEN marketing_channel ELSE NULL END) as chan_s7,
				CONCAT(CASE WHEN visit_num = 8 THEN marketing_channel ELSE NULL END) as chan_s8,
				CONCAT(CASE WHEN visit_num = 9 THEN marketing_channel ELSE NULL END) as chan_s9,
				CONCAT(CASE WHEN visit_num = 10 THEN marketing_channel ELSE NULL END) as chan_s10,
				CONCAT(CASE WHEN transaction_id IS NOT NULL THEN marketing_channel ELSE NULL END) as chan_conv

			FROM bi.temp_attribution_sample

			GROUP BY visitor_id, visit_num, marketing_channel, transaction_id

			ORDER BY visitor_id asc, visit_num asc
		) as t1

		GROUP BY t1.visitor_id) 

	as t3

	GROUP BY 
		t3.order_date,
		t3.chan_s1,
		t3.chan_s2,
		t3.chan_s3,
		t3.chan_s4,
		t3.chan_s5,
		t3.chan_s6,
		t3.chan_s7,
		t3.chan_s8,
		t3.chan_s9,
		t3.chan_s10,
		t3.chan_conv
	ORDER BY order_date desc, nb_orders desc
;

DROP TABLE IF EXISTS bi.temp_attribution_sample;

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

--Attribution modelling


--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'