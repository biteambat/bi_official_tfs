DELIMITER //

CREATE OR REPLACE FUNCTION bi.mfunc_city_overview(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.mfunc_city_overview';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 55_churns_analysis

  DROP TABLE IF EXISTS bi.temp_orders_terminated;
  CREATE TABLE bi.temp_orders_terminated AS

    SELECT
      created_at::date as date,
      event_name,
      (order_Json->>'Order_Start__c') as orderdate,
      order_Json->>'Locale__c' as Locale__c,
      order_Json->>'Order_Id__c' as Order_id,
      order_Json->>'Contact__c' as customer_id,
      order_Json->>'Recurrency__c' as recurrency

    FROM
      events.sodium


    WHERE
      (event_name in ('Order Event:CANCELLED TERMINATED'))
      and created_at >= '2016-05-01'
      and order_Json->>'Recurrency__c' > '0'
  ;

  DROP TABLE IF EXISTS bi.temp_orders_terminated_2;
  CREATE TABLE bi.temp_orders_terminated_2 AS

    SELECT

      t1.date,
      t1.event_name,
      t1.orderdate,
      t1.locale__c,
      t1.order_id,
      t1.customer_id,
      t1.recurrency,
      t2.polygon,
      t2.error_note__c

    FROM bi.temp_orders_terminated t1

      LEFT JOIN bi.orders t2 
        ON t1.order_id = t2.order_id__c

  ;

  DROP TABLE IF EXISTS bi.temp_orders_NMP;
  CREATE TABLE bi.temp_orders_NMP AS

    SELECT
      created_at::date as date,
      event_name,
      (order_Json->>'Order_Start__c') as orderdate,
      order_Json->>'Locale__c' as Locale__c,
      order_Json->>'Order_Id__c' as Order_id,
      order_Json->>'Contact__c' as customer_id,
      order_Json->>'Recurrency__c' as recurrency

    FROM
      events.sodium

    WHERE
      (event_name in ('Order Event:CANCELLED-NO-MANPOWER'))
      and created_at >= '2016-04-01'
      and order_Json->>'Recurrency__c' > '0'

  ;

  DROP TABLE IF EXISTS bi.temp_badrate_orders;
  CREATE TABLE bi.temp_badrate_orders AS

    SELECT
      order_id__c as order_id,
      effectivedate::date as orderdate,
      order_start__c::date as date,
      customer_id__c as customer_id

    FROM
      bi.orders

    WHERE
      rating_service__c in ('1', '2', '3')
      and effectivedate::date >= '2016-04-01'
      and recurrency__c > '0'
      and test__c = '0'
      and status in ('INVOICED')

  ;


  DROP TABLE IF EXISTS bi.churns_motivations;
  CREATE TABLE bi.churns_motivations AS

    SELECT
      t1.date,
      t1.event_name,
      t1.orderdate,
      LEFT(t1.locale__c,2) as locale,
      t1.polygon,
      t1.order_id,
      t1.error_note__c,
      COUNT(DISTINCT(t2.order_id)) as Count_NMP_L28D,
      COUNT(DISTINCT(t3.order_id)) as Count_Badrate_L28D

    FROM bi.temp_orders_terminated_2 t1
      LEFT JOIN bi.temp_orders_NMP t2
        ON t1.customer_id = t2.customer_id 
        AND (t2.date) >= (t1.date - interval '28 days')
      LEFT JOIN bi.temp_badrate_orders t3
        ON t1.customer_id = t3.customer_id
        AND (t3.orderdate) >= (t1.date - interval '28 days')

    GROUP BY t1.date, t1.event_name, t1.orderdate, LEFT(t1.locale__c,2), t1.polygon, t1.order_id, t1.error_note__c

    ORDER BY t1.date desc

  ;

  DROP TABLE IF EXISTS bi.temp_orders_terminated;
  DROP TABLE IF EXISTS bi.temp_orders_NMP;
  DROP TABLE IF EXISTS bi.temp_badrate_orders;

-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------


-- Breakdown: date, locale, polygon, 

-- CHECK ========> # all acquisitions
-- CHECK ========> # Invoiced acquisitions
-- CHECK ========> Invoiced GMV
-- CHECK ========> Invoiced Hours
-- CHECK ========> # active customers 
-- CHECK ========> # churned customers
-- CHECK ========> # Reactivated customers
-- CHECK ========> Overall costs €
-- CHECK ========> % NMP = #NMP / #Orders(invoiced + noshow + NMP)
	-- #NMP per day
	-- #Orders perday (invoiced noshow nmp)
-- CHECK ========> % Skipped = #skipped orders / #orders(skipped + invoiced + noshow)
	-- #Skipped per day
	-- #Orders per day (invoiced noshow skipped)
-- CHECK ========> % UR
-- CHECK ========> % GPM

DROP TABLE IF EXISTS bi.temp_city_overview_1;
CREATE TABLE bi.temp_city_overview_1 AS

	SELECT
		o.effectivedate::date as date,
		LEFT(o.locale__c,2)::text as locale,
		CAST('B2C' as varchar) as unit,
		o.polygon::text as polygon,
		COUNT(DISTINCT CASE WHEN (o.acquisition_new_customer__c = '1' AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') and o.acquisition_customer_creation__c::date = c.createddate::date) THEN o.sfid ELSE NULL END)::numeric as invoiced_acquisitions,
		SUM(CASE WHEN o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN o.gmv_eur_net ELSE 0 END)::numeric as invoiced_gmv_gross, 
		SUM(CASE WHEN o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric as invoiced_hours, 
		COUNT(DISTINCT o.customer_id__c)::numeric as active_customers, 
		COUNT(DISTINCT CASE WHEN (o.acquisition_new_customer__c = '0' AND o.acquisition_channel__c = 'web') or (o.acquisition_customer_creation__c::date != c.createddate::date and acquisition_new_customer__c = '1') THEN o.customer_id__c ELSE NULL END)::numeric as reactivated_customers,

		COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED NO MANPOWER' THEN o.sfid ELSE NULL END)::numeric as orders_no_manpower, 
		COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED NO MANPOWER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN o.sfid ELSE NULL END)::numeric as divisor_NMP_calc, 

		COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED CUSTOMER' THEN o.sfid ELSE NULL END)::numeric as orders_skipped, 
		COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED CUSTOMER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN o.sfid ELSE NULL END)::numeric as divisor_skipped_calc 

	FROM bi.orders o
	JOIN Salesforce.Contact c
	ON (o.contact__c = c.sfid)

	WHERE o.test__c = '0'
		AND o.order_type = '1'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
		AND o.effectivedate::date < current_date::date

	GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon

	ORDER BY date desc, locale asc, polygon asc

;


INSERT INTO bi.temp_city_overview_1

	SELECT
		o.effectivedate::date as date,
		LEFT(o.locale__c,2)::text as locale,
		CAST('B2B' as varchar) as unit,
		o.polygon::text as polygon,
		0 as invoiced_acquisitions,
		SUM(CASE WHEN o.status not like '%CANCELLED%' and o.status not like '%ERROR%' and effectivedate::date < current_date::date THEN o.gmv_eur_net ELSE 0 END)::numeric as invoiced_gmv_gross, 
		SUM(CASE WHEN o.status not like '%CANCELLED%' and o.status not like '%ERROR%' and effectivedate::date < current_date::date THEN o.order_duration__c ELSE 0 END)::numeric as invoiced_hours,
		COUNT(DISTINCT o.customer_id__c)::numeric as active_customers, 
		0 as reactivated_customers,

		COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED NO MANPOWER' THEN o.sfid ELSE NULL END)::numeric as orders_no_manpower,
		COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED NO MANPOWER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') or status not like '%ERROR%' THEN o.sfid ELSE NULL END)::numeric as divisor_NMP_calc, 

		COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED CUSTOMER' THEN o.sfid ELSE NULL END)::numeric as orders_skipped, 
		COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED CUSTOMER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') or status not like '%ERROR%' THEN o.sfid ELSE NULL END)::numeric as divisor_skipped_calc

	FROM bi.orders o
	JOIN Salesforce.Contact c
	ON (o.contact__c = c.sfid)

	WHERE o.test__c = '0'
		AND o.order_type = '2'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
		AND o.effectivedate::date < current_date::date

	GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon

	ORDER BY date desc, locale asc, polygon asc

;



DROP TABLE IF EXISTS bi.temp_city_overview_2;
CREATE TABLE bi.temp_city_overview_2 AS

	SELECT
		o.order_creation__c::date as date,
		LEFT(o.locale__c,2)::text as locale,
		CAST('B2C' as varchar) as unit,
		o.polygon::text as polygon,
		COUNT(DISTINCT CASE WHEN (o.acquisition_new_customer__c = '1') and o.acquisition_customer_creation__c::date = c.createddate::date THEN o.sfid ELSE NULL END)::numeric as all_acquisitions

	FROM bi.orders o
	JOIN Salesforce.Contact c
	ON (o.contact__c = c.sfid)

	WHERE o.test__c = '0'
		AND o.order_type = '1'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
		AND o.order_creation__c::date < current_date::date

	GROUP BY o.order_creation__c::date, LEFT(o.locale__c,2), o.polygon

	ORDER BY date desc, locale asc, polygon asc

;


----
----
----
----
----



DROP TABLE IF EXISTS bi.temp_churns;
CREATE TABLE bi.temp_churns as 
SELECT

	created_at::date as date,
	created_at as time,
	CAST('B2C' as varchar) as unit,
	(order_Json->>'Order_Start__c') as orderdate,
	order_Json->>'Locale__c' as Locale__c,
	order_Json->>'Order_Id__c' as Order_id,
	order_Json->>'Contact__c' as customer_id
	FROM
	 events.sodium
	 
	WHERE  event_name = 'Order Event:CANCELLED TERMINATED'

;


DROP TABLE IF EXISTS bi.temp_acquiredlist;
CREATE TABLE bi.temp_acquiredlist AS

	SELECT DISTINCT

		customer_id__c

	FROM bi.orders 

	WHERE test__c = '0'
		AND acquisition_new_customer__c = '1'
		AND order_type = '1'
		AND status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')

;


DROP TABLE IF EXISTS bi.temp_churnlist;
CREATE TABLE bi.temp_churnlist AS

	SELECT

		t2.*

	FROM bi.temp_acquiredlist t1

	JOIN bi.temp_churns t2 ON t1.customer_id__c = t2.customer_id

;




DROP TABLE IF EXISTS bi.temp_city_overview_3;
CREATE TABLE bi.temp_city_overview_3 AS

	SELECT
		t1.date::date as date,
		LEFT(t1.locale__c,2)::text as locale,
		CAST('B2C' as varchar) as unit,
		t2.polygon::text as polygon,
		COUNT(DISTINCT t1.customer_id)::numeric as churned_customers

	FROM bi.temp_churnlist t1

	JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

	WHERE t2.test__c = '0' 
		AND order_type = '1'
		AND t1.date::date < current_date::date
	
	GROUP BY t1.date::date, LEFT(t1.locale__c,2) , t2.polygon

	ORDER BY date desc, locale asc, polygon asc

;


DROP TABLE IF EXISTS bi.temp_city_overview_4;
CREATE TABLE bi.temp_city_overview_4 AS

	SELECT

	c.orderdate::date as date,
	c.locale::text as locale,
	CAST('B2C' as varchar) as unit,

	NULL::text as polygon,
	SUM(c.sem_non_brand + c.sem_brand + c.seo + c.facebook + c.criteo + c.sociomantic + c.gdn + c.youtube + c.coops + c.tvcampaign + c.offline_marketing + c.other_voucher)::numeric as total_costs

	FROM bi.cpacalclocale c
	
	GROUP BY c.orderdate::date, c.locale

	ORDER BY date desc, locale asc

;


DROP TABLE IF EXISTS bi.temp_city_overview_5;
CREATE TABLE bi.temp_city_overview_5 AS

	SELECT

	u.mindate::date as date,
	LEFT(u.delivery_area,2)::text as locale,
	u.delivery_area::text as polygon,
	CAST('B2C' as varchar) as unit,
	SUM(u.weekly_hours)::numeric as weekly_contract_hours,
	SUM(u.worked_hours)::numeric as weekly_worked_hours,
	SUM(u.revenue)::numeric as weekly_revenue_generated,
	SUM(u.salary_payed)::numeric as weekly_salary_paid

	FROM bi.gpm_weekly_cleaner u

	WHERE mindate < current_date
	
	GROUP BY u.mindate::date, LEFT(u.delivery_area,2), u.delivery_area

	ORDER BY date desc, locale asc, polygon asc

;

DROP TABLE IF EXISTS bi.temp_city_overview_6;
CREATE TABLE bi.temp_city_overview_6 as 
SELECT
	date,
	locale,
	polygon,
	CAST('B2C' as varchar) as unit,
	SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+youtube_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+newsletter_discount+seo_discount+seo_brand_discount+youtube_discount) as total_marketing_costs,
	sum(all_acq) as all_acquisitions_marketing
FROM
	bi.cpacalcpolygon 
GROUP BY
	date,
	polygon,
	locale;


DROP TABLE IF EXISTS bi.city_overview;
CREATE TABLE bi.city_overview AS

	SELECT
		t1.*,
		t6.invoiced_acquisitions,
		t6.invoiced_gmv_gross,
		t6.invoiced_hours,
		t6.active_customers,
		t6.reactivated_customers,
		t6.orders_no_manpower,
		t6.divisor_nmp_calc,
		t6.orders_skipped,
		t6.divisor_skipped_calc,
		t2.all_acquisitions,
		t3.churned_customers,
		t4.total_costs,
		t5.weekly_contract_hours,
		t5.weekly_worked_hours,
		t5.weekly_revenue_generated,
		t5.weekly_salary_paid,
		t7.total_marketing_costs,
		all_acquisitions_marketing


	FROM 
		(SELECT
	polygon,
	Left(polygon,2) as locale,
	date,
	CAST('B2C' as varchar) as Unit
FROM
	bi.orders,
		(select i::date as date from generate_series('2016-06-01', 
  '2017-06-30', '1 day'::interval) i) b
WHERE
	effectivedate::date between current_date::date - Interval '60 days' and current_date::date
GROUP BY
	polygon,
	locale,
	date
UNION
SELECT
	polygon,
	Left(polygon,2) as locale,
	date,
	CAST('B2B' as varchar) as Unit
FROM
	bi.orders,
		(select i::date as date from generate_series('2016-06-01', 
  '2017-06-30', '1 day'::interval) i) b
WHERE
	effectivedate::date between current_date::date - Interval '60 days' and current_date::date
GROUP BY
	polygon,
	locale,
	date	) as t1
	
	
	LEFT JOIN bi.temp_city_overview_1 t6
			ON t1.date = t6.date 
			AND t1.locale = t6.locale
			AND t1.polygon = t6.polygon
			and t1.unit = t6.unit

	LEFT JOIN bi.temp_city_overview_2 t2 
		ON t1.date = t2.date 
			AND t1.locale = t2.locale
			AND t1.polygon = t2.polygon
			and t1.unit = t2.unit

	LEFT JOIN bi.temp_city_overview_3 t3
		ON t1.date = t3.date 
			AND t1.locale = t3.locale
			AND t1.polygon = t3.polygon
			AND t1.unit = t3.unit

	LEFT JOIN bi.temp_city_overview_4 t4
		ON t1.date = t4.date 
			AND t1.locale = t4.locale
			AND t1.polygon = t4.polygon
			and t1.unit = t4.unit

	LEFT JOIN bi.temp_city_overview_5 t5
		ON t1.date = t5.date 
			AND t1.locale = t5.locale
			AND t1.polygon = t5.polygon
			and t1.unit = t5.unit
			
		LEFT JOIN bi.temp_city_overview_6 t7
		ON t1.date = t7.date 
			AND t1.locale = t7.locale
			AND t1.polygon = t7.polygon
			and t1.unit = t7.unit		


;


DROP TABLE IF EXISTS bi.temp_churns;
DROP TABLE IF EXISTS bi.temp_acquiredlist;
DROP TABLE IF EXISTS bi.temp_churnlist;
DROP TABLE IF EXISTS bi.temp_city_overview_1;
DROP TABLE IF EXISTS bi.temp_city_overview_2;
DROP TABLE IF EXISTS bi.temp_city_overview_3;
DROP TABLE IF EXISTS bi.temp_city_overview_4;
DROP TABLE IF EXISTS bi.temp_city_overview_5;


-- Utilization and GPM done NL

DROP TABLE IF EXISTS bi.gpm_city;
CREATE TABLE bi.gpm_city as   
select
    year_month,
    delivery_area,
    min(mindate) as mindate,
    sum( Revenue) as Revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_per_cleaner_v3
GROUP BY
	year_month,
	delivery_area;

DROP TABLE IF EXISTS bi.gpm_city_over_time;
CREATE TABLE bi.gpm_city_over_time as 	
SELECT
	year_month,
	delivery_area,
	min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_city
GROUP BY
	year_month,
	delivery_area;

INSERT INTO bi.gpm_city_over_time	
SELECT
	year_month,
	'DE',
	min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_city
WHERE
	delivery_area like 'de%'
GROUP BY
	year_month;

	
INSERT INTO bi.gpm_city_over_time	
SELECT
	year_month,
	'NL',
	min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_city
WHERE
	delivery_area like 'nl%'
GROUP BY
	year_month;


-- Topline Dashboard


DROP TABLE IF EXISTS bi.topline_kpi;
CREATE TABLE bi.topline_kpi as 
SELECT
	EXTRACT(Month FROM Order_Creation__C::Date) as Month,
	EXTRACT(Year FROM Order_Creation__C::Date) as Year,
	MIN(Order_Creation__C::Date) as Mindate,
	replace(city, '+', '') as city,
	CAST('Booked GMV' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur) as value
FROM
	bi.orders
WHERE
	status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
		and order_type = '1'
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Order_Creation__C::Date) as Month,
	EXTRACT(Year FROM Order_Creation__C::Date) as Year,
	MIN(Order_Creation__C::Date) as Mindate,
	UPPER(LEFT(locale__c,2)) as locale,
	CAST('Booked GMV' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur) as value
FROM
	bi.orders
WHERE
	Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	CAST('M1 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	city;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	LEFT(city,2) as locale,
	CAST('M1 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	locale;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	CAST('M3 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	city;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	LEFT(city,2) as locale,
	CAST('M3 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	locale;
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	CAST('M6 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	city;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	LEFT(city,2) as locale,
	CAST('M6 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	locale;
	

INSERT INTO bi.topline_kpi		
SELECT
	EXTRACT(MONTH FROM Date) as Month,
	EXTRACT(YEAR FROM Date) as Year,
	MIN(date::date) as mindate,
	city,
	CAST('M1 RR' as text) as kpi,
	CASE WHEN acquisition_Channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
	CASE WHEN SUM(Acquisitions) > 0 THEN SUM(Acquisitions*cohort_Return_rate)/SUM(Acquisitions) ELSE 0 END as value
FROM
		bi.retention_city_marketing
GROUP BY
	Month,
	year,
	city,
	kpi,
	channel;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Total Customers' as kpi,
	CAST('B2C' as text) as breakdown,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	citya;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(locale__c,2)) as citya,
	'Total Customers' as kpi,
	CAST('B2C' as text) as breakdown,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Total Customers' as kpi,
	CAST('B2B' as text) as breakdown,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders
WHERE
	test__c = '0'
	and status not like '%CANCELLED%' and status not like '%ERROR%'
	and effectivedate::date < current_date::date
	and order_type = '2'
GROUP BY
	Month,
	Year,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(locale__c,2)) as citya,
	'Total Customers' as kpi,
	CAST('B2B' as text) as breakdown,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders
WHERE
	test__c = '0'
	and status not like '%CANCELLED%' and status not like '%ERROR%'
	and effectivedate::date < current_date::date
	and order_type = '2'
GROUP BY
	Month,
	Year,
	citya;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM Order_Creation__c::Date) as Month,
	EXTRACT(YEAR FROM Order_Creation__c::date) as year,
	min(Order_Creation__c::date) as mindate,
	city,
	'Acquisitions' as kpi,
	marketing_channel as breakdown,
	COUNT(1) as value
FROM
	bi.orders
WHERE
	Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and test__c = '0'
	and Acquisition_New_Customer__c = '1'
	and order_type = '1'
GROUP BY
	Month,
	year,
	city,
	breakdown;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM max_date::date) as Month,
	EXTRACT(YEAR FROM max_date::date) as Year,
	MIN(max_date::date) as mindate,
		case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('COP Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= hr_contract_end__c THEN sfid ELSE null END)) as cop
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Month,
	MAX(Date) as max_date
FROM
	(select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
 	Month) as a,
 	(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
 GROUP BY
 	Year,
 	Month,
 	max_date::date,
 	delivery_areas__c;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM max_date::date) as Month,
	EXTRACT(YEAR FROM max_date::date) as Year,
	MIN(max_date::date) as mindate,
		case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('COP Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and (max_date < hr_contract_end__c or hr_contract_end__c is null) THEN sfid ELSE null END)) as a
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Month,
	MAX(Date) as max_date
FROM
	(select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
 	Month) as a,
 	(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	) as b
WHERE
	LEFT(b.delivery_areas__c,2) = 'ch'
 GROUP BY
 	Year,
 	Month,
 	max_date::date,
 	delivery_areas__c;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM hr_contract_start__c::date) as Month,
	EXTRACT(YEAR FROM hr_contract_start__c::date) as Year,
	MIN(hr_contract_start__c::date) as mindate,
		case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'de-otherDuisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as City,
	CAST('Onboardings Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
	hr_contract_start__c <= current_date
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM hr_contract_end__c::date) as Month,
	EXTRACT(YEAR FROM hr_contract_end__c::date) as Year,
	MIN(hr_contract_end__c::date) as mindate,
			case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as city,
	CAST('Churn Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
	hr_contract_end__c < current_date
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM createddate::date) as Month,
	EXTRACT(YEAR FROM createddate::date) as Year,
	MIN(createddate::date) as mindate,
			case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as city,
	CAST('Onboardings Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	createddate::date < current_date
	and LEFT(Locale__c,2) in ('at')
GROUP BY
	Month,
	Year,
	City;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM hr_contract_start__c::date) as Month,
	EXTRACT(YEAR FROM hr_contract_start__c::date) as Year,
	MIN(hr_contract_start__c::date) as mindate,
			case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as city,
	CAST('Onboardings Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	hr_contract_start__c::date < current_date
	and LEFT(Locale__c,2) in ('ch')
GROUP BY
	Month,
	Year,
	City;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as Month,
	EXTRACT(Year FROM mindate) as Year,
	mindate,
	city,
	kpi,
	CAST('-' as text) as breakdown,
	value
FROM
	bi.recurrent_kpis_monthly 
WHERE
	kpi in ('Existing Recurrent','New Recurrent','Recurrent Cancellations');


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	MIN(t2.date) as mindate,
	t1.working_city__c as city,
	CAST('Churn Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	COUNT(DISTINCT(t1.sfid)) as value
FROM
	Salesforce.Account t1
JOIn
	bi.left_date t2
ON
	(t1.sfid = t2.sfid)
WHERE
	LEFT(locale__c,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	MIN(effectivedate::date) as mindate,
	t2.city as city,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;
	

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	MIN(effectivedate::date) as mindate,
	UPPER(LEFT(t2.locale__c,2)) as citya,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	Citya;

	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	MIN(effectivedate::date) as mindate,
	t2.city as city,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('ch','at')
GROUP BY
	Month,
	Year,
	City;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	MIN(effectivedate::date) as mindate,
	UPPER(LEFT(t2.locale__c,2)) as citya,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('ch','at')
GROUP BY
	Month,
	Year,
	Citya;

	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
	t2.city as city,
	CAST('GMV per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;
		
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
	t2.city as city,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
	and LEFT(t2.locale__C,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	City;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('-' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
	Month,
	Year,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('-' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
	Month,
	Year,
	locale;
		
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('Recurrent' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and recurrency__c > '6'
		and order_type = '1'
GROUP BY
	Month,
	Year,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('Recurrent' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and recurrency__c > '6'
	and  order_type = '1'
GROUP BY
	Month,
	Year,
	locale;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and (recurrency__c is null or recurrency__c = '0')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('One-Off' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and (recurrency__c is null or recurrency__c = '0')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('B2B' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and (status not like '%CANCELLED%' and status not like '%ERROR%')
	and (effectivedate::date < current_date)
	and type = 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('B2B' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and (status not like '%CANCELLED%' and status not like '%ERROR%')
	and (effectivedate::date < current_date)
	and type = 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	locale
	;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('B2C' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
	and (effectivedate::date < current_date)
	and type != 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
		'Invoiced Revenue' as kpi,
	CAST('B2C' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
	and (effectivedate::date < current_date)
	and type != 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/	(SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) != 'ch'
GROUP BY
	Month,
	Year,
	Citya;

	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '2'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(replace(city, '+', ''),2)) as citya,
	'PPH' as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(replace(city, '+', ''),2)) as citya,
	'PPH' as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '2'
GROUP BY
	Month,
	Year,
	Citya;


	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(Locale__c,2)) as citya,
	'PPH' as kpi,
	CAST('-' as text) as breakdown,
		CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/	(SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value

FROM
	bi.orders t2
WHERE
	test__c = '0'
GROUP BY
	Month,
	Year,
	LEFT(Locale__c,2);
	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('Recurrent' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'recurrent'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(Locale__c,2)) as citya,
	'PPH' as kpi,
	CAST('Recurrent' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'recurrent'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	LEFT(Locale__c,2);	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM 
	bi.orders t2
WHERE 
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'web'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(Locale__c,2)) as citya,
	'PPH' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'web'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	LEFT(Locale__c,2);


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
	t2.city as city,
	CAST('GMV per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
	and LEFT(t2.locale__C,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	City;
	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN sum(revenue) >0 THEN SUM(gp)/sum(revenue) ELSE 0END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate::date) as Month,
	EXTRACT(YEAR FROM mindate::date) as Year,
	min(mindate::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B Sub' as text) as breakdown,
	CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value
FROM
	bi.gpm_subcontractor
GROUP BY
	year,
	month,
	citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate::date) as Month,
	EXTRACT(YEAR FROM mindate::date) as Year,
	min(mindate::date) as date,
	UPPER(LEFT(delivery_area,2)) as localea,
	CAST('GPM %' as text) as kpi,
	CAST('B2B Sub' as text) as breakdown,
	CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value
FROM
	bi.gpm_subcontractor
GROUP BY
	year,
	month,
	localea;

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'ch-basel' THEN 'CH-Basel'
  when delivery_area = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_area = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_area = 'ch-lausanne' THEN 'CH-Lausanne'
  WHEN delivery_Area = 'ch-lucerne' THEN 'CH-Lucerne'
  WHEN delivery_Area = 'ch-stgallen' THEN 'CH-Stgallen'
  WHEN delivery_area = 'ch-zurich' THEN 'CH-Zurich'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
  
WHERE
	mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
WHERE
	mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  localea;
  
 INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'ch-basel' THEN 'CH-Basel'
  when delivery_area = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_area = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_area = 'ch-lausanne' THEN 'CH-Lausanne'
  WHEN delivery_Area = 'ch-lucerne' THEN 'CH-Lucerne'
  WHEN delivery_Area = 'ch-stgallen' THEN 'CH-Stgallen'
  WHEN delivery_area = 'ch-zurich' THEN 'CH-Zurich'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
  
WHERE
	mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  citya;
 
  
  
 INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
WHERE
	mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  localea;
  
 

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'at-vienna' THEN 'AT-Vienna'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('AT' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_at

GROUP BY
  year,
  month,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('AT' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_at

GROUP BY
  year,
  month,
  localea;

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
	
INSERT INTO bi.topline_kpi
SELECT
		EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('B2B Revenue' as text) as kpi,
	CASE WHEN (t2.Subcon like '%BAT%' or t2.Subcon like '%BOOK%') THEN 'BAT Cleaner' ELSE 'Sub Cleaner' END as Typeaa,
	SUM(GMV_eur) as value
FROM
	bi.orders t1
LEFT JOIN
(SELECT
 		a.*,
 		t2.name as subcon
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as t2
ON
	(t1.professional__c = t2.sfid)
WHERE
	status not like '%CANCELLED%' and status not in ('INTERNAL ERROR')
	and order_type = '2'
GROUP BY
	year,
	month,
	citya,
	typeaa;

INSERT INTO bi.topline_kpi
SELECT
		EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(delivery_area,2)) as citya,
	CAST('B2B Revenue' as text) as kpi,
	CASE WHEN (t2.Subcon like '%BAT%' or t2.Subcon like '%BOOK%') THEN 'BAT Cleaner' ELSE 'Sub Cleaner' END as Typeaa,
	SUM(GMV_eur) as value
FROM
	bi.orders t1
LEFT JOIN
(SELECT
 		a.*,
 		t2.name as subcon
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as t2
ON
	(t1.professional__c = t2.sfid)
WHERE
	status not like '%CANCELLED%' and status not in ('INTERNAL ERROR')
	and order_type = '2'
GROUP BY
	year,
	month,
	citya,
	typeaa;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate::date) as Month,
	EXTRACT(YEAR FROM mindate::date) as Year,
	min(mindate::date) as date,
	UPPER(LEFT(delivery_area,2)) as locale,
	CAST('GPM %' as text) as kpi,
	CAST('B2B Sub' as text) as breakdown,
	CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value
FROM
	bi.gpm_subcontractor
GROUP BY
	year,
	month,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN sum(b2b_revenue) >0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN sum(b2c_revenue) >0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'DE' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(CASE WHEN sum(revenue) > 0 THEN SUM(gp)/sum(revenue) ELSE 0 END) as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%de%'
GROUP BY
	year,
	month;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'DE' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%de%'
GROUP BY
	year,
	month;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'DE' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%de%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'NL' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN sum(revenue) > 0 THEN SUM(gp)/sum(revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%nl%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'NL' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%nl%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'NL' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%nl%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT-Vienna' as city,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT-Vienna' as city,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT' as city,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT' as city,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;


INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'CH' as city,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and effectivedate::date < '2017-01-01'
GROUP BY
	Month,
	Year
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'CH' as city,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and order_type = '1'
	and effectivedate::date < '2017-01-01'
GROUP BY
	Month,
	Year
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;	

	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon in ('ch-geneva','ch-lausanne')
	and effectivedate::date < '2017-01-01'
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;
	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon in ('ch-geneva','ch-lausanne')
	and order_type = '1'
	and effectivedate::date < '2017-01-01'
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon not in ('ch-geneva','ch-lausanne')
	and effectivedate::date < '2017-01-01'
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;		

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon not in ('ch-geneva','ch-lausanne')
	and order_type = '1'
	and effectivedate::date < '2017-01-01'
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;	

DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
	first_order_month,
	order_month,
	min(start_Date) as order_date,
	min(first_order_Date) as start_date,
	city,
	acquisition_channel,
	COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
	bi.RecurringCustomerCohort
WHERE
	to_char(current_date,'YYYY-MM') >= 	first_order_month
	and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
	first_order_month,
	order_month,
	city,
	acquisition_channel;

CREATE INDEX IDX333 ON bi.customer_cohorts_exp(first_order_month);

DROP TABLE IF EXISTS bi.customer_cohorts_recurrent;
CREATE TABLE bi.customer_cohorts_recurrent as 	
SELECT
	a.first_order_month as cohort,
	b.order_month,
	b.order_date as mindate,
	CASE 
	WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
	WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
	ELSE 0 END as returning_month,
	a.city,
	a.acquisition_channel,
	a.distinct_customer as total_cohort,
	b.distinct_customer as returning_customer
FROM
(SELECT
	first_order_month,
	order_month,
	start_date,
	order_date,
	city,
	acquisition_channel,
	distinct_customer
FROM
	bi.customer_cohorts_exp
WHERE
	first_order_month = order_month) as a
LEFT JOIN
	(SELECT
	first_order_month,
	order_month,
	start_date,
	order_date,
	city,
	acquisition_channel,
	distinct_customer
FROM
	bi.customer_cohorts_exp
) as b
ON
	(a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel);


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M1 RR Recurrent' as kpi,
	CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	city,
	kpi,
	channel;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M1 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	city,
	kpi;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M3 RR Recurrent' as kpi,
	CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	city,
	kpi,
	channel;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M3 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	city;

DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
	first_order_month,
	order_month,
	min(start_Date) as order_date,
	min(first_order_Date) as start_date,
	left(city,2) as locale,
	city,
	acquisition_channel,
	COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
	bi.RecurringCustomerCohort
WHERE
	to_char(current_date,'YYYY-MM') >= 	first_order_month
	and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
	first_order_month,
	order_month,
	left(city,2),
	city,
	acquisition_channel;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	left(city,2) as locale,
	'M1 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	locale;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	left(city,2) as locale,
	'M3 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	locale,
	kpi;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	left(city,2) as locale,
	'M6 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	locale,
	kpi;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M6 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	city,
	kpi;

DROP TABLE IF EXISTS bi.cleaner_availability_morning;
CREATE TABLE bi.cleaner_availability_morning as
SELECT
	sfid,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) END as availability_monday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) < 0  THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) END as availability_tuesday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) < 0 THEN 0 ELSE  (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) END as availability_wednesday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) END as availability_thursday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) END as availability_friday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) END as availability_saturday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time)) END as total_monday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time)) END as total_tuesday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time)) END as total_wednesday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time)) END as total_thursday,	
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time)) END as total_friday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time)) END as total_saturday
FROM
	Salesforce.Account
WHERE
	Status__c in ('ACTIVE','BETA')
	and type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%';

DROP TABLE IF EXISTS bi.cleaner_ur_temp;
CREATE TABLE bi.cleaner_ur_temp as	
SELECT 
	sfid,
	SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as availability_morning,
	SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) as availability_total,
	CASE WHEN SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) = '0' THEN 0 ELSE CAST(SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as decimal)/SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) END as availability_Share
FROM
	bi.cleaner_availability_morning
GROUP BY
	sfid;


DROP TABLE IF EXISTS bi.order_distribution;
CREATE TABLE bi.order_distribution as 
SELECT
	professional__c,
	to_char(effectivedate::date,'YYYY-MM') as Month,
		SUM(CASE WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and EXTRACT(HOUR FROM Order_End__c+ Interval '2 hours') < 13 THEN Order_Duration__c
				WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and extract(hour from order_end__c+ Interval '2 hours') > 13 THEN 13 - EXTRACT(HOUR FROM order_Start__c+ Interval '2 hours') ELSE 0 END) as Working_Hours_Morning,
				SUM(ORder_Duration__c) as total_hours
FROM
	bi.orders
WHERE
	test__c = '0'
	and status = 'INVOICED'
GROUP BY
	professional__c,
	month;

DROP TABLE IF EXISTS bi.margin_per_cleaner;
CREATE TABLE bi.margin_per_cleaner as
SELECT
	t2.name,
	t2.rating__c as rating,
	t5.Score_cleaners,
	t2.type__c,
	t1.*,
	CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
	CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END as contract_morning_hours,
	t3.Working_Hours_Morning,
	availability_Share,
	CASE WHEN availability_share is null or availability_share = 0 THEN 0 ELSE cast(t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END) END as morning_ur,
	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END as contract_hours_afternoon,
	t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
	CASE WHEN (1-availability_Share) is null or (1-availability_Share) = 0 THEN 0 ELSE cast(t3.total_hours-t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*(1-availability_Share) END) END as afternoon_ur,
	CASE WHEN availability_Share is null THEN 0 ELSE CASE WHEN t3.Working_Hours_Morning > working_hours*availability_Share THEN working_hours*availability_Share ELSE t3.Working_Hours_Morning END END capped_hours_morning,
	CASE WHEN (1-availability_Share) = 0 THEN 0 ELSE CASE WHEN t3.total_hours-t3.working_hours_morning  > working_hours*	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END THEN working_hours*(	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END) ELSE t3.total_hours-t3.working_hours_morning END END capped_hours_afternoon
FROM
	bi.gpm_per_cleaner_v3 t1
LEFT JOIN
		Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid)
LEFT JOIN
	bi.order_distribution t3
ON
	(t1.professional__c = t3.professional__c and t1.year_month = t3.month)
LEFT JOIn
	bi.cleaner_ur_temp t4
ON
	(t1.professional__c = t4.sfid)
LEFT JOIN
	bi.KPI_Performance t5
ON
	(t1.professional__c = t5.sfid);

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3;
DROP TABLE IF EXISTS bi.order_distribution;
DROP TABLE IF EXISTS bi.cleaner_ur_temp;

----------------------------------------------------------------------------------------------------------------
-- INSERTING HERE THE GPM DATA OF SWITZERLAND INTO THE MARGIN_PER_CLEANER TABLE


INSERT INTO bi.margin_per_cleaner
SELECT

  t1.*

FROM

  bi.gpm_ch t1;



----------------------------------------------------------------------------------------------------------------
-- INSERTING HERE THE GPM DATA OF AUSTRIA INTO THE MARGIN_PER_CLEANER TABLE


INSERT INTO bi.margin_per_cleaner
SELECT

  t1.*

FROM

  bi.gpm_at t1;


----------------------------------------------------------------------------------------------------------------


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
			case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM Afternoon' as kpi,
	'-' as channel,
	CASE WHEN SUM(contract_hours_afternoon) > 0 THEN SUM(CASE WHEN capped_hours_afternoon > contract_hours_afternoon THEN contract_hours_afternoon ELSE capped_hours_afternoon END)/SUM(contract_hours_afternoon) ELSE 0 END as value
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
			case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM Morning' as kpi,
	'-' as channel,
	CASE WHEN SUM(contract_morning_hours) > 0 THEN SUM(CASE WHEN capped_hours_morning > contract_morning_hours THEN contract_morning_hours ELSE capped_hours_morning END)/SUM(contract_morning_hours) ELSE 0 END as value
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%' or delivery_area like '%nl%'
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
		case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	CASE WHEN SUM(working_hours) > 0 THEN  SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'DE-Total' as City,
	'Utilization GPM' as kpi,
	'-' as channel,
	CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%'
GROUP BY
	Month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'DE' as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%'
GROUP BY
	Month,
	year;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'CH' as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%'
GROUP BY
	Month,
	year;
	

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM Effectivedate::date) as month,
	EXTRACT(YEAR FROM Effectivedate::date) as year,
	min(Effectivedate::date),
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.orders
WHERE
	LEFT(polygon,2) = 'ch' 
GROUP BY
	Month,
	year,
	polygon;
	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'NL' as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'nl%'
GROUP BY
	Month,
	year;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'AT-Vienna'
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'AT'
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'CH'
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year;

-- GROUP KPI's


	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	CAST('GROUP' as text) as locale,
	'PPH' as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/	(SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP' as locale,
	CAST('M1 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '1'
GROUP BY
	month,
	year;
	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP' as locale,
	CAST('M3 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '3'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi		
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP' as locale,
	CAST('M6 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '6'
GROUP BY
	month,
	year;
	

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP',
	'M1 RR Recurrent' as kpi,
	'-' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP',
	'M3 RR Recurrent' as kpi,
	'-' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP',
	'M6 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(Month FROM Order_Creation__C::Date) as Month,
	EXTRACT(Year FROM Order_Creation__C::Date) as Year,
	MIN(Order_Creation__C::Date) as Mindate,
	'GROUP' as value,
	CAST('Booked GMV' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur) as value
FROM
	bi.orders
WHERE
	status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP' as text,
	'Invoiced Revenue' as kpi,
	CAST('-' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
	Month,
	Year;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP' as text,
	'Invoiced Revenue' as kpi,
	CAST('B2C' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
	and (effectivedate::date < current_date)
	and order_type = '1'
GROUP BY
	Month,
	Year;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP' as text,
	'Invoiced Revenue' as kpi,
	CAST('B2B' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and (status not like '%CANCELLED%' and status not like '%ERROR%')
	and (effectivedate::date < current_date)
	and order_type = '2'
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP',
	'Invoiced Revenue' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER')
	and (recurrency__c is null or recurrency__c = '0')
GROUP BY
	Month,
	Year;

-- GPM, UR% and GP per Week


DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.sfid as professional__c,
	delivery_areas__c as delivery_areas,
	hr_contract_start__c::date as contract_start,
	hr_contract_end__c::date as contract_end,
	hr_contract_weekly_hours_min__c as weekly_hours
FROM
	Salesforce.Account t1
WHERE
	t1.type__c like '%cleaning-b2c%'
	and  t1.test__c = '0'
	and status__c not in ('ACTIVE')
GROUP BY
	t1.sfid,
	delivery_areas__c,
	hr_contract_start__c,
	hr_contract_end__c,
	hr_contract_weekly_hours_min__c;


DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222' and effectivedate::date < current_Date-2) THEN GMV__c*1.19 ELSE 0 END) as GMV,
	SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN type = 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END) as Hours

FROM
	Salesforce.Order t1
GROUP BY
	t1.professional__c,
	date;


DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	date,
	gmv,
	hours
FROM
	bi.cleaner_Stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;


DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and date < current_date::date
	and status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	EXTRACT(WEEK FROM Date) as week,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
FROM
	bi.holidays_cleaner_3
GROUP BY
	EXTRACT(WEEK FROM Date),
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
		GREATEST(concat(to_char(mindate,'YYYY-MM'),'-01')::date,contract_start) as calc_start,
		case
		when to_char(mindate,'YYYY-MM') = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(to_char(mindate,'YYYY-MM'),'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
		when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN 	cast(LEAST(now(), contract_end, (date_trunc('MONTH', mindate::date) + INTERVAL '1 MONTH - 1 day')::date) as date) - GREATEST(concat(to_char(mindate,'YYYY-MM'),'-02')::date,contract_start) 
		else 	cast(LEAST(now(), contract_end, (date_trunc('MONTH', mindate::date) + INTERVAL '1 MONTH - 1 day')::date) as date) - GREATEST(concat(to_char(mindate,'YYYY-MM'),'-01')::date,contract_start) + 1
		END AS days_worked
from (
SELECT	
   EXTRACT(WEEK FROM Date) as Year_Week,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	7 as days_of_month,
	contract_start,
	contract_end,
	SUM(hours) as worked_hours,
	SUM(GMV) as GMV,
	MAX(Weekly_hours) as weekly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Week,
	professional__c,
	contract_start,
	contract_end,
	delivery_area	) xx;


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and EXTRACT(WEEK FROM t1.mindate) = t2.week);


DROP TABLE IF EXISTS bi.gpm_per_cleaner_per_week;
CREATE TABLE bi.gpm_per_cleaner_per_week as 
select
    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as absence_hours,
	(weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,

	(CASE WHEN MIN(mindate) <= '2016-12-31'
	THEN
	SUM(CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > weekly_hours THEN (((weekly_hours/5)*(sickness+holiday))+worked_hours)*12.25 ELSE weekly_hours*12.25 END)
	ELSE
	SUM(CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > weekly_hours THEN (((weekly_hours/5)*(sickness+holiday))+worked_hours)*12.5 ELSE weekly_hours*12.5 END)
	END)
	as salary_payed,

	SUM(GMV) as GMV,
	SUM(GMV/1.19) as Revenue,
	contract_end-mindate as diff_week_contractend
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
	 diff_week_contractend;
  

  
INSERT INTO bi.gpm_per_cleaner_per_week 
select
    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as absence_hours,
	(weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
	 CASE WHEN ((weekly_hours/5)*(sickness))+worked_hours > weekly_hours THEN 	(((weekly_hours/5)*(sickness+holiday))+worked_hours)*14.73 ELSE weekly_hours*14.73 END as salary_payed,
	SUM(GMV) as GMV,
	SUM(GMV/1.06) as Revenue,
	contract_end-mindate as diff_week_contractend
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'nl'
    and days_worked > 0
GROUP BY
    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
	diff_week_contractend; 
  
    
DROP TABLE IF EXISTS bi.gpm_weekly_cleaner;
CREATE TABLE bi.gpm_weekly_cleaner as 
SELECT
	year_week,
	mindate,
	professional__c,
	name,
	delivery_area,
	contract_start,
	contract_end,
	weekly_hours,
	absence_hours,
	total_hours,
	SUM(worked_hours) as worked_hours,
	CASE WHEN SUM(worked_hours) > 0 THEN SUM(gmv)/SUM(worked_hours) ELSE 0 END as pph,
	revenue,
	salary_payed,
	hr_contract_weekly_hours_max__c as max_weekly_hours,
	revenue-salary_payed as gp,
	CASE WHEN revenue > 0 THEN (revenue-salary_payed)/revenue ELSE 0 END  as gpm
FROM
	bi.gpm_per_cleaner_per_week t1
LEFT JOIN
	salesforce.account t2
ON
	(t1.professional__c = t2.sfid)
WHERE
	diff_week_contractend > 6
GROUP BY
	year_week,
	mindate,
	contract_start,
	total_hours,
	contract_end,
	absence_hours,
	professional__c,
	name,
	weekly_hours,
	hr_contract_weekly_hours_max__c,
	delivery_area,
	salary_payed,
	revenue;

DROP TABLE IF EXISTS bi.left_date;
CREATE TABLE bi.left_date as 	
SELECT
	professional_json->>'Id' as sfid,
	min(created_at::date) as date
FROM
	events.sodium
WHERE
	event_name in ('Account Event:LEFT','Account Event:TERMINATED','Account Event:SUSPENDED')
GROUP BY
	sfid;


DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate_v2
WHERE
	date > '2016-01-01'
	and status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;




INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
	COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		   case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type = 'cleaning-b2c'
			and status = 'INVOICED'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		   citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	   citya,
	   kpi,
	   breakdown

	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
     'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	SUM(HOURS)/COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		   case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type = 'cleaning-b2c'
			and status = 'INVOICED'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		   citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	   citya,
	   kpi,
	   breakdown

	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	(Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
	
	FROM

		(SELECT
		 'DE' as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type in ('cleaning-b2c','cleaning-b2b')
			and status = 'INVOICED'
			and o.type__c like 'cleaning-b2c%'
			and left(a.locale__c,2) = 'de'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
		 citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    UPPER(t2.citya::text) as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
	COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		LEFT(a.locale__c,2) citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type in ('cleaning-b2c','cleaning-b2b')
			and status = 'INVOICED'
			and (o.type__c like '%cleaning-b2c%' or o.type__c like '%cleaning-b2b%')
		    and LEFT(a.locale__c,2) in ('de','nl')
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	  citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
	COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		'CH' citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and status = 'INVOICED'
			and o.type__c != 'cleaning-b2c%'
		    and left(a.locale__c,2) = 'ch'
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	  citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;





INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	(Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
	
	FROM

		(SELECT
		 'NL' as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type in ('cleaning-b2c','cleaning-b2b')
			and status = 'INVOICED'
			and o.type__c like 'cleaning-b2c%'
			and left(a.locale__c,2) = 'nl'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
		 citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	(Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
	
	FROM

		(SELECT
		 'CH' as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and status = 'INVOICED'
			and o.type__c != 'cleaning-b2c%'
		    and left(a.locale__c,2) = 'ch'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
		 citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date) as Month,
	EXTRACT(YEAR FROM Date) as year,
	min(date) as mindate,
			case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Marketing Costs'::text as kpi,
    '-'::text as breakdown,
    SUM(costs) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
	date >= '2016-09-01'
GROUP BY
    polygon,
    date) as a
GROUP By
	month,
	year,
    citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date) as Month,
	EXTRACT(YEAR FROM Date) as year,
	min(date) as mindate,
			case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Marketing Acquisitions'::text as kpi,
    '-'::text as breakdown,
    SUM(all_acq) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    sum(all_acq) as all_acq,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
	date >= '2016-09-01'
GROUP BY
    polygon,
    date) as a
GROUP By
	month,
	year,
    citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date) as Month,
	EXTRACT(YEAR FROM Date) as year,
	min(date) as mindate,
	UPPER(LEFT(polygon,2)) as locale,
	'Marketing Costs'::text as kpi,
    '-'::text as breakdown,
    SUM(costs) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
	date >= '2016-09-01'
GROUP BY
    polygon,
    date) as a
GROUP By
	month,
	year,
   	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date) as Month,
	EXTRACT(YEAR FROM Date) as year,
	min(date) as mindate,
	UPPER(LEFT(polygon,2)) as locale,
	'Marketing Acquisitions'::text as kpi,
    '-'::text as breakdown,
    SUM(all_acq) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    sum(all_acq) as all_acq,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
	date >= '2016-09-01'
GROUP BY
    polygon,
    date) as a
GROUP By
	month,
	year,
    locale;
    
   INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date) as Month,
	EXTRACT(YEAR FROM Date) as year,
	min(date) as mindate,
	'GROUP' as locale,
	'Marketing Costs'::text as kpi,
    '-'::text as breakdown,
    SUM(costs) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
	date >= '2016-09-01'
GROUP BY
    polygon,
    date) as a
GROUP By
	month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date) as Month,
	EXTRACT(YEAR FROM Date) as year,
	min(date) as mindate,
	'GROUP' as locale,
	'Marketing Acquisitions'::text as kpi,
    '-'::text as breakdown,
    SUM(all_acq) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    sum(all_acq) as all_acq,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
	date >= '2016-09-01'
GROUP BY
    polygon,
    date) as a
GROUP By
	month,
	year;


INSERT INTO bi.topline_kpi
SELECT

	month::double precision,
	year::double precision,
	mindate::date,
	CASE 

 	when CITY = 'at-vienna' THEN 'AT-Vienna'
    WHEN CITY = 'ch-zurich' THEN 'CH-Zurich'
    WHEN CITY = 'ch-bern' THEN 'CH-Bern'
    WHEN CITY = 'ch-geneva' THEN 'CH-Geneva'
    WHEN CITY = 'ch-basel' THEN 'CH-Basel'
    WHEN CITY = 'ch-lausanne' then 'CH-Lausanne'
    when CITY = 'ch-geneve' then 'CH-Geneva'
    when CITY = 'de-berlin' THEN 'DE-Berlin'
    when CITY = 'ch-stgallen' THEN 'CH-St.Gallen'
    when CITY = 'de-bonn' THEN 'DE-Bonn'
    WHEN CITY = 'de-cologne' THEN 'DE-Cologne'
    WHEN CITY = 'de-dortmund' THEN 'DE-Dortmund'
    WHEN CITY = 'de-duisburg' THEN 'DE-Duisburg'
    WHEN CITY = 'de-dusseldorf' THEN 'DE-Dusseldorf'
    WHEN CITY = 'de-essen' THEN 'DE-Essen'
    WHEN CITY = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
    WHEN CITY = 'de-hamburg' THEN 'DE-Hamburg'
    WHEN CITY = 'de-mainz' THEN 'DE-Mainz'
    WHEN CITY = 'de-manheim' THEN 'DE-Mannheim'
    WHEN CITY = 'de-munich' THEN 'DE-Munich'
    WHEN CITY = 'de-nuremberg' THEN 'DE-Nuremberg'
    WHEN CITY = 'de-stuttgart' THEN 'DE-Stuttgart'
    WHEN CITY = 'nl-amsterdam' THEN 'NL-Amsterdam'
    WHEN CITY = 'nl-hague' THEN 'NL-The Hague'
    WHEN CITY IN ('DE', 'NL', 'CH', 'AT') THEN CITY
    ELSE 'Other'
    END as city,
	 kpi::text,
    breakdown::text,
    value::double precision


FROM

	bi.SummaryKPIs
;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'