CREATE OR REPLACE FUNCTION bi.daily$b2b_functions(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$b2b_functions';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 14_b2b_orders

	DROP TABLE IF EXISTS bi.b2borders;
	CREATE TABLE bi.b2borders as 
	SELECT
		t1.contact__c,
		t2.name,
		polygon,
		t2.email__c,
		t2.locale__c,
		t2.contact_name__c,
		to_char(Effectivedate::date,'YYYY-MM') as Year_month,
		min(effectivedate::Date) as Date,
		SUM(t1.GMV__c)+MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as total_amount,
		SUM(t1.GMV__c) as Cleaning_Gross_Revenue,
		MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue,
		COUNT(1) as Total_Order_Count, 
		SUM(t1.Order_Duration__C) as Total_Invoiced_Hours,
		(SUM(t1.GMV__c))/SUM(Order_Duration__c) as PPH,
		COUNT(1),
		SUM(CASE WHEN t3.company_name like '%BAT%' or t3.company_name like '%BOOK%' THEN Order_Duration__c ELSE 0 END) as bat_hour_share,
		SUM(Order_Duration__c) as total_hours
	FROM
		bi.orders t1
	LEFT JOIN
		Salesforce.Opportunity t2
	ON
		(t2.sfid = t1.opportunityid)
	LEFT JOIN
		(SELECT
 		a.*,
 		t2.name as company_name
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED')  and a.test__c = '0' and a.name not like '%test%') as t3
   	ON
   		(t1.professional__c = t3.sfid)
	WHERE
		t1.type = 'cleaning-b2b'
		and t1.test__c = '0'
		and t1.status not like '%CANCELLED%'

	GROUP BY
		year_month,
		polygon,
		t2.name,
		t2.email__c,
		t2.locale__c,
		contact__c,
		contact_name__c

	;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 18_b2brunrate

-- Signed Customers


DROP TABLE IF EXISTS bi.b2b_daily_kpis;
CREATE TABLE bi.b2b_daily_kpis as 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale;
	
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED')
GROUP BY
	date,
	locale,
	sub_kpi;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	'Net New Hours on Platform' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	'Total gross revenue' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
	

	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'New Customer churn' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;

-- DE Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'43' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'48' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'35' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer service' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'5' as value;

-- NL Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- CH Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'17' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'3' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- New Customer End

-- New Hours Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	left(locale,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(hours)/SUM(customers) as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	locale,
	date;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(hours)/SUM(customers) as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount)/SUM(Hours)
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount)/SUM(Hours)
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;


-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'889' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'791' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'97' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('19' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'234' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'270' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('28' as decimal) as value;


-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'306' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'272' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'34' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('39' as decimal) as value;


-- New Hours END

-- New Revenue Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount)/sum(clients) as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as clients
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;


INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','nl','Marketing Stats','Cost Team','Actual','2207');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','nl','Marketing Stats','Cost Team','Actual','3108');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','nl','Marketing Stats','Cost Team','Actual','2716');
	
-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20446' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'18197' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2249' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'426' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7481' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'6481' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'998' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'499' as value;

-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'11903' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'10594' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1309' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'700' as value;

-- marketing costs

-- marketing costs


INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-01-01','de','Marketing Stats','Cost Team','Actual','28755');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-12-01','de','Marketing Stats','Cost Team','Actual','17007');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-11-01','de','Marketing Stats','Cost Team','Actual','22377');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-10-01','de','Marketing Stats','Cost Team','Actual','21163');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-09-01','de','Marketing Stats','Cost Team','Actual','22535');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','de','Marketing Stats','Cost Team','Actual','20639');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-07-01','de','Marketing Stats','Cost Team','Actual','17293');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-06-01','de','Marketing Stats','Cost Team','Actual','20245');


INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-06-01','ch','Marketing Stats','Cost Team','Actual','2207');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-07-01','ch','Marketing Stats','Cost Team','Actual','3108');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','ch','Marketing Stats','Cost Team','Actual','2716');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-09-01','ch','Marketing Stats','Cost Team','Actual','6508');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-10-01','ch','Marketing Stats','Cost Team','Actual','6988');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-11-01','ch','Marketing Stats','Cost Team','Actual','7686');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-12-01','ch','Marketing Stats','Cost Team','Actual','5823');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-01-01','ch','Marketing Stats','Cost Team','Actual','5823');



INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-10-01','nl','Marketing Stats','Cost Team','Actual','8150');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-09-01','nl','Marketing Stats','Cost Team','Actual','6817');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','nl','Marketing Stats','Cost Team','Actual','2116');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-07-01','nl','Marketing Stats','Cost Team','Actual','2837');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-06-01','nl','Marketing Stats','Cost Team','Actual','2699');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-11-01','nl','Marketing Stats','Cost Team','Actual','11700');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-12-01','nl','Marketing Stats','Cost Team','Actual','10507');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-01-01','nl','Marketing Stats','Cost Team','Actual','12961');

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*76 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and sub_kpi = 'outbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	round(sum(value),0)*0.1 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Revenue'
	and sub_kpi = 'New cust revenue sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	SUM(t1.value)/SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date,
	t1.locale;
	
INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	'All' as locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	SUM(t1.value)/SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date;


INSERT INTO  bi.b2b_daily_kpis
	SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	SUM(t2.value)/SUM(t1.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	date,
	CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	date,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

INSERT INTO  bi.b2b_daily_kpis
	SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	date,
	CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	date,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

	
DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team' WHERE sub_kpi = 	'Cost Team Est'; 


-- New Revenue END

-- TOtal Customers START

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	LEFT(t1.locale__c,2) as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.orders t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunityid = t2.sfid)
WHERE
	TO_CHAR(effectivedate::date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and (status not like '%CANCELLED%' and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL'))
	and order_type = '2'
GROUP BY
	Locale;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'185' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'ch' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'27' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'33' as value;


-- Summary

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Runrate on Target% till now' as text) as type,
	CASE WHEN (SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays)))*100 > 0 THEN
	
	round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/(SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays))))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis,
	(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
		sub_kpi,
	CAST('Runrate on Target%' as text) as type,
	CASE WHEN SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END) > 0 THEN round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Achieved' as varchar) as type,
	CAST(SUM(Value) as decimal)/MAX(current_days) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Target' as varchar) as type,
	CAST(SUM(Value) as decimal)/MAX(alldays) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Target'
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	date,
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Projected Achievement' as varchar) as type,
	(CAST(SUM(Value) as decimal)/MAX(current_days))*(MAX(alldays)-MAX(current_days))+SUM(Value) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'Current Days' as type,
	MAX(current_days) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;
 	
 INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'All Days' as type,
	MAX(alldays) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Touched Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;

 INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Generated Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	'All' as locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month) as a
GROUP BY
	date;		
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Signed Customers') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(opportunities) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 53_b2b_kpis

DROP TABLE IF EXISTS bi.b2b_kpis;
CREATE TABLE bi.b2b_kpis AS

	SELECT -- INVOICED GMV

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced GMV'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- INVOICED HOURS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced hours'::text as kpi,
		SUM(o.order_duration__c)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- MIN PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Min PPH'::text as kpi,
		ROUND(MIN(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- MAX PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Max PPH'::text as kpi,
		ROUND(MAX(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- AVERAGE PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Average PPH'::text as kpi,
		ROUND((SUM(o.gmv_eur)/SUM(o.order_duration__c))::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- ACTIVE CLEANERS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;


INSERT INTO bi.b2b_kpis

	SELECT
		year::int as year,
	  	month::int as month,
	  	NULL::int as week,
	  	NULL::date as weekdate,
	  	locale::text as locale,
		city::text as city,
		'Supplies revenue'::text as kpi,
		SUM(Supply_revenue) as value
	FROM(
			SELECT
				 EXTRACT(YEAR from t1.Effectivedate) as year,
				 EXTRACT(MONTH from t1.Effectivedate) as month,
				 left(t1.locale__c,2) as locale,
				 t1.delivery_area__c as city,
					t2.sfid,
				 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
				 Salesforce.Order t1
			LEFT JOIN
				 Salesforce.Opportunity t2
			ON
				 (t2.sfid = t1.opportunityid)
			WHERE
				 type = '222' 
				 AND EXTRACT(YEAR from t1.Effectivedate) <= EXTRACT(YEAR from current_date)
				 AND EXTRACT(MONTH from t1.Effectivedate) <= EXTRACT(MONTH from current_date)
				 AND status NOT LIKE ('%CANCELLED%')
			GROUP BY
				 left(t1.locale__c,2),
				 t1.delivery_area__c,
				 t2.sfid,
				 EXTRACT(YEAR from t1.Effectivedate),
				 EXTRACT(MONTH from t1.Effectivedate)) as a
	GROUP BY
		year,
		month,
		locale,
		city
	ORDER BY
		year,
		month,
		locale,
		city

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced GMV'::text as kpi,
		ROUND(SUM(o.gmv_eur)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced GMV'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced hours'::text as kpi,
		ROUND(SUM(o.order_duration__c)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced hours'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Active cleaners'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 58_b2blikelies


DROP TABLE IF EXISTS bi.b2b_likelies;
CREATE TABLE bi.b2b_likelies AS

	SELECT
		t.*,

		CASE 	


				WHEN LOWER(t.acquisition_channel_ref__c) = 'anygrowth' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'

				WHEN LOWER(t.acquisition_channel_ref__c) = 'reveal' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'

				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
				
				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
				
				WHEN (t.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'

				WHEN ((lower(t.acquisition_channel__c) = 'inbound') AND lower(t.acquisition_tracking_id__c) = 'classifieds')					
				THEN 'Classifieds'
							
				WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		        THEN 'SEM Brand'::text
		        

		        WHEN ((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%'
		        THEN 'SEM'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'SEO'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		            THEN 'SEO Brand'::text
		            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
		                OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text 
		        THEN 'Newsletter'::text
		        

		        WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text)
		        THEN 'DTI'::text

		        /*WHEN t.acquisition_channel_params__c::text ~~ '%coop%'
		            OR t.acquisition_channel_params__c::text ~~ '%afnt%'
		            OR t.acquisition_channel_params__c::text ~~ '%putzchecker%'
		        THEN 'Affiliate/Coops'*/
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		CASE WHEN t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c NOT IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}','{"src":"step1"}') THEN 'B2B Homepage' -- THE FIRST PAGE THE LEAD ACTUALLY VISITED ON THE WEBSITE
				WHEN (t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}')) OR t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Homepage'
				WHEN t.acquisition_channel_params__c IN ('{"src":"step1"}') THEN 'B2C Funnel'
				
				ELSE 'Unknown' END
		as landing_page,

		CASE WHEN t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Funnel' -- WHERE THE LEAD TYPED-IN ITS INFORMATON
				WHEN t.acquisition_tracking_id__c IS NULL THEN 'B2B Funnel'
				ELSE 'Unknown' END
		as conversion_page

	FROM
	Salesforce.likeli__c t
	WHERE
	createddate::date > '2016-03-01'
	and type__c = 'B2B'
;

DROP TABLE IF EXISTS bi.b2b_likelie_cvr;
CREATE Table bi.b2b_likelie_cvr as 
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	left(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	conversion_page,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale,
	Source_Channel,
	landing_page,
	conversion_page;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

	
DROP TABLE IF EXISTS bi.b2b_monthly_kpis;
CREATE TABLE bi.b2b_monthly_kpis as 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	
INSERT INTO bi.b2b_monthly_kpis 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(GMV_eur) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(ORder_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	
	
INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(CASE WHEN TO_CHAR(Effectivedate::date,'YYYY-MM') = TO_CHAR(Plan_Start__c::date,'YYYY-MM') THEN contact__c ELSE NULL END)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
		(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(CASE WHEN TO_CHAR(Effectivedate::date,'YYYY-MM') = TO_CHAR(Plan_Start__c::date,'YYYY-MM') THEN GMV_eur ELSE NULL END) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;	

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(CASE WHEN TO_CHAR(Effectivedate::date,'YYYY-MM') = TO_CHAR(Plan_Start__c::date,'YYYY-MM') THEN Order_Duration__c ELSE NULL END) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;	
	
INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
		CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(GMV_Eur) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(Order_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
  
END;

$BODY$ LANGUAGE 'plpgsql'