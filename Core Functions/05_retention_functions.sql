
CREATE OR REPLACE FUNCTION bi.daily$mfunc_retention(crunchdate date) RETURNS void AS 

$BODY$

DECLARE 

function_name varchar := ' bi.daily$mfunc_retention';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 05_cohortstableau

-- Recurring Cohorts


DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
CREATE TABLE bi.AllOrders_tableaucohorts as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 contact__c,
 Order_Id__c,
 CAST(Effectivedate as Date) as Start_date,
 LEFT(locale__c,2) as locale,
 Recurrency__c,
 status,
 voucher__c,
 discount__c,
 order_duration__c,
 gmv_eur,
 marketing_channel,
 type as ordertype,
 payment_method__c as payment_method,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and order_type = '1'
  and contact__c is not null
ORDER BY
  contact__c,
  CAST(Effectivedate as Date);
 
CREATE INDEX IDX10 ON bi.AllOrders_tableaucohorts(contact__c);
   
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
CREATE TABLE bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date varchar(20) DEFAULT NULL,  city varchar(55) DEFAULT null, OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,  Start_Date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, voucher__c varchar(500) NULL, discount__c varchar(50) default null, order_duration__c integer default null, gmv_eur integer default null,acquisition_channel varchar(40) NOT NULL, ordertype varchar NULL, locale varchar NULL, payment_method varchar(50) Default NULL, funnel varchar(50) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date,city,OrderNo,contact__c,Order_Id__c,Start_Date,Recurrency__c,status,voucher__c,discount__c,order_duration__c,gmv_eur,acquisition_channel,ordertype,locale,payment_method,funnel)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Effectivedate as Date) as Start_Date,
  Recurrency__c,
  status,
  voucher__c,
  discount__c,
  order_duration__c,
  gmv_eur,
  marketing_channel,
  type,
  LEFT(locale__c,2),
  payment_method__c as payment_method,
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and order_type = '1'
  and Recurrency__c > 6
  and contact__c is not null
  and t1.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
ORDER BY
  contact__c,
  Cast(Order_Creation__c as Date);

DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;
CREATE TABLE bi.RecurringCustomer_tableaucohorts as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  acquisition_channel,
  ordertype,
  locale,
  payment_method,
  voucher__c as acquisition_voucher,
  discount__c as acquisition_discount,
  funnel as acquisition_funnel
FROM
  bi.RecurringCohortsCustomers_tableaucohorts
WHERE
  OrderNo = 1;
  
CREATE INDEX IDX145 ON bi.RecurringCustomer_tableaucohorts(contact__c);

DROP TABLE IF EXISTS bi.RecurringCustomerCohort;
CREATE TABLE bi.RecurringCustomerCohort as 
SELECT
  t1.Contact__c,
  city,
  first_order_date,
  recurrency__c,
  start_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(Start_date as date),'YYYY-MM') as Order_Month,
  status,
  order_duration__c,
  gmv_eur,
  acquisition_voucher,
  acquisition_channel,
  acquisition_discount,
  t1.ordertype,
  t1.locale,
  t1.payment_method,
  acquisition_funnel
FROM
  bi.RecurringCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.Contact__c = t2.Contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(Start_Date as date),'YYYY-MM'));


DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCohorts_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;

-- Recurrent Cohorts Aggregation

DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
  first_order_month,
  order_month,
  min(start_Date) as order_date,
  min(first_order_Date) as start_date,
  city,
  acquisition_channel,
  recurrency__c as recurrency,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer,
  SUM(Order_Duration__c) as Hours
FROM
  bi.RecurringCustomerCohort
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  city,
  recurrency,
  acquisition_channel;

DROP TABLE IF EXISTS bi.customer_cohorts_recurrent_aggr;
CREATE TABLE bi.customer_cohorts_recurrent_aggr as  
SELECT
  a.first_order_month as cohort,
  a.start_date::date as start_date,
  b.order_month,
  b.recurrency,
  b.order_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.city,
  a.acquisition_channel,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer,
  b.hours
FROM
(SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer,
  recurrency
FROM
  bi.customer_cohorts_exp
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer,
  Hours,
  recurrency
FROM
  bi.customer_cohorts_exp
) as b
ON
  (a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel and a.recurrency = b.recurrency);


-- Non Recurring Cohorts

DROP TABLE IF EXISTS bi.AllOrders2_tableaucohorts;
CREATE TABLE bi.AllOrders2_tableaucohorts as 
SELECT
 CAST(acquisition_customer_creation__c as date) as acquisition_date,
 Contact__c,
 Order_Id__c,
 CAST(Order_Creation__c as Date) as creation_date,
 Recurrency__c,
 status,
 marketing_channel,
 LEFT(Locale__c,2),
 payment_method__c
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and order_type = '1'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
ORDER BY
  Contact__c,
  CAST(Order_Creation__c as Date);
 
CREATE INDEX IDX141 ON bi.AllOrders2_tableaucohorts(contact__c);
   
DROP TABLE IF EXISTS bi.NonRecurringCohortsCustomers;
CREATE TABLE bi.NonRecurringCohortsCustomers (acquisition_date varchar(20) DEFAULT NULL,  OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,   creation_date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, acquisition_channel varchar(200) default null, locale varchar(20) default null, voucher__c varchar(255) default null, payment_method varchar(255) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.NonRecurringCohortsCustomers
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Order_Creation__c as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Order_Creation__c as Date) as creation_date,
  Recurrency__c,
  status,
  marketing_channel,
  LEFT(locale__c,2),
  voucher__c,
  payment_method__c
FROM
  bi.orders  t1
WHERE
  t1.test__c = '0'
  and order_type = '1'
  and (Recurrency__c is null or Recurrency__c = 0)
  and t1.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
ORDER BY
  Contact__c,
  Cast(Order_Creation__c as Date);
 
DROP TABLE IF EXISTS bi.acquisition_channel_nonrecurring;
CREATE TABLE bi.acquisition_channel_nonrecurring as 
SELECT
  Contact__c,
  acquisition_channel,
  voucher__c as acquisition_voucher,
  locale,
  payment_method
FROM
    bi.NonRecurringCohortsCustomers
WHERE
  OrderNo = '1';
    
       
DROP TABLE IF EXISTS bi.NonRecurringCustomerCohort;
CREATE TABLE bi.NonRecurringCustomerCohort as 
SELECT
  t1.Contact__c,
  to_char(cast(acquisition_date as date),'YYYY-MM') as AcquisitionMonth,
  Order_Id__c,
  to_char(cast(creation_date as date),'YYYY-MM')  as Order_Month,
  status,
  t2.acquisition_channel,
  t1.locale,
  acquisition_voucher,
  t2.payment_method
FROM
  bi.NonRecurringCohortsCustomers t1
LEFT JOIN
  bi.acquisition_channel_nonrecurring t2
ON
  (t1.Contact__c = t2.Contact__c) ;
  
DROP TABLE IF EXISTS bi.AllOrders2_tableaucohorts;
DROP TABLE IF EXISTS bi.NonRecurringCohorts_tableaucohorts;
DROP TABLE IF EXISTS bi.NonRecurringCohortsCustomers_tableaucohorts;

-- All Cohorts

DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
CREATE TABLE bi.AllOrders_tableaucohorts as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 Contact__c,
 Order_Id__c,
 CAST(effectivedate as Date) as start_date,
 Recurrency__c,
 status,
 order_duration__c,
 type,
 pph__c,
 polygon,
 marketing_channel,
 rating_professional__c,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as funnel
FROM
  bi.orders t1
WHERE
  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','CANCELLED NO MANPOWE','NOSHOW CUSTOMER','CANCELLED CUSTOMER SHORTTERM','CANCELLED PROFESSIONAL SHORTTERM','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and order_type = '1'
ORDER BY
  Contact__c,
  CAST(effectivedate as Date);
 
CREATE INDEX IDX10 ON bi.AllOrders_tableaucohorts(contact__c);
   
DROP TABLE IF EXISTS bi.allCohortsCustomers_tableaucohorts;
CREATE TABLE bi.allCohortsCustomers_tableaucohorts(acquisition_date varchar(200) DEFAULT NULL,  city varchar(55) DEFAULT null, OrderNo SERIAL, contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,   creation_date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, order_duration__c integer default null, professional varchar default null, ordertype varchar default null, acquisition_channel varchar(200) default null, acquisition_voucher varchar (200), payment_method varchar(200) default null, pph varchar(200) default null, rating varchar(50) default null, funnel varchar(50) default null, polygon varchar(50), PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.allCohortsCustomers_tableaucohorts(acquisition_date,city,OrderNo,contact__c,Order_Id__c,creation_date,Recurrency__c,status,order_duration__c,professional,ordertype,acquisition_channel,acquisition_voucher,payment_method,pph,rating,funnel,polygon)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  contact__c,
  Order_Id__c,
  effectivedate::date as creation_date,
  Recurrency__c,
  status,
  order_duration__c,
  professional__c,
  type,
  marketing_channel,
  voucher__c,
  payment_method__c,
  pph__c,
   rating_professional__c,
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as funnel,
  polygon
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and contact__c is not null
  and order_type = '1'
ORDER BY
  contact__c,
  Cast(effectivedate as Date);

DROP TABLE IF EXISTS bi.allcohortscustomers_stats;
CREATE TABLE bi.allcohortscustomers_stats as 
SELECT
  contact__c,
  COUNT(DISTINCT(Professional)) as Distinct_Cleaner,
  SUM(CASE WHEN Status = 'CANCELLED NO MANPOWER' THEN 1 ELSE 0 END) as NMP
FROM
   bi.allCohortsCustomers_tableaucohorts
GROUP BY
  contact__c;
  

DROP TABLE IF EXISTS bi.allCustomer_tableaucohorts;
CREATE TABLE bi.allCustomer_tableaucohorts as 
SELECT
  t1.contact__c,
  creation_date::date as first_order_date,
  ordertype,
  acquisition_channel,
  CASE
  WHEN acquisition_voucher like '%CGP%' THEN 'Groupon'
  WHEN acquisition_voucher like '%CDD%' THEN 'DailyDeal'
  WHEN acquisition_voucher like '%CLM%' THEN 'Limango'
  WHEN acquisition_voucher like '%CRB%' THEN 'Rublys'
  WHEN acquisition_voucher like '%CM%' THEN 'CM'
  WHEN acquisition_voucher like '%DEINDEAL%' or acquisition_voucher like '%DEAL%' THEN 'DeinDeal'
  ELSE acquisition_voucher END as acquisitionvoucher,
  payment_method,
  pph,
  distinct_cleaner,
  nmp,
  t1.funnel as acquisition_funnel,
  t1.rating as acquisition_rating,
  t1.Recurrency__c as acquisition_recurrency
FROM
  bi.allCohortsCustomers_tableaucohorts t1
LEFT JOIN
  bi.allcohortscustomers_stats t2
ON
  (t1.contact__c = t2.contact__c) 
WHERE
  OrderNo = 1;

DROP TABLE IF EXISTS bi.allCustomerCohort;
CREATE TABLE bi.allCustomerCohort as 
SELECT
  t1.contact__c,
  city,
  first_order_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(start_date as date),'YYYY-MM') as Order_Month,
  cast(start_date as date) as order_date,
  status,
  order_duration__c,
  ordertype,
  acquisition_channel,
  acquisitionvoucher as acquisition_voucher,
  t1.payment_method,
  pph,
  pph__c,
  distinct_cleaner,
  polygon,
  nmp,
  acquisition_funnel,
  acquisition_rating,
  acquisition_recurrency
FROM
  bi.allCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.contact__c = t2.contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(start_date as date),'YYYY-MM'));


DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
DROP TABLE IF EXISTS bi.allCohorts_tableaucohorts;
DROP TABLE IF EXISTS bi.allCohortsCustomers_tableaucohorts;
DROP TABLE IF EXISTS bi.allCustomer_tableaucohorts;

-- Aggregation Overall Cohorts

DROP TABLE IF EXISTS bi.customer_cohorts_overall_exp;
CREATE TABLE bi.customer_cohorts_overall_exp as 
SELECT
  first_order_month,
  order_month,
  min(order_date) as order_date,
  min(first_order_Date) as start_date,
  city,
  acquisition_channel,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
  bi.allCustomerCohort 
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  city,
  acquisition_channel;

DROP TABLE IF EXISTS bi.customer_cohorts_overall;
CREATE TABLE bi.customer_cohorts_overall as   
SELECT
  a.first_order_month as cohort,
  a.start_date::date as start_date,
  b.order_month,
  b.order_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.city,
  a.acquisition_channel,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer
FROM
(SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer
FROM
  bi.customer_cohorts_overall_exp
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer
FROM
  bi.customer_cohorts_overall_exp
) as b
ON
  (a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel);

--


DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
CREATE TABLE bi.AllOrders_tableaucohorts as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 contact__c,
 Order_Id__c,
 CAST(Effectivedate as Date) as Start_date,
 LEFT(t1.locale__c,2) as locale,
 Recurrency__c,
 status,
 order_duration__c,
 marketing_channel,
 type as ordertype,
 t1.payment_method__c as payment_method,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
LEFT JOIN
  Salesforce.Opportunity t2
ON
  (t2.sfid = t1.opportunityid)
WHERE
  t1.test__c = '0'
  and t2.stagename != 'IRREGULAR'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','FULFILLED') 
  and order_type = '2'
  and contact__c is not null
ORDER BY
  contact__c,
  CAST(Effectivedate as Date);
 
CREATE INDEX IDX10 ON bi.AllOrders_tableaucohorts(contact__c);
   
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
CREATE TABLE bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date varchar(20) DEFAULT NULL,  city varchar(55) DEFAULT null, OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,  Start_Date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, order_duration__c integer default null, acquisition_channel varchar(40) NOT NULL, ordertype varchar NULL, locale varchar NULL, payment_method varchar(50) Default NULL, funnel varchar(50) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date,city,OrderNo,contact__c,Order_Id__c,Start_Date,Recurrency__c,status,order_duration__c,acquisition_channel,ordertype,locale,payment_method,funnel)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Effectivedate as Date) as Start_Date,
  Recurrency__c,
  status,
  order_duration__c,
  marketing_channel,
  type,
  LEFT(t1.locale__c,2),
  t1.payment_method__c as payment_method,
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
LEFT JOIN
  Salesforce.Opportunity t2
ON
  (t2.sfid = t1.opportunityid)
WHERE
  t2.stagename != 'IRREGULAR'
  and t1.test__c = '0'
  and order_type = '2'
  and Recurrency__c > 6
  and contact__c is not null
  and  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','FULFILLED') 
 ORDER BY
  contact__c,
  Cast(Order_Creation__c as Date);

DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;
CREATE TABLE bi.RecurringCustomer_tableaucohorts as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  acquisition_channel,
  ordertype,
  locale,
  payment_method,
  funnel as acquisition_funnel
FROM
  bi.RecurringCohortsCustomers_tableaucohorts
WHERE
  OrderNo = 1;
  
CREATE INDEX IDX145 ON bi.RecurringCustomer_tableaucohorts(contact__c);

DROP TABLE IF EXISTS bi.b2bcohorts;
CREATE TABLE bi.b2bcohorts as 
SELECT
  t1.Contact__c,
  city,
  first_order_date,
  start_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(Start_date as date),'YYYY-MM') as Order_Month,
  status,
  order_duration__c,
  acquisition_channel,
  t1.ordertype,
  t1.locale,
  t1.payment_method,
  acquisition_funnel
FROM
  bi.RecurringCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.Contact__c = t2.Contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(Start_Date as date),'YYYY-MM'));

DROP TABLE IF EXISTS bi.customer_cohorts_overall_b2b;
CREATE TABLE bi.customer_cohorts_overall_b2b as 
SELECT
  first_order_month,
  order_month,
  min(start_date) as order_date,
  min(first_order_Date) as start_date,
  left(city,2) as locale,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
  bi.b2bcohorts 
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  LEFT(city,2);


DROP TABLE IF EXISTS bi.customer_cohorts_b2b;
CREATE TABLE bi.customer_cohorts_b2b as   
SELECT
  a.first_order_month as cohort,
  a.start_date::date as start_date,
  b.order_month,
  b.start_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.locale,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer
FROM
(SELECT
  first_order_month,
  order_month,
  order_date,
  start_date,
  locale,
  distinct_customer
FROM
   bi.customer_cohorts_overall_b2b
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  locale,
  distinct_customer
FROM
  bi.customer_cohorts_overall_b2b
) as b
ON
  (a.first_order_month = b.first_order_month and a.locale = b.locale);

DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCohorts_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;


DROP TABLE IF EXISTS bi.clv_kpis;
CREATE TABLE bi.clv_kpis as 
SELECT
  first_order_date,
  acquisition_channel,
  CAST('CLV30' as text) as kpi,
  SUM(Revenue30) as Revenue,
  COUNT(DISTINCT(contact__c)) as distinct_customer
FROM(
SELECT
  contact__c,
  acquisition_channel,
  first_order_date,
  SUM(CASE WHEN order_date between first_order_date and first_order_date+30 THEN PPH__c*Order_duration__c ELSE 0 END) as Revenue30
FROM
  bi.allCustomerCohort 
WHERE
  status = 'INVOICED'
  and first_order_date < current_date-30
GROUP bY
    contact__c,
  acquisition_channel,
  first_order_date) as b
GROUP BY
  first_order_Date,
  acquisition_channel;
  
INSERT INTO bi.clv_kpis
SELECT
  first_order_date,
  acquisition_channel,
  CAST('CLV90' as text) as kpi,
  SUM(Revenue30) as Revenue,
  COUNT(DISTINCT(contact__c)) as distinct_customer
FROM(
SELECT
  contact__c,
  acquisition_channel,
  first_order_date,
  SUM(CASE WHEN order_date between first_order_date and first_order_date+90 THEN PPH__c*Order_duration__c ELSE 0 END) as Revenue30
FROM
  bi.allCustomerCohort 
WHERE
  status = 'INVOICED'
  and first_order_date < current_date-90
GROUP bY
    contact__c,
  acquisition_channel,
  first_order_date) as b
GROUP BY
  first_order_Date,
  acquisition_channel;


DROP TABLE IF EXISTS bi.clv_kpis;
CREATE TABLE bi.clv_kpis as 
SELECT
  first_order_date,
  acquisition_channel,
  CAST('CLV30' as text) as kpi,
  SUM(Revenue30) as Revenue,
  COUNT(DISTINCT(contact__c)) as distinct_customer
FROM(
SELECT
  contact__c,
  acquisition_channel,
  first_order_date,
  SUM(CASE WHEN order_date between first_order_date and first_order_date+30 THEN PPH__c*Order_duration__c ELSE 0 END) as Revenue30
FROM
  bi.allCustomerCohort 
WHERE
  status = 'INVOICED'
  and first_order_date < current_date-30
GROUP bY
    contact__c,
  acquisition_channel,
  first_order_date) as b
GROUP BY
  first_order_Date,
  acquisition_channel;
  
INSERT INTO bi.clv_kpis
SELECT
  first_order_date,
  acquisition_channel,
  CAST('CLV90' as text) as kpi,
  SUM(Revenue30) as Revenue,
  COUNT(DISTINCT(contact__c)) as distinct_customer
FROM(
SELECT
  contact__c,
  acquisition_channel,
  first_order_date,
  SUM(CASE WHEN order_date between first_order_date and first_order_date+90 THEN PPH__c*Order_duration__c ELSE 0 END) as Revenue30
FROM
  bi.allCustomerCohort 
WHERE
  status = 'INVOICED'
  and first_order_date < current_date-90
GROUP bY
    contact__c,
  acquisition_channel,
  first_order_date) as b
GROUP BY
  first_order_Date,
  acquisition_channel;

-- Recurrent Cohorts Summary

DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
  first_order_month,
  order_month,
  min(start_Date) as order_date,
  min(first_order_Date) as start_date,
  city,
  acquisition_channel,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer,
  SUM(Order_Duration__c) as Hours,
  SUM(GMV_eur) as GMV
FROM
  bi.RecurringCustomerCohort
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  city,
  acquisition_channel;

DROP TABLE IF EXISTS bi.customer_cohorts_recurrent;
CREATE TABLE bi.customer_cohorts_recurrent as   
SELECT
  a.first_order_month as cohort,
  a.start_date::date as start_date,
  b.order_month,
  b.order_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.city,
  a.acquisition_channel,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer,
  b.hours,
  GMV
FROM
(SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer
FROM
  bi.customer_cohorts_exp
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer,
  Hours,
  GMV
FROM
  bi.customer_cohorts_exp
) as b
ON
  (a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel);

-- Recurrent B2B Cohorts



DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts_b2b;
CREATE TABLE bi.AllOrders_tableaucohorts_b2b as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 contact__c,
 Order_Id__c,
 CAST(Effectivedate as Date) as Start_date,
 LEFT(t1.locale__c,2) as locale,
 Recurrency__c,
 status,
 order_duration__c,
 gmv_eur,
 marketing_channel,
 type as ordertype,
 t1.payment_method__c as payment_method,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
JOIn
  Salesforce.Opportunity t2
 ON
  (t1.opportunityid = t2.sfid)
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and order_type = '2'
  and contact__c is not null
  and stagename != 'IRREGULAR'
ORDER BY
  contact__c,
  CAST(Effectivedate as Date);

DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts_b2b;
CREATE TABLE bi.RecurringCohortsCustomers_tableaucohorts_b2b(acquisition_date varchar(20) DEFAULT NULL,  city varchar(55) DEFAULT null, OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,  Start_Date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, order_duration__c integer default null, gmv_eur integer default null,acquisition_channel varchar(40) NOT NULL, ordertype varchar NULL, locale varchar NULL, payment_method varchar(50) Default NULL, funnel varchar(50) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.RecurringCohortsCustomers_tableaucohorts_b2b(acquisition_date,city,OrderNo,contact__c,Order_Id__c,Start_Date,Recurrency__c,status,order_duration__c,gmv_eur,acquisition_channel,ordertype,locale,payment_method,funnel)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Effectivedate as Date) as Start_Date,
  Recurrency__c,
  status,
  order_duration__c,
  gmv_eur,
  marketing_channel,
  type,
  LEFT(t1.locale__c,2),
  t1.payment_method__c as payment_method,
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
 JOIn
  Salesforce.Opportunity t2
ON
  (t1.opportunityid = t2.sfid)
WHERE
  t1.test__c = '0'
  and order_type = '2'
  and Recurrency__c > 6
  and contact__c is not null
  and t1.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
  and stagename != 'IRREGULAR'
ORDER BY
  contact__c,
  Cast(Order_Creation__c as Date);

DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts_b2b;
CREATE TABLE bi.RecurringCustomer_tableaucohorts_b2b as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  acquisition_channel,
  ordertype,
  locale,
  payment_method,
  funnel as acquisition_funnel
FROM
  bi.RecurringCohortsCustomers_tableaucohorts_b2b
WHERE
  OrderNo = 1;

DROP TABLE IF EXISTS bi.RecurringCustomerCohort_b2b;
CREATE TABLE bi.RecurringCustomerCohort_b2b as 
SELECT
  t1.Contact__c,
  city,
  first_order_date::date as first_order_date,
  start_date::date orderdate,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(Start_date as date),'YYYY-MM') as Order_Month,
  status,
  order_duration__c,
  gmv_eur,
  acquisition_channel,
  t1.ordertype,
  t1.locale,
  t1.payment_method,
  acquisition_funnel
FROM
  bi.RecurringCustomer_tableaucohorts_b2b t1
JOIN
  bi.AllOrders_tableaucohorts_b2b t2
on
  (t1.Contact__c = t2.Contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(Start_Date as date),'YYYY-MM'));


DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts_b2b;
DROP TABLE IF EXISTS bi.RecurringCohorts_tableaucohorts_b2b;
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts_b2b;
DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts_b2b;



DROP TABLE IF EXISTS bi.saas_kpis;
CREATE TABLE bi.saas_kpis as 
SELECT
  LEFT(city,2) as locale,
  city as city,
  Order_Month as Month,
    CASE 
  WHEN Recurrency between 1 and 7 THEN CAST('7' as varchar)
  WHEN Recurrency between 8 and 14 THEN CAST('14' as varchar)
  WHEN Recurrency > 27 THEN CAST('28' as varchar)
  ELSE CAST(Recurrency as varchar) END as breakdown,
  CAST('New Recurrent Customer' as varchar) as kpi,
  SUM(CASE WHEN returning_month = 0 THEN Returning_Customer ELSE 0 END) as value
FROM
  bi.customer_cohorts_recurrent_aggr
GROUP BY
  locale,
  city,
  breakdown,
  Month;

INSERT INTO bi.saas_kpis  
SELECT
  LEFT(city,2) as locale,
  city as city,
  Order_Month as Month,
  CASE 
  WHEN Recurrency between 1 and 7 THEN '7'
  WHEN Recurrency between 8 and 14 THEN '14'
  WHEN Recurrency > 27 THEN '28'
  ELSE Recurrency END as breakdown,
  CAST('Total Recurrent Customer EOM' as varchar) as kpi,
  SUM(Returning_Customer) as value  
FROM
  bi.customer_cohorts_recurrent_aggr
GROUP BY
  locale,
  city,
  breakdown,
  Month;
  
INSERT INTO bi.saas_kpis  
SELECT
  LEFT(city,2) as locale,
  city as city,
  TO_CHAR(mindate::date + INTERVAL '1 Month','YYYY-MM') as Month,
    CASE 
  WHEN Recurrency between 1 and 7 THEN '7' 
  WHEN Recurrency between 8 and 14 THEN '14'
  WHEN Recurrency > 27 THEN '28'
  ELSE Recurrency END as breakdown,
  CAST('Total Recurrent Customer BOM' as varchar) as kpi,
  SUM(Returning_Customer) as value  
FROM
  bi.customer_cohorts_recurrent_aggr
GROUP BY
  locale,
  city,
  breakdown,
  Month;  


INSERT INTO bi.saas_kpis  
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
  '-' as breakdown,
  CAST('Gross Recurrent Revenue' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c > '0'
GROUP BY
  locale,
  city_new,
  breakdown,
  month;
  
INSERT INTO bi.saas_kpis
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Gross Recurrent Revenue 7d' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 1 and 7
GROUP BY
  locale,
  city_new,
  breakdown,
  month;

INSERT INTO bi.saas_kpis  
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Gross Recurrent Revenue 14d' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 8 and 14
GROUP BY
  locale,
  breakdown,
  city_new,
  month;

INSERT INTO bi.saas_kpis
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Gross Recurrent Revenue 28d' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 15 and 28
GROUP BY
  locale,
  city_new,
  breakdown,
  month;
  
INSERT INTO bi.saas_kpis  
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
GROUP BY
  locale,
  breakdown,
  city_new,
  month;
  
INSERT INTO bi.saas_kpis
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer 7d' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 1 and 7
GROUP BY
  locale,
  city_new,
  breakdown,
  month;

INSERT INTO bi.saas_kpis  
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer 14d' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 8 and 14
GROUP BY
  locale,
  city_new,
  breakdown,
  month;

INSERT INTO bi.saas_kpis
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer 28d' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 15 and 28
GROUP BY
  locale,
  city_new,
  breakdown,
  month;

INSERT INTO bi.saas_kpis  
SELECT
  UPPER(LEFT(Locale__c,2)) as locale,
        case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city_new,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Trial Revenues' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c = '0'
GROUP BY
  locale,
  city_new,
  breakdown,
  month;

DROP TABLE IF EXISTS bi.TEMP_acquisition_list;
CREATE TABLE bi.TEMP_acquisition_list AS

  SELECT DISTINCT

    o.effectivedate::date as acquisition_date,
    o.customer_id__c::text as customer_id,
    LEFT(o.locale__c,2)::text as locale,
    o.polygon::text as polygon,
    o.marketing_channel::text as acquisition_channel,
    o.recurrency__c::text as acquisition_recurrency,
    CASE WHEN (o.voucher__c <> '' AND o.voucher__c IS NOT NULL AND o.voucher__c <> '0') THEN o.voucher__c ELSE 'No Voucher' END as acquisition_voucher

    FROM bi.orders o
    JOIN salesforce.contact c
    ON (o.contact__c = c.sfid)

    WHERE o.test__c = '0'
      AND o.order_type = '1'
      AND o.acquisition_new_customer__c = '1'
      AND o.status = 'INVOICED'
      AND o.effectivedate::date <= (current_date - interval '60 days')
      AND o.order_creation__c::date = c.createddate::date

    ORDER BY acquisition_date desc, locale asc, polygon asc, acquisition_channel asc, acquisition_recurrency asc, acquisition_voucher asc

;

CREATE INDEX cust_id_index ON bi.TEMP_acquisition_list(customer_id);

DROP TABLE IF EXISTS bi.retention_data;
CREATE TABLE bi.retention_data AS

  SELECT
    a.acquisition_date,
    a.customer_id,
    a.locale,
    a.polygon,
    a.acquisition_channel,
    a.acquisition_recurrency,
    a.acquisition_voucher, --Replace days by days
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '31 days') and (a.acquisition_date + interval '60 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m1,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '61 days') and (a.acquisition_date + interval '90 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m2,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '91 days') and (a.acquisition_date + interval '120 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m3,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '121 days') and (a.acquisition_date + interval '150 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m4,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '151 days') and (a.acquisition_date + interval '180 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m5,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '181 days') and (a.acquisition_date + interval '210 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m6,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '211 days') and (a.acquisition_date + interval '240 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m7,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '241 days') and (a.acquisition_date + interval '270 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m8,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '271 days') and (a.acquisition_date + interval '300 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m9,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '301 days') and (a.acquisition_date + interval '330 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m10,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '331 days') and (a.acquisition_date + interval '360 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m11,
    CASE WHEN (SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '361 days') and (a.acquisition_date + interval '390 days') THEN 1 ELSE 0 END) > 0) THEN 1 ELSE 0 END as rebooking_m12,

    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '31 days') and (a.acquisition_date + interval '60 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m1,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '61 days') and (a.acquisition_date + interval '90 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m2,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '91 days') and (a.acquisition_date + interval '120 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m3,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '121 days') and (a.acquisition_date + interval '150 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m4,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '151 days') and (a.acquisition_date + interval '180 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m5,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '181 days') and (a.acquisition_date + interval '210 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m6,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '211 days') and (a.acquisition_date + interval '240 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m7,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '241 days') and (a.acquisition_date + interval '270 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m8,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '271 days') and (a.acquisition_date + interval '300 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m9,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '301 days') and (a.acquisition_date + interval '330 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m10,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '331 days') and (a.acquisition_date + interval '360 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m11,
    SUM(CASE WHEN o.effectivedate between (a.acquisition_date + interval '361 days') and (a.acquisition_date + interval '390 days') THEN o.order_duration__c ELSE 0 END) as invoiced_hours_m12

  FROM bi.TEMP_acquisition_list a

  LEFT JOIN bi.orders o 
    ON (a.customer_id = o.customer_id__c
    AND o.test__c = '0'
    AND o.order_type = '1'
    AND o.acquisition_new_customer__c = '0'
    AND o.effectivedate < (current_date)
    AND o.status IN ('INVOICED'))


  GROUP BY
    a.acquisition_date, a.customer_id, a.locale, a.polygon, a.acquisition_channel, a.acquisition_recurrency, a.acquisition_voucher

  ORDER BY acquisition_date desc, locale asc, polygon asc, acquisition_channel asc, acquisition_recurrency asc, acquisition_voucher asc

;


DROP TABLE IF EXISTS bi.TEMP_acquisition_list;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 55_churns_analysis

  DROP TABLE IF EXISTS bi.temp_orders_terminated;
  CREATE TABLE bi.temp_orders_terminated AS

    SELECT
      created_at::date as date,
      event_name,
      (order_Json->>'Order_Start__c') as orderdate,
      order_Json->>'Locale__c' as Locale__c,
      order_Json->>'Order_Id__c' as Order_id,
      order_Json->>'Contact__c' as customer_id,
      order_Json->>'Recurrency__c' as recurrency

    FROM
      events.sodium


    WHERE
      (event_name in ('Order Event:CANCELLED TERMINATED'))
      and created_at >= '2016-05-01'
      and order_Json->>'Recurrency__c' > '0'
  ;

  DROP TABLE IF EXISTS bi.temp_orders_terminated_2;
  CREATE TABLE bi.temp_orders_terminated_2 AS

    SELECT

      t1.date,
      t1.event_name,
      t1.orderdate,
      t1.locale__c,
      t1.order_id,
      t1.customer_id,
      t1.recurrency,
      t2.polygon,
      t2.error_note__c

    FROM bi.temp_orders_terminated t1

      LEFT JOIN bi.orders t2 
        ON t1.order_id = t2.order_id__c

  ;

  DROP TABLE IF EXISTS bi.temp_orders_NMP;
  CREATE TABLE bi.temp_orders_NMP AS

    SELECT
      created_at::date as date,
      event_name,
      (order_Json->>'Order_Start__c') as orderdate,
      order_Json->>'Locale__c' as Locale__c,
      order_Json->>'Order_Id__c' as Order_id,
      order_Json->>'Contact__c' as customer_id,
      order_Json->>'Recurrency__c' as recurrency

    FROM
      events.sodium

    WHERE
      (event_name in ('Order Event:CANCELLED-NO-MANPOWER'))
      and created_at >= '2016-04-01'
      and order_Json->>'Recurrency__c' > '0'

  ;

  DROP TABLE IF EXISTS bi.temp_badrate_orders;
  CREATE TABLE bi.temp_badrate_orders AS

    SELECT
      order_id__c as order_id,
      effectivedate::date as orderdate,
      order_start__c::date as date,
      customer_id__c as customer_id

    FROM
      bi.orders

    WHERE
      rating_service__c in ('1', '2', '3')
      and effectivedate::date >= '2016-04-01'
      and recurrency__c > '0'
      and test__c = '0'
      and status in ('INVOICED')

  ;


  DROP TABLE IF EXISTS bi.churns_motivations;
  CREATE TABLE bi.churns_motivations AS

    SELECT
      t1.date,
      t1.event_name,
      t1.orderdate,
      LEFT(t1.locale__c,2) as locale,
      t1.polygon,
      t1.order_id,
      t1.error_note__c,
      COUNT(DISTINCT(t2.order_id)) as Count_NMP_L28D,
      COUNT(DISTINCT(t3.order_id)) as Count_Badrate_L28D

    FROM bi.temp_orders_terminated_2 t1
      LEFT JOIN bi.temp_orders_NMP t2
        ON t1.customer_id = t2.customer_id 
        AND (t2.date) >= (t1.date - interval '28 days')
      LEFT JOIN bi.temp_badrate_orders t3
        ON t1.customer_id = t3.customer_id
        AND (t3.orderdate) >= (t1.date - interval '28 days')

    GROUP BY t1.date, t1.event_name, t1.orderdate, LEFT(t1.locale__c,2), t1.polygon, t1.order_id, t1.error_note__c

    ORDER BY t1.date desc

  ;

  DROP TABLE IF EXISTS bi.temp_orders_terminated;
  DROP TABLE IF EXISTS bi.temp_orders_NMP;
  DROP TABLE IF EXISTS bi.temp_badrate_orders;

-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'