DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_KPIs_Summary(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_KPIs_Summary';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------	


DROP TABLE IF EXISTS bi.SummaryKPIs;
CREATE TABLE bi.SummaryKPIs AS

SELECT

	CAST(0 as text) as month,
	CAST(0 as text) as year,
	CAST('2017-01-01' as date) as mindate,
	CAST(0 as text) as city,
	CAST(0 as text) as kpi,
	CAST(0 as text) as breakdown,
	CAST(0 as numeric) as value;


DELETE FROM bi.SummaryKPIs WHERE month = '0';



----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------	


-- Extracting the number of Active Cleaners on a monthly basis per city


DROP TABLE IF EXISTS bi.SummaryKPIs_temp1;
CREATE TABLE bi.SummaryKPIs_temp1 AS
   
SELECT
	   
	EXTRACT(MONTH FROM t2.Cleaningdate)::text as month,
	EXTRACT(YEAR FROM t2.Cleaningdate)::text as year,
	MIN(t2.Cleaningdate) as mindate,
	t2.city::text as city,
	'Active_cleaners'::text as kpi,
	'-'::text as breakdown,
	COUNT(DISTINCT t2.professional) as value
				
FROM
			
	(SELECT
				
		a.polygon as city,
		o.sfid as sfid,
		a.professional__c as professional,
		o.hr_contract_start__c::date as StartDate,
		o.hr_contract_end__c::date as EndDate,
		a.effectivedate as CleaningDate,
		a.status as Status,
		SUM(a.order_duration__c) as Hours
					 
	FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)
			
	WHERE
					
		o.test__c = '0'
		AND a.test__c = '0'
		AND effectivedate >= '2016-01-01'
		AND a.type IN ('cleaning-b2c','cleaning-b2b', 'cleaning-b2b;cleaning-b2c','cleaning-b2c;cleaning-b2b')
		AND a.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER')
		AND (o.type__c LIKE '%cleaning-b2c%' OR o.type__c LIKE '%cleaning-b2b%')
		AND LEFT(a.locale__c,2) IN ('de','nl', 'ch', 'at')
		AND a.polygon IS NOT NULL
					    
	GROUP BY
				
		o.sfid,
		professional,
		a.polygon,
		StartDate,
		Cleaningdate,
		Status,
		EndDate) as t2
			
GROUP BY
			
	month,
	year,
	city
			   
ORDER BY 
			
	mindate desc, 
	city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------			


-- Extracting the number of Active Cleaners on a monthly basis per country


DROP TABLE IF EXISTS bi.SummaryKPIs_temp2;
CREATE TABLE bi.SummaryKPIs_temp2 AS

SELECT
		   
	EXTRACT(MONTH FROM t2.Cleaningdate)::text as month,
	EXTRACT(YEAR FROM t2.Cleaningdate)::text as year,
	MIN(t2.Cleaningdate) as mindate,
	UPPER(t2.city::text) as city,
	'Active_cleaners'::text as kpi,
	'-'::text as breakdown,
	COUNT(DISTINCT(t2.professional)) as value
				
FROM
			
	(SELECT
				
		LEFT(a.polygon, 2) as city,
		o.sfid as sfid,
		a.professional__c as professional,
		o.hr_contract_start__c::date as StartDate,
		o.hr_contract_end__c::date as EndDate,
		a.effectivedate as CleaningDate,
		a.status as Status,
		SUM(a.order_duration__c) as Hours
					 
   FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)
			
	WHERE
					
		o.test__c = '0'
		AND a.test__c = '0'
		AND effectivedate >= '2016-01-01'
		AND a.type IN ('cleaning-b2c','cleaning-b2b', 'cleaning-b2b;cleaning-b2c','cleaning-b2c;cleaning-b2b')
		AND a.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER')
		AND (o.type__c LIKE '%cleaning-b2c%' OR o.type__c LIKE '%cleaning-b2b%')
		AND LEFT(a.locale__c,2) IN ('de','nl', 'ch', 'at')
		AND a.polygon IS NOT NULL
					    
	GROUP BY
				
		o.sfid,
		professional,
		a.polygon,
		StartDate,
		Cleaningdate,
		Status,
		EndDate) as t2
			
GROUP BY
			
	month,
	year,
	city
			   
ORDER BY 
			
	mindate desc, 
	city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------		

-- Extracting the CAC on a monthly basis per city


DROP TABLE IF EXISTS bi.SummaryKPIs_temp3;
CREATE TABLE bi.SummaryKPIs_temp3 AS


SELECT

	EXTRACT(MONTH from t1.mindate)::text as month,
	EXTRACT(YEAR from t1.mindate)::text as year,
	t1.mindate as mindate,
	t1.polygon1 as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	ROUND((CASE WHEN t1.numero > 0 THEN t1.sum/t1.numero ELSE t1.sum END)::numeric, 1) as value
		
	
FROM
	
	(SELECT
	
		c.polygon as polygon1,
		CAST('CAC' as text) as kpi,
		EXTRACT(MONTH from c.date)::text as month,
		EXTRACT(YEAR from c.date)::text as year,
		MIN(c.date) as mindate,
		SUM(c.onboardings) as numero,
		SUM(c.classifieds_costs+c.sem_costs+c.fb_costs) as sum
							
	FROM
				
		bi.cleaners_costsperonboarding_polygon c
					
	WHERE 
				
		c.polygon NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
			
	GROUP BY
		
		polygon1,
		year,
		month
			
	ORDER BY 
		
		year desc, 
		month desc) t1;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the CAC on a monthly basis per country


DROP TABLE IF EXISTS bi.SummaryKPIs_temp4;
CREATE TABLE bi.SummaryKPIs_temp4 AS

SELECT
		
	EXTRACT(MONTH from t1.mindate)::text as month,
	EXTRACT(YEAR from t1.mindate)::text as year,
	t1.mindate as mindate,
	UPPER(t1.polygon1) as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	ROUND((CASE WHEN t1.numero > 0 THEN t1.sum/t1.numero ELSE t1.sum END)::numeric, 1) as value
		
	
FROM
	
	(SELECT
	
		LEFT(c.polygon, 2) as polygon1,
		CAST('CAC' as text) as kpi,
		EXTRACT(MONTH from c.date)::text as month,
		EXTRACT(YEAR from c.date)::text as year,
		MIN(c.date) as mindate,
		SUM(c.onboardings) as numero,
		SUM(c.classifieds_costs+c.sem_costs+c.fb_costs) as sum
							
	FROM
				
		bi.cleaners_costsperonboarding_polygon c
					
	WHERE 
				
		c.polygon NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
			
	GROUP BY
		
		polygon1,
		year,
		month
			
	ORDER BY 
		
		year desc, 
		month desc, 
		mindate desc) t1;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the #Churn per city on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp5;
CREATE TABLE bi.SummaryKPIs_temp5 AS


SELECT
	  	
	EXTRACT(MONTH FROM date)::text as month, -- 
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	polygon as city,
	kpi,
	'-'::text as breakdown,
	SUM(value) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('Churn' as text) as kpi,
		a.delivery_areas__c as Polygon,
		MAX(CASE WHEN ((a.hr_contract_end__c >= month_start) AND (a.hr_contract_end__c <= month_end)) THEN 1 ELSE 0 END) as value,
		month_start as date
					
	FROM
			
		bi.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			EXTRACT(year from date),
			EXTRACT(month from date)) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('ch', 'de', 'nl', 'at')
		AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		AND a.hr_contract_end__c IS NOT NULL
		
	GROUP BY
			
		name1,
		sfid1,
		polygon,
		date			
				
	ORDER BY 
			
		a.delivery_areas__c asc) as subq 		
		
GROUP BY
	
	year,
	month,
	city,
	kpi
	  	
ORDER BY 
	
	year desc, 
	month desc, 
	city asc;



----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the #Churn per country on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp6;
CREATE TABLE bi.SummaryKPIs_temp6 AS


SELECT
  	
	EXTRACT(MONTH FROM date)::text as month, -- 
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	UPPER(polygon) as city,
	kpi,
	'-'::text as breakdown,
	SUM(value) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('Churn' as text) as kpi,
		LEFT(a.delivery_areas__c, 2) as Polygon,
		MAX(CASE WHEN ((a.hr_contract_end__c >= month_start) AND (a.hr_contract_end__c <= month_end)) THEN 1 ELSE 0 END) as value,
		month_start as date
					
	FROM
			
		bi.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			EXTRACT(year from date),
			EXTRACT(month from date)) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('ch', 'de', 'nl', 'at')
		AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		AND a.hr_contract_end__c IS NOT NULL
		
	GROUP BY
			
		name1,
		sfid1,
		Polygon,
		date
						
	ORDER BY 
			
		polygon asc) as subq 		
		
GROUP BY
	
	year,
	month,
	city,
	kpi
	  	
ORDER BY 
	
	year desc, 
	month desc, 
	city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the COP per city on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp7;
CREATE TABLE bi.SummaryKPIs_temp7 AS


SELECT

   EXTRACT(MONTH FROM date)::text as month,
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	polygon as city,
	kpi,
	'-'::text as breakdown,
	SUM(value) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('COP' as text) as kpi,
		a.delivery_areas__c as Polygon,
		SUM(CASE WHEN ((a.hr_contract_start__c <= month_end) AND ((a.hr_contract_end__c >= month_start) OR (a.hr_contract_end__c IS NULL))) THEN 1 ELSE 0 END) as value,
		month_start as date
					
	FROM
			
		bi.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			EXTRACT(year from date),
			EXTRACT(month from date)) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('ch', 'de', 'nl', 'at')
		-- AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		
	GROUP BY
			
		name1,
		sfid1,
		polygon,
		date
			
				
	ORDER BY 
			
		a.delivery_areas__c asc) as subq 
		
GROUP BY
	
	year,
	month,
	city,
	kpi
	  	
ORDER BY
	
	year desc, 
	month desc, 
	city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the COP per country on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp8;
CREATE TABLE bi.SummaryKPIs_temp8 AS


SELECT
    
   EXTRACT(MONTH FROM date)::text as month,
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	UPPER(polygon) as city,
	kpi,
	'-'::text as breakdown,
	SUM(value) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('COP' as text) as kpi,
		LEFT(a.delivery_areas__c, 2) as Polygon,
		SUM((CASE WHEN ((a.hr_contract_start__c <= month_end) AND ((a.hr_contract_end__c >= month_start) OR (a.hr_contract_end__c IS NULL))) THEN 1 ELSE 0 END)) as value,
		month_start as date
					
	FROM
			
		bi.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			EXTRACT(year from date),
			EXTRACT(month from date)) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('ch', 'de', 'nl', 'at')
		-- AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		
	GROUP BY
			
		name1,
		sfid1,
		polygon,
		date
						
	ORDER BY 
			
		polygon asc) as subq 
		
GROUP BY
	
	year,
	month,
	city,
	kpi
	  	
ORDER BY
	
	year desc, 
	month desc, 
	city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the CPL per city on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp9;
CREATE TABLE bi.SummaryKPIs_temp9 AS



SELECT

	EXTRACT(MONTH from t1.date)::text as month,
	EXTRACT(YEAR from t1.date)::text as year,
	t1.date as mindate,
	t1.polygon as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	ROUND((CASE WHEN t1.numero > 0 THEN t1.sum/t1.numero ELSE t1.sum END)::numeric, 1) as value
		
	
FROM
	
	(SELECT
	
		c.polygon as polygon,
		CAST('CPL' as text) as kpi,
		EXTRACT(MONTH from c.date)::text as month,
		EXTRACT(YEAR from c.date)::text as year,
		MIN(c.date) as date,
		SUM(c.leads) as numero,
		SUM(c.classifieds_costs+c.sem_costs+c.fb_costs) as sum
							
	FROM
				
		bi.cleaners_costsperonboarding_polygon c
					
	WHERE 
				
		c.polygon NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
			
	GROUP BY
		
		polygon,
		year,
		month
			
	ORDER BY 
		
		year desc, 
		month desc, 
		date desc) t1;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the CPL per country on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp10;
CREATE TABLE bi.SummaryKPIs_temp10 AS


SELECT

	EXTRACT(MONTH from t1.mindate1)::text as month,
	EXTRACT(YEAR from t1.mindate1)::text as year,
	t1.mindate1 as mindate,
	UPPER(t1.polygon1) as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	ROUND((CASE WHEN t1.numero > 0 THEN t1.total/t1.numero ELSE t1.total END)::numeric, 1) as value
		
	
FROM
	
	(SELECT
	
		LEFT(c.polygon, 2) as polygon1,
		CAST('CPL' as text) as kpi,
		EXTRACT(MONTH from c.date)::text as month,
		EXTRACT(YEAR from c.date)::text as year,
		MIN(c.date) as mindate1,
		SUM(c.leads) as numero,
		SUM(c.classifieds_costs+c.sem_costs+c.fb_costs) as total
							
	FROM
				
		bi.cleaners_costsperonboarding_polygon c
					
	WHERE 
				
		c.polygon NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
			
	GROUP BY
		
		polygon1,
		kpi,
		year,
		month
	
	ORDER BY 
		
		year desc, 
		month desc, 
		mindate1 desc) t1
		
GROUP BY

	month,
	year,
	mindate,
	city,
	kpi,
	breakdown,
	numero,
	total;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the #MiniJobbers per country on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp11;
CREATE TABLE bi.SummaryKPIs_temp11 AS


SELECT
	    
   EXTRACT(MONTH FROM date)::text as month,
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	UPPER(left(polygon,2)) as city,
	kpi,
	'-'::text as breakdown,
	SUM(VALUE) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('MiniJobbers' as text) as kpi,
		left(a.delivery_areas__c,2) as Polygon,
		SUM((CASE WHEN ((a.hr_contract_start__c <= month_end) AND ((a.hr_contract_end__c >= month_start) OR (a.hr_contract_end__c IS NULL))) THEN 1 ELSE 0 END)) as value,
		month_start as date
					
	FROM
			
		bi.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end,
			TO_CHAR(date,'YYYY-MM') as month
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			month) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('ch', 'de', 'nl', 'at')
		-- AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		AND a.hr_contract_weekly_hours_min__c < 11
		
	GROUP BY
			
		name1,
		sfid1,
		polygon,
		date				
		) as subq 
		
GROUP BY
	
	city,
	kpi,
	year,
	month
	  	
ORDER BY
	
	mindate desc, 
	city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the #MiniJobbers per city on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp12;
CREATE TABLE bi.SummaryKPIs_temp12 AS


SELECT
    
   EXTRACT(MONTH FROM date)::text as month,
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	polygon as city,
	kpi,
	'-'::text as breakdown,
	SUM(VALUE) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('MiniJobbers' as text) as kpi,
		a.delivery_areas__c as Polygon,
		SUM((CASE WHEN ((a.hr_contract_start__c <= month_end) AND ((a.hr_contract_end__c >= month_start) OR (a.hr_contract_end__c IS NULL))) THEN 1 ELSE 0 END)) as value,
		month_start as date
					
	FROM
			
		bi.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end,
			TO_CHAR(date,'YYYY-MM') as month
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			month) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('de')
		-- AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		AND a.hr_contract_weekly_hours_min__c < 11
		
	GROUP BY
			
		name1,
		sfid1,
		polygon,
		date				
		) as subq 
		
		
GROUP BY
	
	city,
	kpi,
	year,
	month
	  	
ORDER BY
	
	year desc,
	month desc,
	mindate desc, 
	city asc;			


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the average Score of the month per city

DROP TABLE IF EXISTS bi.SummaryKPIs_temp13;
CREATE TABLE bi.SummaryKPIs_temp13 AS


SELECT

	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate1,
	m.delivery_area as city,
	CAST('Score' as text) as kpi,
	'-'::text as breakdown,
	ROUND(AVG(m.score_cleaners::numeric),1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	month,
	year,
	m.delivery_area
	
ORDER BY 
		
	mindate1 desc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the average Score of the month per country

DROP TABLE IF EXISTS bi.SummaryKPIs_temp14;
CREATE TABLE bi.SummaryKPIs_temp14 AS

SELECT

	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate1,
	UPPER(LEFT(m.delivery_area, 2)) as city,
	CAST('Score' as text) as kpi,
	'-'::text as breakdown,
	ROUND(AVG(m.score_cleaners::numeric),1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	month,
	year,
	city,
	kpi,
	breakdown
			
ORDER BY 
		
	year desc, month desc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the UR per city on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp15;
CREATE TABLE bi.SummaryKPIs_temp15 AS


SELECT

	EXTRACT(MONTH FROM t1.date)::text as month,
	EXTRACT(YEAR FROM t1.date)::text as year,
	min(t1.date::date) as mindate,
	t1.polygon as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	CASE WHEN SUM(contract_hours) > 0 THEN 
												 CASE WHEN SUM(worked_hours)<=SUM(contract_hours)
													   THEN SUM(worked_hours)/SUM(contract_hours) 
													   ELSE 1
												 END
		  ELSE 0
		  END as value
		  
FROM
	
	(SELECT
		
		m.delivery_area as polygon,
		m.name,
		CAST('UR' as text) as kpi,
		m.mindate as date,
		CASE WHEN m.worked_hours <= m.working_hours THEN m.worked_hours ELSE working_hours END as worked_hours,
		working_hours as contract_hours
							
	FROM
				
		bi.margin_per_cleaner m
					
	WHERE 
				
		m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
							
	GROUP BY 
				
		m.delivery_area,
		date,
		m.name,
		m.worked_hours,
		m.working_hours
					
	ORDER BY 
				
		date desc, polygon asc) t1
			
GROUP BY
	
	polygon,
	kpi,
	year,
	month
		
ORDER BY
	
		year desc, 
		month desc, 
		mindate desc, 
		city asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the UR per country on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp16;
CREATE TABLE bi.SummaryKPIs_temp16 AS

SELECT

	EXTRACT(MONTH FROM t1.date)::text as month,
	EXTRACT(YEAR FROM t1.date)::text as year,
	MIN(t1.date::date) as mindate,
	t1.polygon1 as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	CASE WHEN SUM(contract_hours) > 0 THEN 
												 CASE WHEN SUM(worked_hours)<=SUM(contract_hours)
													   THEN SUM(worked_hours)/SUM(contract_hours) 
													   ELSE 1
												 END
		  ELSE 0
		  END as value

FROM

	(SELECT
	
		UPPER(LEFT(m.delivery_area, 2)) as polygon1,
		m.name,
		CAST('UR' as text) as kpi,
		m.mindate as date,
		CASE WHEN m.worked_hours <= m.working_hours THEN m.worked_hours ELSE working_hours END as worked_hours,
		working_hours as contract_hours
						
	FROM
			
		bi.margin_per_cleaner m
				
	WHERE 
			
		m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
						
	GROUP BY 
			
		polygon1,
		date,
		m.name,
		m.worked_hours,
		m.working_hours
				
	ORDER BY 
			
		date desc, polygon1 asc) t1
		
GROUP BY

	city,
	kpi,
	year,
	month
	
ORDER BY

	year desc, month desc, mindate desc, city asc;	


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the worked hours per city on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp17;
CREATE TABLE bi.SummaryKPIs_temp17 AS

SELECT
	
	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate,
	m.delivery_area as city,
	CAST('Worked_hours' as text) as kpi,
	'-'::text as breakdown,
	ROUND(SUM(m.worked_hours)::numeric,1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	m.delivery_area,
	year,
	month
			
ORDER BY 
		
	mindate desc,
	city desc;
				

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


-- Extracting the worked hours per country on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp18;
CREATE TABLE bi.SummaryKPIs_temp18 AS



SELECT
	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate,
	UPPER(LEFT(m.delivery_area,2)) as city,
	CAST('Worked_hours' as text) as kpi,
	'-'::text as breakdown,
	ROUND(SUM(m.worked_hours)::numeric,1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	city,
	year,
	month
			
ORDER BY 
		
	mindate desc,
	city desc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


INSERT INTO bi.SummaryKPIs

(SELECT
	*
FROM
	bi.SummaryKPIs_temp1)	
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp2)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp3)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp4)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp5)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp6)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp7)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp8)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp9)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp10)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp11)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp12)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp13)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp14)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp15)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp16)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp17)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp18)
	
;



DROP TABLE IF EXISTS	bi.SummaryKPIs_temp1;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp2;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp3;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp4;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp5;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp6;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp7;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp8;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp9;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp10;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp11;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp12;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp13;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp14;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp15;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp16;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp17;
DROP TABLE IF EXISTS	bi.SummaryKPIs_temp18;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'			
			
					