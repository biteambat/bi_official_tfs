CREATE FUNCTION "daily$order_event"("crunchdate" DATE)
	RETURNS UNKNOWN
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT E''
$$ 
BEGIN

DROP TABLE IF EXISTS main.order_events;
CREATE TABLE main.order_events as

select
		  created_at
		, replace(event_name, 'Order Event:','') as order_event
		, order_json::json->>'Id' as Order_ID
from events.sodium
where substring(event_name,1,12) = 'Order Event:'

END;
$$