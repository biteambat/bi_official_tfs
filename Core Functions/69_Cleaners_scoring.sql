
DELIMITER //

CREATE OR REPLACE FUNCTION bi.daily$cleaners_scoring(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := ' bi.daily$cleaners_scoring';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
-------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.temp_cancels;
CREATE TABLE bi.temp_cancels AS

SELECT
      
   t1.Professional as sfid,
   ROUND((CASE WHEN (t1.Terminated = 0) THEN 2 ELSE (1/t1.Terminated::numeric) END), 2) as Terminated_coeff,
   ROUND((CASE WHEN (t1.Cancelled_professional = 0) THEN 2 ELSE (1/t1.Cancelled_professional::numeric) END), 2) as Cancelled_pro_coeff,
   ROUND((CASE WHEN (t1.Noshow_professional = 0) THEN 2 ELSE (1/t1.Noshow_professional::numeric) END), 2) as Noshow_professional_coeff
      
FROM      
      
   (SELECT
       
      o.professional__c as Professional,
      SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as Terminated,
      SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as Cancelled_professional,
      SUM(CASE WHEN o.status IN ('NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as Noshow_professional
         
   FROM bi.orders o 
        
   WHERE
       
      o.status IN ('INVOICED', 'CANCELLED TERMINATED', 'CANCELLED PROFESSIONAL', 'NOSHOW PROFESSIONAL')
      AND o.test__c = '0'
      AND o.effectivedate >= (current_date - 30)
      AND LEFT(o.locale__c, 2) IN ('de', 'nl', 'ch', 'at') 
        
   GROUP BY o.professional__c) as t1;
     
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
    
   
DROP TABLE IF EXISTS bi.temp_results;
CREATE TABLE bi.temp_results AS

SELECT
     
   a.sfid as sfid,
   a.name as cleaners,
   a.delivery_areas__c as polygons,
   Margin_cleaner.rating as rating,
   Margin_cleaner.global_gpm as global_gpm,
   Margin_cleaner.UR as ur,
          
   ROUND((CASE WHEN (Margin_cleaner.sickness != 0) THEN (1/Margin_cleaner.sickness::numeric) ELSE 2 END), 2) as sickness_coeff,
   Margin_cleaner.worked_hours        
      
FROM bi.account a       
      
LEFT JOIN
     
   (SELECT
      
      m.professional__c as sfid,
      m.name,
      (CASE WHEN m.rating IS NOT NULL THEN m.rating ELSE 2.5 END) as rating,
      m.delivery_area,
      SUM(m.sickness) as sickness,
      SUM(m.worked_hours) as worked_hours,
      SUM(m.gp) as GP,
      SUM(m.revenue) as revenue,
      CASE WHEN sum(m.revenue) > 0 THEN SUM(m.gp)/sum(m.revenue) ELSE 0 END as global_gpm,
      CASE WHEN LEFT(m.delivery_area, 2) IN ('at') THEN 4.3*12 ELSE SUM(m.monthly_hours) END as expected_worked_hours,
         
      CASE WHEN LEFT(m.delivery_area, 2) IN ('at')
               THEN (CASE WHEN (SUM(m.worked_hours)/(4.3*12)) < 2 THEN (SUM(m.worked_hours)/(4.3*12)) ELSE 2 END) 
            ELSE
               CASE WHEN LEFT(m.delivery_area, 2) IN ('nl') 
                         THEN (CASE WHEN SUM(m.worked_hours)/(SUM(m.monthly_hours)*12/2) < 2 THEN SUM(m.worked_hours)/(SUM(m.monthly_hours)*12/2) ELSE 2 END) 
                    WHEN LEFT(m.delivery_area, 2) IN ('ch') 
                         THEN (CASE WHEN SUM(m.worked_hours)/(SUM(m.monthly_hours)*12) < 2 THEN SUM(m.worked_hours)/(SUM(m.monthly_hours)*12) ELSE 2 END)  
                    ELSE (CASE WHEN SUM(m.worked_hours)/SUM(m.monthly_hours) < 2 THEN SUM(m.worked_hours)/SUM(m.monthly_hours) ELSE 2 END) 
                    END
          END as UR

        
   FROM bi.margin_per_cleaner m
       
   WHERE 
      
      CAST(RIGHT(m.year_month,2) as numeric) = EXTRACT(month from current_date) 
      AND CAST(LEFT(m.year_month,4) as numeric) = EXTRACT(year from current_date)
       
   GROUP BY 
      
      m.professional__c,
      m.name,
      m.rating,
      m.delivery_area) as Margin_cleaner
      
ON
     
   (Margin_cleaner.sfid = a.sfid)
      
WHERE
     
   a.status__c IN ('BETA', 'ACTIVE')
   AND a.test__c = '0'
   AND LEFT(a.locale__c, 2) IN ('de', 'nl', 'ch', 'at')
   AND a.delivery_areas__c IS NOT NULL
   AND (a.hr_contract_start__c <= (current_date - interval '30 days') OR a.hr_contract_start__c IS NULL) 
   AND Margin_cleaner.UR IS NOT NULL
      
GROUP BY
     
   a.sfid,
   a.delivery_areas__c,
   a.name,
   Margin_cleaner.rating,
   Margin_cleaner.global_gpm,
   Margin_cleaner.UR,
   Margin_cleaner.sickness,
   Margin_cleaner.worked_hours
      
ORDER BY 
   
    polygons
    ;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_KPI_Performance;
CREATE TABLE bi.temp_KPI_Performance AS

SELECT
      
   t1.sfid,
   t1.polygons,
   t1.cleaners,
   t1.rating,
   t1.global_gpm,
   t1.UR,
   t1.sickness_coeff,       
   t3.Terminated_coeff,
   t3.Cancelled_pro_coeff,
   t3.Noshow_professional_coeff
        
FROM bi.temp_results t1

LEFT JOIN bi.temp_cancels t3 ON t1.sfid = t3.sfid

ORDER BY t1.polygons asc 
    
    ;
       
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_KPI_Performance2;
CREATE TABLE bi.temp_KPI_Performance2 AS

SELECT
      
   t4.sfid,
   t4.polygons,
   t4.cleaners,
   t4.rating,
   t4.global_gpm,
   t4.UR,
      
   -- t4.Terminated_coeff as Terminated,
   (CASE WHEN (t4.Terminated_coeff >= (0.5)) THEN 5 ELSE (CASE WHEN (((0.33) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.5))) THEN 4 ELSE (CASE WHEN (((0.25) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.33))) THEN 3 ELSE (CASE WHEN (((0.2) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.25))) THEN 2 ELSE (CASE WHEN (t4.Terminated_coeff < (0.2)) THEN 1 ELSE NULL END) END) END) END) END) as Coeff_terminated, 

   -- t4.Cancelled_pro_coeff as CancelledPro,
   (CASE WHEN (t4.Cancelled_pro_coeff = 2) THEN 5 ELSE (CASE WHEN ((1 <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < 2)) THEN 4 ELSE (CASE WHEN (((0.5) <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < 1)) THEN 3 ELSE (CASE WHEN (((0.33) <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < (0.5))) THEN 2 ELSE (CASE WHEN (t4.Cancelled_pro_coeff < (0.33)) THEN 1 ELSE NULL END) END) END) END) END) as Coeff_cancelled_pro,

   -- t4.Noshow_professional_coeff as Noshow,
   (CASE WHEN (t4.Noshow_professional_coeff = 2) THEN 5 ELSE (CASE WHEN ((1 <= t4.Noshow_professional_coeff) AND (t4.Noshow_professional_coeff < 2)) THEN 4 ELSE (CASE WHEN (((0.5) <= t4.Noshow_professional_coeff) AND (t4.Noshow_professional_coeff < 1))  THEN 3 ELSE (CASE WHEN (((0.33) <= t4.Noshow_professional_coeff ) AND (t4.Noshow_professional_coeff  < (0.5)))  THEN 2 ELSE (CASE WHEN (t4.Noshow_professional_coeff < (0.33)) THEN 1 ELSE NULL END) END) END) END) END) as Coeff_noshow_professional,
       
   -- t4.sickness_coeff as Sickness,
   (CASE WHEN (t4.sickness_coeff = 2) THEN 5 ELSE (CASE WHEN (((1) <= t4.sickness_coeff) AND (t4.sickness_coeff < 2)) THEN 4 ELSE (CASE WHEN (((0.5) <= t4.sickness_coeff) AND (t4.sickness_coeff < (1))) THEN 3 ELSE (CASE WHEN (((0.25) <= t4.sickness_coeff) AND (t4.sickness_coeff < (0.5)))  THEN 2 ELSE (CASE WHEN (t4.sickness_coeff < (0.25)) THEN 1 ELSE NULL END) END) END) END) END) as Coeff_sickness             
        
FROM bi.temp_KPI_Performance t4

ORDER BY t4.polygons asc 
    ;
    
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.KPI_Performance;
CREATE TABLE bi.KPI_Performance AS

SELECT
     
   current_date::date as date_today,      
   t5.sfid,
   t5.polygons,
   t5.cleaners,
   t5.UR,
   t5.rating,
   -- t5.Terminated,
   -- t5.Coeff_terminated,
   -- t5.CancelledPro,
   t5.Coeff_cancelled_pro,
   -- t5.Noshow, 
   t5.Coeff_noshow_professional,
   -- t5.Sickness,
   t5.Coeff_sickness,
    
   ROUND(CAST(((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal)*t5.UR::numeric) as decimal), 1) as Score_cleaners,
   
   CASE WHEN ROUND( CAST( ((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal) *t5.UR::numeric) as decimal), 1) >= 5 THEN 'A' -- Here we define how the letter is assigned to the cleaners according to there score
         WHEN (ROUND( CAST( ((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal) *t5.UR::numeric) as decimal), 1) >= 3.5 AND ROUND( CAST( ((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal) *t5.UR::numeric) as decimal), 1) < 5) THEN 'B'
         WHEN (ROUND( CAST( ((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal) *t5.UR::numeric) as decimal), 1) >= 2.5 AND ROUND( CAST( ((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal) *t5.UR::numeric) as decimal), 1) < 3.5) THEN 'C1'
         WHEN ROUND( CAST( ((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness)/CAST(3 as decimal) *t5.UR::numeric) as decimal), 1) < 2.5 THEN 'C2'
         ELSE 'ERROR'
   END as category,
   
   CASE WHEN a.hr_contract_end__c > current_date 
        THEN 
            CASE WHEN date_part('month', age(current_date, a.hr_contract_start__c)) IS NULL 
            THEN 0 
            ELSE date_part('month', age(current_date, a.hr_contract_start__c)) 
            END
        ELSE 
            CASE WHEN date_part('month', age(a.hr_contract_end__c, a.hr_contract_start__c)) IS NULL 
            THEN 0 
            ELSE date_part('month', age(a.hr_contract_end__c, a.hr_contract_start__c)) 
            END
        END
        as existence
 
        
FROM bi.temp_KPI_Performance2 t5

LEFT JOIN

   Salesforce.account a
   
ON 

   (t5.sfid = a.sfid)

ORDER BY t5.polygons asc;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
    
DELETE FROM bi.History_performance_cleaners WHERE date = current_date::date;
   
INSERT INTO bi.History_performance_cleaners 
   
(SELECT
        
   t6.date_today as date, 
   t6.sfid,
   
   t6.polygons,
   t6.cleaners,
   t6.UR,
   t6.rating,
   t6.Coeff_cancelled_pro, 
   t6.Coeff_noshow_professional,
   t6.Coeff_sickness,
   
   t6.Score_cleaners,
   
   t6.category,
   
   t6.existence
    
        
FROM bi.kpi_performance t6

LEFT JOIN

   Salesforce.account a
   
ON 

   (t6.sfid = a.sfid)

WHERE 

   t6.score_cleaners IS NOT NULL);
   
   
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------   
   
DROP TABLE IF EXISTS bi.temp_cancels;   
DROP TABLE IF EXISTS bi.temp_results;   
DROP TABLE IF EXISTS bi.temp_KPI_Performance;   
DROP TABLE IF EXISTS bi.temp_KPI_Performance2;   
   

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'