DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$weekly_city_reporting (crunchdate date) RETURNS void AS 
$BODY$


DECLARE 
function_name varchar := 'bi.daily$weekly_city_reporting';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	DROP TABLE IF EXISTS bi.temp_group_retentioncitymarketing;
	CREATE TABLE bi.temp_group_retentioncitymarketing as

		SELECT

			extract(week from r.date) as weeknum,
			extract(year from r.date) as year,
			r.city as city,
			ROUND(CASE WHEN SUM(r.acquisitions) = '0' THEN NULL ELSE (SUM(r.cohort_return_rate*r.acquisitions)/SUM(r.acquisitions)) END::numeric,2) as return_rate_m1
			
		FROM bi.retention_city_marketing r 

		GROUP BY year, weeknum, city

		ORDER BY year desc, weeknum desc, city asc

	;

	-- Get the damn return rate p3 from bi.retention_marketing

	DROP TABLE IF EXISTS bi.temp_group_retentioncitymarketing_2;
	CREATE TABLE bi.temp_group_retentioncitymarketing_2 as

		SELECT

			r.date as date,
			r.city as city,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END) as acquisitions,
			MAX(r.cohort_return_rate) as cohort_return_rate

		FROM

			bi.retention_marketing r
				LEFT JOIN bi.orders o
					ON r.date = o.order_creation__c::timestamp::date
					AND LOWER(r.city) = o.delivery_area

		WHERE r.period = 'p3' 
			AND r.kpi_type = 'City analysis'
			AND o.test__c = '0'
			and order_type = '1'
			AND o.status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
			AND Extract(week from o.order_creation__c) < '53'

		GROUP BY r.date, r.city

		ORDER BY r.date desc, r.city asc

	;

	DROP TABLE IF EXISTS bi.temp_group_retentioncitymarketing_3;
	CREATE TABLE bi.temp_group_retentioncitymarketing_3 as

		SELECT

			Extract(week from date) as weeknum,
			Extract(year from date) as year,
			city as city,
			ROUND(CASE WHEN SUM(acquisitions) = '0' THEN NULL ELSE (SUM(cohort_return_rate*acquisitions)/SUM(acquisitions)) END::numeric,2) as return_rate_m3

		FROM bi.temp_group_retentioncitymarketing_2

		GROUP BY weeknum, year, city

		ORDER BY year desc, weeknum desc, city asc

	;

	-- FINAL : REPORTING TABLE

	DROP TABLE IF EXISTS bi.weekly_city_reporting;
	CREATE TABLE bi.weekly_city_reporting AS

		SELECT
			
			Extract(week from o.order_creation__c) as weeknum,
			Extract(year from o.order_creation__c) as year,
			o.delivery_area as city,
			ROUND(SUM(CASE WHEN (o.status) = 'INVOICED' THEN gmv_eur ELSE NULL END)::numeric,2) as Invoiced_GMV,
			ROUND(SUM(CASE WHEN (o.status) not like ('%CANCELLED%') and (o.status) not like ('INVOICED') THEN gmv_eur ELSE NULL END)::numeric,2) as Pending_GMV,
			MAX(r1.return_rate_m1) as RR_M1,
			MAX(r2.return_rate_m3) as RR_M3,
			ROUND(avg(CASE WHEN o.order_duration__c > 0 THEN (o.gmv_eur/o.order_duration__c::numeric) ELSE NULL END)::numeric,2) as pph_avg

		FROM bi.orders o 
			LEFT JOIN bi.temp_group_retentioncitymarketing r1 
				ON r1.year = Extract(year from o.order_creation__c)
				AND r1.weeknum = Extract(week from o.order_creation__c)
				AND LOWER(r1.city) = o.delivery_area
			LEFT JOIN bi.temp_group_retentioncitymarketing_3 r2
				ON r2.year = Extract(year from o.order_creation__c)
				AND r2.weeknum = Extract(week from o.order_creation__c)
				AND LOWER(r2.city) = o.delivery_area

		WHERE o.test__c = '0'
			AND o.type not like ('%222%')
			AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND Extract(week from o.order_creation__c) < '53'
			and order_type = '1'

		GROUP BY Extract(year from o.order_creation__c), Extract(week from o.order_creation__c), o.delivery_area

		ORDER BY year desc, weeknum desc, city asc

	;

	DROP TABLE IF EXISTS bi.temp_group_retentioncitymarketing;
	DROP TABLE IF EXISTS bi.temp_group_retentioncitymarketing_2;
	DROP TABLE IF EXISTS bi.temp_group_retentioncitymarketing_3;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;