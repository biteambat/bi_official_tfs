CREATE OR REPLACE FUNCTION bi.mfunc_gpm(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$mfunc_gpm';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	PERFORM * FROM bi.sfunc_gpm_at(crunchdate);
	PERFORM * FROM bi.sfunc_gpm_ch(crunchdate);
	PERFORM * FROM bi.sfunc_gpm_de(crunchdate);
	PERFORM * FROM bi.sfunc_gpm_nl(crunchdate);
	PERFORM * FROM bi.sfunc_gpm_subcontractors(crunchdate);

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'