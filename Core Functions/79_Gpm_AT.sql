DELIMITER //


CREATE OR REPLACE FUNCTION bi.sfunc_gpm_at(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_gpm_at';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


-- CLEANER STATS TEMP

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 

SELECT

	a.name,
	a.sfid,	
	a.status__c,
	a.sfid as professional__c,
	a.delivery_areas__c as delivery_areas,
	a.type__c,
	a.hr_contract_start__c::date as contract_start,
	a.hr_contract_end__c::date as contract_end,	
	a.hr_contract_weekly_hours_min__c as weekly_hours
	
FROM

   Salesforce.Account a
   
WHERE 
	a.test__c = '0' AND a.name NOT LIKE '%test%' AND a.name NOT LIKE '%Test%' AND a.name NOT LIKE '%TEST%'
	-- AND (a.type__c LIKE 'cleaning-b2c' OR (a.type__c LIKE '%cleaning-b2c;cleaning-b2b%') OR a.type__c LIKE 'cleaning-b2b')
	-- AND a.hr_contract_weekly_hours_min__c IS NOT NULL
	AND  a.test__c = '0'
	AND LEFT(a.delivery_areas__c,2) = 'at'

GROUP BY 

   a.name,
   a.sfid,
   a.status__c,
   professional__c,
   delivery_areas,
   a.type__c,
	contract_start,
	contract_end,
	weekly_hours;
	

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
CREATE TABLE bi.gmp_per_cleaner_temp as 

SELECT

	t1.professional__c,
	t1.pph__c,
	t1.polygon as delivery_area__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN gmv_eur
	         WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date
					  THEN CASE WHEN t1.pph__c < 13
					  				THEN gmv_eur*1.12
					  				ELSE gmv_eur*1.2
					  				END
				ELSE 0 END) as GMV,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN gmv_eur ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date
					  THEN CASE WHEN t1.pph__c < 13
					  				THEN gmv_eur*1.12
					  				ELSE gmv_eur*1.2
					  				END
				ELSE 0 END) as b2b_gmv,
				
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN order_Duration__c ELSE 0 END) + SUM(CASE WHEN (type = 'cleaning-b2b') AND Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2c_hours,
	SUM(CASE WHEN (type = 'cleaning-b2b') AND Status IN  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2b_hours,
	
	CASE WHEN t1.pph__c < 13 
		THEN 		
		((SUM(CASE 
	         WHEN (Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date) THEN gmv_eur*1.12 
				ELSE 0 END))/1.12)*0.2	
		ELSE 
		((SUM(CASE
	         WHEN (Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date) THEN gmv_eur*1.2
				ELSE 0 END))/1.2)*0.2
		END 
		as margin_b2b,			
		
CASE WHEN t1.pph__c < 13 
		THEN 
		((SUM(CASE 
				WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN gmv_eur
				ELSE 0 END))/1.12)*0.2
		ELSE 
		((SUM(CASE 
				WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN gmv_eur
				ELSE 0 END))/1.2)*0.2
		END 
		as margin_b2c
		
FROM

	bi.orders t1
	
WHERE

	t1.professional__c IS NOT NULL
	AND LEFT(t1.polygon,2) IN ('at')
	
GROUP BY

	t1.professional__c,
	t1.pph__c,
	t1.polygon,
	date;
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GMP PER CLEANER V1 TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
CREATE TABLE bi.gmp_per_cleaner_v1_temp as 

SELECT

	t1.name,
	t1.professional__c,
	t2.pph__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	t2.gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours,
	margin_b2c,
	margin_b2b
	
FROM

	bi.cleaner_Stats_temp t1
	
LEFT JOIN

	bi.gmp_per_cleaner_temp t2
	
ON

	(t1.professional__c = t2.professional__c);

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER V2

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_temp as 

SELECT 

	t2.*, 
	GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
	CASE
	WHEN year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
	WHEN to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
	ELSE calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
	END AS days_worked
	
FROM (

	SELECT	
	
	   to_char(date,'YYYY-MM') as Year_Month,
	   MIN(date) as mindate,
	   t1.name,
		professional__c,
		t1.pph__c,
		delivery_areas as delivery_area,
		DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
		contract_start,
		contract_end,
		type__c,
		SUM(hours) as worked_hours,
		CAST(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
		SUM(GMV) as GMV,
		CASE WHEN pph__c < 13 
			  THEN SUM(B2C_GMV/1.12) + SUM(B2B_GMV/1.12)
			  ELSE SUM(B2C_GMV/1.2) + SUM(B2B_GMV/1.2)
			  END as total_revenue,
		CASE WHEN pph__c < 13
		     THEN SUM(B2C_GMV/1.12)
		     ELSE SUM(B2C_GMV/1.2)
			  END as B2C_Revenue,
		CASE WHEN pph__c < 13
		     THEN SUM(B2B_GMV/1.12)
			  ELSE SUM(B2B_GMV/1.2)
			  END as B2B_Revenue,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(Hours)) ELSE 0 END as b2b_share,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(Hours)) ELSE 0 END as b2c_share,
		MAX(Weekly_hours) as weekly_hours,
		0 as monthly_hours,
		SUM(margin_b2c) as margin_b2c,
		SUM(margin_b2b) as margin_b2b
		
	FROM
	
		bi.gmp_per_cleaner_v1_temp t1
		
	WHERE
	
		date >= '2016-01-01'
		
	GROUP BY
	
	   Year_Month,
		professional__c,
		t1.pph__c,
		t1.name,
		contract_start,
		days_of_month,
		type__c,
		contract_end,
		delivery_area,
		calc_end
		) as t2;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
CREATE TABLE bi.holidays_cleaner_temp as

SELECT

	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
	
FROM

	salesforce.hr__c
	
WHERE

	type__c != 'unpaid';
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
CREATE TABLE bi.holidays_cleaner_2_temp as 

SELECT

	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
	
FROM

	bi.holidays_cleaner_temp,
	bi.orderdate
	
WHERE

	date > '2016-01-01'
	AND status__c in ('approved');

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
CREATE TABLE bi.holidays_cleaner_3_temp as 

SELECT

	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
	
FROM

	bi.holidays_cleaner_2_temp
	
WHERE

	weekday != '0'
	AND date < current_date
	
GROUP BY

	date,
	account__c;	

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
CREATE TABLE bi.holidays_cleaner_4_temp as 

SELECT

	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
	
FROM

	bi.holidays_cleaner_3_temp
	
GROUP BY

	Year_Month,
	account__c;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_2_temp as 

SELECT

	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
	
FROM

	bi.gpm_per_cleaner_v2_temp t1
	
LEFT JOIN

	 bi.holidays_cleaner_4_temp t2
	 
ON

	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month)
	
ORDER BY

	t1.year_month,
	t1.name;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
   professional__c,
   delivery_area,
   worked_hours,
   contract_start,
   contract_end,
   0 as monthly_hours,
   0 as weekly_hours,
   days_worked,
   days_of_month,
   0 as sickness,
   MIN(mindate) as mindate,
   0 as holiday,
   0 as absence_hours,
   0 as working_hours,
   0 as total_hours,
	(SUM(B2C_Revenue) + SUM(B2B_Revenue))-0.2*(SUM(B2C_Revenue) + SUM(B2B_Revenue)) as salary_payed,
	
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	
	SUM(margin_b2b) as b2b_gp,
   SUM(margin_b2c) as b2c_gp,
		  
	0.2*(SUM(B2C_Revenue) + SUM(B2B_Revenue))  as gp,
	-- SUM(margin_b2b) + SUM(margin_b2c) as new_gp,
	SUM(GMV) as GMV,
	(SUM(B2C_Revenue) + SUM(B2B_Revenue)) as Revenue
	
	-- CASE WHEN (SUM(B2C_Revenue) + SUM(B2B_Revenue)) > 0 THEN  0.2*(SUM(B2C_Revenue) + SUM(B2B_Revenue))/(SUM(B2C_Revenue) + SUM(B2B_Revenue)) ELSE 0 END as gpm,
	-- CASE WHEN (SUM(B2C_Revenue) + SUM(B2B_Revenue)) > 0 THEN SUM(margin_b2b) + SUM(margin_b2c)/(SUM(B2C_Revenue) + SUM(B2B_Revenue)) ELSE 0 END as new_gpm
	
	
FROM

     bi.gpm_per_cleaner_v2_2_temp
     
WHERE

    LEFT(delivery_area,2) = 'at'
    AND days_worked > '0'
    
GROUP BY
    year_month,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    days_worked,
    worked_hours;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.order_distribution_temp;
CREATE TABLE bi.order_distribution_temp as 

SELECT

	professional__c,
	to_char(effectivedate::date,'YYYY-MM') as Month,
	SUM(CASE WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and EXTRACT(HOUR FROM Order_End__c+ Interval '2 hours') < 13 THEN Order_Duration__c
				WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and extract(hour from order_end__c+ Interval '2 hours') > 13 THEN 13 - EXTRACT(HOUR FROM order_Start__c+ Interval '2 hours') ELSE 0 END) as Working_Hours_Morning,
	SUM(Order_Duration__c) as total_hours
	
FROM

	bi.orders
	
WHERE

	test__c = '0'
	and status = 'INVOICED'
	
GROUP BY

	professional__c,
	month;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;
CREATE TABLE bi.cleaner_ur_temp_temp as	

SELECT 

	sfid,
	SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as availability_morning,
	SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) as availability_total,
	CASE WHEN SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) = '0' THEN 0 ELSE CAST(SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as decimal)/SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) END as availability_Share

FROM

	bi.cleaner_availability_morning
	
GROUP BY

	sfid;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_at;
CREATE TABLE bi.gpm_at as

SELECT

	t2.name,
	t2.rating__c as rating,
	t5.Score_cleaners,
	t2.type__c,
	t1.*,
	CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
	0 as contract_morning_hours,
	t3.Working_Hours_Morning,
	availability_Share,
	0 as morning_ur,
	0 as contract_hours_afternoon,
	t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
	0 as afternoon_ur,
	0 as capped_hours_morning,
	0 as capped_hours_afternoon

FROM

	bi.gpm_per_cleaner_v3_temp_temp t1
	
LEFT JOIN
	Salesforce.Account t2	
ON
	(t1.professional__c = t2.sfid)
	
LEFT JOIN
	bi.order_distribution_temp t3	
ON
	(t1.professional__c = t3.professional__c and t1.year_month = t3.month)
	
LEFT JOIN
	bi.cleaner_ur_temp_temp t4	
ON
	(t1.professional__c = t4.sfid)
	
LEFT JOIN
	bi.KPI_Performance t5	
ON
	(t1.professional__c = t5.sfid);

	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;


-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'