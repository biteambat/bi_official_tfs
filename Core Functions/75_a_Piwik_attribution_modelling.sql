CREATE OR REPLACE FUNCTION bi.daily$attribution_modelling() RETURNS void AS

$BODY$
DECLARE 
function_name varchar := ' bi.daily$attribution_modelling';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
-------------------------------------------------------------------------------------------------------

DELETE FROM bi.temp_transaction_attribution;
INSERT INTO bi.temp_transaction_attribution

	SELECT * FROM bi.etl_piwik_visits
;

DROP TABLE IF EXISTS bi.attribution_modelling;
CREATE TABLE bi.attribution_modelling AS

	SELECT * FROM bi.trig_visits_transaction_matching()

;

-------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;

$BODY$ LANGUAGE 'plpgsql'