DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$matching_stats(crunchdate date) RETURNS VOID AS 
$BODY$

DECLARE 

function_name varchar := 'bi.daily$matching_stats';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


	DROP TABLE IF EXISTS bi.events_matching;

	CREATE TABLE bi.events_matching AS

		SELECT
			order_Json->>'Order_Id__c' as OrderID,
			event_name

		FROM
			events.sodium
			
		WHERE
			created_at::date >= (current_date - interval '70 days')
			and (event_name = 'Order Event:PENDING TO START' or event_name = 'Order Event:RESCHEDULED' or event_name = 'Order Event:CANCELLED-NO-MANPOWER'	or event_name = 'Order Event:CANCELLED SKIPPED'); 

		DROP TABLE IF EXISTS bi.matching_table;

		CREATE TABLE bi.matching_table AS

		SELECT
			t2.effectivedate::date as effectivedate,
			EXTRACT(DOW from t2.effectivedate::date) as dayOfWeek,
			t2.polygon as Polygon,
			t2.acquisition_new_customer__c as New_Old,
			SUM(CASE WHEN event_name = 'Order Event:PENDING TO START' THEN 1 ELSE 0 END) as NbPTS,
			SUM(CASE WHEN event_name = 'Order Event:RESCHEDULED' THEN 1 ELSE 0 END) as NbRS,
			SUM(CASE WHEN event_name = 'Order Event:CANCELLED-NO-MANPOWER' THEN 1 ELSE 0 END) as NbNMP,
			SUM(CASE WHEN event_name = 'Order Event:CANCELLED SKIPPED' THEN 1 ELSE 0 END) as NbCS

		FROM
			bi.events_matching t1 LEFT JOIN bi.orders t2 on (OrderID = t2.order_id__c)
			
		WHERE
			t2.effectivedate >= (current_date - interval '70 days')
			
		GROUP BY
			effectivedate,
			Polygon,
			New_Old
	;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
		
END;

$BODY$ LANGUAGE 'plpgsql'