CREATE OR REPLACE FUNCTION bi.daily$acmdashboard (crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$acmdashboard';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DROP TABLE IF EXISTS bi.acm_kpis;
CREATE TABLE bi.acm_kpis as 
SELECT
	effectivedate::date as date,
	polygon as city,
	CAST('Invoiced Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
	and effectivedate::date >= '2016-01-01'
	and test__c = '0'
GROUP BY
	date,
	polygon;

	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'DE-Total' as City,
	CAST('Invoiced Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
	and effectivedate::date >= '2016-01-01'
	and test__c = '0'
	and LEFT(locale__c,2) = 'de'
GROUP BY
	date;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'CH-Total' as City,
	CAST('Invoiced Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
	and effectivedate::date >= '2016-01-01'
	and test__c = '0'
	and LEFT(locale__c,2) = 'ch'
GROUP BY
	date;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'NL-Total' as City,
	CAST('Invoiced Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
	and effectivedate::date >= '2016-01-01'
	and test__c = '0'
	and LEFT(locale__c,2) = 'nl'
GROUP BY
	date;
	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	polygon as city,
	CAST('Demand Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status not in ('CANCELLED CUSTOMER', 'CANCELLED CUSTOMER SHORTTERM', 'CANCELLED FAKED','CANCELLED MISTAKE', 'CANCELLED NOT THERE YET', 'CANCELLED PAYMENT', 'CANCELLED SKIPPED', 'CANCELLED TERMINATED')
	and effectivedate::date >= '2016-01-01'
GROUP BY
	date,
	polygon;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'DE-Total' as City,
	CAST('Demand Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status not in ('CANCELLED CUSTOMER', 'CANCELLED CUSTOMER SHORTTERM', 'CANCELLED FAKED','CANCELLED MISTAKE', 'CANCELLED NOT THERE YET', 'CANCELLED PAYMENT', 'CANCELLED SKIPPED', 'CANCELLED TERMINATED')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'de'
GROUP BY
	date;


INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'CH-Total' as City,
	CAST('Demand Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status not in ('CANCELLED CUSTOMER', 'CANCELLED CUSTOMER SHORTTERM', 'CANCELLED FAKED','CANCELLED MISTAKE', 'CANCELLED NOT THERE YET', 'CANCELLED PAYMENT', 'CANCELLED SKIPPED', 'CANCELLED TERMINATED')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'ch'
GROUP BY
	date;
	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'NL-Total' as City,
	CAST('Demand Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status not in ('CANCELLED CUSTOMER', 'CANCELLED CUSTOMER SHORTTERM', 'CANCELLED FAKED','CANCELLED MISTAKE', 'CANCELLED NOT THERE YET', 'CANCELLED PAYMENT', 'CANCELLED SKIPPED', 'CANCELLED TERMINATED')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'nl'
GROUP BY
	date;	
		
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	polygon as city,
	CAST('NMP Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED NO MANPOWER')
	and effectivedate::date >= '2016-01-01'
GROUP BY
	date,
	polygon;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'DE-Total' as city,
	CAST('NMP Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED NO MANPOWER')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'de'
GROUP BY
	date;
	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'CH-Total' as city,
	CAST('NMP Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED NO MANPOWER')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'ch'
GROUP BY
	date;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'NL-Total' as city,
	CAST('NMP Hours' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED NO MANPOWER')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'nl'
GROUP BY
	date;
	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	polygon as city,
	CAST('Active Professionals' as varchar) as kpi,
	COUNT(DISTINCT(professional__c)) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
GROUP BY
	date,
	polygon;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'DE-Total' as City,
	'Professionals' kpi,
	sum(t1.value) as a
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Professional__c)) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and LEFT(locale__c,2) = 'de'
GROUP BY
	week) as t1
GROUP BY
		t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'NL-Total' as City,
	'Professionals' kpi,
	sum(t1.value) as a
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Professional__c)) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and left(locale__c,2) = 'nl'
GROUP BY
	week) as t1
GROUP BY
		t1.mindate;


INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'CH-Total' as City,
	'Professionals' kpi,
	sum(t1.value) as a
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Professional__c)) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and left(locale__c,2) = 'ch'
GROUP BY
	week) as t1
GROUP BY
		t1.mindate;
	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	polygon as city,
	CAST('Cancelled Professional' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED PROFESSIONAL')
	and effectivedate::date >= '2016-01-01'
GROUP BY
	date,
	polygon;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'DE-Total' as city,
	CAST('Cancelled Professional' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED PROFESSIONAL')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'de'
GROUP BY
	date;

	
INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'NL-Total' as city,
	CAST('Cancelled Professional' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED PROFESSIONAL')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'nl'
GROUP BY
	date;

INSERT INTO bi.acm_kpis 
SELECT
	effectivedate::date as date,
	'CH-Total' as city,
	CAST('Cancelled Professional' as varchar) as kpi,
	SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('CANCELLED PROFESSIONAL')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'ch'
GROUP BY
	date;
	
INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	t1.city,
	'Professionals' kpi,
	sum(t1.value) as a
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	polygon as city,
	COUNT(DISTINCT(Professional__c)) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
GROUP BY
	week,
	polygon) as t1
GROUP BY
		t1.mindate,
	t1.city;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'DE-Total' as kpi,
	'Professionals' kpi,
	sum(t1.value) as a
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Professional__c)) as value
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
		and LEFT(locale__c,2) = 'de'
GROUP BY
	week) as t1
GROUP BY
		t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	t1.city,
	'Weighted Professional Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	polygon as city,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
		and LEFT(locale__c,2) = 'de'
GROUP BY
	week,
	polygon) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	polygon as city,
	AVG(rating_Professional__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
GROUP BY
	week,
	polygon) as t2
ON
	(t1.week = t2.week and t1.city = t2.city)
GROUP BY
	t1.mindate,
	t1.city;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'DE-Total' as City,
	'Weighted Professional Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
	and LEFT(locale__c,2) = 'de'
	and type = '60'
GROUP BY
	week) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	AVG(rating_Professional__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
	and LEFT(locale__c,2) = 'de'
		and type = '60'
GROUP BY
	week) as t2
ON
	(t1.week = t2.week)
GROUP BY
	t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'CH-Total' as City,
	'Weighted Professional Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
	and LEFT(locale__c,2) = 'ch'
GROUP BY
	week) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	AVG(rating_Professional__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
	and LEFT(locale__c,2) = 'ch'
GROUP BY
	week) as t2
ON
	(t1.week = t2.week)
GROUP BY
	t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'NL-Total' as City,
	'Weighted Professional Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
	and LEFT(locale__c,2) = 'nl'
	and order_type = '1'
GROUP BY
	week) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	AVG(rating_Professional__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_professional__c is not null
	and LEFT(locale__c,2) = 'nl'
	and order_type = '1'
GROUP BY
	week) as t2
ON
	(t1.week = t2.week)
GROUP BY
	t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	t1.city,
	'Weighted Service Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	polygon as city,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and order_type = '1'
GROUP BY
	week,
	polygon) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	polygon as city,
	AVG(rating_service__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and order_type = '1'
GROUP BY
	week,
	polygon) as t2
ON
	(t1.week = t2.week and t1.city = t2.city)
GROUP BY
	t1.mindate,
	t1.city;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'DE-Total' as city,
	'Weighted Service Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and LEFT(locale__c,2) = 'de'
		and order_type = '1'
GROUP BY
	week) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	AVG(rating_service__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and LEFT(locale__c,2) = 'de'
			and type = '60'
GROUP BY
	week) as t2
ON
	(t1.week = t2.week)
GROUP BY
	t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'CH-Total' as city,
	'Weighted Service Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and LEFT(locale__c,2) = 'ch'
GROUP BY
	week) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	AVG(rating_service__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and LEFT(locale__c,2) = 'ch'
GROUP BY
	week) as t2
ON
	(t1.week = t2.week)
GROUP BY
	t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	t1.mindate,
	'NL-Total' as city,
	'Weighted Service Rating' kpi,
	SUM(ratings*avg_rating)/SUM(ratings) as value
FROM
(
SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	COUNT(DISTINCT(Order_id__c)) as ratings
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and LEFT(locale__c,2) = 'nl'
			and type = '60'
GROUP BY
	week) as t1
LEFT JOIN
(SELECT
	MIN(Effectivedate::date) as mindate,
	EXTRACT(WEEK FROM Effectivedate::date) as Week,
	AVG(rating_service__c) as avg_rating
FROM
	bi.orders
WHERE
	order_type = '1'
	and status in ('INVOICED')
	and effectivedate::date >= '2016-01-01'
	and rating_service__c is not null
		and LEFT(locale__c,2) = 'nl'
			and type = '60'
GROUP BY
	week) as t2
ON
	(t1.week = t2.week)
GROUP BY
	t1.mindate;

INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	delivery_area,
	'min contract hours' as contract_hours,
	SUM(contract_hours) as contract_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	delivery_area,
	sum(weekly_hours) as contract_hours
FROM
	 bi.gpm_weekly_cleaner 
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate,
	delivery_area;


INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	'DE-Total',
	'min contract hours' as contract_hours,
	SUM(contract_hours) as contract_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	delivery_area,
	sum(weekly_hours) as contract_hours
FROM
	 bi.gpm_weekly_cleaner 
WHERE
	LEFT(delivery_area,2) = 'de'
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate;

INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	'NL-Total',
	'min contract hours' as contract_hours,
	SUM(contract_hours) as contract_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	delivery_area,
	sum(weekly_hours) as contract_hours
FROM
	 bi.gpm_weekly_cleaner 
WHERE
	LEFT(delivery_area,2) = 'nl'
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate;

INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	delivery_area,
	'sickness&vacation hours %' as kpi,
	SUM(sickness_ratio) as contract_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	delivery_area,
	suM(holidays_hours)/sum(weekly_hours) as sickness_ratio
FROM
	 bi.gpm_weekly_cleaner 
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate,
	delivery_area;

INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	'DE-Total' as kpi,
	'sickness&vacation hours %' as kpi,
	SUM(sickness_ratio) as contract_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	suM(holidays_hours)/sum(weekly_hours) as sickness_ratio
FROM
	 bi.gpm_weekly_cleaner 
WHERE
		LEFT(delivery_area,2) = 'de'
GROUP BY
	year_week) as a
GROUP BY
	mindate;

INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	'NL-Total' as kpi,
	'sickness&vacation hours %' as kpi,
	SUM(sickness_ratio) as contract_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	suM(holidays_hours)/sum(weekly_hours) as sickness_ratio
FROM
	 bi.gpm_weekly_cleaner 
WHERE
		LEFT(delivery_area,2) = 'nl'
GROUP BY
	year_week) as a
GROUP BY
	mindate;
		
INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	delivery_area,
	'worked hours' as kpi,
	SUM(a) as worked_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	delivery_area,
	sum(worked_hours) as a
FROM
	 bi.gpm_weekly_cleaner 
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate,
	delivery_area;

INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	'DE-Total' as kpi,
	'worked hours' as kpi,
	SUM(a) as worked_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	sum(worked_hours) as a
FROM
	 bi.gpm_weekly_cleaner 
WHERE
	 	LEFT(delivery_area,2) = 'de'
GROUP BY
	year_week) as a
GROUP BY
	mindate;


INSERT INTO bi.acm_kpis 
SELECT
	mindate,
	'NL-Total' as kpi,
	'worked hours' as kpi,
	SUM(a) as worked_hours
FROM(
SELECT
	year_week,
	min(mindate) as mindate,
	sum(worked_hours) as a
FROM
	 bi.gpm_weekly_cleaner 
WHERE
	 	LEFT(delivery_area,2) = 'nl'
GROUP BY
	year_week) as a
GROUP BY
	mindate;

INSERT INTO bi.acm_kpis 
SELECT
	date,
	b.delivery_area,
	'UR%' as kpi,
	value
FROM(
SELECT
	min(mindate2) as date,
	year_week,
	delivery_area,
	'UR%' as kpi,
	SUM(weekly_hours_capped)/SUM(weekly_hours) as value
FROM(
SELECT
	year_week,
	professional__c,
	MIN(mindate) as mindate2,
	delivery_area,
	SUM(weekly_hours) as weekly_hours,
	SUM(CASE WHEN worked_hours > weekly_hours THEN weekly_hours else worked_hours END) as weekly_hours_capped
FROM
bi.gpm_weekly_cleaner
GROUP BY
	year_week,
	professional__c,
	delivery_area) as a
GROUP BY
	year_week,
	delivery_area) as b;
	
INSERT INTO bi.acm_kpis 
SELECT
	date,
	'DE-Total',
	'UR%' as kpi,
	value
FROM(
SELECT
	min(mindate2) as date,
	year_week,
	'UR%' as kpi,
	SUM(weekly_hours_capped)/SUM(weekly_hours) as value
FROM(
SELECT
	year_week,
	professional__c,
	MIN(mindate) as mindate2,
	delivery_area,
	SUM(weekly_hours) as weekly_hours,
	SUM(CASE WHEN worked_hours > weekly_hours THEN weekly_hours else worked_hours END) as weekly_hours_capped
FROM
bi.gpm_weekly_cleaner
GROUP BY
	year_week,
	professional__c,
	delivery_area) as a
GROUP BY
	year_week) as b;

INSERT INTO bi.acm_kpis 
SELECT
	date,
	'DE-Total',
	'UR%' as kpi,
	value
FROM(
SELECT
	min(mindate2) as date,
	year_week,
	'UR%' as kpi,
	SUM(weekly_hours_capped)/SUM(weekly_hours) as value
FROM(
SELECT
	year_week,
	professional__c,
	MIN(mindate) as mindate2,
	delivery_area,
	SUM(weekly_hours) as weekly_hours,
	SUM(CASE WHEN worked_hours > weekly_hours THEN weekly_hours else worked_hours END) as weekly_hours_capped
FROM
bi.gpm_weekly_cleaner
WHERE
	left(delivery_area,2) = 'nl'
GROUP BY
	year_week,
	professional__c,
	delivery_area) as a
GROUP BY
	year_week) as b;

INSERT INTO bi.acm_kpis 
SELECT
	date,
	b.delivery_area,
	'Allocation Eff. %' as kpi,
	value
FROM(
SELECT
	min(mindate2) as date,
	year_week,
	delivery_area,
	'UR%' as kpi,
	SUM(worked_hours)/(SUM(weekly_hours)-SUM(holidays_hours)) as value
FROM(
SELECT
	year_week,
	professional__c,
	MIN(mindate) as mindate2,
	delivery_area,
	SUM(18) as weekly_hours,
	SUM(holidays_hours) as holidays_hours,
	SUM(worked_hours) as worked_hours
FROM
bi.gpm_weekly_cleaner
GROUP BY
	year_week,
	professional__c,
	delivery_area) as a
GROUP BY
	year_week,
	delivery_area) as b;

INSERT INTO bi.acm_kpis 
SELECT
	date,
	b.delivery_area,
	'Allocation Eff. %' as kpi,
	value
FROM(
SELECT
	min(mindate2) as date,
	year_week,
	'DE-Total' as delivery_area,
	'UR%' as kpi,
	SUM(worked_hours)/(SUM(weekly_hours)-SUM(holidays_hours)) as value
FROM(
SELECT
	year_week,
	professional__c,
	MIN(mindate) as mindate2,
	delivery_area,
	SUM(18) as weekly_hours,
	SUM(holidays_hours) as holidays_hours,
	SUM(worked_hours) as worked_hours
FROM
bi.gpm_weekly_cleaner
GROUP BY
	year_week,
	professional__c,
	delivery_area) as a
GROUP BY
	year_week) as b;

INSERT INTO bi.acm_kpis 
SELECT
	min(mindate2) as mindate,
	delivery_area,
	'Max Hours' as kpi,
	sum(value) as value
FROM(
SELECT
	year_week,	
	delivery_area,
	MIN(mindate) as mindate2,
	SUM(max_weekly_hours) as value
FROM
bi.gpm_weekly_cleaner
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate2,
	delivery_area;	

INSERT INTO bi.acm_kpis 
SELECT
	min(mindate2) as mindate,
	'DE-Total',
	'Max Hours' as kpi,
	sum(value) as value
FROM(
SELECT
	year_week,	
	delivery_area,
	MIN(mindate) as mindate2,
	SUM(max_weekly_hours) as value
FROM
bi.gpm_weekly_cleaner
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate2;	

INSERT INTO bi.acm_kpis 
SELECT
	min(mindate2) as mindate,
	'NL-Total',
	'Max Hours' as kpi,
	sum(value) as value
FROM(
SELECT
	year_week,	
	delivery_area,
	MIN(mindate) as mindate2,
	SUM(max_weekly_hours) as value
FROM
bi.gpm_weekly_cleaner
WHERE
	LEFT(delivery_area,2) = 'nl'
GROUP BY
	year_week,
	delivery_area) as a
GROUP BY
	mindate2;	
	
	
	
end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


END;

$BODY$ 
LANGUAGE 'plpgsql'