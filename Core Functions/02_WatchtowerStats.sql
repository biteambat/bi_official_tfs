DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$02_watchtowerstats(crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$02_watchtowerstats';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


-- Cancellations
 
DROP TABLE IF EXISTS bi.cancelledorders;
CREATE TABLE bi.cancelledorders as
SELECT
	created_at::date as date,
	created_at::time as time,
	replace(CAST(order_json->'Id' as varchar),'"', '') as OrderId,
	CASE WHEN event_name in ('Order Event:CANCELLED CUSTOMER','Order Event:CANCELLED CUSTOMER SHORTTERM') THEN 'Cancelled Customer'
	ELSE 'Cancelled Terminated' 
	END as Events
FROM
	events.sodium
WHERE
	event_name in ('Order Event:CANCELLED CUSTOMER','Order Event:CANCELLED CUSTOMER SHORTTERM','Order Event:CANCELLED TERMINATED');

DROP TABLE IF EXISTS bi.cancellation_reasons;
CREATE TABLE bi.cancellation_reasons as 
SELECT
	date as cancellation_date,
	LEFT(locale__c,2) as locale,
	t2.city,
	CASE WHEN Acquisition_New_Customer__c = '1' THEN 'Acquisition' ELSE 'Rebooking' END as Type_of_Customer,
	Cancellation_Reason__c,
	COUNT(1) as Orders
FROM
	bi.cancelledorders t1
JOIN
	bi.orders t2
ON
	(t1.orderid = t2.sfid)
WHERE
	Events = 'Cancelled Customer'
GROUP BY
	date,
	LEFT(locale__c,2),
	t2.city,
	Type_of_Customer,
	Cancellation_Reason__c;

DROP TABLE IF EXISTS bi.cancelledorders_tableau;
CREATE TABLE bi.cancelledorders_tableau as	
SELECT
	date,
	t2.city,
	events,
	LEFT(Locale__c,2) as locale,
	COUNT(DISTINCT(OrderId)) as Orders
FROM
	bi.cancelledorders t1
JOIN
	bi.orders t2
ON
	(t1.orderId = t2.sfid)
GROUP BY
	date,
	t2.city,
	events,
	locale; 



DROP TABLE IF EXISTS bi.cancelledorders; 
 
-- Reschedulings
 
DROP TABLE IF EXISTS bi.temp_reschedulings;
CREATE TABLE bi.temp_reschedulings as
SELECT
			replace(CAST(order_json->'Id' as varchar),'"', '') as OrderId,
			Created_at::date as Date,
			'Rescheduling' as Event_name
FROM
	events.sodium
WHERE
	event_name = 'Order Event:RESCHEDULED';
	

DROP TABLE IF EXISTS bi.reschedulings;
CREATE TABLE bi.reschedulings as 
SELECT
	Date,
	t2.city,
  LEFT(Locale__c,2) as locale,
  COUNT(1) as Reschedulings
FROM
	bi.temp_reschedulings t1
JOIN
	bi.orders t2
ON
	(t1.orderid = t2.sfid)
GROUP BY
	Date,
	t2.city,
	locale;

DROP TABLE IF EXISTS bi.temp_reschedulings;	 

-- Date

 
-- CLC by Customer


DROP TABLE IF EXISTS bi.cohortscosts;
CREATE TABLE bi.cohortscosts as 
SELECT
	to_char(cast(orderdate as date),'YYYY-MM') as Month,
	Min(cast(orderdate as date)) as Date,
	SUM(sem_non_brand+sem_brand+display+seo+seo_brand+facebook+offline_marketing) as marketingcosts
FROM
	bi.MarketingCostsImport
GROUP BY
	to_char(cast(orderdate as date),'YYYY-MM');

DROP TABLE IF EXISTS bi.cohortsstats;
CREATE TABLE bi.cohortsstats as 
SELECT
	to_char(cast(Order_Creation__c as date),'YYYY-MM') as OrderMonth,
	to_char(cast(Acquisition_Customer_Creation__c as date),'YYYY-MM') as AcquisitionMonth,
	min(cast(Acquisition_Customer_Creation__c as date)) as Date,
	SUM(Order_Duration__c*2) as GMV,
	SUM(Discount__c) as Discount
FROM
	Salesforce.Order
WHERE
	Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED')
GROUP BY
	to_char(cast(Acquisition_Customer_Creation__c as date),'YYYY-MM'),
	to_char(cast(Order_Creation__c as date),'YYYY-MM');

DROP TABLE IF EXISTS bi.cumm_marketingcosts;
CREATE TABLE bi.cumm_marketingcosts as 
SELECT
	acquisitionmonth,
	t1.Date,
	SUM(marketingcosts)+SUM(Discount) as marketingcosts
FROM
	bi.cohortsstats t1
LEFT JOIN
	bi.cohortscosts t2
ON
	(t1.OrderMonth = t2.Month and acquisitionmonth = t2.Month)
WHERE
	marketingcosts is not  null 
GROUP BY
	acquisitionmonth,
	t1.Date;

DROP TABLE IF EXISTS bi.clvcohort;
CREATE TABLE bi.clvcohort as 
SELECT
	t1.acquisitionmonth,
	t1.date,
	((CAST(left(t1.ordermonth,4) as integer)-CAST(left(t1.acquisitionmonth,4) as integer))*12)+CAST(right(t1.ordermonth,2) as integer)-CAST(right(t1.acquisitionmonth,2) as integer) as diff,	
	round(CAST(CAST(gmv as numeric)/marketingcosts as numeric),4) as CLV
FROM
	bi.cohortsstats t1
JOIN
	bi.cumm_marketingcosts t2
ON 
	(t1.acquisitionmonth = t2.acquisitionmonth)
GROUP BY
	t1.acquisitionmonth,
	t1.date,
	diff,
	CLV;

DROP TABLE IF EXISTS bi.cohortscosts;
DROP TABLE IF EXISTS bi.cohortsstats;
DROP TABLE IF EXISTS bi.cumm_marketingcosts;

DROP TABLE IF EXISTS bi.clvpercustomer_1;
CREATE TABLE  bi.clvpercustomer_1(acquisition_date varchar(20) DEFAULT NULL,  OrderNo SERIAL, contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,   creation_date varchar(40) DEFAULT NULL,   discount numeric DEFAULT NULL,   hours numeric NOT NULL, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.clvpercustomer_1
SELECT
  t1.Acquisition_Customer_Creation__c as acquisition_date,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Order_Creation__c as Date)) as a,
  contact__c,
  Order_Id__c,
  order_creation__c as creation_date,
  discount__c,
  Order_Duration__c
FROM
  Salesforce.Order t1
WHERE
  t1.test__c = '0'
  and t1.status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED')
ORDER BY
  contact__c,
  order_creation__c;

DROP TABLE IF EXISTS bi.clvpercustomer_2;
CREATE TABLE bi.clvpercustomer_2 as 
SELECT
	to_char(cast(acquisition_date as date),'YYYY-MM') as AcquisitionMonth,
	OrderNo,
	CASE WHEN OrderNo = '1' THEN 0 ELSE round(CAST(SUM(discount) as numeric)/COUNT(DISTINCT(Order_Id__c)),2) END as discount_per_order,
	round((CAST(SUM(hours)*3 as numeric)/COUNT(DISTINCT(Order_Id__c))),2) as comission_per_order
FROM
	bi.clvpercustomer_1
GROUP BY
	to_char(cast(acquisition_date as date),'YYYY-MM'),
	OrderNo;

-- DROP TABLE IF EXISTS bi.cpa_by_channel;	
-- CREATE TABLE bi.cpa_by_channel as 
-- SELECT
-- 	to_char(cast(date as date),'YYYY-MM') as AcquisitionMonth,
	-- round(CAST(SUM(sem_non_brand+sem_brand+display+seo+facebook+offline_marketing+gpdd_voucher+other_voucher) as numeric)/SUM(first_order_acquisitions),2) as CPA
-- FROM
-- 	bi.MarketingCostsTableau
-- GROUP BY
-- 	to_char(cast(date as date),'YYYY-MM');

-- DROP TABLE IF EXISTS bi.clvbycustomer_3;
-- CREATE TABLE bi.clvbycustomer_3 as 
-- SELECT
-- 	t1.AcquisitionMonth,
	-- OrderNo,
	-- t1.comission_per_order-discount_per_order-CASE WHEN CPA is null THEN 0 ELSE CPA END as revenue
-- FROM 
-- 	bi.clvpercustomer_2 t1
-- LEFT JOIn
-- 	bi.cpa_by_channel t2
-- ON
	-- (t1.AcquisitionMonth = t2.AcquisitionMonth and orderno = 1)
-- GROUP BY
-- 	t1.AcquisitionMonth,
	-- OrderNo,
	-- revenue;
	
DROP TABLE IF EXISTS bi.clvpercustomer_1;
DROP TABLE IF EXISTS bi.clvpercustomer_2;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
  
 END;

$BODY$ LANGUAGE 'plpgsql'