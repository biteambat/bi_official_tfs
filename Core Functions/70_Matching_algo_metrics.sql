CREATE OR REPLACE FUNCTION bi.daily$matching_algo_metrics(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$matching_algo_metrics';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.avg_commutingdistance_polygon;
CREATE TABLE bi.avg_commutingdistance_polygon AS

	SELECT
		t1.polygon as polygon,
		t1.country as Country,
		t1.DateofService as DayofCleaning,
		avg(t1.distance)/1000 as AVG_distance

	FROM
	(
	SELECT
		o.polygon as polygon,
		left(o.locale__c,2) as Country,
		o.order_id__c as OrderID,
		o.customer_id__c as CustoID,
		o.effectivedate::date as DateofService, 
		o.recurrency__c as RecType,
		o.shippinglongitude as OrderLongitude,
		o.shippinglatitude as OrderLatitude,
		o.professional__c as Cleaner,
		a.longitude__c as CleanerLongitude,
		a.latitude__c as CleanerLatitude,
		ST_Distance(ST_GeogFromText('SRID=4326;POINT(' || o.shippinglongitude || ' ' || o.shippinglatitude || ')'), ST_GeogFromText('SRID=4326;POINT(' || a.longitude__c || ' ' || a.latitude__c || ')')) as Distance
		    
	FROM bi.orders o LEFT JOIN salesforce.account a ON (o.professional__c = a.sfid)

	WHERE
		o.effectivedate >= '2016-01-01'
		and o.status in ('INVOICED', 'NOSHOW PROFESIONNAL', 'NOSHOW CUSTOMER')
		and o.test__c = '0'
		and type__c like '%cleaning-b2c%'
		
	GROUP BY
		RecType,
		CustoID,
		DateofService,
		OrderLongitude,
		OrderLatitude,
		Cleaner,
		CleanerLongitude,
		CleanerLatitude,
		polygon,
		Country,
		OrderID,
		Distance) as t1

	GROUP BY
		country, polygon, DayofCleaning

	ORDER BY DayofCleaning desc, country asc, polygon asc

;


DROP TABLE IF EXISTS bi.avg_dailyorders_polygon;
CREATE TABLE bi.avg_dailyorders_polygon AS

	SELECT
		t1.country as Country,
		t1.polygon as polygon,
		t1.dateofservice as DayofCleaning,
		avg(Nb_OrderID) as AverageOrders
		
	FROM
	(
	SELECT
		left(o.locale__c,2) as Country,
		o.polygon as polygon,
		o.effectivedate::date as DateofService, 
		o.professional__c as Cleaner,
		count(distinct o.order_id__c) as Nb_OrderID
		    
	FROM bi.orders o LEFT JOIN salesforce.account a ON (o.professional__c = a.sfid)

	WHERE
		o.effectivedate >= '2016-01-01'
		and o.status in ('INVOICED', 'NOSHOW PROFESIONNAL', 'NOSHOW CUSTOMER')
		and o.test__c = '0'
		and type__c like '%cleaning-b2c%'
		
	GROUP BY
		DateofService,
		Cleaner,
		polygon,
		Country) as t1
		
	GROUP BY
		Country,
		polygon,
		DayofCleaning

	ORDER BY DayofCleaning desc, country asc, polygon asc

;




DROP TABLE IF EXISTS bi.communting_dist_breakdown;
CREATE TABLE bi.communting_dist_breakdown AS

	SELECT
		left(o.locale__c,2) as Country,
		o.polygon as polygon,
		o.order_id__c as OrderID,
		o.effectivedate::date as DateofService, 
		o.shippinglongitude as OrderLongitude,
		o.shippinglatitude as OrderLatitude,
		o.professional__c as Cleaner,
		a.longitude__c as CleanerLongitude,
		a.latitude__c as CleanerLatitude,
		ST_Distance(ST_GeogFromText('SRID=4326;POINT(' || o.shippinglongitude || ' ' || o.shippinglatitude || ')'), ST_GeogFromText('SRID=4326;POINT(' || a.longitude__c || ' ' || a.latitude__c || ')'))/1000 as Distance
		    
	FROM bi.orders o LEFT JOIN salesforce.account a ON (o.professional__c = a.sfid)

	WHERE
		o.effectivedate >= '2016-01-01'
		and o.status in ('INVOICED', 'NOSHOW PROFESIONNAL', 'NOSHOW CUSTOMER')
		and o.test__c = '0'
		and type__c like '%cleaning-b2c%'
		
	GROUP BY
		DateofService,
		OrderLongitude,
		OrderLatitude,
		Cleaner,
		CleanerLongitude,
		CleanerLatitude,
		polygon,
		Country,
		OrderID,
		Distance

;

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'