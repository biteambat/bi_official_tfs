CREATE OR REPLACE FUNCTION bi.daily$forecast_kpisdashboard (crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$forecast_kpisdashboard';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


	DROP TABLE IF EXISTS bi.forecast_kpisdashboard;

	CREATE TABLE bi.forecast_kpisdashboard AS

	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------

	-- PART 1: CURRENT VALUES : DONE

	-- Total GMV current month (except CANCELLED FAKED and MISTAKE)

		SELECT
		left(o.locale__c,2)::text as locale,
		'Total GMV Current month'::text as kpi,
		ROUND(SUM(o.gmv_eur)::numeric,0) as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Total orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Orders # Current month'::text as kpi,
		COUNT(o.order_id__c)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;


	-- Acquisition GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Acquisition GMV Current month'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '1'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;

	-- Acquisition orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Acquisitions # Current month'::text as kpi,
		COUNT(o.order_id__c)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '1'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;

	-- Rebooking GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Rebookings GMV Current month'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Rebooking orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		left(o.locale__c,2)::text as locale,
		'Rebookings # Current month'::text as kpi,
		COUNT(o.order_id__c)::numeric as value

		FROM
			bi.orders o
		WHERE
			o.status not like ('CANCELLED%') and o.status not like ('NOSHOW PROFESSIONAL') 
			and extract(month from o.order_creation__c) = extract(month from current_date) 
			and extract(year from o.order_creation__c) = extract(year from current_date)
			and o.test__c = '0'
			and o.acquisition_new_customer__c = '0'

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc
	;

	-- RRM1 current month

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		rr.locale::text as locale,
		'Current RR M1'::text as kpi,
		sum(rr.cohort_return_rate * rr.acquisitions)/sum(rr.acquisitions)::numeric as value

		FROM
			bi.running_cohort_analysis_email rr
		
		WHERE
			extract(month from rr.date) = extract(month from current_date)
			and extract(year from rr.date) = extract(year from current_date)

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Global CPA current month

	INSERT INTO bi.forecast_kpisdashboard -- DONT FORGET TO ERASE THE DISCOUNT COSTS THAT SHOULD NOT BE INCLUDED IN THE CPA CALCULATION :)
		
		SELECT

		cpa.locale::text as locale,
		'Current CPA'::text as kpi,
		SUM(
			cpa.sem_brand
			+cpa.sem_non_brand
			+cpa.offline_marketing
			+cpa.facebook 
			+(cpa.gdn) 
			+(cpa.criteo) 
			+(cpa.sociomantic) 
			+(cpa.coops) 
			+cpa.other_voucher
			--+(cpa.vouchercosts)
			+cpa.tvcampaign
			+cpa.deindeal_voucher
			+cpa.seo)
		/
		SUM(cpa.all_acquisitions)::numeric as value

		--SUM([Sem Brand]+[Sem Non Brand]+[Offline Marketing]+[Facebook]+[Sociomantic]+[Criteo]+[Gdn]+[Gpdd Voucher]+[Coops]+[Other Voucher]+[Vouchercosts]+[Tvcampaign]+[Seo acq])/SUM([All Acquisitions])

		FROM bi.cpacalclocale cpa

		WHERE extract(month from cpa.date) = extract(month from current_date)
			and extract(year from cpa.date) = extract(year from current_date)

		GROUP BY locale, kpi

		ORDER BY locale asc
	;

	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------

	-- PART 2 : FORECASTS EOM 

	-- Forecast Total GMV Current month

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		f.locale::text as locale,
		'Forecast - Total GMV Current month'::text as kpi,
		ROUND(SUM(f.acquisition_forecast + f.rebookings_forecast)::numeric,0) as value

		FROM
			bi.booking_forecast_locale f

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Forecast Total orders current month (except CANCELLED FAKED and MISTAKE)


	-- Forecast Acquisition GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		f.locale::text as locale,
		'Forecast - Acquisition GMV Current month'::text as kpi,
		SUM(f.acquisition_forecast)::numeric as value

		FROM
			bi.booking_forecast_locale f

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;

	-- Forecast Acquisition orders current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		a.locale::text as locale,
		'Forecast - Acquisition # Current month'::text as kpi,
		SUM(CASE WHEN a.kpi = 'Forecast' THEN a.value END)::numeric as value

		FROM
			bi.acquisition_kpi a

		GROUP BY
			locale

		ORDER BY
			locale asc

	;

	-- Forecast Rebooking GMV current month (except CANCELLED FAKED and MISTAKE)

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		f.locale::text as locale,
		'Forecast - Rebookings GMV Current month'::text as kpi,
		SUM(f.rebookings_forecast)::numeric as value

		FROM
			bi.booking_forecast_locale f

		GROUP BY
			locale,
			kpi

		ORDER BY
			locale asc

	;


	-- Forecast B2B GMV current month


	-- Forecast RRM1 current month

		INSERT INTO bi.forecast_kpisdashboard

		SELECT 
		rr.locale::text as locale,
		'RR M1 Forecast'::text as kpi,
		rr.rr_m1_forecast as value

		FROM bi.rr_forecast rr

	;

	-- Forecast RRM3 current month

	-- Forecast RRM6 current month


	-- Forecast Global CPA current month

	INSERT INTO bi.forecast_kpisdashboard

		SELECT

		locale::text as locale,
		'Forecast CPA EOM'::text as kpi,

		((SUM(case when channel = 'Overall' and extract(month from dates) = extract(month from current_date) and extract(year from dates) = extract(year from current_date) then cost else NULL end) 
		/ extract(day from max(dates))
		* DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))
		/
		(SUM(case when channel = 'Overall' and extract(month from dates) = extract(month from current_date) and extract(year from dates) = extract(year from current_date) then acquisitions else NULL end) 
		/ extract(day from max(dates))
		* DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)))::numeric as value

		FROM bi.coststransform 

		WHERE channel = 'Overall'

		GROUP BY locale

		ORDER BY locale asc

	;
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------

	-- PART 3: TARGETS : DONE

	-- Setting acquisitions GMV targets per country

	
/*	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Acquisitions GMV Target'::text as kpi,
		'60000'::numeric as value
	;
*/	
	-- Setting acquisitions # targets per country

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'149'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'730'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'605'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Acquisitions # Target'::text as kpi,
		'240'::numeric as value
	;

	-- Setting Rebookings GMV targets per country
/*
	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Rebookings GMV Target'::text as kpi,
		'60000'::numeric as value
	;
*/
	-- Setting Rebookings # targets per country

/*	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Rebookings # Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Rebookings # Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Rebookings # Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Rebookings # Target'::text as kpi,
		'60000'::numeric as value
	;
*/
	-- Setting Total GMV target for all countries

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Total GMV Target'::text as kpi,
		'0'::numeric as value
	;

	-- Setting Total # target for all countries
/*
	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'Total # orders Target'::text as kpi,
		'100000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'Total # orders Target'::text as kpi,
		'600000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'Total # orders Target'::text as kpi,
		'200000'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'Total # orders Target'::text as kpi,
		'60000'::numeric as value
	;
*/

	-- Setting CPA Targets for all countries 

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'CPA Target'::text as kpi,
		'83'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'CPA Target'::text as kpi,
		'39'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'CPA Target'::text as kpi,
		'73'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'CPA Target'::text as kpi,
		'70'::numeric as value
	;

	-- Setting RR M1 Targets for all countries

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'at'::text as locale,
		'RR M1 Target'::text as kpi,
		'20'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'de'::text as locale,
		'RR M1 Target'::text as kpi,
		'35'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'ch'::text as locale,
		'RR M1 Target'::text as kpi,
		'36'::numeric as value
	;

	INSERT INTO bi.forecast_kpisdashboard

		SELECT
		'nl'::text as locale,
		'RR M1 Target'::text as kpi,
		'31'::numeric as value
	;

	-- acquisitions GMV targets per country
	-- acquisitions # targets per country

	-- Rebookings GMV targets per country
	-- Rebookings # targets per country

	-- Total GMV target for all countries
	-- Total # target for all countries

	-- CPA Targets for all countries 
	-- RR M1 Targets for all countries

	INSERT INTO bi.forecast_kpisdashboard

		SELECT

		locale::text as locale,
		'GMV Daily Run Rate'::text as kpi,
		round(max(value) / Extract(day from current_date)::numeric,0) as value

		FROM bi.forecast_kpisdashboard

		WHERE kpi = 'Total GMV Current month'

		GROUP BY locale, kpi

		ORDER BY locale asc

	;

		INSERT INTO bi.forecast_kpisdashboard

			SELECT

			locale::text as locale,
			'% on target GMV €'::text as kpi,
			ROUND(max(value)::numeric / (CASE WHEN locale = 'at' THEN 40433 WHEN locale = 'de' THEN 698468.54 WHEN locale = 'ch' THEN 222890 WHEN locale = 'nl' THEN 24994 END)*100,2) as value

			FROM bi.forecast_kpisdashboard

			WHERE kpi = 'Forecast - Total GMV Current month'

			GROUP BY locale, kpi

			ORDER BY locale asc

	;


		INSERT INTO bi.forecast_kpisdashboard

			SELECT

			locale::text as locale,
			'% on target Acquisitions #'::text as kpi,
			ROUND(max(value)::numeric / (CASE WHEN locale = 'at' THEN 260 WHEN locale = 'de' THEN 2650 WHEN locale = 'ch' THEN 680 WHEN locale = 'nl' THEN 187 END)*100,2)  as value

			FROM bi.forecast_kpisdashboard

			WHERE kpi = 'Forecast - Acquisition # Current month'

			GROUP BY locale, kpi

			ORDER BY locale asc
	;



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;