CREATE OR REPLACE FUNCTION bi.daily$citystats (crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$citystats';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;


BEGIN

DROP TABLE IF EXISTS bi.citystats_1;
CREATE TABLE bi.citystats_1 as 
SELECT 
  to_char(cast(EffectiveDate as date),'YYYY-MM') as Month,
  CASE 
  WHEN Locale__c = 'at-de' and ShippingCountry = 'Austria' THEN 'Austria'
  WHEN LEFT(ShippingPostalCode,2) in ('52') THEN 'Aachen'
  WHEN LEFT(ShippingPostalCode,2) in ('10','11','12','13','14','15') or LEFT(ShippingPostalCode,3) in ('140','141','144','145','153') THEN 'Berlin'
  WHEN LEFT(ShippingPostalCode,3) in ('320','335','336','337','338') THEN 'Bielefeld'
  WHEN LEFT(ShippingPostalCode,3) in ('530','531','532','533','537','538') THEN 'Bonn'
  WHEN LEFT(ShippingPostalCode,2) = '28' THEN 'Bremen'
  WHEN LEFT(ShippingPostalCode,3) in ('502','503','504','505','506','507','508','509','510','511','513','514') THEN 'Cologne'
  WHEN LEFT(ShippingPostalCode,3) in ('642','643') THEN 'Darmstadt'
  WHEN LEFT(ShippingPostalCode,2) = '44' or ShippingPostalCode in ('457','580','582','583','584','586','594') THEN 'Dortmund'
  WHEN LEFT(ShippingPostalCode,2) = '01' THEN 'Dresden'
  WHEN LEFT(ShippingPostalCode,3) in ('454','460','461','470','471','472','473','474','475') THEN 'Duisburg'
  WHEN LEFT(ShippingPostalCode,2) = '40' or LEFT(ShippingPostalCode,3) in ('414','415') THEN 'Dusseldorf'
  WHEN LEFT(ShippingPostalCode,3) in ('446','448','462') or LEFT(ShippingPostalCode,2) = '45' THEN 'Essen'
  WHEN LEFT(ShippingPostalCode,3) in ('611','612','613','614','630','631','632','633','634','654','657','658','659') OR LEFT(ShippingPostalCode,2) = '60' THEN 'Frankfurt am Main'
  WHEN LEFT(ShippingPostalCode,2) = '79' THEN 'Freiburg'
  WHEN LEFT(ShippingPostalCode,2) = '58' THEN 'Hagen'
  WHEN LEFT(ShippingPostalCode,2) in ('20','21','22') THEN 'Hamburg'
  WHEN LEFT(ShippingPostalCode,3) in ('300','301','304','305','306','308','309') THEN 'Hanover'
  WHEN LEFT(ShippingPostalCode,3) in ('041','042','043','044') THEN 'Leipzig'
  WHEN LEFT(ShippingPostalCode,3) in ('550','551','552','650','651','652','653','655') THEN 'Mainz'
  WHEN LEFT(ShippingPostalCode,2) = '68' or LEFT(ShippingPostalCode,3) in ('670','671','672','673','690','691','692','694','699') THEN 'Mannheim'
  WHEN LEFT(ShippingPostalCode,3) in ('410','411','412','413','417','478') THEN 'Mönchengladbach'
  WHEN LEFT(ShippingPostalCode,2) in ('80','81') or LEFT(ShippingPostalCode,3) in ('820','821','855','857') THEN 'Munich'
  WHEN LEFT(ShippingPostalCode,3) in  ('480','481') THEN 'Munster' 
  WHEN LEFT(ShippingPostalCode,2) = '90' or LEFT(ShippingPostalCode,3) in ('910','911','912','913') THEN 'Nuremberg'
  WHEN LEFT(ShippingPostalCode,3) in ('180','181','182') THEN 'Rostock'
  WHEN LEFT(ShippingPostalCode,3) in ('660','661','662','663') THEN 'Saarbrücken'
  WHEN LEFT(ShippingPostalCode,3) in ('700','701','703','704','705','706','707','708','710','712','716','737') THEN 'Stuttgart'
  WHEN LEFT(ShippingPostalCode,3) in ('720','721','725','726','727','730','732','736','743','752','753','754') OR LEFT(ShippingPostalCode,2) in ('71') THEN 'Stuttgart+'
  WHEN LEFT(ShippingPostalCode,2) in ('42') THEN 'Wuppertal'
  WHEN LEFT(ShippingPostalCode,3) in ('970','971','972') THEN 'Würzburg'
  ELSE 'Other'
  END as City,
  SUM(Order_Duration__c) as Hours,
  COUNT(DISTINCT(Professional__c)) as Distinct_Cleaner,
  ROUND(CASE WHEN COUNT(DISTINCT(Professional__c)) = 0 THEN 0 ELSE CAST(SUM(Order_Duration__c) as numeric)/COUNT(DISTINCT(Professional__c)) END,2)  as AVG_Hours_per_Cleaner
FROM
	salesforce.order
WHERE
	test__c = '0'
	and status in ('INVOICED') 
GROUP BY
	MOnth,
	City;
	
DROP TABLE IF EXISTS bi.citystats_2;
CREATE TABLE bi.citystats_2 as 
SELECT 
  to_char(cast(EffectiveDate as date),'YYYY-MM') as Month,
  CASE 
  WHEN Locale__c = 'at-de' and ShippingCountry = 'Austria' THEN 'Austria'
  WHEN LEFT(ShippingPostalCode,2) in ('52') THEN 'Aachen'
  WHEN LEFT(ShippingPostalCode,2) in ('10','11','12','13','14','15') or LEFT(ShippingPostalCode,3) in ('140','141','144','145','153') THEN 'Berlin'
  WHEN LEFT(ShippingPostalCode,3) in ('320','335','336','337','338') THEN 'Bielefeld'
  WHEN LEFT(ShippingPostalCode,3) in ('530','531','532','533','537','538') THEN 'Bonn'
  WHEN LEFT(ShippingPostalCode,2) = '28' THEN 'Bremen'
  WHEN LEFT(ShippingPostalCode,3) in ('502','503','504','505','506','507','508','509','510','511','513','514') THEN 'Cologne'
  WHEN LEFT(ShippingPostalCode,3) in ('642','643') THEN 'Darmstadt'
  WHEN LEFT(ShippingPostalCode,2) = '44' or ShippingPostalCode in ('457','580','582','583','584','586','594') THEN 'Dortmund'
  WHEN LEFT(ShippingPostalCode,2) = '01' THEN 'Dresden'
  WHEN LEFT(ShippingPostalCode,3) in ('454','460','461','470','471','472','473','474','475') THEN 'Duisburg'
  WHEN LEFT(ShippingPostalCode,2) = '40' or LEFT(ShippingPostalCode,3) in ('414','415') THEN 'Dusseldorf'
  WHEN LEFT(ShippingPostalCode,3) in ('446','448','462') or LEFT(ShippingPostalCode,2) = '45' THEN 'Essen'
  WHEN LEFT(ShippingPostalCode,3) in ('611','612','613','614','630','631','632','633','634','654','657','658','659') OR LEFT(ShippingPostalCode,2) = '60' THEN 'Frankfurt am Main'
  WHEN LEFT(ShippingPostalCode,2) = '79' THEN 'Freiburg'
  WHEN LEFT(ShippingPostalCode,2) = '58' THEN 'Hagen'
  WHEN LEFT(ShippingPostalCode,2) in ('20','21','22') THEN 'Hamburg'
  WHEN LEFT(ShippingPostalCode,3) in ('300','301','304','305','306','308','309') THEN 'Hanover'
  WHEN LEFT(ShippingPostalCode,3) in ('041','042','043','044') THEN 'Leipzig'
  WHEN LEFT(ShippingPostalCode,3) in ('550','551','552','650','651','652','653','655') THEN 'Mainz'
  WHEN LEFT(ShippingPostalCode,2) = '68' or LEFT(ShippingPostalCode,3) in ('670','671','672','673','690','691','692','694','699') THEN 'Mannheim'
  WHEN LEFT(ShippingPostalCode,3) in ('410','411','412','413','417','478') THEN 'Mönchengladbach'
  WHEN LEFT(ShippingPostalCode,2) in ('80','81') or LEFT(ShippingPostalCode,3) in ('820','821','855','857') THEN 'Munich'
  WHEN LEFT(ShippingPostalCode,3) in  ('480','481') THEN 'Munster' 
  WHEN LEFT(ShippingPostalCode,2) = '90' or LEFT(ShippingPostalCode,3) in ('910','911','912','913') THEN 'Nuremberg'
  WHEN LEFT(ShippingPostalCode,3) in ('180','181','182') THEN 'Rostock'
  WHEN LEFT(ShippingPostalCode,3) in ('660','661','662','663') THEN 'Saarbrücken'
  WHEN LEFT(ShippingPostalCode,3) in ('700','701','703','704','705','706','707','708','710','712','716','737') THEN 'Stuttgart'
  WHEN LEFT(ShippingPostalCode,3) in ('720','721','725','726','727','730','732','736','743','752','753','754') OR LEFT(ShippingPostalCode,2) in ('71') THEN 'Stuttgart+'
  WHEN LEFT(ShippingPostalCode,2) in ('42') THEN 'Wuppertal'
  WHEN LEFT(ShippingPostalCode,3) in ('970','971','972') THEN 'Würzburg'
  ELSE 'Other'
  END as City,
  SUM(Order_Duration__c) as HOurs
FROM
	salesforce.order
WHERE
	test__c = '0'
	and status in ('CANCELLED NO MANPOWER') 
GROUP BY
	MOnth,
	City;

DROP TABLE IF EXISTS bi.citystats_3;
CREATE TABLE bi.citystats_3 as 
SELECT
	t1.*,
	CASE WHEN t2.HOurs is null then 0.00 ELSE t2.HOurs END as NMP_Hours
FROM
	bi.citystats_1 t1
LEFT JOIN
	bi.citystats_2 t2
ON
	(t1.City = t2.City and t1.Month = t2.Month);
	
DROP TABLE IF EXISTS bi.citystats_1;
DROP TABLE IF EXISTS bi.citystats_2;	

DROP TABLE IF EXISTS bi.citystats_growth;
CREATE TABLE bi.citystats_growth as 
SELECT
	 CASE 
  WHEN t2.Locale__c = 'at-de' and t2.BillingCountry = 'Austria' THEN 'Austria'
  WHEN LEFT(t2.BillingPostalCode,2) in ('52') THEN 'Aachen'
  WHEN LEFT(t2.BillingPostalCode,2) in ('10','11','12','13','14','15') or LEFT(t2.BillingPostalCode,3) in ('140','141','144','145','153') THEN 'Berlin'
  WHEN LEFT(t2.BillingPostalCode,3) in ('320','335','336','337','338') THEN 'Bielefeld'
  WHEN LEFT(t2.BillingPostalCode,3) in ('530','531','532','533','537','538') THEN 'Bonn'
  WHEN LEFT(t2.BillingPostalCode,2) = '28' THEN 'Bremen'
  WHEN LEFT(t2.BillingPostalCode,3) in ('502','503','504','505','506','507','508','509','510','511','513','514') THEN 'Cologne'
  WHEN LEFT(t2.BillingPostalCode,3) in ('642','643') THEN 'Darmstadt'
  WHEN LEFT(t2.BillingPostalCode,2) = '44' or t2.BillingPostalCode in ('457','580','582','583','584','586','594') THEN 'Dortmund'
  WHEN LEFT(t2.BillingPostalCode,2) = '01' THEN 'Dresden'
  WHEN LEFT(t2.BillingPostalCode,3) in ('454','460','461','470','471','472','473','474','475') THEN 'Duisburg'
  WHEN LEFT(t2.BillingPostalCode,2) = '40' or LEFT(t2.BillingPostalCode,3) in ('414','415') THEN 'Dusseldorf'
  WHEN LEFT(t2.BillingPostalCode,3) in ('446','448','462') or LEFT(t2.BillingPostalCode,2) = '45' THEN 'Essen'
  WHEN LEFT(t2.BillingPostalCode,3) in ('611','612','613','614','630','631','632','633','634','654','657','658','659') OR LEFT(t2.BillingPostalCode,2) = '60' THEN 'Frankfurt am Main'
  WHEN LEFT(t2.BillingPostalCode,2) = '79' THEN 'Freiburg'
  WHEN LEFT(t2.BillingPostalCode,2) = '58' THEN 'Hagen'
  WHEN LEFT(t2.BillingPostalCode,2) in ('20','21','22') THEN 'Hamburg'
  WHEN LEFT(t2.BillingPostalCode,3) in ('300','301','304','305','306','308','309') THEN 'Hanover'
  WHEN LEFT(t2.BillingPostalCode,3) in ('041','042','043','044') THEN 'Leipzig'
  WHEN LEFT(t2.BillingPostalCode,3) in ('550','551','552','650','651','652','653','655') THEN 'Mainz'
  WHEN LEFT(t2.BillingPostalCode,2) = '68' or LEFT(t2.BillingPostalCode,3) in ('670','671','672','673','690','691','692','694','699') THEN 'Mannheim'
  WHEN LEFT(t2.BillingPostalCode,3) in ('410','411','412','413','417','478') THEN 'Mönchengladbach'
  WHEN LEFT(t2.BillingPostalCode,2) in ('80','81') or LEFT(t2.BillingPostalCode,3) in ('820','821','855','857') THEN 'Munich'
  WHEN LEFT(t2.BillingPostalCode,3) in  ('480','481') THEN 'Munster' 
  WHEN LEFT(t2.BillingPostalCode,2) = '90' or LEFT(t2.BillingPostalCode,3) in ('910','911','912','913') THEN 'Nuremberg'
  WHEN LEFT(t2.BillingPostalCode,3) in ('180','181','182') THEN 'Rostock'
  WHEN LEFT(t2.BillingPostalCode,3) in ('660','661','662','663') THEN 'Saarbrücken'
  WHEN LEFT(t2.BillingPostalCode,3) in ('700','701','703','704','705','706','707','708','710','712','716','737') THEN 'Stuttgart'
  WHEN LEFT(t2.BillingPostalCode,3) in ('720','721','725','726','727','730','732','736','743','752','753','754') OR LEFT(t2.BillingPostalCode,2) in ('71') THEN 'Stuttgart+'
  WHEN LEFT(t2.BillingPostalCode,2) in ('42') THEN 'Wuppertal'
  WHEN LEFT(t2.BillingPostalCode,3) in ('970','971','972') THEN 'Würzburg'
  ELSE 'Other'
  END as City,
  COUNT(DISTINCT(t2.sfid)) as All_Cleaners,
  COUNT(DISTINCT(t1.Professional__c)) Active_Cleaners,
  (COUNT(DISTINCT(t2.sfid))-COUNT(DISTINCT(t1.Professional__c))) as Inactive_Cleaners
FROM
	salesforce.account t2
LEFT JOIN
	salesforce.order t1
ON
	(t1.Professional__c = t2.sfid 	and t1.status in ('INVOICED','CANCELLED CUSTOMER') and CAST(Effectivedate as DATE) between cast(crunchdate as date) - interval '32 days' and cast(crunchdate as date) - interval '2 days' )
WHERE
	Status__c not in ('LEFT','TERMINATED')
GROUP BY
	City;

DROP TABLE IF EXISTS bi.citystats_growth2;
CREATE TABLE bi.citystats_growth2 as	
SELECT
	 CASE 
  WHEN Locale__c = 'at-de' and ShippingCountry = 'Austria' THEN 'Austria'
  WHEN LEFT(ShippingPostalCode,2) in ('52') THEN 'Aachen'
  WHEN LEFT(ShippingPostalCode,2) in ('10','11','12','13','14','15') or LEFT(ShippingPostalCode,3) in ('140','141','144','145','153') THEN 'Berlin'
  WHEN LEFT(ShippingPostalCode,3) in ('320','335','336','337','338') THEN 'Bielefeld'
  WHEN LEFT(ShippingPostalCode,3) in ('530','531','532','533','537','538') THEN 'Bonn'
  WHEN LEFT(ShippingPostalCode,2) = '28' THEN 'Bremen'
  WHEN LEFT(ShippingPostalCode,3) in ('502','503','504','505','506','507','508','509','510','511','513','514') THEN 'Cologne'
  WHEN LEFT(ShippingPostalCode,3) in ('642','643') THEN 'Darmstadt'
  WHEN LEFT(ShippingPostalCode,2) = '44' or ShippingPostalCode in ('457','580','582','583','584','586','594') THEN 'Dortmund'
  WHEN LEFT(ShippingPostalCode,2) = '01' THEN 'Dresden'
  WHEN LEFT(ShippingPostalCode,3) in ('454','460','461','470','471','472','473','474','475') THEN 'Duisburg'
  WHEN LEFT(ShippingPostalCode,2) = '40' or LEFT(ShippingPostalCode,3) in ('414','415') THEN 'Dusseldorf'
  WHEN LEFT(ShippingPostalCode,3) in ('446','448','462') or LEFT(ShippingPostalCode,2) = '45' THEN 'Essen'
  WHEN LEFT(ShippingPostalCode,3) in ('611','612','613','614','630','631','632','633','634','654','657','658','659') OR LEFT(ShippingPostalCode,2) = '60' THEN 'Frankfurt am Main'
  WHEN LEFT(ShippingPostalCode,2) = '79' THEN 'Freiburg'
  WHEN LEFT(ShippingPostalCode,2) = '58' THEN 'Hagen'
  WHEN LEFT(ShippingPostalCode,2) in ('20','21','22') THEN 'Hamburg'
  WHEN LEFT(ShippingPostalCode,3) in ('300','301','304','305','306','308','309') THEN 'Hanover'
  WHEN LEFT(ShippingPostalCode,3) in ('041','042','043','044') THEN 'Leipzig'
  WHEN LEFT(ShippingPostalCode,3) in ('550','551','552','650','651','652','653','655') THEN 'Mainz'
  WHEN LEFT(ShippingPostalCode,2) = '68' or LEFT(ShippingPostalCode,3) in ('670','671','672','673','690','691','692','694','699') THEN 'Mannheim'
  WHEN LEFT(ShippingPostalCode,3) in ('410','411','412','413','417','478') THEN 'Mönchengladbach'
  WHEN LEFT(ShippingPostalCode,2) in ('80','81') or LEFT(ShippingPostalCode,3) in ('820','821','855','857') THEN 'Munich'
  WHEN LEFT(ShippingPostalCode,3) in  ('480','481') THEN 'Munster' 
  WHEN LEFT(ShippingPostalCode,2) = '90' or LEFT(ShippingPostalCode,3) in ('910','911','912','913') THEN 'Nuremberg'
  WHEN LEFT(ShippingPostalCode,3) in ('180','181','182') THEN 'Rostock'
  WHEN LEFT(ShippingPostalCode,3) in ('660','661','662','663') THEN 'Saarbrücken'
  WHEN LEFT(ShippingPostalCode,3) in ('700','701','703','704','705','706','707','708','710','712','716','737') THEN 'Stuttgart'
  WHEN LEFT(ShippingPostalCode,3) in ('720','721','725','726','727','730','732','736','743','752','753','754') OR LEFT(ShippingPostalCode,2) in ('71') THEN 'Stuttgart+'
  WHEN LEFT(ShippingPostalCode,2) in ('42') THEN 'Wuppertal'
  WHEN LEFT(ShippingPostalCode,3) in ('970','971','972') THEN 'Würzburg'
  ELSE 'Other'
  END as City,
 CASE WHEN SUM(CASE WHEN CAST(Effectivedate as DATE) between CAST(crunchdate as DATE) - interval '8 days' and CAST(crunchdate as DATE) - interval '2 days' THEN 1 ELSE 0 END) > 0 THEN (SUM(CASE WHEN CAST(Effectivedate as DATE) between CAST(crunchdate as DATE) - interval '9 days' and CAST(crunchdate as DATE) - interval '2 days' THEN 1 ELSE 0 END)) ELSE 0 END as Orders_last_week,
 CASE WHEN SUM(CASE WHEN CAST(Effectivedate as DATE) between CAST(crunchdate as DATE) - interval '22 days' and CAST(crunchdate as DATE) - interval '16 days' THEN 1 ELSE 0 END) > 0 THEN (SUM(CASE WHEN CAST(Effectivedate as DATE) between CAST(crunchdate as DATE) - interval '22 days' and CAST(crunchdate as DATE) - interval '16 days' THEN 1 ELSE 0 END)) ELSE 0 END as Orders_3weeks_ago
FROM
	 Salesforce.order
WHERE
	Status = 'INVOICED' 
GROUP BY
	City; 

DROP TABLE IF EXISTS bi.citystats_growth3;
CREATE TABLE bi.citystats_growth3  as	 
SELECT
	t1.City,
	all_cleaners,
	active_cleaners,
	inactive_cleaners,
	orders_last_week,
	orders_3weeks_ago
FROM
	bi.citystats_growth t1
LEFT JOIn
	bi.citystats_growth2 t2
ON
	(t1.city = t2.city);
	
DROP TABLE IF EXISTS bi.citystats_growth2;
DROP TABLE IF EXISTS bi.citystats_growth1;
	
end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ 
LANGUAGE 'plpgsql'
