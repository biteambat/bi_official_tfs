DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$cohorts(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := ' bi.daily$cohorts';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

-- Topline Cohorts p1

DROP TABLE IF EXISTS bi.Running_Cohort_Analysis;
CREATE TABLE bi.Running_Cohort_Analysis AS
SELECT
  Customer_Id__c,
  LEFT(Locale__c,2) as locale,
  CAST(acquisition_customer_creation__c as date) as Acquisition_Date,
  MAX(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_done_last28d,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_count_last28,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days'  THEN 1 ELSE 0 END) as booking_count_last57
FROM
	bi.orders
WHERE
	Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
 	and test__c = '0'
 	and order_type = '1'
GROUP BY
	CAST(acquisition_customer_creation__c as date),
	Customer_Id__c,
	locale;

DELETE FROM bi.Running_Cohort_Analysis_email WHERE Date = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.Running_Cohort_Analysis_email
SELECT
	cast(crunchdate as date) - INTERVAL '1 days' as Date,
   CASE WHEN (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
   CASE WHEN (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_count_last57 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p0_Activitiy_Rate,
   CASE WHEN (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activitiy_Rate,
   (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as Acquisitions,
   (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as Rebookings,
	locale
FROM
   bi.Running_Cohort_Analysis
GROUP BY
	locale;

-- M6 Marekting & Cohorts City

DROP TABLE IF EXISTS bi.Customer_City;
CREATE TABLE bi.Customer_City as 
SELECT   
  Customer_Id__c,   
	City,
	marketing_Channel as acquisition_channel
FROM   
  bi.orders 
WHERE   
  Acquisition_New_Customer__c = '1'
  and order_type = '1'; 


CREATE INDEX orders_index ON bi.Customer_City(customer_Id__c);
  
DROP TABLE IF EXISTS bi.marketing_Channel_v2;
CREATE TABLE bi.marketing_channel_v2 as  
SELECT
	t1.*,
	t2.acquisition_Channel
FROM
	bi.orders t1
LEFT JOIn
	bi.Customer_City t2
ON
	(t1.customer_Id__c = T2.customer_id__c)
WHERE
	order_type = '1';
  
DROP TABLE IF EXISTS bi.Running_Cohort_Analysis_City; 
CREATE TABLE bi.Running_Cohort_Analysis_City as
SELECT   
  Customer_Id__c,   
  City,
  acquisition_channel,
  CAST(acquisition_customer_creation__c as date) as Acquisition_Date,
  MAX(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days'  THEN 1 ELSE 0 END) as booking_done_last28d,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_count_last28,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days'  THEN 1 ELSE 0 END) as booking_count_last210
FROM
   bi.marketing_channel_v2 t1
WHERE
  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and t1.test__c = '0'
GROUP BY
  Customer_Id__c,
  City,
  acquisition_channel,
  CAST(acquisition_customer_creation__c as date) ;

DELETE FROM bi.retention_city_marketing_m6 WHERE date = cast(crunchdate as date) - Interval '1 days';

INSERT INTO bi.retention_city_marketing_m6
SELECT
  cast(crunchdate as date) - INTERVAL '1 days' as date,
  City,
  acquisition_channel,
  (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN 1 ELSE 0 END)) as Acquisitions,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN booking_count_last210 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p0_Activitiy_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activity_Rate
 FROM
  bi.Running_Cohort_Analysis_City 
GROUP BY
  City,
  acquisition_channel;

	
-- Marketing Cohorts & City

DROP TABLE IF EXISTS bi.Customer_City;
CREATE TABLE bi.Customer_City as 
SELECT   
  Customer_Id__c,   
	City,
	marketing_Channel as acquisition_channel
FROM   
  bi.orders 
WHERE   
  Acquisition_New_Customer__c = '1'
  and order_type = '1'; 
  
DROP TABLE IF EXISTS bi.marketing_Channel_v2;
CREATE TABLE bi.marketing_channel_v2 as  
SELECT
	t1.*,
	t2.acquisition_Channel
FROM
	bi.orders t1
LEFT JOIn
	bi.Customer_City t2
ON
	(t1.customer_Id__c = T2.customer_id__c);
  
DROP TABLE IF EXISTS bi.Running_Cohort_Analysis_City; 
CREATE TABLE bi.Running_Cohort_Analysis_City as
SELECT   
  Customer_Id__c,   
  City,
  acquisition_channel,
  CAST(acquisition_customer_creation__c as date) as Acquisition_Date,
  MAX(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days'  THEN 1 ELSE 0 END) as booking_done_last28d,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_count_last28,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days'  THEN 1 ELSE 0 END) as booking_count_last57
FROM
   bi.marketing_channel_v2 t1
WHERE
  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and t1.test__c = '0'
GROUP BY
  Customer_Id__c,
  City,
  acquisition_channel,
  CAST(acquisition_customer_creation__c as date) ;

DELETE FROM bi.retention_city_marketing WHERE date = cast(crunchdate as date) - Interval '1 days';

INSERT INTO bi.retention_city_marketing
SELECT
  cast(crunchdate as date) - INTERVAL '1 days' as date,
  City,
  acquisition_channel,
  (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as Acquisitions,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_count_last57 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p0_Activitiy_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activity_Rate
 FROM
  bi.Running_Cohort_Analysis_City 
GROUP BY
  City,
  acquisition_channel;	
	
-- Marketing Cohorts	p1

DROP TABLE IF EXISTS bi.Customer_Acquisition_Channel;
Create Table bi.Customer_Acquisition_Channel as
SELECT   
  Customer_Id__c, 
  LEFT(locale__c,2) as locale,
  CASE WHEN Marketing_Channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE Marketing_Channel END as Acquisition_Marketing_Channel 
FROM   
  bi.orders 
WHERE   
  Acquisition_New_Customer__c = '1';  
 
DROP TABLE IF EXISTS bi.Orders_Marketing; 
CREATE TABLE bi.Orders_Marketing as
SELECT   
  t1.*,   
  CASE WHEN t2.Acquisition_Marketing_Channel is null THEN 'Unattributed' ELSE t2.Acquisition_Marketing_Channel END as Acquisition_Marketing_Channel 
FROM   
  bi.orders t1 
LEFT JOIN   
  bi.Customer_Acquisition_Channel t2 
USING   
  (Customer_Id__c)
WHERE
	order_type = '1';    
  
DROP TABLE IF EXISTS bi.Running_Cohort_Analysis_Marketing; 
CREATE TABLE bi.Running_Cohort_Analysis_Marketing as   
SELECT   
  Customer_Id__c,
  LEFT(locale__c,2) as locale,
  Acquisition_Marketing_Channel,
  CAST(acquisition_customer_creation__c as date) as Acquisition_Date,
  MAX(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - interval '1 days' THEN 1 ELSE 0 END) as booking_done_last28d,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - interval '1 days' THEN 1 ELSE 0 END) as booking_count_last28,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days'  THEN 1 ELSE 0 END) as booking_count_last57
FROM
   bi.Orders_Marketing t1
WHERE
  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
 GROUP BY
  Customer_Id__c,
  Acquisition_Marketing_Channel,
  Acquisition_Date,
  locale;

DELETE FROM bi.Running_Cohort_Analysis_Marketing2 WHERE DATE = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.Running_Cohort_Analysis_Marketing2
SELECT
 cast(crunchdate as date) - INTERVAL '1 days' as days,
  Acquisition_Marketing_Channel,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_count_last57 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p0_Activitiy_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activity_Rate,
  locale,
  SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) as Acquisitions
FROM
  bi.Running_Cohort_Analysis_Marketing 
GROUP BY
  Acquisition_Marketing_Channel,
  locale;

DROP TABLE IF EXISTS bi.Customer_Acquisition_Channel;

--  Cohorts p1

DROP TABLE IF EXISTS bi.Customer_City;
CREATE TABLE bi.Customer_City as 
SELECT   
  Customer_Id__c,   
	City
FROM   
  bi.orders 
WHERE   
  Acquisition_New_Customer__c = '1'
  and order_type = '1';  
  
DROP TABLE IF EXISTS bi.Orders_City; 
CREATE TABLE bi.Orders_City as
SELECT   
  t1.*
FROM   
  bi.orders t1 
LEFT JOIN   
  bi.Customer_City t2 
ON
	(t1.CUstomer_Id__c = t2.Customer_Id__c)
WHERE
	order_type = '1';
  
DROP TABLE IF EXISTS bi.Running_Cohort_Analysis_City; 
CREATE TABLE bi.Running_Cohort_Analysis_City as
SELECT   
  Customer_Id__c,   
  City,
  CAST(acquisition_customer_creation__c as date) as Acquisition_Date,
  MAX(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days'  THEN 1 ELSE 0 END) as booking_done_last28d,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_count_last28,
  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days'  THEN 1 ELSE 0 END) as booking_count_last57
FROM
   bi.Orders_City t1
WHERE
  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and t1.test__c = '0'
GROUP BY
  Customer_Id__c,
  City,
  CAST(acquisition_customer_creation__c as date) ;

DELETE FROM bi.Running_Cohort_Analysis_City2 WHERE DATE = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.Running_Cohort_Analysis_City2
SELECT
  cast(crunchdate as date) - INTERVAL '1 days' as date,
  City,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_count_last57 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p0_Activitiy_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activity_Rate
 FROM
  bi.Running_Cohort_Analysis_City 
GROUP BY
  City;

DROP TABLE IF EXISTS bi.Customer_City;


-- Topline Cohorts p3

DELETE FROM bi.Running_Cohort_Analysis_emailp3 WHERE Date = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.Running_Cohort_Analysis_emailp3
SELECT
	cast(crunchdate as date) - INTERVAL '1 days' as Date,
   CASE WHEN 	SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
   CASE WHEN 	SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activitiy_Rate,
	SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END) as Acqusitions,
	locale
FROM
  bi.Running_Cohort_Analysis
GROUP BY
	locale;
  

  
-- Marketing Cohorts p3

DELETE FROM bi.Running_Cohort_Analysis_Marketing2p3 WHERE DATE = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.Running_Cohort_Analysis_Marketing2p3 
SELECT
 cast(crunchdate as date) - INTERVAL '1 days' as date,
  Acquisition_Marketing_Channel,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activity_Rate,
  locale,
  SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END) as Acqusitions
FROM
  bi.Running_Cohort_Analysis_Marketing 
GROUP BY
  Acquisition_Marketing_Channel,
  locale;

DROP TABLE IF EXISTS bi.Orders_Marketing; 
DROP TABLE IF EXISTS bi.Running_Cohort_Analysis_Marketing; 

-- City Cohorts p3

DELETE FROM bi.Running_Cohort_Analysis_City2p3 WHERE date = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.Running_Cohort_Analysis_City2p3 
SELECT
  cast(crunchdate as date) - INTERVAL '1 days' as date,
  City,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as Cohort_Return_Rate,
  CASE WHEN SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '111 days' and cast(crunchdate as date) - interval '84 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activity_Rate
FROM
  bi.Running_Cohort_Analysis_City
GROUP BY
  City;

DROP TABLE IF EXISTS bi.Running_Cohort_Analysis_City; 

-- M6 Return Rate 

DELETE FROM bi.retention_kpi WHERE date = cast(crunchdate as date) - INTERVAL '1 days';

INSERT INTO bi.retention_kpi 
SELECT
	cast(crunchdate as date) - INTERVAL '1 days' as Date,
	'M6 Stats'::text as kpi,
   CASE WHEN 	SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN booking_done_last28d ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN booking_done_last28d ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3) ELSE 0 END as _Return_Rate,
   CASE WHEN 	SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' THEN booking_done_last28d ELSE 0 END) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval '180 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as Activitiy_Rate,
	SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '210 days' and cast(crunchdate as date) - interval  '180 days' THEN booking_done_last28d ELSE 0 END) as Returning_Customer,
	locale
FROM
  bi.Running_Cohort_Analysis
GROUP BY
	locale;

DROP TABLE IF EXISTS bi.retention_marketing;
CREATE TABLE bi.retention_marketing AS
  
  SELECT

    t1.date::timestamp::date as date,
    t1.city::text as city,
    ''::text as locale,
    ''::text as channel,
    'p1'::text as period,
    'City analysis' as kpi_type,
    0::numeric as acquisitions,
    0::numeric as rebookings,
    0::numeric as returning_customer,
    sum(round(t1.cohort_return_rate::numeric,3)) as cohort_return_rate,
    sum(round(t1.p0_activitiy_rate::numeric,3)) as p0_activity_rate,
    sum(round(t1.p1_activity_rate::numeric,3)) as p1_activity_rate

  FROM bi.Running_Cohort_Analysis_City2 t1

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

INSERT INTO bi.retention_marketing
  SELECT

    t2.date::timestamp::date as date,
    cast(t2.city as text) as city,
    '' as locale,
    ''::text as channel,
    'p3'::text as period,
    'City analysis' as kpi_type,
    0::numeric as acquisitions,
    0::numeric as rebookings,
    0::numeric as returning_customer,
    sum(round(t2.cohort_return_rate::numeric,3)) as cohort_return_rate,
    0::numeric as p0_activity_rate,
    sum(round(t2.p1_activity_rate::numeric,3)) as p1_activity_rate

  FROM bi.Running_Cohort_Analysis_City2p3 t2

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

INSERT INTO bi.retention_marketing
  SELECT

    t3.date::timestamp::date as date,
    ''::text as city,
    t3.locale as locale,
    ''::text as channel,
    'p1'::text as period,
    'Global' as kpi_type,
    sum(round(t3.acquisitions::numeric,2)) as acquisitions,
    sum(round(t3.rebookings::numeric,2)) as rebookings,
    0::numeric as returning_customer,
    sum(round(t3.cohort_return_rate::numeric,3)) as cohort_return_rate,
    sum(round(t3.p0_activitiy_rate::numeric,3)) as p0_activity_rate,
    sum(round(t3.p1_activitiy_rate::numeric,3)) as p1_activity_rate

  FROM bi.Running_Cohort_Analysis_Email t3

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

INSERT INTO bi.retention_marketing
  SELECT

    t4.date::timestamp::date as date,
    ''::text as city,
    t4.locale as locale,
    ''::text as channel,
    'p3'::text as period,
    'Global' as kpi_type,
    sum(round(t4.acquisitions::numeric,2)) as acquisitions,
    0::numeric as rebookings,
    0::numeric as returning_customer,
    sum(round(t4.cohort_return_rate::numeric,3)) as cohort_return_rate,
    0::numeric as p0_activity_rate,
    sum(round(t4.p1_activitiy_rate::numeric,3)) as p1_activity_rate

  FROM bi.Running_Cohort_Analysis_Emailp3 t4

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

INSERT INTO bi.retention_marketing
  SELECT

    t5.date::timestamp::date as date,
    ''::text as city,
    t5.locale as locale,
    t5.acquisition_marketing_channel::text as channel,
    'p1'::text as period,
    'Channel analysis' as kpi_type,
    sum(round(t5.acqusitions::numeric,2)) as acquisitions,
    0::numeric as rebookings,
    0::numeric as returning_customer,
    sum(round(t5.cohort_return_rate::numeric,3)) as cohort_return_rate,
    sum(round(t5.p0_activitiy_rate::numeric,3)) as p0_activity_rate,
    sum(round(t5.p1_activity_rate::numeric,3)) as p1_activity_rate

  FROM bi.Running_Cohort_Analysis_Marketing2 t5

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

INSERT INTO bi.retention_marketing
  SELECT

    t6.date::timestamp::date as date,
    ''::text as city,
    t6.locale as locale,
    t6.acquisition_marketing_channel::text as channel,
    'p3'::text as period,
    'Channel analysis' as kpi_type,
    sum(round(t6.acquisitions::numeric,2)) as acquisitions,
    0::numeric as rebookings,
    0::numeric as returning_customer,
    sum(round(t6.cohort_return_rate::numeric,3)) as cohort_return_rate,
    0::numeric as p0_activity_rate,
    sum(round(t6.p1_activity_rate::numeric,3)) as p1_activity_rate

  FROM bi.Running_Cohort_Analysis_Marketing2p3 t6

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

INSERT INTO bi.retention_marketing
  SELECT

    t7.date::timestamp::date as date,
    ''::text as city,
    t7.locale as locale,
    ''::text as channel,
    'p6'::text as period,
    'Global' as kpi_type,
    0::numeric as acquisitions,
    0::numeric as rebookings,
    sum(returning_customer::numeric) as returning_customer,
    sum(round(t7._return_rate::numeric,3)) as cohort_return_rate,
    sum(round(t7.activitiy_rate::numeric,3)) as p0_activity_rate,
    0::numeric as p1_activity_rate

  FROM bi.retention_kpi t7

  GROUP BY date, city, locale, channel, period, kpi_type

  ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'