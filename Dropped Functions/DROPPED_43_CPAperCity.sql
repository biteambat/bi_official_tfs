SELECT

	EXTRACT(year from o.order_creation__c) as yearnum,
	EXTRACT(month from o.order_creation__c) as monthnum,
	o.order_creation__c::timestamp::date as daynum,
	o.delivery_area as city,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Facebook' THEN 1 ELSE NULL END) as Fb_acquisitions,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'SEM' THEN 1 ELSE NULL END) as SEM_acquisitions,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Display' THEN 1 ELSE NULL END) as Display_acquisitions,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Voucher Campaigns' THEN 1 ELSE NULL END) as Vouchers_acquisitions,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Facebook' AND o.status not like ('%CANCELLED%') THEN o.discount__c ELSE NULL END) as Fb_discount,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'SEM' AND o.status not like ('%CANCELLED%') THEN o.discount__c ELSE NULL END) as SEM_discount,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Display' AND o.status not like ('%CANCELLED%') THEN o.discount__c ELSE NULL END) as Display_discount,
	SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Voucher Campaigns' AND o.status not like ('%CANCELLED%') THEN o.discount__c ELSE NULL END) as Vouchers_costs

FROM
	bi.orders o

WHERE o.test__c = '0'
	AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE','CANCELLED NOT THERE YET')
	AND o.order_creation__c < current_date and o.order_creation__c >= (current_date - interval '3 months')
	
GROUP BY yearnum, monthnum, daynum, o.delivery_area

ORDER BY yearnum desc, monthnum desc, daynum desc, city asc