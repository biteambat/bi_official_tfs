
CREATE OR REPLACE FUNCTION bi.daily$b2b_dailyrunrate(crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$b2b_dailyrunrate';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

-- Signed Customers


DROP TABLE IF EXISTS bi.b2b_daily_kpis;
CREATE TABLE bi.b2b_daily_kpis as 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale;
	
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED')
GROUP BY
	date,
	locale,
	sub_kpi;	
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'New Customer churn' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;

-- DE Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'59' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'65' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'17' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'48' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7' as value;

-- NL Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'5' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- CH Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'11' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'12' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'5' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1' as value;

-- New Customer End

-- New Hours Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	left(locale,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(hours)/SUM(customers) as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	locale,
	date;


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount)/SUM(Hours)
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;



-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1040' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'936' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'104' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('27.8' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'16' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'144' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'131' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('12' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('20.8' as decimal) as value;


-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'144' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'132' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'12' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('12' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('38.9' as decimal) as value;


-- New Hours END

-- New Revenue Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(amount)/sum(clients) as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as clients
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;
	
-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'26000' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'23400' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2600' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'400' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'3127' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'3000' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'344' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'250' as value;

-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2880' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2640' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'240' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'240' as value;


-- New Revenue END

-- TOtal Customers START

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01'::date as date,
	LEFT(t1.locale__c,2) as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.orders t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunityid = t2.sfid)
WHERE
	TO_CHAR(effectivedate::date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and status not like '%CANCELLED%'
	and order_type = '2'
	and stagename != 'IRREGULAR'
	and recurrency__c > '0'
GROUP BY
	Locale;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'185' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'ch' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'27' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'33' as value;


-- Summary

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Runrate on Target% till now' as text) as type,
	CASE WHEN (SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays)))*100 > 0 THEN
	
	round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/(SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays))))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis,
	(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-10-01' as date,
	locale,
	kpi,
		sub_kpi,
	CAST('Runrate on Target%' as text) as type,
	CASE WHEN SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END) > 0 THEN round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Achieved' as varchar) as type,
	CAST(SUM(Value) as decimal)/MAX(current_days) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Target' as varchar) as type,
	CAST(SUM(Value) as decimal)/MAX(alldays) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Target'
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	date,
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Projected Achievement' as varchar) as type,
	(CAST(SUM(Value) as decimal)/MAX(current_days))*(MAX(alldays)-MAX(current_days))+SUM(Value) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'Current Days' as type,
	MAX(current_days) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;
 	
 INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'All Days' as type,
	MAX(alldays) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
  
 END;

$BODY$ LANGUAGE 'plpgsql'