CREATE OR REPLACE FUNCTION bi.daily$vouchers_redemptions(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$vouchers_redemptions';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	DROP TABLE IF EXISTS bi.vouchers_redemptions_L12Months;
	CREATE TABLE bi.vouchers_redemptions_L12Months AS

		SELECT 
			voucher__c,
			MAX(order_creation__c::date)::date as last_redemption, 
			COUNT(DISTINCT CASE WHEN order_creation__c::date between (current_date - interval '30 days') and current_date THEN sfid ELSE NULL END) as redemptions_l30D

		FROM bi.orders
			WHERE test__c = '0'
			AND order_creation__c::date > (current_date - interval '1 year')

		GROUP BY voucher__c

		ORDER BY voucher__c asc

	;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'