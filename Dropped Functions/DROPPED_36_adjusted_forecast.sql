CREATE OR REPLACE FUNCTION bi."daily$adjusted_GMV_forecast"(crunchdate date)
RETURNS void AS
$BODY$

DECLARE 
function_name varchar := 'bi.daily$adjusted_GMV_forecast';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	-- first run rate calculation and number of days

	DROP TABLE IF EXISTS bi.booking_forecast_1;
	CREATE TABLE bi.booking_forecast_1 as 
	SELECT

		LEFT(locale__c,2) as locale,
		round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/14),0) as Acquisition_Runrate_Per_Day,
		round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/14),0) as Rebookings_Runrate_Per_Day,

		DATE_PART('days', 
	        DATE_TRUNC('month', NOW()) 
	        + '1 MONTH'::INTERVAL 
	        - DATE_TRUNC('month', NOW())
	    ) as month_days,

	   Extract(day from current_date-1) as days
	FROM
		bi.orders
	WHERE
		Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
	  	AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days') 
	  	and order_type = '1'
	GROUP BY
		LEFT(locale__c,2);

	-- creation of the list of daily GMVs per day during the last 15 days and per country

	DROP TABLE IF EXISTS bi.runrateadjustement_temp;
	CREATE TABLE bi.runrateadjustement_temp as
		
		SELECT
			t1.Order_Creation__c::date as date,
			LEFT(t1.locale__c,2) as locale,
			SUM(CASE WHEN t1.Acquisition_New_Customer__c = '1' and (t1.Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or t1.Voucher__c = '' or t1.Voucher__c is null) THEN t1.GMV_Eur ELSE 0 END) as daily_gmv_acquisitions,
			AVG(t2.Acquisition_Runrate_Per_Day) as rr_acquisitions,
			SUM(CASE WHEN t1.Acquisition_New_Customer__c = '0' and (t1.Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or t1.Voucher__c = '' or t1.Voucher__c is null) THEN t1.GMV_Eur ELSE 0 END)as daily_gmv_rebookings,
			AVG(t2.Rebookings_Runrate_Per_Day) as rr_rebookings

		FROM bi.orders t1

		JOIN bi.booking_forecast_1 t2 on left(t1.locale__c,2) = t2.locale 

		WHERE
			Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
	  		AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days')
	  		and order_type = '1'

	  	GROUP BY t1.Order_Creation__c::date, left(t1.locale__c,2)
	;

	DROP TABLE IF EXISTS bi.variation_coefficient_GMV_temp;
	CREATE TABLE bi.variation_coefficient_GMV_temp AS
		
		SELECT
			locale,
			STDDEV(daily_gmv_acquisitions) as stddev_acquisitionsgmv,
			STDDEV(daily_gmv_acquisitions) / AVG(rr_acquisitions) as var_coeff_acquisitions,
			STDDEV(daily_gmv_rebookings) as stddev_rebookingsgmv,
			STDDEV(daily_gmv_rebookings) / AVG(rr_rebookings) as var_coeff_rebookings
		FROM bi.runrateadjustement_temp

		GROUP BY locale

	;
	-- Readjustment of the run rate by excluding extreme values

	DROP TABLE IF EXISTS bi.forecast_gmv_adjusted;
	CREATE TABLE bi.forecast_gmv_adjusted AS

		SELECT
		bi.runrateadjustement_temp.locale as locale,
		
		AVG(

			CASE WHEN (daily_gmv_acquisitions >= (1-var_coeff_acquisitions)*rr_acquisitions AND daily_gmv_acquisitions <= (1+var_coeff_acquisitions)*rr_acquisitions) OR EXTRACT(DOW from date::timestamp::date) = 0 THEN daily_gmv_acquisitions 
				WHEN daily_gmv_acquisitions < (1-var_coeff_acquisitions)*rr_acquisitions AND EXTRACT(DOW from date::timestamp::date) > 0 THEN (1-var_coeff_acquisitions)*rr_acquisitions
				WHEN daily_gmv_acquisitions > (1+var_coeff_acquisitions)*rr_acquisitions AND EXTRACT(DOW from date::timestamp::date) > 0 THEN (1+var_coeff_acquisitions)*rr_acquisitions 
				END
		) 
		as rr_acquisitions_adjusted,
		
		AVG(

			CASE WHEN (daily_gmv_rebookings >= (1-var_coeff_rebookings)*rr_rebookings AND daily_gmv_rebookings =< (1+var_coeff_rebookings)*rr_rebookings) OR EXTRACT(DOW from date::timestamp::date) = 0 THEN daily_gmv_rebookings 
			WHEN daily_gmv_rebookings < (1-var_coeff_rebookings)*rr_rebookings AND EXTRACT(DOW from date::timestamp::date) > 0 THEN (1-var_coeff_rebookings)*rr_rebookings 
			WHEN daily_gmv_rebookings > (1+var_coeff_rebookings)*rr_rebookings AND EXTRACT(DOW from date::timestamp::date) > 0 THEN (1+var_coeff_rebookings)*rr_rebookings
			END

		) 
		as rr_rebookings_adjusted
		

		FROM bi.runrateadjustement_temp JOIN bi.variation_coefficient_GMV_temp ON bi.runrateadjustement_temp.locale = bi.variation_coefficient_GMV_temp.locale

		GROUP BY  bi.runrateadjustement_temp.locale

		ORDER BY  bi.runrateadjustement_temp.locale asc
	;

	-- calculation of GMV amounts for acquisitions and rebookings during current month

	DROP TABLE IF EXISTS bi.booking_forecast_2;
	CREATE TABLE bi.booking_forecast_2 as 
	SELECT
		LEFT(locale__c,2) as locale,
		SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN GMV_Eur ELSE 0 END) as Acquisition_hours_this_month,
		SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN GMV_Eur ELSE 0 END) as Rebookings_hours_this_month
	FROM
		bi.orders
	WHERE
		 Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','NOSHOW CUSOTMER','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
	  	 AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM ORder_Creation__c::date) and EXTRACT(Year FROM ORder_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
	  	 and order_type = '1'
	GROUP BY
		left(locale__c,2);


	-- final forecast calculation


	DROP TABLE IF EXISTS bi.booking_forecast_adjusted;
	CREATE TABLE bi.booking_forecast_adjusted as 
		SELECT
			t1.locale,
			t1.Acquisition_hours_this_month,
			t3.rr_acquisitions_adjusted,
		    t1.Acquisition_hours_this_month+(t3.rr_acquisitions_adjusted*(t2.month_days-t2.days)) Acquisition_Forecast,
			t1.Rebookings_hours_this_month,
			t3.rr_rebookings_adjusted,
			t1.Rebookings_hours_this_month+(t3.rr_rebookings_adjusted*(t2.month_days-t2.days)) as rebookings_Forecast
		FROM
			bi.booking_forecast_2 t1
		LEFT JOIN
			bi.booking_forecast_1 t2 ON (t1.locale = t2.locale)
		LEFT JOIN
			bi.forecast_gmv_adjusted t3 ON (t1.locale = t3.locale)

		GROUP BY
			t1.locale,
			t1.Acquisition_hours_this_month,
			t3.rr_acquisitions_adjusted,
			Acquisition_Forecast,
			t1.Rebookings_hours_this_month,
			t3.rr_rebookings_adjusted,
			Rebookings_Forecast

	;

	DROP TABLE IF EXISTS bi.booking_forecast_1;
	DROP TABLE IF EXISTS bi.booking_forecast_2;
	DROP TABLE IF EXISTS bi.runrateadjustement_temp;
	DROP TABLE IF EXISTS bi.forecast_gmv_adjusted;
	DROP TABLE IF EXISTS bi.variation_coefficient_GMV_temp;

	--per day, valid GMV VS valid GMV kept 

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;
$BODY$
LANGUAGE plpgsql