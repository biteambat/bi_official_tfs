bi.forecast_sales_ INSERTed rows for 2017

-- ------------------------------------------------------------------------------------------------------------------------- 2017-12
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','SME','Sales','Inbound','39'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','SME','Sales','Outbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','SME','Funnel','Inbound','15'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','KA','Sales','Outbound','1'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','KA','Sales','Outbound','3');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-11
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','SME','Sales','Inbound','39'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','SME','Sales','Outbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','SME','Funnel','Inbound','15'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','KA','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','KA','Sales','Outbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','KA','Sales','Outbound','3');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average PPH','KA','Sales','Outbound',0.00);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-10
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','KA','Sales','Inbound','1');

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','SME','Sales','Inbound','39');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','SME','Sales','Outbound','20');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','SME','Funnel','Inbound','15');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','KA','Sales','Outbound','3');	

-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average PPH','KA','Sales','Outbound',0.00);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average PPH','KA','Sales','Outbound',25.90);

-- -------------------------------------------------------------------------------------------------------------------------  2017-09
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Inbound','1');

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','SME','Sales','Inbound','44');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','SME','Sales','Outbound','24');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','SME','Funnel','Inbound','22');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','KA','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','KA','Sales','Outbound','2');

-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','SME','Sales','Inbound','374');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','SME','Sales','Outbound','374');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','SME','Funnel','Inbound','374');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average Revenue','SME','Sales','Inbound','368');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average Revenue','SME','Sales','Outbound','368');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average PPH','KA','Sales','Outbound',0.00);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average PPH','KA','Sales','Outbound',25.90);

-- -------------------------------------------------------------------------------------------------------------------------  2017-08
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','KA','Sales','Inbound','1');

-- -------------------------------------------------------------------------------------------------------------------------  KPI Targets
-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Sales','Inbound','41');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Sales','Outbound','20');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Funnel','Inbound','22');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','KA','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','KA','Sales','Outbound','1');

-- FTE Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Sales','Inbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Sales','Outbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Funnel','Inbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','SME','Sales','Inbound','379');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','SME','Sales','Outbound','379');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','KA','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-07
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','SME','Sales','Inbound','68'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','SME','Sales','Outbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','SME','Funnel','Inbound','22'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','KA','Sales','Outbound','2'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','SME','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','SME','Sales','Inbound','330');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','SME','Sales','Outbound','330');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','SME','Funnel','Inbound','330');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average Revenue','SME','Sales','Inbound','388');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average Revenue','SME','Sales','Outbound','388');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-06
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','SME','Sales','Inbound','35'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','SME','Sales','Outbound','18'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','SME','Funnel','Inbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','KA','Sales','Outbound','2'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','SME','Sales','Outbound','8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','SME','Sales','Inbound','317');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','SME','Sales','Outbound','317');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','SME','Funnel','Inbound','317');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','KA','Sales','Outbound','10000');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-05
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','SME','Sales','Inbound','45'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','SME','Sales','Outbound','10'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','SME','Funnel','Inbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','SME','Sales','Outbound','13');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','SME','Sales','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','SME','Sales','Outbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','SME','Funnel','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average PPH','SME','Sales','Inbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average PPH','SME','Sales','Outbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average PPH','KA','Sales','Outbound',27.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-04
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','SME','Sales','Inbound','45'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','SME','Sales','Outbound','25'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','SME','Sales','Outbound','13');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','SME','Sales','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','SME','Sales','Outbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','SME','Funnel','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average PPH','SME','Sales','Inbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average PPH','SME','Sales','Outbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average PPH','KA','Sales','Outbound',27.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-03
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','SME','Sales','Outbound','8');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','SME','Sales','Outbound','8');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','SME','Sales','Inbound','45'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','SME','Sales','Outbound','25'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','SME','Sales','Outbound','13');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','SME','Sales','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','SME','Sales','Outbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','SME','Funnel','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average PPH','SME','Sales','Inbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average PPH','SME','Sales','Outbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average PPH','KA','Sales','Outbound',27.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-02
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','SME','Sales','Inbound','46'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','SME','Sales','Outbound','25'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','SME','Sales','Inbound','2'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','SME','Sales','Outbound','8'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','SME','Sales','Inbound','9');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','SME','Sales','Outbound','9');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','SME','Sales','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','SME','Sales','Outbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','SME','Funnel','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average Revenue','SME','Sales','Inbound','700.2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average Revenue','SME','Sales','Inbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average Revenue','SME','Sales','Outbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','SME','Sales','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','SME','Sales','Outbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','SME','Funnel','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','KA','Sales','Outbound',23);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average PPH','SME','Sales','Inbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average PPH','SME','Sales','Outbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average PPH','KA','Sales','Outbound',27.70);


-- ------------------------------------------------------------------------------------------------------------------------- 2017-01
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','SME','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','SME','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','SME','Sales','Inbound','14'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','SME','Sales','Outbound','46'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','SME','Sales','Inbound','2'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','SME','Sales','Outbound','8'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','SME','Sales','Inbound','3');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','SME','Sales','Outbound','22');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','SME','Sales','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','SME','Sales','Outbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','SME','Funnel','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average Revenue','SME','Sales','Inbound','700.2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average Revenue','SME','Sales','Inbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average Revenue','SME','Sales','Outbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','SME','Sales','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','SME','Sales','Outbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','SME','Funnel','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','KA','Sales','Outbound',23);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average PPH','SME','Sales','Inbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average PPH','SME','Sales','Outbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average PPH','KA','Sales','Outbound',27.70);