/*
Procedure running every day to generate the forecasts used in BAT Topline - Forecasts Current Month
*/
CREATE OR REPLACE FUNCTION bi.daily$forecast_test (crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$forecast_test';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

DROP TABLE IF EXISTS bi.temp_booking_forecast_valid;
DROP TABLE IF EXISTS bi.temp_booking_forecast_booked;
DROP TABLE IF EXISTS bi.temp_currentmonth_valid;
DROP TABLE IF EXISTS bi.temp_currentmonth_booked;
DROP TABLE IF EXISTS bi.temp_forecast_test_vmt;

-------------------------------------------------------------

--RunrateValid

CREATE TABLE bi.temp_booking_forecast_valid as
SELECT
	LEFT(locale__c,2) as locale,
	CASE WHEN type = '60' THEN 'Muffins' ELSE 'Freelance' END as tiger_type,
	
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/15),0) 
	as Acquisition_DRR_GMV,
	
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/15),0) 
	as Rebookings_DRR_GMV,

	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN order_duration__c ELSE 0 END) as decimal)/15),0) 
	as Acquisition_DRR_Hours,
	
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN order_duration__c ELSE 0 END) as decimal)/15),0) 
	as Rebookings_DRR_Hours,

	DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,

   CASE WHEN Extract(day from current_date) = 1 THEN 0 ELSE Extract(day from current_date-1) END as days

FROM
	bi.orders
WHERE
	Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  	AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days') 
  	AND test__c = '0'
  	AND order_type = '1'
GROUP BY
	locale, 
	tiger_type;

-- ActualValid

DROP TABLE IF EXISTS bi.temp_currentmonth_valid;
CREATE TABLE bi.temp_currentmonth_valid as 
SELECT
	LEFT(locale__c,2) as locale,
	CASE WHEN type = '60' THEN 'Muffins' ELSE 'Freelance' END as tiger_type,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN GMV_Eur ELSE 0 END) as Acquisition_GMV_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN GMV_Eur ELSE 0 END) as Rebookings_GMV_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN order_duration__c ELSE 0 END) as Acquisition_hours_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN order_duration__c ELSE 0 END) as Rebookings_hours_this_month
FROM
	bi.orders
WHERE
	 Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','NOSHOW CUSOTMER','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
  	 AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM Order_Creation__c::date) and EXTRACT(Year FROM Order_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
  	 AND test__c = '0'
  	 AND type not like ('%222%')
  	 and order_type = '1'
  	 
GROUP BY
	locale, 
	tiger_type;

--ForecastValid

DROP TABLE IF EXISTS bi.forecast_performance;
CREATE TABLE bi.forecast_performance AS
SELECT
	t1.locale as country,
	CAST('Valid GMV' as text) as kpi,
	t1.tiger_type as tiger_type,
	(t3.Acquisition_GMV_this_month + (t1.Acquisition_DRR_GMV * (t1.month_days - t1.days))) as Acquisition_GMV_Forecast,
			(t3.Rebookings_GMV_this_month + (t1.Rebookings_DRR_GMV * (t1.month_days - t1.days))) as Rebookings_GMV_Forecast
FROM 

	bi.temp_booking_forecast_valid t1
JOIN bi.temp_currentmonth_valid t3 ON (t3.locale = t1.locale and t3.tiger_type = t1.tiger_type)

GROUP BY 
	t1.locale, 
	t1.tiger_type,
 	Acquisition_GMV_Forecast,
 	Rebookings_GMV_Forecast
ORDER BY
	country asc,
	tiger_type desc;
	
INSERT INTO bi.forecast_performance
SELECT
	t1.locale as country,
	'Valid Hours' as kpi,
	t1.tiger_type as tiger_type,
	(t3.Acquisition_hours_this_month + (t1.Acquisition_DRR_Hours * (t1.month_days - t1.days))) as Acquisition_Hours_Forecast,
	(t3.Rebookings_hours_this_month + (t1.Rebookings_DRR_Hours * (t1.month_days - t1.days))) as Rebookings_Hours_Forecast
FROM 

	bi.temp_booking_forecast_valid t1
JOIN bi.temp_currentmonth_valid t3 ON (t3.locale = t1.locale and t3.tiger_type = t1.tiger_type)

GROUP BY 
	t1.locale, 
	t1.tiger_type,
 	Acquisition_Hours_Forecast,
 	Rebookings_Hours_Forecast
ORDER BY
	country asc,
	tiger_type desc;

------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------

--RunrateBooked

CREATE TABLE bi.temp_booking_forecast_booked as
SELECT
	LEFT(locale__c,2) as locale,
	CASE WHEN type = '60' THEN 'Muffins' ELSE 'Freelance' END as tiger_type,
	
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/15),0) 
	as Acquisition_DRR_GMV,
	
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/15),0) 
	as Rebookings_DRR_GMV,

	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN order_duration__c ELSE 0 END) as decimal)/15),0) 
	as Acquisition_DRR_Hours,
	
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN order_duration__c ELSE 0 END) as decimal)/15),0) 
	as Rebookings_DRR_Hours,

	DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,

   CASE WHEN Extract(day from current_date) = 1 THEN 0 ELSE Extract(day from current_date-1) END as days

FROM
	bi.orders
WHERE
	Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') 
  	AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days') 
  	AND test__c = '0'
  	AND type not like ('%222%')
GROUP BY
	locale, 
	tiger_type;

-- ActualBooked

DROP TABLE IF EXISTS bi.temp_currentmonth_booked;
CREATE TABLE bi.temp_currentmonth_booked as 
SELECT
	LEFT(locale__c,2) as locale,
	CASE WHEN type = '60' THEN 'Muffins' ELSE 'Freelance' END as tiger_type,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN GMV_Eur ELSE 0 END) as Acquisition_GMV_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN GMV_Eur ELSE 0 END) as Rebookings_GMV_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN order_duration__c ELSE 0 END) as Acquisition_hours_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN order_duration__c ELSE 0 END) as Rebookings_hours_this_month
FROM
	bi.orders
WHERE
	 Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  	 AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM Order_Creation__c::date) and EXTRACT(Year FROM Order_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
  	 AND test__c = '0'
  	 AND type not like ('%222%')
GROUP BY
	locale, 
	tiger_type;

--ForecastBooked

INSERT INTO bi.forecast_performance
SELECT
	t1.locale as country,
	'Booked GMV' as kpi,
	t1.tiger_type as tiger_type,
	(t3.Acquisition_GMV_this_month + (t1.Acquisition_DRR_GMV * (t1.month_days - t1.days))) as Acquisition_GMV_Forecast,
			(t3.Rebookings_GMV_this_month + (t1.Rebookings_DRR_GMV * (t1.month_days - t1.days))) as Rebookings_GMV_Forecast
FROM 

	bi.temp_booking_forecast_booked t1
JOIN bi.temp_currentmonth_booked t3 ON (t3.locale = t1.locale and t3.tiger_type = t1.tiger_type)

GROUP BY 
	t1.locale, 
	t1.tiger_type,
 	Acquisition_GMV_Forecast,
 	Rebookings_GMV_Forecast
ORDER BY
	country asc,
	tiger_type desc;
	
INSERT INTO bi.forecast_performance
SELECT
	t1.locale as country,
	'Booked Hours' as kpi,
	t1.tiger_type as tiger_type,
	(t3.Acquisition_hours_this_month + (t1.Acquisition_DRR_Hours * (t1.month_days - t1.days))) as Acquisition_Hours_Forecast,
	(t3.Rebookings_hours_this_month + (t1.Rebookings_DRR_Hours * (t1.month_days - t1.days))) as Rebookings_Hours_Forecast
FROM 

	bi.temp_booking_forecast_booked t1
JOIN bi.temp_currentmonth_booked t3 ON (t3.locale = t1.locale and t3.tiger_type = t1.tiger_type)

GROUP BY 
	t1.locale, 
	t1.tiger_type,
 	Acquisition_Hours_Forecast,
 	Rebookings_Hours_Forecast
ORDER BY
	country asc,
	tiger_type desc;

DROP TABLE IF EXISTS bi.temp_booking_forecast_valid;
DROP TABLE IF EXISTS bi.temp_booking_forecast_booked;
DROP TABLE IF EXISTS bi.temp_currentmonth_valid;
DROP TABLE IF EXISTS bi.temp_currentmonth_booked;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'