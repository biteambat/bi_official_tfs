DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$topline_cityboard(crunchdate date) RETURNS void AS 

$BODY$
DECLARE 
function_name varchar := 'bi.daily$topline_cityboard';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

-- Gross Profit Margin Calc.

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	a.sfid as professional__c,
	a.delivery_areas__c as delivery_areas,
	a.type__c,
	a.hr_contract_start__c::date as contract_start,
	a.hr_contract_end__c::date as contract_end,
	a.hr_contract_weekly_hours_min__c as weekly_hours
    FROM
        Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
	and  a.test__c = '0'
	and left(a.locale__c,2) = 'de'
GROUP BY
	a.sfid,
	a.type__c,
	a.delivery_areas__c,
	a.hr_contract_start__c,
	a.hr_contract_end__c,
	a.hr_contract_weekly_hours_min__c;

DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV__c*1.19 ELSE 0 END) as GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV__c*1.19 ELSE 0 END) as b2b_gmv,
	SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END) as Hours,
		SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END) as b2c_hours,
		SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END) as b2b_hours
		
FROM
	Salesforce.Order t1
GROUP BY
	t1.professional__c,
	date;

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours
FROM
	bi.cleaner_Stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);

DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner as a,
	(select i::date as date from generate_series('2016-01-01', 
  '2017-12-12', '1 day'::interval) i) as b
WHERE
	status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN t1.type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN t1.type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM 
	bi.holidays_cleaner_2 t1
JOIN
	Salesforce.Account t2
ON
	(t1.account__c = t2.sfid and t1.date < t2.hr_contract_end__c)
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_per_cleaner;
CREATE TABLE bi.holidays_per_cleaner as 
SELECT
	t1.*,
	t2.delivery_areas__c as delivery_area,
	COP
FROM
	bi.holidays_cleaner_3 t1
LEFT JOIN  
	Salesforce.Account t2
ON
	(t1.account__c = t2.sfid )
LEFT JOIN
	(SELECT
		delivery_areas__c as delivery_area,
		COUNT(DISTINCT(sfid)) as COP
	FROM
		Salesforce.Account
	WHERE
		status__c in ('ACTIVE','BETA')
		and test__c = '0'
	GROUP BY
		delivery_areas__c) as b
ON
	(t2.delivery_areas__c = b.delivery_area);

DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
	and date < current_date
GROUP BY
	date,
	account__c;	


DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
FROM
	bi.holidays_cleaner_3
GROUP BY
	Year_Month,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
		GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
		case
		when year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
		when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-03')::date,contract_start) 
		else calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
		END AS days_worked
from (
SELECT	
   to_char(date,'YYYY-MM') as Year_Month,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
	contract_start,
	contract_end,
	type__c,
	SUM(hours) as worked_hours,
	cast(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
	SUM(GMV) as GMV,
	SUM(GMV/1.19) as total_revenue,
	SUM(B2C_GMV/1.19) as B2C_Revenue,
	SUM(B2B_GMV/1.19) as B2B_Revenue,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2b_share,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2c_share,
	MAX(Weekly_hours) as weekly_hours,
	MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))/7 )as monthly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Month,
	professional__c,
	contract_start,
	days_of_month,
	type__c,
	contract_end,
	delivery_area,
	calc_end
	) xx;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month);


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp as 
select
    year_month,
    professional__c,
    delivery_area,
    worked_hours,
    type__c,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as holidays_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
	 CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN 	((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.25 ELSE ((monthly_hours/days_of_month)*days_worked)*12.25 END as salary_payed,
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV/1.19) as Revenue,
	SUM(GMV/1.19) - SUM(CASE WHEN (weekly_hours/5)*(sickness+holiday)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN 	((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.25 ELSE (monthly_hours/days_of_month)*days_worked*12.25 END)  as gp
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    year_month,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    holidays_hours,
    days_worked,
    worked_hours;


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3;
CREATE TABLE bi.gpm_per_cleaner_v3 as 
select
    year_month,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    min(mindate) as mindate,
    holiday,
    holidays_hours,
    working_hours,
	total_hours,
	salary_payed,
	SUM(b2c_revenue) as b2c_revenue,
	SUM(b2b_revenue) as b2b_revenue,
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-salary_payed)
	ELSE b2b_revenue-(salary_payed*b2b_share) END) as b2b_gp,
		SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-salary_payed)
	ELSE b2c_revenue-(salary_payed*b2c_share) END) as b2c_gp,
	gp,
	GMV,
	Revenue
from
     bi.gpm_per_cleaner_v3_temp
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    year_month,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    total_hours,
    holidays_hours,
    salary_payed,
    gp,
    gmv,
    revenue,
    days_worked,
    worked_hours;



-- NL GPM & Utilization

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.sfid as professional__c,
	delivery_areas__c as delivery_areas,
	hr_contract_start__c::date as contract_start,
	hr_contract_end__c::date as contract_end,
	hr_contract_weekly_hours_min__c as weekly_hours,
	type__c
FROM
	Salesforce.Account t1
WHERE
	t1.test__c = '0'
	and left(locale__c,2) = 'nl'
GROUP BY
	t1.sfid,
	delivery_areas__c,
	hr_contract_start__c,
	hr_contract_end__c,
	hr_contract_weekly_hours_min__c,
	type__c;

DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c/1.06
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV__c ELSE 0 END) as GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c/1.06 ELSE 0 END) as B2C_GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV__c ELSE 0 END) as 	B2B_GMV,
	SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END) as B2C_Hours,
	SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c 		ELSE 0 END) as B2B_Hours
FROM
	bi.orders t1
WHERE
	left(locale__c,2) = 'nl'
GROUP BY
	t1.professional__c,
	date;


	
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	date,
	gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours,
	type__c
FROM
	bi.cleaner_Stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner;

DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and date < current_date::date
	and status__c in ('approved');


DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(sickness_flag) as sickness
FROM
	bi.holidays_cleaner_3
GROUP BY
	Year_Month,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;




DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
		GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
		case
		when year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
		when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-03')::date,contract_start) 
		else calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
		END AS days_worked
from (
SELECT	
   to_char(date,'YYYY-MM') as Year_Month,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	type__c,
	DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
	contract_start,
	contract_end,
	SUM(hours) as worked_hours,
	cast(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
	SUM(GMV) as GMV,
	SUM(GMV) as total_revenue,
	SUM(B2C_GMV) as B2C_Revenue,
	SUM(B2B_GMV) as B2B_Revenue,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2b_share,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2c_share,
	MAX(Weekly_hours) as weekly_hours,
	MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))/7 )as monthly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Month,
   type__c,
	professional__c,
	contract_start,
	days_of_month,
	contract_end,
	delivery_area,
	calc_end
	) xx;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month);


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp as 
select
    Year_month,
    professional__c,
    delivery_area,
    worked_hours,
    type__c,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    min(mindate) as mindate,
    0 as holiday,
    (weekly_hours/5)*(sickness) as holidays_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness)+worked_hours as total_hours,
	 CASE WHEN ((weekly_hours/5)*(sickness))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN 	((weekly_hours/5)*(sickness)+worked_hours)*14.73 ELSE ((monthly_hours/days_of_month)*days_worked)*14.73 END as salary_payed,
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV) as Revenue,
		SUM(GMV) - SUM(CASE WHEN (weekly_hours/5)*(sickness)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN 	((weekly_hours/5)*(sickness)+worked_hours)*14.73 ELSE (monthly_hours/days_of_month)*days_worked*14.73 END)  as gp
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'nl'
    and days_worked > 0
GROUP BY
    year_month,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    holidays_hours,
    days_worked,
    worked_hours;


INSERT INTO bi.gpm_per_cleaner_v3
select
    year_month,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    min(mindate) as mindate,
    0 as holidays,
    (weekly_hours/5)*(sickness) as holidays_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness)+worked_hours as total_hours,
	 CASE WHEN ((weekly_hours/5)*(sickness))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN 	((weekly_hours/5)*(sickness)+worked_hours)*14.73 ELSE ((monthly_hours/days_of_month)*days_worked)*14.73 END as salary_payed,
		SUM(b2c_revenue) as b2c_revenue,
	SUM(b2b_revenue) as b2b_revenue,
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-salary_payed)
	ELSE b2b_revenue-(salary_payed*b2b_share) END) as b2b_gp,
		SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-salary_payed)
	ELSE b2c_revenue-(salary_payed*b2c_share) END) as b2c_gp,
		SUM(GMV) - SUM(CASE WHEN (weekly_hours/5)*(sickness)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN 	((weekly_hours/5)*(sickness)+worked_hours)*14.73 ELSE (monthly_hours/days_of_month)*days_worked*14.73 END)  as gp,
	SUM(GMV) as GMV,
	SUM(GMV) as Revenue
from
     bi.gpm_per_cleaner_v3_temp
where
    LEFT(delivery_area,2) = 'nl'
    and days_worked > 0
    and professional__c != '0012000001bQCsnAAG'
GROUP BY
    year_month,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holidays_hours,
    days_worked,
    worked_hours;


-- Utilization and GPM done NL

DROP TABLE IF EXISTS bi.gpm_city;
CREATE TABLE bi.gpm_city as   
select
    year_month,
    delivery_area,
    min(mindate) as mindate,
    sum( Revenue) as Revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_per_cleaner_v3
GROUP BY
	year_month,
	delivery_area;

DROP TABLE IF EXISTS bi.gpm_city_over_time;
CREATE TABLE bi.gpm_city_over_time as 	
SELECT
	year_month,
	delivery_area,
	min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_city
GROUP BY
	year_month,
	delivery_area;

INSERT INTO bi.gpm_city_over_time	
SELECT
	year_month,
	'DE',
	min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_city
WHERE
	delivery_area like 'de%'
GROUP BY
	year_month;

	
INSERT INTO bi.gpm_city_over_time	
SELECT
	year_month,
	'NL',
	min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
	SUM(GP) as GP,
	SUM(b2c_gp) as b2c_gp,
	SUM(b2b_gp) as b2b_gp
FROM
	bi.gpm_city
WHERE
	delivery_area like 'nl%'
GROUP BY
	year_month;


-- Topline Dashboard


DROP TABLE IF EXISTS bi.topline_kpi;
CREATE TABLE bi.topline_kpi as 
SELECT
	EXTRACT(Month FROM Order_Creation__C::Date) as Month,
	EXTRACT(Year FROM Order_Creation__C::Date) as Year,
	MIN(Order_Creation__C::Date) as Mindate,
	replace(city, '+', '') as city,
	CAST('Booked GMV' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur) as value
FROM
	bi.orders
WHERE
	status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
		and order_type = '1'
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Order_Creation__C::Date) as Month,
	EXTRACT(Year FROM Order_Creation__C::Date) as Year,
	MIN(Order_Creation__C::Date) as Mindate,
	UPPER(LEFT(locale__c,2)) as locale,
	CAST('Booked GMV' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur) as value
FROM
	bi.orders
WHERE
	Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	CAST('M1 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	city;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	LEFT(city,2) as locale,
	CAST('M1 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	locale;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	CAST('M3 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	city;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	LEFT(city,2) as locale,
	CAST('M3 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	locale;
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	CAST('M6 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	city;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	LEFT(city,2) as locale,
	CAST('M6 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	locale;
	

INSERT INTO bi.topline_kpi		
SELECT
	EXTRACT(MONTH FROM Date) as Month,
	EXTRACT(YEAR FROM Date) as Year,
	MIN(date::date) as mindate,
	city,
	CAST('M1 RR' as text) as kpi,
	CASE WHEN acquisition_Channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
	CASE WHEN SUM(Acquisitions) > 0 THEN SUM(Acquisitions*cohort_Return_rate)/SUM(Acquisitions) ELSE 0 END as value
FROM
		bi.retention_city_marketing
GROUP BY
	Month,
	year,
	city,
	kpi,
	channel;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	city,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	((SUM(GMV_eur)/1.06)-(SUM(Order_Duration__c)*14.62))/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and order_type = '1'
	and LEFT(locale__c,2) = 'nl'
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	LEFT(Locale__c,2) as locale,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	((SUM(GMV_eur)/1.06)-(SUM(Order_Duration__c)*14.62))/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and order_type = '1'
	and LEFT(locale__c,2) = 'nl'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	city as locale,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	((SUM(GMV_eur)/1.19)-(SUM(Order_Duration__c)*12.25))/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and order_type = '1'
	and LEFT(locale__c,2) = 'de'
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	UPPER(LEFT(Locale__c,2)) as locale,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	((SUM(GMV_eur)/1.19)-(SUM(Order_Duration__c)*12.25))/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and order_type = '1'
	and LEFT(locale__c,2) = 'de'
GROUP BY
	Month,
	Year,
	locale;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	city,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	(SUM(GMV_eur)*0.2)/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and LEFT(locale__c,2) = 'at'
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	upper(left(locale__c,2)) as locale,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	(SUM(GMV_eur)*0.2)/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and LEFT(locale__c,2) = 'at'
GROUP BY
	Month,
	Year,
	locale;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	city,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	(SUM(GMV_eur)*0.176)/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and LEFT(locale__c,2) = 'ch'
GROUP BY
	Month,
	Year,
	city;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	'CH',
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	(SUM(GMV_eur)*0.176)/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and LEFT(locale__c,2) = 'ch'
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	CAST('NL' as varchar) as kpi,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	((SUM(GMV_eur)/1.06)-(SUM(Order_Duration__c)*14.62))/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and order_type = '1'
	and LEFT(locale__c,2) = 'nl'
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(Month FROM Effectivedate::Date) as Month,
	EXTRACT(Year FROM Effectivedate::Date) as Year,
	MIN(Effectivedate::Date) as Mindate,
	LEFT(locale__c,2) as locale,
	CAST('Margin per Hour' as text) as kpi,
	CAST('-' as text) as breakdown,
	(SUM(GMV_eur)*0.176)/SUM(Order_Duration__c) as value
FROM
	bi.orders
WHERE
	Status in ('INVOICED','CANCELLED PROFESSIONAL SHORTTERM')
	and LEFT(locale__c,2) = 'ch'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Total Customers' as kpi,
	CAST('B2C' as text) as breakdown,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	citya;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Total Customers' as kpi,
	CAST('B2B' as text) as breakdown,
	COUNT(DISTINCT(contact__c)) as value
FROM
	bi.orders
WHERE
	test__c = '0'
	and status not like '%CANCELLED%' and status not like '%ERROR%'
	and effectivedate::date < current_date::date
	and order_type = '2'
GROUP BY
	Month,
	Year,
	citya;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM Order_Creation__c::Date) as Month,
	EXTRACT(YEAR FROM Order_Creation__c::date) as year,
	min(Order_Creation__c::date) as mindate,
	city,
	'Acquisitions' as kpi,
	marketing_channel as breakdown,
	COUNT(1) as value
FROM
	bi.orders
WHERE
	Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and test__c = '0'
	and Acquisition_New_Customer__c = '1'
	and order_type = '1'
GROUP BY
	Month,
	year,
	city,
	breakdown;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
			case
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__C = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('Cleaner on Platform' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	type__c = 'cleaning-b2c'
	and current_date between hr_contract_start__c and hr_contract_end__c
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	citya;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
			case
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__C = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('Cleaner on Platform' as text) as kpi,
	CAST('B2C&B2B' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	type__c = 'cleaning-b2c;cleaning-b2b'
	and current_date between hr_contract_start__c and hr_contract_end__c
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	citya;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
			case
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__C = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('Cleaner on Platform' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	type__c = 'cleaning-b2b'
	and current_date between hr_contract_start__c and hr_contract_end__c
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	citya;

DELETE FROM  bi.topline_kpi WHERE kpi = 'COP';

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM max_date::date) as Month,
	EXTRACT(YEAR FROM max_date::date) as Year,
	MIN(max_date::date) as mindate,
		case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('COP' as text) as kpi,
	CAST('-' as text) as breakdown,
	COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date < hr_contract_end__c THEN sfid ELSE null END)) as cop
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Month,
	MAX(Date) as max_date
FROM
	(select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
 	Month) as a,
 	(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
 GROUP BY
 	Year,
 	Month,
 	max_date::date,
 	delivery_areas__c;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM max_date::date) as Month,
	EXTRACT(YEAR FROM max_date::date) as Year,
	MIN(max_date::date) as mindate,
		case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('COP' as text) as kpi,
	CAST('-' as text) as breakdown,
	COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and (max_date < hr_contract_end__c or hr_contract_end__c is null) THEN sfid ELSE null END)) as cop
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Month,
	MAX(Date) as max_date
FROM
	(select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
 	Month) as a,
 	(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	) as b
WHERE
	LEFT(b.delivery_areas__c,2) = 'ch'
 GROUP BY
 	Year,
 	Month,
 	max_date::date,
 	delivery_areas__c;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM hr_contract_start__c::date) as Month,
	EXTRACT(YEAR FROM hr_contract_start__c::date) as Year,
	MIN(hr_contract_start__c::date) as mindate,
		case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as City,
	CAST('Onboardings' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
	hr_contract_start__c <= current_date
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM hr_contract_end__c::date) as Month,
	EXTRACT(YEAR FROM hr_contract_end__c::date) as Year,
	MIN(hr_contract_end__c::date) as mindate,
			case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as city,
	CAST('Churn' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
(SELECT
 		a.*
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
	hr_contract_end__c < current_date
	and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
			case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as city,
	CAST('Cleaner on Platform' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	status__c in ('ACTIVE','BETA')
	and LEFT(Locale__c,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	City;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM createddate::date) as Month,
	EXTRACT(YEAR FROM createddate::date) as Year,
	MIN(createddate::date) as mindate,
			case
	when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
	WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
	WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
	WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
	WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
	WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
	when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
	when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
	when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
	when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as city,
	CAST('Onboardings' as text) as kpi,
	CAST('-' as text) as breakdown,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	createddate::date < current_date
	and LEFT(Locale__c,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	City;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as Month,
	EXTRACT(Year FROM mindate) as Year,
	mindate,
	city,
	kpi,
	CAST('-' as text) as breakdown,
	value
FROM
	bi.recurrent_kpis_monthly 
WHERE
	kpi in ('Existing Recurrent','New Recurrent','Recurrent Cancellations');


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	MIN(t2.date) as mindate,
	t1.working_city__c as city,
	CAST('Churn' as text) as kpi,
	CAST('-' as text) as breakdown,
	COUNT(DISTINCT(t1.sfid)) as value
FROM
	Salesforce.Account t1
JOIn
	bi.left_date t2
ON
	(t1.sfid = t2.sfid)
WHERE
	LEFT(locale__c,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	city;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	MIN(effectivedate::date) as mindate,
	t2.city as city,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	MIN(effectivedate::date) as mindate,
	t2.city as city,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('ch','at')
GROUP BY
	Month,
	Year,
	City;

	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
	t2.city as city,
	CAST('GMV per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
	and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
	and LEFT(t2.locale__C,2) in ('de','nl')
GROUP BY
	Month,
	Year,
	City;
		
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
	t2.city as city,
	CAST('Hours per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
	and LEFT(t2.locale__C,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	City;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('-' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
	Month,
	Year,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('-' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
	Month,
	Year,
	locale;
		
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('Recurrent' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and recurrency__c > '6'
		and order_type = '1'
GROUP BY
	Month,
	Year,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('Recurrent' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and recurrency__c > '6'
	and  order_type = '1'
GROUP BY
	Month,
	Year,
	locale;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and (recurrency__c is null or recurrency__c = '0')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('One-Off' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and (recurrency__c is null or recurrency__c = '0')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('B2B' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and (status not like '%CANCELLED%' and status not like '%ERROR%')
	and (effectivedate::date < current_date)
	and type = 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
	'Invoiced Revenue' as kpi,
	CAST('B2B' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and (status not like '%CANCELLED%' and status not like '%ERROR%')
	and (effectivedate::date < current_date)
	and type = 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	locale
	;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Invoiced Revenue' as kpi,
	CAST('B2C' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
	and (effectivedate::date < current_date)
	and type != 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	upper(LEFT(locale__c,2)) as locale,
		'Invoiced Revenue' as kpi,
	CAST('B2C' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
	and (effectivedate::date < current_date)
	and type != 'cleaning-b2b'
GROUP BY
	Month,
	Year,
	locale;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/	(SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) != 'ch'
GROUP BY
	Month,
	Year,
	Citya;

	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '2'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(replace(city, '+', ''),2)) as citya,
	'PPH' as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(replace(city, '+', ''),2)) as citya,
	'PPH' as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and order_type = '2'
GROUP BY
	Month,
	Year,
	Citya;


	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(Locale__c,2)) as citya,
	'PPH' as kpi,
	CAST('-' as text) as breakdown,
		CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/	(SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value

FROM
	bi.orders t2
WHERE
	test__c = '0'
GROUP BY
	Month,
	Year,
	LEFT(Locale__c,2);
	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('Recurrent' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'recurrent'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(Locale__c,2)) as citya,
	'PPH' as kpi,
	CAST('Recurrent' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'recurrent'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	LEFT(Locale__c,2);	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	replace(city, '+', '') as citya,
	'PPH' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM 
	bi.orders t2
WHERE 
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'web'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	Citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	UPPER(LEFT(Locale__c,2)) as citya,
	'PPH' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
	and acquisition_channel__c = 'web'
	and t2.order_type = '1'
GROUP BY
	Month,
	Year,
	LEFT(Locale__c,2);


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM current_date::date) as Month,
	EXTRACT(YEAR FROM current_date::date) as Year,
	MIN(current_date::date) as mindate,
	t2.city as city,
	CAST('GMV per Cleaner' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur)/COUNT(DISTINCT(Professional__c)) as value
FROM
	Salesforce.Account t1
JOIN 
	bi.orders t2
ON
	(t1.sfid = t2.professional__c)
WHERE
	status = 'INVOICED'
	and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
	and LEFT(t2.locale__C,2) in ('at','ch')
GROUP BY
	Month,
	Year,
	City;
	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN sum(revenue) >0 THEN SUM(gp)/sum(revenue) ELSE 0END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN sum(b2b_revenue) >0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN sum(b2c_revenue) >0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	citya;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'DE' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(gp)/sum(revenue) as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%de%'
GROUP BY
	year,
	month;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'DE' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%de%'
GROUP BY
	year,
	month;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'DE' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%de%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'NL' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(gp)/sum(revenue) as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%nl%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'NL' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2B' as text) as breakdown,
	CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%nl%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'NL' as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
	delivery_area like '%nl%'
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT-Vienna' as city,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT-Vienna' as city,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT' as city,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	'AT' as city,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	0.2 as value
FROM
	bi.gpm_city_over_time
GROUP BY
	year,
	month;


INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'CH' as city,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
GROUP BY
	Month,
	Year
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;
	

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'CH' as city,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and order_type = '1'
GROUP BY
	Month,
	Year
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;	

	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon in ('ch-geneva','ch-lausanne')
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;
	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon in ('ch-geneva','ch-lausanne')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('-' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon not in ('ch-geneva','ch-lausanne')
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;		

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdown,
	(((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
ELSE 0 END)/	(SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
	and left(locale__c,2) = 'ch'
	and polygon not in ('ch-geneva','ch-lausanne')
	and order_type = '1'
GROUP BY
	Month,
	Year,
	citya
HAVING
	((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN	Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;	

DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
	first_order_month,
	order_month,
	min(start_Date) as order_date,
	min(first_order_Date) as start_date,
	city,
	acquisition_channel,
	COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
	bi.RecurringCustomerCohort
WHERE
	to_char(current_date,'YYYY-MM') >= 	first_order_month
	and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
	first_order_month,
	order_month,
	city,
	acquisition_channel;

CREATE INDEX IDX333 ON bi.customer_cohorts_exp(first_order_month);

DROP TABLE IF EXISTS bi.customer_cohorts_recurrent;
CREATE TABLE bi.customer_cohorts_recurrent as 	
SELECT
	a.first_order_month as cohort,
	b.order_month,
	b.order_date as mindate,
	CASE 
	WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
	WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
	ELSE 0 END as returning_month,
	a.city,
	a.acquisition_channel,
	a.distinct_customer as total_cohort,
	b.distinct_customer as returning_customer
FROM
(SELECT
	first_order_month,
	order_month,
	start_date,
	order_date,
	city,
	acquisition_channel,
	distinct_customer
FROM
	bi.customer_cohorts_exp
WHERE
	first_order_month = order_month) as a
LEFT JOIN
	(SELECT
	first_order_month,
	order_month,
	start_date,
	order_date,
	city,
	acquisition_channel,
	distinct_customer
FROM
	bi.customer_cohorts_exp
) as b
ON
	(a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel);


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M1 RR Recurrent' as kpi,
	CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	city,
	kpi,
	channel;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M1 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	city,
	kpi;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M3 RR Recurrent' as kpi,
	CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	city,
	kpi,
	channel;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M3 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	city;

DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
	first_order_month,
	order_month,
	min(start_Date) as order_date,
	min(first_order_Date) as start_date,
	left(city,2) as locale,
	city,
	acquisition_channel,
	COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
	bi.RecurringCustomerCohort
WHERE
	to_char(current_date,'YYYY-MM') >= 	first_order_month
	and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
	first_order_month,
	order_month,
	left(city,2),
	city,
	acquisition_channel;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	left(city,2) as locale,
	'M1 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year,
	locale;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	left(city,2) as locale,
	'M3 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year,
	locale,
	kpi;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	left(city,2) as locale,
	'M6 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	locale,
	kpi;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	city,
	'M6 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '6'
GROUP BY
	month,
	year,
	city,
	kpi;

DROP TABLE IF EXISTS bi.cleaner_availability_morning;
CREATE TABLE bi.cleaner_availability_morning as
SELECT
	sfid,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) END as availability_monday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) < 0  THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) END as availability_tuesday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) < 0 THEN 0 ELSE  (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) END as availability_wednesday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) END as availability_thursday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) END as availability_friday,
	CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) END as availability_saturday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time)) END as total_monday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time)) END as total_tuesday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time)) END as total_wednesday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time)) END as total_thursday,	
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time)) END as total_friday,
	CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time)) END as total_saturday
FROM
	Salesforce.Account
WHERE
	Status__c in ('ACTIVE','BETA')
	and type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%';

DROP TABLE IF EXISTS bi.cleaner_ur_temp;
CREATE TABLE bi.cleaner_ur_temp as	
SELECT 
	sfid,
	SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as availability_morning,
	SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) as availability_total,
	CASE WHEN SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) = '0' THEN 0 ELSE CAST(SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as decimal)/SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) END as availability_Share
FROM
	bi.cleaner_availability_morning
GROUP BY
	sfid;


DROP TABLE IF EXISTS bi.order_distribution;
CREATE TABLE bi.order_distribution as 
SELECT
	professional__c,
	to_char(effectivedate::date,'YYYY-MM') as Month,
		SUM(CASE WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and EXTRACT(HOUR FROM Order_End__c+ Interval '2 hours') < 13 THEN Order_Duration__c
				WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and extract(hour from order_end__c+ Interval '2 hours') > 13 THEN 13 - EXTRACT(HOUR FROM order_Start__c+ Interval '2 hours') ELSE 0 END) as Working_Hours_Morning,
				SUM(ORder_Duration__c) as total_hours
FROM
	bi.orders
WHERE
	test__c = '0'
	and status = 'INVOICED'
GROUP BY
	professional__c,
	month;

DROP TABLE IF EXISTS bi.margin_per_cleaner;
CREATE TABLE bi.margin_per_cleaner as
SELECT
	t2.name,
	t2.rating__c as rating,
	t5.Score_cleaners,
	t2.type__c,
	t1.*,
	CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
	CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END as contract_morning_hours,
	t3.Working_Hours_Morning,
	availability_Share,
	CASE WHEN availability_share is null or availability_share = 0 THEN 0 ELSE cast(t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END) END as morning_ur,
	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END as contract_hours_afternoon,
	t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
	CASE WHEN (1-availability_Share) is null or (1-availability_Share) = 0 THEN 0 ELSE cast(t3.total_hours-t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*(1-availability_Share) END) END as afternoon_ur,
	CASE WHEN availability_Share is null THEN 0 ELSE CASE WHEN t3.Working_Hours_Morning > working_hours*availability_Share THEN working_hours*availability_Share ELSE t3.Working_Hours_Morning END END capped_hours_morning,
	CASE WHEN (1-availability_Share) = 0 THEN 0 ELSE CASE WHEN t3.total_hours-t3.working_hours_morning  > working_hours*	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END THEN working_hours*(	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END) ELSE t3.total_hours-t3.working_hours_morning END END capped_hours_afternoon
FROM
	bi.gpm_per_cleaner_v3 t1
LEFT JOIN
		Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid)
LEFT JOIN
	bi.order_distribution t3
ON
	(t1.professional__c = t3.professional__c and t1.year_month = t3.month)
LEFT JOIn
	bi.cleaner_ur_temp t4
ON
	(t1.professional__c = t4.sfid)
LEFT JOIN
	bi.KPI_Performance t5
ON
	(t1.professional__c = t5.sfid);

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3;
DROP TABLE IF EXISTS bi.order_distribution;
DROP TABLE IF EXISTS bi.cleaner_ur_temp;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
			case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM Afternoon' as kpi,
	'-' as channel,
	CASE WHEN SUM(contract_hours_afternoon) > 0 THEN SUM(CASE WHEN capped_hours_afternoon > contract_hours_afternoon THEN contract_hours_afternoon ELSE capped_hours_afternoon END)/SUM(contract_hours_afternoon) ELSE 0 END as value
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
			case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM Morning' as kpi,
	'-' as channel,
	CASE WHEN SUM(contract_morning_hours) > 0 THEN SUM(CASE WHEN capped_hours_morning > contract_morning_hours THEN contract_morning_hours ELSE capped_hours_morning END)/SUM(contract_morning_hours) ELSE 0 END as value
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%' or delivery_area like '%nl%'
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
		case
	when delivery_area = 'de-berlin' THEN 'DE-Berlin'
	when delivery_area = 'de-bonn' THEN 'DE-Bonn'
	WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
	WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
	WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
	WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
	WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
	WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	SUM(capped_work_hours)/SUM(working_hours) as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'DE-Total' as City,
	'Utilization GPM' as kpi,
	'-' as channel,
	SUM(capped_work_hours)/SUM(working_hours) as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%'
GROUP BY
	Month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'DE' as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	SUM(capped_work_hours)/SUM(working_hours) as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%'
GROUP BY
	Month,
	year;


INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'CH' as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'de%'
GROUP BY
	Month,
	year;
	

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM Effectivedate::date) as month,
	EXTRACT(YEAR FROM Effectivedate::date) as year,
	min(Effectivedate::date),
		case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.orders
WHERE
	LEFT(polygon,2) = 'ch' 
GROUP BY
	Month,
	year,
	polygon;
	
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'NL' as citya,
	'Utilization GPM' as kpi,
	'-' as channel,
	SUM(capped_work_hours)/SUM(working_hours) as Utilization
FROM
	bi.margin_per_cleaner
WHERE
	delivery_area like 'nl%'
GROUP BY
	Month,
	year;



INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'AT-Vienna'
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year,
	delivery_area;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'AT'
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'CH'
	'Utilization GPM' as kpi,
	'-' as channel,
	1 as Utilization
FROM
	bi.margin_per_cleaner
GROUP BY
	Month,
	year;

-- GROUP KPI's


	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	CAST('GROUP' as text) as locale,
	'PPH' as kpi,
	CAST('-' as text) as breakdown,
	CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
		SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/	(SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
	bi.orders t2
WHERE
	test__c = '0'
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP' as locale,
	CAST('M1 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '1'
GROUP BY
	month,
	year;
	
	
INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP' as locale,
	CAST('M3 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '3'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi		
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP' as locale,
	CAST('M6 RR' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_overall 
WhERE
	returning_month = '6'
GROUP BY
	month,
	year;
	

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP',
	'M1 RR Recurrent' as kpi,
	'-' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '1'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP',
	'M3 RR Recurrent' as kpi,
	'-' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM mindate) as month,
	EXTRACT(YEAR FROM Mindate) as year,
	min(mindate),
	'GROUP',
	'M6 RR Recurrent' as kpi,
	'All' as channel,
	SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
	bi.customer_cohorts_recurrent
WhERE
	returning_month = '3'
GROUP BY
	month,
	year;

INSERT INTO bi.topline_kpi	
SELECT
	EXTRACT(Month FROM Order_Creation__C::Date) as Month,
	EXTRACT(Year FROM Order_Creation__C::Date) as Year,
	MIN(Order_Creation__C::Date) as Mindate,
	'GROUP' as value,
	CAST('Booked GMV' as text) as kpi,
	CAST('-' as text) as breakdown,
	SUM(GMV_eur) as value
FROM
	bi.orders
WHERE
	status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP' as text,
	'Invoiced Revenue' as kpi,
	CAST('-' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
	Month,
	Year;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP' as text,
	'Invoiced Revenue' as kpi,
	CAST('B2C' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
	and (effectivedate::date < current_date)
	and order_type = '1'
GROUP BY
	Month,
	Year;
	
INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP' as text,
	'Invoiced Revenue' as kpi,
	CAST('B2B' as text) as breakdown,
		SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and (status not like '%CANCELLED%' and status not like '%ERROR%')
	and (effectivedate::date < current_date)
	and order_type = '2'
GROUP BY
	Month,
	Year;

INSERT INTO bi.topline_kpi
SELECT
	EXTRACT(MONTH FROM effectivedate::date) as Month,
	EXTRACT(YEAR FROM effectivedate::date) as Year,
	min(effectivedate::date) as date,
	'GROUP',
	'Invoiced Revenue' as kpi,
	CAST('One-Off' as text) as breakdown,
	SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
				WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
				WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
				WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
				WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END
				
				)
FROM
	bi.orders
WHERE
	test__c = '0'
	and status in ('INVOICED','NOSHOW CUSTOMER')
	and (recurrency__c is null or recurrency__c = '0')
GROUP BY
	Month,
	Year;

-- GPM, UR% and GP per Week


DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.sfid as professional__c,
	delivery_areas__c as delivery_areas,
	hr_contract_start__c::date as contract_start,
	hr_contract_end__c::date as contract_end,
	hr_contract_weekly_hours_min__c as weekly_hours
FROM
	Salesforce.Account t1
WHERE
	t1.type__c like '%cleaning-b2c%'
	and  t1.test__c = '0'
GROUP BY
	t1.sfid,
	delivery_areas__c,
	hr_contract_start__c,
	hr_contract_end__c,
	hr_contract_weekly_hours_min__c;


DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222' and effectivedate::date < current_Date-2) THEN GMV__c*1.19 ELSE 0 END) as GMV,
	SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN type = 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END) as Hours

FROM
	Salesforce.Order t1
GROUP BY
	t1.professional__c,
	date;


DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	date,
	gmv,
	hours
FROM
	bi.cleaner_Stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;


DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and date < current_date::date
	and status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	EXTRACT(WEEK FROM Date) as week,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
FROM
	bi.holidays_cleaner_3
GROUP BY
	EXTRACT(WEEK FROM Date),
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
		GREATEST(concat(to_char(mindate,'YYYY-MM'),'-01')::date,contract_start) as calc_start,
		case
		when to_char(mindate,'YYYY-MM') = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(to_char(mindate,'YYYY-MM'),'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
		when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN 	cast(LEAST(now(), contract_end, (date_trunc('MONTH', mindate::date) + INTERVAL '1 MONTH - 1 day')::date) as date) - GREATEST(concat(to_char(mindate,'YYYY-MM'),'-02')::date,contract_start) 
		else 	cast(LEAST(now(), contract_end, (date_trunc('MONTH', mindate::date) + INTERVAL '1 MONTH - 1 day')::date) as date) - GREATEST(concat(to_char(mindate,'YYYY-MM'),'-01')::date,contract_start) + 1
		END AS days_worked
from (
SELECT	
   EXTRACT(WEEK FROM Date) as Year_Week,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	7 as days_of_month,
	contract_start,
	contract_end,
	SUM(hours) as worked_hours,
	SUM(GMV) as GMV,
	MAX(Weekly_hours) as weekly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Week,
	professional__c,
	contract_start,
	contract_end,
	delivery_area	) xx;


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and EXTRACT(WEEK FROM t1.mindate) = t2.week);


DROP TABLE IF EXISTS bi.gpm_per_cleaner_per_week;
CREATE TABLE bi.gpm_per_cleaner_per_week as 
select
    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as holidays_hours,
	(weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
	SUM(CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > weekly_hours THEN (((weekly_hours/5)*(sickness+holiday))+worked_hours)*12.25 ELSE weekly_hours*12.25 END) as salary_payed,
	SUM(GMV) as GMV,
	SUM(GMV/1.19) as Revenue,
	contract_end-mindate as diff_week_contractend
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    holidays_hours,
	 diff_week_contractend;
  

  
INSERT INTO bi.gpm_per_cleaner_per_week 
select
    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as holidays_hours,
	(weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
	 CASE WHEN ((weekly_hours/5)*(sickness))+worked_hours > weekly_hours THEN 	(((weekly_hours/5)*(sickness+holiday))+worked_hours)*14.73 ELSE weekly_hours*14.73 END as salary_payed,
	SUM(GMV) as GMV,
	SUM(GMV/1.06) as Revenue,
	contract_end-mindate as diff_week_contractend
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'nl'
    and days_worked > 0
GROUP BY
    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    holidays_hours,
	 diff_week_contractend; 
  
    
DROP TABLE IF EXISTS bi.gpm_weekly_cleaner;
CREATE TABLE bi.gpm_weekly_cleaner as 
SELECT
	year_week,
	mindate,
	professional__c,
	name,
	delivery_area,
	contract_start,
	contract_end,
	weekly_hours,
	holidays_hours,
	total_hours,
	SUM(worked_hours) as worked_hours,
	CASE WHEN SUM(worked_hours) > 0 THEN SUM(gmv)/SUM(worked_hours) ELSE 0 END as pph,
	revenue,
	salary_payed,
	hr_contract_weekly_hours_max__c as max_weekly_hours,
	revenue-salary_payed as gp,
	CASE WHEN revenue > 0 THEN (revenue-salary_payed)/revenue ELSE 0 END  as gpm
FROM
	bi.gpm_per_cleaner_per_week t1
LEFT JOIN
	salesforce.account t2
ON
	(t1.professional__c = t2.sfid)
WHERE
	diff_week_contractend > 6
GROUP BY
	year_week,
	mindate,
	contract_start,
	total_hours,
	contract_end,
	holidays_hours,
	professional__c,
	name,
	weekly_hours,
	hr_contract_weekly_hours_max__c,
	delivery_area,
	salary_payed,
	revenue;

DROP TABLE IF EXISTS bi.left_date;
CREATE TABLE bi.left_date as 	
SELECT
	professional_json->>'Id' as sfid,
	min(created_at::date) as date
FROM
	events.sodium
WHERE
	event_name in ('Account Event:LEFT','Account Event:TERMINATED','Account Event:SUSPENDED')
GROUP BY
	sfid;


DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate_v2
WHERE
	date > '2016-01-01'
	and status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;




INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
	COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		   case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type = 'cleaning-b2c'
			and status = 'INVOICED'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		   citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	   citya,
	   kpi,
	   breakdown

	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
     'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	SUM(HOURS)/COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		   case
	when polygon = 'at-vienna' THEN 'AT-Vienna'
	WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
	WHEN polygon = 'ch-bern' THEN 'CH-Bern'
	WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
	WHEN polygon = 'ch-basel' THEN 'CH-Basel'
	WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
	when polygon = 'ch-geneve' then 'CH-Geneva'
	when polygon = 'de-berlin' THEN 'DE-Berlin'
	when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
	when polygon = 'de-bonn' THEN 'DE-Bonn'
	WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
	WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
	WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
	WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
	WHEN polygon = 'de-essen' THEN 'DE-Essen'
	WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
	WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
	WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
	WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
	WHEN polygon = 'de-munich' THEN 'DE-Munich'
	WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
	WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
	WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
	WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
	ELSE 'Other'
	END as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type = 'cleaning-b2c'
			and status = 'INVOICED'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		   citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	   citya,
	   kpi,
	   breakdown

	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	(Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
	
	FROM

		(SELECT
		 'DE' as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type in ('cleaning-b2c','cleaning-b2b')
			and status = 'INVOICED'
			and o.type__c like 'cleaning-b2c%'
			and left(a.locale__c,2) = 'de'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
		 citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
	COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		'DE' citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type in ('cleaning-b2c','cleaning-b2b')
			and status = 'INVOICED'
			and (o.type__c like '%cleaning-b2c%' or o.type__c like '%cleaning-b2b%')
		    and LEFT(a.locale__c,2) in ('de','nl')
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	  citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
	COUNT(DISTINCT(CSFID)) as value
	
	FROM

		(SELECT
		'CH' citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and status = 'INVOICED'
			and o.type__c != 'cleaning-b2c%'
		    and left(a.locale__c,2) = 'ch'
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
	  citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;





INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	(Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
	
	FROM

		(SELECT
		 'NL' as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and a.type in ('cleaning-b2c','cleaning-b2b')
			and status = 'INVOICED'
			and o.type__c like 'cleaning-b2c%'
			and left(a.locale__c,2) = 'nl'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
		 citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi

SELECT
	EXTRACT(month from Cleaningdate)::int as month,
	EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
	(Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
	
	FROM

		(SELECT
		 'CH' as citya,
		   o.sfid as CSFID,
		   o.hr_contract_start__c::date as StartDate,
		   o.hr_contract_end__c::date as EndDate,
		   a.effectivedate as CleaningDate,
		   a.status as Status,
		   sum(a.order_duration__c) as Hours
		 
		FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

		WHERE
		   o.test__c = '0'
		   and a.test__c = '0'
		   and effectivedate >= '2016-01-01'
			and status = 'INVOICED'
			and o.type__c != 'cleaning-b2c%'
		    and left(a.locale__c,2) = 'ch'
		    
		GROUP BY
		   CSFID,
		   StartDate,
		  	citya,
		   Cleaningdate,
		   Status,
		   EndDate) as t2

	GROUP BY
	   month,
		 year,
		 citya
	ORDER BY 
		year desc, month desc, mindate desc, citya asc

;




end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


 END;

$BODY$ LANGUAGE 'plpgsql'

	