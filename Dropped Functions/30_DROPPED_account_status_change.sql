DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$account_status_changes(crunchdate date) RETURNS void AS 

$BODY$

BEGIN

DROP TABLE IF EXISTS main.account_status_change;
CREATE TABLE main.account_status_change as

-- reactivation
select cast("CreatedDate" as date) as created_at, "AccountId" as cleaner_id, 'Reactivation' as Change
from bi."AccountHistory"
where "OldValue" in ('LEFT','TERMINATED')
and "NewValue" not in  ('LIMBO','LEFT','TERMINATED','SUSPENDED')

union

-- churn
select cast("CreatedDate" as date) as created_at, "AccountId" as cleaner_id, 'Churn' as Change
from bi."AccountHistory"
where "NewValue" in ('LEFT','TERMINATED');

end;
 $$