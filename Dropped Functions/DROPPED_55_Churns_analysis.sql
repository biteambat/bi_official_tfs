DELIMITER // 

CREATE OR REPLACE FUNCTION bi.daily$churns_analysis(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$churns_analysis';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


	DROP TABLE IF EXISTS bi.temp_orders_terminated;
	CREATE TABLE bi.temp_orders_terminated AS

		SELECT
			created_at::date as date,
			event_name,
			(order_Json->>'Order_Start__c') as orderdate,
			order_Json->>'Locale__c' as Locale__c,
			order_Json->>'Order_Id__c' as Order_id,
			order_Json->>'Contact__c' as customer_id,
			order_Json->>'Recurrency__c' as recurrency

		FROM
			events.sodium


		WHERE
			(event_name in ('Order Event:CANCELLED TERMINATED'))
			and created_at >= '2016-05-01'
			and order_Json->>'Recurrency__c' > '0'
	;

	DROP TABLE IF EXISTS bi.temp_orders_terminated_2;
	CREATE TABLE bi.temp_orders_terminated_2 AS

		SELECT

			t1.date,
			t1.event_name,
			t1.orderdate,
			t1.locale__c,
			t1.order_id,
			t1.customer_id,
			t1.recurrency,
			t2.polygon,
			t2.error_note__c

		FROM bi.temp_orders_terminated t1

			LEFT JOIN bi.orders t2 
				ON t1.order_id = t2.order_id__c

	;

	DROP TABLE IF EXISTS bi.temp_orders_NMP;
	CREATE TABLE bi.temp_orders_NMP AS

		SELECT
			created_at::date as date,
			event_name,
			(order_Json->>'Order_Start__c') as orderdate,
			order_Json->>'Locale__c' as Locale__c,
			order_Json->>'Order_Id__c' as Order_id,
			order_Json->>'Contact__c' as customer_id,
			order_Json->>'Recurrency__c' as recurrency

		FROM
			events.sodium

		WHERE
			(event_name in ('Order Event:CANCELLED-NO-MANPOWER'))
			and created_at >= '2016-04-01'
			and order_Json->>'Recurrency__c' > '0'

	;

	DROP TABLE IF EXISTS bi.temp_badrate_orders;
	CREATE TABLE bi.temp_badrate_orders AS

		SELECT
			order_id__c as order_id,
			effectivedate::date as orderdate,
			order_start__c::date as date,
			customer_id__c as customer_id

		FROM
			bi.orders

		WHERE
			rating_service__c in ('1', '2', '3')
			and effectivedate::date >= '2016-04-01'
			and recurrency__c > '0'
			and test__c = '0'
			and status in ('INVOICED')

	;


	DROP TABLE IF EXISTS bi.churns_motivations;
	CREATE TABLE bi.churns_motivations AS

		SELECT
			t1.date,
			t1.event_name,
			t1.orderdate,
			LEFT(t1.locale__c,2) as locale,
			t1.polygon,
			t1.order_id,
			t1.error_note__c,
			COUNT(DISTINCT(t2.order_id)) as Count_NMP_L28D,
			COUNT(DISTINCT(t3.order_id)) as Count_Badrate_L28D

		FROM bi.temp_orders_terminated_2 t1
			LEFT JOIN bi.temp_orders_NMP t2
				ON t1.customer_id = t2.customer_id 
				AND (t2.date) >= (t1.date - interval '28 days')
			LEFT JOIN bi.temp_badrate_orders t3
				ON t1.customer_id = t3.customer_id
				AND (t3.orderdate) >= (t1.date - interval '28 days')

		GROUP BY t1.date, t1.event_name, t1.orderdate, LEFT(t1.locale__c,2), t1.polygon, t1.order_id, t1.error_note__c

		ORDER BY t1.date desc

	;

	DROP TABLE IF EXISTS bi.temp_orders_terminated;
	DROP TABLE IF EXISTS bi.temp_orders_NMP;
	DROP TABLE IF EXISTS bi.temp_badrate_orders;

-----------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$
LANGUAGE plpgsql VOLATILE