CREATE OR REPLACE FUNCTION bi.daily$b2b_likelies(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := ' bi.daily$b2b_likelies';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
-------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.b2b_likelies;
CREATE TABLE bi.b2b_likelies AS

	SELECT
		t.*,

		CASE 	
				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	or  (t.acquisition_channel_params__c IN ('{"src":"step1"}'))
				THEN 'B2C'

				WHEN ((lower(t.acquisition_channel__c) = 'inbound') AND lower(t.acquisition_tracking_id__c) = 'classifieds')					
				THEN 'Classifieds'
							
							WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		        THEN 'SEM Brand'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		        THEN 'SEM'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'SEO'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		            THEN 'SEO Brand'::text
		            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
		                OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text 
		        THEN 'Newsletter'::text
		        

		        WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text)
		        THEN 'DTI'::text

		        /*WHEN t.acquisition_channel_params__c::text ~~ '%coop%'
		            OR t.acquisition_channel_params__c::text ~~ '%afnt%'
		            OR t.acquisition_channel_params__c::text ~~ '%putzchecker%'
		        THEN 'Affiliate/Coops'*/
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		CASE WHEN t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c NOT IN ('{"ref":"b2c"}') THEN 'B2B Homepage' -- THE FIRST PAGE THE LEAD ACTUALLY VISITED ON THE WEBSITE
				WHEN (t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c IN ('{"ref":"b2c"}')) OR t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Homepage'
				ELSE 'Unknown' END
		as landing_page,

		CASE WHEN t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Funnel' -- WHERE THE LEAD TYPED-IN ITS INFORMATON
				WHEN t.acquisition_tracking_id__c IS NULL THEN 'B2B Funnel'
				ELSE 'Unknown' END
		as conversion_page

	FROM
	Salesforce.likeli__c t
	WHERE
	createddate::date > '2016-07-01'
	and type__c = 'B2B'
;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'