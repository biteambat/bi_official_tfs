-- Function: bi."daily$03_acquisitions_kpi"(date)

-- DROP FUNCTION bi."daily$03_acquisitions_kpi"(date);
DELIMITER //
CREATE OR REPLACE FUNCTION bi."daily$03_acquisitions_kpi"(crunchdate date)
RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$03_acquisitions_kpi';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN
-- Acquisitions vs Forecast

DROP TABLE IF EXISTS bi.acquisition_kpi;
CREATE TABLE bi.acquisition_kpi as 
SELECT
	LEFT(locale__c,2)::text as locale,
	'Actuals'::text as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE a.Marketing_Channel END as Channel,
	COUNT(1) as value
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and test__c = '0'
	and order_type = '1'
	and (EXTRACT(MONTH FROM Order_Creation__c::date) = EXTRACT(MONTH FROM  current_date) and EXTRACT(YEAR FROM Order_Creation__c::date) = EXTRACT(YEAR FROM  current_date))
	and status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
GROUP BY
	locale,
	Channel;

INSERT INTO bi.acquisition_kpi	
SELECT
	LEFT(locale__c,2) as locale,
	'Runrate per Day' as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
	CAST(COUNT(1)/9 as decimal) as Acquisitions
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and test__c = '0'
	and order_type = '1'
	and Order_Creation__c between (cast(current_date as date)- interval '9 days') and (cast(current_date as date)- interval '1 days')
	and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
	locale,
	Channel;

INSERT INTO bi.acquisition_kpi
SELECT
	a.locale,
	'Forecast' as kpis,
	CASE WHEN a.channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE a.channel END as Channel,
	acquisitions+(b.runrate*(month_days-days)) as value
FROM	
(SELECT
	LEFT(locale__c,2) as locale,
	'Actuals' as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
						  DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
  	Extract(day from current_date-1) as days,
	COUNT(1) as Acquisitions
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and order_type = '1'
	and test__c = '0'
	and (EXTRACT(MONTH FROM Order_Creation__c::date) = EXTRACT(MONTH FROM  current_date) and EXTRACT(YEAR FROM Order_Creation__c::date) = EXTRACT(YEAR FROM  current_date))
	and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
	locale,
	kpi,
	channel,
	days,
	month_days) as a
LEFT JOIN
	(SELECT
	LEFT(locale__c,2) as locale,
	'Runrate per Day' as kpi,
	CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
	round(CAST(CAST(COUNT(1) as decimal)/15 as decimal),2) as runrate
FROM
	bi.orders a
WHERE
	Acquisition_New_Customer__c = '1'
	and order_type = '1'
	and test__c = '0'
	and Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days')
	and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
	locale,
	channel) as b
ON
	a.locale = b.locale and a.channel = b.channel;
	
INSERT INTO bi.acquisition_kpi Values('at','Target','Facebook','35');
INSERT INTO bi.acquisition_kpi Values('at','Target','SEO','20');
INSERT INTO bi.acquisition_kpi Values('at','Target','Brand Marketing','62');
INSERT INTO bi.acquisition_kpi Values('at','Target','SEM','25');
INSERT INTO bi.acquisition_kpi Values('at','Target','Voucher Campaigns','15');
INSERT INTO bi.acquisition_kpi Values('at','Target','Display','8');
INSERT INTO bi.acquisition_kpi Values('at','Target','Unattributed','1');

INSERT INTO bi.acquisition_kpi Values('ch','Target','Unattributed','5');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Voucher Campaigns','7');
INSERT INTO bi.acquisition_kpi Values('ch','Target','SEM','55');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Brand Marketing','190');
INSERT INTO bi.acquisition_kpi Values('ch','Target','SEO','50');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Facebook','75');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Newsletter','15');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Display','24');

INSERT INTO bi.acquisition_kpi Values('de','Target','Brand Marketing','218');
INSERT INTO bi.acquisition_kpi Values('de','Target','Display','17');
INSERT INTO bi.acquisition_kpi Values('de','Target','SEM','42');
INSERT INTO bi.acquisition_kpi Values('de','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('de','Target','Voucher Campaigns','24');
INSERT INTO bi.acquisition_kpi Values('de','Target','SEO','98');
INSERT INTO bi.acquisition_kpi Values('de','Target','Newsletter','39');
INSERT INTO bi.acquisition_kpi Values('de','Target','Facebook','26');

INSERT INTO bi.acquisition_kpi Values('nl','Target','SEM','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Brand Marketing','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Display','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Voucher Campaigns','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','SEO','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Facebook','0');

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
  
 END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bi."daily$03_acquisitions_kpi"(date)
  OWNER TO u8em74mhj6524t;
