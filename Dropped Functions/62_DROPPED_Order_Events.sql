CREATE OR REPLACE FUNCTION bi.daily$order_events (crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$order_events';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

DROP TABLE IF EXISTS main.order_events;
CREATE TABLE main.order_events as

select
		  created_at
		, replace(event_name, 'Order Event:','') as order_event
		, order_json::json->>'Id' as Order_ID
		, order_json->>'EffectiveDate' as Order_Start_Date
from events.sodium
where substring(event_name,1,12) = 'Order Event:';

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ 
LANGUAGE 'plpgsql'