﻿CREATE OR REPLACE FUNCTION bi.daily$truncate_lead_history(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$truncate_lead_history';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN
truncate table bi."LeadHistory";
end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$ LANGUAGE 'plpgsql'