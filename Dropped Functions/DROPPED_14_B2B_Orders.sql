CREATE OR REPLACE FUNCTION bi.daily$b2b_orders_table(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$b2b_orders_table';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


	DROP TABLE IF EXISTS bi.b2borders;
	CREATE TABLE bi.b2borders as 
	SELECT
		t1.contact__c,
		t2.name,
		t2.contact_name__c,
		min(effectivedate::Date) as Date,
		SUM(t1.GMV__c)+MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as total_amount,
		SUM(t1.GMV__c) as Cleaning_Gross_Revenue,
		MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue,
		COUNT(1) as Total_Order_Count, 
		SUM(t1.Order_Duration__C) as Total_Invoiced_Hours,
		(SUM(t1.GMV__c))/SUM(Order_Duration__c) as PPH,
		COUNT(1)
	FROM
		Salesforce.Order t1
	LEFT JOIN
		Salesforce.Opportunity t2
	ON
		(t2.sfid = t1.opportunityid)
	WHERE
		type = 'cleaning-b2b'
		and test__c = '0'
		and status IN ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL'
		and Effectivedate::date between '2016-05-01' and current_date::date
	GROUP BY
		t2.name,
		contact__c,
		contact_name__c;



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'