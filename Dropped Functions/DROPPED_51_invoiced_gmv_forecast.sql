
CREATE OR REPLACE FUNCTION bi.daily$invoiced_gmv_forecast_polygon(crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.daily$invoiced_gmv_forecast_polygon';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	DROP TABLE IF EXISTS bi.invoiced_gmv_forecast_polygon;
	CREATE TABLE bi.invoiced_gmv_forecast_polygon AS

		SELECT
			LEFT(o.locale__c,2) as locale,

			o.polygon as city_polygon,
		MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) )) as days_left_currMonth,

			SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END) as invoiced_gmv_currmonth,

			(SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND 	 (o.effectivedate::date <= current_date::date - 1) AND (o.effectivedate::date > current_date::date - 16)  THEN o.gmv_eur ELSE 0 END)/15)::numeric as rr_L15D,




			(SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END))
				+ 

			((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 16) THEN o.gmv_eur ELSE 0 END)/15)::numeric)
				*
				(MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) ))) 

			as forecast,




			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END) as invoiced_acq_currmonth,

			(SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate < current_date - 1) AND (o.effectivedate >= current_date - 16)THEN 1 ELSE 0 END)/15)::numeric as rr_L15D_acq,

			(SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END)) 
				+ ((SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate < current_date - 1) AND (o.effectivedate >= current_date - 16)THEN 1 ELSE 0 END)/15)::numeric) 
				* (MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-2) )))

			as forecast_invoiced_acquisitions,



			CASE WHEN (EXTRACT (DAY from current_date)) > '10' THEN

								((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END))
									+ 

								((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 16) THEN o.gmv_eur ELSE 0 END)/15)::numeric)
									*
									(MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) ))) )

			ELSE


								(((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 31) THEN o.gmv_eur ELSE 0 END)/30)::numeric)
									*
									(    DATE_PART('days', DATE_TRUNC('month', current_date::date) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)))

			END


			as adjusted_forecast


		FROM bi.orders o

		WHERE o.test__c = '0' and order_type = '1' and (LEFT(o.locale__c,2) = LEFT(o.polygon,2) OR o.polygon IS NULL)

		GROUP BY LEFT(o.locale__c,2), o.polygon

		ORDER BY locale asc, city_polygon asc
	;

	DELETE FROM bi.invoiced_gmv_locked_polygon WHERE forecast_date = current_date;

	INSERT INTO bi.invoiced_gmv_locked_polygon

		SELECT
			current_date::date as forecast_date,
			i.locale as locale,
			i.city_polygon as city_polygon,
			i.forecast as forecast
		FROM bi.invoiced_gmv_forecast_polygon i

	;





	DELETE FROM bi.adjusted_forecast_comparison WHERE forecast_date = current_date;

	INSERT INTO bi.adjusted_forecast_comparison

		SELECT
			current_date::date as forecast_date,
			i.locale as locale,
			i.city_polygon as city_polygon,
			i.forecast as old_forecast,
			i.adjusted_forecast as new_forecast
		FROM bi.invoiced_gmv_forecast_polygon i

	;





	DELETE FROM bi.invoiced_acq_locked_polygon WHERE forecast_date = current_date;

	INSERT INTO bi.invoiced_acq_locked_polygon

		SELECT
			current_date::date as forecast_date,
			i.locale::text as locale,
			i.city_polygon::text as city_polygon,
			i.forecast_invoiced_acquisitions::numeric as forecast
		FROM bi.invoiced_gmv_forecast_polygon i

	;

	DROP TABLE IF EXISTS bi.temp_supplies_forecast;
	CREATE TABLE bi.temp_supplies_forecast AS 

		SELECT
			t1.locale,
			t1.polygon,
			SUM(t1.supply_revenue) as supply_forecast

		FROM 
			(SELECT
			 left(t1.locale__c,2) as locale,
			 t1.polygon,
			 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
			 bi.orders t1
			LEFT JOIN
			 Salesforce.Opportunity t2
			ON
			 (t2.sfid = t1.opportunityid)
			WHERE
			 type = 'cleaning-b2b'
			 and status NOT LIKE ('%CANCELLED%')
			 and EXTRACT(MONTH from Effectivedate) = EXTRACT(MONTH from current_date)
			 and EXTRACT(YEAR from Effectivedate) = EXTRACT(YEAR from current_date)
			GROUP BY
			 t2.name,
			 contact__c,
			 contact_name__c,
			 left(t1.locale__c,2),
			 t1.polygon) t1

		GROUP BY t1.locale, t1.polygon

	;

	DROP TABLE IF EXISTS bi.temp_b2b_bookings_forecast;

	CREATE TABLE bi.temp_b2b_bookings_forecast AS

		SELECT
			left(o.locale__c,2) as locale,

			o.polygon as polygon,

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)) as days_left_currMonth,

			SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END) as actual_sales_currMonth,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
			THEN o.gmv_eur ELSE 0 END)
			/
			14) as run_rate_L14D,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
			THEN o.gmv_eur ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as bookings_forecast,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND o.acquisition_new_customer__c = '1'
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
						AND o.acquisition_new_customer__c = '1'
			THEN o.gmv_eur ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as acquisition_forecast,


			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND o.acquisition_new_customer__c = '0'
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
						AND o.acquisition_new_customer__c = '0'
			THEN o.gmv_eur ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as rebookings_forecast

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.order_type = '2'
			AND o.status not like ('%CANCELLED%')

		GROUP BY left(o.locale__c,2), o.polygon

		ORDER BY locale asc, o.polygon asc

	;

	DROP TABLE IF EXISTS bi.b2b_forecast;

	CREATE TABLE bi.b2b_forecast AS

		SELECT

			t1.locale,
			t1.polygon,
			t1.acquisition_forecast::numeric as acquisition_forecast,
			t1.rebookings_forecast::numeric as rebookings_forecast,
			t1.bookings_forecast::numeric as bookings_forecast,
			t2.supply_forecast::numeric as supply_forecast

		FROM bi.temp_b2b_bookings_forecast t1

		LEFT JOIN bi.temp_supplies_forecast t2 ON (t1.locale = t2.locale AND t1.polygon = t2.polygon)

	;

	DELETE FROM bi.locked_forecast_history_B2B WHERE date = current_date::date;

	INSERT INTO bi.locked_forecast_history_B2B

		SELECT

			current_date::date as date,
			t1.locale::text,
			t1.acquisition_forecast::numeric as acquisition_forecast,
			t1.rebookings_forecast::numeric as rebookings_forecast,
			t1.bookings_forecast::numeric as bookings_forecast,
			t2.supply_forecast::numeric as supply_forecast

		FROM bi.temp_b2b_bookings_forecast t1

		JOIN bi.temp_supplies_forecast t2 ON (t1.locale = t2.locale)

	;

	DROP TABLE IF EXISTS bi.invoiced_forecast_b2c_b2b;
	CREATE TABLE bi.invoiced_forecast_b2c_b2b AS

		SELECT
			f.locale,
			f.city_polygon,
			SUM(f.forecast) as invoiced_gmv_forecast,
			(CASE WHEN (AVG(b.bookings_forecast + b.supply_forecast)) IS NOT NULL THEN (AVG(b.bookings_forecast + b.supply_forecast))
				ELSE 0 END)
			as b2b_forecast,
			SUM(f.forecast) + (CASE WHEN (AVG(b.bookings_forecast + b.supply_forecast)) IS NOT NULL THEN (AVG(b.bookings_forecast + b.supply_forecast)) ELSE 0 END) 
			as b2c_b2b_invoiced_forecast

		FROM bi.invoiced_gmv_forecast_polygon f

			LEFT JOIN bi.b2b_forecast b ON f.locale = b.locale 
				AND f.city_polygon = b.polygon

		GROUP BY f.locale, f.city_polygon

	;



	--DROP TABLE IF EXISTS bi.temp_b2b_bookings_forecast;
	--DROP TABLE IF EXISTS bi.temp_supplies_forecast;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'