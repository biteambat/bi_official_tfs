DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$rr_newfunnel (crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.daily$rr_newfunnel';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	DROP TABLE IF EXISTS bi.NewFunnel_Running_Cohort_Analysis;
	CREATE TABLE bi.NewFunnel_Running_Cohort_Analysis AS
	SELECT
	  Customer_Id__c,
	  recurrency__c,
	  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'New funnel' ELSE 'Old funnel' END as funnel,
	  LEFT(Locale__c,2) as locale,
	  CAST(acquisition_customer_creation__c as date) as Acquisition_Date,
	  MAX(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_done_last28d,
	  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '29 days' and cast(crunchdate as date) - INTERVAL '1 days' THEN 1 ELSE 0 END) as booking_count_last28,
	  SUM(CASE WHEN cast (Order_Creation__c as date)  between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days'  THEN 1 ELSE 0 END) as booking_count_last57
	FROM
		bi.orders
	WHERE
		Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
	 	and test__c = '0'
	 	and order_type = '1'
	GROUP BY
		CAST(acquisition_customer_creation__c as date),
		Customer_Id__c,
		recurrency__c,
		locale,
		CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'New funnel' ELSE 'Old funnel' END
	ORDER BY customer_id__c desc, recurrency__c asc, locale asc
	;

	/*drop table if exists bi.NewFunnel_Running_Cohort_Analysis_email;
	create table bi.NewFunnel_Running_Cohort_Analysis_email as
		SELECT
			crunchdate::date - INTERVAL '1 days' as Date,
			14::int as recurrency,
			'funnel'::text as funnel,
	    	2.15::numeric as Cohort_Return_Rate,
	    	2.15::numeric as p0_Activitiy_Rate,
	    	2.15::numeric as p1_Activitiy_Rate,
			100::int as Acquisitions,
	    	100::numeric as Rebookings,
			'locale'::text as locale*/


	DELETE FROM bi.NewFunnel_Running_Cohort_Analysis_email WHERE Date = cast(crunchdate as date) - INTERVAL '1 days';

	INSERT INTO bi.NewFunnel_Running_Cohort_Analysis_email
	SELECT
		cast(crunchdate as date) - INTERVAL '1 days' as Date,			  
		recurrency__c as recurrency,
	  	funnel as funnel,
	    CASE WHEN 
	    	(SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) > 0 
	    	THEN 
	    		Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as float)
	    		/
	    		CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) *100,3)
	    	ELSE 0 END 
	    as Cohort_Return_Rate,

	    CASE WHEN (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_count_last57 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p0_Activitiy_Rate,
	    CASE WHEN (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) > 0 THEN Round(CAST(CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN booking_count_last28 ELSE 0 END)) as float)/CAST((SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' AND booking_done_last28d = 1 THEN 1 ELSE 0 END)) as float) as numeric) ,3) ELSE 0 END as p1_Activitiy_Rate,
	    (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN booking_done_last28d ELSE 0 END)) as Acquisitions,
	    (SUM(CASE WHEN Acquisition_Date between cast(crunchdate as date) - interval '57 days' and cast(crunchdate as date) - interval '30 days' THEN 1 ELSE 0 END)) as Rebookings,
		locale
	FROM
	   bi.NewFunnel_Running_Cohort_Analysis
	GROUP BY
		locale, recurrency__c, funnel
	ORDER BY date desc, locale asc, funnel asc, recurrency asc;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'