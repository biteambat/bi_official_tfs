CREATE OR REPLACE FUNCTION bi.daily$development_dashboard_kpis(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.daily$development_dashboard_kpis';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

--# Acquisitions ====================================================================> CHECKED 

	DROP TABLE IF EXISTS bi.city_overview_dashboard;
	CREATE TABLE bi.city_overview_dashboard AS

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate)::date as mindate,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(week from o.effectivedate)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--#  Reactivated customers

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate)::date as mindate,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# Reactivated customers'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '0'
			AND o.acquisition_channel__c = 'web'
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(week from o.effectivedate)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--# ALL acquisitions ===============================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.order_creation__c)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.order_creation__c)::int as weeknum,

			MIN(o.order_creation__c)::date as mindate,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# all acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.order_creation__c)::int, EXTRACT(week from o.order_creation__c)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur_net ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% Skipped'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--Gross Profit Margin

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--Utilization Rate

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churns;
	CREATE TABLE bi.temp_churns as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		 
		WHERE  event_name = 'Order Event:CANCELLED TERMINATED'

	;


	DROP TABLE IF EXISTS bi.temp_acquiredlist;
	CREATE TABLE bi.temp_acquiredlist AS

		SELECT DISTINCT

			customer_id__c

		FROM bi.orders 

		WHERE test__c = '0'
			AND acquisition_new_customer__c = '1'
			AND order_type = '1'
			AND status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')

	;


	DROP TABLE IF EXISTS bi.temp_churnlist;
	CREATE TABLE bi.temp_churnlist AS

		SELECT

			t2.*

		FROM bi.temp_acquiredlist t1

		JOIN bi.temp_churns t2 ON t1.customer_id__c = t2.customer_id

	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly;
	CREATE TABLE bi.temp_churncount_weekly AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(week from t1.date) as weeknum,
			MIN(t1.date) as mindate,
			LEFT(t1.locale__c,2) as locale,
			t2.polygon as city,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist t1

		JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(week from t1.date), LEFT(t1.locale__c,2) ,t2.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly;
	CREATE TABLE bi.temp_activecount_weekly AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(week from t1.effectivedate) as weeknum,
			MIN(t1.effectivedate) as mindate,
			LEFT(t1.locale__c,2) as locale,
			t1.polygon as city,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(week from t1.effectivedate), LEFT(t1.locale__c,2), t1.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc
	;

	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,
			MIN(t1.mindate) as mindate,
			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly t1

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, t1.city, kpi, t1.churned_customers

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc
	;


	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,
			MIN(t1.mindate) as mindate,			
			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly t1

		JOIN bi.temp_churncount_weekly t2 
			ON t1.weeknum = t2.weeknum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale
				AND t1.city = t2.city
				AND t1.mindate = t2.mindate

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, t1.city, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc
	;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,			

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,			

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(week from o.effectivedate)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc

	; 

--# Reactivated customers 

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,			

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,			

			'# Reactivated customers'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '0'
			AND o.acquisition_channel__c = 'web' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(week from o.effectivedate)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc

	; 


--#ALL ACQUISITIONS =====================================================================>  CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.order_creation__c)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.order_creation__c)::int as weeknum,

			MIN(o.order_creation__c) as mindate,			

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,			

			'# all acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.order_creation__c)::int, EXTRACT(week from o.order_creation__c)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc

	;
--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,

			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur_net ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,

			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% Skipped'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'
		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

	--take hours
	--take polygons except for cleaner

--Gross Profit Margin

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

--Utilization Rate

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churns;
	CREATE TABLE bi.temp_churns as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		 
		WHERE  event_name = 'Order Event:CANCELLED TERMINATED'

	;


	DROP TABLE IF EXISTS bi.temp_acquiredlist;
	CREATE TABLE bi.temp_acquiredlist AS

		SELECT DISTINCT

			customer_id__c

		FROM bi.orders 

		WHERE test__c = '0'
			AND acquisition_new_customer__c = '1'
			AND order_type = '1'
			AND status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')

	;

	DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;
	CREATE TABLE bi.temp_churnlist_countrylevel AS

		SELECT

			t2.*

		FROM bi.temp_acquiredlist t1

		JOIN bi.temp_churns t2 ON t1.customer_id__c = t2.customer_id

	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
	CREATE TABLE bi.temp_churncount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(week from t1.date) as weeknum,
			MIN(t1.date) as mindate,
			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist_countrylevel t1

		JOIN bi.orders t2 ON t1.customer_id = t2.customer_id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(week from t1.date), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;
	CREATE TABLE bi.temp_activecount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(week from t1.effectivedate) as weeknum,

			MIN(t1.effectivedate) as mindate,

			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(week from t1.effectivedate), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,

			MIN(t1.mindate) as mindate,

			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly_countrylevel t1

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, kpi, t1.churned_customers

		ORDER BY yearnum desc, weeknum desc, locale asc
	;


	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,

			MIN(t1.mindate) as mindate,

			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly_countrylevel t1

		JOIN bi.temp_churncount_weekly_countrylevel t2 
			ON t1.weeknum = t2.weeknum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale
				AND t1.mindate = t2.mindate

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, weeknum desc, locale asc
	;


	INSERT INTO bi.city_overview_dashboard

		SELECT
			 EXTRACT(year from cpa.date) as yearnum,

			 NULL::int as monthnum,

			 EXTRACT(week from cpa.date) as weeknum,

			 MIN(cpa.date) as mindate,

			 cpa.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Global CPA'::text as kpi,
			
			(SUM(cpa.sem_brand + cpa.sem_non_brand + cpa.offline_marketing + cpa.facebook + cpa.gdn + cpa.criteo + cpa.sociomantic + cpa.coops + cpa.other_voucher + cpa.tvcampaign + cpa.deindeal_voucher + cpa.seo)::numeric

				/

			SUM(cpa.all_acquisitions::numeric))

			as value

		FROM bi.cpacalclocale cpa

		GROUP BY EXTRACT(year from cpa.date), monthnum, EXTRACT(week from cpa.date), cpa.locale, city, kpi

		ORDER BY yearnum desc, weeknum desc, locale asc
	;



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(month from o.effectivedate)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--# reactivated customers 


	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# Reactivated customers'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '0'
			AND o.acquisition_channel__c = 'web' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(month from o.effectivedate)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;


--# all acquisitions =====================================================================>  CHECKED


	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.order_creation__c)::int as yearnum,

			EXTRACT(month from o.order_creation__c)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.order_creation__c) as mindate,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# all acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.order_creation__c)::int, EXTRACT(month from o.order_creation__c)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;


--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur_net ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;


--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% Skipped'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--Gross Profit Margin

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--Utilization Rate

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churns;
	CREATE TABLE bi.temp_churns as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		 
		WHERE  event_name = 'Order Event:CANCELLED TERMINATED'

	;


	DROP TABLE IF EXISTS bi.temp_acquiredlist;
	CREATE TABLE bi.temp_acquiredlist AS

		SELECT DISTINCT

			customer_id__c

		FROM bi.orders 

		WHERE test__c = '0'
			AND acquisition_new_customer__c = '1'
			AND order_type = '1'
			AND status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')

	;

	DROP TABLE IF EXISTS bi.temp_churnlist;
	CREATE TABLE bi.temp_churnlist AS

		SELECT

			t2.*

		FROM bi.temp_acquiredlist t1

		JOIN bi.temp_churns t2 ON t1.customer_id__c = t2.customer_id


	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly;
	CREATE TABLE bi.temp_churncount_weekly AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(month from t1.date) as monthnum,

			MIN(t1.date) as mindate,

			LEFT(t1.locale__c,2) as locale,
			t2.polygon as city,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist t1

		JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(month from t1.date), LEFT(t1.locale__c,2) ,t2.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly;
	CREATE TABLE bi.temp_activecount_weekly AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(month from t1.effectivedate) as monthnum,

			MIN(t1.effectivedate) as mindate,

			LEFT(t1.locale__c,2) as locale,
			t1.polygon as city,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(month from t1.effectivedate), LEFT(t1.locale__c,2), t1.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc
	;

	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,

			MIN(t1.mindate) as mindate,

			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly t1

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, t1.city, kpi, t1.churned_customers

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc
	;


	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,

			MIN(t1.mindate) as mindate,

			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly t1

		JOIN bi.temp_churncount_weekly t2 
			ON t1.monthnum = t2.monthnum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale
				AND t1.city = t2.city

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, t1.city, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc
	;

	/*DROP TABLE IF EXISTS bi.temp_activecount_weekly;
	DROP TABLE IF EXISTS bi.temp_churncount_weekly;
	DROP TABLE IF EXISTS bi.temp_churnlist;*/


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,			

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(month from o.effectivedate)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

--# Reactivated customers 


	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,			

			'# Reactivated customers'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_channel__c = 'web'
			AND o.acquisition_new_customer__c = '0' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(month from o.effectivedate)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc

	;


--# all acquisitions =====================================================================>  CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.order_creation__c)::int as yearnum,

			EXTRACT(month from o.order_creation__c)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.order_creation__c) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,			

			'# all acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.order_creation__c)::int, EXTRACT(month from o.order_creation__c)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc

	;


--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur_net ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			MIN(o.effectivedate) as mindate,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% Skipped'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'
		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

	--take hours
	--take polygons except for cleaner

--Gross Profit Margin

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

--Utilization Rate

	INSERT INTO bi.city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum

			MIN(m.mindate) as mindate,

			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churns;
	CREATE TABLE bi.temp_churns as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		 
		WHERE  event_name = 'Order Event:CANCELLED TERMINATED'

	;


	DROP TABLE IF EXISTS bi.temp_acquiredlist;
	CREATE TABLE bi.temp_acquiredlist AS

		SELECT DISTINCT

			customer_id__c

		FROM bi.orders 

		WHERE test__c = '0'
			AND acquisition_new_customer__c = '1'
			AND order_type = '1'
			AND status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')

	;

	DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;
	CREATE TABLE bi.temp_churnlist_countrylevel AS

		SELECT

			t2.*

		FROM bi.temp_acquiredlist t1

		JOIN bi.temp_churns t2 ON t1.customer_id__c = t2.customer_id


	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
	CREATE TABLE bi.temp_churncount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(month from t1.date) as monthnum,

			MIN(t1.date) as mindate,

			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist_countrylevel t1

		JOIN bi.orders t2 ON t1.customer_id = t2.customer_id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(month from t1.date), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;
	CREATE TABLE bi.temp_activecount_weekly_countrylevel AS
		SELECT
		
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(month from t1.effectivedate) as monthnum,

			MIN(t1.effectivedate) as mindate,

			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(month from t1.effectivedate), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,

			MIN(t1.mindate) as mindate,

			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly_countrylevel t1

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, kpi, t1.churned_customers

		ORDER BY yearnum desc, monthnum desc, locale asc
	;


	INSERT INTO bi.city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,

			MIN(t1.mindate) as mindate,

			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly_countrylevel t1

		JOIN bi.temp_churncount_weekly_countrylevel t2 
			ON t1.monthnum = t2.monthnum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

	/*DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;
	DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
	DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;*/


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Comparisons with weekly average per city (L12W)

	--'# Invoiced acquisitions'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'# Invoiced acquisitions - Comparison L12W' as kpi,
				CASE WHEN AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END) > 0 THEN 

						(AVG(CASE WHEN weeknum = (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END)
						/
						AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END))::numeric
						- 1
				
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Weekly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = '# Invoiced acquisitions' 
						AND t.yearnum = (EXTRACT (year from current_date)) 
						AND (t.weeknum between (EXTRACT (week from current_date) - 14) and (EXTRACT (week from current_date) - 1) ) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'# all acquisitions'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'# all acquisitions - Comparison L12W' as kpi,
				CASE WHEN AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END) > 0 THEN 

						(AVG(CASE WHEN weeknum = (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END)
						/
						AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END))::numeric
						- 1
				
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Weekly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = '# all acquisitions' 
						AND t.yearnum = (EXTRACT (year from current_date)) 
						AND (t.weeknum between (EXTRACT (week from current_date) - 14) and (EXTRACT (week from current_date) - 1) ) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'Invoiced GMV'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'Invoiced GMV - Comparison L12W' as kpi,
				CASE WHEN AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END) > 0 THEN 

						(AVG(CASE WHEN weeknum = (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END)
						/
						AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END))::numeric
						- 1
				
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Weekly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = 'Invoiced GMV'
						AND t.yearnum = (EXTRACT (year from current_date)) 
						AND (t.weeknum between (EXTRACT (week from current_date) - 14) and (EXTRACT (week from current_date) - 1) ) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'Invoiced Hours'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'Invoiced hours - Comparison L12W' as kpi,
				CASE WHEN AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END) > 0 THEN 

						(AVG(CASE WHEN weeknum = (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END)
						/
						AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END))::numeric
						- 1
				
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Weekly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = 'Invoiced hours' 
						AND t.yearnum = (EXTRACT (year from current_date)) 
						AND (t.weeknum between (EXTRACT (week from current_date) - 14) and (EXTRACT (week from current_date) - 1) ) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'# churned customers'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'# churned customers - Comparison L12W' as kpi,
				CASE WHEN AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END) > 0 THEN 

						(AVG(CASE WHEN weeknum = (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END)
						/
						AVG(CASE WHEN weeknum < (EXTRACT (week from current_date) - 1) THEN value ELSE NULL END))::numeric
						- 1
				
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Weekly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = '# churned customers' 
						AND t.yearnum = (EXTRACT (year from current_date)) 
						AND (t.weeknum between (EXTRACT (week from current_date) - 14) and (EXTRACT (week from current_date) - 1) ) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'% NMP'

	INSERT INTO bi.city_overview_dashboard

		SELECT

			NULL::int as yearnum,

			NULL::int as monthnum,

			NULL::int as weeknum,

			NULL::date as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NMP - Comparison L12W'::text as kpi,

			CASE WHEN (			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED NO MANPOWER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END) > 0 THEN

				((			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int = (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED NO MANPOWER') 
							AND EXTRACT(week from o.effectivedate)::int = (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int = (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END)
				/
				(			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED NO MANPOWER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END))
				-
				1
				
			ELSE NULL END

			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND order_type = '1'
			AND o.effectivedate < current_date
			AND EXTRACT(week from o.effectivedate)::int <= (EXTRACT(week from current_date)::int - 1)
			AND EXTRACT(week from o.effectivedate)::int > (EXTRACT(week from current_date)::int - 14)

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	--'% Skipped'

	INSERT INTO bi.city_overview_dashboard

		SELECT

			NULL::int as yearnum,

			NULL::int as monthnum,

			NULL::int as weeknum,

			NULL::date as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NTY - Comparison L12W'::text as kpi,

			CASE WHEN (			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED CUSTOMER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END) > 0 THEN

				((			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int = (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED CUSTOMER') 
							AND EXTRACT(week from o.effectivedate)::int = (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int = (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END)
				/
				(			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED CUSTOMER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END))
				-
				1
				
			ELSE NULL END

			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND order_type = '1'
			AND o.effectivedate < current_date
			AND EXTRACT(week from o.effectivedate)::int <= (EXTRACT(week from current_date)::int - 1)
			AND EXTRACT(week from o.effectivedate)::int > (EXTRACT(week from current_date)::int - 14)

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	--'% GPM'

	INSERT INTO bi.city_overview_dashboard

	SELECT

		NULL::int as yearnum,

		NULL::int as monthnum,

		NULL::int as weeknum,

		NULL::date as mindate,

		LEFT(city,2) as locale,
		city as city,

		'City level'::text as geographic_level,

		'Weekly level'::text as time_frame,

		'% GPM - Comparison L12W'::text as kpi,


		CASE WHEN ((CASE WHEN SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END) > 0 THEN(SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END)-SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN salary_payed ELSE NULL END))/SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END)ELSE NULL END)) != 0 THEN 

			(CASE WHEN SUM(CASE WHEN weeknum = EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END) > 0 THEN (SUM(CASE WHEN weeknum = EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END)-SUM(CASE WHEN weeknum = EXTRACT(week from current_date) - 1 THEN salary_payed ELSE NULL END))/SUM(CASE WHEN weeknum = EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END)ELSE NULL END)
			/
			(CASE WHEN SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END) > 0 THEN(SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END)-SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN salary_payed ELSE NULL END))/SUM(CASE WHEN weeknum < EXTRACT(week from current_date) - 1 THEN revenue ELSE NULL END)ELSE NULL END)
			-
			1


		ELSE NULL END as value

		FROM

		(	SELECT
				EXTRACT(year from mindate) as yearnum,
				EXTRACT(week from mindate) as weeknum,
				delivery_area as city,
				SUM(revenue) as revenue,
				SUM(salary_payed) as salary_payed

			FROM bi.gpm_weekly_cleaner

			WHERE EXTRACT(year from mindate) = EXTRACT(year from current_date)
				AND EXTRACT(week from mindate) >= (EXTRACT(week from current_date)-14)
				AND EXTRACT(week from mindate) < (EXTRACT(week from current_date))

			GROUP BY EXTRACT(year from mindate), EXTRACT(week from mindate), delivery_area

			ORDER BY yearnum desc, weeknum desc, delivery_area asc

		) as t1

		GROUP BY city

		ORDER BY city asc
	;
	--'UR %'

	INSERT INTO bi.city_overview_dashboard

		SELECT
			
			NULL::int as yearnum,
			NULL::int as monthnum,
			NULL::int as weeknum,
			NULL::date as mindate,
			left(city,2) as locale,
			city as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% UR - Comparison L12W'::text as kpi,


			(SUM(CASE WHEN weeknum = (EXTRACT(week from current_date)-1) THEN (CASE WHEN worked_hours > weekly_hours THEN weekly_hours ELSE worked_hours END)
				ELSE NULL END
			)
			/
			SUM(CASE WHEN weeknum = (EXTRACT(week from current_date)-1) THEN (weekly_hours) ELSE NULL END) - SUM(CASE WHEN weeknum < (EXTRACT(week from current_date)-1) THEN (CASE WHEN worked_hours > weekly_hours THEN weekly_hours ELSE worked_hours END)
				ELSE NULL END
			)
			/
			SUM(CASE WHEN weeknum < (EXTRACT(week from current_date)-1) THEN (weekly_hours) ELSE NULL END))
			/
			(	SUM(CASE WHEN weeknum < (EXTRACT(week from current_date)-1) THEN (CASE WHEN worked_hours > weekly_hours THEN weekly_hours ELSE worked_hours END)
				ELSE NULL END
			)
			/
			SUM(CASE WHEN weeknum < (EXTRACT(week from current_date)-1) THEN (weekly_hours) ELSE NULL END)) as value

		FROM

			(SELECT
				EXTRACT(year from mindate) as yearnum,
				EXTRACT(week from mindate) as weeknum,
				delivery_area as city,
				SUM(worked_hours) as worked_hours,
				SUM(weekly_hours) as weekly_hours
			
			FROM bi.gpm_weekly_cleaner

			WHERE EXTRACT(year from mindate) = EXTRACT(year from current_date)
				AND EXTRACT(week from mindate) >= (EXTRACT(week from current_date)-14)
				AND EXTRACT(week from mindate) < (EXTRACT(week from current_date))

			GROUP BY EXTRACT(year from mindate), EXTRACT(week from mindate), delivery_area

			ORDER BY yearnum desc, weeknum desc, delivery_area asc) as t1

		GROUP BY city, locale

		ORDER BY city desc

	;

	--'% churned customers LW VS L12W'
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- Comparisons with monthly average per city (L3D)

	--'# Invoiced acquisitions'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'# Invoiced acquisitions - Comparison L3M' as kpi,
				CASE WHEN AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric > 0 THEN
						
						AVG(CASE WHEN monthnum = (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						/
						AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						-
						1
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Monthly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = '# Invoiced acquisitions' 
						AND t.yearnum = (EXTRACT (year from current_date))
						AND (t.monthnum > (EXTRACT (month from current_date) - 5) 
						AND (t.monthnum <= EXTRACT (month from current_date) - 1)) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;


--'# all acquisitions'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'# all acquisitions - Comparison L3M' as kpi,
				CASE WHEN AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric > 0 THEN
						
						AVG(CASE WHEN monthnum = (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						/
						AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						-
						1
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Monthly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = '# all acquisitions' 
						AND t.yearnum = (EXTRACT (year from current_date))
						AND (t.monthnum > (EXTRACT (month from current_date) - 5) 
						AND (t.monthnum <= EXTRACT (month from current_date) - 1)) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'Invoiced GMV'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'Invoiced GMV - Comparison L3M' as kpi,
				CASE WHEN AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric > 0 THEN
						
						AVG(CASE WHEN monthnum = (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						/
						AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						-
						1
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Monthly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = 'Invoiced GMV' 
						AND t.yearnum = (EXTRACT (year from current_date))
						AND (t.monthnum > (EXTRACT (month from current_date) - 5) 
						AND (t.monthnum <= EXTRACT (month from current_date) - 1)) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;


	--'Invoiced Hours'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'Invoiced Hours - Comparison L3M' as kpi,
				CASE WHEN AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric > 0 THEN
						
						AVG(CASE WHEN monthnum = (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						/
						AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						-
						1
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Monthly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = 'Invoiced Hours' 
						AND t.yearnum = (EXTRACT (year from current_date))
						AND (t.monthnum > (EXTRACT (month from current_date) - 5) 
						AND (t.monthnum <= EXTRACT (month from current_date) - 1)) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;

	--'# churned customers'

	INSERT INTO bi.city_overview_dashboard

		SELECT
				NULL::int as yearnum,
				NULL::int as monthnum,
				NULL::int as weeknum,
				NULL::date as mindate,
				locale as locale,
				city as city,
				geographic_level as geographic_level,
				time_frame as time_frame,
				'# churned customers - Comparison L3M' as kpi,
				CASE WHEN AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric > 0 THEN
						
						AVG(CASE WHEN monthnum = (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						/
						AVG(CASE WHEN monthnum < (EXTRACT (month from current_date) - 1) THEN value ELSE NULL END)::numeric
						-
						1
				ELSE NULL END
				as value
				
		FROM(
					SELECT
						t.yearnum as yearnum,

						t.monthnum as monthnum,

						t.weeknum as weeknum,

						t.mindate as mindate,

						t.locale as locale,

						t.city as city,

						t.geographic_level as geographic_level,

						t.time_frame as time_frame,

						t.kpi as kpi,

						AVG(t.value) as value

					FROM bi.city_overview_dashboard t

					WHERE t.time_frame = 'Monthly level'
						AND t.geographic_level = 'City level' 
						AND t.kpi = '# churned customers' 
						AND t.yearnum = (EXTRACT (year from current_date))
						AND (t.monthnum > (EXTRACT (month from current_date) - 5) 
						AND (t.monthnum <= EXTRACT (month from current_date) - 1)) 

					GROUP BY yearnum, monthnum, weeknum, mindate, locale, city, geographic_level, time_frame, kpi

					ORDER BY yearnum desc, weeknum desc, locale asc, city asc
		) as t1

		GROUP BY locale, city, geographic_level, time_frame, kpi

		ORDER BY city asc

	;


	--'% NMP'

	INSERT INTO bi.city_overview_dashboard

		SELECT

			NULL::int as yearnum,

			NULL::int as monthnum,

			NULL::int as weeknum,

			NULL::date as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NMP - Comparison L3M'::text as kpi,

			CASE WHEN (			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED NO MANPOWER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END) > 0 THEN

				((			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(MONTH from o.effectivedate)::int = (EXTRACT(MONTH from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED NO MANPOWER') 
							AND EXTRACT(MONTH from o.effectivedate)::int = (EXTRACT(MONTH from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(MONTH from o.effectivedate)::int = (EXTRACT(MONTH from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

				ELSE NULL END)
				/
				(			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED NO MANPOWER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END))
				-
				1
				
			ELSE NULL END

			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND order_type = '1'
			AND o.effectivedate < current_date
			AND EXTRACT(week from o.effectivedate)::int <= (EXTRACT(week from current_date)::int - 1)
			AND EXTRACT(week from o.effectivedate)::int > (EXTRACT(week from current_date)::int - 14)

		GROUP BY yearnum, monthnum, weeknum, LEFT(o.locale__c,2), o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	--'% Skipped'

	INSERT INTO bi.city_overview_dashboard

		SELECT

			NULL::int as yearnum,

			NULL::int as monthnum,

			NULL::int as weeknum,

			NULL::date as mindate,

			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NTY - Comparison L3M'::text as kpi,

			CASE WHEN (			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED CUSTOMER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END) > 0 THEN

				((			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(MONTH from o.effectivedate)::int = (EXTRACT(MONTH from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED CUSTOMER') 
							AND EXTRACT(MONTH from o.effectivedate)::int = (EXTRACT(MONTH from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(MONTH from o.effectivedate)::int = (EXTRACT(MONTH from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

				ELSE NULL END)
				/
				(			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1) THEN o.order_duration__c ELSE 0 END)) > 0 THEN

					(			SUM(
						CASE WHEN 
							o.status in ('CANCELLED CUSTOMER') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))
					/
					(SUM(
						CASE WHEN 
							o.status in ('INVOICED','CANCELLED CUSTOMER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') 
							AND EXTRACT(week from o.effectivedate)::int < (EXTRACT(week from current_date)::int - 1)
						THEN o.order_duration__c 
						ELSE 0 END))

			ELSE NULL END))
				-
				1
				
			ELSE NULL END

			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND order_type = '1'
			AND o.effectivedate < current_date
			AND EXTRACT(week from o.effectivedate)::int <= (EXTRACT(week from current_date)::int - 1)
			AND EXTRACT(week from o.effectivedate)::int > (EXTRACT(week from current_date)::int - 14)

		GROUP BY yearnum, monthnum, weeknum, LEFT(o.locale__c,2), o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	--'% GPM'

	INSERT INTO bi.city_overview_dashboard

		SELECT

			NULL::int as yearnum,

			NULL::int as monthnum,

			NULL::int as weeknum,

			NULL::date as mindate,

			LEFT(city,2) as locale,
			city as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% GPM - Comparison L3M'::text as kpi,


			CASE WHEN ((CASE WHEN SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END) > 0 THEN(SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END)-SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN salary_payed ELSE NULL END))/SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END)ELSE NULL END)) != 0 THEN 

				(CASE WHEN SUM(CASE WHEN monthnum = EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END) > 0 THEN (SUM(CASE WHEN monthnum = EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END)-SUM(CASE WHEN monthnum = EXTRACT(month from current_date) - 1 THEN salary_payed ELSE NULL END))/SUM(CASE WHEN monthnum = EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END)ELSE NULL END)
				/
				(CASE WHEN SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END) > 0 THEN(SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END)-SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN salary_payed ELSE NULL END))/SUM(CASE WHEN monthnum < EXTRACT(month from current_date) - 1 THEN revenue ELSE NULL END)ELSE NULL END)
				-
				1


			ELSE NULL END as value

		FROM

			(	SELECT
					EXTRACT(year from mindate) as yearnum,
					EXTRACT(month from mindate) as monthnum,
					delivery_area as city,
					SUM(revenue) as revenue,
					SUM(salary_payed) as salary_payed

				FROM bi.gpm_weekly_cleaner

				WHERE EXTRACT(year from mindate) = EXTRACT(year from current_date)
					AND EXTRACT(month from mindate) >= (EXTRACT(month from current_date)-4)
					AND EXTRACT(month from mindate) < (EXTRACT(month from current_date))

				GROUP BY EXTRACT(year from mindate), EXTRACT(month from mindate), delivery_area

				ORDER BY yearnum desc, monthnum desc, delivery_area asc

			) as t1

		GROUP BY yearnum, monthnum, weeknum, LEFT(city,2), city

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	--'UR %' PROBLEM ! HOW THE FUCK CAN I GET LESS HOURS MONTHLY THAN WEEKLY FFS

	INSERT INTO bi.city_overview_dashboard

		SELECT
			
			NULL::int as yearnum,
			NULL::int as monthnum,
			NULL::int as weeknum,
			NULL::date as mindate,
			left(city,2) as locale,
			city as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% UR - Comparison L3M'::text as kpi,


			(SUM(CASE WHEN monthnum = (EXTRACT(month from current_date)-1) THEN (CASE WHEN worked_hours > weekly_hours THEN weekly_hours ELSE worked_hours END)
				ELSE NULL END
			)
			/
			SUM(CASE WHEN monthnum = (EXTRACT(month from current_date)-1) THEN (weekly_hours) ELSE NULL END) - 	SUM(CASE WHEN monthnum < (EXTRACT(month from current_date)-1) THEN (CASE WHEN worked_hours > weekly_hours THEN weekly_hours ELSE worked_hours END)
				ELSE NULL END
			)
			/
			SUM(CASE WHEN monthnum < (EXTRACT(month from current_date)-1) THEN (weekly_hours) ELSE NULL END))
			/
			(	SUM(CASE WHEN monthnum < (EXTRACT(month from current_date)-1) THEN (CASE WHEN worked_hours > weekly_hours THEN weekly_hours ELSE worked_hours END)
				ELSE NULL END
			)
			/
			SUM(CASE WHEN monthnum < (EXTRACT(month from current_date)-1) THEN (weekly_hours) ELSE NULL END)) as evolution

		FROM

			(SELECT
				EXTRACT(year from mindate) as yearnum,
				EXTRACT(month from mindate) as monthnum,
				delivery_area as city,
				SUM(worked_hours) as worked_hours,
				SUM(weekly_hours) as weekly_hours
			
			FROM bi.gpm_weekly_cleaner

			WHERE EXTRACT(year from mindate) = EXTRACT(year from current_date)
				AND EXTRACT(month from mindate) >= (EXTRACT(month from current_date)-4)
				AND EXTRACT(month from mindate) < (EXTRACT(month from current_date))

			GROUP BY EXTRACT(year from mindate), EXTRACT(month from mindate), delivery_area

			ORDER BY yearnum desc, monthnum desc, delivery_area asc) as t1

		GROUP BY city, locale

		ORDER BY city desc

	;

	--'% churned customers'


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_churnlist;
DROP TABLE IF EXISTS bi.temp_churncount_weekly;
DROP TABLE IF EXISTS bi.temp_activecount_weekly;
DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;
DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'