
CREATE OR REPLACE FUNCTION bi.daily$01_coreprocedures(crunchdate date) RETURNS void AS 
$BODY$
BEGIN

-- CPA Calculation

DROP TABLE IF EXISTS  bi.OrderCustomers;
CREATE TABLE bi.OrderCustomers(acquisition_date varchar(20) DEFAULT NULL, locale varchar(2) DEFAULT NULL, OrderNo SERIAL, Customer_Id__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,   creation_date varchar(15) DEFAULT NULL,   acquisition varchar(20) DEFAULT NULL,  PRIMARY KEY(Customer_Id__c,OrderNo) );

INSERT INTO bi.OrderCustomers(acquisition_date,locale,OrderNo,Customer_Id__c,Order_Id__c,creation_date,acquisition)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  LEFT(locale__c,2) as locale,
  row_number() OVER (PARTITION BY customer_Id__c Order by Customer_Id__c,Cast(Order_Creation__c as Date)) as a,
  Customer_Id__c,
  Order_Id__c,
  Cast(Order_Creation__c as Date) as creation_date,
  Acquisition_New_Customer__c 
FROM
  Salesforce.Order t1
WHERE
  t1.test__c = '0'
  and Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
 ORDER BY
  Customer_Id__c,
  Cast(Order_Creation__c as Date);

DROP TABLE IF EXISTS bi.ValidOrders3;
CREATE TABLE bi.ValidOrders3 as 
SELECT
	creation_date,
	COUNT(1) as Orders
FROM
	bi.OrderCustomers
WHERE
	OrderNo = 1
GROUP BY
	creation_date;

DROP TABLE IF EXISTS bi.VoucherCosts;
CREATE TABLE bi.VoucherCosts as 
SELECT
	CAST(Order_Creation__c as DATE) as OrderDate,
	LEFT(Locale__c,2) as locale,
	Voucher__c,
	CASE 
	WHEN Voucher__c = 'TIGERRAUSCH20' THEN COUNT(DISTINCT(Order_Id__C))*7 
	WHEN Voucher__c = 'SPARTIGER20' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'IMMERSAUBER15' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'PUTZENLASSEN22' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'CLEANPONY' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'PUTZCHECKER14' THEN COUNT(DISTINCT(Order_Id__c))*22
	WHEN Voucher__c = 'TIGER4YOU' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'TIGER1H' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'TIGERSTUFF' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'TIGERFITO20' THEN COUNT(DISTINCT(Order_Id__c))*7
	WHEN Voucher__c = 'TIGEREF15' THEN COUNT(DISTINCT(Order_Id__c))*10
	WHEN Voucher__c = 'CPRTGR2320' THEN COUNT(DISTINCT(Order_Id__c))*15
	ELSE COUNT(DISTINCT(Order_Id__c))*7
	END as VoucherCosts
FROM
	Salesforce.Order t1
WHERE
	(Voucher__c in ('TIGERRAUSCH20','SPARTIGER20','TLCTIGER15','PUTZENLASSEN22','IMMERSAUBER15','TIGERGM','CLEANPONY','TIGER4YOU','CPRTGR2320','TIGER1H','TIGERSTUFF','TIGERFITO20','TIGEREF15') or Voucher__c like '%CRB%')
	and t1.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
GROUP BY
	CAST(Order_Creation__c as DATE),
	locale,
	Voucher__c;

DROP TABLE IF EXISTS bi.VoucherCosts2;
CREATE TABLE bi.VoucherCosts2 as 	
SELECT
	orderdate,
	locale,
	sum(CASE WHEN vouchercosts is null then 0 else vouchercosts end) as VoucherCosts
FROM
	bi.VoucherCosts t3
GROUP BY
	orderdate,
	locale;
  
DROP TABLE IF EXISTS bi.MarketingCostsTableau;
CREATE TABLE bi.MarketingCostsTableau as 
SELECT
  Date(t1.Orderdate) as Date,
  t1.*,
  CASE WHEN t4.VoucherCosts is null THEN 0 ELSE t4.VoucherCosts END as VoucherCosts,
  (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN (22*0.6) ELSE 0 END) as GPDD_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as Other_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Acquisition_New_Customer__c = '0' THEN Discount__c ELSE 0 END) as Other_Voucher_reb,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Acquisitions,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' and t2.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') THEN 1 ELSE 0 END) as Valid_Acquisitions,
  Count(1) as All_Orders,
  t3.Orders as First_Order_Acquisitions,
  SUM(Order_Duration__c) as Hours
FROM
  bi.MarketingCostsImport t1
JOIN
  Salesforce.Order t2
ON
  (CAST(t1.Orderdate  as DATE) = CAST(t2.Order_Creation__c as DATE) and t2.Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') and test__c = '0' )
LEFT JOIn
	bi.ValidOrders3 t3
ON
	(CAST(t1.orderdate as DATE) = CAST(t3.creation_date as date))
LEFT JOIN
	bi.VoucherCosts2 t4
ON
	(CAST(t1.Orderdate  as DATE) = t4.orderdate)
GROUP BY
  Date(t1.Orderdate),
  sem_non_brand,
  sem_brand,
  display,
  seo,
  seo_brand,
  facebook,
  offline_marketing,
  tvcampaign,
  youtube,
  t1.coops,
  t3.Orders,
  VoucherCosts;

DROP TABLE IF EXISTS bi.cpacalclocale;
CREATE TABLE bi.cpacalclocale as 
SELECT
  CAST(Order_Creation__C as Date)  as Date,
  sum(CASE WHEN Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END)  as All_Acquisitions,
  t4.orders as Valid_Acquisitions,
  COUNT(1) as Orders,
  SUM(CASE WHEN Marketing_Channel = 'DTI' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as DTI_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEO Brand' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as SEOBrand_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEM' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as SEM_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEM Brand' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as SEM_Brand_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Display' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Display_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Facebook' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 end) as Facebook_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEO' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as SEO_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'Voucher Campaigns' and Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Vouchercampaigns_Acquisitions,
  SUM(CASE WHEN Marketing_Channel = 'SEM' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as SEM_Discount,
  SUM(CASE WHEN Marketing_Channel = 'SEM Brand' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as SEMBrand_Discount,
  SUM(CASE WHEN Marketing_Channel = 'Display' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as Display_Discout,
  SUM(CASE WHEN Marketing_Channel = 'Facebook' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as Facebook_Discount,
  SUM(CASE WHEN Marketing_Channel = 'Voucher Campaigns' and Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Acquisition_New_Customer__c = '1' and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%DEINDEAL%' and Voucher__c not like '%DEAL%' THEN Discount__c ELSE 0 END)+(SUM(CASE WHEN Marketing_Channel = 'Voucher Campaigns' and Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Acquisition_New_Customer__c = '1' and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') THEN (Discount__c*0.8)-(22*0.6) ELSE 0 END)) as Vouchercampaigns_Discount,
  (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-(22*0.6) as GPDD_Voucher,
  CASE WHEN SUM(CASE WHEN (Voucher__c like '%DEINDEAL%' or Voucher__c like '%DEAL%') and Status not like '%CANCELLD%' THEN 1 ELSE 0 END) > 0 THEN (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%DEINDEAL%' or Voucher__c like '%DEAL%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-(80*0.65) ELSE 0 END as Deindeal_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Voucher__c not like '%DEINDEAL%' and Voucher__c not like '%DEAL%' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END ) as Other_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Voucher__c not like '%DEINDEAL%' and Acquisition_New_Customer__c = '0' THEN Discount__c ELSE 0 END ) as Other_Voucher_reb,
  t1.*,
  CASE WHEN t3.VoucherCosts is null THEN 0 ELSE t3.VoucherCosts END as VoucherCosts
FROM
     bi.marketingspending_locale t1
 JOIN
  bi.orders t2
ON
  (CAST(Order_Creation__C as Date) = CAST(t1.Orderdate as date) and t1.locale = LEFT(t2.locale__c,2)) 
LEFT JOIN
	bi.VoucherCosts2 t3
ON
	(CAST(Order_Creation__C as Date) = t3.orderdate and t1.locale = t3.locale)
LEFT JOIN
	bi.ValidOrders t4
ON
	(t1.orderdate::date = t4.creation_date::date and t1.locale = t4.locale)
WHERE
 	t2.Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')  and test__c = '0'
GROUP BY
  CAST(Order_Creation__C as Date),
  CAST(t1.Orderdate as date),
 sem_non_brand,
  sem_brand,
  seo,
  criteo,
  sociomantic,
  gdn,
  facebook,
  offline_marketing,
  tvcampaign,
  youtube,
  t1.coops,
  t1.locale,
  VoucherCosts,
  t4.orders;

DROP TABLE IF EXISTS bi.cpa_by_locale;
CREATE TABLE bi.cpa_by_locale as 
SELECT
  t1.*,
  CASE WHEN t4.VoucherCosts is null THEN 0 ELSE t4.VoucherCosts END as VoucherCosts,
  (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%CDD%' or Voucher__c like '%CGP%') and Acquisition_New_Customer__c = '1' THEN (22*0.6) ELSE 0 END) as GPDD_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Voucher__c not like '%DEINDEAL%' and Voucher__c not like '%DEAL%' and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END) as Other_Voucher,
  SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and Voucher__c not like '%CDD%' and Voucher__c not like '%CGP%' and Voucher__c not like '%CM%' and Acquisition_New_Customer__c = '0' THEN Discount__c ELSE 0 END) as Other_Voucher_reb,
      (SUM(CASE WHEN Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') and (Voucher__c like '%DEINDEAL%' or Voucher__c like '%DEAL%') and Acquisition_New_Customer__c = '1' THEN Discount__c ELSE 0 END)*0.8)-(89*0.6) as Deindeal_Voucher,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN 1 ELSE 0 END) as Acquisitions,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' and t2.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') THEN 1 ELSE 0 END) as Valid_Acquisitions,
  Count(1) as All_Orders,
  t3.Orders as First_Order_Acquisitions,
  SUM(Order_Duration__c) as Hours
FROM
  bi.marketingspending_locale t1
JOIN
  Salesforce.Order t2
ON
  (CAST(t1.Orderdate  as DATE) = CAST(t2.Order_Creation__c as DATE) and t2.Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') and test__c = '0' and LEFT(t2.locale__c,2) = t1.locale)
LEFT JOIn
	bi.ValidOrders t3
ON
	(CAST(t1.orderdate as DATE) = CAST(t3.creation_date as date) and t1.locale = t3.locale )
LEFT JOIN
	bi.VoucherCosts2 t4
ON
	(CAST(t1.Orderdate  as DATE) = t4.orderdate and t1.locale = t4.locale)
GROUP BY
  Date(t1.Orderdate),
  sem_non_brand,
  sem_brand,
  seo,
  criteo,
  sociomantic,
  gdn,
  facebook,
  offline_marketing,
  tvcampaign,
  youtube,
  t1.coops,
  t3.Orders,
  t1.locale,
  VoucherCosts;

DROP TABLE IF EXISTS bi.VoucherCosts;

DROP TABLE IF EXISTS bi.MarketingVouchers;
CREATE TABLE bi.MarketingVouchers as 
SELECT
  CAST(Order_Creation__c as DATE)  as OrderDate,
  CASE WHEN Acquisition_New_Customer__c = '1' THEN 'Acquisitions' ELSE 'Rebookings' END as Ordertype,
  LEFT(Locale__c,2) as Locale,
  CASE 
  WHEN Voucher__c like '%CDD%' or Voucher__c like '%CGP%' THEN 'GROUPON'
  WHEN Voucher__C like 'CRB%' THEN 'RUBLYS'
  ELSE Voucher__c
  END  as Voucher,
  count(1)
FROM
  Salesforce.Order
WHERE
  Voucher__c is not null
  and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') and test__c = '0' 
GROUP BY
   Date(Order_Creation__c),
   Locale__c,
   OrderType,
   Voucher;

DROP TABLE IF EXISTS bi.VoucherAcquisitions;
CREATE TABLE bi.VoucherAcquisitions as 
SELECT
  CAST(Order_Creation__c as Date) as OrderDate,
  CASE WHEN Acquisition_New_Customer__c = '1' THEN 'Acquisitions' ELSE 'Rebookings' END as Ordertype,
  LEFT(Locale__c,2) as Locale,
  CASE
  WHEN Voucher__c like '%CGP%' THEN 'Groupon'
  WHEN Voucher__c like '%CDD%' THEN 'DailyDeal'
  WHEN Voucher__c like '%CLM%' THEN 'Limango'
  WHEN Voucher__c like '%CRB%' THEN 'Rublys'
  WHEN Voucher__c like '%CM%' THEN 'CM'
  ELSE Voucher__c 
  END as Voucher,
  Count(1) as Order_Count
FROM
  Salesforce.Order
Where
  Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  and (Voucher__c is not null and Voucher__c != '')
GROUP BY
  CAST(Order_Creation__c as Date),
  locale,
  Ordertype,
  Voucher;
 
 -- GMV Forecast
 
DROP TABLE IF EXISTS bi.booking_forecast_1;
CREATE TABLE bi.booking_forecast_1 as 
SELECT
	Marketing_Channel,
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN Order_Duration__c ELSE 0 END) as decimal)/14),0) as Acquisition_Runrate_Per_Day,
		round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN Order_Duration__c ELSE 0 END) as decimal)/14),0) as Rebookings_Runrate_Per_Day,
	  DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
  	Extract(day from current_date-1) as days
FROM
	bi.orders
WHERE
   Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  	AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days') 
  	
GROUP BY
	Marketing_Channel;

DROP TABLE IF EXISTS bi.booking_forecast_2;
CREATE TABLE bi.booking_forecast_2 as 
SELECT
	Marketing_Channel,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN Order_Duration__c ELSE 0 END) as Acquisition_hours_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN Order_Duration__c ELSE 0 END) as Rebookings_hours_this_month
FROM
	bi.orders
WHERE
	 Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
	 AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM ORder_Creation__c::date) and EXTRACT(Year FROM ORder_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
GROUP BY
	Marketing_Channel;

DROP TABLE IF EXISTS bi.booking_forecast;
CREATE TABLE bi.booking_forecast as 
SELECT
	t1.Marketing_Channel,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
	Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days)) as Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	Rebookings_hours_this_month+(Rebookings_runrate_per_day*(month_days-days)) as Rebookings_Forecast
FROM
	bi.booking_forecast_2 t1
left join
	bi.booking_forecast_1 t2
ON 
	(t1.Marketing_Channel = t2.Marketing_Channel)
GROUP BY
	t1.Marketing_Channel,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
	Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	Rebookings_Forecast;

DROP TABLE IF EXISTS bi.booking_forecast_1;
DROP TABLE IF EXISTS bi.booking_forecast_2; 

DROP TABLE IF EXISTS bi.booking_forecast_1;
CREATE TABLE bi.booking_forecast_1 as 
SELECT
	LEFT(locale__c,2) as locale,
	round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/15),0) as Acquisition_Runrate_Per_Day,
		round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/15),0) as Rebookings_Runrate_Per_Day,
	  DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
   Extract(day from current_date-1) as days
FROM
	bi.orders
WHERE
	Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  	AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days') 
GROUP BY
	LEFT(locale__c,2);

DROP TABLE IF EXISTS bi.booking_forecast_2;
CREATE TABLE bi.booking_forecast_2 as 
SELECT
	LEFT(locale__c,2) as locale,
	SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN GMV_Eur ELSE 0 END) as Acquisition_hours_this_month,
	SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN GMV_Eur ELSE 0 END) as Rebookings_hours_this_month
FROM
	bi.orders
WHERE
	 Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','NOSHOW CUSOTMER','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  	 AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM ORder_Creation__c::date) and EXTRACT(Year FROM ORder_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
  	 
GROUP BY
	left(locale__c,2); 
 
DROP TABLE IF EXISTS bi.booking_forecast_locale;
CREATE TABLE bi.booking_forecast_locale as 
SELECT
	t1.locale,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days))  Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)) as rebookings_Forecast
FROM
	bi.booking_forecast_2 t1
left join
	bi.booking_forecast_1 t2
ON 
	(t1.locale = t2.locale)
GROUP BY
	t1.locale,
	Acquisition_hours_this_month,
	acquisition_runrate_per_day,
	Acquisition_Forecast,
	Rebookings_hours_this_month,
	Rebookings_runrate_per_day,
	Rebookings_Forecast;

DROP TABLE IF EXISTS bi.booking_forecast_1;
DROP TABLE IF EXISTS bi.booking_forecast_2; 


-- Acquisition Targets






-- Target vs Actual 

-- DROP TABLE IF EXISTS bi.targetvsactual;
-- CREATE TABLE bi.targetvsactual as 
-- SELECT
	-- '2015-09-01'::date as Month,
	-- 'de'::text as locale,
	-- 'aa'::text as KPI,
	-- 'Number'::text as type,
	-- 0::integer as Value;
	-- 
	
DELETE FROM bi.targetvsactual;	
	
INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','CPA','Target',61);

INSERT INTO bi.targetvsactual
SELECT 
	date,
	locale,
	'CPA' as KPI,
	'Actual' as KPI,
	Global_CPA
FROM(
	SELECT
		EXTRACT(MONTH FROM orderdate::date) as Month,
		min(Orderdate) as Date,
		CASE WHEN locale in ('at','de') THEN 'de' ELSE 'ch' END as locale,
		round(CAST(SUM(Sem_Brand+Sem_Non_Brand+Offline_Marketing+Facebook+Sociomantic+Criteo+Gdn+Gpdd_Voucher+Other_Voucher+Vouchercosts+Tvcampaign+Seo) as decimal)/SUM(All_Acquisitions),2) as Global_CPA
	FROM
		bi.cpacalclocale 
	GROUP BY
		Month,
		CASE WHEN locale in ('at','de') THEN 'de' ELSE 'ch' END)
as a;

INSERT INTO bi.targetvsactual
SELECT 
	date,
	'all' as locale,
	'CPA' as KPI,
	'Actual' as KPI,
	Global_CPA
FROM(
	SELECT
		EXTRACT(MONTH FROM orderdate::date) as Month,
		min(Orderdate) as Date,
		round(CAST(SUM(Sem_Brand+Sem_Non_Brand+Offline_Marketing+Facebook+coops+Sociomantic+Criteo+Gdn+Gpdd_Voucher+Other_Voucher+Vouchercosts+Tvcampaign+seo) as decimal)/SUM(All_Acquisitions),2) as Global_CPA
	FROM
		bi.cpacalclocale 
WHERE
	date >= '2015-06-01'
	GROUP BY
		Month)
as a;

DELETE FROM bi.targetvsactual WHERE type = 'GMV';

INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','GMV','Target',826737);

INSERT INTO bi.targetvsactual
SELECT
	CAST(NOW() as DATE) - Interval '1 Day' as Month,
	'all' as locale,
	'GMV' as kpi,
	'Actual' as text,
	SUM(acquisition_forecast)+sum(rebookings_forecast) as gmv
FROM
	bi.booking_forecast_locale;

DELETE FROM bi.targetvsactual WHERE kpi = 'Onboardings';

INSERT INTO bi.targetvsactual
SELECT
	CAST(current_date- interval '1 days' as date) as month,
	'all' as locale,
	'Onboardings' as kpi,
	'Actual' as goal,
	 (a.current_value/a.days)*working_days as value
FROM(
SELECT
	'Onobardings_this_month' as kpi,
	SUM(CASE WHEN Extract(MONTH FROM createddate::date) = EXTRACT(MONTH FROM current_date) and Extract(year FROM createddate::date) = EXTRACT(year FROM current_date) THEN 1 ELSE 0 END) as current_value,
	COUNT(DISTINCT(Createddate::Date)) as days,
	22 as working_days
FROM
	salesforce.account
WHERE
	Extract(MONTH FROM createddate::date) = EXTRACT(MONTH FROM current_date) and Extract(YEAR FROM createddate::date) = EXTRACT(YEAR FROM current_date)) as a;
	
INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','Onboardings','Target',0);

INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','TAC','Target',0);

INSERT INTO bi.targetvsactual
SELECT
	a.date as month,
	'all' as locale,
	'TAC' as kpi,
	'Actual' as goal,
	round(MAX(costs)/SUM(b.count),2) as value
FROM
(SELECT
	min(date::date) as date,
	max(date::date) as maxdate,
	SUM(costs) as costs
FROM
	bi.leadcosts
WHERE
	EXTRACT(MONTH FROM CAST(current_date- interval '1 days' as date)) = EXTRACT(MONTH FROM date::date)) as a
LEFT JOIN(
SELECT 
	'2015-12-01'::date as date,
	createddate::date as date2,
	COUNT(1) count
frOm 
salesforce.account
WHERE
	EXTRACT(MONTH FROM CAST(current_date- interval '1 days' as date)) = EXTRACT(MONTH FROM createddate::date) and EXTRACT(YEAR FROM CAST(current_date- interval '1 days' as date)) = EXTRACT(YEAR FROM createddate::date)
GROUP BY
	date,
	date2) as b
ON
	a.date = b.date and date2 <= maxdate
GROUP BY
	a.date;
	
	
INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','Acquisitions','Target',3700);

INSERT INTO bi.targetvsactual
SELECT
	CAST(current_date- interval '1 days' as date) as Date,
	'all' as locale,
	'Acquisitions' as kpi,
	'Actual' as goal,
	(COUNT(1)/(COUNT(DISTINCT(Order_Creation__c::date))-1))*30 as Orders
FROM
	Salesforce.Order
WHERE
	EXTRACT(MONTH FROM CAST(current_date- interval '1 days' as date)) = EXTRACT(MONTH FROM Order_Creation__c::date) and EXTRACT(YEAR FROM CAST(current_date- interval '1 days' as date)) = EXTRACT(YEAR FROM Order_Creation__c::date)
	and Acquisition_New_Customer__c = '1'
	and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE');

INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','M1 RR','Target',32);

INSERT INTO bi.targetvsactual
SELECT
	CAST(current_date- interval '1 days' as date) as Date,
	'all' as locale,
	'M1 RR' as kpi,
	'Actual' as goal,
	round(SUM(rebookings*cohort_return_rate)/SUM(rebookings),2) as ReturnRate
FROM
	bi.Running_Cohort_Analysis_email
WHERE
	CAST(current_date- interval '2 days' as date) = date;
	
INSERT INTO bi.targetvsactual VALUES('2016-02-01'::date,'all','M3 RR','Target',22);

INSERT INTO bi.targetvsactual
SELECT
	CAST(current_date- interval '1 days' as date) as Date,
	'all' as locale,
	'M3 RR' as kpi,
	'Actual' as goal,
	round(SUM(cohort_return_rate*acquisitions)/SUM(acquisitions),4) as value
FROM
	bi.Running_Cohort_Analysis_emailp3
WHERE
	CAST(current_date- interval '2 days' as date) = date ;
 

-- International Cockpit



DROP TABLE IF EXISTS bi.international_cockpit_temp1;
CREATE TABLE bi.international_cockpit_temp1 as 
SELECT
	a.date,
	a.locale,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN 1 ELSE 0 END) as all_nc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN 1 ELSE 0 END) as all_rc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN Order_Duration__c ELSE 0 END) as all_nc_hours,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN Order_Duration__c ELSE 0 END) as all_rc_hours,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN 1 ELSE 0 END) as valid_nc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN 1 ELSE 0 END) as valid_rc_order,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '1' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN order_Duration__c ELSE 0 END) as valid_nc_hours,
	SUM(CASE WHEN Acquisition_New_CUstomer__c = '0' and Status in ('WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN order_duration__c ELSE 0 END) as valid_rc_hours,
	SUM(CASE WHEN Status in ('ALLOCATION PAUSED','INVOICED','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO INVOICE','PENDING TO START','PENDING VALIDATION','WAITING FOR ACCEPTANCE','WAITING FOR RESCHEDULE','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')  THEN gmv_eur ELSE 0 END) as valid_gmv,
	SUM(CASE WHEN Status not in ('CANCELLED FAKED','CANCELLED MISTAKE') THEN gmv_eur ELSE 0 END) as all_gmv
FROM
	bi.date a
LEFT JOIN
	bi.orders as b
ON
	(a.date = b.order_Creation__c::date and a.locale = LEFT(b.locale__c,2) and test__c = '0')
GROUP BY
	date,
	locale;

DROP TABLE IF EXISTS bi.international_cockpit_temp2;
CREATE TABLE bi.international_cockpit_temp2 as 
SELECT
	date,
	locale,
	SUM(Cleaners_inactive) as cleaner_inactive,
	SUM(Cleaners_active) as Cleaner_active
FROM
	bi.cleaneractivity_change
GROUP BY
	date,
	locale;

DROP TABLE IF EXISTS bi.international_cockpit_temp3;
CREATE TABLE bi.international_cockpit_temp3 as 
SELECT
	created_at::date as date,
	LEFT(REPLACE(CAST(professional_json->'Locale__c' as varchar),'"',''),2) as locale,
	COUNT(1) as Offboardings
FROM
	events.sodium
WHERE
	event_name in ('Account Event:LEFT','Account Event:TERMINATED')
GROUP BY
	date,
	locale;
	
DROP TABLE IF EXISTS bi.international_cockpit_temp4;
CREATE TABLE bi.international_cockpit_temp4 as 
SELECT
	createddate::date as date,
	LEFT(locale__C,2) as locale,
	COUNT(1) as Onboardings
FROM
	salesforce.account
WHERE
	test__c = '0'
	and Status__c != 'SUBCONTRACTOR' and Status__c != ''
GROUP BY
	date,
	locale;


DROP TABLE IF EXISTS bi.international_cockpit_temp5;
CREATE TABLE bi.international_cockpit_temp5 as  
SELECT
	orderdate,
	locale,
	SUM(sem_non_brand+sem_brand+seo+facebook+criteo+sociomantic+gdn+youtube+coops+tvcampaign+offline_marketing+vouchercosts+gpdd_voucher+other_voucher) as Costs,
	SUM(Acquisitions) as Acquisitions,
	SUM(first_order_acquisitions) as first_order_acquisitions,
	CASE WHEN SUM(Acquisitions) = 0 THEN 0 ELSE SUM(sem_non_brand+sem_brand+seo+facebook+criteo+sociomantic+gdn+youtube+coops+tvcampaign+offline_marketing+vouchercosts+gpdd_voucher+other_voucher)/SUM(Acquisitions) END as CPA
FROM
	bi.cpa_by_locale
GROUP BY
	orderdate,
	Locale;

DELETE FROM  bi.international_cockpit_temp6 WHERE 	current_date - Interval '1 Day' = date;

INSERT INTO bi.international_cockpit_temp6 
SELECT
	current_date - Interval '1 Day' as date,
	LEFT(locale__c,2) as locale,
	COUNT(1) as cop
FROM
	Salesforce.Account
WHERE
	test__c = '0'
	and Status__c not in ('LEFT','TERMINATED')
GROUP BY
	date,
	locale;

-- DROP TABLE IF EXISTS bi.sourcelead;
-- CREATE TABLE bi.sourcelead as
-- SELECT
	-- created_at::date as date,
	-- LEFT(locale,2) as locale,
	-- COUNT(1) as Leads
-- FROM
	-- main.lead
-- WHERE
	-- created_at::date >= '2015-01-01'
-- GROUP BY
	-- created_at::date,
	-- LEFT(locale,2);




-- DRoP TABLE IF EXISTS bi.leadbysource_2; 
-- CREATE TABLE bi.leadbysource_2 as 	
-- SELECT
	-- date,
	-- locale,
	-- SUM(Leads) as Leads
-- FROM
	-- bi.sourcelead
-- GROUP BY
	-- date,
	-- locale;

-- DROP TABLE IF EXISTS bi.leadcosts2;
-- CREATE TABLE bi.leadcosts2 as 
-- SELECT
	-- date as date,
	-- lower(locale) as locale,
	-- SUM(Costs) as Costs
-- FROM
	-- bi.LeadCosts
-- GROUP BY
	-- date,
	-- lower(locale);
	
-- DROP TABLE IF EXISTS bi.international_cockpit_temp8;
-- CREATE TABLE bi.international_cockpit_temp8 as 
-- SELECT
	-- t2.date::date as date,
	-- t2.locale,
	-- sum(costs) as costs,
	-- SUM(t1.leads) as leads
-- FROM
	-- bi.leadcosts2 t2
-- LEFT JOIN
	-- bi.leadbysource_2 t1
-- ON
	-- (t1.date::date = t2.date::date and t1.locale = t2.locale)
-- GROUP BY
	-- t2.date,
	-- t2.locale;

DROP TABLE IF EXISTS bi.leadcosts2;
DRoP TABLE IF EXISTS bi.leadbysource_2; 
DROP TABLE IF EXISTS bi.sourcelead;

DROP TABLE IF EXISTS bi.international_cockpit;
CREATE TABLE bi.international_cockpit as 
SELECT
	t1.*,
	t2.distinct_cleaner,
	t2.hours_per_cleaner,
	t3.cleaner_inactive,
	t3.cleaner_active,
	CASE WHEN t4.onboardings is null then 0 else t4.onboardings END as onboardings,
	CASE WHEN t5.offboardings is null then 0 else t5.offboardings END as offboardings,
	t6.costs,
	t6.acquisitions,
	t6.first_order_acquisitions,
	t7.cop,
	t8.cohort_return_rate as p1_retention_rate,
	t9.cohort_return_rate as p3_retention_rate,
	t10.costs as onboardingcosts,
	t10.leads as leads
FROM
	bi.international_cockpit_temp1 t1 
LEFT JOIn
	bi.CleanerActivityLast30Days t2
ON
	(t1.locale = t2.locale and t1.date = t2.orderdate)
LEFT JOIn
	bi.international_cockpit_temp2 t3
ON
	(t1.locale = t3.locale and t1.date = t3.date)
LEFT JOIN
	bi.international_cockpit_temp3 t5
ON
	(t1.locale = t5.locale and t1.date = t5.date)
LEFT JOIN
	bi.international_cockpit_temp4 t4
ON
	(t1.locale = t4.locale and t1.date = t4.date)
LEFT JOIN
	bi.international_cockpit_temp5 t6
ON
	(t1.locale = t6.locale and t1.date = t6.orderdate)
LEFT JOIN
	bi.international_cockpit_temp6 t7
ON
	(t1.locale = t7.locale and t1.date = t7.date)
LEFT JOIN
	bi.Running_Cohort_Analysis_email t8
ON
	(t1.locale = t8.locale and t1.date = t8.date)
LEFT JOIN
	bi.Running_Cohort_Analysis_emailp3 t9
ON
	(t1.locale = t9.locale and t1.date = t9.date)
LEFT JOIn
	bi.international_cockpit_temp8 t10
ON
	(t1.locale = t10.locale and t1.date = t10.date)
WHERE
	t1.date >= '2015-01-01';

DROP TABLE IF EXISTS bi.international_cockpit_temp1;
DROP TABLE IF EXISTS bi.international_cockpit_temp2;
DROP TABLE IF EXISTS bi.international_cockpit_temp3;
DROP TABLE IF EXISTS bi.international_cockpit_temp4;
DROP TABLE IF EXISTS bi.international_cockpit_temp5;
  

 END;

$BODY$ LANGUAGE 'plpgsql'