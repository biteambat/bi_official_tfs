
CREATE OR REPLACE FUNCTION bi.daily$muffin_trainings (crunchdate date) RETURNS void AS
$BODY$

DECLARE 
function_name varchar := 'bi.daily$muffin_trainings';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
	
	DROP TABLE IF EXISTS bi.trainings_muffins;
	CREATE TABLE bi.trainings_muffins AS

		SELECT
			*
		FROM salesforce.hr__c hr

		WHERE (hr.description__c LIKE ('%Training%') OR hr.description__c LIKE ('%training%') OR hr.description__c LIKE ('%2h%') OR hr.type__c = 'training')

	;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


END;

$BODY$ LANGUAGE 'plpgsql'