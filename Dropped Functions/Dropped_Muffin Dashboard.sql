
CREATE OR REPLACE FUNCTION bi.daily$muffindashboard(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$muffindashboard';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DROP FUNCTION bi.daily$muffindashboard(crunchdate date);

--CREATE INDEX professional_id_index ON salesforce.order(professional__c);

DROP TABLE IF EXISTS bi.muffin_kpis;
CREATE TABLE bi.muffin_kpis as 
SELECT
	createddate::date as date,
	'Total Onboardings' as kpi,
	delivery_areas__c as City,
	CAST(COUNT(1) as decimal) as value
FROM
	Salesforce.Account
WHERE
	type__c = '60'
GROUP BY
	date,
	kpi,
	city;

	
INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'GMV by Muffin' as kpi,
	t2.delivery_areas__c as City,
	SUM(t1.PPH__c*ORder_Duration__c) as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c = '60')
GROUP BY
	date,
	kpi,
	city;
	
INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'GMV by Muffin' as kpi,
	'DE-Total Muffin' as city,
	SUM(t1.PPH__c*ORder_Duration__c) as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c = '60')
WHERE
	t2.delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	date,
	kpi;

	
INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'GMV by Old' as kpi,
	'DE-Total Muffin' as city,
	SUM(t1.PPH__c*ORder_Duration__c) as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c != '60')
WHERE
	t2.delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	date,
	kpi;

INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'GMV' as kpi,
	t2.delivery_areas__c as City,
	SUM(t1.PPH__c*ORder_Duration__c)  as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START'))
GROUP BY
	date,
	kpi,
	city;

INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'GMV' as kpi,
	'DE-Total Muffin' as city,
	SUM(t1.PPH__c*ORder_Duration__c)  as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START'))
WHERE
	t2.delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	date,
	kpi,
	city;

INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'GMV by Old' as kpi,
	t2.delivery_areas__c as City,
	SUM(t1.PPH__c*ORder_Duration__c)  as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and type__c != '60')
GROUP BY
	date,
	kpi,
	city;
	
INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Hours by Muffin' as kpi,
	t2.delivery_areas__c as City,
	SUM(ORder_Duration__c) as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c = '60')
GROUP BY
	date,
	kpi,
	city;

INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Hours by Muffin' as kpi,
	'DE-Total Muffin' as City,
	SUM(ORder_Duration__c) as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c = '60')
GROUP BY
	date,
	kpi;
	
INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Hours' as kpi,
	t2.delivery_areas__c as City,
	SUM(ORder_Duration__c)  as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START'))
GROUP BY
	date,
	kpi,
	city;

INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Hours by Old' as kpi,
	t2.delivery_areas__c as City,
	SUM(ORder_Duration__c) as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c != '60')
GROUP BY
	date,
	kpi,
	city;
	

INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Orders by Muffin' as kpi,
	t2.delivery_areas__c as City,
	COUNT(DISTINCT(Order_Id__c)) as value 
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c = '60')
GROUP BY
	date,
	kpi,
	city;
	
INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Orders' as kpi,
	t2.delivery_areas__c as City,
   COUNT(DISTINCT(Order_Id__c))  as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START'))
GROUP BY
	date,
	kpi,
	city;


INSERT INTO bi.muffin_kpis	
SELECT
	Effectivedate::Date as date,
	'Orders by Old' as kpi,
	t2.delivery_areas__c as City,
   COUNT(DISTINCT(Order_Id__c))  as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and type__c != '60')
GROUP BY
	date,
	kpi,
	city;
	
INSERT INTO bi.muffin_kpis	
SELECT
	date as date,
	'All Active Cleaners' as Active_Cleaners,
	city,
	distinct_cleaner as value
FROM(
SELECT
	MIN(Effectivedate::Date) as date,
	t2.delivery_areas__c as city,
	EXTRACT(Year FROM Effectivedate::Date) as year,
	EXTRACT(WEEK FROM Effectivedate::Date) as week,
	COUNT(DISTINCT(professional__c)) as distinct_cleaner
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START'))
GROUP BY
	year,
	week,
	t2.delivery_areas__c) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	CASE WHEN date = '2015-11-04' THEN '2015-11-02'::date ELSE date END as date,
	'Active Cleaners Muffin' as Active_Cleaners,
	city,
	distinct_cleaner as value
FROM(
SELECT
	MIN(Effectivedate::Date) as date,
	t2.delivery_areas__c as city,
	EXTRACT(Year FROM Effectivedate::Date) as year,
	EXTRACT(WEEK FROM Effectivedate::Date) as week,
	COUNT(DISTINCT(professional__c)) as distinct_cleaner
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and type__c = '60')
GROUP BY
	year,
	week,
	t2.delivery_areas__c) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	CASE WHEN date = '2015-11-04' THEN '2015-11-02'::date ELSE date END as date,
	'Active Cleaners Old' as Active_Cleaners,
	city,
	distinct_cleaner as value
FROM(
SELECT
	MIN(Effectivedate::Date) as date,
	t2.delivery_areas__c as city,
	EXTRACT(Year FROM Effectivedate::Date) as year,
	EXTRACT(WEEK FROM Effectivedate::Date) as week,
	COUNT(DISTINCT(professional__c)) as distinct_cleaner
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and type__c != '60')
GROUP BY
	year,
	week,
	t2.delivery_areas__c) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	date,
	'PPH by Muffin' as kpi,
	city,
	CAST(value as decimal)
FROM(
SELECT
	EXTRACT(WEEK FROM Effectivedate::Date) as week,
	EXTRACT(YEAR FROM Effectivedate::date) as year,
	MIN(Effectivedate::Date) as date,
	'PPH' as kpi,
	t2.delivery_areas__c as City,
   SUM(Order_Duration__c*t1.PPH__c)/SUM(Order_Duration__c)   as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and t2.type__c = '60')
GROUP BY
	week,
	year,
	kpi,
	city) as a;
	
INSERT INTO bi.muffin_kpis	
SELECT
	date,
	'PPH' as kpi,
	city,
	CAST(value as decimal)
FROM(
SELECT
	EXTRACT(WEEK FROM Effectivedate::Date) as week,
	EXTRACT(YEAR FROM Effectivedate::date) as year,
	MIN(Effectivedate::Date) as date,
	'PPH' as kpi,
	t2.delivery_areas__c as City,
   SUM(Order_Duration__c*t1.PPH__c)/SUM(Order_Duration__c)   as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START'))
GROUP BY
	week,
	year,
	kpi,
	city) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	date,
	'PPH by Old' as kpi,
	city,
		CAST(value as decimal)
FROM(
SELECT
	EXTRACT(WEEK FROM Effectivedate::Date) as week,
	EXTRACT(YEAR FROM Effectivedate::date) as year,
	MIN(Effectivedate::Date) as date,
	'PPH by Old' as kpi,
	t2.delivery_areas__c as City,
   SUM(Order_Duration__c*t1.PPH__c)/SUM(Order_Duration__c)   as value
FROM
	Salesforce.Order t1
JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('INVOICED','PENDING TO START') and type__c != '60')
GROUP BY
	week,
	year,
	kpi,
	city) as a;

DROP TABLE IF EXISTS bi.left_date;
CREATE TABLE bi.left_date as 	
SELECT
	professional_json->>'Id' as sfid,
	min(created_at::date) as date
FROM
	events.sodium
WHERE
	event_name in ('Account Event:LEFT','Account Event:TERMINATED','Account Event:SUSPENDED')
GROUP BY
	sfid;

DROP TABLE IF EXISTS bi.utalization_temp2;
CREATE TABLE bi.utalization_temp2 as 	
SELECT
	t2.sfid,
	t2.delivery_areas__c,
	t1.Effectivedate::Date as date,
	SUM(Order_Duration__c) as Worked_Hours
FROM
	Salesforce.Account t2
LEFT JOIN 
	Salesforce.Order t1
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('PENDING TO START','INVOICED'))
WHERE
	t2.type__c = '60'
GROUP BY
	t2.sfid,
	t2.delivery_areas__c,
	date;

DROP TABLE IF EXISTS bi.utalization_temp1;
CREATE TABLE bi.utalization_temp1 as 	
SELECT
	sfid,
	date,
	t2.delivery_areas__c,
	hr_contract_end__c::date as end_date,
	hr_contract_start__c::Date as onboarding_date,
	hr_contract_weekly_hours_min__c as Max_Hours,
	hr_contract_weekly_hours_max__c as Availability_Capacity
FROM
	Salesforce.Account t2,
	bi.orderdate
WHERE
	type__c = '60'
GROUP BY
	sfid,
	date,
	t2.delivery_areas__c,
	onboarding_date,
	end_date,
	max_hours,
	Availability_Capacity;

DROP TABLE IF EXISTS bi.utalization_temp12;
CREATE TABLE bi.utalization_temp12 as 		
SELECT
	t1.*,
	t2.date as left_Date
FROM
	bi.utalization_temp1 t1
LEFT JOIN
	bi.left_date t2
ON
	(t1.sfid = t2.sfid and t2.date >= t1.date);

DROP TABLE IF EXISTS bi.utalization_temp3;
CREATE TABLE  bi.utalization_temp3 as 	
SELECT
	t1.date,
	left_date,
	t1.delivery_areas__c,
	onboarding_date,
	end_date::date as end_date,
	t1.sfid,
	CASE WHEN max_hours is null THEN 0 ELSE max_hours END as max_hours,
	Availability_Capacity,
	worked_hours
FROM
	bi.utalization_temp12 t1
LEFT JOIN
	bi.utalization_temp2 t2
ON
	(t1.sfid = t2.sfid and t1.date = t2.date)
WHERE
	t1.date::date between '2015-10-26' and '2016-09-30';


DROP TABLE IF EXISTS bi.utalization_per_cleaner;
CREATE TABLE bi.utalization_per_cleaner as 
SELECT
	EXTRACT(WEEK FROM date) as week,
	CAST(min(date) as date) as min_date,
	delivery_areas__c,
	end_date::date as end_date,
	sfid,
	left_date,
	onboarding_date,
	SUM(worked_hours) as Worked_Hours,
	MAX(MAX_hours) as Min_Hours,
	MAX(Availability_Capacity) as availability_capacity,
	CASE WHEN SUM(worked_hours) > MAX(MAX_hours) THEN MAX(MAX_hours) ELSE SUM(worked_hours) END as worked_hours_2
FROM
	bi.utalization_temp3
GROUP BY
	Week,
	delivery_areas__c,
	end_date,
	left_date,
	onboarding_date,
	sfid;

DROP TABLE IF EXISTS bi.utalization_per_cleaner_weekly;
CREATE TABLE bi.utalization_per_cleaner_weekly as 
SELECT
	week as cw,
	cast(date_trunc('week', min_date) as date) as first_day_of_cw,
	delivery_areas__c as city,
	end_date as contract_end,
	sfid as cleanerid,
	left_date as terminated_date,
	onboarding_date as start_date,
	worked_hours,
	min_hours as contract_hours,
	availability_capacity as availability_hours
FROM
	bi.utalization_per_cleaner; 

DROP TABLE IF EXISTS bi.utalization_per_cleaner2;
CREATE TABLE bi.utalization_per_cleaner2 as 
SELECT
	week,
	delivery_areas__c,
	SUM(Worked_Hours) as Worked_Hours,
	SUM(worked_hours_2) as Worked_Hours_2,
	SUM(CASE WHEN onboarding_date <= min_date and end_date > min_date and (left_date > min_date or left_date is null) THEN Min_Hours ELSE 0 END) as Min_Hours,
	SUM(CASE WHEN onboarding_date <= min_date and end_date > min_date and (left_date > min_date or left_date is null) THEN availability_capacity ELSE 0 END) as available_hours,
	SUM(CASE WHEN onboarding_date <= min_date and end_date > min_date and (left_date > min_date or left_date is null) and worked_hours_2 is null THEN Min_Hours ELSE 0 END) as cleaner_0_hours
FROM
	bi.utalization_per_cleaner t1
GROUP BY
	week,
	delivery_areas__c;


INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Capacity Contract' as kpi,
	delivery_areas__c as city,
	min_hours as value FROM
(SELECT
	week,
	delivery_areas__c,
	min(t2.date) as date,
	Worked_Hours,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	min_hours) as a;
	
INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Capacity Availability' as kpi,
	delivery_areas__c as city,
	min_hours as value FROM
(SELECT
	week,
	delivery_areas__c,
	min(t2.date) as date,
	Worked_Hours,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	min_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Capacity' as kpi,
	delivery_areas__c as city,
	min_hours as value FROM
(SELECT
	week,
	delivery_areas__c,
	min(t2.date) as date,
	Worked_Hours,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	min_hours) as a;

DROP TABLE IF EXISTS bi.cleaner_as;
CREATE TABLE bi.cleaner_as as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.cleaner_as_2;
CREATE TABLE bi.cleaner_as_2 as 
SELECT
	*,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a,
	EXTRACT(DOW FROM date::date) as weekday
FROM
	bi.cleaner_as,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and status__c in ('waiting','approved');




INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Workload' as kpi,
	delivery_areas__c as city,
	worked_hours as value FROM
(SELECT
	week,
	delivery_areas__c,
	min(t2.date) as date,
	Worked_Hours,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	min_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Utilization I in %' as kpi,
	delivery_areas__c as city,
	CASE WHEN min_hours > 0 THEN (worked_hours_2/min_hours)*100 ELSE 0 END as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	worked_hours_2,
	delivery_areas__c,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	worked_hours_2,
	min_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Cleaner Hours wo Job' as kpi,
	delivery_areas__c as city,
	CASE WHEN min_hours > 0 THEN (cleaner_0_hours/min_hours)*100 ELSE 0 END as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	worked_hours_2,
	delivery_areas__c,
	Min_Hours,
	cleaner_0_hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	worked_hours_2,
	min_hours,
	 cleaner_0_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Cleaner Hours wo Job' as kpi,
	delivery_areas__c as city,
	CASE WHEN worked_hours_2 is null THEN min_hours ELSE 0 END as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	worked_hours_2,
	delivery_areas__c,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	worked_hours_2,
	min_hours) as a;


INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Unutalized Hours' as kpi,
	delivery_areas__c as city,
	min_hours-worked_hours as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	worked_hours_2,
	delivery_areas__c,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	worked_hours_2,
	min_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Utilization II in %' as kpi,
	delivery_areas__c as city,
	CASE WHEN min_hours > 0 THEN (worked_hours/min_hours)*100 ELSE 0 END as value 
FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	delivery_areas__c,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	delivery_areas__c,
	worked_hours,
	min_hours) as a;



INSERT INTO bi.muffin_kpis		
SELECT
	hr_contract_start__c::Date as date,
	'Onboardings' as kpi,
	delivery_areas__c as city,
	COUNT(1) as Onboardings
FROM
	Salesforce.Account
WHERE
	type__c = '60'	
GROUP BY
	date,
	city,
	kpi;

INSERT INTO bi.muffin_kpis	
SELECT
	hr_contract_start__c::Date as date,
	'Onboardings in hrs' as kpi,
	delivery_areas__c as city,
	SUM(hr_contract_weekly_hours_min__c) as value
FROM
	Salesforce.Account
WHERE
	type__c = '60'	
GROUP BY
	date,
	kpi,
	city;
	
INSERT INTO bi.muffin_kpis	
SELECt
	hr_contract_end__c::Date as date,
	'Offboardings' as kpi,
	delivery_areas__c as city,
	COUNT(1)
FROM	
	Salesforce.Account
WHERE
	type__c = '60'	
	and hr_contract_end__c::Date < current_date::date
GROUP BY
	date,
	kpi,
	city;

INSERT INTO bi.muffin_kpis	
SELECt
	hr_contract_end__c::Date as date,
	'Offboardings in hrs' as kpi,
	delivery_areas__c as city,
	SUM(hr_contract_weekly_hours_min__c) as value
FROM	
	Salesforce.Account
WHERE
	type__c = '60'	
	and hr_contract_end__c::Date < current_date::date
GROUP BY
	date,
	kpi,
	city;


DROP TABLE IF EXISTS bi.utalization_temp12;
CREATE TABLE bi.utalization_temp12 as 		
SELECT
	t1.*,
	t2.date as left_date
FROM
	salesforce.account t1
LEFT JOIN
	bi.left_date t2
ON
	(t1.sfid = t2.sfid);
	
INSERT INTO bi.muffin_kpis	
SELECT
	date,
	'MC on Platform' as kpi,
	city as city,
	round(mop3,0) FROM(
SELECT
	EXTRACT(WEEK FROM date) WEEK,
	min(a.date) as date,
	city,
	AVG(MUffin_on_platform) as mop3
FROM(
SELECT
	t1.date,
	t2.delivery_areas__c AS CITY,
	SUM(CASE WHEN t1.Date between hr_contract_start__c::Date and hr_contract_end__c::date and (t1.date < t2.left_date or t2.left_date is null or status__C in ('BETA','ACTIVE')) THEN 1 ELSE 0 END)/4 as Muffin_on_Platform
FROM
	bi.utalization_temp12 t2,
	bi.orderdate t1
WHERE
	type__c = '60'
	and t1.date::date > '2015-11-01'
GROUP BY
	t1.Date,
	city) as a
GROUP BY
	week,
	city) as muffinkpi;

INSERT INTO bi.muffin_kpis	
SELECT
	a.mindate::date as date,
	'Churn %' as kpi,
	city,
	value*100
FROM	(	
SELECT
	EXTRACT(WEEK FROM date::date) as week,
	city,
	MIN(DATE) as mindate,
	'Churn %' as kpi,
	CASE WHEN MAX(CASE WHEN kpi = 'MC on Platform' THEN value ELSE 0 END) = 0 THEN 0 ELSE SUM(CASE WHEN kpi = 'Offboardings' THEN VALUE ELSE 0 END)/MAX(CASE WHEN kpi = 'MC on Platform' THEN value ELSE 0 END) END as value
FROM
	bi.muffin_kpis
WHERE
	date::date > '2015-11-01'
GROUP BY
	week,
	city) as a
GROUP BY
	week,
	date,
	city,
	value;

-- Holiays &Sickness

DROP TABLE IF EXISTS bi.cleaner_as;
CREATE TABLE bi.cleaner_as as 
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	Salesforce.Hr__c
WHERE
	type__c in ('sickness','holidays');


DROP TABLE IF EXISTS bi.cleaner_as_2;
CREATE TABLE bi.cleaner_as_2 as 
SELECT
	*,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a,
	EXTRACT(DOW FROM date::date) as weekday
FROM
	bi.cleaner_as,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and status__c in ('waiting','approved');

DROP TABLE IF EXISTS bi.cleaner_as_4;
CREATE TABLE bi.cleaner_as_4 as 
SELECT
	EXTRACT(DOW FROM date::date) as weekdate,
	date,
	t2.delivery_areas__c as city,
	t2.sfid,
	t1.type__c,
	CASE WHEN days__c = '0.5' THEN 0.5 ELSE a END as days,
	hr_contract_weekly_hours_min__c/5 as hours_per_day
FROM
	bi.cleaner_as_2 t1
LEFT JOIn
	Salesforce.Account t2
ON
	(t1.account__c = t2.sfid) 
WHERE
	a = '1'
	and t1.status__c in ('waiting','approved')
	and weekday not in ('6','0')
	and date not in ('2016-01-01')
GROUP BY
	date,
	t2.sfid,
	days,
	t1.type__c,
	city,
	a,
	hr_contract_weekly_hours_min__c;

INSERT INTO bi.muffin_kpis
SELECT
	a.date,
	'Sickness in hrs' as kpi,
	city,
	a.value
FROM
(SELECT
	EXTRACT(WEEK FROM DATE) as WEEK,
	min(date) as date,
	type__c,
	city,
	SUM(Hours_per_day*days) as value
FROM
	bi.cleaner_as_4
WHERE
	type__c = 'sickness'
	and left(city,2) = 'DE'
GROUP BY
	week,
	type__c,
	city) as a;
	
INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Holidays in hrs' as kpi,
	city,
	a.value
FROM
(SELECT
	EXTRACT(WEEK FROM DATE) as WEEK,
	min(date) as date,
	type__c,
	city,
	SUM(Hours_per_day*days) as value
FROM
	bi.cleaner_as_4
WHERE
	type__c = 'holidays'
	and left(city,2) = 'DE'
GROUP BY
	week,
	type__c,
	city) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Holidays in hrs' as kpi,
	'DE-Total Muffin' as city,
	a.value
FROM
(SELECT
	EXTRACT(WEEK FROM DATE) as WEEK,
	min(date) as date,
	type__c,
	SUM(Hours_per_day*days) as value
FROM
	bi.cleaner_as_4
WHERE
	type__c = 'holidays'
	and left(city,2) = 'DE'
GROUP BY
	week,
	type__c) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Sickness in hrs' as kpi,
	'DE-Total Muffin' as city,
	a.value
FROM
(SELECT
	EXTRACT(WEEK FROM DATE) as WEEK,
	min(date) as date,
	type__c,
	SUM(Hours_per_day*days) as value
FROM
	bi.cleaner_as_4
WHERE
	type__c = 'sickness'
GROUP BY
	week,
	type__c) as a;
	
DROP TABLE IF EXISTS bi.cleaner_as_4;
DROP TABLE IF EXISTS bi.cleaner_as_2;

INSERT INTO bi.muffin_kpis
SELECT
	a.date,
	'Sickness in %' as kpi,
	city,
	value*100
FROM
(SELECT
	EXTRACT(WEEK FROM DATE) as week,
	min(date) as date,
	city,
	CASE WHEN SUM(CASE WHEN kpi = 'Muffin Capacity' THEN value ELSE 0 END) >0 THEN  SUM(CASE WHEN kpi = 'Sickness in hrs' THEN value ELSE 0 END)/SUM(CASE WHEN kpi = 'Muffin Capacity' THEN value ELSE 0 END) ELSE 0 END as value
FROM
	bi.muffin_kpis
WHERE
	kpi in ('Sickness in hrs','Muffin Capacity')
	and left(city,2) = 'DE'
GROUP BY
	week,
	city) as a
GROUP BY
	date,
	kpi,
	city,
	value;

INSERT INTO bi.muffin_kpis
SELECT
	a.date,
	'Holidays in %' as kpi,
	city,
	value*100
FROM
(SELECT
	EXTRACT(WEEK FROM DATE) as week,
	min(date) as date,
	city,
	CASE WHEN SUM(CASE WHEN kpi = 'Muffin Capacity' THEN value ELSE 0 END) >0 THEN  SUM(CASE WHEN kpi = 'Holidays in hrs' THEN value ELSE 0 END)/SUM(CASE WHEN kpi = 'Muffin Capacity' THEN value ELSE 0 END) ELSE 0 END as value
FROM
	bi.muffin_kpis
WHERE
	kpi in ('Holidays in hrs','Muffin Capacity')
	and left(city,2) = 'DE'
GROUP BY
	week,
	city) as a
GROUP BY
	date,
	kpi,
	city,
	value;
	
-- Muffin Availability



-- Week1							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Cologne','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Munich','3');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Frankfurt am Main','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Hamburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Stuttgart','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Dusseldorf','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Essen','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Nuremberg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Bonn','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Duisburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Mannheim','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Mainz','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Wuppertal','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Mönchengladbach','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-04','Onboardings Target','DE-Berlin','0');							
							
-- Week2							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Cologne','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Munich','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Frankfurt am Main','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Hamburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Stuttgart','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Dusseldorf','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Essen','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Nuremberg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Bonn','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Duisburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Mannheim','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Mainz','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Wuppertal','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Mönchengladbach','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-11','Onboardings Target','DE-Berlin','0');							
							
-- WEEK3							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Cologne','3');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Munich','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Frankfurt am Main','5');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Hamburg','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Stuttgart','4');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Dusseldorf','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Essen','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Nuremberg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Bonn','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Duisburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Mannheim','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Mainz','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Wuppertal','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Mönchengladbach','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-01-25','Onboardings Target','DE-Berlin','0');							
							
-- WEEK4							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Cologne','5');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Munich','3');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Frankfurt am Main','7');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Hamburg','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Stuttgart','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Dusseldorf','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Essen','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Nuremberg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Bonn','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Duisburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Mannheim','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Mainz','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Wuppertal','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Mönchengladbach','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-01','Onboardings Target','DE-Berlin','0');							
							
-- WEEK5							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Cologne','6');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Munich','4');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Frankfurt am Main','4');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Hamburg','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Stuttgart','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Dusseldorf','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Essen','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Nuremberg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Bonn','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Duisburg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Mannheim','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Mainz','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Wuppertal','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Mönchengladbach','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-08','Onboardings Target','DE-Berlin','0');							
						
-- WEEK6							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Cologne','10');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Munich','4');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Frankfurt am Main','4');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Hamburg','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Stuttgart','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Dusseldorf','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Essen','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Nuremberg','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Bonn','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Duisburg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Mannheim','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Mainz','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Wuppertal','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Mönchengladbach','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-15','Onboardings Target','DE-Berlin','0');							
							
-- WEEK7							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Cologne','13');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Munich','5');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Frankfurt am Main','5');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Hamburg','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Stuttgart','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Dusseldorf','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Essen','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Nuremberg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Bonn','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Duisburg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Mannheim','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Mainz','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Wuppertal','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Mönchengladbach','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Dortmund','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-22','Onboardings Target','DE-Berlin','0');							
							
-- WEEK8							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Cologne','3');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Munich','5');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Frankfurt am Main','6');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Hamburg','3');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Stuttgart','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Dusseldorf','2');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Essen','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Nuremberg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Bonn','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Duisburg','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Mannheim','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Mainz','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Wuppertal','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Mönchengladbach','1');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Dortmund','0');							
INSERT INTO bi.muffin_kpis VALUES('2016-02-29','Onboardings Target','DE-Berlin','0');							


-- DE-Total

DROP TABLE IF EXISTS bi.utalization_per_cleaner;
CREATE TABLE bi.utalization_per_cleaner as 
SELECT
	EXTRACT(WEEK FROM date) as week,
	min(Date) as min_Date,
	end_date,
	sfid,
	left_date,
	onboarding_date,
	SUM(worked_hours) as Worked_Hours,
	MAX(MAX_hours) as Min_Hours,
	CASE WHEN SUM(worked_hours) > MAX(MAX_hours) THEN MAX(MAX_hours) ELSE SUM(worked_hours) END as worked_hours_2
FROM
	bi.utalization_temp3 t1
WHERE
	LEFT(t1.delivery_areas__c,2) = 'DE'
GROUP BY
	Week,
	end_date,
	onboarding_date,
	left_date,
	sfid;

DROP TABLE IF EXISTS bi.utalization_per_cleaner2;
CREATE TABLE bi.utalization_per_cleaner2 as 
SELECT
	week,
	SUM(Worked_Hours) as Worked_Hours,
	SUM(worked_hours_2) as Worked_Hours_2,
	SUM(CASE WHEN onboarding_date <= min_date and end_date > min_date and (left_date > min_date or left_date is null) THEN Min_Hours ELSE 0 END) as Min_Hours
FROM
	bi.utalization_per_cleaner t1
GROUP BY
	week;



INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Capacity' as kpi,
	'DE-Total' as city,
	min_hours as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours as worked_hours,
	Min_Hours as min_hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	worked_hours,
	min_hours) as a;


INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Workload' as kpi,
	'DE-Total' as city,
	worked_hours as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	worked_hours,
	min_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Utilization I in %' as kpi,
	'DE-Total' as city,
	(worked_hours_2/min_hours)*100 as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	worked_hours_2,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	worked_hours,
	worked_hours_2,
	min_hours) as a;

	
INSERT INTO bi.muffin_kpis	
SELECT
	date,
	'MC on Platform' as kpi,
	'DE-Total' as city,
	round(mop3,0) FROM(
SELECT
	EXTRACT(WEEK FROM date) WEEK,
	min(a.date) as date,
	AVG(MUffin_on_platform) as mop3
FROM(
SELECT
	t1.date,
	SUM(CASE WHEN t1.Date between hr_contract_start__c::Date and hr_contract_end__c::date and (t1.date < t2.left_date or t2.left_date is null or status__C in ('BETA','ACTIVE')) THEN 1 ELSE 0 END)/4 as Muffin_on_Platform
FROM
	bi.utalization_temp12 t2,
	bi.orderdate t1
WHERE
	type__c = '60'
	and t1.date::date > '2015-11-01'
	and LEFT(delivery_areas__c,2) = 'DE'
GROUP BY
	t1.Date) as a
GROUP BY
	week) as muffinkpi;

INSERT INTO bi.muffin_kpis	
SELECT
	a.mindate::date as date,
	'Churn %' as kpi,
	'DE-Total' as city,
	value*100
FROM	(	
SELECT
	EXTRACT(WEEK FROM date::date) as week,
	MIN(DATE) as mindate,
	'Churn %' as kpi,
	CASE WHEN MAX(CASE WHEN kpi = 'MC on Platform' THEN value ELSE 0 END) = 0 THEN 0 ELSE SUM(CASE WHEN kpi = 'Offboardings' THEN VALUE ELSE 0 END)/MAX(CASE WHEN kpi = 'MC on Platform' THEN value ELSE 0 END) END as value
FROM
	bi.muffin_kpis
WHERE
	date >= '2015-11-01'
GROUP BY
	week) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	hr_contract_start__c::Date as date,
	'Onboardings' as kpi,
	'DE-Total' as city,
	COUNT(1) as Onboardings
FROM
	Salesforce.Account t1
WHERE
	type__c = '60'	
    and LEFT(t1.delivery_areas__c,2) = 'DE'
GROUP BY
	date,
	kpi;
	
INSERT INTO bi.muffin_kpis	
SELECT
	date::date as date,
	'Onboardings Target' as kpi,
	'DE-Total' as city,
	SUM(value) as value
FROM
	bi.muffin_kpis 
WHERE
	KPI = 'Onboardings Target'
	and LEFT(city,2) = 'DE'
GROUP BY
	date;
	
INSERT INTO bi.muffin_kpis	
SELECt
	hr_contract_end__c::Date as date,
	'Offboardings' as kpi,
	'DE-Total' as city,
	COUNT(1)
FROM	
	Salesforce.Account
WHERE
	type__c = '60'	
	and hr_contract_end__c::Date < current_date::date
	and LEFT(delivery_areas__c,2) = 'DE'
GROUP BY
	date,
	kpi;

-- DE-Total Muffin



DROP TABLE IF EXISTS bi.utalization_per_cleaner;
CREATE TABLE bi.utalization_per_cleaner as 
SELECT
	EXTRACT(WEEK FROM date) as week,
	min(Date) as min_Date,
	end_date,
	sfid,
	left_date,
	onboarding_date,
	SUM(worked_hours) as Worked_Hours,
	MAX(MAX_hours) as Min_Hours,
	CASE WHEN SUM(worked_hours) > MAX(MAX_hours) THEN MAX(MAX_hours) ELSE SUM(worked_hours) END as worked_hours_2
FROM
	bi.utalization_temp3 t1
WHERE
	LEFT(t1.delivery_areas__c,2) = 'DE'
	and delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	Week,
	end_date,
	onboarding_date,
	left_date,
	sfid;

DROP TABLE IF EXISTS bi.utalization_per_cleaner2;
CREATE TABLE bi.utalization_per_cleaner2 as 
SELECT
	week,
	SUM(Worked_Hours) as Worked_Hours,
	SUM(worked_hours_2) as Worked_Hours_2,
	SUM(CASE WHEN onboarding_date <= min_date and end_date > min_date and (left_date > min_date or left_date is null) THEN Min_Hours ELSE 0 END) as Min_Hours
FROM
	bi.utalization_per_cleaner t1
GROUP BY
	week;



INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Capacity' as kpi,
	'DE-Total Muffin' as city,
	min_hours as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours as worked_hours,
	Min_Hours as min_hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	worked_hours,
	min_hours) as a;


INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Muffin Workload' as kpi,
	'DE-Total Muffin' as city,
	worked_hours as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	worked_hours,
	min_hours) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	a.date,
	'Utilization I in %' as kpi,
	'DE-Total Muffin' as city,
	(worked_hours_2/min_hours)*100 as value FROM
(SELECT
	week,
	min(t2.date) as date,
	Worked_Hours,
	worked_hours_2,
	Min_Hours
FROM
	bi.utalization_per_cleaner2 t1
LEFT JOIN
	bi.orderdate t2
ON
	(t1.week = EXTRACT(week FROM t2.date::date) and t2.date >= '2015-08-01'::date)
GROUP BY
	week,
	worked_hours,
	worked_hours_2,
	min_hours) as a;

	
INSERT INTO bi.muffin_kpis	
SELECT
	date,
	'MC on Platform' as kpi,
	'DE-Total Muffin' as city,
	round(mop3,0) FROM(
SELECT
	EXTRACT(WEEK FROM date) WEEK,
	min(a.date) as date,
	AVG(MUffin_on_platform) as mop3
FROM(
SELECT
	t1.date,
	SUM(CASE WHEN t1.Date between hr_contract_start__c::Date and hr_contract_end__c::date and (t1.date < t2.left_date or t2.left_date is null or status__C in ('BETA','ACTIVE')) THEN 1 ELSE 0 END)/4 as Muffin_on_Platform
FROM
	bi.utalization_temp12 t2,
	bi.orderdate t1
WHERE
	type__c = '60'
	and t1.date::date > '2015-11-01'
	and delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	t1.Date) as a
GROUP BY
	week) as muffinkpi;

INSERT INTO bi.muffin_kpis	
SELECT
	a.mindate::date as date,
	'Churn %' as kpi,
	'DE-Total Muffin' as city,
	value*100
FROM	(	
SELECT
	EXTRACT(WEEK FROM date::date) as week,
	MIN(DATE) as mindate,
	'Churn %' as kpi,
	CASE WHEN MAX(CASE WHEN kpi = 'MC on Platform' THEN value ELSE 0 END) = 0 THEN 0 ELSE SUM(CASE WHEN kpi = 'Offboardings' THEN VALUE ELSE 0 END)/MAX(CASE WHEN kpi = 'MC on Platform' THEN value ELSE 0 END) END as value
FROM
	bi.muffin_kpis
WHERE
	date >= '2015-11-01'
	and city != 'DE-Total'
GROUP BY
	week) as a;

INSERT INTO bi.muffin_kpis	
SELECT
	hr_contract_start__c::Date as date,
	'Onboardings' as kpi,
	'DE-Total Muffin' as city,
	COUNT(1) as Onboardings
FROM
	Salesforce.Account t1
WHERE
	type__c = '60'	
    and LEFT(t1.delivery_areas__c,2) = 'DE'
    and delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	date,
	kpi;
	
INSERT INTO bi.muffin_kpis	
SELECT
	date::date as date,
	'Onboardings Target' as kpi,
	'DE-Total Muffin' as city,
	SUM(value) as value
FROM
	bi.muffin_kpis 
WHERE
	KPI = 'Onboardings Target'
	and LEFT(city,2) = 'DE'
	and City != 'DE-Total'
GROUP BY
	date;
	
INSERT INTO bi.muffin_kpis	
SELECt
	hr_contract_end__c::Date as date,
	'Offboardings' as kpi,
	'DE-Total Muffin' as city,
	COUNT(1)
FROM	
	Salesforce.Account
WHERE
	type__c = '60'	
	and hr_contract_end__c::Date < current_date::date
	and LEFT(delivery_areas__c,2) = 'DE'
	and delivery_areas__c in ('DE-Berlin','DE-Bonn','DE-Cologne','DE-Dortmund','DE-Duisburg','DE-Dusseldorf','DE-Essen','DE-Frankfurt am Main','DE-Hamburg','DE-Mainz','DE-Mannheim','DE-Mönchengladbach','DE-Munich','DE-Nuremberg','DE-Stuttgart','DE-Wuppertal')
GROUP BY
	date,
	kpi;

DROP TABLE IF EXISTS bi.utalization_per_cleaner;
DROP TABLE IF EXISTS bi.utalization_per_cleaner2;
DROP TABLE IF EXISTS bi.utalization_temp1;
DROP TABLE IF EXISTS bi.utalization_temp2;
DROP TABLE IF EXISTS bi.utalization_temp3;

	
INSERT INTO bi.muffin_kpis		
SELECT
	c.date,
	'Muffin CAC' as kpi,
	'DE-Total' as city,
	CASE WHEN kpi is null THEN 0 ELSE kpi end as kpi
FROM
(SELECT
	EXTRACT(week from a.date) as week,
	min(a.date) as date,
	round(SUM(value)/sum(onboardings),0) as kpi
FROM
(SELECT
	Date,
	'Marketing Costs' as kpi,
	'DE-Total' as City,
	SUM(costs) as value
FROM
	bi.tiger_operations_costs
WHERE
	type = 'MUFFIN'
	and date < current_Date
GROUP BY
	Date) as a
LEFT JOIN
	(SELECT
	hr_contract_start__c::Date as date,
	'Onboardings' as kpi,
	COUNT(1) as Onboardings
FROM
	Salesforce.Account t1
WHERE
	type__c = '60'	
	and LEFT(t1.delivery_areas__c,2) = 'DE'
GROUP BY
	date,
	kpi) as b
ON
	(a.date = b.date)
GROUP BY
	week) as c;

-- Muffin Availability

DROP TABLE IF EXISTS bi.availability_cleaner;
CREATE TABLE bi.availability_cleaner as 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-02' as date) as date,
	CAST(split_part(availability_monday__c,'-',1) as time) as start_availability,
	CAST(split_part(availability_monday__c,'-',2) as time) as end_availability
FROM
	salesforce.account
WHERE
	(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
	and Status__c in ('ACTIVE','BETA');

INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-03' as date) as day,
	CAST(split_part(availability_tuesday__c,'-',1) as time) as start_availability,
	CAST(split_part(availability_tuesday__c,'-',2) as time) as end_availability
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-04' as date) as day,
	CAST(split_part(availability_wednesday__c,'-',1) as time) as start_availability_wed,
	CAST(split_part(availability_wednesday__c,'-',2) as time) as end_availability_wed
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
		status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-05' as date) as day,
	CAST(split_part(availability_thursday__c,'-',1) as time) as start_availability_thu,
	CAST(split_part(availability_thursday__c,'-',2) as time)as end_availability_thu
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
		status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-06' as date) as day,
	CAST(split_part(availability_friday__c,'-',1) as time) as start_availability_fr,
	CAST(split_part(availability_friday__c,'-',2) as time) as end_availability_fr
FROM
	salesforce.account
WHERE
	(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
	and Status__c in ('ACTIVE','BETA');
	
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
		status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-07' as date) as day,
	CAST(split_part(availability_saturday__c,'-',1) as time) as start_availability_sa,
	CAST(split_part(availability_saturday__c,'-',2) as time) as end_availability_sa
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
	and Status__c in ('ACTIVE','BETA');
	
	
INSERT INTO bi.availability_cleaner 
SELECT
	sfid,
	delivery_areas__c as city,
	status__c,
	LEFT(BillingPostalCode,3) as zipcode,
	LEFT(locale__c,2) as locale,
	CAST('2015-11-08' as date) as day,
	CAST(split_part(availability_sunday__c,'-',1) as time) as start_availability_sun,
	CAST(split_part(availability_sunday__c,'-',2) as time) end_availability_sun
FROM
	salesforce.account
WHERE
		(type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%')
		and Status__c in ('ACTIVE','BETA');

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.availabilitymuffin_temp1;
CREATE TABLE bi.availabilitymuffin_temp1 as 
	SELECT
		*
	FROM
		bi.availability_cleaner,
		unnest(array[0,1,2,3,4,5,6]) as day,
		unnest(array[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) as hour;

DROP TABLE IF EXISTS bi.muffin_availability;
CREATE TABLE bi.muffin_availability as 
SELECT
	city,
	date,
	zipcode,
	locale,
	hour,
	status__c,
	SUM(CASE WHEN EXTRACT(DOW FROM date) = DAY AND HOUR >= EXTRACT(HOUR FROM start_availability) and hour < EXTRACT(HOUR FROM end_availability) THEN 1 ELSE 0 END) as Jobs
FROM
	bi.availabilitymuffin_temp1
GROUP BY
	city,
	zipcode,
	locale,
	date,
	hour,
	status__c;

DROP TABLE IF EXISTS bi.availabilitymuffin_temp1;

-- job calenadar Current


DROP TABLE IF EXISTS bi.muffin_cleaner;
CREATE TABLE bi.muffin_cleaner as 
SELECT
	sfid,
	name,
	createddate::date as onboarding_date,
	CASE WHEN hr_contract_weekly_hours_min__c is null THEN hr_contract_weekly_hours_max__c ELSE hr_contract_weekly_hours_min__c END  as Contract_Hours,
	Status__c as Status
FROM
	Salesforce.Account
WHERE
	type__c = '60';


DROP TABLE IF EXISTS bi.muffin_order_distribution;
CREATE TABLE bi.muffin_order_distribution as 
SELECT
	City,
	contact__c,
	status,
	LEFT(Locale__c,2) as locale,
	LEFT(ShippingPostalCode,3) as zipcode,
	Effectivedate::date as date,
	EXTRACT(DOW FROM Effectivedate) as weekday,
	EXTRACT(HOUR FROM Order_Start__c)+2 as Start_time,
	EXTRACT(HOUR FROM Order_End__c)+2 as End_time
FROM
	bi.orders
WHERE
	Effectivedate::date >= '2016-02-01'
	and test__c = '0'
	and order_type = '1';

DROP TABLE IF EXISTS bi.muffin_order_distribution_v2;
CREATE TABLE bi.muffin_order_distribution_v2 as 
SELECT
	*
FROM
	bi.muffin_order_distribution,
	unnest(array[0,1,2,3,4,5,6]) as day,
	unnest(array[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) as hour;

DROP TABLE IF EXISTS bi.muffin_order_distribution_v3;
CREATE TABLE bi.muffin_order_distribution_v3 as 	
SELECT
	city,
	Status,
	zipcode,
	locale,
	date::date order_date,
	EXTRACT(WEEK FROM date::date) as CW,
	EXTRACT(DOW FROM date::date) as Weekday,
	hour,
	SUM(CASE WHEN EXTRACT(DOW FROM date) = DAY AND HOUR >= bi.muffin_order_distribution_v2.Start_time and hour < bi.muffin_order_distribution_v2.End_time THEN 1 ELSE 0 END) as Orders
FROM
	bi.muffin_order_distribution_v2
GROUP BY
	CW,
	zipcode,
	Status,
	locale,
	weekday,
	order_date,
	city,
	hour;

DROP TABLE IF EXISTS bi.workcalendar;
DROP TABLE IF EXISTS bi.utalization_temp12;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


END;

$BODY$ LANGUAGE 'plpgsql'
	