DELIMITER //

CREATE OR REPLACE FUNCTION bi.daily$cpacalculation_city(crunchdate date)
RETURNS void AS
$BODY$

DECLARE 
function_name varchar := 'bi.daily$cpacalculation_city';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	DROP TABLE IF EXISTS bi.vouchercosts_city;

	CREATE TABLE bi.vouchercosts_city AS

		SELECT

			EXTRACT(year from o.order_creation__c) as yearnum,
			EXTRACT(month from o.order_creation__c) as monthnum,
			o.order_creation__c::timestamp::date as daynum,
			o.delivery_area as city,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Facebook' THEN 1 ELSE NULL END) as Fb_acquisitions,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'SEM' THEN 1 ELSE NULL END) as SEM_acquisitions,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Display' THEN 1 ELSE NULL END) as Display_acquisitions,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Voucher Campaigns' THEN 1 ELSE NULL END) as Vouchers_acquisitions,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Facebook' THEN o.discount__c ELSE NULL END) as Fb_discount,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'SEM' THEN o.discount__c ELSE NULL END) as SEM_discount,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Display' THEN o.discount__c ELSE NULL END) as Display_discount,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Brand Marketing Offline' THEN o.discount__c ELSE NULL END) as BrandMarketingOffline_discount,
			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.marketing_channel = 'Voucher Campaigns' AND o.status not like ('%CANCELLED%') THEN o.discount__c ELSE NULL END) as Vouchers_costs

		FROM
			bi.orders o

		WHERE o.test__c = '0'
			AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE','CANCELLED NOT THERE YET')
			AND o.order_creation__c < current_date --and o.order_creation__c >= (current_date - interval '3 months')
			and order_type = '1'
			
		GROUP BY yearnum, monthnum, daynum, o.delivery_area

		ORDER BY yearnum desc, monthnum desc, daynum desc, city asc

	;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$

  LANGUAGE plpgsql VOLATILE

  COST 100;

ALTER FUNCTION bi.daily$cpacalculation_city(date)

  OWNER TO u8em74mhj6524t;