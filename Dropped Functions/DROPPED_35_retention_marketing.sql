CREATE OR REPLACE FUNCTION bi.daily$retention_marketing (crunchdate date) RETURNS void AS 
$BODY$


DECLARE 
function_name varchar := 'bi.daily$retention_marketing';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;


BEGIN

	DROP TABLE IF EXISTS bi.retention_marketing;
	CREATE TABLE bi.retention_marketing AS
		
		SELECT

			t1.date::timestamp::date as date,
			t1.city::text as city,
			''::text as locale,
			''::text as channel,
			'p1'::text as period,
			'City analysis' as kpi_type,
			0::numeric as acquisitions,
			0::numeric as rebookings,
			0::numeric as returning_customer,
			sum(round(t1.cohort_return_rate::numeric,3)) as cohort_return_rate,
			sum(round(t1.p0_activitiy_rate::numeric,3)) as p0_activity_rate,
			sum(round(t1.p1_activity_rate::numeric,3)) as p1_activity_rate

		FROM bi.Running_Cohort_Analysis_City2 t1

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

	INSERT INTO bi.retention_marketing
		SELECT

			t2.date::timestamp::date as date,
			cast(t2.city as text) as city,
			'' as locale,
			''::text as channel,
			'p3'::text as period,
			'City analysis' as kpi_type,
			0::numeric as acquisitions,
			0::numeric as rebookings,
			0::numeric as returning_customer,
			sum(round(t2.cohort_return_rate::numeric,3)) as cohort_return_rate,
			0::numeric as p0_activity_rate,
			sum(round(t2.p1_activity_rate::numeric,3)) as p1_activity_rate

		FROM bi.Running_Cohort_Analysis_City2p3 t2

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

	INSERT INTO bi.retention_marketing
		SELECT

			t3.date::timestamp::date as date,
			''::text as city,
			t3.locale as locale,
			''::text as channel,
			'p1'::text as period,
			'Global' as kpi_type,
			sum(round(t3.acquisitions::numeric,2)) as acquisitions,
			sum(round(t3.rebookings::numeric,2)) as rebookings,
			0::numeric as returning_customer,
			sum(round(t3.cohort_return_rate::numeric,3)) as cohort_return_rate,
			sum(round(t3.p0_activitiy_rate::numeric,3)) as p0_activity_rate,
			sum(round(t3.p1_activitiy_rate::numeric,3)) as p1_activity_rate

		FROM bi.Running_Cohort_Analysis_Email t3

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

	INSERT INTO bi.retention_marketing
		SELECT

			t4.date::timestamp::date as date,
			''::text as city,
			t4.locale as locale,
			''::text as channel,
			'p3'::text as period,
			'Global' as kpi_type,
			sum(round(t4.acquisitions::numeric,2)) as acquisitions,
			0::numeric as rebookings,
			0::numeric as returning_customer,
			sum(round(t4.cohort_return_rate::numeric,3)) as cohort_return_rate,
			0::numeric as p0_activity_rate,
			sum(round(t4.p1_activitiy_rate::numeric,3)) as p1_activity_rate

		FROM bi.Running_Cohort_Analysis_Emailp3 t4

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

	INSERT INTO bi.retention_marketing
		SELECT

			t5.date::timestamp::date as date,
			''::text as city,
			t5.locale as locale,
			t5.acquisition_marketing_channel::text as channel,
			'p1'::text as period,
			'Channel analysis' as kpi_type,
			sum(round(t5.acqusitions::numeric,2)) as acquisitions,
			0::numeric as rebookings,
			0::numeric as returning_customer,
			sum(round(t5.cohort_return_rate::numeric,3)) as cohort_return_rate,
			sum(round(t5.p0_activitiy_rate::numeric,3)) as p0_activity_rate,
			sum(round(t5.p1_activity_rate::numeric,3)) as p1_activity_rate

		FROM bi.Running_Cohort_Analysis_Marketing2 t5

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

	INSERT INTO bi.retention_marketing
		SELECT

			t6.date::timestamp::date as date,
			''::text as city,
			t6.locale as locale,
			t6.acquisition_marketing_channel::text as channel,
			'p3'::text as period,
			'Channel analysis' as kpi_type,
			sum(round(t6.acquisitions::numeric,2)) as acquisitions,
			0::numeric as rebookings,
			0::numeric as returning_customer,
			sum(round(t6.cohort_return_rate::numeric,3)) as cohort_return_rate,
			0::numeric as p0_activity_rate,
			sum(round(t6.p1_activity_rate::numeric,3)) as p1_activity_rate

		FROM bi.Running_Cohort_Analysis_Marketing2p3 t6

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

	INSERT INTO bi.retention_marketing
		SELECT

			t7.date::timestamp::date as date,
			''::text as city,
			t7.locale as locale,
			''::text as channel,
			'p6'::text as period,
			'Global' as kpi_type,
			0::numeric as acquisitions,
			0::numeric as rebookings,
			sum(returning_customer::numeric) as returning_customer,
			sum(round(t7._return_rate::numeric,3)) as cohort_return_rate,
			sum(round(t7.activitiy_rate::numeric,3)) as p0_activity_rate,
			0::numeric as p1_activity_rate

		FROM bi.retention_kpi t7

		GROUP BY date, city, locale, channel, period, kpi_type

		ORDER BY date desc, city asc, locale asc, channel asc, period asc, kpi_type asc

	;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'