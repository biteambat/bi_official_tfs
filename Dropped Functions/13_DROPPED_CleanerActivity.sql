DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$cleaneractivity(crunchdate date) RETURNS void AS
$BODY$
DECLARE 
function_name varchar := 'bi.daily$cleaneractivity';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


DELETE FROM bi.CleanerActivityL30 t1 WHERE CAST(t1.Date::TEXT as DATE) = CAST((cast(crunchdate as date) - interval '2 days') as DATE);

INSERT INTO bi.CleanerActivityL30
SELECT
  cast((cast(crunchdate as date) - interval '2 days') as DATE) as date,
  Count(Distinct(Professional__c)) as distinct_cleaner,
  round(CAST(SUM(Order_Duration__c) as decimal)/Count(Distinct(Professional__c)),1) as Hours_per_Cleaner,
  LEFT(Locale__c,2) as locale
FROM
  Salesforce.Order
WHERE
  Status = 'INVOICED'
  and CAST(Effectivedate as date) between cast((cast(crunchdate as date) - interval '2 days') as date) - interval '30 days' and cast((cast(crunchdate as date) - interval '2 days') as date)
GROUP BY
	locale;
	
	
DROP TABLE IF EXISTS bi.CleanerActivityLast30Days;
CREATE TABLE bi.CleanerActivityLast30Days as 
SELECT
  CAST(t1.date as Date) as OrderDate,
  locale,
  distinct_cleaner,
  Hours_per_Cleaner
FROM
  bi.CleanerActivityL30 t1;
 
DROP TABLE IF EXISTS bi.cleanerchange;
CREATE TABLE bi.cleanerchange as
SELECT
	t1.sfid,
	CASE WHEN t1.in_test_period__c = TRUE THEN 1 ELSE 0 END as on_babysitting,
	t1.createddate as onboardingdate,
	t1.delivery_areas__c,
	LEFT(t1.locale__c,2) as locale,
   MAX(CASE WHEN CAST(Effectivedate as date) between cast((cast(crunchdate as date) - interval '2 days') as date) - interval '30 days' and cast((cast(crunchdate as date) - interval '2 days') as date) THEN 1 ELSE 0 END) as cleanersnow,
   MAX(CASE WHEN CAST(Effectivedate as date) between cast((cast(crunchdate as date) - interval '3 days') as date) - interval '30 days' and cast((cast(crunchdate as date) - interval '3 days') as date) THEN 1 ELSE 0 END) as cleanersbefore
FROM
	salesforce.account t1
LEFT JOIN
	salesforce.order t2
ON
	(t1.sfid = t2.professional__c and t2.status = 'INVOICED')
GROUP BY
	t1.sfid,
	t1.createddate,
	t1.delivery_areas__c,
	LEFT(t1.Locale__c,2),
	on_babysitting;

DELETE FROM bi.cleaneractivity_change WHERE date = cast((cast(crunchdate as date) - interval '2 days') as date);

INSERT INTO bi.cleaneractivity_change 	
SELECT
	cast((cast(crunchdate as date) - interval '2 days') as date) as date,
	CAST(delivery_areas__c as varchar) as city,
	CAST(SUM(CASE WHEN cleanersnow = 0 and cleanersbefore = 1 THEN 1 ELSE 0 END) as INT) as Cleaners_Inactive,
	CAST(SUM(CASE WHEN cleanersnow = 1 and cleanersbefore = 0 THEN 1 ELSE 0 END) as INT) as Cleaners_Active,
	locale,
	on_babysitting
FROM
	bi.cleanerchange
WHERE
	cleanersnow = 1 or cleanersbefore = 1
GROUP BY
	delivery_areas__c,
	locale,
	on_babysitting;

DELETE FROM bi.cleaneractivity_change WHERE Cleaners_Inactive = 0 and Cleaners_Active = 0;
 
DROP TABLE IF EXISTS bi.cleaneractivated14d_temp;
CREATE TABLE bi.cleaneractivated14d_temp as 
SELECT
	(crunchdate::date - Interval '2 Days') as date,
	t1.sfid,
	t1.delivery_areas__c,
	t1.createddate,
	MAX(CASE WHEN Effectivedate between t1.Createddate::date and t1.Createddate::date + Interval '14 days' THEN 1 ELSE 0 END) as Cleaner_Activated
FROM
	salesforce.account t1
LEFT JOIn
	salesforce.order t2
ON
	(t1.sfid = t2.professional__c and status = 'INVOICED')
WHERE
	t1.Createddate between (crunchdate::date - Interval ' 30 days') and (crunchdate::date - Interval '16 Days')
GROUP BY
	t1.sfid,
	t1.delivery_areas__c,
	t1.createddate;

DELETE FROM bi.cleaneractivated14d WHERE DATE = 	(crunchdate::date - Interval '2 Days');

INSERT INTO bi.cleaneractivated14d
SELECT
	Date,
	delivery_areas__c as city,
	CAST(SUM(CASE WHEN cleaner_activated = 1 THEN 1 ELSE 0 END) as decimal)/COUNT(1) as Cleaner_job_14,
	COUNT(1) as All_Cleaner
FROM
	 bi.cleaneractivated14d_temp
GROUP BY
	Date,
	delivery_areas__c;
 
DROP TABLE IF EXISTS  bi.cleaneractivated14d_temp;

DELETE FROM bi.activitybycity WHERE CAST(crunchdate as DATE) - interval '2 days' = date;

INSERT INTO bi.activitybycity
SELECT
  CAST(crunchdate as DATE) - interval '2 days' as date,
  t2.delivery_areas__c,
  round(CAST(COUNT(DISTINCT(t1.Professional__c)) as decimal)/COUNT(DISTINCT(t2.sfid)),4)*100 as Activity_Rate,
  COUNT(DISTINCT(t1.Professional__c)) as Active_Cleaner
FROM
	salesforce.account t2
LEFT JOIN
	salesforce.order t1
ON
	(t1.Professional__c = t2.sfid 	and t1.status in ('INVOICED') 	and (CAST(Effectivedate as DATE) between  (CAST(crunchdate as DATE) - interval '32 days') and CAST(crunchdate as DATE) - interval '2 days') )
WHERE
	Status__c not in ('LEFT','TERMINATED')
GROUP BY
	t2.delivery_areas__c;
	
 DROP TABLE IF EXISTS reports.active_cleaners_per_day;
CREATE TABLE reports.active_cleaners_per_day as 
SELECT
	professional__c,
	type__c as type,
	effectivedate,
	LEFT(t1.Locale__c,2) as locale,
	t2.delivery_areas__c as City,
	COUNT(DISTINCT(1)) as Orders
FROM
	salesforce.order t1
JOIN
	salesforce.account t2
ON
	(t1.professional__c = t2.sfid)
WHERE
	t1.Status = 'INVOICED'
	and t1.test__c = '0'
GROUP BY
	professional__c,
	type__c,
	Effectivedate,
	locale,
	City;
 
 end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

 
END;

$BODY$ LANGUAGE 'plpgsql'
