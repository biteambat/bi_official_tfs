DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$FUNCTION_NAME(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$FUNCTION_NAME';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

/*	performing Queries
DROP TABLE IF EXISTS reports.TABLE_NAME;
CREATE TABLE reports.TABLE_NAME as 
--- select etc
*/

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;

$BODY$ LANGUAGE 'plpgsql'