CREATE OR REPLACE FUNCTION schema.sfunc_functionname(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'schema.sfunc_functionname';
start_time timestamp := clock_timestamp() + interval '2 hours';

end_time timestamp;
duration interval;

BEGIN
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------






---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'