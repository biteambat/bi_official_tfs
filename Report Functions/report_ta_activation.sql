﻿CREATE OR REPLACE FUNCTION bi.daily$report_ta_activation(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$report_ta_activation';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DELETE FROM reports.TA_Activation
where date = crunchdate-1;

insert into reports.TA_Activation

select crunchdate-1 as date, x.*
from (
-- #Beta on Babysitting
select 'Beta on Babysitting' as KPI, count(*) as "Count"
from main.account
where status = 'BETA'
and on_babysitting = true
group by status

union

-- #Missing Documents -> backgroundcheck oder tradelicense leer
select 'Missing Documents' as KPI, count(*) as "Count"
from main.account
where status = 'BETA'
and on_babysitting = true
and (legal_backgroundcheck is null or  legal_tradelicense is null)

union

-- #without 4 Jobs	->		nur invoiced
select 'cleaner without 4 jobs' as KPI, count(*) as "Count" from (
	select cleaner_id, count(*)
	from main.order
	where cleaner_id in (
								select cleaner_id
								from main.account
								where status = 'BETA'
								and on_babysitting = true)
	and order_status = 'INVOICED'
	group by cleaner_id
	having count(*) < 4	
) x

union

-- #without Feedback with Jobs -> siehe order tabelle
select 'Jobs without Feedback' as KPI, count(*) as "Count" from (
	select cleaner_id
	from main.order
	where cleaner_id in (
								select cleaner_id
								from main.account
								where status = 'BETA'
								and on_babysitting = true)
	and order_status = 'INVOICED'
 	AND rating_professional is null
	group by cleaner_id
) x

union


-- ######################## "Results" ###############################
-- #conversion
SELECT 'Conversion' as KPI, count(*) as "Count"
FROM main.account
	where name <> '#'
	and is_test_account is false
	and status is not null
	and status <> 'SUBCONTRACTOR'
	and cast(created_at as date) = crunchdate-1

union

-- #Tiger Activated / #Left / #Terminated /#Low Available (out of process) / #Suspended / #Limbo
select "NewValue" as KPI, count(*) as "Count"
from bi."AccountHistory"
where "Field" = 'Status__c'
and "NewValue" in ('LEFT', 'TERMINATED', 'LOW AVAILABILITY', 'SUSPENDED', 'LIMBO')
and "AccountId" in (
								select cleaner_id
								from main.account
								where status = 'BETA'
								and on_babysitting = true)
and cast("CreatedDate" as date) = crunchdate-1
group by "NewValue"
) x
;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$ LANGUAGE 'plpgsql'