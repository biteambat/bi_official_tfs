﻿CREATE OR REPLACE FUNCTION bi.daily$report_launch_team_tracking_and_forecast(crunchdate DATE) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$report_launch_team_tracking_and_forecast';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DROP TABLE IF EXISTS reports.Launch_Team_Tracking_and_Forecast;
CREATE TABLE reports.Launch_Team_Tracking_and_Forecast as
select 
			  a.date
			, coalesce(b.Cleaner_Activity_L30_Count,0) 				AS Cleaner_Activity_L30_Count
			, coalesce(c.Onboardings_Count,0) 						AS Onboardings_Count
			, coalesce(d.Signup_Count,0) 							AS Signup_Count
			, coalesce(e.Signup_Costs,0) 							AS Signup_Costs
			, coalesce((e.Signup_Costs/d.Signup_Count),0) 			AS CPL
			, coalesce((e.Signup_Costs/c.Onboardings_Count),0)		AS CAC
			, coalesce(sum(e.Signup_Costs) OVER (Order by a.date ROWS BETWEEN 2 Preceding and CURRENT ROW) /
				sum(c.Onboardings_Count) OVER (Order by a.date ROWS BETWEEN  2 Preceding and CURRENT ROW),0) as CAC_L90days
			, g.Churn_Count
			, g.Reactivation_Count
			, (c.Onboardings_Count - g.Churn_Count + g.Reactivation_Count) as Net_New_Professionals
			, cast(0 as integer) as PoP_EoM

from (
		-- complete list of dates beginning from 1st of august 2014 till today
		select 
				to_char(date,'YYYY-MM') as date
		from generate_series(
		  '2014-08-01'::date,
		  now()::date,
		  '1 month'::interval
		  ) date
) a

	-- cleaneractivityl30, last date of the month taken
	left join (
					select
							  to_char(date,'YYYY-MM') as date
							, SUM(distinct_cleaner) AS Cleaner_Activity_L30_Count
					FROM bi.cleaneractivityl30
					where date in (
										select max(date)
										from bi.cleaneractivityl30
										group by to_char(date,'YYYY-MM')
										)
					group by date
					) b
			on a.date = b.date

	-- Onboarding - neue cleaner pro tag (daily)
	left join (
					SELECT
							  to_char(created_at,'YYYY-MM') as date
							, COUNT(cleaner_id) AS Onboardings_Count
					FROM main.account
					where name <> '#'
					and is_test_account is false
					and status is not null
					and status <> 'SUBCONTRACTOR'
					GROUP BY to_char(created_at,'YYYY-MM')
					) c
			on a.date = c.date

	-- new Leads - neue leads pro tag (daily)
	left join (
					SELECT
							  to_char(created_at,'YYYY-MM') as date
							, COUNT(lead_id) AS Signup_Count
					FROM main.lead
					where is_test_lead = false
					GROUP BY to_char(created_at,'YYYY-MM')
					) d
			on a.date = d.date

	-- signup costs per day
	left join (
					SELECT
							  to_char(to_date(DATE, 'MM-DD-YYYY'),'YYYY-MM') as date
							, SUM(costs) AS Signup_Costs
					FROM bi.leadcosts
					GROUP BY to_char(to_date(DATE, 'MM-DD-YYYY'),'YYYY-MM')
					) e
			on a.date = e.date
			
	-- count of reactivated of churned cleaner
	left join (			
					select 
							  to_char(created_at, 'YYYY-MM') as date
							, count(case when change = 'Churn' then 1 end) as Churn_Count
							, count(case when change = 'Reactivation' then 1 end) as Reactivation_Count
					from main.account_status_change
					group by to_char(created_at, 'YYYY-MM')
					) g
			on a.date = g.date;
			
	-- fügt händisch gezählte Werte für die Spalte Professionals on Plattform ein
	update reports.launch_team_tracking_and_forecast as x
	set pop_eom  = y.PoP_EoM
	from (values
			    ('2014-08', 102),
			    ('2014-09', 175),
			    ('2014-10', 274),
			    ('2014-11', 345),
			    ('2014-12', 436),
			    ('2015-01', 532),
			    ('2015-02', 650),
			    ('2015-03', 815),
			    ('2015-04', 939),
			    ('2015-05', 1075),
			    ('2015-06', 1284),
			    ('2015-07', 1481),
			    ('2015-08', 1481 + (select net_new_professionals as a
											from reports.launch_team_tracking_and_forecast
											where date = '2015-08')
				 )
			) as y (date, PoP_Eom) 
	where x.date = y.date;

	-- fügt händisch gezählte Werte für die Spalte CAC von August 2014 bis Juli 2015 ein
	update reports.launch_team_tracking_and_forecast as x
	set cac  = y.cac, cac_l90days = y.avg_cac
	from (values
			    ('2014-08', 189, 0),								
			    ('2014-09', 283, 0),
			    ('2014-10', 146, 203.75),
			    ('2014-11', 169, 194.60),
			    ('2014-12', 105, 140.61),
			    ('2015-01', 221, 169.38),
			    ('2015-02', 187, 177.14),
			    ('2015-03', 153, 180.02),
			    ('2015-04', 130, 156.40),
			    ('2015-05', 179, 153.69),
			    ('2015-06', 178, 164.57),
			    ('2015-07', 143, 165.16)
			) as y (date, cac, avg_cac) 
	where x.date = y.date;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$ LANGUAGE 'plpgsql'