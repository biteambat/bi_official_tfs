-- ==================================================================================== 
-- Contract SENT or EXPIRED WITHOUT other ACCEPTED, SIGNED, RESIGNED or CANCELLED contract AND orders this month 
-- ==================================================================================== 

SELECT t1.*

FROM 
(
SELECT 	OpportunityId
		, opp.name
		, o.locale__c
		, o.test__c 

FROM salesforce.order o
LEFT JOIN salesforce.opportunity opp ON o.opportunityid = opp.sfid

WHERE 	o.status 					IN 	('PENDING TO START', 'PENDING ALLOCATION', 'FULFILLED', 'NOSHOW CUSTOMER', 'CANCELLED CUSTOMER') 
		AND EXTRACT(YEAR FROM o.order_start__c::date ) 		= EXTRACT(YEAR FROM CURRENT_DATE)
		AND EXTRACT(MONTH FROM o.order_start__c::date ) 	= EXTRACT(MONTH FROM CURRENT_DATE)
		AND o.type 					= 	'cleaning-b2b' 
		AND o.locale__c 			IN 	('de-en', 'de-de') 
		AND o.test__c  				= FALSE
		AND o.opportunityId  	IN 	(	SELECT 	co.opportunity__c 
										FROM 	salesforce.contract__c co
										WHERE 	co.status__c 	IN 	('SENT', 'EXPIRED', 'CANCELLED MISTAKE'))
GROUP BY opportunityId
		, opp.name
		, o.locale__c 
		, o.test__c
		) AS t1
		
LEFT JOIN 

(SELECT OpportunityId
		, opp.name
		, o.locale__c
		, o.test__c 

FROM salesforce.order o
LEFT JOIN salesforce.opportunity opp ON o.opportunityid = opp.sfid

WHERE 	o.status 					IN 	('PENDING TO START', 'PENDING ALLOCATION', 'FULFILLED', 'NOSHOW CUSTOMER', 'CANCELLED CUSTOMER') 
		AND EXTRACT(YEAR FROM o.order_start__c::date ) 		= EXTRACT(YEAR FROM CURRENT_DATE)
		AND EXTRACT(MONTH FROM o.order_start__c::date ) 	= EXTRACT(MONTH FROM CURRENT_DATE)
		AND o.type 					= 	'cleaning-b2b' 
		AND o.locale__c 			IN 	('de-en', 'de-de') 
		AND o.test__c  				= FALSE
		AND o.opportunityId  	IN 	(	SELECT 	co.opportunity__c 
										FROM 	salesforce.contract__c co
										WHERE 	co.status__c 	IN 	('ACCEPTED', 'SIGNED', 'RESIGNED','CANCELLED'))
GROUP BY opportunityId
		, opp.name
		, o.locale__c 
		, o.test__c
		) AS t2 		ON t1.opportunityid = t2.opportunityid
WHERE 	t2.opportunityid IS NULL
	--	AND t1.opportunityid = '0060J00000rxStqQAE'