-- ==================================================================================== 
-- CCE in the Past AND CCE < when accepted date in VALID contract 
-- ==================================================================================== 

SELECT 	opp.sfid 				opportuniy_id
		, opp.status__c
--		, o.effectivedate
--		, o.sfid 				order_id
		, co.confirmed_end__c
		, co.sfid				contract_id
		,*

FROM 	salesforce.opportunity opp
--LEFT JOIN salesforce.order o ON o.opportunityid = opp.sfid
LEFT JOIN salesforce.contract__c co ON opp.sfid = co.opportunity__c

WHERE 	co.when_accepted__c::date > co.confirmed_end__c::date
		AND opp.sfid  	 IN 	(	SELECT 	valid.opportunity__c 
									FROM 	salesforce.contract__c valid
									WHERE 	valid.status__c 	IN 	('SIGNED', 'ACCEPTED'))
									--AND co.opportunity__c = '0060J00000rxStqQAE'
		AND co.confirmed_end__c::date < CURRENT_DATE