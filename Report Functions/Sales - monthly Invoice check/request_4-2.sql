-- ==================================================================================== 
-- CCE this month
-- ==================================================================================== 

SELECT 	opp.sfid AS opp_id
		, opp.name
		, opp.status__c AS opp_status
		, MAX (o.effectivedate::date) AS lastorder
		-- last order date
		, co.sfid AS contract_id
		, co.status__c AS contract_status
		, co.resignation_date__c
		, co.confirmed_end__c
		, opp.plan_end__c
		, opp.churn_reason__c

FROM 		salesforce.opportunity 	opp
LEFT JOIN 	salesforce.contract__c 	co 		ON opp.sfid = co.opportunity__c
LEFT JOIN 	salesforce.order		o 		ON opp.sfid = o.opportunityid

WHERE	EXTRACT(YEAR FROM co.confirmed_end__c::date) 		= EXTRACT(YEAR FROM CURRENT_DATE )
		AND EXTRACT(MONTH FROM co.confirmed_end__c::date) 	= EXTRACT(MONTH FROM CURRENT_DATE)
		AND opp.test__c = FALSE
GROUP BY
		opp.sfid
		, opp.name
		, opp.status__c
		--, o.effectivedate
		, co.sfid
		, co.status__c
		, co.resignation_date__c
		, co.confirmed_end__c
		, opp.plan_end__c
		, opp.churn_reason__c
;