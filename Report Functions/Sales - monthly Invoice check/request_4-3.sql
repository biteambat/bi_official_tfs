-- =========
-- NEW customer resigned contract with orders after contract confirmed end date  
-- ========
SELECT 	opp.sfid 				opportuniy_id
		, opp.status__c
		, o.effectivedate
		, o.sfid 				order_id
		, co.confirmed_end__c
		, co.sfid				contract_id
--		,*

FROM 	salesforce.opportunity opp
LEFT JOIN salesforce.order o ON o.opportunityid = opp.sfid
LEFT JOIN salesforce.contract__c co ON opp.sfid = co.opportunity__c

WHERE 	o.effectivedate > co.confirmed_end__c
		AND opp.sfid  	NOT IN 	(	SELECT 	valid.opportunity__c 
									FROM 	salesforce.contract__c valid
									WHERE 	valid.status__c 	IN 	('SIGNED', 'ACCEPTED'))
									--AND co.opportunity__c = '0060J00000rxStqQAE'