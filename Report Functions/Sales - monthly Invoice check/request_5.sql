-- =========
-- new One Off contracts with orders in the previous month (month of invoice round) 
-- RUN THIS QUERY AFTER YOU'VE INSERTED THE ONE OFFS IN THE bi.b2b_additionl_bookings 
-- ========
SELECT 	ab.opportunity
		, ab.closed_date
		, ab.acquisition_type
		, ab.package
		
FROM 	bi.b2b_additional_booking 	ab
LEFT JOIN salesforce.order 			o ON ab.opportunity = o.opportunityid
LEFT JOIN salesforce.contract__c	c ON ab.opportunity = c.opportunity__c
WHERE 	
	 EXTRACT(YEAR FROM o.effectivedate::date)	= EXTRACT(YEAR FROM  CURRENT_DATE - INTERVAL '1 Month' )
	AND EXTRACT(MONTH FROM o.effectivedate::date)	= EXTRACT(MONTH FROM CURRENT_DATE - INTERVAL '1 Month')
	AND ab.package IN ('standard','economy','premium','holiday replacement')
	AND c.status__c IN ('SIGNED', 'ACCEPTED')
	AND c.duration__c IS NULL

GROUP BY ab.opportunity
		, ab.closed_date
		, ab.acquisition_type
		, ab.package

--LIMIT 10
;