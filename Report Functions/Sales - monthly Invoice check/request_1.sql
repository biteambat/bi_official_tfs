-- ==================================================================================== 
-- DONE - Opportunities with contracts ACCEPTED,SIGNED this month but orders in the past 
-- ==================================================================================== 

SELECT 	opportunityid										opportunity_id
		, opp.name											opportunity
		, o.locale__c
		, o.test__c 
		, co.contract_name									contract_name
		, co.contract_id									contract_id
		, co.status											contract_status
		, co.when_accepted
		, co.grand_total									contract_grand_total
		, co.startdate										contract_start
		, co.eventdate										eventdate
		, co.newvalue
		, co.field
		

FROM salesforce.order o
LEFT JOIN salesforce.opportunity opp 	ON o.opportunityid 	= opp.sfid
LEFT JOIN 	(SELECT contract.sfid				contract_id
					, contract.name				contract_name
					, contract.opportunity__c	opportunity									
					, contract.status__c		status								
					, contract.when_accepted__c when_accepted
					, contract.grand_total__c	grand_total								
					, contract.start__c			startdate
					, contract.test__c			test				
					, hi.createddate::date 			eventdate						
					, hi.newvalue 				newvalue
					, hi.field					field
			FROM salesforce.contract__c contract 	
			LEFT JOIN salesforce.contract__history hi 		ON contract.sfid = hi.parentid
			
			WHERE 	hi.field 			='status__c'
					AND hi.newvalue 	IN ('ACCEPTED','SIGNED')
			) AS co 										ON o.opportunityid 	= co.opportunity

WHERE 	o.status 											IN ('INVOICED','PENDING TO START', 'PENDING ALLOCATION', 'FULFILLED', 'NOSHOW CUSTOMER', 'CANCELLED CUSTOMER') 
	--	AND EXTRACT(YEAR FROM o.order_start__c::date ) 		<= EXTRACT(YEAR FROM (CURRENT_DATE - interval '3 month'))
	--	AND EXTRACT(MONTH FROM o.order_start__c::date ) 	<= EXTRACT(MONTH FROM (CURRENT_DATE - interval '3 month'))
		AND o.order_start__c::date 							< co.eventdate::date
		AND o.type 											= 'cleaning-b2b' 
		AND o.locale__c 									IN ('de-en', 'de-de') 
		AND o.test__c  										= FALSE
		AND co.test 										= FALSE 
		AND EXTRACT(YEAR FROM co.eventdate::date) 			= EXTRACT(YEAR FROM (CURRENT_DATE - interval '0 month'))
		AND EXTRACT(MONTH FROM co.eventdate::date) 			= EXTRACT(MONTH FROM (CURRENT_DATE - interval '0 month'))
		AND co.status 										IN 	('ACCEPTED', 'SIGNED') 

GROUP BY opportunityId
		, opp.name
		, o.locale__c 
		, o.test__c
		, co.contract_name
		, co.contract_id
		, co.status
		, co.when_accepted
		, co.grand_total
		, co.startdate
		, co.eventdate 								
		, co.newvalue
		, co.field		

ORDER BY  co.eventdate
		, co.startdate