﻿CREATE OR REPLACE FUNCTION bi.daily$report_cac_cpl_by_new_leadsource(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$report_cac_cpl_by_new_leadsource';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DROP TABLE IF EXISTS reports.CAC_CPL_by_new_Leadsource;
CREATE TABLE reports.CAC_CPL_by_new_Leadsource as
Select
 a.date
, a.locale
, a.lead_source
, COALESCE(b.Onboardings_Count,0) as Onboardings_Count
, COALESCE(c.Signup_Count,0) as Signup_Count
, COALESCE(d.Costs,0) as Costs

from (
-- complete list of dates beginning from 1st of august 2014 till today plus locales and leadsources
select 
 cast(date as date) as date
     , locale.* as locale
, unnest(array (
select distinct(
case
when new_lead_source = 'SEM' or new_lead_source = 'Brand' then 'Search'
when new_lead_source = 'Landing Page' or new_lead_source = 'Newsletter' then 'Others'
when new_lead_source = 'Outbound lead gen' then 'Outbound Lead gen'
else COALESCE(new_lead_source,'Others')
end
) as lead_source
from main.lead_source_new)) as lead_source
from generate_series(
 '2014-08-01'::date,
 now()::date,
 '1 day'::interval
 ) date, unnest(array['DE','CH','AT','NL']) as locale
) a
left join ( -- onboarding account per day, locale and leadsource

select 
 cast(x.created_at as date) as dateB
, upper(left(x.locale,2)) as localeB
, case
when y.new_lead_source = 'SEM' or y.new_lead_source = 'Brand' then 'Search'
when y.new_lead_source = 'Landing Page' or y.new_lead_source = 'Newsletter' then 'Others'
when y.new_lead_source = 'Outbound lead gen' then 'Outbound Lead gen'
else COALESCE(y.new_lead_source,'Others')
end as lead_sourceB
, count(*) as Onboardings_Count
from main.account x
inner join main.lead_source_new y
on x.cleaner_id  = y.cleaner_id
where x.is_test_account = false
and status <> 'SUBCONTRACTOR'

group by dateB, localeB, lead_sourceB
) b
on a.date = b.dateB
and a.locale = b.localeB
and a.lead_source = b.lead_sourceB
left join (-- signup account per day, locale and leadsource
select 
 cast(x.created_at as date) as dateC
, upper(left(x.locale,2)) as localeC
, case
when y.new_lead_source = 'SEM' or y.new_lead_source = 'Brand' then 'Search'
when y.new_lead_source = 'Landing Page' or y.new_lead_source = 'Newsletter' then 'Others'
when y.new_lead_source = 'Outbound lead gen' then 'Outbound Lead gen'
else COALESCE(y.new_lead_source,'Others')
end as lead_sourceC
, count(*) as Signup_Count
from main.lead x
inner join main.lead_source_new y
on x.lead_id  = y.lead_id
where x.is_test_lead <> true
and left(x.locale,2) in ('de','ch','at','nl')
group by dateC, localeC, lead_sourceC
) c
on a.date = c.dateC
and a.locale = c.localeC
and a.lead_source = c.lead_sourceC

left join ( -- leadcosts
select 
 to_date(date, 'mm/dd/yyyy') as dateD
, locale as localeD
, leadsource as lead_sourceD
, sum(costs) as costs
from bi.leadcosts
group by dateD, localeD, lead_sourceD
) d
on a.date = d.dateD
and a.locale = d.localeD
and a.lead_source = d.lead_sourceD;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$ LANGUAGE 'plpgsql'