﻿CREATE OR REPLACE FUNCTION bi.daily$report_new_dashboard(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$report_new_dashboard';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DROP TABLE IF EXISTS reports.new_dashboard;
CREATE TABLE reports.new_dashboard as 

-- ++++++++++++++ INVOICED GMV
select
		  order_start_date_utc as date
		, upper(left(o.locale,2)) as locale
		, a.working_city
		, 'InvoicedGMV' as KPI
		, sum(gmv_amount) as Value
from main.order o
inner join (
				-- benötigt für working city
				select cleaner_id, working_city
				from main.account
				where is_test_account = false
				) a
		on o.cleaner_id = a.cleaner_id
where order_status = 'INVOICED'
group by order_start_date_utc, upper(left(o.locale,2)), a.working_city
UNION
-- ++++++++++++++ ACQUISITIONS
select
		  created_at as date
		, upper(left(o.locale,2)) as locale
		, a.working_city
		, 'Acquisitions' as KPI
		, count(*) as Value
from main.order o
inner join (
				-- benötigt für working city
				select cleaner_id, working_city
				from main.account
				where is_test_account = false
				) a
		on o.cleaner_id = a.cleaner_id
where order_status <> 'CANCELLED FAKED' and order_status <> 'CANCELLED MISTAKE'
and is_new_customer is true
group by created_at, upper(left(o.locale,2)), a.working_city
UNION
-- ++++++++++++++ Cleaner Onboarding
select 
			  cast(created_at as date) as date
			, upper(left(locale,2)) as locale
			, working_city
			, 'Cleaner_Onboarding' as KPI
			, count(*) as Value

				from main.account
				where is_test_account = false
group by cast(created_at as date), upper(left(locale,2)), working_city
UNION
-- ++++++++++++++ NO MAN POWER
select
		  order_start_date_utc as date
		, upper(left(o.locale,2)) as locale
		, a.working_city
		, 'NMP' as KPI
		, count(*) as Value
from main.order o
inner join (
				-- benötigt für working city
				select cleaner_id, working_city
				from main.account
				where is_test_account = false
				) a
		on o.cleaner_id = a.cleaner_id
where order_status = 'CANCELLED NO MANPOWER'
group by order_start_date_utc, upper(left(o.locale,2)), a.working_city
UNION
-- ++++++++++++++ Cleaner Offboarding
select
	cast(hr_contract_end as date) as date
	, upper(left(locale,2)) as locale
	, working_city
	, 'Cleaner_Offboarding' as KPI
	, count(*)
from main.account
where hr_contract_end is not null
group by cast(hr_contract_end as date), upper(left(locale,2)), working_city
;
-- ++++++++++++++ Active Cleaner
-- tba

-- ++++++++++++++ Utilization
-- tba

-- ++++++++++++++ M1RR
-- tba

-- ++++++++++++++ M3RR
-- tba

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$ LANGUAGE 'plpgsql'