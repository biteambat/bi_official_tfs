﻿CREATE OR REPLACE FUNCTION bi.daily$report_muffin_stats_per_working_city(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.daily$report_muffin_stats_per_working_city';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

DROP TABLE IF EXISTS reports.muffin_stats_per_working_city;
CREATE TABLE reports.muffin_stats_per_working_city as 

select
		  order_start_date_utc
		, a.cleaner_id
		, order_id
		, order_status
		, a.working_city
		, price_per_hour
		, gmv_amount
		, order_duration
		, recurrency
		, a.locale
		
from main.order o
inner join (
				-- benötigt für working city und cleanertype
				select cleaner_id, cleaner_type, working_city, upper(left(locale,2)) as locale
				from main.account
				where is_test_account = false
				) a
		on o.cleaner_id = a.cleaner_id

inner join (
				-- schließt nur Städte in denen Muffins arbeiten ein
				select distinct working_city
				from main.account
				where cleaner_type = '60'
				and is_test_account = false
				) f
		on a.working_city = f.working_city
		
where order_status in ('INVOICED', 'PENDING TO START');

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);
END;
$BODY$ LANGUAGE 'plpgsql'