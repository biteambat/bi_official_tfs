

-- Comments on question #14
-- Not quite sure what this means but let's do buckets of contracts:
-- Unlimited, 6 months, 12 months, Other


SELECT

	t1.type_contract,
	-- t1.opportunity__c,
	-- t1.status__c,
	-- t1.sfid as sfid_contract,
	COUNT(DISTINCT t1.opportunity__c) as number_opps,
	SUM(t1.grand_total_adjusted) as revenue_opps


FROM	
	
	(SELECT
	
		o.opportunity__c,
		o.sfid,
		o.duration__c,
		o.status__c,
		-- o.confirmed_end__c,
		CASE WHEN (o.duration__c IS NULL AND o.confirmed_end__c IS NOT NULL) THEN 'one-off'
		     WHEN o.duration__c = '6' THEN '6 months'
		     WHEN o.duration__c = '12' THEN '12 months'
		     WHEN o.duration__c = '3' THEN '3 months'
		     WHEN (o.duration__c IS NULL AND o.confirmed_end__c IS NULL) THEN 'unlimited'
		     ELSE 'other'
		     END as type_contract,
		MAX(CASE WHEN oo.grand_total__c IS NULL THEN ooo.potential ELSE oo.grand_total__c END) as grand_total_adjusted
	
	FROM
	
	
		salesforce.contract__c o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunity__c = oo.sfid
		
	LEFT JOIN
		
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunity__c = ooo.opportunityid
		
	WHERE
	
		o.active__c IS TRUE
		-- o.status__c IN ('ACCEPTED', 'SIGNED', 'NEW', 'SENT')
		-- o.status__c NOT IN ('ACCEPTED', 'SIGNED', 'NEW', 'SENT', 'DECLINED', 'RESIGNED', 'CANCELLED MISTAKE')
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
		AND LEFT(oo.locale__c, 2) = 'de'
		AND oo.status__c IN ('RUNNING', 'RETENTION', 'OFFBOARDING', 'RENEGOTIATION', 'ONBOARDED') -- NEW LINE
		AND o.service_type__c LIKE 'maintenance cleaning'
		
	GROUP BY
	
		o.opportunity__c,
		o.sfid,
		o.duration__c,
		o.status__c,
		CASE WHEN (o.duration__c IS NULL AND o.confirmed_end__c IS NOT NULL) THEN 'one-off'
		     WHEN o.duration__c = '6' THEN '6 months'
		     WHEN o.duration__c = '12' THEN '12 months'
		     WHEN o.duration__c = '3' THEN '3 months'
		     WHEN (o.duration__c IS NULL AND o.confirmed_end__c IS NULL) THEN 'unlimited'
		     ELSE 'other'
		     END) as t1
		-- CASE WHEN o.grand_total__c IS NULL THEN ooo.potential ELSE o.grand_total__c END) as t1
		
GROUP BY

	t1.type_contract
	-- t1.sfid,
	-- t1.opportunity__c
	-- t1.status__c
		
