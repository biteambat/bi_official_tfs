SELECT

	TO_CHAR(o.issued__c, 'YYYY-MM') as year_month,
	MIN(o.issued__c) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	ooo.delivery_area__c as city,
	'B2B' as type,
	'Revenue' as kpi,
	oo.provider,
	-- 'Partner' as sub_kpi_1,
	oo.company_name as sub_kpi_2,
	oo.opportunity as sub_kpi_3,
	ooo.status__c as sub_kpi_4,
	'-' as sub_kpi_5,
	MAX(o.amount__c)/1.19 as value
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	bi.order_provider oo
	
ON

	o.opportunity__c = oo.opportunityid

LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.opportunity__c = ooo.sfid
	
WHERE

	oo.provider = 'Partner'
	-- AND 
	AND o.test__C IS FALSE
	AND TO_CHAR(o.issued__c, 'YYYY-MM') = '2019-04'
	AND o.opportunity__c IS NOT NULL
	
GROUP BY

	year_month,
	LEFT(o.locale__c, 2),
	o.locale__c,
	ooo.delivery_area__c,
	oo.provider,
	oo.company_name,
	oo.opportunity,
	ooo.status__c
	
ORDER BY

	year_month desc;