


SELECT

	o.opportunityid,
	oo.potential,
	o.served_by__c,
	a.name

FROM

	salesforce.order o
	
LEFT JOIN

	bi.potential_revenue_per_opp oo
	
ON

	o.opportunityid = oo.opportunityid
	
LEFT JOIN

	salesforce.account a
	
ON 
	
	o.served_by__c = a.sfid
	
WHERE

	o.test__c IS FALSE
	AND o.opportunityid IS NOT NULL
	AND o.effectivedate >= '2019-04-01'
	AND o.effectivedate <= '2019-04-30'
	AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'CANCELLED CUSTOMER')
	
GROUP BY

	o.opportunityid,
	oo.potential,
	o.served_by__c,
	a.name