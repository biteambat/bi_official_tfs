

SELECT

	TO_CHAR(o.effectivedate, 'YYYY-WW') as year_week,
	COUNT(DISTINCT o.sfid) as number_orders,
	COUNT(DISTINCT o.customer_name__c) as number_customers,
	COUNT(DISTINCT o.sfid)/COUNT(DISTINCT o.customer_name__c)::decimal as avg_noshow_cust

FROM

	salesforce."order" o
	
WHERE

	o.opportunityid IS NOT NULL
	AND o."status" IN ('NOSHOW PROFESSIONAL')
	AND o.effectivedate > '2019-01-01'
	
GROUP BY

	TO_CHAR(o.effectivedate, 'YYYY-WW')
	
ORDER BY

	TO_CHAR(o.effectivedate, 'YYYY-WW') desc