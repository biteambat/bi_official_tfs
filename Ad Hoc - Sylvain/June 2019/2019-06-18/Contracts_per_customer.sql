	



SELECT

	t1.customer__c,
	SUM(CASE WHEN active__c IS FALSE THEN 1 ELSE 0 END) as inactive,
	SUM(CASE WHEN active__c IS TRUE THEN 1 ELSE 0 END) as active

FROM	
	
	(SELECT
	
		o.customer__c,
		o.sfid,
		oo.sfid,
		oo.active__c,
		oo.status__c,
		oo.service_type__c
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.sfid = oo.opportunity__c
		
	WHERE
	
		-- oo.service_type__c = 'maintenance cleaning'
		o.status__c IN ('RUNNING', 'RETENTION', 'ONBOARDED', 'SIGNED', 'RENEGOTIATION')) as t1

WHERE

	customer__c IS NOT NULL
		
GROUP BY

	t1.customer__c