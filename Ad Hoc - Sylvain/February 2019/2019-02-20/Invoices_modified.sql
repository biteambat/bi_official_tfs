


SELECT


	*

FROM

	salesforce.invoice__c o
	
WHERE 

	o.lastmodifieddate > '2019-02-21'
	AND LEFT(o.issued__c::text, 7) = '2019-01'