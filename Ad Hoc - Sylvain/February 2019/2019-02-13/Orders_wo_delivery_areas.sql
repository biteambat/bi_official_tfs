



SELECT

	o.opportunityid,
	o.sfid,
	o.effectivedate,
	o.shippingcity,
	oo.delivery_area__c as area_opp,
	o.delivery_area__c as area_order
	
FROM

	salesforce."order" o

LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
WHERE

	(o."status" = 'INVOICED'
	OR o."status" = 'PENDING TO START')
	AND o.delivery_area__c IS NULL
	AND o.effectivedate > '2018-12-31'
	AND o.test__c IS FALSE
	AND o.locale__c LIKE 'de%'
	-- o."type" = 'cleaning-b2b'
	
GROUP BY
	
	o.opportunityid,
	o.sfid,
	o.effectivedate,
	o.shippingcity,
	oo.delivery_area__c,
	o.delivery_area__c
	