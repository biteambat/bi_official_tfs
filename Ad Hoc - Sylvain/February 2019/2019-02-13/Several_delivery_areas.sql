


SELECT

	o.sfid,
	o.name,
	COUNT(DISTINCT o.delivery_area__c) as delivery_Areas


FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.order oo
	
ON 

	o.sfid = oo.opportunityid

WHERE

	o.stagename IN ('WON', 'PENDING')
	
GROUP BY

	o.sfid,
	o.name

	