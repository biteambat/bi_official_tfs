	
	
SELECT

	t3.*,
	CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
	CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date

FROM	
	
	(SELECT
	
		t1.country,
		t1.opportunityid,
		t1.date_last_order,
		t2.date_churn as contract_end
	
	FROM
	
		(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
		         -- It's the last day on which they are making money
			LEFT(o.locale__c, 2) as country,
			o.opportunityid,
			'last_order' as type_date,
			MAX(o.effectivedate) as date_last_order
			
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.status__c IN ('RESIGNED', 'CANCELLED')
			AND o.type = 'cleaning-b2b'
			AND o.professional__c IS NOT NULL
			AND o.test__c IS FALSE
			AND oo.test__c IS FALSE
		
		GROUP BY
		
			LEFT(o.locale__c, 2),
			type_date,
			o.opportunityid) as t1
			
	LEFT JOIN
	
		(SELECT -- Here we make the list of opps with their confirmed end or end date when available
								
			o.opportunity__c as opportunityid,
			MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
		
		FROM
		
			salesforce.contract__c o
		
		WHERE
		
			o.test__c IS FALSE
			AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
			
		GROUP BY
		
			o.opportunity__c) as t2 
	
	ON
	
		t1.opportunityid = t2.opportunityid) as t3
		
		
		
		
	
	
