

SELECT

	o.sfid,
	o."status",
	o.delivery_area__c,
	oo.delivery_area__c,
	o.opportunityid,
	oo.stagename,
	oo.status__c,
	o.professional__c,
	a.delivery_areas__c

FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid

LEFT JOIN

	salesforce.account a
	
ON

	o.professional__c = a.sfid
	
WHERE

	o.delivery_area__c IS NULL
	AND o.opportunityid IS NOT NULL
	AND oo.delivery_area__c IS NULL
	AND o."type" = 'cleaning-b2b'
	AND o.test__c IS FALSE
	AND oo.test__c IS FALSE
	AND oo.status__c IS NOT NULL
	AND o.professional__c IS NULL
	AND LEFT(o.locale__c, 2) = 'de'
	