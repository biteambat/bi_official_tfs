



SELECT

	o.*,
	oo.potential

FROM

	bi.order_provider o
	
LEFT JOIN

	bi.potential_revenue_per_opp oo
	
ON

	o.opportunityid = oo.opportunityid
	
WHERE

	o.effectivedate >= '2019-02-01'
	AND LEFT(o.delivery_area__c, 2) = 'de'
	-- LEFT(o.effectivedate::text, 7) = '2019-02'