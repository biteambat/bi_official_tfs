


SELECT


	*

FROM

	salesforce.invoice__c o
	
WHERE 

	o.lastmodifieddate > '2019-02-19'
	AND o.lastmodifieddate < '2019-02-22'
	AND LEFT(o.issued__c::text, 7) = '2019-01'
	AND o.opportunity__c IS NOT NULL
	AND o.status__c = 'REVERSED'