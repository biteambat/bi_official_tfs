



SELECT

	o.sfid,
	o.status__c as status_opp,
	oo.status__c as status_contract,
	oo.sfid as sfid_contract,
	oo.confirmed_end__c,
	MAX(ooo.effectivedate) as last_valid_order


FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.order ooo
	
ON

	o.sfid = ooo.opportunityid
	
WHERE

	o.status__c IN ('RESIGNED', 'CANCELLED', 'OFFBOARDING')
	AND ooo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'CANCELLED CUSTOMER')
	AND oo.status__c IN ('CANCELLED', 'RESIGNED', 'ACCEPTED', 'SIGNED')
	AND oo.confirmed_end__c IS NULL
	AND o.test__c IS FALSE
	AND oo.test__c IS FALSE
	AND ooo.test__c IS FALSE
	
GROUP BY

	o.sfid,
	o.status__c,
	oo.sfid,
	oo.status__c,
	oo.confirmed_end__c