

SELECT

	o.createddate,
	o.sfid as sfid_offer,
	o.status__c,
	o.opportunity__c,
	oo.name,
	oo.grand_total__c,
	CASE WHEN oo.grand_total__c < 250 THEN 'Very Small'
		  WHEN oo.grand_total__c >= 250 AND oo.grand_total__c < 500 THEN 'Small'
		  WHEN oo.grand_total__c >= 500 AND oo.grand_total__c < 1000 THEN 'Medium'
		  WHEN oo.grand_total__c IS NULL THEN 'Unknown'
		  ELSE 'Key Account'
		  END as type_customer,
	oo.hours_weekly__c,
	oo.times_per_week__c,
	oo.office_bathrooms__c,
	oo.office_employees__c,
	oo.office_rooms__c,
	oo.office_kitchens__c,
	oo.office_bathrooms__c + oo.office_rooms__c + oo.office_kitchens__c as all_rooms,
	
	CASE WHEN oo.office_size__c <= 100 THEN '<= 100'
	     WHEN (oo.office_size__c > 100  AND oo.office_size__c <= 200) THEN '100 < x <= 200'
		  WHEN (oo.office_size__c > 200  AND oo.office_size__c <= 300) THEN '200 < x <= 300'
		  WHEN (oo.office_size__c > 300  AND oo.office_size__c <= 400) THEN '300 < x <= 400'
		  ELSE '> 400'
		  END as office_size,
	
	CASE WHEN oo.additional_services__c IS NULL THEN 'No' ELSE 'Yes' END as additional_service,
	oo.recurrency__c,
	o.partner__c as partner_won,
	new_dates.new_date,
	send_dates.send_date,
	pending_dates.pending_date,

	accepted_dates.accepted_date,
	cancelled_dates.cancelled_date,
	CASE WHEN (o.status__c IN ('ACCEPTED', 'SEND', 'CANCELLED', 'PENDING') AND accepted_dates.accepted_date IS NOT NULL) THEN accepted_dates.accepted_date::timestamp - o.createddate::timestamp
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NOT NULL) THEN accepted_dates.accepted_date - o.createddate::timestamp
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NULL) THEN cancelled_dates.cancelled_date - o.createddate::timestamp
		  WHEN o.status__c IN ('NEW', 'SEND', 'PENDING') THEN current_timestamp - o.createddate::timestamp
		  ELSE current_timestamp - o.createddate::timestamp
		  END as waiting_time,
		  
	(CASE WHEN o.status__c = 'ACCEPTED' THEN DATE_PART('day', accepted_dates.accepted_date::timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', accepted_dates.accepted_date::timestamp - o.createddate::timestamp) + DATE_PART('minute', accepted_dates.accepted_date::timestamp - o.createddate::timestamp)/60
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NOT NULL) THEN DATE_PART('day', accepted_dates.accepted_date - o.createddate::timestamp)*24 + DATE_PART('hour', accepted_dates.accepted_date - o.createddate::timestamp) + DATE_PART('minute', accepted_dates.accepted_date - o.createddate::timestamp)/60
		 
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NULL) THEN DATE_PART('day', cancelled_dates.cancelled_date - o.createddate::timestamp)*24 + DATE_PART('hour', cancelled_dates.cancelled_date - o.createddate::timestamp) + DATE_PART('minute', cancelled_dates.cancelled_date - o.createddate::timestamp)/60
		  
		  WHEN o.status__c IN ('NEW', 'SEND', 'PENDING') THEN DATE_PART('day', current_timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', current_timestamp - o.createddate::timestamp) + DATE_PART('minute', current_timestamp - o.createddate::timestamp)/60
		  ELSE DATE_PART('day', current_timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', current_timestamp - o.createddate::timestamp) + DATE_PART('minute', current_timestamp - o.createddate::timestamp)/60
		  END)/24 as waiting_time_2
	
	
	 
		  

FROM

	salesforce.partner_offer__c o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunity__c = oo.sfid
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as new_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.oldvalue = 'NEW'
		
	GROUP BY
	
		o.parentid) as new_dates
		
ON

	o.sfid = new_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as send_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'SEND'
		
	GROUP BY
	
		o.parentid) as send_dates
		
ON

	o.sfid = send_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as pending_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'PENDING'
		
	GROUP BY
	
		o.parentid) as pending_dates
		
ON

	o.sfid = pending_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as accepted_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'ACCEPTED'
		
	GROUP BY
	
		o.parentid) as accepted_dates
		
ON

	o.sfid = accepted_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as cancelled_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'CANCELLED'
		
	GROUP BY
	
		o.parentid) as cancelled_dates
		
ON

	o.sfid = cancelled_dates.sfid_offer
	
WHERE

	LOWER(oo.name) NOT LIKE '%test%'
	AND oo.test__c IS FALSE
	

ORDER BY

	createddate desc