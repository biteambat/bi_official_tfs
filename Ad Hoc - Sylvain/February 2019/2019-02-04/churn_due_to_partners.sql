
SELECT

	o.effectivedate as last_order,
	o.provider,
	o.company_name,
	o.delivery_area__c,
	o.opportunityid,
	o.opportunity,
	oo.status__c


FROM

	bi.order_provider o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON 

	o.opportunityid = oo.sfid