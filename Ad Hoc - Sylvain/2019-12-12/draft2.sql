


WITH cases AS
 (SELECT
	
		cas.opportunity__c,
		SUM(CASE WHEN cas.origin LIKE '%B2B customer%' 
		              OR cas.reason LIKE '%B2B customer%'
		              OR cas.reason LIKE 'Customer%'
		         THEN 1 
					ELSE 0 END) as cases_opp,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE 0 END) as feedbacks,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback: 1' THEN 1
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 2' THEN 2
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 3' THEN 3
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 4' THEN 4
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 5' THEN 5
		     ELSE 0
		     END) / SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE NULL END)::decimal as avg_feedback	
	FROM
	
		salesforce.case cas
		
	LEFT JOIN
	
		salesforce.opportunity o
		
	ON
	
		cas.opportunity__c = o.sfid
	
	WHERE
	
		o.stagename = 'WON'
		AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
		AND o.test__c IS FALSE
		
	GROUP BY	
	
		cas.opportunity__c),
		
owners AS (SELECT
			
				o.opportunityid,	
				COUNT(o.newvalue) - 1 as number_owners
			
			FROM
			
				salesforce.opportunityfieldhistory o
				
			WHERE
			
				o.field = 'Owner'
				
			GROUP BY
			
				o.opportunityid),
				
cleaners AS (SELECT
				
					o.opportunityid,
					COUNT(DISTINCT o.professional__c) as number_cleaners
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.test__c IS FALSE
					AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
					
				GROUP BY
				
					o.opportunityid),
					
orders AS (SELECT
				
					o.opportunityid,
					MIN(o.effectivedate) as first_order,
					oo.closedate as closedate,
					SUM(CASE WHEN o.status = 'CANCELLED CUSTOMER' THEN 1 ELSE 0 END) as canc_cust,
					SUM(CASE WHEN o.status = 'NOSHOW PROFESSIONAL' THEN 1 ELSE 0 END) as nsw_pro,
					SUM(CASE WHEN o.status = 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) as canc_pro
						
									
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON
				
					o.opportunityid = oo.sfid
					
				WHERE
				
					o.test__c IS FALSE
				   AND oo.status__c NOT IN ('RESIGNED', 'CANCELLED')
				   AND oo.stagename IN ('WON', 'PENDING')
					AND oo.test__c IS FALSE
					
				GROUP BY
				
					o.opportunityid,
					oo.closedate),
					
supply AS (SELECT

				p.opportunity__c,
				MAX(p.servicedate__c) as last_shipment,
				SUM(p.quantity__c) as items_shipped
					
			FROM
			
				salesforce.productlineitem__c p
				
			WHERE
			
				p.test__c IS FALSE
				AND p.opportunity__c IS NOT NULL
				
			GROUP BY
			
				p.opportunity__c),

visits AS (SELECT

				ev.whatid as opportunity,
				COUNT(DISTINCT ev.sfid) as number_visits
			
			FROM
			
				salesforce.event ev
				
			WHERE
			
				ev.whatid LIKE '006%'
				AND ev.event_reason__c LIKE 'Visit%'
				
			GROUP BY

				ev.whatid),
				
invoices AS (SELECT

			    inv.*
			
				FROM
				
				    salesforce.invoice__c inv
				    
				WHERE
				
					inv.test__c IS FALSE),
					
avg_revenue AS (SELECT

						inv.opportunity__c,
						SUM(inv.amount__c)/1.19 as total_life_amount_netto,
						COUNT(DISTINCT LEFT(inv.issued__c::text, 7)) as months_invoiced,
						SUM(inv.total_invoiced_hours_count__c) as hours_invoiced,
						CASE WHEN SUM(inv.total_invoiced_hours_count__c) <> 0 THEN
									 SUM(inv.amount__c)/(SUM(inv.total_invoiced_hours_count__c)*1.19)
							  ELSE 0
							  END as avg_hourly_revenue_netto,
						SUM(inv.amount__c)/COUNT(DISTINCT LEFT(inv.issued__c::text, 7))/1.19 as avg_monthly_amount_netto
						
					FROM
					
						salesforce.invoice__c inv
						
					WHERE
					
						inv.test__c IS FALSE
						AND inv.original_invoice__c IS NULL
						AND inv.opportunity__c IS NOT NULL
						
					GROUP BY
					
						inv.opportunity__c),
						
contract_info AS (SELECT
						
							cont.sfid as sfid_contract,
							cont.opportunity__c,
							o.name,
							o.stagename,
							o.shippingaddress_city__c,
							cont.service_type__c,
							cont.duration__c,
							cont.end__c,
							cont.notice__c,
							cont.notice_date__c,
							cont.status__c,
							cont.grand_total__c,
							o.potential_partner_costs__c,
							CASE WHEN cont.grand_total__c <> 0 THEN (cont.grand_total__c - o.potential_partner_costs__c)/cont.grand_total__c ELSE NULL END as potential_gpm,
							pot.potential as potential_last_3months,
							cont.pph__c,
							cont.hours_weekly__c,
							cont.recurrency__c as recurrency_contract,
							o.recurrency__c as recurrency_opp,
							cont.frequency__c,
							o.served_by__c,
							a.name as name_partner,
							a.pph__c as pph_partner,
							prov.professional__c,
							prov.pro_name as last_cleaner,
							prov.provider,
							CASE WHEN a.pph__c IS NULL OR prov.provider = 'BAT' THEN 12.9 ELSE a.pph__c END as hourly_cost
												
						FROM
						
							salesforce.contract__c cont
							
						LEFT JOIN
						
							salesforce.opportunity o
							
						ON
						
							cont.opportunity__c = o.sfid
							
						LEFT JOIN
						
							salesforce.account a
							
						ON
						
							o.served_by__c = a.sfid
							
						LEFT JOIN
						
							bi.order_provider prov
							
						ON
						
							cont.opportunity__c = prov.opportunityid
							
						LEFT JOIN
						
							bi.potential_revenue_per_opp pot
							
						ON
						
							cont.opportunity__c = pot.opportunityid
							
						WHERE
						
							cont.test__c IS FALSE
							AND cont.service_type__c = 'maintenance cleaning'
							AND cont.active__c IS TRUE
							AND ((cont.duration__c IS NULL AND cont.end__c IS NULL) OR cont.duration__c IS NOT NULL)
							AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')),
							
renewal AS (SELECT
				
					t1.sfid,
					t1.name,
					COUNT(t1.oldvalue) as number_renewed
				
				FROM	
					
					(SELECT
					
						hi.parentid,
						hi.field,
						hi.oldvalue,
						hi.newvalue,
						o.sfid,
						o.name
					
					FROM
					
						contract__history hi
						
					LEFT JOIN
					
						salesforce.contract__c cont
						
					ON
					
						hi.parentid = cont.sfid
						
					LEFT JOIN
					
						salesforce.opportunity o
						
					ON
					
						cont.opportunity__c = o.sfid
						
					WHERE
					
						hi.field = 'end__c'
						AND hi.oldvalue IS NOT NULL
						AND cont.service_type__c = 'maintenance cleaning'
						AND o.test__c IS FALSE
						AND cont.test__c IS FALSE
						AND hi.newvalue IS NOT NULL) as t1
						
				GROUP BY
				
					t1.sfid,
					t1.name)							
																			
-----------------------------------------------------------------------------------------------		
-----------------------------------------------------------------------------------------------	
-----------------------------------------------------------------------------------------------	
-----------------------------------------------------------------------------------------------		
								

SELECT

	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate)) as month_active,
	o.closedate,
	orders.first_order,
	o.traffic_light__c,
	cases.cases_opp,
	cases.feedbacks,
	cases.avg_feedback,
	CASE WHEN owners.number_owners IS NULL THEN 1 ELSE owners.number_owners END as number_owners,
	cleaners.number_cleaners,
	orders.canc_cust,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN orders.canc_cust 
	     ELSE orders.canc_cust/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) 
		  END as can_cust_per_month,
	orders.canc_pro,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN orders.canc_pro 
	     ELSE orders.canc_pro/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate)))
		  END as can_pro_per_month,
	orders.nsw_pro,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN orders.nsw_pro 
	     ELSE orders.nsw_pro/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) 
		  END as nsw_per_month,
	SUM(inv.balance__c) as outstanding_debt,
	avg_revenue.total_life_amount_netto,
	avg_revenue.hours_invoiced,
	avg_revenue.avg_hourly_revenue_netto,
	avg_revenue.months_invoiced,
	avg_revenue.avg_monthly_amount_netto,
		
	SUM(CASE WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '3 months')::text, 7) AND inv.original_invoice__c IS NULL AND inv.service_type__c = 'maintenance cleaning' THEN inv.amount__c
				WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '2 months')::text, 7) AND inv.original_invoice__c IS NULL AND inv.service_type__c = 'maintenance cleaning' THEN inv.amount__c
				WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 months')::text, 7) AND inv.original_invoice__c IS NULL AND inv.service_type__c = 'maintenance cleaning' THEN inv.amount__c
				ELSE 0
				END) as last_3months_invoiced,
	SUM(CASE WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '3 months')::text, 7) AND inv.original_invoice__c IS NULL AND inv.service_type__c = 'maintenance cleaning' THEN inv.amount__c
				WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '2 months')::text, 7) AND inv.original_invoice__c IS NULL AND inv.service_type__c = 'maintenance cleaning' THEN inv.amount__c
				WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 months')::text, 7) AND inv.original_invoice__c IS NULL AND inv.service_type__c = 'maintenance cleaning' THEN inv.amount__c
				ELSE 0
				END)/3 as avg_last_3months_invoiced,
	SUM(CASE WHEN (inv.service_type__c NOT LIKE '%maintenance cleaning%' AND inv.parent_invoice__c IS NULL) THEN 1 ELSE 0 END) as special_services,
	COUNT(DISTINCT inv.parent_invoice__c) as corrected_invoices,
	supply.last_shipment,
	supply.items_shipped,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN supply.items_shipped
	     ELSE supply.items_shipped/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) 
		  END as items_per_month,
	visits.number_visits,
	contract_info.*,
	avg_revenue.avg_hourly_revenue_netto,
	CASE WHEN avg_revenue.avg_hourly_revenue_netto <> 0 THEN
				(avg_revenue.avg_hourly_revenue_netto - contract_info.hourly_cost)/avg_revenue.avg_hourly_revenue_netto
		  ELSE NULL
		  END as virtual_gpm,
	renewal.number_renewed

FROM 

	salesforce.opportunity o
	
LEFT JOIN

	cases
	
ON

	o.sfid = cases.opportunity__c
	
LEFT JOIN

	owners
	
ON

	o.sfid = owners.opportunityid
	
LEFT JOIN

	cleaners
	
ON

	o.sfid = cleaners.opportunityid
	
LEFT JOIN

	orders
	
ON 

	o.sfid = orders.opportunityid
	
LEFT JOIN

	invoices as inv
	
ON

	o.sfid = inv.opportunity__c
	
LEFT JOIN

	supply
	
ON

	o.sfid = supply.opportunity__c
	
LEFT JOIN

	visits
	
ON

	o.sfid = visits.opportunity
	
LEFT JOIN

	avg_revenue
	
ON

	o.sfid = avg_revenue.opportunity__c
	
LEFT JOIN

	contract_info
	
ON

	o.sfid = contract_info.opportunity__c
	
LEFT JOIN

	renewal
	
ON

	o.sfid = renewal.sfid
	
WHERE

	o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	AND o.test__c IS FALSE
	AND o.stagename IN ('WON', 'PENDING')
	-- AND contract_info.service_type__c IS NOT NULL
	
GROUP BY
	
	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	o.closedate,
	orders.first_order,
	EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate)),
	o.traffic_light__c,
	cases.cases_opp,
	cases.feedbacks,
	cases.avg_feedback,
	owners.number_owners,
	cleaners.number_cleaners,
	orders.canc_cust,
	orders.nsw_pro,
	orders.canc_pro,
	supply.last_shipment,
	supply.items_shipped,
	visits.number_visits,
	avg_revenue.total_life_amount_netto,
	avg_revenue.avg_hourly_revenue_netto,
	avg_revenue.hours_invoiced,
	avg_revenue.months_invoiced,
	avg_revenue.avg_monthly_amount_netto,
	contract_info.sfid_contract,
	contract_info.opportunity__c,
	contract_info.name,
	contract_info.stagename,
	contract_info.shippingaddress_city__c,
	contract_info.service_type__c,
	contract_info.duration__c,
	contract_info.end__c,
	contract_info.notice__c,
	contract_info.notice_date__c,
	contract_info.status__c,
	contract_info.grand_total__c,
	contract_info.potential_partner_costs__c,
	contract_info.potential_gpm,
	contract_info.potential_last_3months,
	contract_info.pph__c,
	contract_info.hours_weekly__c,
	contract_info.recurrency_contract,
	contract_info.recurrency_opp,
	contract_info.frequency__c,
	contract_info.served_by__c,
	contract_info.name_partner,
	contract_info.professional__c,
	contract_info.last_cleaner,
	contract_info.provider,
	contract_info.pph_partner,
	contract_info.hourly_cost,
	renewal.number_renewed
	
	
	
	
	