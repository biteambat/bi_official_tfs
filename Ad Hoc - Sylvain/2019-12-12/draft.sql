

-- Active maintenance cleaning contracts started before 2019-12-01


SELECT 
    t1.year_month,
    t1.effective_start__c,
    t1.opportunity__c,
    t1.opp_name,
    t1.city,
    t1.subscription_model,
    t1.supplies__c,
    t1.month_active,
    t1.monthly_hours::DECIMAL,
    CASE WHEN t1.month_active = 0 THEN t1.grand_total__c
        ELSE SUM(t1.grand_total__c) / NULLIF((t1.month_active), 0) END AS avg_monthly_revenue,
  --  CASE WHEN t1.grand_total__c = 0 THEN t1.plan_pph__c * t1.invoiced_hours ELSE t1.grand_total__c END AS revenue
    CASE WHEN t1.hours_weekly__c > 0 AND t1.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t1.grand_total__c::NUMERIC / (t1.hours_weekly__c * 4.33)::NUMERIC), 2) END AS pph_contractual,
    CASE WHEN t1.hours_weekly__c > 0 AND t1.subscription_model = 'flat_fee_subscription' AND t1.recurrence = 'biweekly_recurrence' 
        THEN ROUND((t1.grand_total__c::NUMERIC / (t1.hours_weekly__c * 2.16)::NUMERIC), 2) END AS pph_contractual_biweekly,
    SUM(CASE WHEN t1.amount__c IS NULL THEN t1.plan_pph__c * t1.total_invoiced_hours_count__c ELSE t1.amount__c END)::DECIMAL AS total_invoiced_amount
FROM 
(
SELECT
    TO_CHAR(c.effective_start__c, 'YYYY-MM') AS year_month,
    c.effective_start__c,
    c.opportunity__c,
    c.name AS contract_name,
    LEFT(opps.locale__c, 2) AS country,
    SPLIT_PART(opps.delivery_area__c, '-' , 2) AS city,
    opps.name AS opp_name,
    CASE WHEN c.pph__c IS NOT NULL AND c.pph__c != 0 THEN 'pph_subscription' ELSE 'flat_fee_subscription' END AS subscription_model,
    EXTRACT(YEAR FROM AGE(c.effective_start__c)) * 12 + EXTRACT(MONTH FROM AGE(c.effective_start__c)) AS month_active,
    c.grand_total__c,
    inv.amount__c,
    inv.total_invoiced_hours_count__c,
    opps.hours_weekly__c,
    opps.hours_weekly__c * 4.33 AS monthly_hours,
    opps.recurrency__c,
    CASE WHEN opps.recurrency__c = '7' THEN 'weekly_recurrence'
        WHEN opps.recurrency__c = '14' THEN 'biweekly_recurrence'
        WHEN opps.recurrency__c IS NULL THEN 'empty_recurrence'
        END AS recurrence,
    c.pph__c,
    opps.plan_pph__c,
    opps.supplies__c
    
FROM
    salesforce.contract__c c
LEFT JOIN salesforce.opportunity opps ON
    c.opportunity__c = opps.sfid
LEFT JOIN salesforce.invoice__c inv ON
    opps.sfid = inv.opportunity__c

WHERE
    c.test__c IS FALSE
    AND opps.status__c NOT IN ('OFFBOARDING', 'RETENTION', 'CANCELLED', 'RESIGNED')
    AND c.active__c IS TRUE 
    AND c.status__c IN ('ACCEPTED', 'SIGNED')
    AND c.effective_start__c < '2019-12-01'
    AND c.service_type__c = 'maintenance cleaning'
    AND LEFT(opps.locale__c, 2) = 'de'
    AND opps.test__c IS FALSE
    AND inv.test__c IS FALSE
    AND inv.original_invoice__c IS NULL
    AND inv.parent_invoice__c IS NULL
--    AND opps.supplies__c ILIKE 'Standard%' OR opps.supplies__c ILIKE 'Premium%' OR opps.supplies__c ILIKE 'Economy%' OR opps.supplies__c SIMILAR TO 'SATNDARD%|STADNARD%|stabdard%|standarad%'

GROUP BY 
    c.effective_start__c,
    TO_CHAR(c.effective_start__c, 'YYYY-MM'),
    c.opportunity__c,
    c.name,
    c.grand_total__c,
    opps.name,
    c.effective_start__c,
    EXTRACT(YEAR FROM AGE(c.effective_start__c)) * 12 + EXTRACT(MONTH FROM AGE(c.effective_start__c)),
    c.pph__c,
    opps.plan_pph__c,
    c.hours_weekly__c,
    opps.hours_weekly__c,
    opps.supplies__c,
    LEFT(opps.locale__c, 2),
    SPLIT_PART(opps.delivery_area__c, '-' , 2),
    opps.recurrency__c,
    inv.amount__c,
    inv.total_invoiced_hours_count__c
ORDER BY 
    c.effective_start__c DESC) AS t1
GROUP BY 
    t1.year_month,
    t1.effective_start__c,
    t1.opportunity__c,
    t1.opp_name,
    t1.city,
    t1.subscription_model,
    t1.month_active,
    t1.monthly_hours,
    t1.recurrence,
    t1.hours_weekly__c,
    t1.grand_total__c,
    t1.supplies__c

ORDER BY 
    t1.effective_start__c DESC

    