


SELECT

	o.sfid,
	o.name,
	o.status__c,
	o.grand_total__c,
	cont.confirmed_end__c


FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c cont
	
ON

	o.sfid = cont.opportunity__c
	
WHERE

	o.status__c IN ('RESIGNED', 'CANCELLED')
	AND cont.status__c IN ('RESIGNED', 'CANCELLED', 'SIGNED', 'ACCEPTED')
	AND o.test__c IS FALSE
	AND cont.confirmed_end__c > current_date
	AND cont.test__c IS FALSE
	-- AND cont.active__c IS TRUE
	
	
	