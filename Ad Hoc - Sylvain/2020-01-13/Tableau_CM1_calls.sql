


SELECT

	'call' as type,
	calls.sfid,
	LEFT(calls.createddate::text, 10)::date as date_trunc,
	calls.createddate,
	calls.owner__c,
	u.name,
	calls.callconnected__c as sub_kpi_1,
	calls.calldirection__c as sub_kpi_2,
	calls.wrapup_string_1__c as sub_kpi_3,
	calls.wrapup_string_2__c as sub_kpi_4,
	calls.calldurationseconds__c as call_duration,
	calls.calltotaltalkseconds__c as call_talk,
	calls.callringseconds__c as call_ring

FROM

	salesforce.natterbox_call_reporting_object__c calls
	
LEFT JOIN

	salesforce.user u
	
ON

	calls.owner__c = u.sfid
	
WHERE 

	calls.owner__c IS NOT NULL
	AND u.name IS NOT NULL
	AND calls.createddate >= '2019-06-01'
	-- AND LOWER(calls.wrapup_string_1__c) LIKE '%b2b%'
	AND (LOWER(u.department) LIKE '%cm%' OR calls.department__c = 'CM')
	






























