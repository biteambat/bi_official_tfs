

SELECT

	*
		

FROM

	(SELECT 	
			 cases.date_part 				AS cw 
			, MIN(cases.date) 				AS date
			, cases.sub_kpi_6	 			AS agent
	
	-- FTE of every CM agent to calculate the share of the targets		
			, CASE 	WHEN cases.sub_kpi_6 = 'Katharina Kühner' 	THEN 1
					WHEN cases.sub_kpi_6 = 'Katharina Kuehner' 	THEN 1 
					WHEN cases.sub_kpi_6 = 'Sercan Tas'	 		THEN 1
					WHEN cases.sub_kpi_6 = 'André Bauß' 		THEN 1
					WHEN cases.sub_kpi_6 = 'Carmen Haas' 		THEN 1
					WHEN cases.sub_kpi_6 = 'Julian Schäfer'		THEN 1
					WHEN cases.sub_kpi_6 = 'Susanne Marino' 	THEN 1
					WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' 	THEN 1
					WHEN cases.sub_kpi_6 = 'Danny Taszarek'		THEN 1
					WHEN cases.sub_kpi_6 = 'Felix Liedtke'		THEN 1
					WHEN cases.sub_kpi_6 = 'Vivien Greve' 		THEN 1
					WHEN cases.sub_kpi_6 = 'Daniela Kaim'		THEN 1
					WHEN cases.sub_kpi_6 = 'Salim Abdoulaye'	THEN 1
					WHEN cases.sub_kpi_6 = 'Lennart Bär'		THEN 0.5
					WHEN cases.sub_kpi_6 = 'Karla Sorgato'		THEN 0.5
					WHEN cases.sub_kpi_6 = 'CM Support2'		THEN 0.5
					WHEN cases.sub_kpi_6 = 'Marleen Dreyer'		THEN 0.5
																ELSE 0 END 	AS FTE
			
			, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.value ELSE 0 END)	AS inbound_calls
			, CAST 	('20' 	AS numeric)					AS target_inbound_calls
			, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.call_duration ELSE 0 END) as call_duration
	
			, SUM(CASE WHEN cases.kpi = 'Closed Cases' 	THEN cases.value ELSE 0 END) 	AS cases_closed
			, CAST 	('90'	AS numeric)					AS target_cases_closed
	
			, svl_final.svl_share_within_24h 			AS svl_share_within_24h
			, svl_final.cases							AS svl_casesclosed
			, svl_final.svl_cases_within_24h			AS svl_cases_within_24h
			, svl_final.case_reason
			, CAST 	('0.95'	AS numeric)					AS targert_svl
	
	
	FROM 
	
		( SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
				, MIN 	(n.call_start_date_time__c::date) 					AS date
				, CAST 	('-' 			AS varchar) 						AS locale
				, n.e164callednumber__c										AS origin -- NEW
				, CAST 	('B2B'			AS varchar)							AS type
				, CAST	('Inbound Calls'AS varchar)							AS kpi
				, CAST 	('Count'		AS varchar)							AS sub_kpi_1
				, n.callconnectedcheckbox__c 								AS sub_kpi_2
				, n.wrapup_string_1__c										AS sub_kpi_3
				, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
						WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
						WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
						ELSE 'unknown' END 									AS sub_kpi_4
				, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
						WHEN n.account__c 				IS NOT NULL 	THEN a.name
						WHEN n.lead__c 					IS NOT NULL 	THEN l.name
						ELSE 'unknown' END 									AS sub_kpi_5
				, u.name													AS sub_kpi_6
				, n.callringseconds__c              						AS sub_kpi_7 
				, n.calldurationseconds__c as call_duration
				, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
						WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
						WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
						ELSE 'unknown' END 									AS sub_kpi_8
				, n.number_not_in_salesforce__c								AS sub_kpi_9
				, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
				
				, COUNT (*) as value
	--			, *
				
	FROM 		salesforce.natterbox_call_reporting_object__c 	n
	LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
	LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
	LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
	LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid
	
	WHERE 		calldirection__c = 'Inbound'
				AND e164callednumber__c IN ('493030807264', '41435084849')
				-- AND n.call_start_date_time__c::date = 'YESTERDAY'
				AND ((n.callringseconds__c < 5 AND n.callconnectedcheckbox__c IS FALSE) IS FALSE)
				
	GROUP BY 	n.call_start_date_time__c::date
				, n.e164callednumber__c
				, n.callconnectedcheckbox__c
				, n.wrapup_string_1__c
				, n.relatedcontact__c
				, n.account__c
				, n.callringseconds__c
				, n.calldurationseconds__c
				, n.lead__c
				, co.type__c
				, a.type__c
				, co.name
				, a.name
				, l.name
				, u.name
				, n.number_not_in_salesforce__c) AS cases

LEFT JOIN 
		(
			SELECT 	
					cw
					, MIN(svl_details.opened_date) 				AS MinDate
					, agent
					, COUNT(*)									AS cases
					, SUM(svl_details.svl_within_24h) 			AS svl_cases_within_24h
					, ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4)	AS svl_share_within_24h
					, svl_details.case_reason
			
			FROM
					(
					SELECT 	TO_CHAR (svl.date1_opened, 'YYYY-IW')	AS cw
							, svl.date1_opened::date 				AS opened_date
							, svl.event_user						AS agent
							, svl.case_id 							AS case_id
							, svl.case_number						AS case_number
							, svl.type 								AS svl_type
							, svl.closed 							AS svl_has_closeddate
							, svl.case_isclosed						AS case_stil_closed
							, svl.case_type							AS case_type
							, svl.case_origin						AS case_origin
							, svl.case_reason						AS case_reason
							, svl.case_status						AS case_status	
							, svl.svl_customer						AS svl_customer
							
					-- IMPROVEMENT: maybe the following should be included in the SVL function!
							, CASE WHEN svl.svl_customer/60 <= 24 THEN 1 ELSE 0 END AS svl_within_24h
							
							, CASE WHEN svl.svl_customer = 0 		AND svl.type = 'Reopened Case' 					THEN 1 ELSE 0 END AS exclude_reopened_closed_due_mail 	-- exclude when 1
							, CASE WHEN svl.type = 'New Case' 		AND svl.case_origin = 'direct email outbound' 	THEN 1 ELSE 0 END AS exclude_direct_email_outbound	 	-- exclude when 1
							, CASE WHEN svl.type = 'Damage' 		OR svl.case_reason = '%Damage%'					THEN 1 ELSE 0 END AS exclude_damage_cases				-- exclude when 1
							, CASE WHEN svl.case_reason = 'Opportunity - Onboarding'								THEN 1 ELSE 0 END AS exclude_onboarding_cases			-- exclude when 1
							, CASE WHEN svl.case_status = 'Closed' AND svl.case_isclosed IS FALSE					THEN 1 ELSE 0 END AS exclude_created_within_clean_up	-- exclude when 1
							, CASE WHEN svl.case_reason LIKE 'Customer - Retention%'					            THEN 1 ELSE 0 END AS exclude_retention	                -- exclude when 1
					
					FROM 	bi.cm_cases_service_level 	svl
					WHERE 	NOT svl.type 	= '# Reopened'
							AND svl.date1_opened::date >= '2019-01-01'
					
					) AS svl_details
			
			 WHERE 
			 		svl_details.exclude_reopened_closed_due_mail 		= 0
					AND svl_details.exclude_direct_email_outbound 		= 0
					AND svl_details.exclude_damage_cases 				= 0
					AND svl_details.exclude_onboarding_cases 			= 0
					AND svl_details.exclude_created_within_clean_up 	= 0
					AND svl_details.exclude_retention                   = 0
			
			GROUP BY 	
						cw
						, agent
						, svl_details.case_reason
						
						
			) AS svl_final  ON (svl_final.cw = cases.date_part AND svl_final.agent = cases.sub_kpi_6)

WHERE cases.date 	>= '2019-06-01'

GROUP BY 	cases.date_part 				
			, cases.sub_kpi_6	
			, svl_final.svl_share_within_24h
			, svl_final.cases							
			, svl_final.svl_cases_within_24h
			, svl_final.case_reason
			
) AS performance

ORDER BY

	cw desc,
	agent


