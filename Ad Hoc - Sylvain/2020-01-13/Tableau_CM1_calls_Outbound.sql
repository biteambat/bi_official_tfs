



SELECT 		

	'Outbound Calls' as type,
	n.sfid,
	LEFT(n.createddate::text, 10)::date as date_trunc,
	n.createddate,
	n.ownerid,
	u.name,
	'' as sub_kpi_1,
	'' as sub_kpi_2,
	n.wrapup_string_1__c as sub_kpi_3,
	n."wrapup_string_2__c" as sub_kpi_4,
	n.calldurationseconds__c as call_duration,
	n.calltotaltalkseconds__c as call_talk,
	n.callringseconds__c as call_ring
			
FROM 		

	salesforce.natterbox_call_reporting_object__c 	n
	
LEFT JOIN	salesforce.user					u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact				co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account				a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead					l 		ON n.lead__c			= l.sfid

WHERE 		

	calldirection__c = 'Outbound'
	AND n.department__c LIKE 'CM'
	AND co.type__c NOT LIKE 'customer-b2c'
			
GROUP BY 	
	
	n.sfid,
	LEFT(n.createddate::text, 10),
	n.createddate,
	n.ownerid,
	u.name,
	n.wrapup_string_1__c,
	n."wrapup_string_2__c",
	n.calldurationseconds__c,
	n.calltotaltalkseconds__c,
	n.callringseconds__c
















