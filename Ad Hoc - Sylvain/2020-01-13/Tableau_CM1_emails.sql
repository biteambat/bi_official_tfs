



SELECT

	'email' as type,
	cases.sfid,
	LEFT(cases.createddate::text, 10)::date as date_trunc,
	cases.createddate,
	cases.ownerid,
	u.name,
	'' as sub_kpi_1,
	'' as sub_kpi_2,
	cases.origin as sub_kpi_3,
	cases."status" as sub_kpi_4,
	'' as call_duration,
	'' as call_talk,
	'' as call_ring


FROM

	salesforce.case cases
	
LEFT JOIN

	salesforce.user u
	
ON

	cases.ownerid = u.sfid
	
WHERE 

	cases.ownerid IS NOT NULL
	AND u.name IS NOT NULL
	AND cases.origin = 'B2B customer - direct email inbound'
	AND cases.test__c IS FALSE
	AND (cases.customer_type__c = 'customer-b2b' OR LOWER(cases.type) LIKE '%b2b%')
	
	

	
	