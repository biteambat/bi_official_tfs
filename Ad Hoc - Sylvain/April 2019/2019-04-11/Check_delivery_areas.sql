
SELECT

	sfid,
	o.delivery_area__c

FROM

	salesforce.order o
	
WHERE

	o.opportunityid LIKE '%0060J00000uyLtf%'
	AND o."status" IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'CANCELLED CUSTOMER', 'PENDING TO START')