


SELECT


	o.sfid,
	o.name,
	o.test__c,
	o.stagename,
	o.status__c


FROM

	salesforce.opportunity o
	
WHERE

	(LOWER(o.name) LIKE '%claude%'
	OR LOWER(o.name) LIKE '%test%'
	OR LOWER(o.contact_name__c) LIKE '%test%'
	OR LOWER(o.name) LIKE '%tiger%'
	OR LOWER(o.name) LIKE '%bat%')
	AND o.test__c IS FALSE