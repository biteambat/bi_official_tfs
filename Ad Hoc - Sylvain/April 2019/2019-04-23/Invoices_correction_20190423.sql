



SELECT

	o.name as name_first_invoice,
	o.opportunity__c,
	o.createddate as date_first_invoice,
	o.sfid as first_invoice,
	o.amount__c as first_amount,
	oo.parent_invoice__c,
	oo.name as name_second_invoice,
	oo.createddate as date_second_invoice,
	oo.sfid as second_invoice,
	oo.amount__c as second_amount
	



FROM

	salesforce.invoice__c o
	
LEFT JOIN

	salesforce.invoice__c oo
	
ON

	o.sfid = oo.parent_invoice__c
	
WHERE

	o.opportunity__c IS NOT NULL
	AND (oo.parent_invoice__c = 'a000J00000xsagdQAA' OR o.sfid = 'a000J00000xsagdQAA')
	
GROUP BY

	o.name,
	o.opportunity__c,
	o.createddate,
	o.sfid,
	o.amount__c,
	oo.parent_invoice__c,
	oo.name,
	oo.createddate,
	oo.sfid,
	oo.amount__c 