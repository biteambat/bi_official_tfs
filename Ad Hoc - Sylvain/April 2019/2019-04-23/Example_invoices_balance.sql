


SELECT

	id,
	amount__c,
	name,
	sfid,
	issued__c,
	type__c,
	parent_invoice__c,
	status__c
	
	
	
FROM

	salesforce.invoice__c
	
WHERE

	(sfid = 'a000J00000yuEyUQAU'
	OR parent_invoice__c = 'a000J00000yuEyUQAU'
	OR sfid = 'a000J000010AYKrQAO'	
	OR parent_invoice__c = 'a000J000010AYKrQAO'
	OR sfid = 'a000J000010AYLmQAO'
	OR parent_invoice__c = 'a000J000010AYLmQAO'
	OR sfid = 'a000J00000ysCQ1QAM'
	OR parent_invoice__c = 'a000J00000ysCQ1QAM'
	)
	
ORDER BY

	issued__c asc