(SELECT
	
	
		t1.*,
			
		t2.monthly_partner_costs__c
	
	FROM
		
		(SELECT
		
			TO_CHAR(t0.effectivedate, 'YYYY-MM') as year_month,
			MIN(t0.effectivedate) as mindate,
			LEFT(t0.locale__c, 2) as locale,
			t0.locale__c as languages,
			t0.delivery_area__c as city,
			'B2B' as type,
			'Operational Costs' as kpi,
			'Partner' as sub_kpi_1,
			o.company_name__c as sub_kpi_2,
			ooo.name as sub_kpi_3,
			t0.opportunityid as sub_kpi_4,
			'-' as sub_kpi_5,
			SUM(t0.order_duration__c) as hours,
			o.pph_partner,
			SUM(t0.order_duration__c*o.pph_partner) as value
			
		
		FROM
		
			bi.list_company_cleaners o
			
		LEFT JOIN
		
			salesforce.order t0
			
		ON
		
			o.sfid = t0.professional__c
		
		LEFT JOIN

			salesforce.opportunity ooo
	
		ON

			t0.opportunityid = ooo.sfid
			
		WHERE
		
			o.category = 'Partner'
			AND TO_CHAR(t0.effectivedate, 'YYYY-MM') IS NOT NULL
			AND t0.effectivedate <= (current_date + 30)
			AND t0."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND t0."type" = 'cleaning-b2b'
			AND LOWER(o.name) NOT LIKE '%test%'
			AND LOWER(o.company_name__c) NOT LIKE '%test%'
			
		GROUP BY
		
			year_month,
			locale,
			languages,
		
			t0.delivery_area__c,
			sub_kpi_2,
			sub_kpi_3,
			sub_kpi_4,
			o.pph_partner
			
		ORDER BY
		
			year_month desc) as t1
			
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON
	
		t1.sub_kpi_4 = t2.sfid
		
WHERE

	t1.year_month = '2019-02'
	AND t1.sub_kpi_2 LIKE 'BAT Business Services GmbH')
	
	
	