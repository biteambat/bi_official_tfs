


SELECT

	o.sfid,
	o."status",
	o.professional__c,
	a.parentid,
	aa.name
	

FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.account a 
	
ON

	o.professional__c = a.sfid
	
LEFT JOIN

	salesforce.account aa
	
ON

	a.parentid = aa.sfid
	
WHERE

	a.parentid = '0010J00001wI3dpQAC'
	AND o.effectivedate < current_date