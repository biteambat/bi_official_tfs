(SELECT	
		
		TO_CHAR(t1.date,'YYYY-MM') as date_part,
		MIN(t1.date) as date,
		t1.locale,
		t1.languages as languages,
		t1.city as city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		
		t1.name_first_invoice,
		t1.opportunity__c,
	
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c,
		-- t1.name_second_invoice,
		-- MAX(t1.date_second_invoice) as date_last_invoice,
		SUM(t1.first_amount) as sum_first_amount,
		SUM(t1.second_amount) as sum_second_amount,
		COUNT(t1.first_invoice) as number_invoices,
		CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
		     ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
			  END as final_amount_invoiced
		
	FROM
	
		(SELECT

			-- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
			MIN(o.issued__c::date) as date,
			LEFT(o.locale__c,2) as locale,
			o.locale__c as languages,
			ooo.delivery_area__c as city,
			CAST('B2B' as varchar) as type,
			CAST('Revenue Balanced' as varchar) as kpi,
			CAST('Netto' as varchar) as sub_kpi_1,
			CAST('Monthly' as varchar) as sub_kpi_2,
			CAST('-' as varchar) as sub_kpi_3,
			CAST('-' as varchar) as sub_kpi_4,
			CAST('-' as varchar) as sub_kpi_5,
			o.sfid as first_invoice,
			o.name as name_first_invoice,
			o.opportunity__c,
			o.createddate as date_first_invoice,
			o.amount__c as first_amount,
			oo.original_invoice__c,
			oo.name as name_second_invoice,
			oo.createddate as date_second_invoice,
			oo.sfid as second_invoice,
			oo.amount__c as second_amount	
			
		FROM
			
			salesforce.invoice__c o
			
		LEFT JOIN
			
			salesforce.invoice__c oo
			
		ON
			
			o.sfid = oo.original_invoice__c
			
		LEFT JOIN
			
			salesforce.order ooo
			
		ON 
			
			o.opportunity__c = ooo.opportunityid
			
		WHERE
			
			o.opportunity__c IS NOT NULL
			AND o.test__c IS FALSE
			AND o.issued__c IS NOT NULL
			AND o.original_invoice__c IS NULL
			-- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
			
		GROUP BY
			
			-- TO_CHAR(o.issued__c,'YYYY-MM'),
			LEFT(o.locale__c,2),
			o.locale__c,
			ooo.delivery_area__c,
			o.name,
			o.opportunity__c,
			o.issued__c,
			o.createddate,
			o.sfid,
			o.amount__c,
			oo.original_invoice__c,
			oo.name,
			oo.createddate,
			oo.sfid,
			oo.amount__c
			
		ORDER BY
		
			date desc) as t1
			
			
	GROUP BY
	
		TO_CHAR(t1.date,'YYYY-MM'),
		t1.locale,
		t1.languages,
		t1.city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		t1.name_first_invoice,
		t1.opportunity__c,
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c) 