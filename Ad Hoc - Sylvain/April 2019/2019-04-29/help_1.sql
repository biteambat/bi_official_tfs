	

SELECT

	t1.sfid,
	t1.name,
	t1.amount__c,
	t1.original_invoice__c,
	t1.issued__c,
	t2.issued__c as first_issued,
	t2.amount__c as first_amount,
	t1.amount__c - t2.amount__c as difference
	
	
FROM	
	
	(SELECT
	
		*
		-- o.locale__c,
		-- SUM(o.amount__c) as amount
	
	
	FROM
	
		salesforce.invoice__c o
		
	WHERE
	
		o.issued__c < '2019-02-28'
		AND o.issued__c > '2019-02-01'
		AND o.opportunity__c IS NOT NULL
		AND o.name NOT LIKE '%S'
		AND o.original_invoice__c IS NOT NULL
		) as t1
		
LEFT JOIN

	salesforce.invoice__c t2
	
ON

	t1.original_invoice__c = t2.sfid
	
-- WHERE

--	t2.issued__c = '2019-02-28'
