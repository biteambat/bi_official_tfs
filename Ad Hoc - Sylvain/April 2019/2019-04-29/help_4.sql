

SELECT

	o.sfid,
	o.opportunity__c,
	o.name,
	o.amount__c/1.19,
	o.parent_invoice__c,
	o.issued__c
	
FROM

	salesforce.invoice__c o
	
WHERE

	o.original_invoice__c = 'a000J00000zVpO9QAK'
	OR o.sfid = 'a000J00000zVpO9QAK'