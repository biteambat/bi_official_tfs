

-- ==================================================================================== 
-- DONE - Oppportunities with contracts ACCEPTED,SIGNED this month but orders in the past 
-- ==================================================================================== 

SELECT 	opportunityid										opportunity_id
		, opp.name											opportunity
		, o.locale__c
		, o.test__c 
		, co.name											contract
		, co.sfid											contract_id
		, co.status__c										contract_status
		, co.when_accepted__c
		, co.grand_total__c									contract_grand_total
		, co.start__c										contract_start

FROM salesforce.order o
LEFT JOIN salesforce.opportunity opp 	ON o.opportunityid 	= opp.sfid
LEFT JOIN salesforce.contract__c co 	ON o.opportunityid 	= co.opportunity__c

WHERE 	o.status 											IN ('INVOICED', 'PENDING TO START', 'PENDING ALLOCATION', 'FULFILLED', 'NOSHOW CUSTOMER', 'CANCELLED CUSTOMER') 
		-- AND EXTRACT(YEAR FROM o.order_start__c::date ) 		= EXTRACT(YEAR FROM (CURRENT_DATE- interval '1 month'))
		-- AND EXTRACT(MONTH FROM o.order_start__c::date ) 	= EXTRACT(MONTH FROM (CURRENT_DATE- interval '1 month'))
		AND o.type 											= 'cleaning-b2b' 
		AND o.locale__c 									IN ('de-en', 'de-de') 
		AND o.test__c  										= FALSE
		AND co.test__c 										= FALSE 
		AND EXTRACT(YEAR FROM co.when_accepted__c::date) 	= EXTRACT(YEAR FROM (CURRENT_DATE - 5))
		AND EXTRACT(MONTH FROM co.when_accepted__c::date) 	= EXTRACT(MONTH FROM (CURRENT_DATE - 5))
		AND co.status__c 	IN 	('ACCEPTED', 'SIGNED') 
		
		AND o.order_start__c < co.when_accepted__c


GROUP BY opportunityId
		, opp.name
		, o.locale__c 
		, o.test__c
		, co.name
		, co.sfid
		, co.status__c
		, co.when_accepted__c
		, co.grand_total__c	
		, co.start__c		

ORDER BY  co.when_accepted__c
		, co.start__c			