




SELECT

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
	MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
	a.sfid,
	a.name,
	a.company_name__c,
	a.delivery_areas__c,
	a.type__c,
	a.status__c,
	a.role__c,
	COUNT(o.suggested_partner__c) as offers_received,
	SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted


FROM

	salesforce.account a


LEFT JOIN

	salesforce.partner_offer_partner__c o
		
ON

	a.sfid = o.suggested_partner__c 
	
WHERE

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
	AND a.test__c IS FALSE
	
GROUP BY

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
	a.sfid,
	a.name,
	a.company_name__c,
	a.delivery_areas__c,
	a.type__c,
	a.status__c,
	a.role__c

ORDER BY

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
	a.name asc