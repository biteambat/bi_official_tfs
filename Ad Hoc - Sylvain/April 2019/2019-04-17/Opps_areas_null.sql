

SELECT

	*


FROM
	
	salesforce.opportunity o
	
WHERE

	o.delivery_area__c IS NULL
	AND o.stagename IN ('WON')
	AND o.lost_reason__c IS NULL
	AND o.status__c NOT LIKE 'RESIGNED'
	AND o.status__c NOT LIKE 'CANCELLED'



