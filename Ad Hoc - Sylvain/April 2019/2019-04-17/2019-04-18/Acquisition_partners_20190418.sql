
SELECT

	TO_CHAR(a.createddate, 'YYYY-MM') as partner_conversion_month,
	a.sfid,
	a.name,
	a.locale__c,
	a.delivery_areas__c,
	a.type__c,
	a.role__c,
	l.sfid as sfid_lead,
	l.acquisition_channel__c,
	l.acquisition_channel_ref__c,
	l.acquisition_channel_params__c

FROM

	salesforce.account a
	
LEFT JOIN

	salesforce.lead l
	
ON

	a.sfid = l.convertedaccountid
	
WHERE

	a.type__c = 'partner'
	AND a.test__c IS FALSE
	AND a.locale__c LIKE 'de%'
	
ORDER BY

	partner_conversion_month desc