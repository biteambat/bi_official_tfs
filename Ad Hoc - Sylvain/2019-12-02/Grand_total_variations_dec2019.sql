			

SELECT

	table3.country,
	SUM(table3.var_jan_feb) as var_jan_feb,
	SUM(table3.var_feb_mar) as var_feb_mar,
	SUM(table3.var_mar_apr) as var_mar_apr,
	SUM(table3.var_apr_may) as var_apr_may,
	SUM(table3.var_may_jun) as var_may_jun,
	SUM(table3.var_jun_jul) as var_jun_jul,
	SUM(table3.var_jul_aug) as var_jul_aug,
	SUM(table3.var_aug_sept) as var_aug_sept,
	SUM(table3.var_sept_oct) as var_sept_oct,
	SUM(table3.var_oct_nov2019) as var_oct_nov2019

FROM		
	
	(SELECT
	
		table2.country,
		table2.opportunity__c,
		SUM(contract_jan) as contract_jan,
		CASE WHEN SUM(contract_jan) <> 0 AND SUM(contract_feb) <> 0
		     THEN SUM(contract_feb) - SUM(contract_jan)
			  ELSE 0
			  END as var_jan_feb,
		SUM(contract_feb) as contract_feb,
		CASE WHEN SUM(contract_feb) <> 0 AND SUM(contract_mar) <> 0
		     THEN SUM(contract_mar) - SUM(contract_feb)
			  ELSE 0
			  END as var_feb_mar,
		SUM(contract_mar) as contract_mar,
		CASE WHEN SUM(contract_mar) <> 0 AND SUM(contract_apr) <> 0
		     THEN SUM(contract_apr) - SUM(contract_mar)
			  ELSE 0
			  END as var_mar_apr,
		SUM(contract_apr) as contract_apr,
		CASE WHEN SUM(contract_apr) <> 0 AND SUM(contract_may) <> 0
		     THEN SUM(contract_may) - SUM(contract_apr)
			  ELSE 0
			  END as var_apr_may,
		SUM(contract_may) as contract_may,
		CASE WHEN SUM(contract_may) <> 0 AND SUM(contract_june) <> 0
		     THEN SUM(contract_june) - SUM(contract_may)
			  ELSE 0
			  END as var_may_jun,
		SUM(contract_june) as contract_june,
		CASE WHEN SUM(contract_june) <> 0 AND SUM(contract_july) <> 0
		     THEN SUM(contract_july) - SUM(contract_june)
			  ELSE 0
			  END as var_jun_jul,
		SUM(contract_july) as contract_july,
		CASE WHEN SUM(contract_july) <> 0 AND SUM(contract_aug) <> 0
		     THEN SUM(contract_aug) - SUM(contract_july)
			  ELSE 0
			  END as var_jul_aug,
		SUM(contract_aug) as contract_aug,
		CASE WHEN SUM(contract_aug) <> 0 AND SUM(contract_sept) <> 0
		     THEN SUM(contract_sept) - SUM(contract_aug)
			  ELSE 0 
			  END as var_aug_sept,
		SUM(contract_sept) as contract_sept,
		CASE WHEN SUM(contract_sept) <> 0 AND SUM(contract_oct) <> 0
		     THEN SUM(contract_oct) - SUM(contract_sept)
			  ELSE 0
			  END as var_sept_oct,
		SUM(contract_oct) as contract_oct,
		CASE WHEN SUM(contract_oct) <> 0 AND SUM(contract_nov) <> 0
		     THEN SUM(contract_nov) - SUM(contract_oct)
			  ELSE 0
			  END as var_oct_nov2019,
		SUM(contract_nov2019) as contract_nov2019
		
	FROM	
		
		(SELECT
			
			table1.country,
			table1.opportunity__c,
			table1.issued__c,
			CASE WHEN table1.issued__c = '2018-11-30' THEN table1.grand_total_contract ELSE 0 END as contract_nov,
			CASE WHEN table1.issued__c = '2018-12-31' THEN table1.grand_total_contract ELSE 0 END as contract_dec,
			CASE WHEN table1.issued__c = '2019-01-31' THEN table1.grand_total_contract ELSE 0 END as contract_jan,
			CASE WHEN table1.issued__c = '2019-02-28' THEN table1.grand_total_contract ELSE 0 END as contract_feb,
			CASE WHEN table1.issued__c = '2019-03-31' THEN table1.grand_total_contract ELSE 0 END as contract_mar,
			CASE WHEN table1.issued__c = '2019-04-30' THEN table1.grand_total_contract ELSE 0 END as contract_apr,
			CASE WHEN table1.issued__c = '2019-05-31' THEN table1.grand_total_contract ELSE 0 END as contract_may,
			CASE WHEN table1.issued__c = '2019-06-30' THEN table1.grand_total_contract ELSE 0 END as contract_june,
			CASE WHEN table1.issued__c = '2019-07-31' THEN table1.grand_total_contract ELSE 0 END as contract_july,
			CASE WHEN table1.issued__c = '2019-08-31' THEN table1.grand_total_contract ELSE 0 END as contract_aug,
			CASE WHEN table1.issued__c = '2019-09-30' THEN table1.grand_total_contract ELSE 0 END as contract_sept,
			CASE WHEN table1.issued__c = '2019-10-31' THEN table1.grand_total_contract ELSE 0 END as contract_oct,
			CASE WHEN table1.issued__c = '2019-11-30' THEN table1.grand_total_contract ELSE 0 END as contract_nov2019
		
		
		FROM	
			
			(SELECT
			
				i.sfid,
				i.name,
				i.service_type__c,
				i.amount__c,
				i.balance__c,
				i.amount_paid__c,
				i.opportunity__c,
				o.name as opp_name,
				o.delivery_area__c,
				LEFT(o.locale__c, 2) as country,
				o.locale__c,
				o.stagename,
				o.status__c,
				o.grand_total__c as opp_grand_total,
				o.plan_pph__c,
				i.issued__c,
				i.contract__c,
				cont.status__c,
				cont.service_type__c,
				cont.grand_total__c as grand_total_contract
			
			FROM
			
				salesforce.invoice__c i
				
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				i.opportunity__c = o.sfid
				
			LEFT JOIN
			
				salesforce.contract__c cont
				
			ON
			
				i.contract__c = cont.sfid
				
			WHERE
			
				i.opportunity__c IS NOT NULL
				AND i.test__c IS FALSE
				AND i.issued__c >= '2019-01-01'
				AND i.service_type__c = 'maintenance cleaning'
				AND LEFT(o.locale__c, 2) = 'de'
				AND i.original_invoice__c IS NULL
				AND o.test__c IS FALSE
				AND cont.test__c IS FALSE
				AND cont.service_type__c = 'maintenance cleaning') as table1) as table2
				
	WHERE
	
		(contract_nov > 0
		OR contract_dec > 0
		OR contract_jan > 0
		OR contract_feb > 0
		OR contract_mar > 0
		OR contract_apr > 0
		OR contract_may > 0
		OR contract_june > 0
		OR contract_july > 0
		OR contract_aug > 0
		OR contract_sept > 0
		OR contract_oct > 0
		OR contract_nov > 0
		)
				
	GROUP BY
	
		table2.opportunity__c,
		table2.country) as table3
		
GROUP BY

	table3.country
			
			
			
			
