			
		
SELECT

	t3.sfid,
	t3.name,
	t3.delivery_areas__c,
	t3.tot_offers_received,
	t3.pc_accepted_this_month,
	t3.pc_accepted_last_month,
	t3.pc_accepted_last_month_2,
	t3.pc_accepted_this_month*0.6 + t3.pc_accepted_last_month*0.3 + t3.pc_accepted_last_month_2*0.1 as pc_accepted_weighted,
	t3.score_this_month,
	t3.score_last_month,
	t3.score_last_month2,
	t3.score_this_month*0.6 + t3.score_last_month*0.3 + t3.score_last_month2*0.1 as score_weighted
	
FROM	
	
	(SELECT
	
		t2.sfid,
		t2.name,
		t2.delivery_areas__c,
		t2.tot_offers_received,
		t2.pc_accepted_this_month,
		CASE WHEN pc_accepted_this_month = 0 THEN 0
			  WHEN pc_accepted_this_month > 0 AND pc_accepted_this_month <= 0.1 THEN 1
			  WHEN pc_accepted_this_month > 0.1 AND pc_accepted_this_month <= 0.2 THEN 2
			  WHEN pc_accepted_this_month > 0.2 AND pc_accepted_this_month <= 0.3 THEN 3
			  WHEN pc_accepted_this_month > 0.3 AND pc_accepted_this_month <= 0.4 THEN 4
			  WHEN pc_accepted_this_month > 0.4 AND pc_accepted_this_month <= 0.5 THEN 5
			  WHEN pc_accepted_this_month > 0.5 AND pc_accepted_this_month <= 0.6 THEN 6
			  WHEN pc_accepted_this_month > 0.6 AND pc_accepted_this_month <= 0.7 THEN 7
			  WHEN pc_accepted_this_month > 0.7 AND pc_accepted_this_month <= 0.8 THEN 8
			  WHEN pc_accepted_this_month > 0.8 AND pc_accepted_this_month <= 0.9 THEN 9
			  WHEN pc_accepted_this_month > 0.9 AND pc_accepted_this_month <= 1 THEN 10
			  ELSE 11
			  END as score_this_month,
		t2.pc_accepted_last_month,
		CASE WHEN pc_accepted_last_month = 0 THEN 0
			  WHEN pc_accepted_last_month > 0 AND pc_accepted_last_month <= 0.1 THEN 1
			  WHEN pc_accepted_last_month > 0.1 AND pc_accepted_last_month <= 0.2 THEN 2
			  WHEN pc_accepted_last_month > 0.2 AND pc_accepted_last_month <= 0.3 THEN 3
			  WHEN pc_accepted_last_month > 0.3 AND pc_accepted_last_month <= 0.4 THEN 4
			  WHEN pc_accepted_last_month > 0.4 AND pc_accepted_last_month <= 0.5 THEN 5
			  WHEN pc_accepted_last_month > 0.5 AND pc_accepted_last_month <= 0.6 THEN 6
			  WHEN pc_accepted_last_month > 0.6 AND pc_accepted_last_month <= 0.7 THEN 7
			  WHEN pc_accepted_last_month > 0.7 AND pc_accepted_last_month <= 0.8 THEN 8
			  WHEN pc_accepted_last_month > 0.8 AND pc_accepted_last_month <= 0.9 THEN 9
			  WHEN pc_accepted_last_month > 0.9 AND pc_accepted_last_month <= 1 THEN 10
			  ELSE 11
			  END as score_last_month,
		t2.pc_accepted_last_month_2,
		CASE WHEN pc_accepted_last_month_2 = 0 THEN 0
			  WHEN pc_accepted_last_month_2 > 0 AND pc_accepted_last_month_2 <= 0.1 THEN 1
			  WHEN pc_accepted_last_month_2 > 0.1 AND pc_accepted_last_month_2 <= 0.2 THEN 2
			  WHEN pc_accepted_last_month_2 > 0.2 AND pc_accepted_last_month_2 <= 0.3 THEN 3
			  WHEN pc_accepted_last_month_2 > 0.3 AND pc_accepted_last_month_2 <= 0.4 THEN 4
			  WHEN pc_accepted_last_month_2 > 0.4 AND pc_accepted_last_month_2 <= 0.5 THEN 5
			  WHEN pc_accepted_last_month_2 > 0.5 AND pc_accepted_last_month_2 <= 0.6 THEN 6
			  WHEN pc_accepted_last_month_2 > 0.6 AND pc_accepted_last_month_2 <= 0.7 THEN 7
			  WHEN pc_accepted_last_month_2 > 0.7 AND pc_accepted_last_month_2 <= 0.8 THEN 8
			  WHEN pc_accepted_last_month_2 > 0.8 AND pc_accepted_last_month_2 <= 0.9 THEN 9
			  WHEN pc_accepted_last_month_2 > 0.9 AND pc_accepted_last_month_2 <= 1 THEN 10
			  ELSE 11
			  END as score_last_month2
			  
	FROM	
		
		(SELECT	
			
			t1.sfid,
			t1.name,
			t1.delivery_areas__c,
			SUM(CASE WHEN t1.mindate >= (current_date - 90) THEN t1.offers_received ELSE 0 END) as tot_offers_received,
			SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_received ELSE 0 END) as offers_received_this_month,
			SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_accepted ELSE 0 END) as offers_accepted_this_month,
			CASE WHEN SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_received ELSE 0 END) > 0 THEN
				SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_accepted ELSE 0 END)/SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_received ELSE 0 END)
			ELSE 0 
			END  as pc_accepted_this_month,
			
			SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_received ELSE 0 END) as offers_received_last_month,
			SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_accepted ELSE 0 END) as offers_accepted_last_month,
			CASE WHEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_received ELSE 0 END) > 0
			THEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_accepted ELSE 0 END)/SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_received ELSE 0 END)
			ELSE 0
			END  as pc_accepted_last_month,
			
			SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_received ELSE 0 END) as offers_received_last_month_2,
			SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_accepted ELSE 0 END) as offers_accepted_last_month_2,
			CASE WHEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_received ELSE 0 END) > 0
			THEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_accepted ELSE 0 END)/SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_received ELSE 0 END)
			ELSE 0
			END  as pc_accepted_last_month_2
		
		FROM
					
			(SELECT
			
				TO_CHAR(t2.createddate, 'YYYY-MM') as year_month_offer,
				MIN(t2.createddate) as mindate,
				t1.*,
				SUM(CASE WHEN ((t2.suggested_partner1__c = t1.sfid
							  OR t2.suggested_partner2__c = t1.sfid
							  OR t2.suggested_partner3__c = t1.sfid
							  OR t2.suggested_partner4__c = t1.sfid
							  OR t2.suggested_partner5__c = t1.sfid))
					      THEN 1
					      ELSE 0
							END) as offers_received,
				SUM(CASE WHEN (t2.partner__c = t1.sfid AND t2.status__c = 'ACCEPTED') THEN 1 ELSE 0 END) as offers_accepted
			
			FROM	
				
				(SELECT
				
					a.sfid,
					a.name,
					a.company_name__c,
					a.delivery_areas__c,
					a.type__c,
					a.status__c,
					a.role__c
									
				FROM
				
					salesforce.account a
					
				WHERE
				
					a.type__c = 'partner'
					AND a.test__c IS FALSE
					AND a.status__c IN ('ACTIVE', 'BETA')
					AND LEFT(a.locale__c, 2) = 'de'
					AND a.role__c = 'master'
					AND a.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
					AND a.name NOT LIKE '%Handyman Kovacs%'
					AND a.name NOT LIKE '%BAT Business Services GmbH%'
					
				ORDER BY
				
					a.name) as t1
					
			LEFT JOIN
			
				salesforce.partner_offer__c t2
								
			ON
			
				t2.suggested_partner5__c = t1.sfid
				OR t2.suggested_partner4__c = t1.sfid
				OR t2.suggested_partner3__c = t1.sfid
				OR t2.suggested_partner2__c = t1.sfid
				OR t2.suggested_partner1__c = t1.sfid
			
			WHERE
			
				TO_CHAR(t2.createddate, 'YYYY-MM') IS NOT NULL
				
			GROUP BY
			
				TO_CHAR(t2.createddate, 'YYYY-MM'),
				t1.sfid,
				t1.name,
				t1.company_name__c,
				t1.delivery_areas__c,
				t1.type__c,
				t1.status__c,
				t1.role__c
				
			ORDER BY
			
				TO_CHAR(t2.createddate, 'YYYY-MM') desc) as t1
				
		GROUP BY
		
			t1.sfid,
			t1.name,
			t1.delivery_areas__c) as t2) as t3
