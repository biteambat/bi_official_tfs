

SELECT

	o.sfid,
	o.name,
	o.grand_total__c,
	oo.start__c,
	oo.duration__c,
	oo.end__c,
	oo.resignation_date__c,
	oo.confirmed_end__c,
	oo.confirmed_end__c - 266 as anticipation_date,
	MAX(oooo.effectivedate) as last_order,
	oo.end__c - oo.confirmed_end__c as diff_days

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.order oooo
	
ON

	o.sfid = oooo.opportunityid

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('CANCELLED', 'RESIGNED')
	AND oo.status__c IN ('CANCELLED', 'RESIGNED')
	AND oooo.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
	AND oo.end__c IS NOT NULL
	
GROUP BY

	o.sfid,
	o.name,
	o.grand_total__c,
	oo.start__c,
	oo.duration__c,
	oo.end__c,
	oo.resignation_date__c,
	oo.confirmed_end__c,
	oo.confirmed_end__c