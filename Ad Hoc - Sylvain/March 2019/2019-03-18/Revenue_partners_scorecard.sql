					

SELECT

	t10.*,
	CASE WHEN t10.hours_per_cleaner = 0 THEN 0
	     WHEN (t10.hours_per_cleaner > 0 AND t10.hours_per_cleaner <= 4) THEN 1
	     WHEN (t10.hours_per_cleaner > 4 AND t10.hours_per_cleaner <= 8) THEN 2
	     WHEN (t10.hours_per_cleaner > 8 AND t10.hours_per_cleaner <= 12) THEN 3
	     WHEN (t10.hours_per_cleaner > 12 AND t10.hours_per_cleaner <= 16) THEN 4
	     WHEN t10.hours_per_cleaner > 16 THEN 5
	     ELSE 100
	     END as score_hours_cleaners

FROM

	(SELECT
	
		t8.*,
		CASE WHEN t8.active_pro_last_month > 0 THEN
					 t8.hours_executed/t8.active_pro_last_month
			  ELSE 0
			  END as hours_per_cleaner,
		CASE WHEN t8.share_city < (t8.threshold_hours - 0.05) THEN '> 5% under threshold'
		     WHEN t8.share_city > t8.threshold_hours THEN 'Above threshold'
		     ELSE '< 5% under threshold'
		     END as light_city_share,
		CASE WHEN t8.active_pro_last_month = t8.active_pro_last_month2 THEN 'Stagnation # workers'
				  WHEN t8.active_pro_last_month > t8.active_pro_last_month2 THEN 'Increasing # workers'
				  ELSE 'Decreasing # workers'
				  END as trend_active_pros,
		CASE WHEN t8.active_pro_last_month = 0 THEN 0
		     WHEN (t8.active_pro_last_month > 0 AND t8.active_pro_last_month <= 2) THEN 1
		     WHEN (t8.active_pro_last_month > 2 AND t8.active_pro_last_month <= 4) THEN 2
		     WHEN (t8.active_pro_last_month > 4 AND t8.active_pro_last_month <= 6) THEN 3
		     WHEN (t8.active_pro_last_month > 6 AND t8.active_pro_last_month <= 8) THEN 4
		     WHEN t8.active_pro_last_month > 8  THEN 5
		     ELSE 100
		     END as score_active_pro
		     
	FROM	
		
		(SELECT
		
			t5.sfid,
			t5.partner,
			t5.delivery_areas__c,
			t5.revenue_last_month,
			t5.revenue_last_month2,
			CASE WHEN t5.revenue_last_month = t5.revenue_last_month2 THEN 'Stagnation'
				  WHEN t5.revenue_last_month > t5.revenue_last_month2 THEN 'Increasing revenue'
				  ELSE 'Decreasing revenue'
				  END as trend,
			t5.score_revenue,
			t5.gp,
			t5.gpm_last_month,
			t5.score_gpm,
			CASE WHEN t6.hours IS NULL THEN 0 ELSE t6.hours END as total_hours_city,
			CASE WHEN t5.hours_executed IS NULL THEN 0 ELSE t5.hours_executed END as hours_executed,
			
			CASE WHEN t6.hours > 0 THEN 
				  		(CASE WHEN t5.hours_executed IS NULL THEN 0 ELSE t5.hours_executed END)/t6.hours
				  ELSE 0
				  END as share_city,
			
			CASE WHEN t5.delivery_areas__c = 'de-berlin' THEN 0.2
				  WHEN t5.delivery_areas__c = 'de-cologne' THEN 0.35
				  WHEN t5.delivery_areas__c = 'de-dusseldorf' THEN 0.4
				  WHEN t5.delivery_areas__c = 'de-frankfurt' THEN 0.5
				  WHEN t5.delivery_areas__c = 'de-hamburg' THEN 0.35
				  WHEN t5.delivery_areas__c = 'de-hannover' THEN 0.6
				  WHEN t5.delivery_areas__c = 'de-leipzig' THEN 0.5
				  WHEN t5.delivery_areas__c = 'de-munich' THEN 0.35
				  WHEN t5.delivery_areas__c = 'de-stuttgart' THEN 0.5
				  ELSE 1
				  END as threshold_hours,
			CASE WHEN t5.active_pro_last_month IS NULL THEN 0 ELSE t5.active_pro_last_month END active_pro_last_month,
			CASE WHEN t5.active_pro_last_month2 IS NULL THEN 0 ELSE t5.active_pro_last_month2 END active_pro_last_month2
		
		FROM		
			
			(SELECT	
			
				t4.sfid,
				t4.partner,
				t4.delivery_areas__c,
				SUM(t4.revenue_last_month) as revenue_last_month,
				SUM(t4.revenue_last_month2) as revenue_last_month2,
				MAX(t4.score_revenue) as score_revenue,
				MAX(t4.gp) as gp,
				SUM(t4.gpm) as gpm_last_month,
				MAX(CASE WHEN t4.gpm <= 0 THEN 0
					  WHEN t4.gpm > 0 AND t4.gpm <= 0.02 THEN 1
					  WHEN t4.gpm > 0.02 AND t4.gpm <= 0.04 THEN 2
					  WHEN t4.gpm > 0.04 AND t4.gpm <= 0.06 THEN 3
					  WHEN t4.gpm > 0.06 AND t4.gpm <= 0.08 THEN 4
					  WHEN t4.gpm > 0.08 AND t4.gpm <= 0.1 THEN 5
					  WHEN t4.gpm > 0.1 AND t4.gpm <= 0.12 THEN 6
					  WHEN t4.gpm > 0.12 AND t4.gpm <= 0.14 THEN 7
					  WHEN t4.gpm > 0.14 AND t4.gpm <= 0.16 THEN 8
					  WHEN t4.gpm > 0.16 AND t4.gpm <= 0.18 THEN 9
					  WHEN t4.gpm > 0.18 AND t4.gpm <= 0.20 THEN 10
					  WHEN t4.gpm > 0.2 AND t4.gpm <= 0.22 THEN 11
					  WHEN t4.gpm > 0.22 AND t4.gpm <= 0.24 THEN 12
					  WHEN t4.gpm > 0.24 AND t4.gpm <= 0.26 THEN 13
					  WHEN t4.gpm > 0.26 AND t4.gpm <= 0.28 THEN 14
					  WHEN t4.gpm > 0.28 THEN 15
					  ELSE 0
					  END) as score_gpm,
				MIN(t4.hours_executed) as hours_executed,
				MIN(t4.active_pro_last_month) as active_pro_last_month,
				MIN(t4.active_pro_last_month2) as active_pro_last_month2
				
			FROM
				
				(SELECT
				
					t0.sfid,
					t1.partner,
					t1.city as delivery_areas__c,
					CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END as revenue_last_month,
					CASE WHEN t1.revenue_last_month2 IS NULL THEN 0 ELSE t1.revenue_last_month2 END as revenue_last_month2,
					
					SUM(CASE WHEN t9.active_pro_last_month IS NULL THEN 0 ELSE t9.active_pro_last_month END) as active_pro_last_month,
					SUM(CASE WHEN t9.active_pro_last_month2 IS NULL THEN 0 ELSE t9.active_pro_last_month2 END) as active_pro_last_month2,
					
					CASE WHEN t1.revenue_last_month = 0 THEN 0
						  WHEN (t1.revenue_last_month > 0 AND t1.revenue_last_month <= 500) THEN 1
						  WHEN (t1.revenue_last_month > 500 AND t1.revenue_last_month <= 1000) THEN 2
						  WHEN (t1.revenue_last_month > 1000 AND t1.revenue_last_month <= 1500) THEN 3
						  WHEN (t1.revenue_last_month > 1500 AND t1.revenue_last_month <= 2000) THEN 4
						  WHEN (t1.revenue_last_month > 2000 AND t1.revenue_last_month <= 2500) THEN 5
						  WHEN (t1.revenue_last_month > 2500 AND t1.revenue_last_month <= 3000) THEN 6
						  WHEN (t1.revenue_last_month > 3000 AND t1.revenue_last_month <= 3500) THEN 7
						  WHEN (t1.revenue_last_month > 3500 AND t1.revenue_last_month <= 4000) THEN 8
						  WHEN (t1.revenue_last_month > 4000 AND t1.revenue_last_month <= 4500) THEN 9
						  WHEN (t1.revenue_last_month > 4500 AND t1.revenue_last_month <= 5000) THEN 10
						  WHEN (t1.revenue_last_month > 5000 AND t1.revenue_last_month <= 5500) THEN 11
						  WHEN (t1.revenue_last_month > 5500 AND t1.revenue_last_month <= 6000) THEN 12
						  WHEN (t1.revenue_last_month > 6000 AND t1.revenue_last_month <= 6500) THEN 13
						  WHEN (t1.revenue_last_month > 6500 AND t1.revenue_last_month <= 7000) THEN 14
						  WHEN (t1.revenue_last_month > 7000 AND t1.revenue_last_month <= 7500) THEN 15
						  WHEN (t1.revenue_last_month > 7500 AND t1.revenue_last_month <= 8000) THEN 16
						  WHEN (t1.revenue_last_month > 8000 AND t1.revenue_last_month <= 8500) THEN 17
						  WHEN (t1.revenue_last_month > 8500 AND t1.revenue_last_month <= 9000) THEN 18
						  WHEN (t1.revenue_last_month > 9000 AND t1.revenue_last_month <= 9500) THEN 19
						  WHEN (t1.revenue_last_month > 9500) THEN 20
						  ELSE 0
						  END as score_revenue,
					CASE WHEN t2.cost_supply IS NULL THEN 0 ELSE t2.cost_supply END as cost_supply,
					CASE WHEN t3.operational_costs IS NULL THEN 0 ELSE t3.operational_costs END as operational_costs,
					((CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END) - (CASE WHEN t2.cost_supply IS NULL THEN 0 ELSE t2.cost_supply END) - CASE WHEN t3.operational_costs IS NULL THEN 0 ELSE t3.operational_costs END) as gp,
					
					CASE WHEN (CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END) <> 0 THEN 
				   					((CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END) - (CASE WHEN t2.cost_supply IS NULL THEN 0 ELSE t2.cost_supply END) - CASE WHEN t3.operational_costs IS NULL THEN 0 ELSE t3.operational_costs END)/CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END
				   					ELSE 0
				   					END as gpm,
				   SUM(t7.hours_executed) as hours_executed
					
				FROM	
				
					(SELECT
				
					a.sfid,
					a.delivery_areas__c,
					a.name as partner
				
				FROM
				
					salesforce.account a 
						
				WHERE
				
					a.test__c IS FALSE
					AND a.role__c = 'master') as t0
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.date_part = LEFT((current_date - 30)::text, 7) THEN o.value ELSE 0 END) as revenue_last_month,
						SUM(CASE WHEN o.date_part = LEFT((current_date - 60)::text, 7) THEN o.value ELSE 0 END) as revenue_last_month2
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 60)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t1
						
				ON
				
					t0.partner = t1.partner
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.city,
						o.sub_kpi_2 as partner,
						SUM(CASE WHEN o.value IS NULL THEN 0 ELSE o.value END) as cost_supply 
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t2
						
				ON
				
					t0.partner = t2.partner	
					AND t1.city = t2.city
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.value IS NULL THEN 0 ELSE o.value END) as operational_costs 
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t3
						
				ON
				
					t0.partner = t3.partner	
					AND t1.city = t3.city
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.value IS NULL THEN 0 ELSE o.value END) as hours_executed 
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t7
						
				ON
				
					t0.partner = t7.partner	
					AND t1.city = t7.city
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.date_part = LEFT((current_date - 30)::text, 7) THEN o.value ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN o.date_part = LEFT((current_date - 60)::text, 7) THEN o.value ELSE 0 END) as active_pro_last_month2
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 60)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t9
						
				ON
				
					t0.partner = t9.partner	
					AND t1.city = t9.city
				
				GROUP BY
				
					t0.sfid,
					t1.partner,
					t1.city,
					t1.revenue_last_month,
					t1.revenue_last_month2,
					t2.cost_supply,
					t3.operational_costs		
					
				ORDER BY
				
					t1.partner) as t4
					
			LEFT JOIN
			
				(SELECT
		
					LEFT((o.effectivedate - 30)::text, 7),
					o.delivery_area__c,
					SUM(o.order_duration__c) as hours
					
					
				FROM
				
					salesforce."order" o
					
				WHERE
				
					o."type" = 'cleaning-b2b'
					AND LEFT((o.effectivedate - 30)::text, 7)  = LEFT((current_date - 30)::text, 7)
					AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
					
				GROUP BY
				
					LEFT((o.effectivedate - 30)::text, 7),
					o.delivery_area__c) as t6
					
			ON 
			
				t4.delivery_areas__c = t6.delivery_area__c
					
			GROUP BY
			
				t4.sfid,
				t4.partner,
				t4.delivery_areas__c
				
			ORDER BY
			
				t4.partner asc) as t5
						
		LEFT JOIN
		
			(SELECT
		
				LEFT((o.effectivedate - 30)::text, 7),
				o.delivery_area__c,
				SUM(o.order_duration__c) as hours
				
				
			FROM
			
				salesforce."order" o
				
			WHERE
			
				o."type" = 'cleaning-b2b'
				AND LEFT((o.effectivedate - 30)::text, 7)  = LEFT((current_date - 30)::text, 7)
				AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
				
			GROUP BY
			
				LEFT((o.effectivedate - 30)::text, 7),
				o.delivery_area__c) as t6	
				
		ON
			
			t5.delivery_areas__c = t6.delivery_area__c	 
			
		WHERE
		
			t5.partner IS NOT NULL
			
		ORDER BY
		
			t5.partner) as t8) as t10
					
					
				
				
				
				
				
