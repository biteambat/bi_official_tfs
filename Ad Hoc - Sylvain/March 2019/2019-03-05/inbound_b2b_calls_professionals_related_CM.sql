



SELECT

	TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
	COUNT(DISTINCT o.sfid) as number_calls

FROM


	salesforce.natterbox_call_reporting_object__c o
	
LEFT JOIN

	salesforce.contact oo
	
ON

	o.relatedcontact__c = oo.sfid

WHERE 

	o.calldirection__c = 'Inbound'
   AND o.e164callednumber__c IN ('493030807264', '41435084849', '493030807403') -- CM only
   AND o.e164callednumber__c IN ('493030807403') -- for B2B only
   AND o.account__c IS NOT NULL -- professional related
   
   -- , CASE WHEN n.e164callednumber__c = '493030807403' THEN 'B2C' ELSE 'B2B' END AS call_type
   
GROUP BY

	TO_CHAR(o.createddate , 'YYYY-MM')
	
ORDER BY

	TO_CHAR(o.createddate , 'YYYY-MM') desc
   
   
   
