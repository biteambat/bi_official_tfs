
SELECT

	orders_b2b.year_month,
	orders.orders_b2b as orders_b2b,
	orders.orders_b2c as orders_b2c

FROM

	(SELECT
	
		TO_CHAR(o.effectivedate , 'YYYY-MM') as year_month,
		SUM(CASE WHEN o."type" = 'cleaning-b2b' THEN 1 ELSE 0 END) as orders_b2b,
		SUM(CASE WHEN o."type" = 'cleaning-b2c' THEN 1 ELSE 0 END) as orders_b2c,
		COUNT(DISTINCT o.sfid) as number_orders
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.test__c IS FALSE
		AND o.effectivedate >= '2018-01-01'
		AND o."status" IN ('PENDING TO START', 'FULFILLED', 'INVOICED', 'NOSHOW CUSTOMER')
		
	GROUP BY
	
		TO_CHAR(o.effectivedate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.effectivedate , 'YYYY-MM') desc) as orders
