	


SELECT

	t2.year_month,
	SUM(t2.number_cases) as good_b2c_cases

FROM
	
	
	(SELECT
	
		t1.year_month,
		t1.case_id,
		(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
					 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
					 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
		SUM(t1.number_b2c_cases) as number_cases
	
	FROM
	
		(SELECT
		
			TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
			o.case_id,
			o.date1_opened,
			o.date2_closed,
			ca."status",
			o.closed,
			o.type,
			ca.origin,
			-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
			COUNT(DISTINCT o.case_id) as number_b2c_cases
		
		FROM
		
			bi.cm_b2c_cases_service_level_basis o
			
		LEFT JOIN
		
			salesforce.case ca
			
		ON
		
			o.case_id = ca.sfid
				
		WHERE
		
			o.type NOT IN ('# Reopened')
			AND ca.reason NOT LIKE '%Onboarding%'
			AND ca."type" NOT LIKE '%Damage%'
			AND ca.test__c IS NOT TRUE
			-- AND o.case_id = '5000J00001TDAEhQAP'
			
		GROUP BY
		
			TO_CHAR(o.date1_opened, 'YYYY-MM'),
			o.case_id,
			o.date1_opened,
			o.date2_closed,
			ca."status",
			o.closed,
			o.type,
			ca.origin
			-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
			
		ORDER BY
		
			TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
			
	WHERE
	
		-- t1.number_b2b_cases_1 > 0
		-- t1.case_id = '5000J00001TCmRsQAL'
		(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
					 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
					 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			
	GROUP BY
	
		t1.year_month,
		t1.case_id,
		CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
					 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
					 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
		
	ORDER BY
	
		t1.year_month desc) as t2
		
GROUP BY

	t2.year_month
	
ORDER BY

	t2.year_month desc