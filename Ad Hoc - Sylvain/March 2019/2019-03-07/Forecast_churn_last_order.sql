


SELECT

	o.sfid,
	o.name,
	oo.confirmed_end__c,
	oo.end__c,
	oo.resignation_date__c,
	MAX(ooo.effectivedate) as last_valid_order,
	MAX(ooo.effectivedate) - oo.end__c as anticipation


FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.order ooo
	
ON

	o.sfid = ooo.opportunityid
	
WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('CANCELLED', 'RESIGNED')
	AND ooo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND oo.end__c IS NOT NULL 
	-- OR oo.confirmed_end__c IS NOT NULL)
	AND oo.status__c IN ('CANCELLED', 'RESIGNED')
	
GROUP BY

	o.sfid,
	o.name,
	oo.confirmed_end__c,
	oo.end__c,
	oo.resignation_date__c