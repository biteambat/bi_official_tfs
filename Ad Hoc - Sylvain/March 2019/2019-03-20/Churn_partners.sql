		
		
SELECT

	t2.company_name,
	0.6*t2.churn_this_month + 0.3*t2.churn_last_month + 0.1*t2.churn_last_month2 as weighted_churn,
	0.6*t2.score_churn_this_month + 0.3*t2.score_churn_last_month + 0.1*t2.score_churn_last_month2 as weighted_score_churn

FROM	
	
	(SELECT	
		
		t1.company_name,
		t1.churn_this_month,
		CASE WHEN t1.churn_this_month = 0 THEN 10
		     WHEN t1.churn_this_month = 1 THEN 5
		     ELSE 0
		     END as score_churn_this_month,	
		t1.churn_last_month,
		CASE WHEN t1.churn_last_month = 0 THEN 10
		     WHEN t1.churn_last_month = 1 THEN 5
		     ELSE 0
		     END as score_churn_last_month,	
		t1.churn_last_month2,
		CASE WHEN t1.churn_last_month2 = 0 THEN 10
		     WHEN t1.churn_last_month2 = 1 THEN 5
		     ELSE 0
		     END as score_churn_last_month2
	
	FROM
	
		(SELECT
		
			-- o.year_month,
			o.company_name,
			SUM(CASE WHEN o.year_month = LEFT((current_date)::text, 7) THEN 1 ELSE 0 END) as churn_this_month,
			SUM(CASE WHEN o.year_month = LEFT((current_date - 30)::text, 7) THEN 1 ELSE 0 END) as churn_last_month,
			SUM(CASE WHEN o.year_month = LEFT((current_date - 60)::text, 7) THEN 1 ELSE 0 END) as churn_last_month2,
			COUNT(DISTINCT o.opportunityid) as opps_churned
		
		FROM
		
			bi.deep_churn o
			
		WHERE
		
			(o.year_month = LEFT((current_date)::text, 7)
			OR o.year_month = LEFT((current_date - 30)::text, 7)
			OR o.year_month = LEFT((current_date - 60)::text, 7))
			AND o.kpi = 'Churn'
			
		GROUP BY
		
			-- o.year_month,
			o.company_name) as t1) as t2