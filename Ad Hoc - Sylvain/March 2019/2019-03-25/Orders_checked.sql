SELECT

	t3.sfid_partner,
	t3.name_partner,
	t3.polygon as polygon2,
	t3.pc_checked_this_month,
	t3.pc_checked_last_month,
	t3.pc_checked_last_month_2,
	t3.pc_checked_this_month*0.6 + t3.pc_checked_last_month*0.3 + t3.pc_checked_last_month_2*0.1 as pc_checked_weighted,
	t3.score_this_month,
	t3.score_last_month,
	t3.score_last_month2,
	t3.score_this_month*0.6 + t3.score_last_month*0.3 + t3.score_last_month2*0.1 as score_weighted
	
FROM

	(SELECT
	
		t6.sfid_partner,
		t6.name_partner,
		t6.polygon,
		t6.pc_checked_this_month,
		CASE WHEN pc_checked_this_month = 0 THEN 0
			  WHEN pc_checked_this_month > 0 AND pc_checked_this_month <= 0.1 THEN 1
			  WHEN pc_checked_this_month > 0.1 AND pc_checked_this_month <= 0.2 THEN 2
			  WHEN pc_checked_this_month > 0.2 AND pc_checked_this_month <= 0.3 THEN 3
			  WHEN pc_checked_this_month > 0.3 AND pc_checked_this_month <= 0.4 THEN 4
			  WHEN pc_checked_this_month > 0.4 AND pc_checked_this_month <= 0.5 THEN 5
			  WHEN pc_checked_this_month > 0.5 AND pc_checked_this_month <= 0.6 THEN 6
			  WHEN pc_checked_this_month > 0.6 AND pc_checked_this_month <= 0.7 THEN 7
			  WHEN pc_checked_this_month > 0.7 AND pc_checked_this_month <= 0.8 THEN 8
			  WHEN pc_checked_this_month > 0.8 AND pc_checked_this_month <= 0.9 THEN 9
			  WHEN pc_checked_this_month > 0.9 AND pc_checked_this_month <= 1 THEN 10
			  ELSE 11
			  END as score_this_month,
		t6.pc_checked_last_month,
		CASE WHEN pc_checked_last_month = 0 THEN 0
			  WHEN pc_checked_last_month > 0 AND pc_checked_last_month <= 0.1 THEN 1
			  WHEN pc_checked_last_month > 0.1 AND pc_checked_last_month <= 0.2 THEN 2
			  WHEN pc_checked_last_month > 0.2 AND pc_checked_last_month <= 0.3 THEN 3
			  WHEN pc_checked_last_month > 0.3 AND pc_checked_last_month <= 0.4 THEN 4
			  WHEN pc_checked_last_month > 0.4 AND pc_checked_last_month <= 0.5 THEN 5
			  WHEN pc_checked_last_month > 0.5 AND pc_checked_last_month <= 0.6 THEN 6
			  WHEN pc_checked_last_month > 0.6 AND pc_checked_last_month <= 0.7 THEN 7
			  WHEN pc_checked_last_month > 0.7 AND pc_checked_last_month <= 0.8 THEN 8
			  WHEN pc_checked_last_month > 0.8 AND pc_checked_last_month <= 0.9 THEN 9
			  WHEN pc_checked_last_month > 0.9 AND pc_checked_last_month <= 1 THEN 10
			  ELSE 11
			  END as score_last_month,
		t6.pc_checked_last_month_2,
		CASE WHEN pc_checked_last_month_2 = 0 THEN 0
			  WHEN pc_checked_last_month_2 > 0 AND pc_checked_last_month_2 <= 0.1 THEN 1
			  WHEN pc_checked_last_month_2 > 0.1 AND pc_checked_last_month_2 <= 0.2 THEN 2
			  WHEN pc_checked_last_month_2 > 0.2 AND pc_checked_last_month_2 <= 0.3 THEN 3
			  WHEN pc_checked_last_month_2 > 0.3 AND pc_checked_last_month_2 <= 0.4 THEN 4
			  WHEN pc_checked_last_month_2 > 0.4 AND pc_checked_last_month_2 <= 0.5 THEN 5
			  WHEN pc_checked_last_month_2 > 0.5 AND pc_checked_last_month_2 <= 0.6 THEN 6
			  WHEN pc_checked_last_month_2 > 0.6 AND pc_checked_last_month_2 <= 0.7 THEN 7
			  WHEN pc_checked_last_month_2 > 0.7 AND pc_checked_last_month_2 <= 0.8 THEN 8
			  WHEN pc_checked_last_month_2 > 0.8 AND pc_checked_last_month_2 <= 0.9 THEN 9
			  WHEN pc_checked_last_month_2 > 0.9 AND pc_checked_last_month_2 <= 1 THEN 10
			  ELSE 11
			  END as score_last_month2
	
	FROM
		
		(SELECT
		
			t5.sfid_partner,
			t5.name_partner,
			t5.polygon,
			
			SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_submitted ELSE 0 END) as offers_submitted_this_month,
			SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_checked ELSE 0 END) as offers_checked_this_month,
			CASE WHEN SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_submitted ELSE 0 END) > 0 THEN
				SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_checked ELSE 0 END)/SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_submitted ELSE 0 END)
			ELSE 0 
			END  as pc_checked_this_month,
			
			SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_submitted ELSE 0 END) as offers_submitted_last_month,
			SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_checked ELSE 0 END) as offers_checked_last_month,
			CASE WHEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_submitted ELSE 0 END) > 0
			THEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_checked ELSE 0 END)/SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_submitted ELSE 0 END)
			ELSE 0
			END  as pc_checked_last_month,
			
			SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_submitted ELSE 0 END) as offers_submitted_last_month_2,
			SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_checked ELSE 0 END) as offers_checked_last_month_2,
			CASE WHEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_submitted ELSE 0 END) > 0
			THEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_checked ELSE 0 END)/SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_submitted ELSE 0 END)
			ELSE 0
			END  as pc_checked_last_month_2
		
		
		FROM
		
				
				(SELECT
					
				  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
				  MIN(effectivedate::date) as mindate,
				  MAX(effectivedate::date) as maxdate,
				  t4.delivery_area__c as polygon,
				  t3.subcon as name_partner,
				  t3.sfid_partner,
				  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
				  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
				
				FROM
				
				  (SELECT
				  
				     t5.*,
				     t2.name as subcon,
				     t2.sfid as sfid_partner
				     
				   FROM
				   
				   	Salesforce.Account t5
				   
					JOIN
				      
						Salesforce.Account t2
				   
					ON
				   
						(t2.sfid = t5.parentid)
				     
					WHERE 
					
						t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
						-- AND t5.type__c = 'partner'
						AND LEFT(t5.locale__c, 2) = 'de'
						-- AND t5.role__c = 'master'
						AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
						AND t5.name NOT LIKE '%Handyman Kovacs%'
						AND t5.name NOT LIKE '%BAT Business Services GmbH%'
				   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
				   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
				      
				JOIN 
				
				  salesforce.order t4
				  
				ON
				
				  (t3.sfid = t4.professional__c)
				  
				WHERE
				
				  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
				  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
				  and LEFT(t4.locale__c,2) IN ('de')
				  and t4.effectivedate < current_date
				  AND t4.type = 'cleaning-b2b'
				  AND t4.test__c IS FALSE
				
				GROUP BY
				
				  year_month,
				  polygon,
				  t3.subcon,
				  t3.sfid_partner
				  
				  
				ORDER BY
					
				  year_month desc,
				  polygon,
				  t3.subcon) as t5
				  
		GROUP BY
		
			t5.sfid_partner,
			t5.name_partner,
			t5.polygon) as t6) as t3