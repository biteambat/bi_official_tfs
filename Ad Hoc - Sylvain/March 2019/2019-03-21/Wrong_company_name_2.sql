SELECT

	a.sfid,
	a.name,
	a.company_name__c
	
FROM

	salesforce.account a
	
WHERE

	a.sfid IN ('0010J00001wHHLzQAO', '0010J00001xxpqnQAA', '0010J00001xxpqYQAQ', '0010J00001xyZbRQAU', '0010J0000203ZtCQAU')