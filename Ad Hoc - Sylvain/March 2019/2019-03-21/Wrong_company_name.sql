SELECT

  a.sfid,
  a.hr_contract_start__c,
  a.hr_contract_end__c,
  a.name,
  CASE WHEN a.company_name__c IS NULL THEN 'BAT Business Services GmbH' ELSE a.company_name__c END as company_name__c,
  a.locale__c,
  a.status__c,
  a.type__c,
  CASE WHEN a.delivery_areas__c IS NULL THEN CASE WHEN a.locale__c LIKE 'de-%' THEN 'de-other'
                          WHEN a.locale__c LIKE 'ch-%' THEN 'ch-other'
                          WHEN a.locale__c LIKE 'nl-%' THEN 'nl-other'
                          ELSE 'at-other'
                          END
                      ELSE a.delivery_areas__c
                      END as polygon,
  CASE WHEN (t1.number_employees_tot IS NULL) THEN a.numberofemployees ELSE t1.number_employees_tot END as number_employees_tot,
  t1.hourly_cost_sub as pph_partner,
  t1.parentid_cleaner,
  t1.sfid_cleaner,
  t1.name_cleaner,
  CASE WHEN a.company_name__c IS NULL THEN 'BAT' ELSE 'Partner' END as category
  
FROM

  salesforce.account a
  
LEFT JOIN

  (SELECT
  
    a.name as name_partner,
    a.status__c,
    a.type__c,
    a.hr_contract_start__c as contract_start,
    a.hr_contract_end__c as contract_end,
    a.sfid as sfid_partner,
    a.parentid as parentid_company,
    a.company_name__c,
    CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
                            WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
                            WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
                            ELSE 'at-other'
                            END
                        ELSE b.delivery_areas__c
                        END as polygon,
    a.numberofemployees as number_employees_tot,
    a.pph__c as hourly_cost_sub,
    b.parentid as parentid_cleaner,
    b.sfid as sfid_cleaner,
    b.name as name_cleaner,
    b.status__c as status_cleaner
    
  FROM
  
    salesforce.account a
    
  LEFT JOIN
  
    salesforce.account b
    
  ON 
  
    a.sfid = b.parentid
    
  WHERE
  
    -- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
    a.type__c = 'partner'
    AND a.type__c NOT LIKE 'master'
    AND a.company_name__c NOT LIKE '%BAT%'
    AND a.company_name__c IS NOT NULL
    AND a.company_name__c NOT LIKE ''
    AND LOWER(a.name) not like '%test%'
    AND LOWER(a.company_name__c) not like '%test%'
    AND a.test__c IS FALSE) as t1
    
ON

  a.sfid = t1.sfid_cleaner

WHERE

  a.type__c NOT IN ('partner', 'master')
  AND LOWER(a.name) not like '%test%'
  -- AND LOWER(a.company_name__c) not like '%test%'
  AND a.test__c IS FALSE
  AND a.sfid IN ('0010J00001wHHLzQAO', '0010J00001xxpqnQAA', '0010J00001xxpqYQAQ', '0010J00001xyZbRQAU', '0010J0000203ZtCQAU')