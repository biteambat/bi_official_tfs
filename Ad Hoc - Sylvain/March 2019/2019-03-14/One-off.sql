



SELECT

	TO_CHAR(oo.confirmed_end__c, 'YYYY-MM') as year_month,
	oo.confirmed_end__c,
	o.sfid,
	o.name,
	o.delivery_area__c,
	oo.duration__c

FROM


	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
WHERE

	o.status__c IN ('RESIGNED', 'CANCELLED')
	AND o.test__c IS FALSE 
	AND oo.status__c IN ('RESIGNED', 'CANCELLED')
	AND o.closedate > '2018-01-01'