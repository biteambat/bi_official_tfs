

SELECT

	o.sfid,
	o.name,
	o.grand_total__c,
	--- ooo.amount__c,
	oo.start__c,
	oo.duration__c,
	oo.end__c,
	oo.resignation_date__c,
	oo.confirmed_end__c,
	oo.confirmed_end__c - 266 as anticipation_date,
	LEFT(oo.confirmed_end__c::text, 7) as month_churn


FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	-- AND oo.status__c IN ('RESIGNED', 'CANCELLED')
	AND LEFT(oo.confirmed_end__c::text, 7) = '2019-03'
	-- AND ooo.amount__c IS NOT NULL