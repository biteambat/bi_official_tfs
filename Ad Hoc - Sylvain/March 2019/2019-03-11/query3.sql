


SELECT
	
	TO_CHAR(o.effectivedate , 'YYYY-MM') as year_month,
	CASE WHEN oo.provider IS NULL THEN 'BAT' ELSE oo.provider END,
	SUM(CASE WHEN o."type" = 'cleaning-b2b' THEN 1 ELSE 0 END) as orders_b2b,
	SUM(CASE WHEN o."type" = 'cleaning-b2c' THEN 1 ELSE 0 END) as orders_b2c

FROM

	salesforce.order o
	
LEFT JOIN	

	bi.order_provider oo
	
ON

	o.opportunityid = oo.opportunityid
	
WHERE

	o.test__c IS FALSE
	AND o.effectivedate >= '2018-01-01'
	AND o."status" IN ('PENDING TO START', 'FULFILLED', 'INVOICED', 'NOSHOW CUSTOMER')
	
GROUP BY

	TO_CHAR(o.effectivedate , 'YYYY-MM'),
	CASE WHEN oo.provider IS NULL THEN 'BAT' ELSE oo.provider END
	
ORDER BY

	TO_CHAR(o.effectivedate , 'YYYY-MM')

