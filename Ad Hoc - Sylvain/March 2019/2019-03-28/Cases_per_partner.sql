
SELECT

	DISTINCT t2.company,
	COUNT(DISTINCT t2.sfid) as number_cases

FROM
		
	(SELECT	
		
		DISTINCT t1.accountid,
		t1.sfid,
		t1.name1,
		CASE WHEN t1.name2 IS NULL THEN t1.name1 ELSE t1.name2 END as company
	
	FROM
	
		(SELECT
		
			o.sfid,
			o.accountid,
			o.partner__c,
			a.name as name1,
			a.parentid,
			b.name as name2,
			o.reason,
			o.subject
		
		FROM
		
			salesforce.case o
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.accountid = a.sfid
			OR (o.partner__c = a.sfid)
		
		LEFT JOIN
		
			salesforce.account b
			
		ON
			
			a.parentid = b.sfid
			
		WHERE
		
			(o.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement') OR o.reason IS NULL)
			AND o.test__c IS FALSE
			AND o.opportunity__c NOT LIKE '%test%'
			AND (o.origin NOT IN ('System - Notification') OR o.origin IS NULL)
			AND (o.subject NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4') OR o.subject IS NULL)
			AND o.type NOT LIKE '%CM B2C%'
			AND (o.accountid IS NOT NULL OR o.partner__c IS NOT NULL)
			AND o.parentid IS NULL
			AND o.createddate > '2019-01-31'
			AND o.createddate < '2019-03-01'
			
			) as t1) as t2
			
GROUP BY

	t2.company
