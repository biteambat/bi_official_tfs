SELECT

	LEFT(oo.locale__c, 2) as locale,
	oo.locale__c as languages,
	oo.delivery_area__c as city,
	'B2B' as type,
	'Hours Executed Last 30 Days' as kpi,
	'Partner' as sub_kpi_1,
	o.company_name__c as sub_kpi_2,

	SUM(oo.order_duration__c) as value
	

FROM

	bi.list_company_cleaners o
	
LEFT JOIN

	salesforce.order oo
	
ON

	o.sfid = oo.professional__c
	
WHERE

	o.category = 'Partner'
	AND TO_CHAR(oo.effectivedate, 'YYYY-MM') IS NOT NULL
	AND (oo.effectivedate >= (current_date - 30) AND oo.effectivedate <= current_date)
	AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
	AND oo."type" = 'cleaning-b2b'
	AND LOWER(o.name) NOT LIKE '%test%'
	AND LOWER(o.company_name__c) NOT LIKE '%test%'
	AND oo.test__c IS FALSE
	
GROUP BY

	locale,
	languages,
	oo.delivery_area__c,
	sub_kpi_2
	
