SELECT

	t10.*,
	CASE WHEN t10.hours_per_cleaner = 0 THEN 0
	     WHEN (t10.hours_per_cleaner > 0 AND t10.hours_per_cleaner <= 4) THEN 1
	     WHEN (t10.hours_per_cleaner > 4 AND t10.hours_per_cleaner <= 8) THEN 2
	     WHEN (t10.hours_per_cleaner > 8 AND t10.hours_per_cleaner <= 12) THEN 3
	     WHEN (t10.hours_per_cleaner > 12 AND t10.hours_per_cleaner <= 16) THEN 4
	     WHEN t10.hours_per_cleaner > 16 THEN 5
	     ELSE 100
	     END as score_hours_cleaners,
	CASE WHEN t11.weighted_churn IS NULL THEN 0 ELSE t11.weighted_churn END as weighted_churn,
	CASE WHEN t11.score_weighted_churn IS NULL THEN 10 ELSE t11.score_weighted_churn END as score_weighted_churn,
	CASE WHEN t11.weighted_churn_€ IS NULL THEN 0 ELSE t11.weighted_churn_€ END as weighted_churn_€,
	CASE WHEN t11.score_weighted_churn_€ IS NULL THEN 10 ELSE t11.score_weighted_churn_€ END as score_weighted_churn_€,
	t12.*,
	t13.*,
	t15.number_cases
	

FROM

	(SELECT
	
		t8.*,
		CASE WHEN t8.active_pro_last_month > 0 THEN
					 t8.hours_executed/t8.active_pro_last_month
			  ELSE 0
			  END as hours_per_cleaner,
		CASE WHEN t8.share_city < (t8.threshold_hours - 0.05) THEN '> 5% under threshold'
		     WHEN t8.share_city > t8.threshold_hours THEN 'Above threshold'
		     ELSE '< 5% under threshold'
		     END as light_city_share,
		CASE WHEN t8.active_pro_last_month = t8.active_pro_last_month2 THEN 'Stagnation # workers'
				  WHEN t8.active_pro_last_month > t8.active_pro_last_month2 THEN 'Increasing # workers'
				  ELSE 'Decreasing # workers'
				  END as trend_active_pros,
		CASE WHEN t8.active_pro_last_month = 0 THEN 0
		     WHEN (t8.active_pro_last_month > 0 AND t8.active_pro_last_month <= 2) THEN 1
		     WHEN (t8.active_pro_last_month > 2 AND t8.active_pro_last_month <= 4) THEN 2
		     WHEN (t8.active_pro_last_month > 4 AND t8.active_pro_last_month <= 6) THEN 3
		     WHEN (t8.active_pro_last_month > 6 AND t8.active_pro_last_month <= 8) THEN 4
		     WHEN t8.active_pro_last_month > 8  THEN 5
		     ELSE 100
		     END as score_active_pro
		     
	FROM	
		
		(SELECT

			t5.sfid,
			t5.partner,
			t5.delivery_areas__c,
			t5.revenue_last_month,
			t5.revenue_last_month2,
			CASE WHEN t5.revenue_last_month = t5.revenue_last_month2 THEN 'Stagnation'
				  WHEN t5.revenue_last_month > t5.revenue_last_month2 THEN 'Increasing revenue'
				  ELSE 'Decreasing revenue'
				  END as trend,
			t5.score_revenue,
			t5.gp,
			t5.gpm_last_month,
			t5.score_gpm,
			MAX(CASE WHEN t6.hours IS NULL THEN 0 ELSE t6.hours END) as total_hours_city,
			MAX(CASE WHEN t5.hours_executed IS NULL THEN 0 ELSE t5.hours_executed END) as hours_executed,
			
			MAX(CASE WHEN t6.hours > 0 THEN 
				  		(CASE WHEN t14.hours_30days IS NULL THEN 0 ELSE t14.hours_30days END)/t6.hours
				  ELSE 0
				  END) as share_city,
			
			MAX(CASE WHEN t5.delivery_areas__c = 'de-berlin' THEN 0.2
				  WHEN t5.delivery_areas__c = 'de-cologne' THEN 0.35
				  WHEN t5.delivery_areas__c = 'de-dusseldorf' THEN 0.4
				  WHEN t5.delivery_areas__c = 'de-frankfurt' THEN 0.5
				  WHEN t5.delivery_areas__c = 'de-hamburg' THEN 0.35
				  WHEN t5.delivery_areas__c = 'de-hannover' THEN 0.6
				  WHEN t5.delivery_areas__c = 'de-leipzig' THEN 0.5
				  WHEN t5.delivery_areas__c = 'de-munich' THEN 0.35
				  WHEN t5.delivery_areas__c = 'de-stuttgart' THEN 0.5
				  ELSE 1
				  END) as threshold_hours,
			MAX(CASE WHEN t5.active_pro_last_month IS NULL THEN 0 ELSE t5.active_pro_last_month END) active_pro_last_month,
			MAX(CASE WHEN t5.active_pro_last_month2 IS NULL THEN 0 ELSE t5.active_pro_last_month2 END) active_pro_last_month2
		
		FROM		
			
			(SELECT	
			
				t4.sfid,
				t4.partner,
				t4.delivery_areas__c,
				SUM(t4.revenue_last_month) as revenue_last_month,
				SUM(t4.revenue_last_month2) as revenue_last_month2,
				MAX(t4.score_revenue) as score_revenue,
				MAX(t4.gp) as gp,
				SUM(t4.gpm) as gpm_last_month,
				MAX(CASE WHEN t4.gpm <= 0 THEN 0
					  WHEN t4.gpm > 0 AND t4.gpm <= 0.02 THEN 1
					  WHEN t4.gpm > 0.02 AND t4.gpm <= 0.04 THEN 2
					  WHEN t4.gpm > 0.04 AND t4.gpm <= 0.06 THEN 3
					  WHEN t4.gpm > 0.06 AND t4.gpm <= 0.08 THEN 4
					  WHEN t4.gpm > 0.08 AND t4.gpm <= 0.1 THEN 5
					  WHEN t4.gpm > 0.1 AND t4.gpm <= 0.12 THEN 6
					  WHEN t4.gpm > 0.12 AND t4.gpm <= 0.14 THEN 7
					  WHEN t4.gpm > 0.14 AND t4.gpm <= 0.16 THEN 8
					  WHEN t4.gpm > 0.16 AND t4.gpm <= 0.18 THEN 9
					  WHEN t4.gpm > 0.18 AND t4.gpm <= 0.20 THEN 10
					  WHEN t4.gpm > 0.2 AND t4.gpm <= 0.22 THEN 11
					  WHEN t4.gpm > 0.22 AND t4.gpm <= 0.24 THEN 12
					  WHEN t4.gpm > 0.24 AND t4.gpm <= 0.26 THEN 13
					  WHEN t4.gpm > 0.26 AND t4.gpm <= 0.28 THEN 14
					  WHEN t4.gpm > 0.28 THEN 15
					  ELSE 0
					  END) as score_gpm,
				MIN(t4.hours_executed) as hours_executed,
				MIN(t4.active_pro_last_month) as active_pro_last_month,
				MIN(t4.active_pro_last_month2) as active_pro_last_month2
				
			FROM
				
				(SELECT
				
					t1.sfid,
					t1.partner,
					t1.city as delivery_areas__c,
					CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END as revenue_last_month,
					CASE WHEN t1.revenue_last_month2 IS NULL THEN 0 ELSE t1.revenue_last_month2 END as revenue_last_month2,
					
					SUM(CASE WHEN t9.active_pro_last_month IS NULL THEN 0 ELSE t9.active_pro_last_month END) as active_pro_last_month,
					SUM(CASE WHEN t9.active_pro_last_month2 IS NULL THEN 0 ELSE t9.active_pro_last_month2 END) as active_pro_last_month2,
					
					CASE WHEN t1.revenue_last_month = 0 THEN 0
						  WHEN (t1.revenue_last_month > 0 AND t1.revenue_last_month <= 500) THEN 1
						  WHEN (t1.revenue_last_month > 500 AND t1.revenue_last_month <= 1000) THEN 2
						  WHEN (t1.revenue_last_month > 1000 AND t1.revenue_last_month <= 1500) THEN 3
						  WHEN (t1.revenue_last_month > 1500 AND t1.revenue_last_month <= 2000) THEN 4
						  WHEN (t1.revenue_last_month > 2000 AND t1.revenue_last_month <= 2500) THEN 5
						  WHEN (t1.revenue_last_month > 2500 AND t1.revenue_last_month <= 3000) THEN 6
						  WHEN (t1.revenue_last_month > 3000 AND t1.revenue_last_month <= 3500) THEN 7
						  WHEN (t1.revenue_last_month > 3500 AND t1.revenue_last_month <= 4000) THEN 8
						  WHEN (t1.revenue_last_month > 4000 AND t1.revenue_last_month <= 4500) THEN 9
						  WHEN (t1.revenue_last_month > 4500 AND t1.revenue_last_month <= 5000) THEN 10
						  WHEN (t1.revenue_last_month > 5000 AND t1.revenue_last_month <= 5500) THEN 11
						  WHEN (t1.revenue_last_month > 5500 AND t1.revenue_last_month <= 6000) THEN 12
						  WHEN (t1.revenue_last_month > 6000 AND t1.revenue_last_month <= 6500) THEN 13
						  WHEN (t1.revenue_last_month > 6500 AND t1.revenue_last_month <= 7000) THEN 14
						  WHEN (t1.revenue_last_month > 7000 AND t1.revenue_last_month <= 7500) THEN 15
						  WHEN (t1.revenue_last_month > 7500 AND t1.revenue_last_month <= 8000) THEN 16
						  WHEN (t1.revenue_last_month > 8000 AND t1.revenue_last_month <= 8500) THEN 17
						  WHEN (t1.revenue_last_month > 8500 AND t1.revenue_last_month <= 9000) THEN 18
						  WHEN (t1.revenue_last_month > 9000 AND t1.revenue_last_month <= 9500) THEN 19
						  WHEN (t1.revenue_last_month > 9500) THEN 20
						  ELSE 0
						  END as score_revenue,
					CASE WHEN t2.cost_supply IS NULL THEN 0 ELSE t2.cost_supply END as cost_supply,
					CASE WHEN t3.operational_costs IS NULL THEN 0 ELSE t3.operational_costs END as operational_costs,
					((CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END) - (CASE WHEN t2.cost_supply IS NULL THEN 0 ELSE t2.cost_supply END) - CASE WHEN t3.operational_costs IS NULL THEN 0 ELSE t3.operational_costs END) as gp,
					
					CASE WHEN (CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END) <> 0 THEN 
				   					((CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END) - (CASE WHEN t2.cost_supply IS NULL THEN 0 ELSE t2.cost_supply END) - CASE WHEN t3.operational_costs IS NULL THEN 0 ELSE t3.operational_costs END)/CASE WHEN t1.revenue_last_month IS NULL THEN 0 ELSE t1.revenue_last_month END
				   					ELSE 0
				   					END as gpm,
				   MAX(t7.hours_executed) as hours_executed
					
				FROM	
				
					(SELECT
					
						t3.sfid,
						t3.partner as partner,
						t3.year_month,
						t3.partner as partner2,
						t3.city,
						t3.revenue_last_month,
						t3.revenue_last_month2
						
					
					FROM
						
						
						((SELECT
						
							a.sfid,
							a.delivery_areas__c,
							a.name as partner
						
						FROM
						
							salesforce.account a 
								
						WHERE
						
							a.test__c IS FALSE
							AND a.role__c = 'master') as t0
							
						LEFT JOIN
							
							(SELECT
							
								o.date_part as year_month,
								o.sub_kpi_2 as partner2,
								o.city,
								SUM(CASE WHEN o.date_part = LEFT((current_date - 30)::text, 7) THEN o.value ELSE 0 END) as revenue_last_month,
								SUM(CASE WHEN o.date_part = LEFT((current_date - 60)::text, 7) THEN o.value ELSE 0 END) as revenue_last_month2
								
							FROM
							
								bi.kpi_master o
								
							WHERE
							
								(o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 60)::text, 7))
										
							GROUP BY
							
								year_month,
								partner2,
								o.city
								
							ORDER BY
							
								year_month desc,
								partner2 asc) as t1
								
						ON
						
							t0.partner = t1.partner2) t3) as t1						
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.city,
						o.sub_kpi_2 as partner,
						SUM(CASE WHEN o.value IS NULL THEN 0 ELSE o.value END) as cost_supply 
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t2
						
				ON
				
					t1.partner = t2.partner	
					AND t1.city = t2.city
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.value IS NULL THEN 0 ELSE o.value END) as operational_costs 
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t3
						
				ON
				
					t1.partner = t3.partner	
					AND t1.city = t3.city
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.value IS NULL THEN 0 ELSE o.value END) as hours_executed 
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t7
						
				ON
				
					t1.partner = t7.partner	
					AND t1.city = t7.city
					
				LEFT JOIN
					
					(SELECT
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner,
						o.city,
						SUM(CASE WHEN o.date_part = LEFT((current_date - 30)::text, 7) THEN o.value ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN o.date_part = LEFT((current_date - 60)::text, 7) THEN o.value ELSE 0 END) as active_pro_last_month2
						
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						(o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 30)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 60)::text, 7))
								
					GROUP BY
					
						year_month,
						partner,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner asc) as t9
						
				ON
				
					t1.partner = t9.partner	
					AND t1.city = t9.city
				
				GROUP BY
				
					t1.sfid,
					t1.partner,
					t1.city,
					t1.revenue_last_month,
					t1.revenue_last_month2,
					t2.cost_supply,
					t3.operational_costs		
					
				ORDER BY
				
					t1.partner) as t4
					
			LEFT JOIN
			
				(SELECT
		
					LEFT((o.effectivedate - 30)::text, 7),
					o.delivery_area__c,
					SUM(o.order_duration__c) as hours
					
					
				FROM
				
					salesforce."order" o
					
				WHERE
				
					o."type" = 'cleaning-b2b'
					AND LEFT((o.effectivedate - 30)::text, 7)  = LEFT((current_date - 30)::text, 7)
					AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
					
				GROUP BY
				
					LEFT((o.effectivedate - 30)::text, 7),
					o.delivery_area__c) as t6
					
			ON 
			
				t4.delivery_areas__c = t6.delivery_area__c
					
			GROUP BY
			
				t4.sfid,
				t4.partner,
				t4.delivery_areas__c
				
			ORDER BY
			
				t4.partner asc) as t5
						
		LEFT JOIN
		
			(SELECT
		
				LEFT((o.effectivedate - 30)::text, 7),
				o.delivery_area__c,
				SUM(o.order_duration__c) as hours
				
				
			FROM
			
				salesforce."order" o
				
			WHERE
			
				o."type" = 'cleaning-b2b'
				AND LEFT((o.effectivedate - 30)::text, 7)  = LEFT((current_date - 30)::text, 7)
				AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
				
			GROUP BY
			
				LEFT((o.effectivedate - 30)::text, 7),
				o.delivery_area__c) as t6	
				
		ON
			
			t5.delivery_areas__c = t6.delivery_area__c	 
			
		LEFT JOIN
		
			(SELECT
		
				LEFT(oo.locale__c, 2) as locale,
				oo.locale__c as languages,
				oo.delivery_area__c as city,
				'B2B' as type,
				'Hours Executed Last 30 Days' as kpi,
				'Partner' as sub_kpi_1,
				o.company_name__c as sub_kpi_2,
			
				SUM(oo.order_duration__c) as hours_30days
				
			
			FROM
			
				bi.list_company_cleaners o
				
			LEFT JOIN
			
				salesforce.order oo
				
			ON
			
				o.sfid = oo.professional__c
				
			WHERE
			
				o.category = 'Partner'
				AND TO_CHAR(oo.effectivedate, 'YYYY-MM') IS NOT NULL
				AND (oo.effectivedate >= (current_date - 30) AND oo.effectivedate <= current_date)
				AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
				AND oo."type" = 'cleaning-b2b'
				AND LOWER(o.name) NOT LIKE '%test%'
				AND LOWER(o.company_name__c) NOT LIKE '%test%'
				AND oo.test__c IS FALSE
				
			GROUP BY
			
				locale,
				languages,
				oo.delivery_area__c,
				sub_kpi_2) as t14
				
		ON
		
			t5.delivery_areas__c = t14.city
			AND t5.partner = t14.sub_kpi_2
			
		WHERE
		
			t5.partner IS NOT NULL
			
		GROUP BY
		
			t5.sfid,
			t5.partner,
			t5.delivery_areas__c,
			t5.revenue_last_month,
			t5.revenue_last_month2,
			t5.score_revenue,
			t5.gp,
			t5.gpm_last_month,
			t5.score_gpm
			
		ORDER BY
		
			t5.partner) as t8) as t10
			
LEFT JOIN

	(SELECT
		
		t2.partner,
		t2.weighted_churn,
		CASE WHEN (t2.weighted_churn = 0 OR t2.weighted_churn IS NULL) THEN 10
	     WHEN (t2.weighted_churn > 0 AND t2.weighted_churn < 1) THEN 5
	     ELSE 0 
	     END as score_weighted_churn,
	   t2.weighted_churn_€,
	   CASE WHEN (t2.weighted_churn_€ = 0 OR t2.weighted_churn_€ IS NULL) THEN 10
	        WHEN (t2.weighted_churn_€ > 0 AND t2.weighted_churn_€ <= 50) THEN 9
	        WHEN (t2.weighted_churn_€ > 50 AND t2.weighted_churn_€ <= 100) THEN 8
	        WHEN (t2.weighted_churn_€ > 100 AND t2.weighted_churn_€ <= 150) THEN 7
	        WHEN (t2.weighted_churn_€ > 150 AND t2.weighted_churn_€ <= 200) THEN 6
	        WHEN (t2.weighted_churn_€ > 200 AND t2.weighted_churn_€ <= 250) THEN 5
	        WHEN (t2.weighted_churn_€ > 250 AND t2.weighted_churn_€ <= 300) THEN 4
	        WHEN (t2.weighted_churn_€ > 300 AND t2.weighted_churn_€ <= 350) THEN 3
	        WHEN (t2.weighted_churn_€ > 350 AND t2.weighted_churn_€ <= 400) THEN 2
	        WHEN (t2.weighted_churn_€ > 400 AND t2.weighted_churn_€ <= 450) THEN 1
	        WHEN t2.weighted_churn_€ > 450 THEN 0
	   	  ELSE 0 
	        END as score_weighted_churn_€
	
	FROM
	
		(SELECT	
				
			t1.company_name as partner,
			0.6*t1.churn_this_month + 0.3*t1.churn_last_month + 0.1*t1.churn_last_month2 as weighted_churn,
			0.6*t1.churn_this_month_€ + 0.3*t1.churn_last_month_€ + 0.1*t1.churn_last_month2_€ as weighted_churn_€
		
		FROM
		
			(SELECT
			
				-- o.year_month,
				o.company_name,
				SUM(CASE WHEN o.year_month = LEFT((current_date)::text, 7) THEN 1 ELSE 0 END) as churn_this_month,
				SUM(CASE WHEN o.year_month = LEFT((current_date - 30)::text, 7) THEN 1 ELSE 0 END) as churn_last_month,
				SUM(CASE WHEN o.year_month = LEFT((current_date - 60)::text, 7) THEN 1 ELSE 0 END) as churn_last_month2,
				SUM(CASE WHEN o.year_month = LEFT((current_date)::text, 7) THEN 1 ELSE money END) as churn_this_month_€,
				SUM(CASE WHEN o.year_month = LEFT((current_date - 30)::text, 7) THEN 1 ELSE money END) as churn_last_month_€,
				SUM(CASE WHEN o.year_month = LEFT((current_date - 60)::text, 7) THEN 1 ELSE money END) as churn_last_month2_€
				
			
			FROM
			
				bi.deep_churn o
				
			WHERE
			
				(o.year_month = LEFT((current_date)::text, 7)
				OR o.year_month = LEFT((current_date - 30)::text, 7)
				OR o.year_month = LEFT((current_date - 60)::text, 7))
				AND o.kpi = 'Churn'
				
			GROUP BY
			
				-- o.year_month,
				o.company_name) as t1) as t2) as t11
				
ON 

	t10.partner = t11.partner
	
LEFT JOIN

	(SELECT

		t3.sfid as sfid2,
		t3.name as partner2,
		t3.polygon,
		t3.tot_offers_received,
		t3.pc_accepted_this_month,
		t3.pc_accepted_last_month,
		t3.pc_accepted_last_month_2,
		t3.pc_accepted_this_month*0.6 + t3.pc_accepted_last_month*0.3 + t3.pc_accepted_last_month_2*0.1 as pc_accepted_weighted,
		t3.score_this_month as score_acceptance_this_month,
		t3.score_last_month as score_acceptance_last_month,
		t3.score_last_month2 as score_acceptance_last_month2,
		t3.score_this_month*0.6 + t3.score_last_month*0.3 + t3.score_last_month2*0.1 as score_acceptance_weighted
		
	FROM	
		
		(SELECT
		
			t2.sfid,
			t2.name,
			t2.polygon,
			t2.tot_offers_received,
			t2.pc_accepted_this_month,
			CASE WHEN pc_accepted_this_month = 0 THEN 0
				  WHEN pc_accepted_this_month > 0 AND pc_accepted_this_month <= 0.1 THEN 1
				  WHEN pc_accepted_this_month > 0.1 AND pc_accepted_this_month <= 0.2 THEN 2
				  WHEN pc_accepted_this_month > 0.2 AND pc_accepted_this_month <= 0.3 THEN 3
				  WHEN pc_accepted_this_month > 0.3 AND pc_accepted_this_month <= 0.4 THEN 4
				  WHEN pc_accepted_this_month > 0.4 AND pc_accepted_this_month <= 0.5 THEN 5
				  WHEN pc_accepted_this_month > 0.5 AND pc_accepted_this_month <= 0.6 THEN 6
				  WHEN pc_accepted_this_month > 0.6 AND pc_accepted_this_month <= 0.7 THEN 7
				  WHEN pc_accepted_this_month > 0.7 AND pc_accepted_this_month <= 0.8 THEN 8
				  WHEN pc_accepted_this_month > 0.8 AND pc_accepted_this_month <= 0.9 THEN 9
				  WHEN pc_accepted_this_month > 0.9 AND pc_accepted_this_month <= 1 THEN 10
				  ELSE 11
				  END as score_this_month,
			t2.pc_accepted_last_month,
			CASE WHEN pc_accepted_last_month = 0 THEN 0
				  WHEN pc_accepted_last_month > 0 AND pc_accepted_last_month <= 0.1 THEN 1
				  WHEN pc_accepted_last_month > 0.1 AND pc_accepted_last_month <= 0.2 THEN 2
				  WHEN pc_accepted_last_month > 0.2 AND pc_accepted_last_month <= 0.3 THEN 3
				  WHEN pc_accepted_last_month > 0.3 AND pc_accepted_last_month <= 0.4 THEN 4
				  WHEN pc_accepted_last_month > 0.4 AND pc_accepted_last_month <= 0.5 THEN 5
				  WHEN pc_accepted_last_month > 0.5 AND pc_accepted_last_month <= 0.6 THEN 6
				  WHEN pc_accepted_last_month > 0.6 AND pc_accepted_last_month <= 0.7 THEN 7
				  WHEN pc_accepted_last_month > 0.7 AND pc_accepted_last_month <= 0.8 THEN 8
				  WHEN pc_accepted_last_month > 0.8 AND pc_accepted_last_month <= 0.9 THEN 9
				  WHEN pc_accepted_last_month > 0.9 AND pc_accepted_last_month <= 1 THEN 10
				  ELSE 11
				  END as score_last_month,
			t2.pc_accepted_last_month_2,
			CASE WHEN pc_accepted_last_month_2 = 0 THEN 0
				  WHEN pc_accepted_last_month_2 > 0 AND pc_accepted_last_month_2 <= 0.1 THEN 1
				  WHEN pc_accepted_last_month_2 > 0.1 AND pc_accepted_last_month_2 <= 0.2 THEN 2
				  WHEN pc_accepted_last_month_2 > 0.2 AND pc_accepted_last_month_2 <= 0.3 THEN 3
				  WHEN pc_accepted_last_month_2 > 0.3 AND pc_accepted_last_month_2 <= 0.4 THEN 4
				  WHEN pc_accepted_last_month_2 > 0.4 AND pc_accepted_last_month_2 <= 0.5 THEN 5
				  WHEN pc_accepted_last_month_2 > 0.5 AND pc_accepted_last_month_2 <= 0.6 THEN 6
				  WHEN pc_accepted_last_month_2 > 0.6 AND pc_accepted_last_month_2 <= 0.7 THEN 7
				  WHEN pc_accepted_last_month_2 > 0.7 AND pc_accepted_last_month_2 <= 0.8 THEN 8
				  WHEN pc_accepted_last_month_2 > 0.8 AND pc_accepted_last_month_2 <= 0.9 THEN 9
				  WHEN pc_accepted_last_month_2 > 0.9 AND pc_accepted_last_month_2 <= 1 THEN 10
				  ELSE 11
				  END as score_last_month2
				  
		FROM	
			
			(SELECT	
				
				t1.sfid,
				t1.name,
				t1.delivery_areas__c as polygon,
				SUM(CASE WHEN t1.mindate >= (current_date - 90) THEN t1.offers_received ELSE 0 END) as tot_offers_received,
				SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_received ELSE 0 END) as offers_received_this_month,
				SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_accepted ELSE 0 END) as offers_accepted_this_month,
				CASE WHEN SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_received ELSE 0 END) > 0 THEN
					SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_accepted ELSE 0 END)/SUM(CASE WHEN t1.year_month_offer = LEFT(current_date::text, 7) THEN offers_received ELSE 0 END)
				ELSE 0 
				END  as pc_accepted_this_month,
				
				SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_received ELSE 0 END) as offers_received_last_month,
				SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_accepted ELSE 0 END) as offers_accepted_last_month,
				CASE WHEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_received ELSE 0 END) > 0
				THEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_accepted ELSE 0 END)/SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 30)::text, 7) THEN offers_received ELSE 0 END)
				ELSE 0
				END  as pc_accepted_last_month,
				
				SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_received ELSE 0 END) as offers_received_last_month_2,
				SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_accepted ELSE 0 END) as offers_accepted_last_month_2,
				CASE WHEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_received ELSE 0 END) > 0
				THEN SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_accepted ELSE 0 END)/SUM(CASE WHEN t1.year_month_offer = LEFT((current_date - 60)::text, 7) THEN offers_received ELSE 0 END)
				ELSE 0
				END  as pc_accepted_last_month_2
			
			FROM
						
				(SELECT
				
					TO_CHAR(t2.createddate, 'YYYY-MM') as year_month_offer,
					MIN(t2.createddate) as mindate,
					t1.*,
					SUM(CASE WHEN ((t2.suggested_partner1__c = t1.sfid
								  OR t2.suggested_partner2__c = t1.sfid
								  OR t2.suggested_partner3__c = t1.sfid
								  OR t2.suggested_partner4__c = t1.sfid
								  OR t2.suggested_partner5__c = t1.sfid))
						      THEN 1
						      ELSE 0
								END) as offers_received,
					SUM(CASE WHEN (t2.partner__c = t1.sfid AND t2.status__c = 'ACCEPTED') THEN 1 ELSE 0 END) as offers_accepted
				
				FROM	
					
					(SELECT
					
						a.sfid,
						a.name,
						a.company_name__c,
						a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c
										
					FROM
					
						salesforce.account a
						
					WHERE
					
						a.type__c = 'partner'
						AND a.test__c IS FALSE
						AND a.status__c IN ('ACTIVE', 'BETA')
						AND LEFT(a.locale__c, 2) = 'de'
						AND a.role__c = 'master'
						AND a.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
						AND a.name NOT LIKE '%Handyman Kovacs%'
						AND a.name NOT LIKE '%BAT Business Services GmbH%'
						
					ORDER BY
					
						a.name) as t1
						
				LEFT JOIN
				
					salesforce.partner_offer__c t2
									
				ON
				
					t2.suggested_partner5__c = t1.sfid
					OR t2.suggested_partner4__c = t1.sfid
					OR t2.suggested_partner3__c = t1.sfid
					OR t2.suggested_partner2__c = t1.sfid
					OR t2.suggested_partner1__c = t1.sfid
				
				WHERE
				
					TO_CHAR(t2.createddate, 'YYYY-MM') IS NOT NULL
					
				GROUP BY
				
					TO_CHAR(t2.createddate, 'YYYY-MM'),
					t1.sfid,
					t1.name,
					t1.company_name__c,
					t1.delivery_areas__c,
					t1.type__c,
					t1.status__c,
					t1.role__c
					
				ORDER BY
				
					TO_CHAR(t2.createddate, 'YYYY-MM') desc) as t1
					
			GROUP BY
			
				t1.sfid,
				t1.name,
				t1.delivery_areas__c) as t2) as t3) as t12
				
ON 

	t10.partner = t12.partner2
	AND t10.delivery_areas__c = t12.polygon
	
LEFT JOIN

	(SELECT

		t3.sfid_partner,
		t3.name_partner,
		t3.polygon as polygon2,
		t3.pc_checked_this_month,
		t3.pc_checked_last_month,
		t3.pc_checked_last_month_2,
		t3.pc_checked_this_month*0.6 + t3.pc_checked_last_month*0.3 + t3.pc_checked_last_month_2*0.1 as pc_checked_weighted,
		t3.score_this_month as score_checked_this_month,
		t3.score_last_month as score_checked_last_month,
		t3.score_last_month2 as score_checked_last_month2,
		t3.score_this_month*0.6 + t3.score_last_month*0.3 + t3.score_last_month2*0.1 as score_checked_weighted
		
	FROM
	
		(SELECT
		
			t6.sfid_partner,
			t6.name_partner,
			t6.polygon,
			t6.pc_checked_this_month,
			CASE WHEN pc_checked_this_month = 0 THEN 0
				  WHEN pc_checked_this_month > 0 AND pc_checked_this_month <= 0.1 THEN 1
				  WHEN pc_checked_this_month > 0.1 AND pc_checked_this_month <= 0.2 THEN 2
				  WHEN pc_checked_this_month > 0.2 AND pc_checked_this_month <= 0.3 THEN 3
				  WHEN pc_checked_this_month > 0.3 AND pc_checked_this_month <= 0.4 THEN 4
				  WHEN pc_checked_this_month > 0.4 AND pc_checked_this_month <= 0.5 THEN 5
				  WHEN pc_checked_this_month > 0.5 AND pc_checked_this_month <= 0.6 THEN 6
				  WHEN pc_checked_this_month > 0.6 AND pc_checked_this_month <= 0.7 THEN 7
				  WHEN pc_checked_this_month > 0.7 AND pc_checked_this_month <= 0.8 THEN 8
				  WHEN pc_checked_this_month > 0.8 AND pc_checked_this_month <= 0.9 THEN 9
				  WHEN pc_checked_this_month > 0.9 AND pc_checked_this_month <= 1 THEN 10
				  ELSE 11
				  END as score_this_month,
			t6.pc_checked_last_month,
			CASE WHEN pc_checked_last_month = 0 THEN 0
				  WHEN pc_checked_last_month > 0 AND pc_checked_last_month <= 0.1 THEN 1
				  WHEN pc_checked_last_month > 0.1 AND pc_checked_last_month <= 0.2 THEN 2
				  WHEN pc_checked_last_month > 0.2 AND pc_checked_last_month <= 0.3 THEN 3
				  WHEN pc_checked_last_month > 0.3 AND pc_checked_last_month <= 0.4 THEN 4
				  WHEN pc_checked_last_month > 0.4 AND pc_checked_last_month <= 0.5 THEN 5
				  WHEN pc_checked_last_month > 0.5 AND pc_checked_last_month <= 0.6 THEN 6
				  WHEN pc_checked_last_month > 0.6 AND pc_checked_last_month <= 0.7 THEN 7
				  WHEN pc_checked_last_month > 0.7 AND pc_checked_last_month <= 0.8 THEN 8
				  WHEN pc_checked_last_month > 0.8 AND pc_checked_last_month <= 0.9 THEN 9
				  WHEN pc_checked_last_month > 0.9 AND pc_checked_last_month <= 1 THEN 10
				  ELSE 11
				  END as score_last_month,
			t6.pc_checked_last_month_2,
			CASE WHEN pc_checked_last_month_2 = 0 THEN 0
				  WHEN pc_checked_last_month_2 > 0 AND pc_checked_last_month_2 <= 0.1 THEN 1
				  WHEN pc_checked_last_month_2 > 0.1 AND pc_checked_last_month_2 <= 0.2 THEN 2
				  WHEN pc_checked_last_month_2 > 0.2 AND pc_checked_last_month_2 <= 0.3 THEN 3
				  WHEN pc_checked_last_month_2 > 0.3 AND pc_checked_last_month_2 <= 0.4 THEN 4
				  WHEN pc_checked_last_month_2 > 0.4 AND pc_checked_last_month_2 <= 0.5 THEN 5
				  WHEN pc_checked_last_month_2 > 0.5 AND pc_checked_last_month_2 <= 0.6 THEN 6
				  WHEN pc_checked_last_month_2 > 0.6 AND pc_checked_last_month_2 <= 0.7 THEN 7
				  WHEN pc_checked_last_month_2 > 0.7 AND pc_checked_last_month_2 <= 0.8 THEN 8
				  WHEN pc_checked_last_month_2 > 0.8 AND pc_checked_last_month_2 <= 0.9 THEN 9
				  WHEN pc_checked_last_month_2 > 0.9 AND pc_checked_last_month_2 <= 1 THEN 10
				  ELSE 11
				  END as score_last_month2
		
		FROM
			
			(SELECT
			
				t5.sfid_partner,
				t5.name_partner,
				t5.polygon,
				
				SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_submitted ELSE 0 END) as offers_submitted_this_month,
				SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_checked ELSE 0 END) as offers_checked_this_month,
				CASE WHEN SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_submitted ELSE 0 END) > 0 THEN
					SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_checked ELSE 0 END)/SUM(CASE WHEN t5.year_month = LEFT(current_date::text, 7) THEN orders_submitted ELSE 0 END)
				ELSE 0 
				END  as pc_checked_this_month,
				
				SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_submitted ELSE 0 END) as offers_submitted_last_month,
				SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_checked ELSE 0 END) as offers_checked_last_month,
				CASE WHEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_submitted ELSE 0 END) > 0
				THEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_checked ELSE 0 END)/SUM(CASE WHEN t5.year_month = LEFT((current_date - 30)::text, 7) THEN orders_submitted ELSE 0 END)
				ELSE 0
				END  as pc_checked_last_month,
				
				SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_submitted ELSE 0 END) as offers_submitted_last_month_2,
				SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_checked ELSE 0 END) as offers_checked_last_month_2,
				CASE WHEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_submitted ELSE 0 END) > 0
				THEN SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_checked ELSE 0 END)/SUM(CASE WHEN t5.year_month = LEFT((current_date - 60)::text, 7) THEN orders_submitted ELSE 0 END)
				ELSE 0
				END  as pc_checked_last_month_2
			
			
			FROM
			
					
					(SELECT
						
					  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
					  MIN(effectivedate::date) as mindate,
					  MAX(effectivedate::date) as maxdate,
					  t4.delivery_area__c as polygon,
					  t3.subcon as name_partner,
					  t3.sfid_partner,
					  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
					  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
					
					FROM
					
					  (SELECT
					  
					     t5.*,
					     t2.name as subcon,
					     t2.sfid as sfid_partner
					     
					   FROM
					   
					   	Salesforce.Account t5
					   
						JOIN
					      
							Salesforce.Account t2
					   
						ON
					   
							(t2.sfid = t5.parentid)
					     
						WHERE 
						
							t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
							-- AND t5.type__c = 'partner'
							AND LEFT(t5.locale__c, 2) = 'de'
							-- AND t5.role__c = 'master'
							AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
							AND t5.name NOT LIKE '%Handyman Kovacs%'
							AND t5.name NOT LIKE '%BAT Business Services GmbH%'
					   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
					   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
					      
					JOIN 
					
					  salesforce.order t4
					  
					ON
					
					  (t3.sfid = t4.professional__c)
					  
					WHERE
					
					  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
					  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
					  and LEFT(t4.locale__c,2) IN ('de')
					  and t4.effectivedate < current_date
					  AND t4.type = 'cleaning-b2b'
					  AND t4.test__c IS FALSE
					
					GROUP BY
					
					  year_month,
					  polygon,
					  t3.subcon,
					  t3.sfid_partner
					  
					  
					ORDER BY
						
					  year_month desc,
					  polygon,
					  t3.subcon) as t5
					  
			GROUP BY
			
				t5.sfid_partner,
				t5.name_partner,
				t5.polygon) as t6) as t3) as t13
				
ON 

	t10.partner = t13.name_partner
	AND t10.delivery_areas__c = t13.polygon2
	
LEFT JOIN

	(SELECT
	
		t2.name_partner,
		COUNT(DISTINCT t2.sfid_case) as number_cases,
		t2.delivery_area__c
	
	FROM	
	
		(SELECT
		
			CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
			t1.sfid_case,
			a.name as name_partner,
			t1.delivery_area__c
			
		FROM	
			
			(SELECT 
			
				c.CaseNumber, 
				c.sfid as sfid_case, 
				o.sfid, a.sfid, 
				o.delivery_area__c,
				c.origin, 
				c.*
			
			FROM 
			
				salesforce.case c
				
			LEFT JOIN 
			
				salesforce.opportunity o
			
			ON
				o.sfid = c.opportunity__c
				
			LEFT JOIN 
			
				salesforce.account a on a.sfid = c.accountid
				
			WHERE 
			
				c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
				AND COALESCE(c.Origin, '') != 'System - Notification'
				AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
				AND c.type != 'CM B2C'
				AND c.CreatedDate BETWEEN DATE '2019-03-01' AND '2019-03-31'
				AND COALESCE(o.name, '') NOT LIKE '%test%' 
				-- SFID from ## account
				AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
				-- SFID from BAT Business Services GmbH account
				AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX') as t1
				
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			(t1.accountid = a.sfid)
			-- OR t1.partner__c = a.sfid)
			
		WHERE 
		
			a.name IS NOT NULL) as t2
			
	GROUP BY 
	
		t2.name_partner,
		t2.delivery_area__c
		
	ORDER BY
	
		t2.name_partner) as t15
		
ON

	t10.partner = t15.name_partner
	AND t10.delivery_areas__c = t15.delivery_area__c
		
ORDER BY 

	t10.partner
	
	