		
		

SELECT

	table2.mention,
	table2.opp_id,
	SUM(contract_nov) as contract_nov,
	SUM(contract_dec) as contract_dec,
	SUM(contract_jan) as contract_jan,
	SUM(contract_feb) as contract_feb,
	SUM(contract_mar) as contract_mar,
	SUM(contract_apr) as contract_apr,
	SUM(contract_may) as contract_may,
	SUM(contract_june) as contract_june

FROM

	(SELECT
	
		'Active Contract' as mention,
		table1.opp_id,
		table1.issued__c,
		CASE WHEN table1.issued__c = '2018-11-30' THEN table1.grand_total__c ELSE 0 END as contract_nov,
		CASE WHEN table1.issued__c = '2018-12-31' THEN table1.grand_total__c ELSE 0 END as contract_dec,
		CASE WHEN table1.issued__c = '2019-01-31' THEN table1.grand_total__c ELSE 0 END as contract_jan,
		CASE WHEN table1.issued__c = '2019-02-28' THEN table1.grand_total__c ELSE 0 END as contract_feb,
		CASE WHEN table1.issued__c = '2019-03-31' THEN table1.grand_total__c ELSE 0 END as contract_mar,
		CASE WHEN table1.issued__c = '2019-04-30' THEN table1.grand_total__c ELSE 0 END as contract_apr,
		CASE WHEN table1.issued__c = '2019-05-31' THEN table1.grand_total__c ELSE 0 END as contract_may,
		CASE WHEN table1.issued__c = '2019-06-30' THEN table1.grand_total__c ELSE 0 END as contract_june
		
	FROM	
		
		
		(SELECT
		
			list_old_cust.opportunity__c as opp_id,
			invoices.*
		
		FROM		
			
			(SELECT
			
				list_cust.opportunity__c
			
			FROM	
				
				(SELECT
				
					i.opportunity__c,
					COUNT(DISTINCT i.sfid) as number_invoices
						
				FROM
				
					salesforce.invoice__c i
					
				WHERE
					
					i.opportunity__c IS NOT NULL
					AND i.type__c IS NULL
					AND i.test__c IS FALSE
					AND i.issued__c >= '2018-01-01'
					AND LEFT(i.locale__c, 2) = 'de'
					
				GROUP BY
				
					i.opportunity__c) as list_cust
					
			WHERE
			
				list_cust.number_invoices > 1) list_old_cust
				
		LEFT JOIN
		
			(SELECT
			
				t1.*,
				cont.grand_total__c,
				pot.potential,
				t1.amount__c - cont.grand_total__c as deficit_grand_total,
				t1.amount__c - pot.potential as deficit_potential
			
			FROM	
				
				(SELECT 
				
					i.sfid as invoice_id, 
					i.opportunity__c,
					i.issued__c,
					i.amount__c/1.19 as amount__c,
					jsonpack_unpack(json__c)#>>'{contract,Id}' as contract_id
					
				FROM 
				
					salesforce.invoice__c i
					
				WHERE
				
					i.opportunity__c IS NOT NULL
					AND i.type__c IS NULL
					AND i.test__c IS FALSE
					AND i.issued__c >= '2018-01-01'
					AND LEFT(i.locale__c, 2) = 'de') as t1
					
			LEFT JOIN
			
				salesforce.contract__c cont
				
			ON
			
				t1.contract_id = cont.sfid
				
			LEFT JOIN
			
				bi.potential_revenue_per_opp pot
				
			ON
			
				t1.opportunity__c = pot.opportunityid
				
			WHERE
			
				(cont.active__c IS TRUE AND cont.service_type__c = 'maintenance cleaning')
				OR (cont.status__c IN ('RESIGNED', 'CANCELLED', 'CANCELLED MISTAKE') AND cont.service_type__c = 'maintenance cleaning')) as invoices
				
		ON
		
			list_old_cust.opportunity__c = invoices.opportunity__c) as table1) as table2
			
WHERE

	(contract_nov > 0
	OR contract_dec > 0
	OR contract_jan > 0
	OR contract_feb > 0
	OR contract_mar > 0
	OR contract_apr > 0
	OR contract_may > 0
	OR contract_june > 0)
			
GROUP BY
	
	table2.mention,
	table2.opp_id