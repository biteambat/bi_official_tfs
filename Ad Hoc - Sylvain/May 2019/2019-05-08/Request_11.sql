



SELECT

	t1.year_month,
	t1.country,
	COUNT(DISTINCT t1.opportunityid) as number_opps,
	SUM(t1.potential) as potential

FROM

	
	(SELECT
	
		TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
		o.opportunityid,
		LEFT(o.locale__c, 2) as country,
		(CASE WHEN oo.grand_total__c IS NULL THEN ooo.potential ELSE oo.grand_total__c END) as potential
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
		
		o.opportunityid = oo.sfid
		
	LEFT JOIN
			
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunityid = ooo.opportunityid
		
	WHERE
	
		o.effectivedate >= '2019-04-01'
		AND o.effectivedate <= '2019-05-31'
		AND o.test__c IS FALSE
		AND o."status" IN ('INVOICED', 'PENDING START', 'FULFILLED', 'CANCELLED CUSTOMER', 'NOSHOW PROFESSIONAL', 'CANCELLED PROFESSIONAL')
		AND o.opportunityid IS NOT NULL
		
	GROUP BY
	
		TO_CHAR(o.effectivedate, 'YYYY-MM'),
		o.opportunityid,
		o.locale__c,
		(CASE WHEN oo.grand_total__c IS NULL THEN ooo.potential ELSE oo.grand_total__c END)) as t1
		
GROUP BY

	t1.year_month,
	t1.country