(SELECT

	a.sfid,
	a.name,
	CASE WHEN o.delivery_area__c IS NULL THEN a.delivery_areas__c ELSE o.delivery_area__c END as delivery_areas,
	o.opportunityid as sfid_opp,
	SUM(o.order_duration__c) as hours_executed,
	a.pph__c,
	MAX(CASE WHEN oo.grand_total__c IS NULL THEN ooo.potential ELSE oo.grand_total__c END) as grand_total_adjusted,
	MAX(oo.monthly_partner_costs__c) as cost_partner,
	
	
FROM

	salesforce.account a 

LEFT JOIN

	salesforce.order o
	
ON

	a.sfid = o.served_by__c
	
LEFT JOIN 

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
LEFT JOIN
		
	bi.potential_revenue_per_opp ooo
	
ON

	o.opportunityid = ooo.opportunityid

WHERE

	a.test__c IS FALSE
	-- AND a."type" = 'partner'
	AND a.type__c = 'partner'
	AND a.status__c = 'ACTIVE'
	AND LEFT(a.locale__c, 2) IN ('de', 'ch')
	AND (o.effectivedate >= '2019-01-01' AND o.effectivedate < '2019-05-01')
	AND oo.status__c IN ('RUNNING', 'RETENTION', 'OFFBOARDING')
	AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')

GROUP BY

	a.sfid,
	a.name,
	a.pph__c,
	CASE WHEN o.delivery_area__c IS NULL THEN a.delivery_areas__c ELSE o.delivery_area__c END,
	o.opportunityid)