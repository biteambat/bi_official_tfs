	

SELECT

	operational_activity.year_week,
	MIN(operational_activity.mindate) as mindate,
	COUNT(DISTINCT operational_activity.partner) as partners_op_active


FROM
	
	(SELECT
	
		TO_CHAR(o.effectivedate, 'YYYY-WW') as year_week,
		MIN(o.effectivedate) as mindate,
		o.served_by__c as partner,
		a.name,
		COUNT(DISTINCT o.sfid) as number_orders
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.account a
		
	ON
	
		o.served_by__c = a.sfid
		
	WHERE
	
		o.test__c IS FALSE
		AND a.test__c IS FALSE
		AND o."type" = 'cleaning-b2b'
		AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
		AND o.opportunityid IS NOT NULL
		AND o.effectivedate <= current_date
		AND o.effectivedate > '2019-03-31'
		AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
		
	GROUP BY
	
		TO_CHAR(o.effectivedate, 'YYYY-WW'),
		o.served_by__c,
		a.name
		
	ORDER BY
	
		TO_CHAR(o.effectivedate, 'YYYY-WW') desc) as operational_activity
		
GROUP BY

	operational_activity.year_week
	
ORDER BY

	operational_activity.year_week desc