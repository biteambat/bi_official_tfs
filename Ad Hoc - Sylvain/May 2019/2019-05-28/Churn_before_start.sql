	

SELECT

	TO_CHAR(list_churn.confirmed_end__c,'YYYY-WW') as date_part,
	MIN(list_churn.confirmed_end__c::date) as date,
	LEFT(list_churn.locale__c, 2) as locale,
	list_churn.locale__c as languages,
	list_churn.delivery_area__c as city,
	'B2B' as type,	
	'Churn' as kpi,
	'Before start'  as sub_kpi_1,
	'Weekly' as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT list_churn.sfid) as value

FROM


	(SELECT
	
		o.sfid,
		o.name,
		o.locale__c,
		o.delivery_area__c,
		o.grand_total__c,
		-- ooo.amount__c,
		oo.start__c,
		oo.duration__c,
		oo.end__c,
		oo.resignation_date__c,
		oo.confirmed_end__c
	
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.sfid = oo.opportunity__c
		
	LEFT JOIN
	
		salesforce.invoice__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
	
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		-- AND ooo.issued__c::text IS NULL
		-- AND ooo.issued__c::text = '2019-04-30'
		-- AND LEFT(oo.confirmed_end__c::text, 7) = '2019-05'
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.grand_total__c,
		o.locale__c,
		o.delivery_area__c,
		-- ooo.amount__c,
		oo.start__c,
		oo.duration__c,
		oo.end__c,
		oo.resignation_date__c,
		oo.confirmed_end__c) as list_churn
		
LEFT JOIN

	(SELECT

		o.opportunityid,
		MIN(o.effectivedate) as first_order
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.test__c IS FALSE
		AND o."type" = 'cleaning-b2b'
		AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'CANCELLED CUSTOMER')
		AND o.opportunityid IS NOT NULL
		
	GROUP BY
	
		o.opportunityid) as dates
		
ON

	list_churn.sfid = dates.opportunityid
	
WHERE

	dates.first_order IS NULL
	
GROUP BY

	TO_CHAR(list_churn.confirmed_end__c,'YYYY-WW'),
	LEFT(list_churn.locale__c, 2),
	list_churn.locale__c,
	list_churn.delivery_area__c
	
