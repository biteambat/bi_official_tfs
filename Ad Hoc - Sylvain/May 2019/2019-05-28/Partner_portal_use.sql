		
	
	
SELECT

	digital_activity.year_week,
	MIN(digital_activity.mindate) as mindate,
	COUNT(DISTINCT digital_activity.sfid_partner) as partners_digitaly_active,
	MAX(operational_activity2.partners_op_active) - COUNT(DISTINCT digital_activity.sfid_partner) as partners_digitaly_inactive,
	MAX(operational_activity2.partners_op_active) as partners_op_active,
	COUNT(DISTINCT digital_activity.sfid_partner)/MAX(operational_activity2.partners_op_active)::decimal as share_digitaly_active

FROM	
	
	(SELECT
	
		TO_CHAR(portal.created_at, 'YYYY-WW') as year_week,
		MIN(portal.created_at) as mindate,
		list_active_partners.partner as sfid_partner,
		list_active_partners.name as partner_name,
		CASE WHEN portal.is_tfs_user IS TRUE THEN 'TFS Agent' ELSE 'Partner' END as tfs_user,
		
		CASE WHEN portal.event_name = 'job_marked_fulfilled' THEN 'Job Marked Fulfilled'
			  WHEN portal.event_name = 'job_marked_not_fulfilled' THEN 'Job Marked Not Fulfilled'
			  WHEN portal.event_name = 'job_comment_added' THEN 'Job Comment Added'
			  
			  WHEN portal.event_name = 'plan_adjustment_confirmed' THEN 'Plan Adjustment Confirmed'
			  WHEN portal.event_name = 'order_adjustment_confirmed' THEN 'Order Adjustment Confirmed'
			  
			  WHEN portal.event_name = 'professional_create' THEN 'Professional Create'
			  WHEN portal.event_name = 'professional_sms_sent' THEN 'Professional SMS Sent'
			  WHEN portal.event_name = 'professional_profile_changes_saved' THEN 'Professional Profile Changes Saved'
			  
			  WHEN portal.event_name = 'offer_accepted' THEN 'Offer Accepted'
			  WHEN portal.event_name = 'offer_pending_accepted' THEN 'Offer Pending Accepted'
			  
			  ELSE 'Unknown'
			  END as event_name,
		COUNT(DISTINCT portal.id) as number_actions
	
	FROM	
		
		(SELECT
		
			o.served_by__c as partner,
			a.name,
			MIN(o.effectivedate) as first_order
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.served_by__c = a.sfid
			
		WHERE
		
			o.test__c IS FALSE
			AND a.test__c IS FALSE
			AND o."type" = 'cleaning-b2b'
			AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
			AND o.opportunityid IS NOT NULL
			-- AND o.effectivedate > (current_date - 30)
			AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
			
		GROUP BY
		
			o.served_by__c,
			a.name) as list_active_partners
			
	LEFT JOIN
	
		events.tfs portal
		
	ON
	
		list_active_partners.partner = portal.subject
		
	WHERE
	
		portal.app = 'PARTNER_PORTAL'
		AND portal.is_tfs_user IS FALSE
		
	GROUP BY
	
		TO_CHAR(portal.created_at, 'YYYY-WW'),
		list_active_partners.partner,
		list_active_partners.name,
		portal.is_tfs_user,
		portal.event_name) as digital_activity
		
LEFT JOIN

	(SELECT
	
		operational_activity.year_week,
		MIN(operational_activity.mindate) as mindate,
		COUNT(DISTINCT operational_activity.partner) as partners_op_active
		
	FROM
		
		(SELECT
		
			TO_CHAR(o.effectivedate, 'YYYY-WW') as year_week,
			MIN(o.effectivedate) as mindate,
			o.served_by__c as partner,
			a.name,
			COUNT(DISTINCT o.sfid) as number_orders
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.served_by__c = a.sfid
			
		WHERE
		
			o.test__c IS FALSE
			AND a.test__c IS FALSE
			AND o."type" = 'cleaning-b2b'
			AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
			AND o.opportunityid IS NOT NULL
			AND o.effectivedate <= current_date
			AND o.effectivedate > '2019-03-31'
			AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
			
		GROUP BY
		
			TO_CHAR(o.effectivedate, 'YYYY-WW'),
			o.served_by__c,
			a.name
			
		ORDER BY
		
			TO_CHAR(o.effectivedate, 'YYYY-WW') desc) as operational_activity
			
	GROUP BY
	
		operational_activity.year_week
		
	ORDER BY
	
		operational_activity.year_week desc) as operational_activity2
		
ON

	digital_activity.year_week = operational_activity2.year_week

GROUP BY

	digital_activity.year_week