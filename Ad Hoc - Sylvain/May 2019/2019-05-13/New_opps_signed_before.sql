		
		
SELECT

	t2.year_month,
	MIN(t2.mindate) as mindate,
	LEFT(t2.locale__c, 2) as locale,
	t2.locale__c as languages,
	t2.delivery_area__c,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Count' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Signed Before' as varchar) as sub_kpi_3,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.signed_before) as value	
		
FROM
		
	(SELECT
	
		TO_CHAR(t1.first_order, 'YYYY-MM') as year_month,
		MIN(t1.first_order) as mindate,
		t1.sfid,
		t1.name,
		t1.locale__c,
		t1.email__c,
		t1.delivery_area__c,
		t1.acquisition_channel__c,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') < TO_CHAR(t1.first_order,'YYYY-MM') THEN 1 ELSE 0 END) as signed_before,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') = TO_CHAR(t1.first_order,'YYYY-MM') THEN 1 ELSE 0 END) as signed_same_month
		
		
	FROM
		
		(SELECT -- This first query is building a table showing the first order date for every opportunity
						
			o.sfid,
			o.name,
			o.locale__c,
			o.email__c,
			o.delivery_area__c,
			o.acquisition_channel__c,
			o.closedate,
			MIN(oo.effectivedate) as first_order,
			ooo.potential
		
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp ooo
		
		ON
		
			o.sfid = ooo.opportunityid
			
		WHERE
		
			-- o.status__c = 'RUNNING'
			o.test__c IS FALSE
			AND oo.test__c IS FALSE
			AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.professional__C IS NOT NULL
			
			
		GROUP BY
		
			o.sfid,
			o.name,
			o.locale__c,
			o.delivery_area__c,
			o.acquisition_channel__c,
			o.closedate,
			o.email__c,
			ooo.potential) as t1
			
	GROUP BY
	
		TO_CHAR(t1.first_order, 'YYYY-MM'),
		t1.sfid,
		t1.name,
		t1.locale__c,
		t1.email__c,
		t1.delivery_area__c,
		t1.acquisition_channel__c) as t2
		
GROUP BY

	t2.year_month,
	LEFT(t2.locale__c, 2),
	t2.locale__c,
	t2.delivery_area__c,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END
	
ORDER BY

	t2.year_month desc,
	LEFT(t2.locale__c, 2),
	t2.locale__c,
	t2.delivery_area__c,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END