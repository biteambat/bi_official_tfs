SELECT

	TO_CHAR(t1.first_order, 'YYYY-MM') as year_month,
	MIN(t1.first_order) as mindate,
	LEFT(t1.locale__c, 2) as locale,
	t1.locale__c as languages,
	t1.delivery_area__c,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Started' as varchar) as sub_kpi_3,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t1.potential) as value


FROM

	
	(SELECT -- This first query is building a table showing the first order date for every opportunity
				
		o.sfid,
		o.name,
		o.locale__c,
		o.email__c,
		o.delivery_area__c,
		o.acquisition_channel__c,
		MIN(oo.effectivedate) as first_order,
		ooo.potential
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	LEFT JOIN

		bi.potential_revenue_per_opp ooo

	ON

		o.sfid = ooo.opportunityid
		
	WHERE
	
		-- o.status__c = 'RUNNING'
		o.test__c IS FALSE
		AND oo.test__c IS FALSE
		AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.professional__C IS NOT NULL
		
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.locale__c,
		o.delivery_area__c,
		o.acquisition_channel__c,
		o.email__c,
		ooo.potential) as t1
	
GROUP BY

	TO_CHAR(t1.first_order, 'YYYY-MM'),
	LEFT(t1.locale__c, 2),
	t1.locale__c,
	t1.delivery_area__c,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END
	
ORDER BY

	TO_CHAR(t1.first_order, 'YYYY-MM') desc;