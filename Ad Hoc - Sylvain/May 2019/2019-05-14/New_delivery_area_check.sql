


SELECT

	o.sfid,
	o.opportunityid,
	oo.name,
	o."status",
	o.served_by__c,
	a.name
	
FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
LEFT JOIN

	salesforce.account a
	
ON

	o.served_by__c = a.sfid
	
WHERE

	LOWER(o.shippingcity) LIKE '%lüneburg%'