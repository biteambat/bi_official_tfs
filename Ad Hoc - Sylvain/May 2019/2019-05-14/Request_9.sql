	
SELECT

	t1.*,
	TRUNC(t1.grand_total_adjusted, 1) as truncat


FROM

	
	
	(SELECT
	
		o.sfid,
		-- o.customer__C,
		LEFT(o.locale__c, 2) as country,
		o.shippingaddress_postalcode__c,
		o.shippingaddress_city__c,
		-- o.grand_total__c,
		-- o.hours_weekly__c,
		-- o.plan_pph__c,
		MAX(CASE WHEN o.grand_total__c IS NULL THEN oo.potential ELSE o.grand_total__c END)::decimal as grand_total_adjusted
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c IN ('RUNNING', 'RETENTION', 'OFFBOARDING', 'RENEGOTIATION', 'ONBOARDED')
		
	GROUP BY
	
		o.sfid,
		LEFT(o.locale__c, 2),
		o.shippingaddress_postalcode__c,
		o.shippingaddress_city__c
		-- CASE WHEN o.grand_total__c IS NULL THEN oo.potential ELSE o.grand_total__c END
		
	ORDER BY
	
		grand_total_adjusted desc) as t1
		