


---- alle leads der letzten 4 Wochen mit dem Status "New lead"
DROP VIEW IF EXISTS  reports.Current_Lead_Status_per_Country;
create view reports.Current_Lead_Status_per_Country as
-- locale CH und NL ohne Lead Owner Einschränkung
select 
		  created_at
		, lead_id
		, status
		, upper(left(locale,2)) as locale
		, null as "Name"
from main.lead
where cast(created_at as date) between current_date-30 and current_date
and upper(left(locale,2)) in ('CH', 'NL')
and status = 'New lead'

union

-- locale AT und DE, nur Leads mit OwnerID von Rhan, Vosen und Tiger
select 
		  created_at
		, lead_id
		, status
		, upper(left(locale,2)) as locale
		, b."Name"
from main.lead a
inner join (
			select "Id", "Name"
			from bi."User"
			where "Name" in ('Ninja Tiger', 'Matthias Vosen', 'Serjoscha Rahn')
			) b
		on a.owner_id = b."Id"
where cast(created_at as date) between current_date-30 and current_date
and upper(left(locale,2)) in ('DE', 'AT')
and status = 'New lead'
;