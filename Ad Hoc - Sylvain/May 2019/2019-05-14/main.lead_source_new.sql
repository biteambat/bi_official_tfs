
DROP VIEW IF EXISTS main.lead_source_new;
CREATE OR REPLACE VIEW main.lead_source_new AS

select 
	lead_id
	, case when cleaner_id = '' or cleaner_id is null then null else cleaner_id
	end as cleaner_id
	, new_lead_source
from main.lead