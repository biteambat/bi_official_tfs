DROP VIEW IF EXISTS main.order;
CREATE VIEW main.order AS
select
  order_creation__c as Created_At
, sfid as Order_ID
, order_id__c as Order_ID2
, contact__c as Customer_ID
-- , customer_id__c as Customer_ID2
, professional__c as Cleaner_ID
, status as Order_Status
, cancellation_reason__c as Cancellation_Reason
-- , payment_method__c as Payment_Method
, acquisition_new_customer__c as Is_New_Customer
, customer_name__c as Customer_Name
, customer_email__c as Customer_Email
-- , customer_phone__c as Customer_Phone
, effectivedate as Order_Start_Date_UTC
, cast(order_start__c as time) as Order_Start_Time_UTC
, cast(order_end__c as date) as Order_End_Date_UTC
, cast(order_end__c as time)Order_End_Time_UTC
, order_duration__c as Order_Duration
, job_details__c as Job_Details
, extras__c as Extras
, description as Description
, quick_note__c as Quick_Note
, setting_flexible_time__c as Setting_Flexible_Time
, setting_gender__c as Setting_Gender
-- , default_professional__c as Default_Professional
, error_note__c as Error_Note
, recurrency__c as Recurrency
-- , order_next__c as Order_Next_Date
-- , order_origin__c as Parent_Order_ID
, rating_professional__c as Rating_Professional
, rating_service__c as Rating_Service
, voucher__c as Voucher
, type as Price_Type
, pph__c as Price_Per_Hour
, commission__c as Commission_Amount
, discount__c as Discount_Amount
, gmv__c as GMV_Amount
, web_grand_total__c as GMV_Net_Amount
, grand_total_eur__c as GMV_Net_Amount_Eur
-- , refunded_amount__c as Refunded_Amount
, commissioned__c as Date_Of_Commission
-- , debit_charge_sent__c as Date_Of_Debit_Charge
, paid__c as Date_Of_Payment
, invoiced__c as Date_Of_Invoice
-- , refunded__c as Date_Of_Refund
, shippingstreet as Shipping_Street
, shippingpostalcode as Shipping_Postal_Code
, shippingcity as Shipping_City
, shippingcountry as Shipping_Country
, shippinglatitude as Shipping_Latitude
, shippinglongitude as Shipping_Longitude
, billingstreet as Billing_Street
, billingpostalcode as Billing_Postal_Code
, billingcity as Billing_City
, billingcountry as Billing_County
, locale__c as Locale
-- , auto_matched__c as Auto_Matched
, selfmatched__c as Self_Matched
-- , scoring__c as Scoring
, allocation_attempts__c as Number_Of_Allocation_Attempts
, allocation_attempts_last__c as Last_Allocation_Attempt
, professionalslot1__c as Professional_Slot_1
, professionalslot2__c as Professional_Slot_2
, professionalslot3__c as Professional_Slot_3
, professionalslot4__c as Professional_Slot_4
, professionalslot5__c as Professional_Slot_5
-- , professionalslot6__c as Professional_Slot_6
-- , professionalslot7__c as Professional_Slot_7
-- , professionalslot8__c as Professional_Slot_8
-- , professionalslot9__c as Professional_Slot_9
-- , professionalslot10__c as Professional_Slot_10
, density_check__c as Density_Check
, density_professionals__c as Density_Professionals
, ip__c as IP_Address
, user_agent__c as User_Agent
, acquisition_customer_creation__c as Date_of_First_Order
-- , in_test_period__c as In_Test_Period
, test__c as Is_Test_Order
-- , tracking_life__c as Order_Tracking_Life
, referred_by__c as Ref_Customer_ID
, acquisition_channel__c as Acquisition_Channel
, acquisition_channel_params__c as Acquisition_Channel_Params
, acquisition_channel_ref__c as Acquisition_Channel_Ref
, acquisition_tracking_id__c as Acquisition_Tracking_ID
-- , contact
from salesforce.order;