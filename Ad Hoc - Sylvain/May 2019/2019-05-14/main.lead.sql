
DROP VIEW IF EXISTS main.lead;
CREATE OR REPLACE VIEW main.lead AS
select x.*, y.new_lead_source

from (	
	select
	  sfid as Lead_ID
	, createddate as Created_At
	, status as Status
	, isconverted as Is_converted
	, convertedaccountid as Cleaner_ID
	, converteddate as Converted_Date
	, rejection_reason__c as Rejection_Reason
	, salutation as Title
	, firstname as First_Name
	, lastname as Last_Name
	, name as Name
	, gender__c as Gender
	, birthdate__c as Birthdate
	, email__c as Email
	, email_access__c as Has_Email_Access
	, languages__c as Languages
	, nationality__c as Nationality
	, company as Company_Name
	, phone as Phone
	, mobilephone as Mobile_Phone
	, otherphone__c as Other_Phone
	, when_to_call__c as Phone_Availability
	, photourl as Photo_URL
	, lastactivitydate as Last_Activity_Date
	, clean_background__c as Has_Cleaning_Background
	, cleaning_experience__c as Cleaning_Experience
	, category__c as Cleaning_Experience2
	, employment_status__c as Employment_Status
	, has_vehicle__c as Has_Vehicle
	, has_tradelicense__c as Has_Tradelicense
	, legal_tradelicense__c as Legal_Tradelicense
	, has_police_clearence__c as Has_Backgroundcheck
	, legal_backgroundcheck__c as Legal_Backgroundcheck
	, legal_contract__c as Legal_Contract
	, legal_contract_ip__c as Legal_Contract_IP
	, working_permit__c as Has_working_permit
	, tax_number__c as Tax_Number
	, note__c as Note
	, availability_daily__c as Availability_Daily
	, availability_weekly__c as Availability_Weekly
	, projected_working_hours__c as Projected_Working_Hours
	-- , working_city__c as Working_City
	, street as Street
	, postalcode as Postal_Code
	, city as City
	, country as Country
	, locale__c as Locale
	, longitude as Longitude
	, latitude as Latitude
	, ip__c as IP
	, mobile_device__c as Mobile_Device
	, deviceid__c as Device_ID
	, user_agent__c as User_Agent
	, ownerid as Owner_ID
	, isunreadbyowner as Is_unread_by_owner
	, test__c as Is_Test_Lead
	, referred_by__c as Ref_by
	, copy_id__c as Copy_ID
	, acquisition_channel__c as Acquisition_Channel
	, acquisition_channel_params__c as Acquisition_Params
	, acquisition_channel_ref__c as Acquisition_Ref
	, leadchannel__c as Lead_Channel
	, leadsource as Lead_Source
	, leadsourcecomment__c as Lead_Source_Comment
	, prp_friend__c as Ref_Cleaner_ID
	, lead_id__c as Lead_ID2
	
	from salesforce.lead
	) x
	
	left join (
		-- leads mit org. source: facebook, google, online
		select
		  sfid as lead_id
		, leadsource
		,  case
				when
							acquisition_channel_params__c like '%"clid":"disp"%' 
						or acquisition_channel_params__c like '%"clid":"dsp"%' 
						or acquisition_channel_params__c like '%"clid":["dsp","dsp"]%' 
					 then 'GDN'
				when
							acquisition_channel_params__c like '%"clid":"goog"%'
						or acquisition_channel_params__c like '%"clid":"ysm"%'
					then 'SEM'
				when	
							acquisition_channel_params__c like '%"clid":"goob"%'
						or acquisition_channel_params__c like '%"clid":"ysmb"%'
					then 'Brand'
				when
					acquisition_channel_params__c like '%"clid":"batfb"%'
					or acquisition_channel_params__c like '%"utm_source":"facebook"%'
					or	acquisition_channel_params__c like '%"fb-signups-pro-wc"%'
					or acquisition_channel_params__c = '{}' and acquisition_channel_ref__c like '%facebook%'
					or acquisition_channel_params__c = '' and acquisition_channel_ref__c like '%facebook%'
					or acquisition_channel_params__c is null and acquisition_channel_ref__c like '%facebook%'							
						or leadsourcecomment__c = 'facebook'
					then 'Facebook'
				when
							acquisition_channel_params__c like '%"utm_source":"newsletter"%'
					then 'Newsletter'
				when
							acquisition_channel_params__c like '%"utm_medium":"classifieds"%'
					then 'Classifieds'						
			else
					case -- inner case start
						when (leadsource is null or leadsource = '' or leadsource in ('Google','Facebook','Online','Classifieds')) then 'Others'
						when leadsource = 'Classified' then 'Classifieds'
					end -- inner case end
			end as new_lead_source
			, leadsourcecomment__c
			, needs_Leadsource_change
			, has_comment
		from (
					select 
						  sfid
						, leadsource
						, leadsourcecomment__c
						, acquisition_channel_params__c
						, acquisition_channel_ref__c
						, case
							when leadsource in ('Google','Facebook','Online', 'Classifieds', '') or leadsource is null
							then true else false
							end as needs_Leadsource_change
						, case 
							when leadsourcecomment__c is null or leadsourcecomment__c = ''
							then false else true
							end as has_comment
					from salesforce.lead
				) a
		where needs_Leadsource_change = true and has_comment = false
		
		union

		-- leads mit übrigens lead_sources 
		-- und leads mit kommentar werden nicht verändert
		select
			  sfid as lead_id
			, leadsource
			, leadsource as new_lead_source
			, leadsourcecomment__c
			, needs_Leadsource_change
			, has_comment
		from (
					select 
						  sfid
						, leadsource
						, leadsourcecomment__c
						, acquisition_channel_params__c
						, acquisition_channel_ref__c
						, case
							when leadsource in ('Google','Facebook','Online', 'Classifieds', '') or leadsource is null
							then true else false
							end as needs_Leadsource_change
						, case 
							when leadsourcecomment__c is null or leadsourcecomment__c = ''
							then false else true
							end as has_comment
					from salesforce.lead
				) b
		where (needs_Leadsource_change = true and has_comment = true) or
			  (needs_Leadsource_change = false and has_comment = true) or
			  (needs_Leadsource_change = false and has_comment = false)
	) y
	on x.lead_id = y.lead_id