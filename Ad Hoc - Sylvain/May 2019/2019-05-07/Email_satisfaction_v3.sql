			
			
	
SELECT	
	
	t3.sfid,
	t3.name,
	t3.locale__c,
	t3.email__c,
	t3.first_order,
	t3.date_first_email,
	t3.second_email,
	t3.status
	
FROM	
	
	(SELECT
	
		t2.sfid,
		t2.name,
		t2.locale__c,
		t2.email__c,
		t2.first_order,
		t2.date_first_email,
		
		-- CASE WHEN (t2.date_first_email + 60) < current_date THEN 
		t2.date_first_email + 60 as second_email,
		
		
		CASE WHEN TO_CHAR(current_date, 'day') = 'saturday' THEN 'DO NOT SEND'
		     WHEN TO_CHAR(current_date, 'day') = 'sunday' THEN 'DO NOT SEND'
		     WHEN TO_CHAR(current_date, 'day') = 'monday' THEN
						(CASE 
						     -- Iterations for the Monday sending
							  WHEN current_date = t2.date_first_email THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 60) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 120) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 180) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 240) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 300) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 360) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 420) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 480) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 540) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 600) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 660) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 720) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 780) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 840) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 900) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 960) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1020) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1080) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1140) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1200) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1260) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1320) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1380) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1440) THEN 'SEND'
							  -- Iterations for the Saturday sending
							  WHEN current_date - 2 = t2.date_first_email THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 60) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 120) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 180) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 240) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 300) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 360) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 420) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 480) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 540) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 600) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 660) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 720) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 780) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 840) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 900) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 960) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1020) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1080) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1140) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1200) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1260) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1320) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1380) THEN 'SEND'
							  WHEN current_date - 2 = (t2.date_first_email + 1440) THEN 'SEND'
							  -- Iterations for the Sunday sending
							  WHEN current_date - 1 = t2.date_first_email THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 60) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 120) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 180) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 240) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 300) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 360) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 420) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 480) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 540) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 600) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 660) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 720) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 780) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 840) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 900) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 960) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1020) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1080) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1140) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1200) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1260) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1320) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1380) THEN 'SEND'
							  WHEN current_date - 1 = (t2.date_first_email + 1440) THEN 'SEND'
				
						     ELSE 'DO NOT SEND'
						     END)							  																				  
				ELSE
						CASE 
						     -- Iterations for all the other days
							  WHEN current_date = t2.date_first_email THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 60) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 120) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 180) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 240) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 300) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 360) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 420) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 480) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 540) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 600) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 660) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 720) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 780) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 840) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 900) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 960) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1020) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1080) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1140) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1200) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1260) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1320) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1380) THEN 'SEND'
							  WHEN current_date = (t2.date_first_email + 1440) THEN 'SEND'	
							  ELSE 'DO NOT SEND'
							  END															  
			   END													  
						as status
	
	FROM		
			
		(SELECT	
			
			t1.sfid,
			t1.name,
			t1.locale__c,
			t1.email__c,
			t1.first_order,
			
			CASE WHEN (current_date >= (t1.first_order + 15 + 60) AND t1.first_order < '2018-01-01') THEN '2019-05-07'
			     WHEN (current_date >= (t1.first_order + 15 + 60) AND (t1.first_order >= '2018-01-01' AND t1.first_order < '2018-08-01')) THEN '2019-05-13'
			     WHEN (current_date >= (t1.first_order + 15 + 60) AND (t1.first_order >= '2018-08-01' AND t1.first_order < '2019-01-01')) THEN '2019-05-20'
			     WHEN (current_date >= (t1.first_order + 15 + 60) AND (t1.first_order >= '2019-01-01' AND t1.first_order < current_date)) THEN '2019-05-27'	
			     ELSE t1.first_order + 75
				  END as date_first_email
		
		FROM
		
			
			(SELECT -- This first query is building a table showing the first order date for every opportunity
			
				o.sfid,
				o.name,
				o.locale__c,
				o.email__c,
				MIN(oo.effectivedate) as first_order
			
			FROM
			
				salesforce.opportunity o
				
			LEFT JOIN
			
				salesforce.order oo
				
			ON
			
				o.sfid = oo.opportunityid
				
			WHERE
			
				o.status__c = 'RUNNING'
				AND o.test__c IS FALSE
				AND oo.test__c IS FALSE
				AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
				
			GROUP BY
			
				o.sfid,
				o.name,
				o.locale__c,
				o.email__c) as t1) as t2) as t3
				
WHERE

	t3.status = 'DO NOT SEND'
				