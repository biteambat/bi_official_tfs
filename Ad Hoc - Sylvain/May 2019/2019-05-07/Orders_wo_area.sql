


SELECT

	o.sfid,
	o.opportunityid,
	o.delivery_area__c,
	oo.delivery_area__c
	

FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
where

	o.test__c IS FALSE
	AND oo.test__c IS FALSE
	AND o.delivery_area__c IS NULL
	AND oo.delivery_area__c IS NOT NULL