	
SELECT

	offers1.sfid,
	offers1.name,
	offers1.delivery_areas__c,
	SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
	SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
	SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
	SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_received_this_month,
	SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
	SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
	
	
	-- offers1.polygon

FROM

	(SELECT
					
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
		MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
		a.sfid,
		a.name,
		a.company_name__c,
		a.delivery_areas__c,
		a.type__c,
		a.status__c,
		a.role__c,
		COUNT(o.suggested_partner__c) as offers_received,
		SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
	
	
	FROM
	
		salesforce.account a
	
	
	LEFT JOIN
	
		salesforce.partner_offer_partner__c o
			
	ON
	
		a.sfid = o.suggested_partner__c 
		
	LEFT JOIN
	
		salesforce.partner_offer__c oo
		
	ON 
	
		o.sfid = oo.sfid
		
	LEFT JOIN
	
		salesforce.opportunity ooo
		
	ON
	
		oo.opportunity__c = ooo.sfid
		
	WHERE
	
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
		AND a.test__c IS FALSE
		
	GROUP BY
	
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
		a.sfid,
		a.name,
		a.company_name__c,
		a.delivery_areas__c,
		a.type__c,
		a.status__c,
		a.role__c
	
	ORDER BY
	
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
		a.name asc) as offers1
		
GROUP BY

	offers1.sfid,
	offers1.name,
	offers1.delivery_areas__c