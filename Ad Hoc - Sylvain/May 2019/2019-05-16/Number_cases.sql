	
	
	(SELECT
	
		t3.year_month,
		t3.partnerid,
		t4.name as name_partner,
		t3.delivery_area__c,
		SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
		SUM(CASE WHEN t3.year_month = LEFT((current_date - 32)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
		SUM(CASE WHEN t3.year_month = LEFT((current_date - 62)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
		
	FROM
				
		(SELECT
		
			TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
			CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
			t2.name_partner,
			t2.parentid,
			COUNT(DISTINCT t2.sfid_case) as number_cases,
			t2.delivery_area__c
		
		FROM	
		
			(SELECT
			
				t1.date_case as date_case,
				CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
				t1.sfid_case,
				a.name as name_partner,
				a.parentid,
				t1.delivery_area__c
				
			FROM	
				
				(SELECT 
				
					c.createddate as date_case,
					c.CaseNumber, 
					c.sfid as sfid_case, 
					o.sfid, a.sfid, 
					o.delivery_area__c,
					c.origin, 
					c.*
				
				FROM 
				
					salesforce.case c
					
				LEFT JOIN 
				
					salesforce.opportunity o
				
				ON
					o.sfid = c.opportunity__c
					
				LEFT JOIN 
				
					salesforce.account a on a.sfid = c.accountid
					
				WHERE 
				
					c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
					AND COALESCE(c.Origin, '') != 'System - Notification'
					AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
					AND c.type != 'CM B2C'
					AND c.CreatedDate >= (current_date - 103)
					AND c.test__c IS FALSE
					AND a.test__c IS FALSE
					AND COALESCE(o.name, '') NOT LIKE '%test%' 
					-- SFID from ## account
					AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
					-- SFID from BAT Business Services GmbH account
					AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
					AND c.parentid IS NULL) as t1
					
			LEFT JOIN
			
				salesforce.account a
				
			ON
			
				(t1.accountid = a.sfid)
				-- OR t1.partner__c = a.sfid)
				
			WHERE 
			
				a.name IS NOT NULL) as t2
				
		GROUP BY 
		
			TO_CHAR(t2.date_case, 'YYYY-MM'),
			t2.partner,
			t2.name_partner,
			t2.parentid,
			t2.delivery_area__c
			
		ORDER BY
		
			t2.name_partner,
			TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
			
	LEFT JOIN
	
		salesforce.account t4
		
	ON
	
		t3.partnerid = t4.sfid
		
	GROUP BY
	
		t3.year_month,
		t3.partnerid,
		t3.delivery_area__c,
		t4.name)