	
SELECT

	orders_checked1.year_month,
	orders_checked1.sfid_partner,
	orders_checked1.name_partner,
	orders_checked1.polygon,
	SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
	SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
	SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
	SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
	SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
	SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2

FROM	
	
	(SELECT
							
	  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
	  MIN(effectivedate::date) as mindate,
	  MAX(effectivedate::date) as maxdate,
	  t4.delivery_area__c as polygon,
	  t3.subcon as name_partner,
	  t3.sfid_partner,
	  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
	  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
	
	FROM
	
	  (SELECT
	  
	     t5.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t5
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t5.parentid)
	     
		WHERE 
		
			t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
			-- AND t5.type__c = 'partner'
			AND LEFT(t5.locale__c, 2) = 'de'
			-- AND t5.role__c = 'master'
			AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
			AND t5.name NOT LIKE '%Handyman Kovacs%'
			AND t5.name NOT LIKE '%BAT Business Services GmbH%'
	   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
	      
	JOIN 
	
	  salesforce.order t4
	  
	ON
	
	  (t3.sfid = t4.professional__c)
	  
	WHERE
	
	  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
	  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
	  and LEFT(t4.locale__c,2) IN ('de')
	  and t4.effectivedate < current_date
	  AND t4.type = 'cleaning-b2b'
	  AND t4.test__c IS FALSE
	
	GROUP BY
	
	  year_month,
	  polygon,
	  t3.subcon,
	  t3.sfid_partner
	  
	  
	ORDER BY
		
	  year_month desc,
	  polygon,
	  t3.subcon) as orders_checked1
	  
GROUP BY

	orders_checked1.sfid_partner,
	orders_checked1.name_partner,
	orders_checked1.year_month,
	orders_checked1.polygon
	
ORDER BY

	orders_checked1.sfid_partner,
	orders_checked1.name_partner,
	orders_checked1.year_month desc