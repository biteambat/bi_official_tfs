		
		
SELECT

	t2.sfid_partner,
	t2.name_partner,
	t2.delivery_areas,
	t3.opps,
	AVG(t2.number_opps_served) as avg_opps_served,
	SUM(t2.hours_executed) as hours_executed,
	SUM(t2.revenue_generated) as revenue_generated,
	SUM(t2.operational_costs) as operational_costs,
	(SUM(t2.revenue_generated-t2.operational_costs)/SUM(t2.revenue_generated)) as gpm

FROM	
	
	(SELECT
	
		t1.year_month,
		t1.sfid as sfid_partner,
		t1.name as name_partner,
		t1.delivery_areas,
		COUNT(DISTINCT t1.sfid_opp) as number_opps_served,
		SUM(t1.hours_executed) as hours_executed,
		SUM(t1.grand_total_adjusted) as revenue_generated,
		SUM(CASE WHEN t1.cost_partner IS NULL THEN t1.hours_executed*t1.pph__c ELSE t1.cost_partner END) as operational_costs,
		(SUM(t1.grand_total_adjusted) - SUM(CASE WHEN t1.cost_partner IS NULL THEN t1.hours_executed*t1.pph__c ELSE t1.cost_partner END))/SUM(t1.grand_total_adjusted) as gpm
	
	FROM	
		
		
		(SELECT
		
			TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
			a.sfid,
			a.name,
			CASE WHEN o.delivery_area__c IS NULL THEN a.delivery_areas__c ELSE o.delivery_area__c END as delivery_areas,
			o.opportunityid as sfid_opp,
			SUM(o.order_duration__c) as hours_executed,
			a.pph__c,
			MAX(CASE WHEN oo.grand_total__c IS NULL THEN ooo.potential ELSE oo.grand_total__c END) as grand_total_adjusted,
			MAX(oo.monthly_partner_costs__c) as cost_partner
			
		FROM
		
			salesforce.account a 
		
		LEFT JOIN
		
			salesforce.order o
			
		ON
		
			a.sfid = o.served_by__c
			
		LEFT JOIN 
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		LEFT JOIN
				
			bi.potential_revenue_per_opp ooo
			
		ON
		
			o.opportunityid = ooo.opportunityid
		
		WHERE
		
			a.test__c IS FALSE
			-- AND a."type" = 'partner'
			AND a.type__c = 'partner'
			AND a.status__c = 'ACTIVE'
			AND LEFT(a.locale__c, 2) IN ('de', 'ch')
			AND (o.effectivedate >= '2019-01-01' AND o.effectivedate < '2019-05-01')
			AND oo.status__c IN ('RUNNING', 'RETENTION', 'OFFBOARDING')
			AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		
		GROUP BY
		
			TO_CHAR(o.effectivedate, 'YYYY-MM'),
			a.sfid,
			a.name,
			a.pph__c,
			CASE WHEN o.delivery_area__c IS NULL THEN a.delivery_areas__c ELSE o.delivery_area__c END,
			o.opportunityid) as t1
			
	WHERE
	
		t1.name NOT LIKE 'BAT Business Services GmbH'
		
	GROUP BY
		
		t1.year_month,
		t1.sfid,
		t1.name,
		t1.delivery_areas) as t2
		
LEFT JOIN

	(SELECT
		
		a.sfid,
		a.name,
		CASE WHEN o.delivery_area__c IS NULL THEN a.delivery_areas__c ELSE o.delivery_area__c END as delivery_areas,
		COUNT(DISTINCT o.opportunityid) as opps
		
	FROM
	
		salesforce.account a 
	
	LEFT JOIN
	
		salesforce.order o
		
	ON
	
		a.sfid = o.served_by__c
		
	LEFT JOIN 
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
		
	
	WHERE
	
		a.test__c IS FALSE
		-- AND a."type" = 'partner'
		AND a.type__c = 'partner'
		AND a.status__c = 'ACTIVE'
		AND LEFT(a.locale__c, 2) IN ('de', 'ch')
		AND (o.effectivedate >= '2019-01-01' AND o.effectivedate < '2019-05-01')
		AND oo.status__c IN ('RUNNING', 'RETENTION', 'OFFBOARDING')
		AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND a.name NOT LIKE 'BAT Business Services GmbH'
	
	GROUP BY
	
		a.sfid,
		a.name,
		CASE WHEN o.delivery_area__c IS NULL THEN a.delivery_areas__c ELSE o.delivery_area__c END
		
	ORDER BY
		
		a.name asc) as t3
		
ON

	t2.name_partner = t3.name
	AND t2.delivery_areas = t3.delivery_areas
		
GROUP BY

	t2.sfid_partner,
	t2.name_partner,
	t2.delivery_areas,
	t3.opps
	
ORDER BY

	t2.name_partner