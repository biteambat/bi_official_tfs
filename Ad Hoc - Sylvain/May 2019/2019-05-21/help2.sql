DROP TABLE IF EXISTS bi.scorecard_partners1;
CREATE TABLE bi.scorecard_partners1 as 

SELECT

	block3.sfid,
	block3.partner,
	block3.city as delivery_areas__c,
	block3.hours_city as total_hours_city,
	block3.hours_executed_this_month as hours_executed,	
	block3.threshold_hours,
	block3.hours_executed_this_month/block3.hours_city as share_city,
	CASE WHEN (block3.hours_executed_this_month/block3.hours_city) < (block3.threshold_hours - 0.05) THEN '> 5% under threshold'
	 	  WHEN (block3.hours_executed_this_month/block3.hours_city) > block3.threshold_hours THEN 'Above threshold'
	 	  ELSE '< 5% under threshold'
	     END as light_city_share,
		     
	block3.revenue_last_month,
	block3.revenue_last_month2,
	block3.score_revenue,
	CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation revenue €'
				  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue €'
				  ELSE 'Decreasing revenue €'
				  END as trend,
	block3.operational_costs_last_month,
	block3.gpm as gpm_last_month,
	
	CASE WHEN block3.gpm <= 0 THEN 0
		  WHEN block3.gpm > 0 AND block3.gpm <= 0.02 THEN 1
		  WHEN block3.gpm > 0.02 AND block3.gpm <= 0.04 THEN 2
		  WHEN block3.gpm > 0.04 AND block3.gpm <= 0.06 THEN 3
		  WHEN block3.gpm > 0.06 AND block3.gpm <= 0.08 THEN 4
		  WHEN block3.gpm > 0.08 AND block3.gpm <= 0.1 THEN 5
		  WHEN block3.gpm > 0.1 AND block3.gpm <= 0.12 THEN 6
		  WHEN block3.gpm > 0.12 AND block3.gpm <= 0.14 THEN 7
		  WHEN block3.gpm > 0.14 AND block3.gpm <= 0.16 THEN 8
		  WHEN block3.gpm > 0.16 AND block3.gpm <= 0.18 THEN 9
		  WHEN block3.gpm > 0.18 AND block3.gpm <= 0.20 THEN 10
		  WHEN block3.gpm > 0.2 AND block3.gpm <= 0.22 THEN 11
		  WHEN block3.gpm > 0.22 AND block3.gpm <= 0.24 THEN 12
		  WHEN block3.gpm > 0.24 AND block3.gpm <= 0.26 THEN 13
		  WHEN block3.gpm > 0.26 AND block3.gpm <= 0.28 THEN 14
		  WHEN block3.gpm > 0.28 THEN 15
		  ELSE 0
		  END as score_gpm,

	block3.active_pro_this_month,
	block3.active_pro_last_month,
	block3.active_pro_last_month2,
	CASE WHEN block3.active_pro_last_month = 0 THEN 0
	     WHEN (block3.active_pro_last_month > 0 AND block3.active_pro_last_month <= 2) THEN 1
	     WHEN (block3.active_pro_last_month > 2 AND block3.active_pro_last_month <= 4) THEN 2
	     WHEN (block3.active_pro_last_month > 4 AND block3.active_pro_last_month <= 6) THEN 3
	     WHEN (block3.active_pro_last_month > 6 AND block3.active_pro_last_month <= 8) THEN 4
	     WHEN block3.active_pro_last_month > 8  THEN 5
	     ELSE 0
	     END as score_active_pro,
	CASE WHEN block3.active_pro_this_month > 0 
		  THEN block3.hours_executed_this_month/block3.active_pro_this_month
		  ELSE 0
		  END as hours_per_cleaner,
		  
	CASE WHEN (CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) = 0 THEN 0
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 0 AND (CASE WHEN block3.active_pro_this_month > 0 
													THEN block3.hours_executed_this_month/block3.active_pro_this_month
													ELSE 0
													END) <= 4) THEN 1
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 4 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 8) THEN 2
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 8 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 12) THEN 3
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 12 AND (CASE WHEN block3.active_pro_this_month > 0 
													 THEN block3.hours_executed_this_month/block3.active_pro_this_month
													 ELSE 0
													 END) <= 16) THEN 4
	     WHEN (CASE WHEN block3.active_pro_this_month > 0 
		  				 THEN block3.hours_executed_this_month/block3.active_pro_this_month
						 ELSE 0
						 END) > 16 THEN 5
					    ELSE 100
					    END as score_hours_cleaners,
	     
	CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
		  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
		  ELSE 'Decreasing # workers'
		  END as trend_active_pros,
	block3.churn_this_month,
	block3.churn_last_month,
	block3.churn_last_month2,
	block3.avg_churn as weighted_churn,
	
	CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
	     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
	     ELSE 0 
	     END as score_weighted_churn,
	
	block3.churn_this_month€,
	block3.churn_last_month€,
	block3.churn_last_month2€,  
	block3.avg_churn€ as weighted_churn_€,   
	
	CASE WHEN (block3.avg_churn€ = 0 OR block3.avg_churn€ IS NULL) THEN 10
        WHEN (block3.avg_churn€ > 0 AND block3.avg_churn€ <= 50) THEN 9
        WHEN (block3.avg_churn€ > 50 AND block3.avg_churn€ <= 100) THEN 8
        WHEN (block3.avg_churn€ > 100 AND block3.avg_churn€ <= 150) THEN 7
        WHEN (block3.avg_churn€ > 150 AND block3.avg_churn€ <= 200) THEN 6
        WHEN (block3.avg_churn€ > 200 AND block3.avg_churn€ <= 250) THEN 5
        WHEN (block3.avg_churn€ > 250 AND block3.avg_churn€ <= 300) THEN 4
        WHEN (block3.avg_churn€ > 300 AND block3.avg_churn€ <= 350) THEN 3
        WHEN (block3.avg_churn€ > 350 AND block3.avg_churn€ <= 400) THEN 2
        WHEN (block3.avg_churn€ > 400 AND block3.avg_churn€ <= 450) THEN 1
        WHEN block3.avg_churn€ > 450 THEN 0
   	  ELSE 0 
        END as score_weighted_churn_€,
	
	block3.offers_received_this_month,
	block3.offers_received_last_month,
	block3.offers_received_last_month2,
	block3.offers_accepted_this_month,
	block3.offers_accepted_last_month,	     
	block3.offers_accepted_last_month2,		
	block3.avg_offers_accepted as pc_accepted_weighted,
	
	CASE WHEN block3.avg_offers_accepted = 0 THEN 0
		  WHEN block3.avg_offers_accepted > 0 AND block3.avg_offers_accepted <= 0.1 THEN 1
		  WHEN block3.avg_offers_accepted > 0.1 AND block3.avg_offers_accepted <= 0.2 THEN 2
		  WHEN block3.avg_offers_accepted > 0.2 AND block3.avg_offers_accepted <= 0.3 THEN 3
		  WHEN block3.avg_offers_accepted > 0.3 AND block3.avg_offers_accepted <= 0.4 THEN 4
		  WHEN block3.avg_offers_accepted > 0.4 AND block3.avg_offers_accepted <= 0.5 THEN 5
		  WHEN block3.avg_offers_accepted > 0.5 AND block3.avg_offers_accepted <= 0.6 THEN 6
		  WHEN block3.avg_offers_accepted > 0.6 AND block3.avg_offers_accepted <= 0.7 THEN 7
		  WHEN block3.avg_offers_accepted > 0.7 AND block3.avg_offers_accepted <= 0.8 THEN 8
		  WHEN block3.avg_offers_accepted > 0.8 AND block3.avg_offers_accepted <= 0.9 THEN 9
		  WHEN block3.avg_offers_accepted > 0.9 AND block3.avg_offers_accepted <= 1 THEN 10
		  ELSE 11
		  END as score_acceptance_weighted,
	
	block3.orders_submitted_this_month,
	block3.orders_submitted_last_month,
	block3.orders_submitted_last_month2,
	block3.orders_checked_this_month,
	block3.orders_checked_last_month,
	block3.orders_checked_last_month2,	
	block3.avg_orders_checked as pc_checked_weighted,
		  
	CASE WHEN block3.avg_orders_checked = 0 THEN 0
		  WHEN block3.avg_orders_checked > 0 AND block3.avg_orders_checked <= 0.1 THEN 1
		  WHEN block3.avg_orders_checked > 0.1 AND block3.avg_orders_checked <= 0.2 THEN 2
		  WHEN block3.avg_orders_checked > 0.2 AND block3.avg_orders_checked <= 0.3 THEN 3
		  WHEN block3.avg_orders_checked > 0.3 AND block3.avg_orders_checked <= 0.4 THEN 4
		  WHEN block3.avg_orders_checked > 0.4 AND block3.avg_orders_checked <= 0.5 THEN 5
		  WHEN block3.avg_orders_checked > 0.5 AND block3.avg_orders_checked <= 0.6 THEN 6
		  WHEN block3.avg_orders_checked > 0.6 AND block3.avg_orders_checked <= 0.7 THEN 7
		  WHEN block3.avg_orders_checked > 0.7 AND block3.avg_orders_checked <= 0.8 THEN 8
		  WHEN block3.avg_orders_checked > 0.8 AND block3.avg_orders_checked <= 0.9 THEN 9
		  WHEN block3.avg_orders_checked > 0.9 AND block3.avg_orders_checked <= 1 THEN 10
		  ELSE 11
		  END as score_checked_weighted,
	
	block3.cases_this_month,
	block3.cases_last_month,
	block3.cases_last_month2,
	block3.avg_number_cases as number_cases


FROM	
	
	(SELECT
	
		block2.sfid,
		block2.partner,
		block2.city,
		hours_city.hours_executed_this_month as hours_city,
		block2.hours_executed_this_month,	
		CASE WHEN block2.city = 'de-berlin' THEN 0.2
			  WHEN block2.city = 'de-cologne' THEN 0.35
			  WHEN block2.city = 'de-dusseldorf' THEN 0.4
			  WHEN block2.city = 'de-frankfurt' THEN 0.5
			  WHEN block2.city = 'de-hamburg' THEN 0.35
			  WHEN block2.city = 'de-hannover' THEN 0.6
			  WHEN block2.city = 'de-leipzig' THEN 0.5
			  WHEN block2.city = 'de-munich' THEN 0.35
			  WHEN block2.city = 'de-stuttgart' THEN 0.5
			  ELSE 1
			  END as threshold_hours,
		block2.revenue_last_month,
		block2.revenue_last_month2,
		CASE WHEN block2.revenue_last_month = 0 THEN 0
				WHEN (block2.revenue_last_month > 0 AND block2.revenue_last_month <= 500) THEN 1
				WHEN (block2.revenue_last_month > 500 AND block2.revenue_last_month <= 1000) THEN 2
				WHEN (block2.revenue_last_month > 1000 AND block2.revenue_last_month <= 1500) THEN 3
				WHEN (block2.revenue_last_month > 1500 AND block2.revenue_last_month <= 2000) THEN 4
				WHEN (block2.revenue_last_month > 2000 AND block2.revenue_last_month <= 2500) THEN 5
				WHEN (block2.revenue_last_month > 2500 AND block2.revenue_last_month <= 3000) THEN 6
				WHEN (block2.revenue_last_month > 3000 AND block2.revenue_last_month <= 3500) THEN 7
				WHEN (block2.revenue_last_month > 3500 AND block2.revenue_last_month <= 4000) THEN 8
				WHEN (block2.revenue_last_month > 4000 AND block2.revenue_last_month <= 4500) THEN 9
				WHEN (block2.revenue_last_month > 4500 AND block2.revenue_last_month <= 5000) THEN 10
				WHEN (block2.revenue_last_month > 5000 AND block2.revenue_last_month <= 5500) THEN 11
				WHEN (block2.revenue_last_month > 5500 AND block2.revenue_last_month <= 6000) THEN 12
				WHEN (block2.revenue_last_month > 6000 AND block2.revenue_last_month <= 6500) THEN 13
				WHEN (block2.revenue_last_month > 6500 AND block2.revenue_last_month <= 7000) THEN 14
				WHEN (block2.revenue_last_month > 7000 AND block2.revenue_last_month <= 7500) THEN 15
				WHEN (block2.revenue_last_month > 7500 AND block2.revenue_last_month <= 8000) THEN 16
				WHEN (block2.revenue_last_month > 8000 AND block2.revenue_last_month <= 8500) THEN 17
				WHEN (block2.revenue_last_month > 8500 AND block2.revenue_last_month <= 9000) THEN 18
				WHEN (block2.revenue_last_month > 9000 AND block2.revenue_last_month <= 9500) THEN 19
				WHEN (block2.revenue_last_month > 9500) THEN 20
				ELSE 0
				END as score_revenue,
		block2.operational_costs_last_month,
		CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END as gpm,
	
		block2.active_pro_this_month,
		block2.active_pro_last_month,
		block2.active_pro_last_month2,
		block2.churn_this_month,
		block2.churn_last_month,
		block2.churn_last_month2,
		0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
		block2.churn_this_month€,
		block2.churn_last_month€,
		block2.churn_last_month2€,  
		0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
		block2.offers_received_this_month,
		block2.offers_received_last_month,
		block2.offers_received_last_month2,
		block2.offers_accepted_this_month,
		block2.offers_accepted_last_month,	     
		block2.offers_accepted_last_month2,
			
		0.6*(CASE WHEN block2.offers_received_this_month > 0 
		     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.offers_received_last_month > 0
			  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.offers_received_last_month2 > 0
		     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
			  ELSE 0
			  END) as avg_offers_accepted,
		
		block2.orders_submitted_this_month,
		block2.orders_submitted_last_month,
		block2.orders_submitted_last_month2,
		block2.orders_checked_this_month,
		block2.orders_checked_last_month,
		block2.orders_checked_last_month2,
		
		0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
		     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.orders_submitted_last_month > 0
			  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
		     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
			  ELSE 0
			  END) as avg_orders_checked,
			  
		block2.cases_this_month,
		block2.cases_last_month,
		block2.cases_last_month2,
		0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
	
	FROM
			
		(SELECT
		
			block1.sfid,
			block1.partner,
			-- block1.year_month,
			block1.city,
			SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
			     ELSE block1.revenue_last_month
			     END) as revenue_last_month,
			SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
			     ELSE block1.revenue_last_month2
			     END) as revenue_last_month2,
			SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
			     ELSE block1.operational_costs_last_month
			     END) as operational_costs_last_month,
			SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
			     ELSE block1.hours_executed_this_month
			     END) as hours_executed_this_month,
			SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
			     ELSE block1.active_pro_this_month
			     END) as active_pro_this_month,
			SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
			     ELSE block1.active_pro_last_month
			     END) as active_pro_last_month,
			SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
			     ELSE block1.active_pro_last_month2
			     END) as active_pro_last_month2,
			SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
			     ELSE block1.churn_this_month
			     END) as churn_this_month,
			SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
			     ELSE block1.churn_last_month
			     END) as churn_last_month,
			SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
			     ELSE block1.churn_last_month2
			     END) as churn_last_month2,
			SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
			     ELSE block1.churn_this_month€
			     END) as churn_this_month€,
			SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
			     ELSE block1.churn_last_month€
			     END) as churn_last_month€,
			SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
			     ELSE block1.churn_last_month2€
			     END) as churn_last_month2€,     
			SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
			     ELSE block1.offers_received_this_month
			     END) as offers_received_this_month,
			SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
			     ELSE block1.offers_received_last_month
			     END) as offers_received_last_month,
			SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_received_last_month2
			     END) as offers_received_last_month2,
			SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_this_month
			     END) as offers_accepted_this_month,
			SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month
			     END) as offers_accepted_last_month,	     
			SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month2
			     END) as offers_accepted_last_month2,
			SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_this_month
			     END) as orders_submitted_this_month,
			SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month
			     END) as orders_submitted_last_month,
			SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month2
			     END) as orders_submitted_last_month2,
			SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
			     ELSE block1.orders_checked_this_month
			     END) as orders_checked_this_month,
			SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month
			     END) as orders_checked_last_month,
			SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month2
			     END) as orders_checked_last_month2,	
			SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
			     ELSE block1.cases_this_month
			     END) as cases_this_month,
			SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
			     ELSE block1.cases_last_month
			     END) as cases_last_month,
			SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
			     ELSE block1.cases_last_month2
			     END) as cases_last_month2
				
		FROM	
			
			(SELECT
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month,
				in_kpi_master.city,
				MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
				MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
				MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
				MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
				MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
				MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
				MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
				MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
				
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_this_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month2,
										
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_this_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month2€,
				MAX(offers.offers_received_this_month) as offers_received_this_month,
				MAX(offers.offers_received_last_month) as offers_received_last_month,
				MAX(offers.offers_received_last_month2) as offers_received_last_month2,
				MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
				MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
				MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
				
				MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
				MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
				MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
				MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
				MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
				MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
				
				MAX(number_cases.cases_this_month) as cases_this_month,
				MAX(number_cases.cases_last_month) as cases_last_month,
				MAX(number_cases.cases_last_month2) as cases_last_month2
									
			FROM
				
				
				((SELECT -- Simple list of all the accounts with the role master
				
					a.sfid,
					a.delivery_areas__c,
					a.name as partner
				
				FROM
				
					salesforce.account a 
						
				WHERE
				
					a.test__c IS FALSE
					AND a.role__c = 'master') as t0
					
				LEFT JOIN
					
					(SELECT 
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner2,
						o.city,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as revenue_last_month,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as revenue_last_month2,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
										(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
				                        THEN o.value
							               ELSE 0 END) 
										      END) as cost_supply_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as operational_costs_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as active_pro_last_month2									
														
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
											
					GROUP BY
					
						o.date_part,
						partner2,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner2 asc) as t1
						
				ON
				
					t0.partner = t1.partner2) as in_kpi_master
					
			LEFT JOIN
			
				bi.deep_churn as deep_churn1
				
			ON
			
				in_kpi_master.partner = deep_churn1.company_name
				AND in_kpi_master.city = deep_churn1.city
				AND in_kpi_master.year_month = deep_churn1."year_month"
				
			LEFT JOIN
			
				(SELECT
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name as company_name__c,
					offers1.delivery_area__c,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
					
				
				FROM
				
					(SELECT
									
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
						MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
						a.sfid,
						a.name,
						a.company_name__c,
						ooo.delivery_area__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c,
						COUNT(o.suggested_partner__c) as offers_received,
						SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
					
					
					FROM
					
						salesforce.account a
					
					
					LEFT JOIN
					
						salesforce.partner_offer_partner__c o
							
					ON
					
						a.sfid = o.suggested_partner__c 
						
					LEFT JOIN
					
						salesforce.partner_offer__c oo
						
					ON 
					
						o.partner_offer__c = oo.sfid
						
					LEFT JOIN
					
						salesforce.opportunity ooo
						
					ON
					
						oo.opportunity__c = ooo.sfid
						
					WHERE
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
						AND a.test__c IS FALSE
						
					GROUP BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
						a.sfid,
						a.name,
						a.company_name__c,
						ooo.delivery_area__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c
					
					ORDER BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
						a.name asc) as offers1
						
				GROUP BY
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name,
					offers1.delivery_area__c) as offers
					
			ON
			
				in_kpi_master.partner = offers.company_name__c
				AND in_kpi_master.city = offers.delivery_area__c
				AND in_kpi_master.year_month = offers.year_month_offer
				
			LEFT JOIN
			
				(SELECT
				
					orders_checked1.year_month,
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.polygon,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
				
				FROM	
					
					(SELECT
											
					  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
					  MIN(effectivedate::date) as mindate,
					  MAX(effectivedate::date) as maxdate,
					  t4.delivery_area__c as polygon,
					  t3.subcon as name_partner,
					  t3.sfid_partner,
					  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
					  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
					
					FROM
					
					  (SELECT
					  
					     t5.*,
					     t2.name as subcon,
					     t2.sfid as sfid_partner
					     
					   FROM
					   
					   	Salesforce.Account t5
					   
						JOIN
					      
							Salesforce.Account t2
					   
						ON
					   
							(t2.sfid = t5.parentid)
					     
						WHERE 
						
							t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
							-- AND t5.type__c = 'partner'
							AND LEFT(t5.locale__c, 2) = 'de'
							-- AND t5.role__c = 'master'
							AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
							AND t5.name NOT LIKE '%Handyman Kovacs%'
							AND t5.name NOT LIKE '%BAT Business Services GmbH%'
					   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
					   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
					      
					JOIN 
					
					  salesforce.order t4
					  
					ON
					
					  (t3.sfid = t4.professional__c)
					  
					WHERE
					
					  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
					  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
					  and LEFT(t4.locale__c,2) IN ('de')
					  and t4.effectivedate < current_date
					  AND t4.type = 'cleaning-b2b'
					  AND t4.test__c IS FALSE
					
					GROUP BY
					
					  year_month,
					  polygon,
					  t3.subcon,
					  t3.sfid_partner
					  
					  
					ORDER BY
						
					  year_month desc,
					  polygon,
					  t3.subcon) as orders_checked1
					  
				GROUP BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month,
					orders_checked1.polygon
					
				ORDER BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month desc) as orders_checked
					
			ON
			
				in_kpi_master.partner = orders_checked.name_partner
				AND in_kpi_master.city = orders_checked.polygon
				AND in_kpi_master.year_month = orders_checked.year_month
				
			LEFT JOIN
			
				(SELECT
				
					t3.year_month,
					t3.partnerid,
					t4.name as name_partner,
					t3.delivery_area__c,
					SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 32)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 62)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
					
				FROM
							
					(SELECT
					
						TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
						CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
						t2.name_partner,
						t2.parentid,
						COUNT(DISTINCT t2.sfid_case) as number_cases,
						t2.delivery_area__c
					
					FROM	
					
						(SELECT
						
							t1.date_case as date_case,
							CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
							t1.sfid_case,
							a.name as name_partner,
							a.parentid,
							t1.delivery_area__c
							
						FROM	
							
							(SELECT 
							
								c.createddate as date_case,
								c.CaseNumber, 
								c.sfid as sfid_case, 
								o.sfid, a.sfid, 
								o.delivery_area__c,
								c.origin, 
								c.*
							
							FROM 
							
								salesforce.case c
								
							LEFT JOIN 
							
								salesforce.opportunity o
							
							ON
								o.sfid = c.opportunity__c
								
							LEFT JOIN 
							
								salesforce.account a on a.sfid = c.accountid
								
							WHERE 
							
								c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
								AND COALESCE(c.Origin, '') != 'System - Notification'
								AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
								AND c.type != 'CM B2C'
								AND c.CreatedDate >= (current_date - 103)
								AND c.test__c IS FALSE
								AND a.test__c IS FALSE
								AND COALESCE(o.name, '') NOT LIKE '%test%' 
								-- SFID from ## account
								AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
								-- SFID from BAT Business Services GmbH account
								AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
								AND c.parentid IS NULL) as t1
								
						LEFT JOIN
						
							salesforce.account a
							
						ON
						
							(t1.accountid = a.sfid)
							-- OR t1.partner__c = a.sfid)
							
						WHERE 
						
							a.name IS NOT NULL) as t2
							
					GROUP BY 
					
						TO_CHAR(t2.date_case, 'YYYY-MM'),
						t2.partner,
						t2.name_partner,
						t2.parentid,
						t2.delivery_area__c
						
					ORDER BY
					
						t2.name_partner,
						TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
						
				LEFT JOIN
				
					salesforce.account t4
					
				ON
				
					t3.partnerid = t4.sfid
					
				GROUP BY
				
					t3.year_month,
					t3.partnerid,
					t3.delivery_area__c,
					t4.name) as number_cases
					
			ON
			
				in_kpi_master.partner = number_cases.name_partner
				AND in_kpi_master.city = number_cases.delivery_area__c
				AND in_kpi_master.year_month = number_cases.year_month	
			
			GROUP BY
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month,
				in_kpi_master.city
				
			ORDER BY
			
				in_kpi_master.sfid,
				in_kpi_master.year_month desc) as block1
	
		WHERE
	
			block1.year_month IS NOT NULL
					
		GROUP BY
		
			block1.sfid,
			block1.partner,
			block1.city) as block2
			
	LEFT JOIN
	
		(SELECT 
	
			o.city,
			SUM(CASE WHEN 
				         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
				               THEN o.value
							      ELSE 0 END) IS NULL THEN 0
						ELSE 
						   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
			                  THEN o.value
						         ELSE 0 END) 
									END) as hours_executed_this_month								
											
		FROM
		
			bi.kpi_master o
			
		WHERE
		
			(o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
		
		GROUP BY
		
			o.city) as hours_city
			
	ON
	
		block2.city = hours_city.city
	
	WHERE
	
		block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3;				

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.scorecard_partners2;
CREATE TABLE bi.scorecard_partners2 as 


SELECT

	block3.sfid,
	block3.partner,
	block3.hours_executed_this_month as hours_executed,	

	block3.revenue_last_month,
	block3.revenue_last_month2,
	block3.score_revenue,
	CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation revenue €'
				  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue €'
				  ELSE 'Decreasing revenue €'
				  END as trend,
	block3.operational_costs_last_month,
	block3.gpm as gpm_last_month,
	
	CASE WHEN block3.gpm <= 0 THEN 0
		  WHEN block3.gpm > 0 AND block3.gpm <= 0.02 THEN 1
		  WHEN block3.gpm > 0.02 AND block3.gpm <= 0.04 THEN 2
		  WHEN block3.gpm > 0.04 AND block3.gpm <= 0.06 THEN 3
		  WHEN block3.gpm > 0.06 AND block3.gpm <= 0.08 THEN 4
		  WHEN block3.gpm > 0.08 AND block3.gpm <= 0.1 THEN 5
		  WHEN block3.gpm > 0.1 AND block3.gpm <= 0.12 THEN 6
		  WHEN block3.gpm > 0.12 AND block3.gpm <= 0.14 THEN 7
		  WHEN block3.gpm > 0.14 AND block3.gpm <= 0.16 THEN 8
		  WHEN block3.gpm > 0.16 AND block3.gpm <= 0.18 THEN 9
		  WHEN block3.gpm > 0.18 AND block3.gpm <= 0.20 THEN 10
		  WHEN block3.gpm > 0.2 AND block3.gpm <= 0.22 THEN 11
		  WHEN block3.gpm > 0.22 AND block3.gpm <= 0.24 THEN 12
		  WHEN block3.gpm > 0.24 AND block3.gpm <= 0.26 THEN 13
		  WHEN block3.gpm > 0.26 AND block3.gpm <= 0.28 THEN 14
		  WHEN block3.gpm > 0.28 THEN 15
		  ELSE 0
		  END as score_gpm,

	block3.active_pro_this_month,
	block3.active_pro_last_month,
	block3.active_pro_last_month2,
	CASE WHEN block3.active_pro_last_month = 0 THEN 0
	     WHEN (block3.active_pro_last_month > 0 AND block3.active_pro_last_month <= 2) THEN 1
	     WHEN (block3.active_pro_last_month > 2 AND block3.active_pro_last_month <= 4) THEN 2
	     WHEN (block3.active_pro_last_month > 4 AND block3.active_pro_last_month <= 6) THEN 3
	     WHEN (block3.active_pro_last_month > 6 AND block3.active_pro_last_month <= 8) THEN 4
	     WHEN block3.active_pro_last_month > 8  THEN 5
	     ELSE 0
	     END as score_active_pro,
	CASE WHEN block3.active_pro_this_month > 0 
		  THEN block3.hours_executed_this_month/block3.active_pro_this_month
		  ELSE 0
		  END as hours_per_cleaner,
		  
	CASE WHEN (CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) = 0 THEN 0
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 0 AND (CASE WHEN block3.active_pro_this_month > 0 
													THEN block3.hours_executed_this_month/block3.active_pro_this_month
													ELSE 0
													END) <= 4) THEN 1
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 4 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 8) THEN 2
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 8 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 12) THEN 3
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 12 AND (CASE WHEN block3.active_pro_this_month > 0 
													 THEN block3.hours_executed_this_month/block3.active_pro_this_month
													 ELSE 0
													 END) <= 16) THEN 4
	     WHEN (CASE WHEN block3.active_pro_this_month > 0 
		  				 THEN block3.hours_executed_this_month/block3.active_pro_this_month
						 ELSE 0
						 END) > 16 THEN 5
					    ELSE 100
					    END as score_hours_cleaners,
	     
	CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
		  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
		  ELSE 'Decreasing # workers'
		  END as trend_active_pros,
	block3.churn_this_month,
	block3.churn_last_month,
	block3.churn_last_month2,
	block3.avg_churn as weighted_churn,
	
	CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
	     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
	     ELSE 0 
	     END as score_weighted_churn,
	
	block3.churn_this_month€,
	block3.churn_last_month€,
	block3.churn_last_month2€,  
	block3.avg_churn€ as weighted_churn_€,   
	
	CASE WHEN (block3.avg_churn€ = 0 OR block3.avg_churn€ IS NULL) THEN 10
        WHEN (block3.avg_churn€ > 0 AND block3.avg_churn€ <= 50) THEN 9
        WHEN (block3.avg_churn€ > 50 AND block3.avg_churn€ <= 100) THEN 8
        WHEN (block3.avg_churn€ > 100 AND block3.avg_churn€ <= 150) THEN 7
        WHEN (block3.avg_churn€ > 150 AND block3.avg_churn€ <= 200) THEN 6
        WHEN (block3.avg_churn€ > 200 AND block3.avg_churn€ <= 250) THEN 5
        WHEN (block3.avg_churn€ > 250 AND block3.avg_churn€ <= 300) THEN 4
        WHEN (block3.avg_churn€ > 300 AND block3.avg_churn€ <= 350) THEN 3
        WHEN (block3.avg_churn€ > 350 AND block3.avg_churn€ <= 400) THEN 2
        WHEN (block3.avg_churn€ > 400 AND block3.avg_churn€ <= 450) THEN 1
        WHEN block3.avg_churn€ > 450 THEN 0
   	  ELSE 0 
        END as score_weighted_churn_€,
	
	block3.offers_received_this_month,
	block3.offers_received_last_month,
	block3.offers_received_last_month2,
	block3.offers_accepted_this_month,
	block3.offers_accepted_last_month,	     
	block3.offers_accepted_last_month2,		
	block3.avg_offers_accepted as pc_accepted_weighted,
	
	CASE WHEN block3.avg_offers_accepted = 0 THEN 0
		  WHEN block3.avg_offers_accepted > 0 AND block3.avg_offers_accepted <= 0.1 THEN 1
		  WHEN block3.avg_offers_accepted > 0.1 AND block3.avg_offers_accepted <= 0.2 THEN 2
		  WHEN block3.avg_offers_accepted > 0.2 AND block3.avg_offers_accepted <= 0.3 THEN 3
		  WHEN block3.avg_offers_accepted > 0.3 AND block3.avg_offers_accepted <= 0.4 THEN 4
		  WHEN block3.avg_offers_accepted > 0.4 AND block3.avg_offers_accepted <= 0.5 THEN 5
		  WHEN block3.avg_offers_accepted > 0.5 AND block3.avg_offers_accepted <= 0.6 THEN 6
		  WHEN block3.avg_offers_accepted > 0.6 AND block3.avg_offers_accepted <= 0.7 THEN 7
		  WHEN block3.avg_offers_accepted > 0.7 AND block3.avg_offers_accepted <= 0.8 THEN 8
		  WHEN block3.avg_offers_accepted > 0.8 AND block3.avg_offers_accepted <= 0.9 THEN 9
		  WHEN block3.avg_offers_accepted > 0.9 AND block3.avg_offers_accepted <= 1 THEN 10
		  ELSE 11
		  END as score_acceptance_weighted,
	
	block3.orders_submitted_this_month,
	block3.orders_submitted_last_month,
	block3.orders_submitted_last_month2,
	block3.orders_checked_this_month,
	block3.orders_checked_last_month,
	block3.orders_checked_last_month2,	
	block3.avg_orders_checked as pc_checked_weighted,
		  
	CASE WHEN block3.avg_orders_checked = 0 THEN 0
		  WHEN block3.avg_orders_checked > 0 AND block3.avg_orders_checked <= 0.1 THEN 1
		  WHEN block3.avg_orders_checked > 0.1 AND block3.avg_orders_checked <= 0.2 THEN 2
		  WHEN block3.avg_orders_checked > 0.2 AND block3.avg_orders_checked <= 0.3 THEN 3
		  WHEN block3.avg_orders_checked > 0.3 AND block3.avg_orders_checked <= 0.4 THEN 4
		  WHEN block3.avg_orders_checked > 0.4 AND block3.avg_orders_checked <= 0.5 THEN 5
		  WHEN block3.avg_orders_checked > 0.5 AND block3.avg_orders_checked <= 0.6 THEN 6
		  WHEN block3.avg_orders_checked > 0.6 AND block3.avg_orders_checked <= 0.7 THEN 7
		  WHEN block3.avg_orders_checked > 0.7 AND block3.avg_orders_checked <= 0.8 THEN 8
		  WHEN block3.avg_orders_checked > 0.8 AND block3.avg_orders_checked <= 0.9 THEN 9
		  WHEN block3.avg_orders_checked > 0.9 AND block3.avg_orders_checked <= 1 THEN 10
		  ELSE 11
		  END as score_checked_weighted,
	
	block3.cases_this_month,
	block3.cases_last_month,
	block3.cases_last_month2,
	block3.avg_number_cases as number_cases


FROM	
	
	(SELECT
	
		block2.sfid,
		block2.partner,
		block2.hours_executed_this_month,	
		block2.revenue_last_month,
		block2.revenue_last_month2,
		CASE WHEN block2.revenue_last_month = 0 THEN 0
				WHEN (block2.revenue_last_month > 0 AND block2.revenue_last_month <= 500) THEN 1
				WHEN (block2.revenue_last_month > 500 AND block2.revenue_last_month <= 1000) THEN 2
				WHEN (block2.revenue_last_month > 1000 AND block2.revenue_last_month <= 1500) THEN 3
				WHEN (block2.revenue_last_month > 1500 AND block2.revenue_last_month <= 2000) THEN 4
				WHEN (block2.revenue_last_month > 2000 AND block2.revenue_last_month <= 2500) THEN 5
				WHEN (block2.revenue_last_month > 2500 AND block2.revenue_last_month <= 3000) THEN 6
				WHEN (block2.revenue_last_month > 3000 AND block2.revenue_last_month <= 3500) THEN 7
				WHEN (block2.revenue_last_month > 3500 AND block2.revenue_last_month <= 4000) THEN 8
				WHEN (block2.revenue_last_month > 4000 AND block2.revenue_last_month <= 4500) THEN 9
				WHEN (block2.revenue_last_month > 4500 AND block2.revenue_last_month <= 5000) THEN 10
				WHEN (block2.revenue_last_month > 5000 AND block2.revenue_last_month <= 5500) THEN 11
				WHEN (block2.revenue_last_month > 5500 AND block2.revenue_last_month <= 6000) THEN 12
				WHEN (block2.revenue_last_month > 6000 AND block2.revenue_last_month <= 6500) THEN 13
				WHEN (block2.revenue_last_month > 6500 AND block2.revenue_last_month <= 7000) THEN 14
				WHEN (block2.revenue_last_month > 7000 AND block2.revenue_last_month <= 7500) THEN 15
				WHEN (block2.revenue_last_month > 7500 AND block2.revenue_last_month <= 8000) THEN 16
				WHEN (block2.revenue_last_month > 8000 AND block2.revenue_last_month <= 8500) THEN 17
				WHEN (block2.revenue_last_month > 8500 AND block2.revenue_last_month <= 9000) THEN 18
				WHEN (block2.revenue_last_month > 9000 AND block2.revenue_last_month <= 9500) THEN 19
				WHEN (block2.revenue_last_month > 9500) THEN 20
				ELSE 0
				END as score_revenue,
		block2.operational_costs_last_month,
		CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END as gpm,
	
		block2.active_pro_this_month,
		block2.active_pro_last_month,
		block2.active_pro_last_month2,
		block2.churn_this_month,
		block2.churn_last_month,
		block2.churn_last_month2,
		0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
		block2.churn_this_month€,
		block2.churn_last_month€,
		block2.churn_last_month2€,  
		0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
		block2.offers_received_this_month,
		block2.offers_received_last_month,
		block2.offers_received_last_month2,
		block2.offers_accepted_this_month,
		block2.offers_accepted_last_month,	     
		block2.offers_accepted_last_month2,
			
		0.6*(CASE WHEN block2.offers_received_this_month > 0 
		     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.offers_received_last_month > 0
			  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.offers_received_last_month2 > 0
		     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
			  ELSE 0
			  END) as avg_offers_accepted,
		
		block2.orders_submitted_this_month,
		block2.orders_submitted_last_month,
		block2.orders_submitted_last_month2,
		block2.orders_checked_this_month,
		block2.orders_checked_last_month,
		block2.orders_checked_last_month2,
		
		0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
		     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.orders_submitted_last_month > 0
			  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
		     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
			  ELSE 0
			  END) as avg_orders_checked,
			  
		block2.cases_this_month,
		block2.cases_last_month,
		block2.cases_last_month2,
		0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
	
	FROM
			
		(SELECT
		
			block1.sfid,
			block1.partner,
			-- block1.year_month,
			SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
			     ELSE block1.revenue_last_month
			     END) as revenue_last_month,
			SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
			     ELSE block1.revenue_last_month2
			     END) as revenue_last_month2,
			SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
			     ELSE block1.operational_costs_last_month
			     END) as operational_costs_last_month,
			SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
			     ELSE block1.hours_executed_this_month
			     END) as hours_executed_this_month,
			SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
			     ELSE block1.active_pro_this_month
			     END) as active_pro_this_month,
			SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
			     ELSE block1.active_pro_last_month
			     END) as active_pro_last_month,
			SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
			     ELSE block1.active_pro_last_month2
			     END) as active_pro_last_month2,
			SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
			     ELSE block1.churn_this_month
			     END) as churn_this_month,
			SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
			     ELSE block1.churn_last_month
			     END) as churn_last_month,
			SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
			     ELSE block1.churn_last_month2
			     END) as churn_last_month2,
			SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
			     ELSE block1.churn_this_month€
			     END) as churn_this_month€,
			SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
			     ELSE block1.churn_last_month€
			     END) as churn_last_month€,
			SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
			     ELSE block1.churn_last_month2€
			     END) as churn_last_month2€,     
			SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
			     ELSE block1.offers_received_this_month
			     END) as offers_received_this_month,
			SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
			     ELSE block1.offers_received_last_month
			     END) as offers_received_last_month,
			SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_received_last_month2
			     END) as offers_received_last_month2,
			SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_this_month
			     END) as offers_accepted_this_month,
			SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month
			     END) as offers_accepted_last_month,	     
			SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month2
			     END) as offers_accepted_last_month2,
			SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_this_month
			     END) as orders_submitted_this_month,
			SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month
			     END) as orders_submitted_last_month,
			SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month2
			     END) as orders_submitted_last_month2,
			SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
			     ELSE block1.orders_checked_this_month
			     END) as orders_checked_this_month,
			SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month
			     END) as orders_checked_last_month,
			SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month2
			     END) as orders_checked_last_month2,	
			SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
			     ELSE block1.cases_this_month
			     END) as cases_this_month,
			SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
			     ELSE block1.cases_last_month
			     END) as cases_last_month,
			SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
			     ELSE block1.cases_last_month2
			     END) as cases_last_month2
				
		FROM	
			
			(SELECT
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month,
				MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
				MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
				MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
				MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
				MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
				MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
				MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
				MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
				
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_this_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month2,
										
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_this_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month2€,
				MAX(offers.offers_received_this_month) as offers_received_this_month,
				MAX(offers.offers_received_last_month) as offers_received_last_month,
				MAX(offers.offers_received_last_month2) as offers_received_last_month2,
				MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
				MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
				MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
				
				MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
				MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
				MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
				MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
				MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
				MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
				
				MAX(number_cases.cases_this_month) as cases_this_month,
				MAX(number_cases.cases_last_month) as cases_last_month,
				MAX(number_cases.cases_last_month2) as cases_last_month2
									
			FROM
				
				
				((SELECT -- Simple list of all the accounts with the role master
				
					a.sfid,
					a.delivery_areas__c,
					a.name as partner
				
				FROM
				
					salesforce.account a 
						
				WHERE
				
					a.test__c IS FALSE
					AND a.role__c = 'master') as t0
					
				LEFT JOIN
					
					(SELECT 
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner2,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as revenue_last_month,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as revenue_last_month2,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
										(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
				                        THEN o.value
							               ELSE 0 END) 
										      END) as cost_supply_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as operational_costs_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as active_pro_last_month2									
														
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
											
					GROUP BY
					
						o.date_part,
						partner2
						
					ORDER BY
					
						year_month desc,
						partner2 asc) as t1
						
				ON
				
					t0.partner = t1.partner2) as in_kpi_master
					
			LEFT JOIN
			
				bi.deep_churn as deep_churn1
				
			ON
			
				in_kpi_master.partner = deep_churn1.company_name
				AND in_kpi_master.year_month = deep_churn1."year_month"
				
			LEFT JOIN
			
				(SELECT
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name as company_name__c,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
					
				
				FROM
				
					(SELECT
									
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
						MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
						a.sfid,
						a.name,
						a.company_name__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c,
						COUNT(o.suggested_partner__c) as offers_received,
						SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
					
					
					FROM
					
						salesforce.account a
					
					
					LEFT JOIN
					
						salesforce.partner_offer_partner__c o
							
					ON
					
						a.sfid = o.suggested_partner__c 
						
					LEFT JOIN
					
						salesforce.partner_offer__c oo
						
					ON 
					
						o.partner_offer__c = oo.sfid
						
					LEFT JOIN
					
						salesforce.opportunity ooo
						
					ON
					
						oo.opportunity__c = ooo.sfid
						
					WHERE
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
						AND a.test__c IS FALSE
						
					GROUP BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
						a.sfid,
						a.name,
						a.company_name__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c
					
					ORDER BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
						a.name asc) as offers1
						
				GROUP BY
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name) as offers
					
			ON
			
				in_kpi_master.partner = offers.company_name__c
				AND in_kpi_master.year_month = offers.year_month_offer
				
			LEFT JOIN
			
				(SELECT
				
					orders_checked1.year_month,
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
				
				FROM	
					
					(SELECT
											
					  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
					  MIN(effectivedate::date) as mindate,
					  MAX(effectivedate::date) as maxdate,
					  t3.subcon as name_partner,
					  t3.sfid_partner,
					  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
					  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
					
					FROM
					
					  (SELECT
					  
					     t5.*,
					     t2.name as subcon,
					     t2.sfid as sfid_partner
					     
					   FROM
					   
					   	Salesforce.Account t5
					   
						JOIN
					      
							Salesforce.Account t2
					   
						ON
					   
							(t2.sfid = t5.parentid)
					     
						WHERE 
						
							t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
							-- AND t5.type__c = 'partner'
							AND LEFT(t5.locale__c, 2) = 'de'
							-- AND t5.role__c = 'master'
							AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
							AND t5.name NOT LIKE '%Handyman Kovacs%'
							AND t5.name NOT LIKE '%BAT Business Services GmbH%'
					   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
					   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
					      
					JOIN 
					
					  salesforce.order t4
					  
					ON
					
					  (t3.sfid = t4.professional__c)
					  
					WHERE
					
					  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
					  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
					  and LEFT(t4.locale__c,2) IN ('de')
					  and t4.effectivedate < current_date
					  AND t4.type = 'cleaning-b2b'
					  AND t4.test__c IS FALSE
					
					GROUP BY
					
					  year_month,
					  t3.subcon,
					  t3.sfid_partner
					  
					  
					ORDER BY
						
					  year_month desc,
					  t3.subcon) as orders_checked1
					  
				GROUP BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month
					
				ORDER BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month desc) as orders_checked
					
			ON
			
				in_kpi_master.partner = orders_checked.name_partner
				AND in_kpi_master.year_month = orders_checked.year_month
				
			LEFT JOIN
			
				(SELECT
				
					t3.year_month,
					t3.partnerid,
					t4.name as name_partner,
					SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 32)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 62)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
					
				FROM
							
					(SELECT
					
						TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
						CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
						t2.name_partner,
						t2.parentid,
						COUNT(DISTINCT t2.sfid_case) as number_cases
					
					FROM	
					
						(SELECT
						
							t1.date_case as date_case,
							CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
							t1.sfid_case,
							a.name as name_partner,
							a.parentid
							
						FROM	
							
							(SELECT 
							
								c.createddate as date_case,
								c.CaseNumber, 
								c.sfid as sfid_case, 
								o.sfid, 
								a.sfid,
								c.origin, 
								c.*
							
							FROM 
							
								salesforce.case c
								
							LEFT JOIN 
							
								salesforce.opportunity o
							
							ON
								o.sfid = c.opportunity__c
								
							LEFT JOIN 
							
								salesforce.account a on a.sfid = c.accountid
								
							WHERE 
							
								c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
								AND COALESCE(c.Origin, '') != 'System - Notification'
								AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
								AND c.type != 'CM B2C'
								AND c.CreatedDate >= (current_date - 103)
								AND c.test__c IS FALSE
								AND a.test__c IS FALSE
								AND COALESCE(o.name, '') NOT LIKE '%test%' 
								-- SFID from ## account
								AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
								-- SFID from BAT Business Services GmbH account
								AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
								AND c.parentid IS NULL) as t1
								
						LEFT JOIN
						
							salesforce.account a
							
						ON
						
							(t1.accountid = a.sfid)
							-- OR t1.partner__c = a.sfid)
							
						WHERE 
						
							a.name IS NOT NULL) as t2
							
					GROUP BY 
					
						TO_CHAR(t2.date_case, 'YYYY-MM'),
						t2.partner,
						t2.name_partner,
						t2.parentid
						
					ORDER BY
					
						t2.name_partner,
						TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
						
				LEFT JOIN
				
					salesforce.account t4
					
				ON
				
					t3.partnerid = t4.sfid
					
				GROUP BY
				
					t3.year_month,
					t3.partnerid,
					t4.name) as number_cases
					
			ON
			
				in_kpi_master.partner = number_cases.name_partner
				AND in_kpi_master.year_month = number_cases.year_month	
			
			GROUP BY
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month
				
			ORDER BY
			
				in_kpi_master.sfid,
				in_kpi_master.year_month desc) as block1
	
		WHERE
	
			block1.year_month IS NOT NULL
					
		GROUP BY
		
			block1.sfid,
			block1.partner) as block2
	
	WHERE
	
		block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3;