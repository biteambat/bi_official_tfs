
SELECT

	t2.year_month,
	t2.country,
	t2.locale__c,
	t2.polygon,
	t2.opportunityid,
	t2.category,
	o.status__c as status_now,
	MAX(CASE WHEN o.grand_total__c IS NULL THEN ooo.potential ELSE o.grand_total__c END)::decimal as grand_total

FROM
	
	(SELECT
		
			time_table.*,
			table_dates.*,
			CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
		
		FROM	
			
			(SELECT
				
				'1'::integer as key,	
				TO_CHAR(i, 'YYYY-MM') as year_month,
				MIN(i) as ymd,
				MAX(i) as ymd_max
				-- i::date as date 
				
			FROM
			
				generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
				
			GROUP BY
			
				key,
				year_month
				-- date
				
			ORDER BY 
			
				year_month desc) as time_table
				
		LEFT JOIN
		
			(SELECT
			
				'1'::integer as key_link,
				t1.country,
				t1.locale__c,
				t1.delivery_area__c as polygon,
				t1.opportunityid,
				t1.date_start,
				TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
				CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
				CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
				
			
			FROM
				
				((SELECT

					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					o.opportunityid,
					o.delivery_area__c,
					MIN(o.effectivedate) as date_start
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					
				GROUP BY
				
					o.opportunityid,
					o.locale__c,
					o.delivery_area__c,
					LEFT(o.locale__c, 2))) as t1
					
			LEFT JOIN
			
				(SELECT
				
					t3.*,
					CASE WHEN t3.end_contract IS NULL THEN t3.date_last_order ELSE t3.end_contract END as date_churn,
					CASE WHEN t3.date_last_order > t3.end_contract THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
				
				FROM	
					
					(SELECT
					
						t1.country,
						t1.opportunityid,
						t1.date_last_order,
						t2.date_churn as end_contract
					
					FROM
					
						(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
						         -- It's the last day on which they are making money
							LEFT(o.locale__c, 2) as country,
							o.opportunityid,
							'last_order' as type_date,
							MAX(o.effectivedate) as date_last_order
							
						FROM
						
							salesforce.order o
							
						LEFT JOIN
						
							salesforce.opportunity oo
							
						ON 
						
							o.opportunityid = oo.sfid
							
						WHERE
						
							o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
							AND oo.status__c IN ('RESIGNED', 'CANCELLED')
							AND o.type = 'cleaning-b2b'
							AND o.professional__c IS NOT NULL
							AND o.test__c IS FALSE
							AND oo.test__c IS FALSE
						
						GROUP BY
						
							LEFT(o.locale__c, 2),
							type_date,
							o.opportunityid) as t1
							
					LEFT JOIN
					
						(SELECT
						
							t3.*,
							CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
							CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
						
						FROM	
							
							(SELECT
							
								t1.country,
								t1.opportunityid,
								t1.date_last_order,
								t2.date_churn as contract_end
							
							FROM
							
								(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
								         -- It's the last day on which they are making money
									LEFT(o.locale__c, 2) as country,
									o.opportunityid,
									'last_order' as type_date,
									MAX(o.effectivedate) as date_last_order
									
								FROM
								
									salesforce.order o
									
								LEFT JOIN
								
									salesforce.opportunity oo
									
								ON 
								
									o.opportunityid = oo.sfid
									
								WHERE
								
									o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
									AND oo.status__c IN ('RESIGNED', 'CANCELLED')
									AND o.type = 'cleaning-b2b'
									AND o.professional__c IS NOT NULL
									AND o.test__c IS FALSE
									AND oo.test__c IS FALSE
								
								GROUP BY
								
									LEFT(o.locale__c, 2),
									type_date,
									o.opportunityid) as t1
									
							LEFT JOIN
							
								(SELECT -- Here we make the list of opps with their confirmed end or end date when available
														
									o.opportunity__c as opportunityid,
                           
									MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
								
								FROM
								
									salesforce.contract__c o
								
								WHERE
								
									o.test__c IS FALSE
									AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
									-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
									
								GROUP BY
								
									o.opportunity__c) as t2 
							
							ON
							
								t1.opportunityid = t2.opportunityid) as t3) as t2 
					
					ON
					
						t1.opportunityid = t2.opportunityid) as t3) as t2
					
			ON
			
				t1.opportunityid = t2.opportunityid) as table_dates
				
		ON 
		
			time_table.key = table_dates.key_link
			
	) as t2
		
LEFT JOIN

	salesforce.opportunity o
	
ON

	t2.opportunityid = o.sfid
	
LEFT JOIN

	salesforce.user ct
	
ON

	o.closed_by__c = ct.sfid
	
LEFT JOIN
	
	salesforce.user usr
	
ON

	o.ownerid = usr.sfid

LEFT JOIN

	bi.potential_revenue_per_opp ooo

ON

	o.sfid = ooo.opportunityid

WHERE 

	o.test__c IS FALSE
	AND t2.year_month IN ('2019-04', '2019-05')
	AND t2.category = 'RUNNING'
	AND t2.opportunityid = '0060J00000o4rYDQAY'
	
GROUP BY

		ooo.potential,
		o.status__c,
		o.closed_by__c,
		o.ownerid,
		ct.name,
		usr.name,
		t2.key,	
		t2.year_month,
		t2.ymd,
		t2.ymd_max,
		t2.key_link,
		t2.country,
		t2.locale__c,
		t2.polygon,
		t2.opportunityid,
		t2.date_start,
		t2.year_month_start,
		t2.date_churn,
		t2.year_month_churn,
		t2.category


	