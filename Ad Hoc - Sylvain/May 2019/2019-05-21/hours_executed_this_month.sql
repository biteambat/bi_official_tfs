(SELECT 

	o.city,
	SUM(CASE WHEN 
		         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
		               THEN o.value
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
	                  THEN o.value
				         ELSE 0 END) 
							END) as hours_executed_this_month								
									
FROM

	bi.kpi_master o
	
WHERE

	(o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))

GROUP BY

	o.city)