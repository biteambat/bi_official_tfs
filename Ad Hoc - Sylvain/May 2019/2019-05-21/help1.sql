
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.working_days_monthly;
CREATE TABLE bi.working_days_monthly as 

SELECT

	t3.year_month,
	t3.date,
	t3.weekday,
	-- t3.number_weekday,
	t3.day_type,
	t3.working_day_number,
	t3.non_working_day_number,
	CASE WHEN t3.day_type = 'Working Day'
	     THEN CONCAT(t3.day_type, ' - ', t3.working_day_number)
		  ELSE CONCAT(t3.day_type, ' - ', t3.non_working_day_number)
		  END as day_type_number,
	t3.number_working_days_in_month
	
FROM	
		
	(SELECT
	
		CASE WHEN t0.day_type = 'Working Day' THEN ROW_NUMBER() OVER 
		                 (PARTITION BY t0.year_month
		                  ORDER BY t0.day_type desc,
								         t0.date asc) ELSE 0 END as working_day_number,
		CASE WHEN t0.day_type = 'Non Working Day' THEN ROW_NUMBER() OVER 
		                 (PARTITION BY t0.year_month
		                  ORDER BY t0.day_type asc,
								         t0.date asc) ELSE 0 END as non_working_day_number,
		t0.year_month,
		t0.date,
		t0.weekday,
		t0.number_weekday,
		t0.day_type,
		t2.number_working_days_in_month
	
	FROM
		
		(SELECT 
		
			TO_CHAR(i, 'YYYY-MM') as year_month,
			i::date as date,
			TO_CHAR(i, 'day') as weekday,
			EXTRACT(DOW FROM i) as number_weekday,
			CASE WHEN EXTRACT(DOW FROM i) IN ('1', '2', '3', '4', '5') 
			          AND i NOT IN ('2018-01-01', '2018-03-30', '2018-04-02', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03', '2018-12-25', '2018-12-26',
							            '2019-01-01', '2019-04-19', '2019-04-22', '2019-05-01', '2019-05-30', '2019-06-10', '2019-10-03', '2019-12-25', '2019-12-26',
							            '2020-01-01', '2020-04-10', '2020-04-13', '2020-05-01', '2020-05-21', '2020-06-01', '2020-10-03', '2020-12-25', '2020-12-26')
				  THEN 'Working Day' ELSE 'Non Working Day' END as day_type
			
		FROM 
		
			generate_series('2017-12-31', '2021-01-01', '1 day'::interval) i
			
		ORDER BY
		
			date desc) as t0
			
	LEFT JOIN
	
		(SELECT	
			
			year_month,
			SUM(CASE WHEN day_type = 'Working Day' THEN 1 ELSE 0 END) as number_working_days_in_month
		
		FROM
		
			
			(SELECT 
			
				TO_CHAR(i, 'YYYY-MM') as year_month,
				i::date as date,
				TO_CHAR(i, 'day') as weekday,
				EXTRACT(DOW FROM i) as number_weekday,
				CASE WHEN EXTRACT(DOW FROM i) IN ('1', '2', '3', '4', '5') 
				          AND i NOT IN ('2018-01-01', '2018-03-30', '2018-04-02', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03', '2018-12-25', '2018-12-26',
							               '2019-01-01', '2019-04-19', '2019-04-22', '2019-05-01', '2019-05-30', '2019-06-10', '2019-10-03', '2019-12-25', '2019-12-26',
							               '2020-01-01', '2020-04-10', '2020-04-13', '2020-05-01', '2020-05-21', '2020-06-01', '2020-10-03', '2020-12-25', '2020-12-26'
												)
				              
				
					  THEN 'Working Day' ELSE 'Non Working Day' END as day_type
				
			FROM 
			
				generate_series('2017-12-31', '2021-01-01', '1 day'::interval) i
				
			ORDER BY
			
				date desc) as t1
				
		GROUP BY
		
			year_month
			
		ORDER BY
		
			year_month desc) as t2
	
	ON
	
		t0.year_month = t2.year_month
		
	ORDER BY
	
		-- t0.day_type desc,
		t0.date desc) as t3;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 14_b2b_orders

	DROP TABLE IF EXISTS bi.b2borders;
	CREATE TABLE bi.b2borders as 
	SELECT
		t1.contact__c,
		t2.name,
		t2.sfid as opportunity_id,
		polygon,
		t2.email__c,
		t2.locale__c,
		t2.contact_name__c,
		CASE 
 		WHEN CAST(EXTRACT(YEAR FROM effectivedate::date) as integer) = CAST(EXTRACT(YEAR FROM first_order_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM effectivedate::date) as integer)- CAST(EXTRACT(MONTH FROM first_order_date::date) as integer) 
  		WHEN CAST(EXTRACT(YEAR FROM effectivedate::date) as integer) != CAST(EXTRACT(YEAR FROM first_order_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM effectivedate::date) as integer)-CAST(EXTRACT(YEAR FROM first_order_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM effectivedate::date) as integer)- CAST(EXTRACT(MONTH FROM first_order_date::date) as integer) )
  		ELSE 0 END as returning_month,
		to_char(Effectivedate::date,'YYYY-MM') as Year_month,
		min(effectivedate::Date) as Date,
		SUM(t1.GMV__c)+MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as total_amount,
		SUM(t1.GMV__c) as Cleaning_Gross_Revenue,
		MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue,
		COUNT(1) as Total_Order_Count, 
		first_order_date,
		TO_CHAR(first_order_date,'YYYY-MM') as cohort,
		SUM(t1.Order_Duration__C) as Total_Invoiced_Hours,
		CASE WHEN SUM(Order_Duration__c) > 0 THEN (SUM(t1.GMV__c))/SUM(Order_Duration__c) ELSE NULL END as PPH,
		COUNT(1),
		SUM(CASE WHEN t3.company_name like '%BAT%' or t3.company_name like '%BOOK%' THEN Order_Duration__c ELSE 0 END) as bat_hour_share,
		SUM(Order_Duration__c) as total_hours,
		grand_total__c,
		CASE WHEN to_char(Effectivedate::date,'YYYY-MM') = to_char(first_order_date,'YYYY-MM') THEN 'New Customer' ELSE 'Existing Customer' End as customer_type,
		t2.closedate::date as sign_date,
		MAX(CASE WHEN to_char(Effectivedate::date,'YYYY-MM') = to_char(first_order_date,'YYYY-MM') THEN (32-EXTRACT(DAY FROM first_order_date))/31*grand_total__c ELSE grand_total__c END) as grand_total_calc
	FROM
		bi.orders t1
	LEFT JOIN
		Salesforce.Opportunity t2
	ON
		(t2.sfid = t1.opportunityid)
	LEFT JOIN
		(SELECT
 		a.*,
 		t2.name as company_name,
 		t2.pph__c as sub_pph
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED')  and a.test__c = '0' and a.name not like '%test%') as t3
   	ON
   		(t1.professional__c = t3.sfid)
   	LEFT JOIN
   		(SELECT
   			opportunityid,
   			min(effectivedate::date) as first_order_Date
   		FROM
   			bi.orders
   		WHERE
   			status not like '%CANCELLED%'
   		GROUP BY
   			opportunityid) as t4
   	ON
   		(t1.opportunityid = t4.opportunityid)
   	
   	
	WHERE
		t1.type = 'cleaning-b2b'
		and t1.test__c = '0'
		and t1.status not like '%CANCELLED%'

	GROUP BY
		year_month,
		polygon,
		t2.name,
		cohort,
		t2.email__c,
		t2.sfid,
		t2.locale__c,
		contact__c,
		contact_name__c,
		returning_month,
		first_order_date,
		grand_total__c,
		customer_type,
		sign_date

	;
	
DELETE FROM bi.b2b_pph_eff;	
	
INSERT INTO bi.b2b_pph_eff	
SELECT
	opportunity_id,
	year_month,
	grand_total__c,
	grand_total_calc,
	total_invoiced_hours,
	grand_total_calc/total_invoiced_hours as Eff_PPH
FROM
	bi.b2borders
WHERE
	year_month > '2017-02'
	and grand_total_calc > 0;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 18_b2brunrate

-- Signed Customers


DROP TABLE IF EXISTS bi.b2b_daily_kpis;
CREATE TABLE bi.b2b_daily_kpis as 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	
	locale;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale,
	sub_kpi;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	'Net New Hours on Platform' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	'Total gross revenue' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel Inbound' ELSE 'Sales Inbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
	and acquisition_channel__c in ('inbound','web')
GROUP BY
	date,
	locale,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'New Customer churn' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;

-- DE Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'43' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'48' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'35' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer service' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'5' as value;

-- NL Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- CH Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'17' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'3' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- New Customer End

-- New Hours Start

/*INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	left(locale,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(customers) > 0 THEN SUM(hours)/SUM(customers) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		CASE WHEN SUM (plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a
GROUP BY
	locale,
	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(customers) > 0 THEN SUM(hours)/SUM(customers) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(hours) > 0 THEN SUM(amount)/SUM(Hours) ELSE NULL END
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(hours) > 0 THEN SUM(amount)/SUM(Hours) ELSE NULL END
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;
*/

-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'889' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'791' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'97' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('19' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'234' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'270' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('28' as decimal) as value;


-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'306' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'272' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'34' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('39' as decimal) as value;


-- New Hours END

-- New Revenue Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(clients) > 0 THEN SUM(amount)/sum(clients) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
			SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as amount,
		COUNT(DISTINCT(sfid)) as clients
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20446' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'18197' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2249' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'426' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7481' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'6481' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'998' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'499' as value;

-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'11903' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'10594' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1309' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'700' as value;

-- marketing costs

-- marketing costs

INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-07-01','de','Marketing Stats','Cost Team Inbound','Actual','12792');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-07-01','nl','Marketing Stats','Cost Team Inbound','Actual','3717');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-07-01','ch','Marketing Stats','Cost Team Inbound','Actual','901');

INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-05-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-04-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-03-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','de','Marketing Stats','Cost Team','Actual','38352');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','de','Marketing Stats','Cost Team','Actual','35883');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-12-01','de','Marketing Stats','Cost Team','Actual','36135');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-11-01','de','Marketing Stats','Cost Team','Actual','37442');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-10-01','de','Marketing Stats','Cost Team','Actual','40504');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-09-01','de','Marketing Stats','Cost Team','Actual','41118');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-08-01','de','Marketing Stats','Cost Team','Actual','46110');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-07-01','de','Marketing Stats','Cost Team','Actual','44930');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-06-01','de','Marketing Stats','Cost Team','Actual','61215');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-05-01','de','Marketing Stats','Cost Team','Actual','73985');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-04-01','de','Marketing Stats','Cost Team','Actual','64608');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-03-01','de','Marketing Stats','Cost Team','Actual','55148');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-02-01','de','Marketing Stats','Cost Team','Actual','54984');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-01-01','de','Marketing Stats','Cost Team','Actual','46993');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-12-01','de','Marketing Stats','Cost Team','Actual','37387');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-11-01','de','Marketing Stats','Cost Team','Actual','38446');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-10-01','de','Marketing Stats','Cost Team','Actual','34393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-09-01','de','Marketing Stats','Cost Team','Actual','36101');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','de','Marketing Stats','Cost Team','Actual','22989');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-07-01','de','Marketing Stats','Cost Team','Actual','19008');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-06-01','de','Marketing Stats','Cost Team','Actual','24363');


INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-06-01','ch','Marketing Stats','Cost Team','Actual','2207');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-07-01','ch','Marketing Stats','Cost Team','Actual','3108');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','ch','Marketing Stats','Cost Team','Actual','2716');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-09-01','ch','Marketing Stats','Cost Team','Actual','6508');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-10-01','ch','Marketing Stats','Cost Team','Actual','6988');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-11-01','ch','Marketing Stats','Cost Team','Actual','7686');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-12-01','ch','Marketing Stats','Cost Team','Actual','5823');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-01-01','ch','Marketing Stats','Cost Team','Actual','5823');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-02-01','ch','Marketing Stats','Cost Team','Actual','5124');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-03-01','ch','Marketing Stats','Cost Team','Actual','5124');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-04-01','ch','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-05-01','ch','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-06-01','ch','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-07-01','ch','Marketing Stats','Cost Team','Actual','901');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-08-01','ch','Marketing Stats','Cost Team','Actual','0');
-- 2017-09 missing
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-10-01','ch','Marketing Stats','Cost Team','Actual','9370');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-11-01','ch','Marketing Stats','Cost Team','Actual','9370');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-12-01','ch','Marketing Stats','Cost Team','Actual','9379');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','ch','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','ch','Marketing Stats','Cost Team','Actual','0');


INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-10-01','nl','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-09-01','nl','Marketing Stats','Cost Team','Actual','6817');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-08-01','nl','Marketing Stats','Cost Team','Actual','2116');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-07-01','nl','Marketing Stats','Cost Team','Actual','2837');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-06-01','nl','Marketing Stats','Cost Team','Actual','2699');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-11-01','nl','Marketing Stats','Cost Team','Actual','3926');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2016-12-01','nl','Marketing Stats','Cost Team','Actual','5834');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-01-01','nl','Marketing Stats','Cost Team','Actual','5175');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-02-01','nl','Marketing Stats','Cost Team','Actual','4540');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-03-01','nl','Marketing Stats','Cost Team','Actual','4343');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-04-01','nl','Marketing Stats','Cost Team','Actual','4462');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-05-01','nl','Marketing Stats','Cost Team','Actual','4496');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-06-01','nl','Marketing Stats','Cost Team','Actual','4270');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-07-01','nl','Marketing Stats','Cost Team','Actual','3989');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-08-01','nl','Marketing Stats','Cost Team','Actual','3912');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-09-01','nl','Marketing Stats','Cost Team','Actual','3807');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-10-01','nl','Marketing Stats','Cost Team','Actual','4990');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-11-01','nl','Marketing Stats','Cost Team','Actual','4646');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2017-12-01','nl','Marketing Stats','Cost Team','Actual','5004');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','nl','Marketing Stats','Cost Team','Actual','4149');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','nl','Marketing Stats','Cost Team','Actual','3567');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-03-01','nl','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-04-01','nl','Marketing Stats','Cost Team','Actual','0');


-- INSERT INTO bi.b2b_daily_kpis
-- SELECT
-- 	mindate,
-- 	locale,
-- 	'Marketing Stats',
-- 	'Adwords Cost',	
	-- 'Actual',
-- 	costs
-- FROM

	-- (SELECT
	-- 	TO_CHAR(date,'YYYY-MM') as Month,
	-- 	locale,
	-- 	min(date) as mindate,
	-- 	SUM(sem_cost) as costs
	-- FROM
	-- 	bi.b2b_leadgencosts
	-- GROUP BY
	-- 	Month,
	-- 	locale) as a;

INSERT INTO bi.b2b_daily_kpis VALUES ('2017-07-01','de','Marketing Stats','Adwords Cost','Actual','4504'); ----------------------- THIS IS REINIGUNGDIREKT
INSERT INTO bi.b2b_daily_kpis VALUES ('2017-08-01','de','Marketing Stats','Adwords Cost','Actual','7790');
INSERT INTO bi.b2b_daily_kpis VALUES ('2017-09-01','de','Marketing Stats','Adwords Cost','Actual','6645');
INSERT INTO bi.b2b_daily_kpis VALUES ('2017-10-01','de','Marketing Stats','Adwords Cost','Actual','10981');
INSERT INTO bi.b2b_daily_kpis VALUES ('2017-11-01','de','Marketing Stats','Adwords Cost','Actual','11957');
INSERT INTO bi.b2b_daily_kpis VALUES ('2017-12-01','de','Marketing Stats','Adwords Cost','Actual','8726');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-01-01','de','Marketing Stats','Adwords Cost','Actual','20201');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-02-01','de','Marketing Stats','Adwords Cost','Actual','18805');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-03-01','de','Marketing Stats','Adwords Cost','Actual','9948');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-04-01','de','Marketing Stats','Adwords Cost','Actual','11350');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-05-01','de','Marketing Stats','Adwords Cost','Actual','12085');


INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-07-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*200*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and sub_kpi = 'Outbound'
	and left(locale,2) in ('de','nl')
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;


INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*40*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and left(locale,2) in ('de','nl')
	and sub_kpi = 'Inbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team Inbound',
	'Actual',
	sum(value)*40*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and left(locale,2) in ('de','nl')
	and sub_kpi = 'Inbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
	


INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t2.value) > 0 THEN SUM(t1.value)/SUM(t2.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date,
	t1.locale;
	
INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	'All' as locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t2.value) > 0 THEN SUM(t1.value)/SUM(t2.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date;


INSERT INTO  bi.b2b_daily_kpis
	SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t1.value) > 0 THEN SUM(t2.value)/SUM(t1.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	date,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	min(date) as date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	year_month,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;
	
	
DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team' WHERE sub_kpi = 	'Cost Team Est'; 

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Inbound Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	min(date) as date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team Inbound'
GROUP BY
	locale,
	year_month,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team Inbound' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team Inbound' WHERE sub_kpi = 	'Cost Team Inbound Est'; 


-- New Revenue END

-- TOtal Customers START

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	LEFT(t1.locale__c,2) as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.orders t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunityid = t2.sfid)
WHERE
	TO_CHAR(effectivedate::date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and (status not like '%CANCELLED%' and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL'))
	and order_type = '2'
GROUP BY
	Locale;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'185' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'ch' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'27' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'33' as value;


-- Summary

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Runrate on Target% till now' as text) as type,
	CASE WHEN MAX(alldays) > 0 THEN 

		CASE WHEN (SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays)))*100 > 0 THEN
	
			round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/(SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays))))*100,0) ELSE 0 END 


	ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
	(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
		sub_kpi,
	CAST('Runrate on Target%' as text) as type,
	CASE WHEN SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END) > 0 THEN round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Achieved' as varchar) as type,
	CASE WHEN MAX(current_days) > 0 THEN CAST(SUM(Value) as decimal)/MAX(current_days) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Target' as varchar) as type,
	CASE WHEN MAX(alldays) > 0 THEN CAST(SUM(Value) as decimal)/MAX(alldays) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Target'
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	date,
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Projected Achievement' as varchar) as type,
	(CASE WHEN MAX(current_days) > 0 THEN CAST(SUM(Value) as decimal)/MAX(current_days) ELSE NULL END)*(MAX(alldays)-MAX(current_days))+SUM(Value) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'Current Days' as type,
	MAX(current_days) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;
 	
 INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'All Days' as type,
	MAX(alldays) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Touched Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;

 INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Generated Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	'All' as locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month) as a
GROUP BY
	date;		
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Signed Customers') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(opportunities) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 53_b2b_kpis

DROP TABLE IF EXISTS bi.b2b_kpis;
CREATE TABLE bi.b2b_kpis AS

	SELECT -- INVOICED GMV

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced GMV'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- INVOICED HOURS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced hours'::text as kpi,
		SUM(o.order_duration__c)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

/*
INSERT INTO bi.b2b_kpis -- MIN PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Min PPH'::text as kpi,
		ROUND(MIN(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;



INSERT INTO bi.b2b_kpis -- MAX PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Max PPH'::text as kpi,
		ROUND(MAX(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;*/

INSERT INTO bi.b2b_kpis -- AVERAGE PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Average PPH'::text as kpi,
		CASE WHEN SUM(o.order_duration__c) > 0 THEN ROUND((SUM(o.gmv_eur)/SUM(o.order_duration__c))::numeric,1) ELSE NULL END as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- ACTIVE CLEANERS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;


INSERT INTO bi.b2b_kpis

	SELECT
		year::int as year,
	  	month::int as month,
	  	NULL::int as week,
	  	NULL::date as weekdate,
	  	locale::text as locale,
		city::text as city,
		'Supplies revenue'::text as kpi,
		SUM(Supply_revenue) as value
	FROM(
			SELECT
				 EXTRACT(YEAR from t1.Effectivedate) as year,
				 EXTRACT(MONTH from t1.Effectivedate) as month,
				 left(t1.locale__c,2) as locale,
				 t1.polygon as city,
					t2.sfid,
				 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
				 bi.orders t1
			LEFT JOIN
				 Salesforce.Opportunity t2
			ON
				 (t2.sfid = t1.opportunityid)
			WHERE
				 type = '222' 
				 AND EXTRACT(YEAR from t1.Effectivedate) <= EXTRACT(YEAR from current_date)
				 AND EXTRACT(MONTH from t1.Effectivedate) <= EXTRACT(MONTH from current_date)
				 AND status NOT LIKE ('%CANCELLED%')
			GROUP BY
				 left(t1.locale__c,2),
				 t1.polygon,
				 t2.sfid,
				 EXTRACT(YEAR from t1.Effectivedate),
				 EXTRACT(MONTH from t1.Effectivedate)) as a
	GROUP BY
		year,
		month,
		locale,
		city
	ORDER BY
		year,
		month,
		locale,
		city

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced GMV'::text as kpi,
		ROUND(SUM(o.gmv_eur)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced GMV'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced hours'::text as kpi,
		ROUND(SUM(o.order_duration__c)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced hours'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Active cleaners'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 58_b2blikelies


DROP TABLE IF EXISTS bi.b2b_likelies;
CREATE TABLE bi.b2b_likelies AS

SELECT
		t.*,

		CASE 	


				WHEN LOWER(t.acquisition_channel_ref__c) = 'anygrowth' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'

				WHEN LOWER(t.acquisition_channel_ref__c) = 'reveal' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'

				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
				
				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
				
				WHEN (t.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'

				WHEN ((lower(t.acquisition_channel__c) = 'inbound') AND lower(t.acquisition_tracking_id__c) = 'classifieds')					
				THEN 'Classifieds'
							
				WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text

		            AND t.createddate::date <= '2017-02-08'

		        THEN 'SEM Brand'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goob%')

		        	AND t.createddate::date > '2017-02-08'
		        	AND ((t.acquisition_channel_params__c LIKE '%goob%') AND (t.acquisition_channel_params__c LIKE '%bt:b2b%' OR t.acquisition_channel_params__c LIKE '%bt: b2b%' OR t.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
		        	)

		        THEN 'SEM Brand B2B'::text

		        WHEN (((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goob%')

		        	AND t.createddate::date > '2017-02-08'
					AND ((t.acquisition_channel_params__c LIKE '%goob%') AND (t.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%' ))
		        	
		        THEN 'SEM Brand B2C'::text
		        

		        WHEN (((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date <= '2017-02-08'
		        THEN 'SEM'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date > '2017-02-08'
		        	AND (((t.acquisition_channel_params__c LIKE '%goog%') AND (t.acquisition_channel_params__c LIKE '%bt: b2b%' OR t.acquisition_channel_params__c LIKE '%bt:b2b%' OR t.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
		        	))
		        	 OR
		            (t.acquisition_tracking_id__c LIKE 'unbounce_reinigungdirekt' AND t.createddate::date <= '2017-11-15')
		            OR
		            	acquisition_tracking_id__c LIKE 'rd%'

		            OR
		            	acquisition_channel_params__c LIKE '%ysm%'

		        THEN 'SEM B2B'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date > '2017-02-08' 
		        	AND ((t.acquisition_channel_params__c LIKE '%goog%') AND (t.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%')))
		        	OR t.acquisition_channel_params__c::text LIKE '%goog%'
		        THEN 'SEM B2C'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text


		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text
		            AND LOWER(acquisition_tracking_id__c) LIKE '%b2b seo page form&'
		        THEN 'SEO B2B'::text
		        

		        WHEN ((t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text
					AND LOWER(acquisition_tracking_id__c) NOT LIKE '%b2b seo page form&')

		        	OR
		        		((acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL AND (acquisition_tracking_id__c LIKE '%b2b%' OR acquisition_tracking_id__c LIKE '%tfs%')))

		        THEN 'SEO'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		            THEN 'SEO Brand'::text
		            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
		                OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text 
		        THEN 'Newsletter'::text
		        

		        WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text)
		        	OR (acquisition_channel__c IS NULL AND acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL)
		        THEN 'DTI'::text

		        
        

        		
		        
		        -- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'Reinigungsdirekt'

		        ---------------------------------------------------------------------------------------------------------

		        -- WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'b2c home form'
		        -- WHEN acquisition_tracking_id__c like '%b2c home funnel%' THEN 'b2c home funnel'
		        -- WHEN acquisition_tracking_id__c like '%b2c funnel availability request%' THEN 'b2c funnel availability request'

		        -- WHEN acquisition_tracking_id__c like '%b2b funnel%' THEN 'b2b funnel'
		        -- WHEN acquisition_tracking_id__c like '%availability request%' THEN 'availability request'
		        -- WHEN acquisition_tracking_id__c like '%price page funnel%' THEN 'price page funnel'
		        -- WHEN acquisition_tracking_id__c like '%help page funnel%' THEN 'help page funnel'
		        -- WHEN acquisition_tracking_id__c like '%about page funnel%' THEN 'about page funnel'
		        -- WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'b2b home form'
		        -- WHEN acquisition_tracking_id__c like '%b2b home funnel%' THEN 'b2b home funnel'
		        -- WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'chatbot'
		        -- WHEN acquisition_tracking_id__c like '%b2b amp%' THEN 'b2b amp'
		        -- WHEN acquisition_tracking_id__c like '%footer property management cb%' THEN 'footer property management cb'
		        -- WHEN acquisition_tracking_id__c like '%footer newsletter b2b%' THEN 'footer newsletter b2b'
		        -- WHEN acquisition_tracking_id__c like '%price page form%' THEN 'price page form'
		        -- WHEN acquisition_tracking_id__c like '%help page form%' THEN 'help page form'
		        -- WHEN acquisition_tracking_id__c like '%about page form%' THEN 'about page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b contact page form%' THEN 'b2b contact page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b service page form%' THEN 'b2b service page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b about page form%' THEN 'b2b about page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b floating cb%' THEN 'b2b floating cb'
		        -- WHEN acquisition_tracking_id__c like '%b2b floating newsletter%' THEN 'b2b floating newsletter'
		        -- WHEN acquisition_tracking_id__c like '%b2b seo page form%' THEN 'b2b seo page form'
		        -- WHEN acquisition_tracking_id__c like '%unbounce%' THEN 'unbounce'
		        -- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'unbounce_reinigungdirekt'
		        -- WHEN acquisition_tracking_id__c like '%unbound_tfs%' THEN 'unbound_tfs'

		        /*WHEN t.acquisition_channel_params__c::text ~~ '%coop%'
		            OR t.acquisition_channel_params__c::text ~~ '%afnt%'
		            OR t.acquisition_channel_params__c::text ~~ '%putzchecker%'
		        THEN 'Affiliate/Coops'*/
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		CASE WHEN t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c NOT IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}','{"src":"step1"}') THEN 'B2B Homepage' -- THE FIRST PAGE THE LEAD ACTUALLY VISITED ON THE WEBSITE
				WHEN (t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}')) OR t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Homepage'
				WHEN t.acquisition_channel_params__c IN ('{"src":"step1"}') THEN 'B2C Funnel'
				
				ELSE 'Unknown' END
		as landing_page,

		CASE WHEN t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Funnel' -- WHERE THE LEAD TYPED-IN ITS INFORMATON
				WHEN t.acquisition_tracking_id__c IS NULL THEN 'B2B Funnel'
				ELSE 'Unknown' END
		as conversion_page,
		CASE
		WHEN acquisition_tracking_id__c like '%b2b funnel%' and direct_relation__c = TRUE THEN 'B2B Funnel Direct'
		WHEN acquisition_tracking_id__c like '%b2b funnel%' and direct_relation__c = FALSE THEN 'B2B Funnel Partner'
		WHEN acquisition_tracking_id__c like '%b2b home cb%' THEN 'B2B Home CB'
		WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'B2B Home Form'
		WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'B2C Home Form'
		WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'Chatbot'
		-- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'Reinigungdirekt'
		-- WHEN acquisition_tracking_id__c like '%unbounce%' and acquisition_tracking_id__c not like '%unbounce_reinigungdirekt%'  THEN 'Unbounce'
		-- WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'b2c home form'

		-------------------------------------------------------------------------------------------------------------------  

        WHEN acquisition_tracking_id__c like '%b2c home funnel%' THEN 'b2c home funnel'
        WHEN acquisition_tracking_id__c like '%b2c funnel availability request%' THEN 'b2c funnel availability request'

        -- WHEN acquisition_tracking_id__c like '%b2b funnel%' THEN 'b2b funnel'
        WHEN acquisition_tracking_id__c like '%availability request%' THEN 'availability request'
        WHEN acquisition_tracking_id__c like '%price page funnel%' THEN 'price page funnel'
        WHEN acquisition_tracking_id__c like '%help page funnel%' THEN 'help page funnel'
        WHEN acquisition_tracking_id__c like '%about page funnel%' THEN 'about page funnel'
        -- WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'b2b home form'
        WHEN acquisition_tracking_id__c like '%b2b home funnel%' THEN 'b2b home funnel'
        WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'chatbot'
        WHEN acquisition_tracking_id__c like '%b2b amp%' THEN 'b2b amp'
        WHEN acquisition_tracking_id__c like '%footer property management cb%' THEN 'footer property management cb'
        WHEN acquisition_tracking_id__c like '%footer newsletter b2b%' THEN 'footer newsletter b2b'
        WHEN acquisition_tracking_id__c like '%price page form%' THEN 'price page form'
        WHEN acquisition_tracking_id__c like '%help page form%' THEN 'help page form'
        WHEN acquisition_tracking_id__c like '%about page form%' THEN 'about page form'
        WHEN acquisition_tracking_id__c like '%b2b contact page form%' THEN 'b2b contact page form'
        WHEN acquisition_tracking_id__c like '%b2b service page form%' THEN 'b2b service page form'
        WHEN acquisition_tracking_id__c like '%b2b about page form%' THEN 'b2b about page form'
        WHEN acquisition_tracking_id__c like '%b2b floating cb%' THEN 'b2b floating cb'
        WHEN acquisition_tracking_id__c like '%b2b floating newsletter%' THEN 'b2b floating newsletter'
        WHEN acquisition_tracking_id__c like '%b2b seo page form%' THEN 'b2b seo page form'
        WHEN acquisition_tracking_id__c like 'unbounce' THEN 'unbounce'
        WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'unbounce reinigungdirekt'
        WHEN acquisition_tracking_id__c like '%unbounce_tfs%' THEN 'unbounce tfs'


        WHEN acquisition_tracking_id__c like '%price_page_funnel_b2b%' THEN 'price page funnel b2b'
        WHEN acquisition_tracking_id__c like '%quality_page_funnel_b2b%' THEN 'quality page funnel b2b'
        WHEN acquisition_tracking_id__c like '%quality_page_funnel_b2c%' THEN 'quality page funnel b2c'
        WHEN acquisition_tracking_id__c like '%how_it_works_page_funnel_b2b%' THEN 'how it works page funnel b2b'
        WHEN acquisition_tracking_id__c like '%how_it_works_page_funnel_b2c%' THEN 'how it works page funnel b2c'
        WHEN acquisition_tracking_id__c like '%about_us_page_funnel_b2b%' THEN 'about us page funnel b2b'
        WHEN acquisition_tracking_id__c like '%about_us_page_funnel_b2c%' THEN 'about us page funnel b2c'
        WHEN acquisition_tracking_id__c like '%about_us_page%' THEN 'about us page'

		ELSE 'Other' END as lead_source


	FROM
	Salesforce.likeli__c t
	WHERE
	createddate::date > '2016-03-01'
	and type__c = 'B2B'
	-- and t.test__c IS FALSE
	AND acquisition_channel__c NOT LIKE 'outbound'
	AND company_name__c NOT LIKE '%test%'
	AND company_name__c NOT LIKE '%bookatiger%'
	AND email__c NOT LIKE '%bookatiger%'
	AND ((t.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR t.lost_reason__c IS NULL)
	-- AND o.acquisition_channel__c IN ('inbound', 'web')
	-- AND LEFT(o.locale__c, 2) = 'de'
	AND t.test__c IS FALSE
	AND t.name NOT LIKE '%test%';


DROP TABLE IF EXISTS bi.b2b_likelie_cvr;
CREATE Table bi.b2b_likelie_cvr as 
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	left(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	min(t1.createddate::date) as date,
	conversion_page,
	lead_source,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	COUNT(DISTINCT(CASE WHEN t2.stagename IN ('LOST') THEN t2.sfid ELSE NULL END)) as lost,
	COUNT(DISTINCT(CASE WHEN t2.stagename IN ('OFFER SENT', 'VERBAL CONFIRMATION') THEN t2.sfid ELSE NULL END)) as offer_sent_verbal_conf,
	COUNT(DISTINCT(CASE WHEN t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING') THEN t2.sfid ELSE NULL END)) as signed_opps,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c NOT IN ('outbound')
	AND t1.test__c IS FALSE
	AND t1.company_name__c NOT LIKE '%test%'
	AND t1.company_name__c NOT LIKE '%bookatiger%'
	AND (t1.lost_reason__c NOT IN ('invalid - sem duplicate') OR t1.lost_reason__c IS NULL)
	AND LEFT(t1.locale__c, 2) = 'de'
	AND t1.createddate >= '2018-01-01'

	-- AND t2.test__c IS FALSE
	-- and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale,
	lead_source,
	Source_Channel,
	landing_page,
	conversion_page;

	
DROP TABLE IF EXISTS bi.b2b_likelie_cvr_weekly;
CREATE TABLE bi.b2b_likelie_cvr_weekly as 
SELECT
	EXTRACT(YEAR FROM t1.createddate) as yearnum,
	EXTRACT(WEEK FROM t1.createddate) as weeknum,
	LEFT(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	min(t1.createddate::date) as date,
	conversion_page,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	EXTRACT(YEAR FROM t1.createddate),
	EXTRACT(WEEK FROM t1.createddate),
	LEFT(t1.locale__c,2),
	Source_Channel,
	landing_page,
	conversion_page

ORDER BY 
	yearnum desc,
	weeknum desc,
	locale asc

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

		
DROP TABLE IF EXISTS bi.b2b_monthly_kpis;
CREATE TABLE bi.b2b_monthly_kpis as 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(t2.sfid)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid and LOWER(t2.name) not like '%test%')
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL','WAITING FOR RESCHEDULE')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
		
INSERT INTO bi.b2b_monthly_kpis 	
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as revenue
FROM
	bi.b2borders 
GROUP BY
	year_month,
	locale,
	city;
	
	

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(ORder_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New (Signed Before)' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	TO_CHAR(sign_date,'YYYY-MM') < Year_MOnth
	and customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;
	
INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New (Signed same month)' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	TO_CHAR(sign_date,'YYYY-MM') = Year_MOnth
	and customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;	
	
INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(CASE WHEN TO_CHAR(Effectivedate::date,'YYYY-MM') = TO_CHAR(first_order::date,'YYYY-MM') THEN Order_Duration__c ELSE NULL END) as value
	
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
LEFT JOIN
(SELECT
	t2.sfid,
	min(effectivedate::date) as first_order
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	t2.sfid) as t3
ON
	(t2.sfid = t3.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;	

INSERT INTO bi.b2b_monthly_kpis	
SELECT
	year_month,
	min(date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN TO_CHAR(first_date::date,'YYYY-MM') = TO_CHAR(date::date,'YYYY-MM') THEN (CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) ELSE 0 END) as revenue

FROM
	bi.b2borders a
LEFT JOIN
	(SELECT
		name,
		min(date) as first_date
	FROM
		bi.b2borders	
	GROUP BY
		name) as b
ON
	(a.name = b.name)
GROUP BY
	year_month,
	locale,
	city;

------------------------------------------------------------------------------------------------------- Revenue Churned Opps 27/03/2018

INSERT INTO bi.b2b_monthly_kpis
SELECT

	to_char(churn_date,'YYYY-MM') as year_month,
	min(churn_date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CAST('-' as varchar) as city,
	CAST('Churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'B2B' as customer_type,
	SUM(total) as revenue

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		t3.potential as total
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		-- EXTRACT(WEEK FROM oo.createddate) = 6
		-- AND EXTRACT(year FROM oo.createddate) = 2018
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		-- AND oo.name NOT LIKE '%test%'
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid

	LEFT JOIN

		bi.potential_revenue_per_opp t3

	ON

		t1.opportunityid = t3.opportunityid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t3.potential
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_month,
	locale,
	city
	
ORDER BY

	year_month desc;
		
------------------------------------------------------------------------------------------------------- Revenue Churned Opps 27/03/2018

INSERT INTO bi.b2b_monthly_kpis
SELECT

	to_char(churn_date,'YYYY-MM') as year_month,
	min(churn_date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CAST('-' as varchar) as city,
	CAST('Churned Opps' as varchar) as kpi,
	CAST('Customer' as varchar) as type,
	COUNT(DISTINCT t3.opportunityid) as customers
	

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		t3.potential as total
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		-- EXTRACT(WEEK FROM oo.createddate) = 6
		-- AND EXTRACT(year FROM oo.createddate) = 2018
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		-- AND oo.name NOT LIKE '%test%'
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid

	LEFT JOIN

		bi.potential_revenue_per_opp t3

	ON

		t1.opportunityid = t3.opportunityid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t3.potential
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_month,
	locale,
	city
	
ORDER BY

	year_month desc;
		

-------------------------------------------------------------------------------------------------------

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(t2.sfid)) as sfid
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis 	
SELECT
	TO_CHAR(date::date + Interval '1 Month','YYYY-MM') as Year_Months,
	min(date::date + Interval '1 Month')::date as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as revenue
FROM
	bi.b2borders 
GROUP BY
	year_months,
	locale,
	city;


INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(Order_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	
DROP TABLE IF EXISTS bi.closemonth_stats;
CREATE TABLE bi.closemonth_stats as 
SELECT
	Signing_Month,
	start_month,
	locale,
	min(sign_date) as min_sign_date,
	((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) as Year_Diff,
	CASE WHEN stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING') and status__c in ('RESIGNED','CANCELLED') THEN 'LEFT' ELSE 'PENDING' END as status,
	SUM(unique_customers) as Unique_Customers
FROM(
SELECt
	to_char(closedate::date,'YYYY-MM') as Signing_Month,
	TO_CHAR(Order_Start::date,'YYYY-MM') as Start_Month,
	min(closedate::date) as Sign_Date,
	min(order_Start::Date) as Start_date,
	stagename,
	status__c,
	LEFT(Locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as unique_customers
FROM
	Salesforce.Opportunity t1
LEFT JOIN
	(SELECT
		t3.opportunityid,
		min(effectivedate::date) as Order_Start
	FROM
		bi.orders t3
	WHERE
		t3.status not like '%CANCELLED%'
	GROUP BY
		t3.opportunityid) as t2
ON
	(t1.sfid = t2.opportunityid)
WHERE
	t1.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Signing_Month,
	Start_Month,
	status__c,
	locale,
	stagename) as a
GROUP BY
	Signing_Month,
	Start_Month,
	status,
	locale,
	Year_Diff
HAVING
	((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) > -1 or ((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) is null;

DROP VIEW IF EXISTS bi.history_view;

DROP TABLE IF EXISTS bi.leadhistory_touched_date;
CREATE TABLE bi.leadhistory_touched_date as 
SELECT
	parentid,
	min(createddate::date) as createddate
FROM
	salesforce.likeli__history t1
WHERE
	field = 'Owner'
	and createddate::date > '2017-02-01'
	and oldvalue in ('API Tiger','MC Tiger','IT Tiger','Alejandra Chavez','Renan Ribeiro','Austin Marcus')
	and newvalue not in ('Alejandra Chavez','Renan Ribeiro','Austin Marcus','API Tiger','MC TIGER','IT TIGER')
GROUP BY
	parentid;
	
DROP TABLE IF EXISTS bi.likeli_to_opp_conversion;
CREATE TABLE bi.likeli_to_opp_conversion as
SELECT
	t2.sfid as opp_id,
	t1.acquisition_tracking_id__c,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t1.createddate::date as lead_created,
	t2.createddate::date as opp_created,
	t2.closedate::date as signed_date,
	extract(day from age(t2.closedate::date,t2.createddate::date)) as time_to_convert,
	t4.potential as Monthly_Amount,
	CASE WHEN t2.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING') THEN 1 ELSE 0 END as Conversion_Signed,
	CASE WHEN t2.stagename not in  ('NEGOTIATION','NEW','OFFER SENT','POSITIVE FEEDBACK','PENDING','WON') THEN 1 ELSE 0 END as Conversion_Finished,
	CASE WHEN t3.hours > 0 THEN 1 ELSE 0 END as Conversion_Started
FROM
	salesforce.likeli__c t1
JOIN 
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)

LEFT JOIN
	bi.potential_revenue_per_opp t4

ON

	t1.opportunity__c = t4.opportunityid

LEFT JOIN
	(SELECT
		t3.opportunity_id,
		sum(total_hours) as hours
	FROM
		bi.b2borders t3
	GROUP BY
		opportunity_id) as t3
ON
	(t2.sfid = t3.opportunity_id);

DROP VIEW IF EXISTS bi.history_view;
CREATE VIEW bi.history_view as 
SELECT
	parentid,
	locale,
	acquisition_tracking_id__c,
	lead_source,
	lead_created,
	times_recycled,
	first_touch_date,
	1 as new_cvr,
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null or not_reached_date is not null or ended_date is not null THEN 1 ELSE 0 END) as touched_cvr,	
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null THEN 1 ELSE 0 END) as reached_cvr,
	MAX(CASE WHEN opp_date is not null or qualified_date is not null THEN 1 ELSE 0 END) as qualified_cvr,
	MAX(CASE WHEN opp_date is not null  THEN 1 ELSE 0 END) as opp_cvr,
	MAX(CASE WHEN ended_date is not null THEN 1 ELSE 0 END) as ended_cvr,
	MAX(CASE WHEN not_reached_date is not null THEN 1 ELSE 0 END) as not_reached_cvr,
	MAX(CASE WHEN dm_reached_date is not null THEN 1 ELSE 0 END) as dm_reached_cvr,
	MAX(CASE WHEN signed_date is not null THEN 1 ELSE 0 END) as signed_cvr
FROM(
SELECT
	t1.parentid,
	t2.acquisition_tracking_id__c,
	LEFT(t2.locale__c,2) as locale,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t2.createddate::date as lead_created,
	t3.createddate::date as first_touch_date,
	CASE WHEN recycle__c is null THEN 0 ELSE recycle__c END as times_recycled,
	MIN(CASE WHEN newvalue = 'NEW' and field  = 'stage__c' THEN t1.createddate::date else null END) as new_date,
	MIN(CASE WHEN newvalue = 'VALIDATED' and field  = 'stage__c' THEN t1.createddate::date else null END) as validated_date,
	MIN(CASE WHEN newvalue = 'NOT REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as not_reached_date,
	MIN(CASE WHEN newvalue = 'REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as reached_date,
	MIN(CASE WHEN newvalue = 'DM REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as dm_reached_date,
	MIN(CASE WHEN newvalue = 'QUALIFIED' and field  = 'stage__c' THEN t1.createddate::date else null END) as qualified_date,
	MIN(CASE WHEN newvalue = 'ENDED' and field  = 'stage__c' THEN t1.createddate::date else null END) as ended_date,
	MIN(CASE WHEN newvalue != ''  and field = 'opportunity__c' THEN t1.createddate::date else null end) as opp_date, 
	MIN(CASE WHEN stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','PENDING','WON') THEN closedate::date ELSE null END) as signed_date
FROM
	salesforce.likeli__history t1
JOIN
	salesforce.likeli__c t2
ON
	(t1.parentid = t2.sfid)
LEFT JOIN
	bi.leadhistory_touched_date t3
ON
	(t1.parentid = t3.parentid)
LEFT JOIn
	Salesforce.opportunity t4
ON
	(t2.opportunity__c = t4.sfid)
WHERE
	field  in ('stage__c','opportunity__c')
GROUP BY
	t1.parentid,
	t2.acquisition_tracking_id__c,
	t2.createddate::date,
	lead_source,
	locale,
	first_touch_date,
	times_recycled) as a
GROUP BY
	parentid,
	lead_source,
	first_touch_date,
	locale,
	acquisition_tracking_id__c,
	lead_created,
	times_recycled;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.opportunity_churn;
CREATE TABLE bi.opportunity_churn as 
	
SELECT

	-- CASE WHEN t4.monthly_revenue_loss > 700 THEN '> 700€'
		  -- WHEN (t4.monthly_revenue_loss > 250 AND t4.monthly_revenue_loss < 700) THEN '250€ - 700€'
		  -- WHEN t4.monthly_revenue_loss < 250 THEN '> 250€'
		  -- ELSE NULL
		  -- END as cluster,
	t4.*

FROM	
	
	
	(SELECT
	
		MIN(t1.date_churn) as date_churn,
		t1.year,
		t1.month,
		t1.opportunityid,
		t1.name,
		t1.locale,
		t2.polygon,
		-- t1.stagename,
		t1.churn_reason,
		t2.hours as monthly_hours_loss,
		grand_total__c,
		t2.monthly_revenue as monthly_revenue_loss,
		t1.acquisition_channel__c as acquisition_channel
		
	FROM
	
		(SELECT

			opphistory.*,
			op.name,
			left(op.locale__c,2) as locale,
			op.churn_reason__c as churn_reason,
			op.grand_total__c,
			op.acquisition_channel__c
		
		FROM
		
			(SELECT
				
				EXTRACT(YEAR FROM o.createddate) as year,
				EXTRACT(MONTH FROM o.createddate) as month,	
				MAX(o.createddate::date) as date_churn,
				o.opportunityid
				-- o.stagename
			
			FROM
			
				salesforce.opportunityfieldhistory o
				
			WHERE
			
				o.newvalue IN ('DECLINED', 'TERMINATED', 'RESIGNED', 'CANCELLED')
				
			GROUP BY
			
				o.opportunityid,
				-- o.stagename,
				year,
				month
				
			ORDER BY
				
				year desc,
				month desc,
				o.opportunityid) as opphistory
			
		LEFT JOIN 
		
			salesforce.opportunity op
			
		ON 
		
			opphistory.opportunityid = op.sfid) as t1
			
	LEFT JOIN
	
	
		(SELECT

			t3.opportunityid,
			t3.polygon,
			MAX(t3.hours) as hours,
			MAX(t3.monthly_revenue) as monthly_revenue
		
		FROM
		
			(SELECT
		
				EXTRACT(YEAR FROM o.effectivedate) as year,
				EXTRACT(MONTH FROM o.effectivedate) as month,
				o.opportunityid,
				o.polygon,
				SUM(CASE WHEN o.eff_pph IS NULL THEN o.pph__c*o.order_duration__c ELSE o.eff_pph*o.order_duration__c END) as monthly_revenue,
				SUM(o.order_duration__c) as hours
				
			
			FROM
			
				bi.orders o
				
			WHERE
			
				o.order_type = 2
				
				
				
			GROUP BY
			
				year,
				month,
				o.opportunityid,
				o.polygon) as t3
		
		GROUP BY 
		
			t3.opportunityid,
			t3.polygon
			) as t2
				
	ON 
	
		t1.opportunityid = t2.opportunityid
		
	GROUP BY
	
		t1.year,
		t1.month,
		t1.opportunityid,
		t1.name,
		t1.locale,
		t2.polygon,
		-- t1.stagename,
		t1.churn_reason,
		monthly_hours_loss,
		grand_total__c,
		t2.monthly_revenue,
		t1.acquisition_channel__c
	
	ORDER BY
	
		year desc,
		month desc) as t4;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.forecast_sales;
CREATE TABLE bi.forecast_sales AS


SELECT

	year,
	month,
	country,
	channel,
	SUM(t3.number_opportunities) as actual_opportunities,
	SUM(t3.forecast_opportunities) as forecast_opps,
	CASE WHEN (country = 'DE' AND channel = 'INBOUND') THEN 50 
		  WHEN (country = 'DE' AND channel = 'OUTBOUND') THEN 20
		  ELSE (CASE WHEN (country = 'CH' AND channel = 'INBOUND')  THEN 4 
		             WHEN (country = 'CH' AND channel = 'OUTBOUND')  THEN 4
		  				 ELSE (CASE WHEN (country = 'NL' AND channel = 'INBOUND') THEN 7 
						            WHEN (country = 'NL' AND channel = 'OUTBOUND') THEN 7
						            ELSE NULL END)
						 END)
		  END as target,
	CASE WHEN (country = 'DE' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/50
	     WHEN (country = 'DE' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/20 
		  ELSE (CASE WHEN (country = 'CH' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/4
	     				 WHEN (country = 'CH' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/4
		  				ELSE (CASE WHEN (country = 'NL' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/7
	     				 			  WHEN (country = 'NL' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/7
	     				 			  ELSE NULL
	     				 			  END)
						END)
		  END as runrate

FROM

	(SELECT
	
		UPPER(LEFT(locale__c, 2)) as country,
		EXTRACT(YEAR FROM t2.closedate) as year,
		EXTRACT(MONTH FROM t2.closedate) as month,
		EXTRACT(DAY FROM t2.closedate) as day,
		DATE_PART('days', DATE_TRUNC('month', t2.closedate) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_in_month,
		CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'INBOUND' ELSE 'OUTBOUND' END as channel,
		SUM(CASE WHEN t2.stagename IN ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING') THEN 1 ELSE 0 END) as number_opportunities,
		ROUND((SUM(CASE WHEN t2.stagename IN ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING') THEN 1 ELSE 0 END)/EXTRACT(DAY FROM current_date))*DATE_PART('days', DATE_TRUNC('month', t2.closedate) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)) as forecast_opportunities
	
	FROM
	
		(SELECT

			*
			
		FROM
		
			salesforce.opportunity
			
		WHERE
		
			stagename in ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING')
			-- AND (closedate::date BETWEEN '2017-06-01' AND '2017-06-30')
			) as t2
	
	GROUP BY
	
		country,
		year,
		month,
		day,
		number_days_in_month,
		channel
		
	ORDER BY
	
		year,
		month) as t3
		
WHERE

	year = EXTRACT(YEAR FROM current_date)
	AND month =EXTRACT(MONTH FROM current_date)
		
GROUP BY

	country,
	year,
	month,
	channel;

-- B2B Sales Dashboard

DROP TABLE IF EXISTS bi.salesreport_daily;
CREATE TABLE bi.salesreport_daily as 

-- Signed Deals - Actual
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Deals' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	ROUND(COUNT(*),2) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
	
-- Signed Revenue - Actual	
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Revenue' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	SUM(grand_total__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
-- Signed Hours - Actual		
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Hours' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	
	CASE WHEN LEFT(locale__c,2) ='de' THEN SUM(grand_total__c)/25.9 
	WHEN LEFT(locale__c,2) ='nl' THEN SUM(grand_total__c)/27.9 
	ELSE 0 END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;	

-- ------------------------------------------------------------------------------------------------------------------------- 2018-07
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Actual','FTE','SME','Sales','Inbound','3');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Actual','FTE','KA','Sales','Outbound','1');
 
-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','FTE','SME','Sales','Inbound','3');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','FTE','KA','Sales','Outbound','1');

-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Signed Deals','SME','Sales','Inbound','80'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Signed Deals','SME','Sales','Outbound','5'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Signed Deals','KA','Sales','Inbound','6'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Signed Deals','KA','Sales','Outbound','0'); 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average Revenue','SME','Sales','Inbound','423');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average Revenue','SME','Sales','Outbound','326');
-- INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average Revenue','SME','Funnel','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average Revenue','KA','Sales','Outbound','1199');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-07','Target','Average PPH','KA','Sales','Outbound',25.90);


-- ------------------------------------------------------------------------------------------------------------------------- 2018-06
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Actual','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Actual','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Actual','FTE','KA','Sales','Outbound','1');
 
-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','FTE','KA','Sales','Outbound','1');

-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Signed Deals','SME','Sales','Inbound','77'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Signed Deals','SME','Sales','Outbound','5'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Signed Deals','KA','Sales','Outbound','5'); 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average Revenue','SME','Sales','Inbound','415');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average Revenue','SME','Sales','Outbound','330');
-- INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average Revenue','SME','Funnel','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-06','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2018-05
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Actual','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Actual','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Actual','FTE','KA','Sales','Outbound','1');
-- INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Actual','FTE','SME','Sales','Inbound','1');
-- INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Actual','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Actual','FTE','KA','Sales','Outbound','1');
-- INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?

-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Actual','FTE','SME','Sales','Inbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Actual','FTE','SME','Sales','Outbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Actual','FTE','KA','Sales','Outbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?
 
-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','FTE','KA','Sales','Outbound','1');
-- INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','FTE','SME','Sales','Inbound','1');
-- INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','FTE','KA','Sales','Outbound','1');
-- INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?

-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','FTE','SME','Sales','Inbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','FTE','SME','Sales','Outbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','FTE','KA','Sales','Outbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Signed Deals','SME','Sales','Inbound','70'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Signed Deals','SME','Sales','Outbound','5'); 
-- INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Signed Deals','SME','Funnel','Inbound','0'); -- is excl. in the Sales RR file  - replaced by one offs? 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Signed Deals','KA','Sales','Outbound','5'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Signed Deals','SME','Sales','Outbound','0'); 
-- INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Signed Deals','KA','Sales','Outbound','0'); 

-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Signed Deals','SME','Sales','Inbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Signed Deals','SME','Sales','Outbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Signed Deals','KA','Sales','Outbound','0'); -- irrelevant?  

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average Revenue','SME','Sales','Inbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average Revenue','SME','Sales','Outbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average Revenue','SME','Funnel','Inbound','300');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Average Revenue','KA','Sales','Outbound','0');

-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Average Revenue','SME','Sales','Inbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Average Revenue','SME','Sales','Outbound','0'); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Average Revenue','KA','Sales','Outbound','0'); -- irrelevant?

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-05','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-05','Target','Average PPH','KA','Sales','Outbound',0.00);

-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Average PPH','SME','Sales','Inbound',0); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Average PPH','SME','Sales','Outbound',0); -- irrelevant?
-- INSERT INTO bi.salesreport_daily VALUES ('nl','2018-05','Target','Average PPH','KA','Sales','Outbound',0); -- irrelevant?


-- ------------------------------------------------------------------------------------------------------------------------- 2018-04
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Actual','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Actual','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Actual','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Actual','FTE','SME','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Actual','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Actual','FTE','KA','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?
 
-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','FTE','SME','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','FTE','KA','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Signed Deals','SME','Sales','Inbound','70'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Signed Deals','SME','Sales','Outbound','5'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Signed Deals','KA','Sales','Outbound','5'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Signed Deals','SME','Sales','Outbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Signed Deals','SME','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Signed Deals','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Signed Deals','KA','Sales','Outbound','0'); -- irrelevant?  

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average Revenue','SME','Sales','Inbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average Revenue','SME','Sales','Outbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average Revenue','SME','Funnel','Inbound','300');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Average Revenue','SME','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Average Revenue','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Average Revenue','KA','Sales','Outbound','0'); -- irrelevant?

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-04','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-04','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Average PPH','SME','Sales','Inbound',0); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Average PPH','SME','Sales','Outbound',0); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-04','Target','Average PPH','KA','Sales','Outbound',0); -- irrelevant?


-- ------------------------------------------------------------------------------------------------------------------------- 2018-03
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Actual','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Actual','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Actual','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Actual','FTE','SME','Sales','Outbound','0');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Actual','FTE','SME','Sales','Inbound','0');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Actual','FTE','SME','Sales','Outbound','0');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Actual','FTE','KA','Sales','Outbound','0');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','FTE','SME','Sales','Inbound','0');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','FTE','SME','Sales','Outbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','FTE','KA','Sales','Outbound','0');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Signed Deals','SME','Sales','Inbound','70'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Signed Deals','SME','Sales','Outbound','5'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Signed Deals','KA','Sales','Outbound','5'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Signed Deals','SME','Sales','Outbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Signed Deals','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average Revenue','SME','Sales','Inbound','395');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average Revenue','SME','Sales','Outbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average Revenue','SME','Funnel','Inbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Average Revenue','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-03','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-03','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Average PPH','SME','Sales','Inbound',0);
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Average PPH','SME','Sales','Outbound',0);
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-03','Target','Average PPH','KA','Sales','Outbound',0);


-- ------------------------------------------------------------------------------------------------------------------------- 2018-02
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Actual','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','FTE','SME','Sales','Inbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Signed Deals','SME','Sales','Inbound','60'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Signed Deals','SME','Sales','Outbound','5'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Signed Deals','SME','Funnel','Inbound','10'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Signed Deals','KA','Sales','Outbound','5'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Signed Deals','SME','Sales','Outbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Signed Deals','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average Revenue','SME','Sales','Inbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average Revenue','SME','Sales','Outbound','397');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average Revenue','SME','Funnel','Inbound','300');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-02','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-02','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-02','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2018-01
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Signed Deals','SME','Sales','Inbound','39'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Signed Deals','SME','Sales','Outbound','15'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Signed Deals','SME','Funnel','Inbound','15'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Signed Deals','KA','Sales','Outbound','1'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Signed Deals','KA','Sales','Outbound','3');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2018-01','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2018-01','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2018-01','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-12
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','SME','Sales','Inbound','39'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','SME','Sales','Outbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','SME','Funnel','Inbound','15'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Signed Deals','KA','Sales','Outbound','1'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Signed Deals','KA','Sales','Outbound','3');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-12','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-12','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-12','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-11
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','SME','Sales','Inbound','39'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','SME','Sales','Outbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','SME','Funnel','Inbound','15'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Signed Deals','KA','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Signed Deals','KA','Sales','Outbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Signed Deals','KA','Sales','Outbound','3');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-11','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-11','Target','Average PPH','KA','Sales','Outbound',0.00);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-11','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-10
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Actual','FTE','KA','Sales','Inbound','1');

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','SME','Sales','Inbound','39');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','SME','Sales','Outbound','20');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','SME','Funnel','Inbound','15');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Signed Deals','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Signed Deals','KA','Sales','Outbound','3');	

-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','SME','Funnel','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average Revenue','SME','Sales','Inbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average Revenue','SME','Sales','Outbound','376');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-10','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-10','Target','Average PPH','KA','Sales','Outbound',0.00);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-10','Target','Average PPH','KA','Sales','Outbound',25.90);

-- -------------------------------------------------------------------------------------------------------------------------  2017-09
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Inbound','1');

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','SME','Sales','Inbound','44');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','SME','Sales','Outbound','24');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','SME','Funnel','Inbound','22');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Signed Deals','KA','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Signed Deals','KA','Sales','Outbound','2');

-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','SME','Sales','Outbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Actual','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','SME','Sales','Inbound','374');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','SME','Sales','Outbound','374');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','SME','Funnel','Inbound','374');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average Revenue','SME','Sales','Inbound','368');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average Revenue','SME','Sales','Outbound','368');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-09','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-09','Target','Average PPH','KA','Sales','Outbound',0.00);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-09','Target','Average PPH','KA','Sales','Outbound',25.90);

-- -------------------------------------------------------------------------------------------------------------------------  2017-08
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','KA','Sales','Inbound','1');

-- -------------------------------------------------------------------------------------------------------------------------  KPI Targets
-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Sales','Inbound','41');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Sales','Outbound','20');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Funnel','Inbound','22');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','KA','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','KA','Sales','Outbound','1');

-- FTE Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Sales','Inbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Sales','Outbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Funnel','Inbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','SME','Sales','Inbound','379');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','SME','Sales','Outbound','379');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','KA','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-07
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','SME','Sales','Inbound','68'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','SME','Sales','Outbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','SME','Funnel','Inbound','22'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Signed Deals','KA','Sales','Outbound','2'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','SME','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','SME','Sales','Inbound','330');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','SME','Sales','Outbound','330');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','SME','Funnel','Inbound','330');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average Revenue','KA','Sales','Outbound','1100');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average Revenue','SME','Sales','Inbound','388');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average Revenue','SME','Sales','Outbound','388');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-07','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-07','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-07','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-06
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','SME','Sales','Inbound','35'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','SME','Sales','Outbound','18'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','SME','Funnel','Inbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Signed Deals','KA','Sales','Outbound','2'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','SME','Sales','Outbound','8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','SME','Sales','Inbound','317');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','SME','Sales','Outbound','317');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','SME','Funnel','Inbound','317');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average Revenue','KA','Sales','Outbound','10000');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-06','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-06','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-06','Target','Average PPH','KA','Sales','Outbound',25.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-05
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','SME','Sales','Inbound','45'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','SME','Sales','Outbound','10'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','SME','Funnel','Inbound','20'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','SME','Sales','Outbound','13');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','SME','Sales','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','SME','Sales','Outbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','SME','Funnel','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-05','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-05','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average PPH','SME','Sales','Inbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average PPH','SME','Sales','Outbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-05','Target','Average PPH','KA','Sales','Outbound',27.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-04
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','SME','Sales','Inbound','45'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','SME','Sales','Outbound','25'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','SME','Sales','Outbound','13');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','SME','Sales','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','SME','Sales','Outbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','SME','Funnel','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-04','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-04','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average PPH','SME','Sales','Inbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average PPH','SME','Sales','Outbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-04','Target','Average PPH','KA','Sales','Outbound',27.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-03
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','SME','Sales','Outbound','8');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','SME','Sales','Outbound','8');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','SME','Sales','Inbound','45'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','SME','Sales','Outbound','25'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','SME','Sales','Inbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','SME','Sales','Outbound','1'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','SME','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','SME','Sales','Outbound','13');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','SME','Sales','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','SME','Sales','Outbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','SME','Funnel','Inbound','450');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average Revenue','SME','Sales','Inbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average Revenue','SME','Sales','Outbound','475');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-03','Target','Average PPH','KA','Sales','Outbound',25.90);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-03','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average PPH','SME','Sales','Inbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average PPH','SME','Sales','Outbound',27.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-03','Target','Average PPH','KA','Sales','Outbound',27.90);

-- ------------------------------------------------------------------------------------------------------------------------- 2017-02
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','SME','Sales','Outbound','1');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','SME','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','SME','Sales','Outbound','1'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','SME','Sales','Inbound','46'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','SME','Sales','Outbound','25'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','SME','Sales','Inbound','2'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','SME','Sales','Outbound','8'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','SME','Sales','Inbound','9');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','SME','Sales','Outbound','9');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','SME','Sales','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','SME','Sales','Outbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','SME','Funnel','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average Revenue','SME','Sales','Inbound','700.2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average Revenue','SME','Sales','Inbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average Revenue','SME','Sales','Outbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','SME','Sales','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','SME','Sales','Outbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','SME','Funnel','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-02','Target','Average PPH','KA','Sales','Outbound',23);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-02','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average PPH','SME','Sales','Inbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average PPH','SME','Sales','Outbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-02','Target','Average PPH','KA','Sales','Outbound',27.70);


-- ------------------------------------------------------------------------------------------------------------------------- 2017-01
-- FTE Numbers - Actual
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','SME','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Actual','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Actual','FTE','KA','Sales','Inbound','1');-- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','SME','Sales','Outbound','2');-- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Actual','FTE','KA','Sales','Inbound','0');-- irrelevant?

-- --------------------------------------------------------------------------------------------------------------KPI Targets
-- FTE - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','SME','Sales','Outbound','6');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','FTE','KA','Sales','Inbound','1'); -- irrelevant?

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','SME','Sales','Outbound','2'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','FTE','KA','Sales','Inbound','0'); -- irrelevant?


-- Signed Deals - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','SME','Sales','Inbound','14'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','SME','Sales','Outbound','46'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','SME','Funnel','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','KA','Sales','Inbound','0'); 
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','SME','Sales','Inbound','2'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','SME','Sales','Outbound','8'); 
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Signed Deals','KA','Sales','Outbound','0'); 

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','SME','Sales','Inbound','3');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','SME','Sales','Outbound','22');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','KA','Sales','Inbound','0'); -- irrelevant?
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Signed Deals','KA','Sales','Outbound','0');	 

-- Average Revenue - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','SME','Sales','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','SME','Sales','Outbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','SME','Funnel','Inbound','426');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average Revenue','SME','Sales','Inbound','700.2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average Revenue','KA','Sales','Outbound','0');

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average Revenue','SME','Sales','Inbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average Revenue','SME','Sales','Outbound','498.8');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average Revenue','KA','Sales','Outbound','0');

-- Average PPH - Target
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','SME','Sales','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','SME','Sales','Outbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','SME','Funnel','Inbound',23);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-01','Target','Average PPH','KA','Sales','Outbound',23);

INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-01','Target','Average PPH','KA','Sales','Outbound',0.00);

INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average PPH','SME','Sales','Inbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average PPH','SME','Sales','Outbound',27.70);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-01','Target','Average PPH','KA','Sales','Outbound',27.70);


-- Signed Revenue - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
sub1.value*sub2.value AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Deals')
	AND sub2.kpi in ('Average Revenue')
		AND sub1.sub_kpi in ('Target');	
	
-- Signed Hours - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Hours' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Average PPH')
	AND sub1.sub_kpi in ('Target');
	
-- Average PPH - Actual

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Average Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Signed Deals')
	AND sub1.sub_kpi in ('Actual');
	

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- new Opportunity Traffic Light based on Opportunities NOT contacts

DELETE FROM 	bi.opportunity_traffic_light_new 		WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO 	bi.opportunity_traffic_light_new

	SELECT		(CURRENT_DATE - INTERVAL '0 DAY')							AS date
				, traffic_lights.*
	
				-- AVG Traffic light:
				-- if opp status = RETENTION -> traffic light has te be 1(red)
				-- Order related Traffic Lights are trible-weightes. 
				-- The rest are equally weighted.
				, ROUND (CASE WHEN 	Status 										= 'RETENTION' 
									OR Status 									= 'OFFBOARDING'
									OR 	traffic_light_Retention 				= 1 						THEN 1 
							ELSE	((	( traffic_light_noshow + traffic_light_cancelled_pro)*3 
										+ traffic_light_inbound_calls 
										+ traffic_light_lost_calls 
										+ traffic_light_CreatedCases
										+ traffic_light_InvoiceCorrection 
										+ traffic_light_ProfessionalImprovement 
										+ (CASE WHEN traffic_light_Retention = 2 							THEN 1 
												ELSE traffic_light_Retention END )) 
										/ 12) 
										END , 1) 															AS AVG_Traffic_Light

	FROM	(
			SELECT		traffic_light_detail.*
						, ROUND ((CASE	WHEN	nsp_Rate 						>= 0.05 					THEN 1
										WHEN	nsp_Rate 						BETWEEN 0.005 AND 0.05 		THEN 2
										WHEN	nsp_Rate 						<= 0.005 					THEN 3 
										END), 1)  															AS traffic_light_noshow
										
						, ROUND ((CASE	WHEN 	cp_Rate 						>= 0.1 						THEN 1
										WHEN 	cp_Rate 						BETWEEN 0.005 AND 0.1 		THEN 2
										WHEN 	cp_Rate 						<= 0.005 					THEN 3 
										END), 1)  															AS traffic_light_cancelled_pro
										
						, ROUND ((CASE	WHEN 	cc_Rate 						>= 0.2 						THEN 1
										WHEN 	cc_Rate 						BETWEEN 0.005 AND 0.2 		THEN 2
										WHEN 	cc_Rate 						<= 0.005 					THEN 3 
										END), 1) 															AS traffic_light_cancelled_customer
										
						, ROUND ((CASE	WHEN 	all_inbound_calls 				>= 3 						THEN 1
										WHEN 	all_inbound_calls 				BETWEEN 1 AND 3 			THEN 2
										WHEN 	all_inbound_calls 				<= 1 
											OR 	(all_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_inbound_calls
										
						, ROUND ((CASE	WHEN 	lost_inbound_calls				>= 3 						THEN 1
										WHEN 	lost_inbound_calls 				BETWEEN 1 AND 4 			THEN 2
										WHEN 	lost_inbound_calls <= 1 
											OR 	(lost_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_lost_calls
										
						, ROUND ((CASE	WHEN 	createdcases 					>= 3 						THEN 1
										WHEN 	createdcases 					BETWEEN 1 AND 3 			THEN 2
										WHEN 	createdcases					<=1 
											OR 	(createdcases IS NULL) 			= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_CreatedCases
										
						, ROUND ((CASE	WHEN 	retention_all 					> 0 
											AND retention_closed 				<> retention_all 			THEN 1 -- Retention case open
										WHEN 	retention_all 					> 0 
											AND retention_closed 				= retention_all 			THEN 2 -- Retention case closed
										WHEN 	retention_all 					= 0 
											OR 	(retention_all IS NULL) 		= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_Retention
								
						, ROUND ((CASE	WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		<> invoicecorrection_all 	THEN 1
										WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		= invoicecorrection_all 	THEN 1
										WHEN 	invoicecorrection_all 			= 0 
											OR 	(invoicecorrection_all IS NULL) = TRUE 						THEN 3 
										END), 1) 															AS traffic_light_InvoiceCorrection
						
						, ROUND	((CASE	WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	<> professionalimprovement_all 	THEN 1
										WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	= professionalimprovement_all 	THEN 1
										WHEN 	professionalimprovement_all 	= 0 
											OR (professionalimprovement_all IS NULL) = TRUE 					THEN 3 
										END), 1) 															AS traffic_light_ProfessionalImprovement


			FROM 	(
					SELECT 	O.locale
							, O.customer
							, O.opportunity_name
							, O.opportunity
							, O.status
							, O.opps
							, O.Owner																	

							-- , orders_final.delivery_area AS delivery_area_1
							-- , last_order.delivery_area__c AS delivery_area_2
							, CASE 		WHEN O.delivery_area IS NOT NULL THEN O.delivery_area
										ELSE 'unknown' 					END 								AS delivery_area
							, last_order.operated_by
							, last_order.operated_by_detail
			
					-- orders
							, orders_final.orders_nsp
							, orders_final.nsp
							, (CASE 	WHEN orders_final.orders_nsp 	> 0 		THEN orders_final.nsp/orders_final.orders_nsp 			ELSE 0 	END) 	AS nsp_Rate
							
							, orders_final.orders_cp
							, orders_final.cp
							, (CASE 	WHEN orders_final.orders_cp 	> 0 		THEN orders_final.cp/orders_final.orders_cp 			ELSE 0 	END) 	AS cp_Rate
							
							, orders_final.orders_cc
							, orders_final.cc
							, (CASE 	WHEN orders_final.orders_cc 	> 0 		THEN orders_final.cc/orders_final.orders_cc 			ELSE 0 	END) 	AS cc_Rate
							
							, orders_final.orders_nsp_cp
							, orders_final.nsp_cp
							, (CASE 	WHEN orders_final.orders_nsp_cp > 0 		THEN orders_final.nsp_cp/orders_final.orders_nsp_cp 	ELSE 0 	END) 	AS nso_cp_Rate
							
					-- inbound cases
							, Inbound_Cases_final.createdcases
							
					-- internal cases
							, Intern_Cases_final.retention_all
							, Intern_Cases_final.retention_closed
							, Intern_Cases_final.invoicecorrection_all
							, Intern_Cases_final.invoicecorrection_closed
							, Intern_Cases_final.professionalimprovement_all
							, Intern_Cases_final.professionalimprovement_closed
							
					-- calls
							, Calls_final.all_inbound_calls
							, Calls_final.lost_inbound_calls
							, Calls_final.all_outbound
							, Calls_final.b2b_outbound


FROM
(SELECT
locale__c																				AS locale
, delivery_area__c																		AS delivery_area
, customer__c																			AS Customer
, opp.name																				AS Opportunity_Name
, opp.sfid																				AS Opportunity
, status__c																				AS Status
, COUNT (opp.sfid) OVER (PARTITION BY customer__c) 						AS Opps
, tuser.name																			AS Owner

FROM
		Salesforce.opportunity Opp
		
		LEFT JOIN salesforce.user tuser ON (Opp.ownerid = tuser.sfid)
				
		
WHERE
test__c = false
-- AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')

GROUP BY
locale__c
, delivery_area__c
, customer__c
, opp.name		
, opp.sfid	
, status__c
, owner

) AS O

-- --
-- -- last order details
-- -- served by BAT (TFS) or Partner

LEFT JOIN

		(SELECT DISTINCT ON (opportunityid)
		opportunityid AS Opp
		, effectivedate::date 
		, sfid AS orderid
		, delivery_area__c
		, professional__c  AS Professional
		, operated_by
		, operated_by_detail
				 -- add any other column (expression) from the same row
		FROM   salesforce."order" AS orders
		
		LEFT JOIN
			(SELECT
				Professional.sfid AS professional,
				CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE 'Partner' END AS operated_by,
				CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE Partner.name END AS operated_by_detail
		
			FROM
				Salesforce.Account Professional
		
				LEFT JOIN Salesforce.Account Partner
					ON (Professional.parentid = Partner.sfid)
			)	AS ProfessionalDetails
		
			ON (orders.professional__c = ProfessionalDetails.professional	)
		
		WHERE
		opportunityid IS NOT NULL
		
		ORDER  BY 
		opportunityid, 
		effectivedate::date DESC
		
		) AS last_order

ON (last_order.Opp = O.opportunity)

-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')

LEFT JOIN		
		
		(
		SELECT
		Opp
		-- , delivery_area
		, SUM(Orders_NSP)			AS Orders_NSP
		, SUM(NSP)					AS NSP
		, SUM(Orders_CP)			AS Orders_CP
		, SUM(CP)					AS CP
		, SUM(Orders_CC)			AS Orders_CC
		, SUM(CC)					AS CC
		, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
		, SUM(NSP_CP)				AS NSP_CP
		
		FROM (
		SELECT 
		
		opportunityid AS Opp
		-- , delivery_area__c 																		AS delivery_area_mass
		-- , CASE 	WHEN (FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c)) IS NULL 
		--			THEN 'unknown'
		--			ELSE FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c) END		AS delivery_area
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
						WHEN status like '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
									
		FROM
		salesforce."order" AS orders
		
		WHERE
		effectivedate::date 	BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
									AND (CURRENT_DATE - INTERVAL '0 DAY')
-- 
-- effectivedate::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks'
-- 									AND to_date('2018-05-03', 'YYYY-MM-DD')
											
		AND opportunityid IS NOT NULL
		
		GROUP BY
		Opp
		, delivery_area__c 
		
		) AS orders2
		
		GROUP BY
		Opp
		--, delivery_area
		
) AS orders_final
		
ON (orders_final.opp = O.opportunity)

		
-- --
-- -- inbound cases
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
		
LEFT JOIN 
LATERAL
		
		(
		SELECT
		Opportunity
		, SUM(createdcases)										AS createdcases
		
		FROM
		(
		
		SELECT
		Opps.*
		, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * createdcases / Opps END)
							ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
											THEN 0.00
											ELSE 1.0 * createdcases END) END)::numeric, 2)							AS createdcases
		
		FROM
		(
		SELECT
		locale__c				AS locale
		, customer__c			AS Customer
		, name					AS Opportunity_Name
		, sfid					AS Opportunity
		, status__c				AS Status
		, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps
		
		FROM
				Salesforce.opportunity Opp
				
		WHERE
		test__c = false
-- 		AND stagename IN ('WON', 'PENDING') 
		AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')
		
		GROUP BY
		locale__c
		, customer__c
		, name		
		, sfid	
		, status__c ) AS Opps
		
		
		LEFT JOIN 
		LATERAL
		
		
		(SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity
		, COUNT(1) as CreatedCases
		
		FROM
		Salesforce.case		
		WHERE
		CreatedDate::date between (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' and (CURRENT_DATE - INTERVAL '0 DAY')

		AND 
		(type LIKE '%KA%'
		OR
		((Origin LIKE '%B2B - Contact%'
		OR Origin LIKE '%B2B AT - CLOSET%'
		OR Origin LIKE '%B2B CH - CLOSET%'
		OR Origin LIKE '%B2B DE - CLOSET%'
		OR Origin LIKE '%B2B NL - CLOSET%'
		OR Origin LIKE '%TFS CM%'
		OR Origin LIKE '%B2B de - Customer dashboard%')
		AND type != 'KA')
		OR (Origin LIKE '%B2B customer%' AND (type = 'CM B2B' OR type = 'Pool'))
		OR (Origin NOT LIKE '%B2B customer%' AND type = 'CM B2B')
		)
		AND Isdeleted = false
		AND CreatedById = '00520000003IiNCAA0'

		AND reason NOT IN ('Checkout')
		
		GROUP BY 
		Contact
		, Opportunity
		
		) AS Inbound_Cases
		
		ON (CASE WHEN Inbound_Cases.Opportunity IS NULL 
				THEN Opps.Customer = Inbound_Cases.Contact 
				ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)
				
		) AS Inbound_Cases_f
		
		GROUP BY
		Opportunity
		
) AS Inbound_Cases_final

ON (Inbound_Cases_final.Opportunity = O.Opportunity)




-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO

LEFT JOIN

		(SELECT
		Opportunity
		, SUM(retention_all)								AS retention_all
		, SUM(Retention_closed)							AS Retention_closed
		, SUM(InvoiceCorrection_All)					AS InvoiceCorrection_All
		, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
		, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
		, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
		
		FROM
		(
		SELECT
		Opps.*
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * retention_all / Opps END)
							ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
											THEN 0.00
											ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * Retention_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
											THEN 0.00
											ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed
		
		FROM
		(
		SELECT
		locale__c				AS locale
		, customer__c			AS Customer
		, name					AS Opportunity_Name
		, sfid					AS Opportunity
		, status__c				AS Status
		, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps
		
		FROM
				Salesforce.opportunity Opp
				
		WHERE
		test__c = false
-- 		AND stagename IN ('WON', 'PENDING') 
		AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')
		
		GROUP BY
		locale__c
		, customer__c
		, name		
		, sfid	
		, status__c ) AS Opps
		
	
		LEFT JOIN 
		LATERAL
		
		(
		SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity
		-- ,sfid
		, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
		AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment / invoice%'
		OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment /invoice%'
		OR subject LIKE '%nvoice %orrection%'
		AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
		
		FROM
		Salesforce.case	interncases	
		
		WHERE
			-- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
			CreatedDate::date between (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
									AND (CURRENT_DATE - INTERVAL '0 DAY')	
			
			AND Isdeleted = false
			
			-- internal cases: 
			-- 	Customer - Retention 		> communication with sales
			-- 	Order - Invoice editing 	> communication with finance 
			-- 	Professional - Improvement > communication with TO
			
			AND ((reason LIKE '%Customer - Retention%' 
						OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 
						
					OR (reason LIKE '%Professional - Improvement%' 
						AND (type LIKE '%TO%' OR type LIKE '%CLM%'))))
		
		
		GROUP BY
		Contact
		, Opportunity
		
		) AS Intern
		
		ON (CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)
				
		)		AS Intern_f
		
		GROUP BY
		Opportunity
		
) AS Intern_Cases_final
		
ON (Intern_Cases_final.opportunity = O.Opportunity)


-- --
-- -- calls:


LEFT JOIN

(
SELECT
Opps.opportunity
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)										AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)										AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
					

FROM
(
SELECT
locale__c				AS locale
, customer__c			AS Customer
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps

FROM
		Salesforce.opportunity Opp
		
WHERE
test__c = false
-- AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')

GROUP BY
locale__c
, customer__c
, name		
, sfid	
, status__c ) AS Opps


		LEFT JOIN 
		LATERAL
		(		
		SELECT

		RelatedContact__c 													AS Contact
		
	
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END)
			+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						THEN 1 
						ELSE 0 END) 											AS all_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END) 											AS lost_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
						THEN 1 
						ELSE 0 END)												AS all_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS b2b_outbound
		
		FROM 
		Salesforce.natterbox_call_reporting_object__c t4
		
			LEFT JOIN 
			salesforce.contact t5
			
			ON
			(t4.relatedcontact__c = t5.sfid)
		WHERE
		-- -- Calls created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
		call_start_date_date__c::date 	BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
													AND (CURRENT_DATE - INTERVAL '0 DAY')
		
		AND 	(	(calldirection__c = 'Inbound' 	AND E164Callednumber__C IN ('493030807263', '493030807264'))
				OR (calldirection__c = 'Outbound' 	AND Callerfirstname__c LIKE 'CM%'))
				
		AND RelatedContact__c IS NOT NULL
		
		GROUP BY
		Contact) as Call
		
		ON
		(Opps.Customer = Call.Contact)	
		
) AS Calls_final

ON (Calls_final.Opportunity = O.Opportunity)


) AS traffic_light_detail

) AS traffic_lights

;

-- ----------------------------------------------------------------- 
-- Author: Christina Janson
-- Short Description: Opportunity Traffic Light TODAY. Synced with Salesforce Marketing Cloud. 
-- Created on: 19/10/2018

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_today;
CREATE TABLE 			bi.opportunity_traffic_light_today 			AS 

	SELECT		*
	FROM 		bi.opportunity_traffic_light_new
	WHERE		date 												= (CURRENT_DATE - INTERVAL '0 DAY') 
;

-- ----------------------------------------------------------------- 
-- Author: Christina Janson
-- Short Description: 	Opportunity Traffic Light TODAY for tableau 
-- -------------------	(History and current AVG result in one table for tableau) 

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_tableau;
CREATE TABLE 			bi.opportunity_traffic_light_tableau 		AS 

	SELECT		history.date										AS history_date
				, history.locale									AS history_locale
				, history.customer									AS history_customer
				--, history.opportunity_name							AS history_opportunity_name
				, history.opportunity								AS history_opportunity
				, history.opps										AS history_opps
				, history.traffic_light_noshow						AS history_traffic_light_noshow
				, history.traffic_light_cancelled_pro				AS history_traffic_light_cancelled_pro
				, history.traffic_light_cancelled_customer			AS history_traffic_light_cancelled_customer
				, history.traffic_light_inbound_calls				AS history_traffic_light_inbound_calls
				, history.traffic_light_lost_calls					AS history_traffic_light_lost_calls
				, history.traffic_light_createdcases				AS history_traffic_light_createdcases
				, history.traffic_light_retention					AS history_traffic_light_retention
				, history.traffic_light_invoicecorrection			AS history_traffic_light_invoicecorrection
				, history.traffic_light_professionalimprovement		AS history_traffic_light_professionalimprovement
				, history.avg_traffic_light							AS history_avg_traffic_light
	
				, current.date										AS current__date
				, current.opportunity								AS current_opportunity
				, current.opportunity_name							AS current_opportunity_name
				, current.delivery_area								AS current_delivery_area
				, current.status									AS current_status
				, current.owner										AS current_owner
				, current.operated_by								AS operated_by
				, current.operated_by_detail						AS operated_by_detail
				, current.traffic_light_noshow						AS current_traffic_light_noshow
				, current.traffic_light_cancelled_pro				AS current_traffic_light_cancelled_pro
				, current.traffic_light_cancelled_customer			AS current_traffic_light_cancelled_customer
				, current.traffic_light_inbound_calls				AS current_traffic_light_inbound_calls
				, current.traffic_light_lost_calls					AS current_traffic_light_lost_calls
				, current.traffic_light_createdcases				AS current_traffic_light_createdcases
				, current.traffic_light_retention					AS current_traffic_light_retention
				, current.traffic_light_invoicecorrection			AS current_traffic_light_invoicecorrection
				, current.traffic_light_professionalimprovement		AS current_traffic_light_professionalimprovement
				, current.avg_traffic_light							AS current_avg_traffic_light
	
	FROM		bi.opportunity_traffic_light_new AS history
	
	LEFT JOIN 	(
				SELECT		*
				FROM		bi.opportunity_traffic_light_new
				WHERE		date 							= (CURRENT_DATE - INTERVAL '0 DAY')
				) AS 		current							ON (history.opportunity = current.opportunity)
	
	WHERE		history.date 								BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
						
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.click_to_signed;
CREATE TABLE bi.click_to_signed AS

SELECT

	LEFT(created.locale__c, 2) as country,
	-- created.sfid,
	created.created_date,
	-- contract_sent.sent_date,
	EXTRACT(EPOCH FROM (contract_sent.sent_date - created.created_date))/3600::numeric as time_creation_contract_sent,
	(click.clicks_or_reloads) as clicks_or_reloads,
	-- click.first_click,
	EXTRACT(EPOCH FROM (click.first_click - contract_sent.sent_date))/3600::numeric  as time_contract_sent_click,
	-- click.last_click_or_reload,
	-- click.time_first_to_last_click,
	-- signed.signed_date::time,
	EXTRACT(EPOCH FROM (signed.signed_date -  created.created_date))/3600::numeric  as time_creation_signed,
	EXTRACT(EPOCH FROM (signed.signed_date - contract_sent.sent_date))/3600::numeric  as time_contract_sent_signed,
	EXTRACT(EPOCH FROM (signed.signed_date - click.first_click))/3600::numeric  as time_first_click_signed
	
	-- EXTRACT(EPOCH FROM '2 months 3 days 12 hours 65 minutes'::INTERVAL)/60

FROM

	(SELECT
	
		o.createddate as created_date,
		o.sfid,
		o.locale__c
	
	FROM
	
		salesforce.opportunity o

	WHERE

		o.createddate >= '2018-01-22' ) as created


LEFT JOIN

	(SELECT

		t1.opportunityid,
		t1.createddate as signed_date
		
	FROM
	
	(SELECT
	
		DISTINCT oo.opportunityid,
		oo.createddate
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	
		
	WHERE
	
		oo.newvalue IN ('PENDING')) t1
		
	ORDER BY
	
		t1.createddate desc) as signed
		
		
ON 

	created.sfid = signed.opportunityid
	
LEFT JOIN

	(SELECT 

	   metadata_json#>>'{opportunity,Id}' as "opp_id",
	   COUNT(id) as "clicks_or_reloads",
	   MIN(created_at) as "first_click",
	   MAX(created_at) as "last_click_or_reload",
		MAX(created_at) - MIN(created_at) as time_first_to_last_click
	            
	FROM       
	
		events.sodium
	
	WHERE 
	
	   event_name = 'opportunity:get_offer'
	   AND id > 3300000
	            
	GROUP BY    
	
		"opp_id") as click
		
ON

	created.sfid = click.opp_id
	
LEFT JOIN

	(SELECT

		t1.opportunityid,
		t1.createddate as sent_date
		
	FROM
	
	(SELECT
	
		DISTINCT oo.opportunityid,
		oo.createddate
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	
		
	WHERE
	
		oo.newvalue IN ('CONTRACT SEND OUT')) t1
		
	ORDER BY
	
		t1.createddate desc) as contract_sent
		
ON

	created.sfid = contract_sent.opportunityid

GROUP BY

	LEFT(created.locale__c, 2),
	created.created_date,
	time_creation_contract_sent,
	time_contract_sent_click,
	time_creation_signed,
	time_contract_sent_signed,
	time_first_click_signed,
	clicks_or_reloads
	
ORDER BY 

	created.created_date desc;	
			
-- Author: Chandra
-- Short Description: This script calculates the actual mom churn rate and the churn rate before start, it uses bi.b2b_monthly_kpis and bi.closemonth_stats tables
-- Created on: 19/02/2018
DROP TABLE IF EXISTS bi.churn_rate;
CREATE TABLE bi.churn_rate AS
				
				
SELECT 	distinct main.year_month as year_month,
			-- On 27/03/2018 chandra: adding locale to the query to use as filter
			main.locale,
			new,
			total_bom,
			total_eom,
			round(((new.new - ((total_eom.total_eom) - (total_bom.total_bom))) / total_bom.total_bom) * 100, 2) AS churn_percent,
			lbs.left_before_start AS left_before_start_churn_percent
FROM bi.b2b_monthly_kpis main
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS new
		FROM bi.b2b_monthly_kpis _inn
			WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'New'
			GROUP BY year_month, locale
	) new
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS total_bom
		FROM bi.b2b_monthly_kpis _inn
			WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'Total BOM'
			GROUP BY year_month, locale
	) total_bom
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS total_eom
		FROM bi.b2b_monthly_kpis _inn
		WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'Total EOM'
			GROUP BY year_month, locale
	) total_eom
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	to_char(min_sign_date, 'YYYY-MM') AS signing_month,
					locale,
					round((SUM(CASE
							WHEN year_diff IS NULL AND status = 'LEFT' THEN unique_customers
								ELSE 0
							END) / sum(unique_customers)) * 100, 2) AS left_before_start

		FROM bi.closemonth_stats _inn
		WHERE _inn.signing_month = main.year_month
			AND _inn.locale = main.locale
		GROUP BY to_char(min_sign_date, 'YYYY-MM'), locale

	) lbs
ON TRUE
WHERE main."year_month" <= to_char(now(), 'YYYY-MM')
GROUP BY 
			main.year_month,
			main.locale,
			new.new,
			total_bom.total_bom,
			total_eom.total_eom,
			lbs.left_before_start
ORDER BY main.year_month desc;	


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED
-- churn date = last order's date - It's the last day on which they are making money
-- Created on: 02/07/2018

DROP TABLE IF EXISTS bi.churn_details;
CREATE TABLE bi.churn_details as 


SELECT  

		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		o.delivery_area__c,
		o.opportunityid,
		oo.name,
		oo.stagename,
		oo.status__c,
		ooo.potential as grand_total,
		oo.contract_duration__c,
		oo.hours_weekly__c,
	 	oo.hours_weekly__c*4.33 AS avg_monthly_hours,	
		oo.churn_reason__c,
		oo.nextstep,
		MAX(o.effectivedate) AS date_churn,
		MIN(o.effectivedate) AS first_order,
		MAX(o.effectivedate) - MIN(o.effectivedate) AS Age_days,
			CASE WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) < 31 THEN 'M0'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 31 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 61 THEN 'M1'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 61 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 92 THEN 'M2'
			  ELSE '>M3'
			  END as Age_Segment
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN

		bi.potential_revenue_per_opp ooo

	ON

		o.opportunityid = ooo.opportunityid
				
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE

	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		o.delivery_area__c,
		-- type_date,
		o.opportunityid,
		oo.name,
		oo.stagename,
		ooo.potential,
		oo.contract_duration__c,
		oo.hours_weekly__c,
		oo.status__c,
		oo.churn_reason__c,
		oo.nextstep;
		
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: Table containing the information regarding the activities of the partners on the portal (validation of orders, comments...)
-- Created on: 18/07/2018

DELETE FROM bi.monitoring_portal_partners WHERE year_month = TO_CHAR(current_date::date,'YYYY-MM');

INSERT INTO bi.monitoring_portal_partners

SELECT

	t5.*,
	t6.origin as origin_case,
	t6.subject,
	COUNT(DISTINCT t6.sfid_case) as number_cases

FROM

	(SELECT
	
	  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
	  MIN(effectivedate::date) as mindate,
	  MAX(effectivedate::date) as maxdate,
	  t3.delivery_areas__c as polygon,
	  t3.subcon as name_partner,
	  t3.sfid_partner,
	  COUNT(DISTINCT t4.sfid) as total_orders,
	  SUM(t4.order_duration__c) as hours,
	  COUNT(DISTINCT t4.professional__c) as number_prof,
	  COUNT(DISTINCT t4.contact__c) as number_customers,
	  SUM(CASE WHEN t4.status IN ('FULFILLED') THEN 1 ELSE 0 END) as fulfilled,
	  SUM(CASE WHEN t4.status IN ('INVOICED') THEN 1 ELSE 0 END) as invoiced,
	  SUM(CASE WHEN t4.status IN ('PENDING TO START') THEN 1 ELSE 0 END) as pts,
	  SUM(CASE WHEN t4.status IN ('NOSHOW CUSTOMER') THEN 1 ELSE 0 END) as noshow_customer,
	  SUM(CASE WHEN t4.status IN ('NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as noshow_professional,
	  SUM(CASE WHEN t4.status IN ('CANCELLED NOMANPOWER') THEN 1 ELSE 0 END) as nomanpower,
	  SUM(CASE WHEN t4.status LIKE '%ERROR%' THEN 1 ELSE 0 END) as error,
	  SUM(CASE WHEN t4.status LIKE '%MISTAKE%' THEN 1 ELSE 0 END) as mistake
  
	FROM
	
	  (SELECT
	  
	     t1.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t1
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t1.parentid)
	     
		WHERE 
		
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	   	and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
	      
	JOIN 
	
	  salesforce.order t4
	  
	ON
	
	  (t3.sfid = t4.professional__c)
	  
	WHERE
	
	  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
	  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
	  and LEFT(t4.locale__c,2) IN ('de')
	  and t4.effectivedate < '2019-04-01'
	  AND t4.type = 'cleaning-b2b'
	  AND t4.test__c IS FALSE
	
	GROUP BY
	
	  year_month,
	  polygon,
	  t3.subcon,
	  t3.sfid_partner
	  
	  
	ORDER BY
		
	  year_month,
	  polygon,
	  t3.subcon	) as t5
	  
LEFT JOIN	

	(SELECT

		TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
		MIN(o.createddate) as mindate,
		a.delivery_areas__c,
		a.company_name__c,
		a.sfid_partner,
		-- a.name,
		-- a.sfid as sfid_partner,
		o.sfid as sfid_case,
		o.accountid as sfid_cleaner,
		COUNT(DISTINCT o.contactid) as number_customers,
		COUNT(DISTINCT o.sfid) as number_cases,
		COUNT(DISTINCT oo.sfid) as number_orders,
		COUNT(DISTINCT oo.contact__c) as number_customers2,
		COUNT(DISTINCT oo.professional__c) as number_prof,
		SUM(oo.order_duration__c) as hours,
		SUM(CASE WHEN oo."status" = 'FULFILLED' THEN 1 ELSE 0 END) as orders_validated,
		o.origin,
		-- o.reason,
		o.subject
		
	FROM
	
		(SELECT
	  
	     t1.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t1
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t1.parentid)
	     
		WHERE 
		
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	   	and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') a
	   	
	LEFT JOIN
	
		salesforce."case" o
	
	ON
	
		 a.sfid = o.accountid
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		a.sfid = oo.professional__c
		AND TO_CHAR(o.createddate, 'YYYY-MM') = TO_CHAR(oo.effectivedate, 'YYYY-MM') 
		
	WHERE
	
		(o.origin = 'Partner portal' OR o.origin LIKE 'Partner - portal%')
		AND LOWER(o.description) NOT LIKE '%test%'
		AND LOWER(a.name) NOT LIKE '%test%'
		AND LOWER(a.company_name__c) NOT LIKE '%test%'
		AND LOWER(a.name) NOT LIKE '%book a cat%'
		AND oo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NO MANPOWER')
		
		AND LEFT(oo.locale__C,2) in ('de')
	   AND oo.test__c IS FALSE
		AND LOWER(o.description) NOT LIKE '%test%'
		
	GROUP BY
	
		year_month,
		a.company_name__c,
		a.sfid_partner,
		o.sfid,
		o.accountid,
		o.contactid,
		-- a.name,
		-- a.sfid,
		-- o.sfid,
		a.delivery_areas__c,
		o.origin,
		-- o.reason,
		o.subject
		
	ORDER BY
	
		year_month desc) as t6
		
ON

	t5.sfid_partner = t6.sfid_partner
	AND t5.year_month = t6.year_month
	
GROUP BY

	t5.year_month,
	t5.mindate,
	t5.maxdate,
	t5.polygon,
	t5.name_partner,
	t5.sfid_partner,
	t5.total_orders,
	t5.hours,
	t5.number_prof,
	t5.number_customers,
	t5.fulfilled,
	t5.invoiced,
	t5.pts,
	t5.noshow_customer,
	t5.noshow_professional,
	t5.nomanpower,
	t5.error,
	t5.mistake,
	t6.origin,
	t6.subject

ORDER BY

	year_month desc;


-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
 
 -- Sales First Response Time

DROP TABLE IF EXISTS bi.sales_firstresponsetime;
CREATE TABLE bi.sales_firstresponsetime as

SELECT DISTINCT ON (LikelieID)
*

FROM

(SELECT

t1.*,

-- ---------------------------------------------------------------------------------- difference: owner change -> first contact		

-- -
-- - working h Mo-Fr 09:00:00 - 18:00:00 
-- - summertime timezone difference of 2 h

CASE 
	WHEN t1.ownerchange_date::date IS NULL THEN 'rule 0.1'
	WHEN t1.first_contact::date IS NULL THEN 'rule 0.2'
	WHEN t1.ownerchange_date::timestamp > t1.first_contact::timestamp	THEN 'rule 1.0'
	WHEN t1.ownerchange_date::date = t1.first_contact::date THEN 
		(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' THEN 'rule 2.1' ELSE 'rule 2.2' END)
	ELSE 
		(CASE WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 'rule 3.1' 
				WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 'rule 3.2'
				ELSE (CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' THEN 'rule 3.3' ELSE 'rule 3.4'END)
		END) END AS rule_set,


CASE 
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau
	WHEN t1.ownerchange_date::timestamp > t1.first_contact::timestamp THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
	WHEN t1.ownerchange_date::date = t1.first_contact::date
	THEN 
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
		(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0'
				THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' ) * 60 +
						DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
				ELSE
						DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 60 +
						DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) 
				END)
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
	ELSE
	
	-- minutes pased on the owner change date UNTIL END OF WORK
				-- owner changed after working day
		(CASE WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 0 
				-- owner change at the weekend
				WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 0
				ELSE
					-- owner changed before working day
					(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
							ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - t1.ownerchange_date::time) * 60 +
									DATE_PART 	('minute', TIME '16:00:00.0' - t1.ownerchange_date::time) END) END)
		+
	
	-- minutes pased on the first contact date beginning at the morning working time
		(CASE WHEN t1.first_contact::time < TIME '07:00:00.0' THEN 0 
				WHEN t1.first_contact::time < TIME '16:00:00.0' 
				THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' )*60 +
						DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
				ELSE (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60 END)

		+
	
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
		(CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END)
			
END AS First_Response_Time_Minutes,

-- --------------------------------------------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------------------------------------------
-- FRT DETAILS

-- weekday
date_part('dow', t1.ownerchange_date::date) AS weekday,

-- minutes same day
CASE 	WHEN t1.ownerchange_date::date = t1.first_contact::date
		THEN 
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
		(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0'
				THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' )*60 +
						DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
				ELSE
						DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp)*60 +
						DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) 
				END) 
		ELSE NULL END AS FRT_sameday,

-- minutes until workday end

CASE  WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 0 
		WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 0
		ELSE
		-- owner changed before working day
			(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' 
					THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
					ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - t1.ownerchange_date::time)*60 +
							DATE_PART 	('minute', TIME '16:00:00.0' - t1.ownerchange_date::time) 
					END)
		END AS FRT_afternoon,
		
-- minutes pased on the first contact date beginning at the morning working time

CASE	WHEN t1.first_contact::time < TIME '07:00:00.0' THEN 0 
		WHEN t1.first_contact::time < TIME '16:00:00.0' 
		THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' )*60 +
				DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
		ELSE (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60 
		END AS FRT_morning,		

-- day difference - weekend
CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 
			END AS FRT_daydif,

-- day difference - weekend
CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) 
			ELSE 0 
			END AS FRT_days,


-- ---------------------------------------------------------------------------------- difference: created date -> owner change		
-- -
-- - working h Mo-Fr 09:00:00 - 18:00:00 
-- - summertime timezone difference of 2 h

CASE 

-- if likeli wasn't assigned to an agent use -1 to filter in tableau
	WHEN t1.ownerchange_date::timestamp IS NULL THEN -1

-- if the created date was after the owner change date just use the value "-1" -> this will be used as a filter in tableau
-- WHEN t1.ownerchange_date::timestamp < t1.created_date::timestamp
-- THEN -1

-- if created date and owner change date on the same day, calculate difference between times in minutes
	WHEN t1.ownerchange_date::date = t1.created_date::date
	THEN
		DATE_PART 	('day', t1.ownerchange_date::timestamp - t1.created_date::timestamp) * 24 +
		DATE_PART 	('hour', t1.ownerchange_date::timestamp - t1.created_date::timestamp) * 60 +
		DATE_PART 	('minute', t1.ownerchange_date::timestamp - t1.created_date::timestamp)
	
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
	ELSE
	
	-- minutes pased on the created date until end of work day
	-- in case likeli was created outside business hours use 0 as value
		(CASE WHEN 
		(TIME '16:00:00.0' < t1.created_date::time )
		THEN 0
		ELSE (DATE_PART 	('hour', TIME '16:00:00.0' - t1.created_date::time) * 60 +
		DATE_PART 	('minute', TIME '16:00:00.0' - t1.created_date::time)) END)
		+
	
	-- minutes pased on the owner change date beginning at the morning working time
		DATE_PART 	('hour', t1.ownerchange_date::time - TIME '07:00:00.0' ) * 60 +
		DATE_PART 	('minute', t1.ownerchange_date::time - TIME '07:00:00.0')
		+
	
	-- create a list of all date incl. created date and owner change date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: created date, owner change date -> minutes are calculated separatly
		(CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.created_date::date, t1.ownerchange_date::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.created_date::date, t1.ownerchange_date::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END)
				
END AS Assign_to_Agent_Minutes,

-- Likeli created outside of working hours
CASE WHEN TIME '16:00:00.0' < t1.created_date::time THEN 1 ELSE 0 END AS created_outside_working_h 


FROM
(SELECT
sub1.parentid AS LikelieID,
likeli.contact_name__c AS Name,
likeli.locale__c AS locale,
likeli.billingaddress_city__c AS city,
likeli.acquisition_channel__c AS aquisition_channel,
likeli.acquisition_tracking_id__c AS aquisition_trackingId,
likeli.stage__c AS stage,
likeli.lost_reason__c AS lost_reason,

CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
		WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
		ELSE 'Agent' END AS createdby,
sub1.createddate AS created_date,
-- CASE WHEN sub1.

CASE 	WHEN t2.ownerchange_date IS NOT NULL THEN t2.ownerchange_date
		WHEN t2.ownerchange_date IS NULL 
			AND (CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
							WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
							ELSE 'Agent' END) <> 'API Tiger' THEN sub1.createddate
		ELSE NULL END AS ownerchange_date,
		
-- t2.ownerchange_date AS ownerchange_date_old,
		
CASE 	WHEN t2.Agent IS NOT NULL THEN t2.Agent
		WHEN t2.Agent IS NULL 
			AND (CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
							WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
							ELSE 'Agent' END) <> 'API Tiger' THEN likeli.ownername
		ELSE NULL END AS Agent,
		
-- t2.Agent AS Agent_old,

t3.stage_not_reached_date AS stage_not_reached_date,
t4.stage_ended_date AS stage_ended_date,
t5.stage_reached_date AS stage_reached_date,
t6.stage_dm_reached_date AS stage_dm_reached_date,
t7.stage_qualified_date AS stage_qualified_date,


-- ---------------------------------------------------------------------------------- first contacts

LEAST 
		(t3.stage_not_reached_date,
		t4.stage_ended_date,
		t5.stage_reached_date,
		t6.stage_dm_reached_date,
		t7.stage_qualified_date) AS first_contact
		
		


FROM 
salesforce.likeli__history sub1

LEFT JOIN
			(
			SELECT
			sublikeli.sfid AS userid,
			subuser.name AS ownername,
			*
			FROM
			salesforce.likeli__c AS sublikeli
			
			LEFT JOIN	
				salesforce.user AS subuser
				ON (sublikeli.ownerid = subuser.sfid)
			
			) AS likeli
				
				
			ON
				(sub1.parentid = likeli.userid)
			

-- ---------------------------------------------------------------------------------- likelihistory - owner-change

LEFT JOIN
		(SELECT
		sub2.sfid,
		sub2.parentid,
		sub2.createddate AS ownerchange_date,
		CASE WHEN sub3.name IS NULL THEN sub2.newvalue ELSE sub3.name END AS Agent
		
		FROM 
		salesforce.likeli__history sub2
		
			LEFT JOIN
			salesforce.user sub3
			ON
				(sub2.newvalue = sub3.sfid)
		
		WHERE
		sub2.field = 'Owner'
		) AS t2
		
		ON (sub1.parentid = t2.parentid) 
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - NOT REACHED

LEFT JOIN
		(SELECT
		sub4.sfid,
		sub4.parentid,
		sub4.oldvalue,
		sub4.createddate AS stage_not_reached_date
		
		FROM 
		salesforce.likeli__history sub4
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'NOT REACHED'
		) AS t3
		
		ON (sub1.parentid = t3.parentid)
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - ENDED		

LEFT JOIN		
		(SELECT
		sub5.sfid,
		sub5.parentid,
		sub5.createddate AS stage_ended_date
		
		FROM 
		salesforce.likeli__history sub5
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'ENDED'
		) AS t4
		
		ON (sub1.parentid = t4.parentid)		
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - REACHED		

LEFT JOIN
		(SELECT
		sub6.sfid,
		sub6.parentid,
		sub6.createddate AS stage_reached_date
		
		FROM 
		salesforce.likeli__history sub6
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'REACHED'
		) AS t5

		ON ( sub1.parentid = t5.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - DM REACHED		

LEFT JOIN
		(SELECT
		sub7.sfid,
		sub7.parentid,
		sub7.createddate AS stage_dm_reached_date
		
		FROM 
		salesforce.likeli__history sub7
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'DM REACHED'
		) AS t6

		ON ( sub1.parentid = t6.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - QUALIFIED		

LEFT JOIN
		(SELECT
		sub8.sfid,
		sub8.parentid,
		sub8.createddate AS stage_qualified_date
		
		FROM 
		salesforce.likeli__history sub8
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'QUALIFIED'
		) AS t7

		ON ( sub1.parentid = t7.parentid)
		
				
WHERE
sub1.field = 'created'
AND sub1.createddate::date > '2017-06-01'
AND likeli.acquisition_channel__c <> 'outbound'
AND likeli.type__c <> 'B2C'
AND likeli.test__c = 'false'
AND likeli.ownerid NOT LIKE '00520000004n1OxAAI'



GROUP BY
sub1.parentid,
likeli.contact_name__c,
likeli.locale__c,
likeli.billingaddress_city__c,
likeli.acquisition_channel__c,
likeli.acquisition_tracking_id__c,
likeli.stage__c,
likeli.lost_reason__c,
likeli.ownername,

likeli.createdbyid,
sub1.createddate,

t2.ownerchange_date,
t2.Agent,

t3.stage_not_reached_date,
t4.stage_ended_date,
t5.stage_reached_date,
t6.stage_dm_reached_date,
t7.stage_qualified_date
) AS t1

ORDER BY created_date desc
	
) AS FRT_final

ORDER BY LikelieID, ownerchange_date ASC;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.variation_grand_total;
CREATE TABLE bi.variation_grand_total as
		
		
SELECT

	t4.year_week,
	MIN(t4.mindate) as mindate,
	SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as oldvalue,
	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) as newvalue,

	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) - SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as variation

FROM

			
	(SELECT
	
		t1.year_week,
		t1.opportunityid,
		t1.mindate,
		t1.maxdate,
		t2.oldvalue,
		t3.newvalue
				
	FROM
	
		
		(SELECT -- Here I create the list of first and last changes, every week, for every opportunity
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			MIN(o.createddate) as mindate,
			MAX(o.createddate) as maxdate,
			o.opportunityid
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('1')
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.opportunityid
			
		ORDER BY
		
			year_week desc) as t1
			
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every week
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_week desc) as t2
		
	ON
	
		t1.mindate = t2.createddate
		AND t1.opportunityid = t2.opportunityid
	
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every week
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_week desc) as t3
		
	ON
	
		t1.maxdate = t3.createddate
		AND t1.opportunityid = t3.opportunityid
		
	ORDER BY
	
		t1.year_week desc) as t4
		
GROUP BY

	t4.year_week
	
ORDER BY 

	t4.year_week desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------



DELETE FROM public.marketing_spending;

INSERT INTO public.marketing_spending

SELECT

	l."Date" as start_date,
	l."Date" as end_date,	
	LEFT(LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)), 2) as locale,	
	LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)) as languages,	
	LOWER(SUBSTRING(l."CampaignName" from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')) as "city",	
	LEFT(l."CampaignName", 3) as type,	
	'Marketing Costs' as kpi,	
	'Adwords' as sub_kpi_1,
		
	CASE WHEN l."AccountDescriptiveName" = 'Reinigung Direkt' THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services' END as sub_kpi_2,
	
	LOWER(substring(l."AdGroupName" from '[0-9]{3}_[A-Z]{2}_([^\+]+)')) as sub_kpi_3,
	'New' as sub_kpi_4,
	'SEM B2B' as sub_kpi_5,
	'' as sub_kpi_6,
	'' as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	SUM(l."Cost"::numeric/1000000) as value
	
	
	-- CASE WHEN l."AccountDescriptiveName" IN ('unbounce rei') THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services'
		
	-- lower(substring(l.campaignname from 'tpc:(.[^\,]+)')) as "keyword",
	-- lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as "city",


FROM

	public.adwords_adgroup_report l

GROUP BY

	l."Date",
	l."AccountDescriptiveName",
	LOWER(SUBSTRING(l."CampaignName" from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')),
	LEFT(LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)), 2),
	LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)),
	LEFT(l."CampaignName", 3),
	LOWER(substring(l."AdGroupName" from '[0-9]{3}_[A-Z]{2}_([^\+]+)'));

---------------------------------------------------------------------------------------------

INSERT INTO public.marketing_spending	

SELECT

	l.date as start_date,
	l.date as end_date,	
	LEFT(LOWER(RIGHT(LEFT(l.campaign, 9), 5)), 2) as locale,	
	LOWER(RIGHT(LEFT(l.campaign, 9), 5)) as languages,	
	LOWER(SUBSTRING(l.campaign from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')) as "city",	
	LEFT(l.campaign, 3) as type,	
	'Marketing Costs' as kpi,	
	'Bing' as sub_kpi_1,
		
	CASE WHEN l.account = 'Reinigung Direkt DE' THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services' END as sub_kpi_2,
	
	LOWER(substring(l.adgroup from '[0-9]{3}_[A-Z]{2}_([^\+]+)')) as sub_kpi_3,
	'New' as sub_kpi_4,
	'SEM B2B' as sub_kpi_5,
	'' as sub_kpi_6,
	'' as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	SUM(l.cost::numeric) as value
	
	
	-- CASE WHEN l."AccountDescriptiveName" IN ('unbounce rei') THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services'
		
	-- lower(substring(l.campaignname from 'tpc:(.[^\,]+)')) as "keyword",
	-- lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as "city",

FROM

	public.bing_ads_report l

GROUP BY

	l.date,
	l.account,
	LOWER(SUBSTRING(l.campaign from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')),
	LEFT(LOWER(RIGHT(LEFT(l.campaign, 9), 5)), 2),
	LOWER(RIGHT(LEFT(l.campaign, 9), 5)),
	LEFT(l.campaign, 3),
	LOWER(substring(l.adgroup from '[0-9]{3}_[A-Z]{2}_([^\+]+)'));

---------------------------------------------------------------------------------------------
	
	
INSERT INTO public.marketing_spending	
	
SELECT

	TO_CHAR(l.createddate, 'YYYY-MM-DD')::date as start_date,
	TO_CHAR(l.createddate, 'YYYY-MM-DD')::date as end_date,	
	
	LEFT(l.locale__c, 2) as locale,	
	l.locale__c as languages,	
	
	LOWER(SUBSTRING(REPLACE(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
	
		
	l.type__c as type,	
	'Likelies' as kpi,
	CASE 	
		WHEN LOWER(l.acquisition_channel_ref__c) = 'anygrowth' and l.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'
	
		WHEN LOWER(l.acquisition_channel_ref__c) = 'reveal' and l.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'
	
		WHEN (l.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
		
		WHEN (l.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
		
		WHEN (l.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'
	
		WHEN ((lower(l.acquisition_channel__c) = 'inbound') AND lower(l.acquisition_tracking_id__c) = 'classifieds')					
		THEN 'Classifieds'
					
		WHEN (l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	
	         AND l.createddate::date <= '2017-02-08'
	
	     THEN 'SEM Brand'::text
	
	     WHEN ((((l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goob%')
	
	     	AND l.createddate::date > '2017-02-08'
	     	AND ((l.acquisition_channel_params__c LIKE '%goob%') AND (l.acquisition_channel_params__c LIKE '%bt:b2b%' OR l.acquisition_channel_params__c LIKE '%bt: b2b%' OR l.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
	     	)
	
	     THEN 'SEM Brand B2B'::text
	
	     WHEN (((l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goob%')
	
	     	AND l.createddate::date > '2017-02-08'
			AND ((l.acquisition_channel_params__c LIKE '%goob%') AND (l.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%' ))
	     	
	     THEN 'SEM Brand B2C'::text
	     
	
	     WHEN (((l.acquisition_channel_params__c::text ~~ '%goog%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goog%')
	
	     	AND l.createddate::date <= '2017-02-08'
	     THEN 'SEM'::text
	
	     WHEN (l.acquisition_channel_params__c::text ~~ '%goog%'::text) AND (l.acquisition_channel_params__c LIKE '%bt: b2b%' OR l.acquisition_channel_params__c LIKE '%"bt":"b2b%') THEN 'Adwords'
	     
	     WHEN l.acquisition_channel_params__c::text ~~ '%ysm%'::text THEN 'Bing'
	
	     WHEN ((((l.acquisition_channel_params__c::text ~~ '%goog%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goog%')
	
	     	AND l.createddate::date > '2017-02-08' 
	     	AND ((l.acquisition_channel_params__c LIKE '%goog%') AND (l.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%')))
	     	OR l.acquisition_channel_params__c::text LIKE '%goog%'
	     THEN 'SEM B2C'::text
	     
	
	     WHEN (l.acquisition_channel_params__c::text ~~ '%disp%'::text OR l.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%facebook%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	     THEN 'Display'::text
	                 
	
	     WHEN l.acquisition_channel_params__c::text ~~ '%ytbe%'::text
	     THEN 'Youtube Paid'::text
	
	
	     WHEN (l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_ref__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text
	         AND LOWER(acquisition_tracking_id__c) LIKE '%b2b seo page form&'
	     THEN 'SEO B2B'::text
	     
	
	     WHEN ((l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_ref__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text
			AND LOWER(acquisition_tracking_id__c) NOT LIKE '%b2b seo page form&')
	
	     	OR
	     		((acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL AND (acquisition_tracking_id__c LIKE '%b2b%' OR acquisition_tracking_id__c LIKE '%tfs%')))
	
	     THEN 'SEO'::text
	     
	     WHEN (l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goog%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	         THEN 'SEO Brand'::text
	         
	     WHEN (l.acquisition_channel_params__c::text ~~ '%batfb%'::text  
	             OR l.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%facebook%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
	     THEN 'Facebook'::text
	
	
	     WHEN l.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%email%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%vero%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%batnl%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%fullname%'::text
	             OR l.acquisition_channel_params__c::text ~~ '%invoice%'::text 
	     THEN 'Newsletter'::text
	     
	     WHEN (l.acquisition_channel_params__c::text !~~ '%goog%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	             AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	             AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%fb%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%clid%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%utm%'::text 
	             AND l.acquisition_channel_params__c::text <> ''::text)
	     	OR (acquisition_channel__c IS NULL AND acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL)
	     THEN 'DTI'::text
	     
	     ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as sub_kpi_1,	

	CASE WHEN (l.acquisition_tracking_id__c LIKE 'rd %' OR l.acquisition_tracking_id__c LIKE '%unbounce_reinigungdirekt%') THEN 'Reinigungdirekt'
	     WHEN (l.acquisition_tracking_id__c LIKE 'tfs %' OR l.acquisition_tracking_id__c LIKE '%unbounce_tfs%') THEN 'Tiger Facility Services'
		  ELSE 'Other'
		  END as sub_kpi_2,
	l.acquisition_tracking_id__c as sub_kpi_3,
	lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as sub_kpi_4,
	-- LOWER(SUBSTRING(l.acquisition_channel_params__c from 'tpc:(.[^\,]+)')) as sub_kpi_3,
	
	'SEM B2B' as sub_kpi_5,
	
	l.stage__c as sub_kpi_6,
	l.lost_reason__c as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	COUNT(DISTINCT l.sfid) as value

FROM

	salesforce.likeli__c l
	
WHERE

	l.type__c = 'B2B'
	AND l.acquisition_channel__c IN ('web', 'inbound')
	AND l.createddate > '2017-12-31'	
	AND l.company_name__c NOT LIKE '%test%'
	AND l.company_name__c NOT LIKE '%bookatiger%'
	AND l.email__c NOT LIKE '%bookatiger%'
	
	AND lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) NOT LIKE '%test%'
	
GROUP BY

	start_date,
	end_date,	
	l.acquisition_channel_params__c,
	LEFT(l.locale__c, 2),	
	l.locale__c,	
	type,	
	kpi,	
	sub_kpi_1,	
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5,
	l.stage__c,
	l.lost_reason__c,
	sub_kpi_8,
	sub_kpi_9,
	sub_kpi_10
	
ORDER BY

	start_date desc;
	
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: Creates a list of all the opportunities running at the moment and give insights on their current status concerning their notice date and traffic light
-- Created on: 25/10/2018

DROP TABLE IF EXISTS bi.notice_lights;
CREATE TABLE bi.notice_lights AS

SELECT

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	o.grand_total__c,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.opportunityid,
	oo.name,
	oo.status__c,
	t1.current_owner,
	t1.operated_by,
	t1.operated_by_detail,
	t1.current_avg_traffic_light,	
	CASE WHEN (current_date > (notice_date__c - 30) AND (current_date <= notice_date__c)) THEN 'Notice approaching (within 30days)'
		 WHEN  current_date > notice_date__c THEN 'Notice expired'
		 ELSE 'Notice far away (more than 30 days)' END as notice_type,	
	CASE WHEN current_date > (notice_date__c - 30) AND t1.current_avg_traffic_light <= 2.5 THEN 'Alert' ELSE 'Low priority' END as status_notice,	
	o.start__c,	
	o.end__c,	
	o.notice_date__c,
	o.notice__c,
	CASE WHEN  o.grand_total__c = '0' THEN 'irregular' ELSE
	(CASE 
		-- duration: unlimited
		WHEN additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
		WHEN additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
		-- duration: 12 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
		-- duration: 6 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
		-- duration: 3 month
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
		-- duration: 1 month
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
		ELSE (CASE WHEN o.duration__c IS NULL THEN '1' ELSE o.duration__c END)END) END as duration2,
	o.additional_agreements__c

FROM	
	
	(SELECT
	
		year_month,
		date,
		-- max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid,
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps' as varchar) as kpi,
			CAST('Count' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.category
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				oo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								o.delivery_area__c,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								
							GROUP BY
							
								o.opportunityid,
								o.locale__c,
								o.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND oo.test__c IS FALSE
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid

			LEFT JOIN

				bi.potential_revenue_per_opp oo

			ON

				t2.opportunityid = oo.opportunityid
	
			WHERE 
	
				o.test__c IS FALSE
				
			GROUP BY
			
					oo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category
				) as table1
	
		WHERE
	
			table1.opportunityid IS NOT NULL
				
		GROUP BY
		
			table1.year_month,
			table1.ymd_max,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	GROUP BY
	
		year_month,
		date,
		-- max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		table2.opportunityid,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5
		
	ORDER BY 
	
		year_month desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_tableau t1
	
ON

	table3.opportunityid = t1.current_opportunity
	
LEFT JOIN

	salesforce.contract__c o
	
ON

	table3.opportunityid = o.opportunity__c
	
LEFT JOIN	

	salesforce.opportunity oo
	
ON

	table3.opportunityid = oo.sfid
	
WHERE

	table3.year_month = TO_CHAR(current_date, 'YYYY-MM')
	AND table3.value = 1
	AND t1.current_avg_traffic_light IS NOT NULL
	AND o.status__c IN ('ACCEPTED', 'SIGNED')
	AND o.service_type__c LIKE 'maintenance cleaning'
	-- AND o.active__c IS TRUE
	
GROUP BY

	table3.year_month,
	table3.date,
	-- max_date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	o.grand_total__c,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.opportunityid,
	oo.name,
	oo.status__c,
	t1.current_owner,
	t1.operated_by,
	t1.operated_by_detail,
	table3.value,
	t1.current_avg_traffic_light,
	o.start__c,
	o.end__c,
	o.notice_date__c,
	o.notice__c,
	o.duration__c,
	o.additional_agreements__c,
	CASE WHEN  o.grand_total__c = '0' THEN 'irregular' ELSE
	(CASE 
		-- duration: unlimited
		WHEN additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
		WHEN additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
		-- duration: 12 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
		-- duration: 6 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
		-- duration: 3 month
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
		-- duration: 1 month
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
		ELSE (CASE WHEN o.duration__c IS NULL THEN '1' ELSE o.duration__c END)END) END;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: new analysis regarding the churn, new filters on the contracts, the providers, the category of revenue, the cohort... (CHURNED OPPS PART)
-- Created on: 25/10/2018


DROP TABLE IF EXISTS bi.deep_churn;
CREATE TABLE bi.deep_churn as

SELECT

	table3.year_month,
	table3.year_month_start,
	table3.mindate_churn as date,
	-- max_date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	
	table3.pricing,
	table3.money,
	table3.churn_reason,
	table3.contract_duration,
	
	table3.value,
	
	CASE WHEN table6.provider IS NULL THEN 'Not matched yet' ELSE table6.provider END as provider,
	table6.company_name,
	CASE WHEN table3.churn_reason LIKE '%one off%' THEN 'One-Off'
		  ELSE 'Recurrent'
		  END as acquisition_type,
	MIN(CASE WHEN (table7.name NOT LIKE '%S' AND table7.name NOT LIKE '%G' AND table7.name NOT LIKE '%N') THEN table7.amount__c ELSE 0 END)/1.19 as amount__c

FROM	
	
	(SELECT
	
		TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
		TO_CHAR(table1.date_start,'YYYY-MM') as year_month_start,
		MIN(table1.date_churn::date) as mindate_churn,
		LEFT(table1.country,2) as locale,
		table1.contract_when_accepted__c,
		table1.closedate,
		table1.locale__c as languages,
		-- CAST('-' as varchar) as city,
		table1.delivery_area__c as city,
		table1.opportunityid,
		table1.name,
		CAST('B2B' as varchar) as type,
		CAST('Churn' as varchar) as kpi,
		CAST('Count opps' as varchar) as sub_kpi_1,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
				  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
				  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
				  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
				  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
				  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
				  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
				  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
				  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
				  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
				  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
				  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
				  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
				  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
				  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
				  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
				  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
				  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
				  ELSE '>M18'
				  END as sub_kpi_2,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			  
		CASE WHEN table1.money < 250 THEN '<250€'
			  WHEN table1.money >= 250 AND table1.money < 500 THEN '250€-500€'
			  WHEN table1.money >= 500 AND table1.money < 1000 THEN '500€-1000€'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
		CASE WHEN table1.money < 250 THEN 'Very Small'
			  WHEN table1.money >= 250 AND table1.money < 500 THEN 'Small'
			  WHEN table1.money >= 500 AND table1.money < 1000 THEN 'Medium'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
		table1.pricing,
		table1.money,
		table1.churn_reason__c as churn_reason,
		MAX(table1.contract_duration) contract_duration,
		COUNT(DISTINCT table1.opportunityid) as value
	
	FROM
		
		(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
		         -- It's the last day on which they are making money
			LEFT(oo.locale__c, 2) as country,
			oo.locale__c,
			o.delivery_area__c,
			o.opportunityid,
			oo.name,
			ooooo.potential as money,
			CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END as pricing,
			'last_order' as type_date,
			MIN(o.effectivedate) as date_start,
			MAX(o.effectivedate) as date_churn,
			MIN(oo.contract_when_accepted__c) as contract_when_accepted__c,
			MIN(oo.closedate) as closedate,
			oo.churn_reason__c,
			CASE WHEN oooo.grand_total__c = '0' THEN 'irregular' ELSE
			(CASE 
				-- duration: unlimited
				WHEN oooo.additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
				-- duration: 12 month
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
				-- duration: 6 month
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
				-- duration: 3 month
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
				-- duration: 1 month
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
				ELSE (CASE WHEN oooo.duration__c IS NULL THEN '1' ELSE oooo.duration__c END)END) END as contract_duration
			
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			o.opportunityid = oo.sfid
			
		LEFT JOIN
		
			bi.b2borders ooo
			
		ON
		
			o.opportunityid = ooo.opportunity_id
			
		LEFT JOIN
		
			salesforce.contract__c oooo
		
		ON
		
			o.opportunityid = oooo.opportunity__c

		LEFT JOIN

			bi.potential_revenue_per_opp ooooo

		ON

			o.opportunityid = ooooo.opportunityid
			
		WHERE
		
			o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.status__c IN ('RESIGNED', 'CANCELLED')
			-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
			AND oo.test__c IS FALSE
			AND o.test__c IS FALSE
			AND o.professional__c IS NOT NULL
			AND oooo.service_type__c LIKE 'maintenance cleaning'
			-- AND oooo.active__c IS TRUE
		
		GROUP BY
		
			LEFT(oo.locale__c, 2),
			oo.locale__c,
			o.delivery_area__c,
			type_date,
			ooooo.potential,
			CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END,
			oo.churn_reason__c,
			o.opportunityid,
			oo.name,
			contract_duration) as table1
			
	GROUP BY
	
		TO_CHAR(table1.date_churn, 'YYYY-MM'),
		TO_CHAR(table1.date_start,'YYYY-MM'),
		table1.country,
		table1.locale__c,
		table1.delivery_area__c,
		table1.opportunityid,
		table1.name,
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table1.pricing,
		table1.money,
		-- table2.operated_by,
		-- table2.operated_by_detail,
		table1.contract_when_accepted__c,
		table1.churn_reason__c,
		-- table1.contract_duration,
		table1.closedate
		
	ORDER BY
	
		TO_CHAR(table1.date_churn, 'YYYY-MM') desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_new table4
	
ON
	
	table3.opportunityid = table4.opportunity
	
LEFT JOIN

	bi.b2b_additional_booking table5
	
ON 
	
	table3.opportunityid = table5.opportunity
		
LEFT JOIN

	bi.order_provider table6
	
ON
	
	table3.opportunityid = table6.opportunityid
	
LEFT JOIN

	salesforce.invoice__c table7
	
ON

	table3.opportunityid = table7.opportunity__c
	AND table3.year_month = TO_CHAR(table7.issued__c, 'YYYY-MM')
	
GROUP BY

	table3.year_month,
	table3.year_month_start,
	table3.mindate_churn,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.pricing,
	table3.money,
	table3.churn_reason,
	table3.contract_duration,
	table3.value,
	table6.provider,
	table5.acquisition_type,
	table6.company_name
	
ORDER BY

	table3.year_month desc;
	
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: new analysis regarding the churn, new filters on the contracts, the providers, the category of revenue, the cohort... (RUNNING OPPS PART)
-- Created on: 25/10/2018

INSERT INTO bi.deep_churn

SELECT

	table3.year_month,
	table3.year_month_start,
	table3.date,
	-- max_date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	
	table3.pricing,
	table3.money,
	table3.churn_reason__c,
	table3.contract_duration,
	
	table3.value,
		
	CASE WHEN table6.provider IS NULL THEN 'Not matched yet' ELSE table6.provider END as provider,
	table6.company_name,
	CASE WHEN table3.churn_reason__c LIKE '%one off%' THEN 'One-Off'
		  ELSE 'Recurrent'
		  END as acquisition_type,
	MIN(CASE WHEN (table7.name NOT LIKE '%S' AND table7.name NOT LIKE '%G' AND table7.name NOT LIKE '%N') THEN table7.amount__c ELSE 0 END)/1.19 as amount__c

FROM	
	
	(SELECT
	
		year_month,
		year_month_start,
		date,
		-- max_date,
		locale,
		contract_when_accepted__c,
		closedate,
		languages,
		city,
		table2.opportunityid,
		table2.name,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		
		table2.pricing,
		table2.grand_total as money,
		table2.churn_reason__c,
		table2.contract_duration,
		
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			TO_CHAR(table1.date_start,'YYYY-MM') as year_month_start,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			table1.contract_when_accepted__c,
			table1.closedate,
			table1.churn_reason__c,
			table1.contract_duration,
			
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps' as varchar) as kpi,
			CAST('Count' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.name,
			table1.category,
			table1.pricing,
			table1.grand_total
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				ooooo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.name,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn,
							t1.contract_when_accepted__c,
							t1.closedate,
							t1.churn_reason__c,
							MAX(t1.money) as money,
							t1.pricing,
							MAX(t1.contract_duration) as contract_duration
																								
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								oo.name,
								o.delivery_area__c,
								MIN(o.effectivedate) as date_start,
								MIN(oo.contract_when_accepted__c) as contract_when_accepted__c,
								MIN(oo.closedate) as closedate,
								oo.churn_reason__c,
								ooooo.potential as money,
								CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END as pricing,
								CASE WHEN oooo.grand_total__c = '0' THEN 'irregular' ELSE
								(CASE 
									-- duration: unlimited
									WHEN oooo.additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
									-- duration: 12 month
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
									-- duration: 6 month
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
									-- duration: 3 month
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
									-- duration: 1 month
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
									ELSE (CASE WHEN oooo.duration__c IS NULL THEN '1' ELSE oooo.duration__c END)END) END as contract_duration
							
							FROM
							
								salesforce.order o	
								
							LEFT JOIN
			
								salesforce.opportunity oo
				
							ON 
			
								o.opportunityid = oo.sfid
								
							LEFT JOIN
			
								salesforce.contract__c oooo
							
							ON
							
								o.opportunityid = oooo.opportunity__c

							LEFT JOIN

								bi.potential_revenue_per_opp ooooo

							ON

								o.opportunityid = ooooo.opportunityid
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
								AND o.test__c IS FALSE
								AND oooo.service_type__c LIKE 'maintenance cleaning'
			                    -- AND oooo.active__c IS TRUE
								
							GROUP BY
							
								o.opportunityid,
								CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END,
								oo.name,
								ooooo.potential,
								o.locale__c,
								contract_duration,
								oo.churn_reason__c,
								o.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND oo.test__c IS FALSE
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid
							
						GROUP BY
						
							
							t1.country,
							t1.locale__c,
							t1.delivery_area__c,
							t1.opportunityid,
							t1.name,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01',
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END,
							t1.contract_when_accepted__c,
							t1.closedate,
							t1.churn_reason__c,
							-- t1.money,
							t1.pricing) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid

			LEFT JOIN

				bi.potential_revenue_per_opp ooooo

			ON

				t2.opportunityid = ooooo.opportunityid
	
			WHERE 
	
				o.test__c IS FALSE
				
			GROUP BY
			
					ooooo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.name,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category,
					t2.contract_when_accepted__C,
					t2.closedate,
					t2.churn_reason__c,
					t2.money,
					t2.pricing,
					t2.contract_duration
				) as table1
	
		WHERE
	
			table1.opportunityid IS NOT NULL
				
		GROUP BY
		
			table1.year_month,
			TO_CHAR(table1.date_start,'YYYY-MM'),
			table1.ymd_max,
			table1.contract_when_accepted__c,
			table1.closedate,
			table1.churn_reason__c,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid,
			table1.name,
			table1.pricing,
			table1.grand_total,
			table1.contract_duration
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	GROUP BY
	
		year_month,
		year_month_start,
		date,
		-- max_date,
		locale,
		contract_when_accepted__c,
		closedate,
		churn_reason__c,
		languages,
		city,
		type,
		kpi,
		opportunityid,
		name,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.pricing,
		table2.grand_total,
		table2.contract_duration
		
	ORDER BY 
	
		year_month desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_new table4
	
ON
	
	table3.opportunityid = table4.opportunity
	
LEFT JOIN

	bi.b2b_additional_booking table5
	
ON 
	
	table3.opportunityid = table5.opportunity
	
LEFT JOIN

	bi.order_provider table6
	
ON
	
	table3.opportunityid = table6.opportunityid
	
LEFT JOIN

	salesforce.invoice__c table7
	
ON

	table3.opportunityid = table7.opportunity__c
	AND table3.year_month = TO_CHAR(table7.issued__c, 'YYYY-MM')
	
WHERE

	table3.value = 1
	-- AND table3.year_month = '2018-09'
	-- AND table3.locale = 'de'
	
GROUP BY

	table3.year_month,
	table3.year_month_start,
	table3.date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.pricing,
	table3.money,
	table3.churn_reason__c,
	table3.contract_duration,
	table3.value,
	table6.provider,
	table5.acquisition_type,
	table6.company_name
	
ORDER BY

	table3.year_month desc;



-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: latest valid (INVOICED,FULFILLED or CANCELLED CUSTOMER) order and information about provider (BAT or Partner)
-- Created on: 17/10/2018

DROP TABLE IF EXISTS 		bi.order_provider;
CREATE TABLE 				bi.order_provider AS 


SELECT DISTINCT ON 	(orders.opportunityid)
			orders.delivery_area__c
			, orders.sfid AS orderid
			, orders.opportunityid
			, opp.name as opportunity
			, orders.effectivedate
			, orders.professional__c
			, prof.pro_name
			, prof.provider
			, prof.company_name

FROM 		salesforce.order orders

LEFT JOIN 	salesforce.opportunity opp 					ON ( orders.opportunityid = opp.sfid)
LEFT JOIN 	( SELECT 
					pro.sfid															pro_id
					, pro.name															pro_name
					, pro.company_name__c												pro_company_name
					, company.sfid														company_id
					, company.name														company_name
					, CASE 	WHEN company.sfid 	= '0012000001TDMgGAAX' 	THEN 'BAT'
							WHEN company.sfid 	IS NULL					THEN 'unknown'
							ELSE 'Partner' 								END 			provider
					-- ,*
			FROM 		salesforce.account pro
			LEFT JOIN 	salesforce.account company		ON (pro.parentid = company.sfid)
			) AS prof									ON (prof.pro_id = orders.professional__c)

WHERE 		orders.status IN ('INVOICED','FULFILLED','PENDING TO START','NOSHOW CUSTOMER')

ORDER BY 	orders.opportunityid, 
			orders.effectivedate DESC
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Backlog today (used for the backlog history tracking)
-- Created on: 01/11/2018

DROP 	TABLE IF EXISTS 	bi.CM_cases_today;
CREATE 	TABLE 				bi.CM_cases_today 	AS 

SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.createddate				case_createddate
			, ca.isclosed					case_isclosed
			, ca.ownerid					case_ownerid
			,  u.name						case_owner
			, ca.origin						case_origin
			, ca.type						case_type
			, ca.reason						case_reason
			, ca.status						case_status
			, ca.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, ca.order__c					orderid
			, o.type						order_type
			, ca.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, ca.opportunity__c				opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE		ca.isclosed				= FALSE																					-- 1	
	AND (opp.test__c				= FALSE  
		OR opp.test__c				IS NULL)
					
	--	excluded case owner	
	AND((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1
				WHEN 	u.name 		LIKE 	'%Frank_Wendt%' 		THEN 1  
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	ca.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'				THEN 1
						WHEN 	ca.type 	= 'CLM'					THEN 1
						WHEN 	ca.type		= 'CM B2C'				THEN 1
						WHEN 	ca.type		= 'Sales'				THEN 1
						WHEN 	ca.type		= 'PM'				THEN 1
						WHEN 	ca.type		= 'Damage'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	LIKE 'CM%'				THEN 1
						WHEN 	ca.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	ca.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	ca.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'					THEN 1
						WHEN 	ca.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'Pool'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			)
		)		
--		AND ca.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Backlog History (count) 
-- Created on: 01/11/2018

DELETE FROM 	bi.CM_cases 		WHERE date = CURRENT_DATE AND kpi = 'Open Cases';
INSERT INTO 	bi.CM_cases
SELECT		
			TO_CHAR (CURRENT_DATE,'YYYY-WW') 		AS date_part
			, MIN 	(CURRENT_DATE::date) 			AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Open Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6	
			, MIN 	(cases.case_createddate::date)	AS sub_kpi_7
			, cases.Opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.cm_cases_today	cases

GROUP BY 	case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Cases created yesterday (used for the case inbound history tracking)
-- Created on: 07/11/2018

DROP 	TABLE IF EXISTS 	bi.CM_cases_created_yesterday;
CREATE 	TABLE 				bi.CM_cases_created_yesterday 	AS 

SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.createddate				case_createddate
			, ca.isclosed					case_isclosed
			, ca.ownerid					case_ownerid
			, u.name						case_owner
			, ca.origin						case_origin
			, ca.type						case_type
			, ca.reason						case_reason
			, ca.status						case_status
			, ca.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, ca.order__c					orderid
			, o.type						order_type
			, ca.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, ca.opportunity__c				opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE																						-- 1		
					
	--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	ca.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'				THEN 1
						WHEN 	ca.type 	= 'CLM'					THEN 1
						WHEN 	ca.type		= 'CM B2C'				THEN 1
						WHEN 	ca.type		= 'Sales'				THEN 1
						WHEN 	ca.type		= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	LIKE 'CM%'				THEN 1
						WHEN 	ca.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	ca.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	ca.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'					THEN 1
						WHEN 	ca.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'Pool'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			)
		)	
		
	AND ca.createddate::date = 'YESTERDAY'	
--		AND ca.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM NEW cases History (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Created Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((cases.case_createddate::date),'YYYY-WW') 		AS date_part
			, MIN 	(cases.case_createddate::date)					AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Created Cases' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, cases.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10			
			, COUNT(*)								AS value

FROM 		bi.cm_cases_created_yesterday	cases

GROUP BY 	cases.case_createddate
			, cases.case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Reopened Cases (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Reopened Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Reopened Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
		
					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2018-11-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Closed Cases (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Closed Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
		
					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Closed'
			AND hi.createddate::date >= '2018-11-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Type changes (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Cases Type changed';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((type_change.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(type_change.date::date)				AS date
			, CAST 	('-' 			AS varchar) 			AS locale
			, type_change.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)				AS type
			, CAST	('Cases Type changed' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)				AS sub_kpi_1
			, type_change.case_status						AS sub_kpi_2
			, type_change.case_reason						AS sub_kpi_3			
			, CASE 	WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NULL 		THEN 'unknown'
					WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN type_change.grand_total < 250 													THEN '<250€'
		  			WHEN type_change.grand_total >= 250 	AND type_change.grand_total < 500 			THEN '250€-500€'
		 			WHEN type_change.grand_total >= 500 	AND type_change.grand_total < 1000 			THEN '500€-1000€'
		  																								ELSE '>1000€'		END AS sub_kpi_4
			, type_change.opportunity						AS sub_kpi_5
			, type_change.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)				AS sub_kpi_7
			, type_change.opportunityid						AS sub_kpi_8
			, CAST 	('-' 			AS varchar)				AS sub_kpi_9
			, CAST 	('-' 			AS varchar)				AS sub_kpi_10			
			, COUNT(*)										AS value


FROM 		(


SELECT 		hi.createddate::date 							AS date
			, *
			-- , field
FROM salesforce.casehistory hi

LEFT JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
						
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE
		-- just CM B2B
		-- excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
		
					) 						ca 		ON hi.caseid			= ca.case_ID			
		
	-- reopened cases
WHERE				hi.field 			= 			'Type'
			AND 	hi.newvalue			= 			'CM B2B'
			AND		hi.oldvalue			NOT IN 		('B2B','KA')
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2018-11-01'
			AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
			AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
			
) AS type_change

GROUP BY 	type_change.date
			, type_change.case_origin
			, type_change.case_status	
			, type_change.case_reason
			, type_change.grand_total
			, type_change.opportunityid
			, type_change.opportunity	
			, type_change.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Inbound Calls (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Inbound Calls';
INSERT INTO 	bi.CM_cases

 SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-WW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 			AS varchar)							AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493030807264', '41435084849')
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Outbound Calls (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Outbound Calls';
INSERT INTO 	bi.CM_cases

SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-WW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 				AS varchar) 					AS locale
			, CAST 	('BAT CM'			AS varchar) 					AS origin 
			, CAST 	('B2B'				AS varchar)						AS type
			, CAST	('Outbound Calls'	AS varchar)						AS kpi
			, CAST 	('Count'			AS varchar)						AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 				AS varchar)						AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 				AS varchar)						AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Outbound'
			AND n.department__c LIKE 'CM'
			AND co.type__c NOT LIKE 'customer-b2c'
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Kpis Traffic Lights (count) 
-- Created on: 05/12/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Traffic Light';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR (traffic_lights.date,'YYYY-WW') AS date_part
			, MIN 	(traffic_lights.date::date) 	AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, CAST 	('-' 			AS varchar)		AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Traffic Light' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, traffic_lights.avg_traffic_light		AS sub_kpi_2
			, CASE 	WHEN traffic_lights.avg_traffic_light IS NULL						THEN 'red'
					WHEN traffic_lights.avg_traffic_light <= 1.8 						THEN 'red'
					WHEN traffic_lights.avg_traffic_light > 1.8 
						AND traffic_lights.avg_traffic_light <= 2.5 					THEN 'yellow'
					WHEN traffic_lights.avg_traffic_light >2.5							THEN 'green' 
																						ELSE '-' 			END AS sub_kpi_3																	
			, CASE 	WHEN traffic_lights.avg_traffic_light IS NULL						THEN 1
					WHEN traffic_lights.avg_traffic_light <= 1.8 						THEN 1
					WHEN traffic_lights.avg_traffic_light > 1.8 
						AND traffic_lights.avg_traffic_light <= 2.5 					THEN 2
					WHEN traffic_lights.avg_traffic_light >2.5							THEN 3 
																						ELSE 0 			END AS sub_kpi_4																					
			, CAST 	('-' 			AS varchar)		AS sub_kpi_5
			, CAST 	('-' 			AS varchar)		AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, CAST 	('-' 			AS varchar)		AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.opportunity_traffic_light_new	traffic_lights

GROUP BY 	traffic_lights.date
			, traffic_lights.avg_traffic_light
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Team and Agent Performance
-- Created on: 14/05/2019

DROP TABLE IF EXISTS bi.cm_performance;
CREATE TABLE bi.cm_performance AS 

SELECT *
		,CASE WHEN performance.inbound_calls > 0 		THEN  performance.inbound_calls >= (performance.target_inbound_calls * performance.FTE) END		AS performance_inbound_calls
		,CASE WHEN performance.cases_closed > 0 		THEN  performance.cases_closed >= (performance.target_cases_closed * performance.FTE) END		AS performance_cases_closed
		,CASE WHEN performance.svl_share_within_24h > 0 THEN  performance.svl_share_within_24h >= (performance.targert_svl * performance.FTE) END		AS performance_svl



FROM
(
SELECT 	
		 cases.date_part 				AS cw
		, MIN(cases.date) 				AS date
		, cases.sub_kpi_6	 			AS agent

-- FTE of every CM agent to calculate the share of the targets		
		, CASE 	WHEN cases.sub_kpi_6 = 'Katharina Kühner' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Katharina Kuehner' 	THEN 1 
				WHEN cases.sub_kpi_6 = 'Sercan Tas'	 		THEN 1
				WHEN cases.sub_kpi_6 = 'André Bauß' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Carmen Haas' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Julian Schäfer'		THEN 1
				WHEN cases.sub_kpi_6 = 'Susanne Marino' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Danny Taszarek'		THEN 1
				WHEN cases.sub_kpi_6 = 'Felix Liedtke'		THEN 1
				WHEN cases.sub_kpi_6 = 'Vivien Greve' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Daniela Kaim'		THEN 1
				WHEN cases.sub_kpi_6 = 'Lennart Bär'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Karla Sorgato'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'CM Support2'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Marleen Dreyer'		THEN 0.5
															ELSE 0 END 	AS FTE
		
		, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.value ELSE 0 END)	AS inbound_calls
		, CAST 	('20' 	AS numeric)					AS target_inbound_calls

		, SUM(CASE WHEN cases.kpi = 'Closed Cases' 	THEN cases.value ELSE 0 END) 	AS cases_closed
		, CAST 	('90'	AS numeric)					AS target_cases_closed

		, svl_final.svl_share_within_24h 			AS svl_share_within_24h
		, svl_final.cases							AS svl_casesclosed
		, svl_final.svl_cases_within_24h			AS svl_cases_within_24h
		, CAST 	('0.95'	AS numeric)					AS targert_svl


FROM bi.cm_cases 		cases

LEFT JOIN 
		(
			SELECT 	
					cw
					, MIN(svl_details.opened_date) 				AS MinDate
					, agent
					, COUNT(*)									AS cases
					, SUM(svl_details.svl_within_24h) 			AS svl_cases_within_24h
					, ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4)	AS svl_share_within_24h
			
			FROM
					(
					SELECT 
					--		*
					--		TO_CHAR(svl.date1_opened, 'YYYY-MM') 	AS month
							 TO_CHAR (svl.date1_opened, 'YYYY-IW')	AS cw
							, svl.date1_opened::date 				AS opened_date
							, svl.event_user						AS agent
							, svl.case_id 							AS case_id
							, svl.case_number						AS case_number
							, svl.type 								AS svl_type
							, svl.closed 							AS svl_has_closeddate
							, svl.case_isclosed						AS case_stil_closed
							, svl.case_type							AS case_type
							, svl.case_origin						AS case_origin
							, svl.case_reason						AS case_reason
							, svl.case_status						AS case_status	
							, svl.svl_customer						AS svl_customer
							
					-- IMPROVEMENT: maybe the following should be included in the SVL function!
							, CASE WHEN svl.svl_customer/60 <= 24 THEN 1 ELSE 0 END AS svl_within_24h
							
							, CASE WHEN svl.svl_customer = 0 		AND svl.type = 'Reopened Case' 					THEN 1 ELSE 0 END AS exclude_reopened_closed_due_mail 	-- exclude when 1
							, CASE WHEN svl.type = 'New Case' 		AND svl.case_origin = 'direct email outbound' 	THEN 1 ELSE 0 END AS exclude_direct_email_outbound	 	-- exclude when 1
							, CASE WHEN svl.type = 'Damage' 		OR svl.case_reason = '%Damage%'					THEN 1 ELSE 0 END AS exclude_damage_cases				-- exclude when 1
							, CASE WHEN svl.case_reason = 'Opportunity - Onboarding'								THEN 1 ELSE 0 END AS exclude_onboarding_cases			-- exclude when 1
							, CASE WHEN svl.case_status = 'Closed' AND svl.case_isclosed IS FALSE					THEN 1 ELSE 0 END AS exclude_created_within_clean_up	-- exclude when 1
					
					FROM 	bi.cm_cases_service_level 	svl
					WHERE 	NOT svl.type 	= '# Reopened'
							AND svl.date1_opened::date >= '2019-01-01'
					
					) AS svl_details
			
			 WHERE 
			 		svl_details.exclude_reopened_closed_due_mail 		= 0
					AND svl_details.exclude_direct_email_outbound 		= 0
					AND svl_details.exclude_damage_cases 				= 0
					AND svl_details.exclude_onboarding_cases 			= 0
					AND svl_details.exclude_created_within_clean_up 	= 0
			
			GROUP BY 	
						cw
						, agent
						
						
			) AS svl_final  ON (svl_final.cw = cases.date_part AND svl_final.agent = cases.sub_kpi_6)

WHERE cases.date 	> '2019-01-01'

GROUP BY 	cases.date_part 				
			, cases.sub_kpi_6	
			, svl_final.svl_share_within_24h
			, svl_final.cases							
			, svl_final.svl_cases_within_24h
) AS performance
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains the data concerning the profitability of the BAT cleaners on each opportunity served
-- Created on: 08/11/2018

DROP TABLE IF EXISTS bi.profitability_BAT_opps;
CREATE TABLE bi.profitability_BAT_opps AS 

SELECT

	TO_CHAR(o.issued__c, 'YYYY-MM') as year_month,
	MIN(o.issued__c) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	oo.delivery_area__c as city,
	'B2B' as type,
	'Revenue' as kpi,
	'BAT' as sub_kpi_1,
	oo.company_name as sub_kpi_2,
	oo.opportunity as sub_kpi_3,
	oo.professional__c,
	oo.pro_name,
	SUM(ooo.order_duration__c) as hours,
	SUM(ooo.order_duration__c)*12.9 as cost_cleaner,
	MAX(o.amount__c)/1.19 as revenue,
	(MAX(o.amount__c)/1.19)/SUM(ooo.order_duration__c) revenue_per_hour,
	CASE WHEN MAX(o.amount__c)/1.19 > 0 THEN
				(MAX(o.amount__c)/1.19 - SUM(ooo.order_duration__c)*12.9)/MAX(o.amount__c)/1.19
		  ELSE 0
		  END as gpm
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	bi.order_provider oo
	
ON

	o.opportunity__c = oo.opportunityid
	
LEFT JOIN

	salesforce.order ooo
	
ON

	o.opportunity__c = ooo.opportunityid
	AND TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR(ooo.effectivedate, 'YYYY-MM')
	
WHERE

	oo.provider = 'BAT'
	AND ooo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND ooo.effectivedate <= current_date
	AND ooo.type = 'cleaning-b2b'
	
GROUP BY

	year_month,
	LEFT(o.locale__c, 2),
	o.locale__c,
	oo.delivery_area__c,
	oo.company_name,
	oo.opportunity,
	oo.professional__c,
	oo.pro_name
	
ORDER BY

	year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains a reduced amount of information necessary to the calculation of the CVR from lead to opportunity (filter on working days possible)
-- Created on: 21/11/2018

DROP TABLE IF EXISTS bi.cvr_leads;
CREATE TABLE bi.cvr_leads AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as date,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.createddate,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN o.opportunity__c IS NULL THEN 'No' ELSE 'Yes' END as converted_in_opportunity,
	ooo.stagename as stage_opp,
	o.lost_reason__c,
	o.locale__C,
	LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
	o.contact_name__c,
	o.acquisition_channel__c,
	o.acquisition_tracking_id__c,
	lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as keyword,
	t1.spending,
	o.customer__c,
	o.stage__c,
	o.type__c,
	o.company_name__c,
	o.opportunity__c,
	o.ownerid,
	u.name as owner_name,
	o.total_calls__c,
	o.delivery_area__c,
	o.grand_total__c
	
FROM

	salesforce.likeli__c o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.opportunity__c = ooo.sfid

LEFT JOIN

	salesforce.user u
	
ON

	o.ownerid = u.sfid
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.start_date, 'YYYY-MM') as year_month,
		o.sub_kpi_3 as keyword,
		o.city,
		SUM(o.value) as spending
		
		
	
	FROM
	
		public.marketing_spending o
		
	GROUP BY
	
		year_month,
		city,
		keyword
		
	ORDER BY
	
		year_month desc,
		keyword) as t1

ON

	TO_CHAR(o.createddate, 'YYYY-MM') = t1.year_month
	AND lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) = t1.keyword
	AND LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) = t1.city
	
WHERE

	o.type__c = 'B2B'
	AND o.acquisition_channel__c IN ('inbound', 'web')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.test__c IS FALSE
	AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
	AND o.acquisition_channel__c NOT LIKE 'outbound'
	AND o.company_name__c NOT LIKE '%test%'
	AND o.company_name__c NOT LIKE '%bookatiger%'
	AND o.email__c NOT LIKE '%bookatiger%'
	AND o.name NOT LIKE '%test%'
	
ORDER BY

	date desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains the info concerning the opportunities created every working day
-- Created on: 22/11/2018

DROP TABLE IF EXISTS bi.cvr_opps;
CREATE TABLE bi.cvr_opps AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as createddate,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END as converted_in_opportunity,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c as closed_by_id,
	u.name as owner_name,
	o.delivery_area__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.sfid = ooo.sfid
	
LEFT JOIN

	salesforce.likeli__C oooo
	
ON

	o.sfid = oooo.opportunity__c
	
LEFT JOIN

	salesforce.user u
	
ON

	o.closed_by__c = u.sfid
	
WHERE

	o.test__c IS FALSE
	AND oooo.acquisition_channel__c IN ('web', 'inbound')
	AND LEFT(o.locale__c, 2) = 'de'
	
GROUP BY

	year_month,
	o.createddate,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	o.acquisition_channel__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c,
	u.name,
	o.delivery_area__c
	
ORDER BY

	TO_CHAR(o.createddate, 'YYYY-MM-DD') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains the data concerning the opportunities closed on every working day
-- Created on: 26/11/2018

DROP TABLE IF EXISTS bi.closed_opps;
CREATE TABLE bi.closed_opps AS 

SELECT

	TO_CHAR(o.closedate, 'YYYY-MM') as year_month,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END as converted_in_opportunity,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c as closed_by_id,
	u.name as owner_name,
	o.delivery_area__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.closedate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.sfid = ooo.sfid
	
LEFT JOIN

	salesforce.likeli__C oooo
	
ON

	o.sfid = oooo.opportunity__c
	
LEFT JOIN

	salesforce.user u
	
ON

	o.closed_by__c = u.sfid
	
WHERE

	o.test__c IS FALSE
	AND o.acquisition_channel__c IN ('web', 'inbound')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.closedate <= current_date
	AND o.stagename NOT LIKE '%LOST%'
	AND o.stagename IN ('WRITTEN CONFIRMATION', 'WON','PENDING')
	AND o.closed_by__c IS NOT NULL
	
GROUP BY

	year_month,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	o.acquisition_channel__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c,
	u.name,
	o.delivery_area__c
	
ORDER BY

	TO_CHAR(o.closedate, 'YYYY-MM-DD') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains gives insights on the efficiency of the keywords used in marketing (CVR%, spendings...)
-- Created on: 27/11/2018

DROP TABLE IF EXISTS bi.keywords_efficiency;
CREATE TABLE bi.keywords_efficiency AS 

SELECT

	o.year_month,
	MIN(o.date) as mindate,
	o.locale__c,
	o.city,
	o.keyword,
	COUNT(DISTINCT o.sfid) as leads_created,
	SUM(CASE WHEN o.converted_in_opportunity = 'Yes' THEN 1 ELSE 0 END) as opps_created,
	SUM(CASE WHEN o.stage__c IN ('ENDED') THEN 1 ELSE 0 END) as lead_ended,
	SUM(CASE WHEN o.stage__c NOT IN ('ENDED', 'WON', 'PENDING') THEN 1 ELSE 0 END) as pipeline,
	SUM(CASE WHEN o.stage_opp IN ('WON', 'PENDING') THEN 1 ELSE 0 END) as customer_converted,
	MAX(o.spending) as spending,
	SUM(o.grand_total__c) as grand_total

FROM

	bi.cvr_leads o
	
GROUP BY

	o.year_month,
	o.locale__c,
	o.city,
	o.keyword
	
ORDER BY
	
	year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table gives insights concerning the retention efficiency
-- Created on: 28/11/2018

DROP TABLE IF EXISTS bi.retention_efficiency;
CREATE TABLE bi.retention_efficiency AS 

SELECT
	
	DISTINCT o.opportunityid,
	MAX(o.createddate)::date as retention_date,
	oo.name,
	TRUNC(t1.potential::numeric, 2) as potential,
	CASE WHEN t1.potential < 250 THEN 'Very Small'
			  WHEN t1.potential >= 250 AND t1.potential < 500 THEN 'Small'
			  WHEN t1.potential >= 500 AND t1.potential < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END,
	oo.delivery_area__c,
	ooo.provider,
	ooo.company_name,
	ooo.pro_name,
	oo.status__c,
	oo.churn_reason__c,
	MAX(t2.resignation_date__c)::date as resignation_date,
	MAX(t2.confirmed_end__c)::date as confirmed_end

FROM

	salesforce.opportunityfieldhistory o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
LEFT JOIN

	bi.order_provider ooo
	
ON

	o.opportunityid = ooo.opportunityid
	
LEFT JOIN

	bi.potential_revenue_per_opp t1
	
ON 

	o.opportunityid = t1.opportunityid
	
LEFT JOIN

	salesforce.contract__c t2
	
ON

	o.opportunityid = t2.opportunity__c
	
-- LEFT JOIN

	-- salesforce.natterbox_call_reporting_object__c t3
	
-- ON

	-- o.opportunityid = t3.opportunity__c
	
WHERE

	o.newvalue = 'RETENTION'
	AND oo.test__c IS FALSE
	AND t2.service_type__c LIKE 'maintenance cleaning'
	-- AND t2.active__c IS TRUE
	-- AND t3.calldirection__c = 'Outbound'
	-- AND t3.callerlastname__c = 'Kharoo'
	-- AND t3.createddate >= o.createddate
	
GROUP BY

	o.opportunityid,
	oo.name,
	t1.potential,
	CASE WHEN t1.potential < 250 THEN 'Very Small'
			  WHEN t1.potential >= 250 AND t1.potential < 500 THEN 'Small'
			  WHEN t1.potential >= 500 AND t1.potential < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END,
	oo.delivery_area__c,
	ooo.provider,
	ooo.company_name,
	ooo.pro_name,
	oo.status__c,
	oo.churn_reason__c
	
ORDER BY

	retention_date desc;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: Opportunity invoice forecast details for the invoice check done by sales
-- Created on: 28/11/2018

DELETE 	FROM 		bi.opportunity_invoice_check 		WHERE TO_CHAR(((date - interval '1day')::date), 'YYYY-MM') = TO_CHAR(((CURRENT_DATE - interval '1day')::date), 'YYYY-MM');
INSERT INTO  		bi.opportunity_invoice_check   
  
  SELECT  CURRENT_DATE						AS date
  		  , TO_CHAR(((CURRENT_DATE - interval '1day')::date), 'YYYY-MM')  AS year_month
  		  , o.sfid                          AS OpportunityId
          , o.customer__c                   AS customerId
          , o.Name
          , o.StageName
          , o.status__c                     AS status
          , o.supplies__c                   AS supplies		
          , o.grand_total__c
          , o.CloseDate
          , ooo.first_order_date
          , (CASE   WHEN  EXTRACT (month  	FROM ooo.first_order_date::date) = EXTRACT (month   FROM (CURRENT_DATE - interval '1day')::date) 
                    AND  (EXTRACT (year  	FROM ooo.first_order_date::date) = EXTRACT (year   	FROM (CURRENT_DATE - interval '1day')::date)  ) THEN 1 ELSE 0 END)  AS FirstMonth
          , oo.first_order_date_monthly
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- orders
          , oo.orders AS orders
          , (CASE   WHEN oooo.orders_cancelled                > 0   THEN oooo.orders_cancelled                ELSE 0 END)             AS Orders_Cancelled
          , (CASE   WHEN ooooo.Orders_cancelled_customer      > 0   THEN ooooo.Orders_cancelled_customer      ELSE 0 END)             AS Orders_Cancelled_Customer
          , (CASE   WHEN oooo.Orders_Cancelled_Professional   > 0   THEN oooo.Orders_Cancelled_Professional   ELSE 0 END)             AS Orders_Cancelled_Professional
          , (CASE   WHEN oooooooo.Orders_Noshow_Professional  > 0   THEN oooooooo.Orders_Noshow_Professional  ELSE 0 END)             AS Orders_Noshow_Professional
          , (CASE   WHEN oooo.Orders_Cancelled_Mistake        > 0   THEN oooo.Orders_Cancelled_Mistake        ELSE 0 END)             AS Orders_Cancelled_Mistake
          , (CASE   WHEN ooooooooo.orders_bridging_day        > 0   THEN ooooooooo.orders_bridging_day        ELSE 0 END)             AS Orders_Bridging_Day
          , (CASE   WHEN oooo.Orders_Cancelled_Terminated     > 0   THEN oooo.Orders_Cancelled_Terminated     ELSE 0 END)             AS Orders_Cancelled_Terminated	
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- hours
          , o.hours_weekly__c                 AS weekly_hours
          , oo.executed_hours                 AS executed_hours
          , o.hours_weekly__c*oo.CW           AS max_monthly_hours
          , oooo.cancelled_Professional_hours
          , oooo.cancelled_Mistake_hours
          , oooo.cancelled_Terminated_hours
          , ooooo.cancelled_customer_hours    AS cancelled_customer_hours
          , ooooooo.holiday_mistake_hours     AS holiday_mistake_hours
          , oooooooo.Noshow_Professional_hours
          , (CASE   WHEN o.hours_weekly__c                    > 0   THEN (o.grand_total__c/o.hours_weekly__c/4.33) ELSE 0 END)        	AS PPH
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE 		
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE A - executed hours = max hours	
        , (CASE WHEN oo.executed_hours                    		= (o.hours_weekly__c*oo.CW)   					THEN 1 ELSE 0 END)      AS fine_A
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE B - executed hours + CANCELLED CUSTOMER = max hours	
  		, (CASE WHEN (oo.executed_hours + ooooo.cancelled_customer_hours) = (o.hours_weekly__c*oo.CW) 			THEN 1 ELSE 0 END) 		AS fine_B
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE C - first month	
  		, (CASE WHEN 	EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (month 	FROM ooo.first_order_date::date)
  				AND 	EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (year 	FROM ooo.first_order_date::date) THEN 1 ELSE 0 END) AS fine_C
  	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE D - executed hours < max hours AND no cancelled Orders		
  		, (CASE WHEN ((oo.executed_hours 						< (o.hours_weekly__c*oo.CW)) 
  				AND ((CASE WHEN oooo.orders_cancelled 			> 0 THEN oooo.orders_cancelled ELSE 0 END) = 0)) THEN 1 ELSE 0 END)		 AS fine_D
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE E - executed hours < max hours AND CANCELLED CUSTOMER		
  		, (CASE WHEN ((oo.executed_hours 						< (o.hours_weekly__c*oo.CW)) 
  				AND ((CASE WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS fine_E
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE G - executed hours = max hours AND Public Holiday = CANCELLED MISTAKE
  		, (CASE WHEN ((oo.executed_hours 						= (o.hours_weekly__c*oo.CW)) 
  				AND ((CASE WHEN ooooooo.orders_holiday_mistake  > 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS fine_G
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE H - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE
  		, (CASE WHEN (o.hours_weekly__c*oo.CW) 					= oo.executed_hours  
  		+ (CASE WHEN ooooooo.holiday_mistake_hours 				> 0 THEN ooooooo.holiday_mistake_hours 	ELSE 0 END) 
  				AND ((CASE WHEN ooooooo.orders_holiday_mistake 	> 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)THEN 1 ELSE 0 END) AS fine_H
  	  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW 		
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW first month	
  		, (CASE WHEN 	EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (month 	FROM ooo.first_order_date::date)
  				AND 	EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (year 	FROM ooo.first_order_date::date) THEN 1 ELSE 0 END) AS Review_firstmonth
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW h
  		, (CASE WHEN o.hours_weekly__c 							> 0 											THEN 0 ELSE 1 END) 		AS Review_h
	    	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW executed hours > max hours
  		, (CASE WHEN oo.executed_hours 							> (o.hours_weekly__c*oo.CW) 					THEN 1 ELSE 0 END) 		AS Review_extrabooking
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW public holidays
  		, (CASE WHEN (CASE WHEN oooooo.orders_holiday 			> 0 THEN oooooo.orders_holiday ELSE 0 END) > 0 	THEN 1 ELSE 0 END) 		AS Review_public_holidays
  	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW max hours > executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day		
  		, (CASE WHEN ((o.hours_weekly__c*oo.CW) 				> (oo.executed_hours  
  		+ (CASE WHEN ooooooo.holiday_mistake_hours 				> 0 THEN ooooooo.holiday_mistake_hours 	ELSE 0 END)
  		+ (CASE WHEN ooooooooo.holiday_bridging_day 			> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END))
  			AND (CASE WHEN ooooooooo.holiday_bridging_day 		> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END) > 0 )  THEN 1 ELSE 0 END) AS Review_Bridging_days_h
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - executed hours < max hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day		
  		, (CASE WHEN ((oo.executed_hours 						< (o.hours_weekly__c*oo.CW)) 
  			AND ((CASE WHEN ooooooo.orders_holiday_mistake 		> 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)
  			AND ((CASE WHEN ooooooooo.orders_bridging_day 		> 0 THEN ooooooooo.orders_bridging_day 	ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS REVIEW_Bridging_day
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day		
  		, (CASE WHEN ((o.hours_weekly__c*oo.CW) 				= (oo.executed_hours  
  		+ (CASE WHEN ooooooo.holiday_mistake_hours 				> 0 THEN ooooooo.holiday_mistake_hours 	ELSE 0 END)
  		+ (CASE WHEN ooooooooo.holiday_bridging_day 			> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END))
  			AND (CASE WHEN ooooooooo.holiday_bridging_day 		> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END) > 0)  THEN 1 ELSE 0 END) AS Review_incl_Bridging_day_h
  	
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  FROM 			salesforce.opportunity o 
  	
  	INNER JOIN 	(SELECT 	basic.opportunityid									AS opportunityid
							, basic.first_order_date_monthly					AS first_order_date_monthly
							, basic.orders										AS orders
							, basic.executed_hours								AS executed_hours
							, SUM (CASE WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday 	THEN 1 
																										ELSE 0 END) AS CW
				
				FROM 	 	(SELECT t1.opportunityid							AS opportunityid
					  				, COUNT 	(sfid) 							AS orders
						  			, SUM 		(order_duration__c) 			AS executed_hours
						  			, MIN 		(effectivedate::date) 			AS first_order_date_monthly
						  			
							FROM 	salesforce."order" 		t1			  		
							
							WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
									AND EXTRACT (month 	FROM effectivedate::date) 	= EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
									AND EXTRACT (year 	FROM effectivedate::date) 	= EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
								  						
							GROUP BY t1.opportunityid ) 	AS basic
				
				LEFT JOIN 	bi.working_days_monthly 		days 				ON (TO_CHAR(basic.first_order_date_monthly, 'YYYY-MM') = days.year_month)
				
				GROUP BY 	basic.opportunityid
							, basic.first_order_date_monthly
							, basic.orders
							, basic.executed_hours) 		AS oo				ON o.sfid = oo.opportunityid				
  
  
  	INNER JOIN 	bi.b2borders 								ooo 				ON o.sfid = ooo.opportunity_id
  	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED ORDERS without CANCELLED CUSTOMER	
  	LEFT JOIN 	(SELECT 	opportunityid
				  			, COUNT (sfid) 											AS Orders_cancelled
				  			, SUM(CASE WHEN Status LIKE 'CANCELLED PROFESSIONAL' 	THEN 1 					ELSE 0 END) AS Orders_Cancelled_Professional
				  			, SUM(CASE WHEN Status LIKE 'CANCELLED MISTAKE' 		THEN 1 					ELSE 0 END) AS Orders_Cancelled_Mistake
				  			, SUM(CASE WHEN Status LIKE 'CANCELLED TERMINATED' 		THEN 1 					ELSE 0 END) AS Orders_Cancelled_Terminated
							, SUM(CASE WHEN Status LIKE 'CANCELLED PROFESSIONAL' 	THEN order_duration__c 	ELSE 0 END) AS Cancelled_Professional_hours
							, SUM(CASE WHEN Status LIKE 'CANCELLED MISTAKE' 		THEN order_duration__c 	ELSE 0 END) AS Cancelled_Mistake_hours
							, SUM(CASE WHEN Status LIKE 'CANCELLED TERMINATED' 		THEN order_duration__c 	ELSE 0 END) AS Cancelled_Terminated_hours

  				FROM 		salesforce."order" 			t2
  				
  				WHERE 		status 		LIKE 		'%CANCELLED%'
  							AND status 	NOT LIKE 	'CANCELLED CUSTOMER'
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  							
  				GROUP BY 	opportunityid) 					AS oooo 			ON o.sfid = oooo.opportunityid
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- NOSHOW PROFESSIONAL	
  	LEFT JOIN 	(SELECT 	opportunityid
  							, SUM(CASE WHEN Status LIKE 'NOSHOW PROFESSIONAL' 		THEN 1 					ELSE 0 END) AS Orders_Noshow_Professional
							, SUM(CASE WHEN Status LIKE 'NOSHOW PROFESSIONAL' 		THEN order_duration__c 	ELSE 0 END) AS Noshow_Professional_hours
  			
  				FROM 		salesforce."order" 			t22
  				
  				WHERE 		status 			LIKE 		'NOSHOW PROFESSIONAL'
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  				
  				GROUP BY 	opportunityid) 					AS oooooooo 		ON o.sfid = oooooooo.opportunityid
  		
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED CUSTOMER ORDERS	
  	LEFT JOIN 	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS Orders_cancelled_customer
  							, SUM 	(order_duration__c) 							AS cancelled_customer_hours
  		
  				FROM 		salesforce."order" 			t3
  				
  				WHERE 		status 			LIKE 		'CANCELLED CUSTOMER'
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  				
  				GROUP BY  	opportunityid) 					AS ooooo 			ON o.sfid = ooooo.opportunityid	
   
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- 		
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- 
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS	w/o CANCELLED MISTAKE	
  	LEFT JOIN 	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS orders_holiday
  							, SUM 	(order_duration__c) 							AS holiday_hours
  							, MIN 	(effectivedate::date) 							AS first_order_date_monthly
  			
  				FROM 		salesforce."order" t4
  		
  				WHERE 	(	status 			IN (		'PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
  							OR (status 		LIKE 		'%CANCELLED%' 		
  								AND status 	NOT LIKE 	'CANCELLED MISTAKE'))
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND effectivedate::date IN ('2018-01-01', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03') -- should be replaced by an JOIN with the working day table
  				
  				GROUP BY 	opportunityid) 					AS oooooo			ON o.sfid = oooooo.opportunityid			
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAY is CANCELLED MISTAKE	
  	LEFT JOIN	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS orders_holiday_mistake
  							, SUM 	(order_duration__c) 							AS holiday_mistake_hours
  							, MIN 	(effectivedate::date) 							AS first_order_date_monthly
  			
  				FROM  		salesforce."order" 			t5
  		
  				WHERE 		status 			IN 			('CANCELLED MISTAKE')
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND effectivedate::date IN ('2018-01-01', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03') -- should be replaced by an JOIN with the working day table
  		GROUP BY
  			opportunityid) AS ooooooo	
  		ON o.sfid = ooooooo.opportunityid	
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- BRIDGING DAY is CANCELLED CUSTOMER	
  	LEFT JOIN	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS orders_bridging_day
  							, SUM 	(order_duration__c) 							AS holiday_bridging_day
  
  				FROM 		salesforce."order" t6
  		
  				WHERE 		status 			IN 			('CANCELLED CUSTOMER')
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (year FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year FROM (CURRENT_DATE - interval '1day')::date)
  							AND effectivedate::date IN ('2018-05-11')
  		
  				GROUP BY 	opportunityid) 					AS ooooooooo		ON o.sfid = ooooooooo.opportunityid
  		
  		
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  	
  WHERE 		o.grand_total__c 	IS NOT NULL
  				AND o.test__c 		IS FALSE
  				-- AND o.stagename IN ('WON')
  
  GROUP BY 		o.sfid
 				, o.customer__c
 				, o.Name
 				, o.StageName
 				, o.status__c
 				, o.supplies__c
				, o.grand_total__c
				, o.CloseDate
				, o.hours_weekly__c
  				, oo.first_order_date_monthly
 				, oo.orders
				, oo.executed_hours
        		, oo.CW
				, ooo.first_order_date
  				, oooo.cancelled_Professional_hours
  				, oooo.cancelled_Mistake_hours
  				, oooo.cancelled_Terminated_hours
  				, oooo.orders_cancelled
 				, oooo.Orders_Cancelled_Professional
 				, oooo.Orders_Cancelled_Mistake
 				, oooo.Orders_Cancelled_Terminated
 				, ooooo.Orders_cancelled_customer
 				, ooooo.cancelled_customer_hours
				, oooooo.orders_holiday
  				, ooooooo.orders_holiday_mistake
 				, ooooooo.holiday_mistake_hours
 				, oooooooo.Orders_Noshow_Professional
 				, oooooooo.Noshow_Professional_hours
 				, ooooooooo.orders_bridging_day
 				, ooooooooo.holiday_bridging_day
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: partner invoice forecast details for the invoice check done by PM
-- Created on: 30/11/2018

DROP TABLE IF EXISTS 	bi.partner_invoice_check;
CREATE TABLE 			bi.partner_invoice_check AS 

SELECT 		TO_CHAR(o.effectivedate, 'YYYY-MM') AS  year_month
			, CASE 	WHEN 	a.locale		LIKE ('ch-%') THEN 'ch'
					WHEN 	a.locale		LIKE ('de-%') THEN 'de'
					ELSE 'unknown' END AS 	locale 
			, a.partnername					partner
			, a.partnerid					partnerID
			, a.partnerpph					partnerpph
			, opp.name						opportunity
            , opp.sfid                      opportunityID
			, opp.monthly_partner_costs__c	ppp_monthly_partner_costs
			, opp.grand_total__c			opp_grand_total
			, invoiced.amount__c/1.19 as 	amount_invoiced
			--, o.effectivedate				order_effectivedate
			--, o.professional__c
			--, o.status					order_status
			--, o.order_duration__c	        order_duration
			--,*
			
			-- number of order
			, COUNT(*)								orders
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN 1 ELSE 0 END )			AS FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN 1 ELSE 0 END )				AS INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN 1 ELSE 0 END )		AS CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN 1 ELSE 0 END )		AS CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN 1 ELSE 0 END )		AS CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN 1 ELSE 0 END )			AS NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN 1 ELSE 0 END )		AS NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN 1 ELSE 0 END )		AS PENDING_ALLOCATION						
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN 1 ELSE 0 END )			AS PENDING_TO_START	
			
			-- hours (order duration)
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN o.order_duration__c ELSE 0 END )			AS h_FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN o.order_duration__c ELSE 0 END )				AS h_INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN o.order_duration__c ELSE 0 END )			AS h_NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN o.order_duration__c ELSE 0 END )		AS h_NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN o.order_duration__c ELSE 0 END )		AS h_PENDING_ALLOCATION	
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN o.order_duration__c ELSE 0 END )			AS h_PENDING_TO_START						
			
FROM		salesforce.order 		o
			
LEFT JOIN	salesforce.opportunity	opp 	ON o.opportunityid 	= opp.sfid
LEFT JOIN	salesforce.invoice__c	invoiced 	ON (o.opportunityid 	= invoiced.opportunity__c AND TO_CHAR(o.effectivedate, 'YYYY-MM') = TO_CHAR(invoiced.issued__c, 'YYYY-MM'))
LEFT JOIN 	(	SELECT 			pro.sfid			proid
								, pro.name			proname
								, pro.parentid		parentid
								, partner.sfid		partnerid
								, partner.name		partnername
								, pro.locale__c		locale
								, partner.pph__c	partnerpph
								
				FROM 			salesforce.account 	pro 
				LEFT JOIN 		salesforce.account 	partner 	ON 	pro.parentid = partner.sfid 
				WHERE 			pro.parentid		IS NOT NULL
			) 						AS a	ON o.professional__c		= a.proid


-- LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid


WHERE 		o.test__c 						= FALSE
			AND o.effectivedate::date 		>= '2018-01-01'
			--AND o.effectivedate::date 		<= '2018-10-31'
			AND o.type						IN ('cleaning-b2b','cleaning-window')
			AND (a.locale					LIKE ('de-%') OR a.locale  LIKE ('ch-%'))
			AND a.parentid 					IS NOT NULL


GROUP BY 	 a.partnername
			, a.locale
			, a.partnerid
			, a.partnerpph
			, opp.name
            , opp.sfid  
			, opp.monthly_partner_costs__c
			, opp.grand_total__c
			, invoiced.amount__c
			, TO_CHAR(o.effectivedate, 'YYYY-MM')
			--, o.status
;

-- Author: Christina Janson
-- Short Description: partner invoice forecast details for the invoice check done by PM Version 2
-- Created on: 30/11/2018
-- latest change: 23/04/2019

DROP TABLE IF EXISTS 	bi.partner_invoice_check_v2;
CREATE TABLE 			bi.partner_invoice_check_v2 AS 

SELECT 		TO_CHAR(o.effectivedate, 'YYYY-MM') AS  year_month
			, CASE 	WHEN 	a.locale		LIKE ('ch-%') THEN 'ch'
					WHEN 	a.locale		LIKE ('de-%') THEN 'de'
					ELSE 'unknown' END AS 	locale 
			, a.partnername					partner
			, a.partnerid					partnerID
			, a.partnerpph					partnerpph
			, COUNT(opp.sfid) OVER (PARTITION BY opp.sfid, TO_CHAR(o.effectivedate, 'YYYY-MM')) AS partner_per_opp
			, opp.name						opportunity
            , opp.sfid                      opportunityID
            , opp.status__c					opportunity_status
			, opp.monthly_partner_costs__c	ppp_monthly_partner_costs
			, opp.grand_total__c			opp_grand_total
			, invoice.net 					amount_invoice
			, opp.hours_weekly__c			opp_weekly_hours
			, opp.hours_weekly__c*4.33		opp_AVG_monthly_hours
			, oo.max_hours					opp_MAX_monthly_hours
			, MIN 		(o.effectivedate::date) 	AS first_order_date_monthly
			, first_month.first_order		first_month
			, first_month.last_order 		last_order
			, one_offs.acquistion_type		AS oneoff_acquistion_type
			, one_offs.package				AS oneoff_package
			
			-- number of order
			, COUNT(*)								orders
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN 1 ELSE 0 END )			AS FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN 1 ELSE 0 END )				AS INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN 1 ELSE 0 END )		AS CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	
							AND (o.quick_note__c ILIKE '%public_holiday%'
									OR o.error_note__c ILIKE '%public_holiday%')	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE_public_holidays

			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN 1 ELSE 0 END )		AS CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN 1 ELSE 0 END )		AS CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN 1 ELSE 0 END )			AS NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN 1 ELSE 0 END )		AS NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN 1 ELSE 0 END )		AS PENDING_ALLOCATION						
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN 1 ELSE 0 END )			AS PENDING_TO_START	
			
			-- hours (order duration)
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN o.order_duration__c ELSE 0 END )			AS h_FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN o.order_duration__c ELSE 0 END )				AS h_INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	
							AND (o.quick_note__c ILIKE '%public_holiday%'
									OR o.error_note__c ILIKE '%public_holiday%') THEN o.order_duration__c ELSE 0 END )	AS h_CANCELLED_MISTAKE_public_holidays
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN o.order_duration__c ELSE 0 END )			AS h_NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN o.order_duration__c ELSE 0 END )		AS h_NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN o.order_duration__c ELSE 0 END )		AS h_PENDING_ALLOCATION	
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN o.order_duration__c ELSE 0 END )			AS h_PENDING_TO_START	
			
			, extra_orders.extra_hours 																					AS h_extra_hours					
			
FROM		salesforce.order 		o
			
LEFT JOIN	salesforce.opportunity	opp 		ON o.opportunityid 	= opp.sfid

LEFT JOIN  
			( 	SELECT 	month
						, opp
						, SUM(amount__c)/1.19 AS net

				FROM
					(
						SELECT 	TO_CHAR(i.issued__c, 'YYYY-MM') AS month
								, i.opportunity__c AS opp
								, i.amount__c
		
						FROM 	salesforce.invoice__c i
						LEFT JOIN salesforce.invoice__c i2 ON i.sfid = i2.original_invoice__c
						WHERE 	i.original_invoice__c IS NULL 
								AND i.opportunity__c IS NOT NULL

					UNION

						SELECT 	TO_CHAR(i.issued__c, 'YYYY-MM') AS month
								, i2.opportunity__c AS opp
								, i2.amount__c
		
						FROM 	salesforce.invoice__c i
						LEFT JOIN salesforce.invoice__c i2 ON i.sfid = i2.original_invoice__c
						WHERE 	i.opportunity__c IS NOT NULL

					) As t1

				GROUP BY month
						, opp
			) as invoice 		ON o.opportunityid 	= invoice.opp AND TO_CHAR(o.effectivedate, 'YYYY-MM') = month

LEFT JOIN 	(	SELECT 			pro.sfid			proid
								, partner.sfid		partnerid
								, partner.name		partnername
								, pro.locale__c		locale
								, partner.pph__c	partnerpph
								
				FROM 			salesforce.account 	pro 
				LEFT JOIN 		salesforce.account 	partner 	ON 	pro.parentid = partner.sfid 
				WHERE 			pro.parentid		IS NOT NULL
			) 						AS a	ON o.professional__c		= a.proid
				
-- max monthly hours and first order date monthly

LEFT JOIN
				(SELECT 	max_hours.opportunityid
							, max_hours.year_month
							, MIN (max_hours.first_order_date_monthly) 	AS first_order_date_monthly
							, SUM (max_hours.max_h) 					AS max_hours
				FROM
		
						(SELECT basic.* 
						      , SUM (CASE WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday 	THEN 1 ELSE 0 END) 	AS CW_max
						      , basic.duration * CEIL(SUM (CASE WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday 	THEN 1.0 ELSE 0.0 END) /
						      	CASE 	WHEN basic.recurrency = 7 THEN 1.0
						      			WHEN basic.recurrency = 14 THEN 2.0
						      			WHEN basic.recurrency = 21 THEN 3.0
						      			WHEN basic.recurrency = 28 THEN 4.0 ELSE 1 END)	 AS max_h
		
						FROM 	(SELECT TO_CHAR (t1.order_date::date, 'YYYY-MM') 	AS year_month
										, t1.opportunityid							AS opportunityid
										, MIN(t1.order_date::date) 					AS first_order_date_monthly
										, t1.recurrency								AS recurrency
										, t1.order_duration							AS duration
						    			, ROW_NUMBER() OVER (PARTITION BY t1.opportunityid, TO_CHAR (order_date::date, 'YYYY-MM') ORDER BY order_date::date ) AS r
						   
		    					FROM 	
		    						(SELECT opportunityid
		    								, MIN (effectivedate::date)					AS order_date
		    								, SUM(order_duration__c) 					AS order_duration
		    								, recurrency__c								AS recurrency
		    								
		    								
		    						FROM 	salesforce."order" 	orders		  		
									
									WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
											AND orders.opportunityid IS NOT NULL   
											AND orders.recurrency__c <> '0'
										
									GROUP BY opportunityid
											, effectivedate::date
											, recurrency__c		 
									) t1
										
							  	GROUP BY t1.opportunityid, TO_CHAR (t1.order_date::date, 'YYYY-MM') , t1.order_duration, t1.order_date, t1.recurrency
							   	
							    ORDER BY t1.opportunityid, year_month 
							    
							    ) basic
		    
						LEFT JOIN	salesforce.opportunity			opp2				ON  basic.opportunityid 	= opp2.sfid 
						LEFT JOIN 	bi.working_days_monthly 		days 				ON (basic.year_month = days.year_month) 
		
						WHERE 	basic.r <= opp2.times_per_week__c
						 
						GROUP BY basic.opportunityid
								, basic.duration
								, basic.first_order_date_monthly
								, basic.year_month
								, basic.r
								, basic.recurrency
		 
						) as max_hours
		 
		 		GROUP BY max_hours.opportunityid
						, max_hours.year_month
				) AS oo																	ON (o.opportunityid = oo.opportunityid 	AND oo.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))				
				
-- first month / last month
LEFT JOIN 
			( 	SELECT 	o.opportunityid			opp
						, MIN 		(effectivedate::date) 			AS first_order
						, MAX 		(effectivedate::date) 			AS last_order
				FROM 	salesforce."order" 		o			  		
							
				WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
									AND o.opportunityid IS NOT NULL
				GROUP BY o.opportunityid
									
			) AS first_month ON first_month.opp = o.opportunityid

-- extra hours			
LEFT JOIN 
			( 	SELECT 	TO_CHAR (effectivedate::date, 'YYYY-MM') 	AS year_month
						, o.opportunityid							AS opportunityid
						, COUNT(*)									AS extra_orders
						, SUM( o.order_duration__c)					AS extra_hours

				FROM 	salesforce.order o

				WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
						AND o.opportunityid IS NOT NULL
					--	AND TO_CHAR (o.effectivedate::date, 'YYYY-MM') = '2019-01'
						AND o.recurrency__c = 0
						AND o.pph__c = '29.9'
													
				GROUP BY o.opportunityid
						, TO_CHAR (effectivedate::date, 'YYYY-MM') 
						
			) AS extra_orders 					ON (extra_orders.opportunityid 	= o.opportunityid 
													AND extra_orders.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))
													
-- one offs													
LEFT JOIN
			(	SELECT opportunity 									AS opportunityid
					, TO_CHAR 	(closed_date::date, 'YYYY-MM') 		AS year_month
					, acquisition_type								AS acquistion_type
					, package										AS package
					 
				FROM bi.b2b_additional_booking one_off
			
				WHERE type = 'one-off'
			) AS one_offs 						ON	(one_offs.opportunityid = o.opportunityid
													AND one_offs.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))											


WHERE 		o.test__c 						= FALSE
			AND o.effectivedate::date 		>= '2018-01-01'
			AND o.type						IN ('cleaning-b2b','cleaning-window')
			AND (a.locale					LIKE ('de-%') OR a.locale  LIKE ('ch-%'))
			AND a.partnerid 				IS NOT NULL

GROUP BY 	 a.partnername
			, a.locale
			, a.partnerid
			, a.partnerpph
			, opp.name
            , opp.sfid  
			, opp.monthly_partner_costs__c
			, opp.grand_total__c
			, TO_CHAR(o.effectivedate, 'YYYY-MM')
			, opp.hours_weekly__c
			, invoice.net
			, oo.max_hours	
			, first_month.first_order
			, opp.status__c	
			, first_month.last_order
			, extra_orders.extra_hours
			, one_offs.acquistion_type
			, one_offs.package				
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: all the cases created by the partners via the portal
-- Created on: 04/12/2018

DROP TABLE IF EXISTS bi.partner_portal_cases;
CREATE TABLE bi.partner_portal_cases AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	o.createddate,
	o.accountid,
	b.delivery_areas__c,
	a.company_name__c,
	CASE WHEN a.type__c = 'partner' THEN 'None' ELSE a.name END as cleaner,
	a.type__c,
	o.sfid as sfid_case,
	o.reason,
	o.origin,
	o.subject,
	o.description,	
	COUNT(DISTINCT o.sfid) as cases
	
FROM

	salesforce.case o
	
LEFT JOIN

	salesforce.account a
	
ON

	o.accountid = a.sfid

LEFT JOIN

	salesforce.account b
	
ON 

	a.company_name__c = b.name
	
WHERE

	(o.origin = 'Partner portal' OR o.origin LIKE 'Partner - portal%')
	AND LOWER(o.description) NOT LIKE '%test%'
	AND LOWER(o.description) NOT LIKE '%test%'
	AND LOWER(a.name) NOT LIKE '%test%'
	AND a.test__c IS FALSE
	AND b.test__c IS FALSE
	AND a.company_name__c NOT IN ('##')
	
GROUP BY

	TO_CHAR(o.createddate, 'YYYY-MM'),
	o.createddate,
	o.accountid,
	b.delivery_areas__c,
	a.name,
	a.company_name__c,
	a.type__c,
	o.sfid,
	o.reason,
	o.origin,
	o.description,
	o.subject
	
ORDER BY

	TO_CHAR(o.createddate, 'YYYY-MM') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: table containing the activity of the partners on the portal and the use of the application by the cleaners
-- Created on: 05/12/2018

DROP TABLE IF EXISTS bi.partner_portal_activity;
CREATE TABLE bi.partner_portal_activity AS 		

SELECT

	t4.company_name__c,
	t4.delivery_areas__c,
	t4.parentid,
	t4.sfid_cleaner,
	t4.last_order,
	t4.is_active,
	t4.cleaner_w_app,
	-- t4.*,
	MAX(t5.login) as last_login


FROM	
	
	(SELECT
	
		t2.*,
		CASE WHEN (t2.last_order > (current_date -91) AND t2.last_order <= current_date) THEN 'Yes' ELSE 'No' END as is_active,
		CASE WHEN t3.deviceid__c IS NOT NULL THEN 'Yes' ELSE 'No' END as cleaner_w_app,
		t3.last_login__c
	
	FROM
			
		(SELECT
		
			t1.company_name__c,
			t1.delivery_areas__c,
			t1.parentid,
			t1.sfid as sfid_cleaner,
			t1.last_order
		
		FROM	
			
			(SELECT	
				
				a.sfid,
				a.delivery_areas__c,
				a.billingcity,
				a.shippingcity,
				a.parentid,
				a.company_name__c,
				a.status__c,
				MAX(o.effectivedate) as last_order
			
			FROM
				
				salesforce.account a
				
			LEFT JOIN
			
				salesforce.account b
				
			ON 
			
				a.company_name__c = b.name
				
			LEFT JOIN
			
				salesforce.order o
				
			ON
			
				a.sfid = o.professional__c 
				
			WHERE
				
				LOWER(a.name) NOT LIKE '%test%'
				AND a.test__c IS FALSE
				AND b.test__c IS FALSE
				AND LEFT(a.locale__c, 2) = 'de'
				AND a.company_name__c NOT IN ('##')
				AND a.status__c IN ('BETA', 'ACTIVE', 'FROZEN')
				AND a.parentid IS NOT NULL
				AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
				AND o.effectivedate <= current_date
				
			GROUP BY
			
				a.sfid,
				a.delivery_areas__c,
				a.billingcity,
				a.shippingcity,
				a.parentid,
				a.company_name__c,
				a.status__c) as t1
				
		GROUP BY
		
			t1.company_name__c,
			t1.delivery_areas__c,
			t1.parentid,
			t1.last_order,
			t1.sfid) as t2
			
	LEFT JOIN
		
		(SELECT 
		
			parent.sfid, 
			parent.name,
			prof.sfid as sfid_cleaner,
			prof.deviceid__c,
			parent.last_login__c,
			COUNT(prof.sfid) as cleaners_w_app
		
		FROM 
		
			salesforce.account prof
		
		JOIN 
		
			salesforce.account parent 
			
		ON 
		
			prof.parentid = parent.sfid
			AND prof.status__c IN ('BETA', 'ACTIVE', 'FROZEN')
			AND prof.type__c LIKE '%cleaning-b2b%' 
			AND prof.locale__c LIKE 'de-%'
			AND prof.test__c = False 
			AND parent.test__c = False
			AND prof.deviceid__c IS NOT NULL
		
		GROUP BY 
		
			parent.sfid, 
			prof.deviceid__c,
			prof.sfid,
			parent.last_login__c,
			parent.name
			
		ORDER BY
		
			parent.name) as t3
			
	ON
	
		t2.company_name__c = t3.name
		AND t2.sfid_cleaner = t3.sfid_cleaner) as t4
		
LEFT JOIN

	(SELECT 

		name, 
		MAX(last_login__c) as login 
		
	FROM 
	
		salesforce.account 
	
	WHERE 
	
		last_login__c > DATE '2019-01-01' 
		
	GROUP BY 
	
		name 
		
	order by 
	
		login) as t5
		
ON

	t4.company_name__c = t5.name	
		
GROUP BY

	t4.company_name__c,
	t4.delivery_areas__c,
	t4.parentid,
	t4.sfid_cleaner,
	t4.last_order,
	t4.is_active,
	t4.cleaner_w_app;		

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Table containing the dates of the first and last orders of each opportunity
-- Created on: 11/12/2018


DROP TABLE IF EXISTS bi.first_last_orders;
CREATE TABLE bi.first_last_orders AS 

SELECT

	'1'::integer as key_link,
	t1.country,
	t1.locale__c,
	t1.delivery_area__c as polygon,
	t1.opportunityid,
	t1.date_first_order,
	TO_CHAR(t1.date_first_order, 'YYYY-MM') || '-01' as year_month_first_order,
	CASE WHEN t2.last_order IS NULL THEN '2099-12-31'::date ELSE t2.last_order END as date_last_order,
	CASE WHEN t2.last_order IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.last_order, 'YYYY-MM') || '-01' END as year_month_lat_order
	
FROM
	
	((SELECT

		LEFT(o.locale__c, 2) as country,
		o.locale__c,
		o.opportunityid,
		o.delivery_area__c,
		MIN(o.effectivedate) as date_first_order
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		
	GROUP BY
	
		o.opportunityid,
		o.locale__c,
		o.delivery_area__c,
		LEFT(o.locale__c, 2))) as t1
		
LEFT JOIN

	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(o.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as last_order
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(o.locale__c, 2),
		type_date,
		o.opportunityid) as t2
		
ON

	t1.opportunityid = t2.opportunityid;
	
--------------------------------------------
--------------------------------------------
--------------------------------------------

-- Author: Christina Janson
-- CM B2B Case details
-- Created on: 22/01/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_details;
CREATE 	TABLE 				bi.CM_cases_details 	AS 

SELECT 	cas.sfid						case_ID
		, cas.casenumber				case_number
		, cas.createddate				case_createddate
		, cas.origin 					case_origin
		, u.name						case_owner

FROM 		salesforce.case 		cas
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

WHERE
-- just CM B2B
--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
				WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Frank Wendt%' 		THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
						WHEN 	cas.type 	= 'CLM'					THEN 1
						WHEN 	cas.type	= 'CM B2C'				THEN 1
						WHEN 	cas.type	= 'Sales'				THEN 1
						WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
						WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
						WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
						WHEN 	cas.type 	= 'Pool'				THEN 1
						WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	
				(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
						WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			
		)		
;
-- Author: Christina Janson
-- CM B2B Service Level SVL calculation
-- Created on: 11/01/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, cahi.createdbyid																			AS event_by
			
FROM 		bi.CM_cases_details 		ca

-- date2													
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_id			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')

WHERE 		ca.case_createddate::date 					>= 		'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate	
			, cahi.createdbyid			

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value
			, cahi.createdbyid																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')	
 														AND cahi.createddate 	>= 	hi.createddate
 														
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, cahi.createdbyid

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, cahi.createdbyid																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')	
 														AND cahi.createddate 	>= 	hi.createddate

WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								= 		'CM B2B'
			AND	hi.oldvalue								NOT IN 	('B2B','KA')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2018-01-01'
			AND ca.case_origin							NOT LIKE '%B2B customer%' 	
			AND ca.case_origin	 						NOT LIKE 'CM - Team'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, cahi.createdbyid

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, hi.createdbyid																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
 														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, hi.createdbyid

ORDER BY 	case_id
			, date1_opened
			, date2_closed
			, event_by
;
-- Author: Christina Janson
-- CM B2B Case Service Level
-- Created on: 11/01/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level;
CREATE 	TABLE 				bi.CM_cases_service_level 	AS 

SELECT 	basis.*
-- ---------------------------------------------------------------------------------- difference: case opened / reopened / type change -> case closed		
-- -
-- - working h Mo-Fr 08:00:00 - 17:00:00 bi
-- - summertime timezone difference of 1 h

		, CASE 	WHEN basis.date1_opened::date 			IS NULL 							THEN 'rule 0.1' -- opened date missing
				WHEN basis.date2_closed::date 			IS NULL 							THEN 'rule 0.2' -- still open case
				WHEN basis.date1_opened::timestamp 	> basis.date2_closed::timestamp			THEN 'rule 1.0' -- open after closed
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN -- is same date
					 (CASE WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 2.1' 	 -- opened befor working day start
																							ELSE 'rule 2.2' END) -- 
				ELSE (CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 				THEN 'rule 3.1' -- opened after working day start
							WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') 	THEN 'rule 3.2' -- opened at the weekend
							WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 3.3' -- opened before working day start
																							ELSE 'rule 3.4' -- opened at a weekday within working hours
				END) END 																	AS rule_set 
				
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN 
		
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
					(CASE WHEN basis.date1_opened::time < TIME '07:00:00.0'					THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
						+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) END)
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
	-- minutes pased on the owner change date UNTIL END OF WORK
				-- owner changed after working day
						(CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 			THEN 0 
						-- owner change at the weekend
								WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') THEN 0
				ELSE
					-- owner changed before working day
						(CASE 	WHEN basis.date1_opened::time < TIME '07:00:00.0' 			THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60
								ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - basis.date1_opened::time) * 60 
										+ DATE_PART 	('minute', TIME '16:00:00.0' - basis.date1_opened::time) END) END)
					+
					-- minutes pased on the first contact date beginning at the morning working time
						(CASE 	WHEN basis.date2_closed::time < TIME '07:00:00.0' 			THEN 0 
								WHEN basis.date2_closed::time < TIME '16:00:00.0' 			THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60 END)
					+
					
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days and SUBTRACT 2 days 
	-- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) -2	) 			> 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END) END 																AS SVL_Minutes
			
-- ------------------------------------------------------------------------------------------------------------------------------------------------
-- - SVL Customer, dont care about working hours
-- -
				
-- if the case opened was after the case closed date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if case opened date and case closed date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					
				THEN 		DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
							+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) 
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
				
				-- date opened to EOD in minutes
				CASE WHEN date_part('dow', basis.date1_opened) IN ('6','0')	THEN 0 ELSE  
				(DATE_PART 	('hour', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened) * 60 
						+ DATE_PART 	('minute', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened)) END 
						
				-- + minutes on the case closed date		
				+	(DATE_PART 	('hour', basis.date2_closed) * 60  + DATE_PART 	('minute', basis.date2_closed))		
				+
					
	-- create a list of all date incl. case opened date and case closed date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) - 2)			> 0 
			
			-- in case the working days are > 0 -> workind days * 24 * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNT(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) - CASE WHEN date_part('dow', basis.date1_opened) IN ('6')	THEN 1 ELSE 2 END) * 24 * 60
			ELSE 0 END)  	END
																								AS SVL_Customer
			, cas.createddate				case_createddate
			, cas.isclosed					case_isclosed
			, cas.ownerid					case_ownerid
			, u.name						case_owner
			, cas.origin					case_origin
			, cas.type						case_type
			, cas.reason					case_reason
			, cas.status					case_status
			, cas.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, cas.order__c					orderid
			, o.type						order_type
			, cas.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, cas.opportunity__c			opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			, u2.name 						event_user
		
FROM bi.cm_cases_service_level_basis basis

LEFT JOIN 	salesforce.case 		cas 	ON basis.case_id		= cas.sfid
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 
LEFT JOIN 	salesforce.user 		u2 		ON basis.event_by 		= u2.sfid

-- WHERE basis.case_id = '5000J00001Q5PmtQAF'
-- LIMIT 100
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- CM B2C Case details
-- Created on: 18/03/2019

DROP 	TABLE IF EXISTS 	bi.CM_B2C_cases_details;
CREATE 	TABLE 				bi.CM_B2C_cases_details 	AS 

SELECT 	cas.sfid						case_ID
		, cas.casenumber				case_number
		, cas.createddate				case_createddate
		, cas.origin 					case_origin
		, u.name						case_owner

FROM 		salesforce.case 		cas
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

WHERE
-- just CM B2B
--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
				WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Frank Wendt%' 		THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- new case setup 
			( (CASE 	WHEN 	cas.type	= 'CM B2C'				THEN 1 ELSE 0 END) = 1									-- 11
			)
			
	-- old case setup
		OR	(	
				(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1 
						WHEN 	cas.origin 	LIKE 'B2C Customer%'	THEN 1 
						WHEN 	cas.origin 	LIKE 'Insurance'		THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	cas.type	= 'General'				THEN 1
						WHEN 	cas.type 	= 'Pool'				THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		)	
	AND cas.test__c IS NOT TRUE		
;


DROP 	TABLE IF EXISTS 	bi.CM_B2C_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_B2C_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			
FROM 		bi.CM_B2C_cases_details 		ca

-- date2													
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_id			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')

WHERE 		ca.case_createddate::date 					>= 		'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate				

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_B2C_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')	
 														AND cahi.createddate 	>= 	hi.createddate
 														
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_B2C_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')	
 														AND cahi.createddate 	>= 	hi.createddate

WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								= 		'CM B2C'
		--	AND	hi.oldvalue								NOT IN 	('PM')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2018-01-01'
			AND ca.case_origin							NOT LIKE 'CM%' 	
			AND ca.case_origin	 						NOT LIKE 'B2C Customer'
			AND ca.case_origin	 						NOT LIKE 'Insurance'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_B2C_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
 														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

ORDER BY 	case_id
			, date1_opened
			, date2_closed
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- PM B2B Case details
-- Created on: 18/03/2019

DROP 	TABLE IF EXISTS 	bi.PM_cases_details;
CREATE 	TABLE 				bi.PM_cases_details 	AS 

SELECT 	cas.sfid						case_ID
		, cas.casenumber				case_number
		, cas.createddate				case_createddate
		, cas.origin 					case_origin
		, u.name						case_owner

FROM 		salesforce.case 		cas
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

WHERE
-- just CM B2B
--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
			--	WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 		THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
				WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Wendt%' 				THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- new case setup 
			( (CASE 	WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 1									-- 11
			)
			
	-- old case setup
		OR	(	
				(CASE 	WHEN 	cas.origin 	LIKE 'TFS PM%'			THEN 1 
						WHEN 	cas.origin 	LIKE 'PM -Team%'		THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	cas.type	= 'General'				THEN 1
						WHEN 	cas.type 	= 'Pool'				THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		)	
	AND cas.test__c IS NOT TRUE		
;

DROP 	TABLE IF EXISTS 	bi.PM_cases_service_level_basis;
CREATE 	TABLE 				bi.PM_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			
FROM 		bi.PM_cases_details 		ca

-- date2													
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_id			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')

WHERE 		ca.case_createddate::date 					>= 		'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate				

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.PM_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')	
 														AND cahi.createddate 	>= 	hi.createddate
 														
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.PM_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('created','Status') 
														AND cahi.newvalue 		IN 	('Closed')	
 														AND cahi.createddate 	>= 	hi.createddate

WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								= 		'PM'
			AND	hi.oldvalue								NOT IN 	('PM')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2018-01-01'
			AND ca.case_origin							NOT LIKE 'TFS PM' 	
			AND ca.case_origin	 						NOT LIKE 'PM - Team'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.PM_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
 														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

ORDER BY 	case_id
			, date1_opened
			, date2_closed

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- commented out on 2019/04/02 by Christina -- fields replaced by new object (suggested_partner)
-- commented IN again by Sylvain afte modifying the query on 17/04/2019

-- Author: Sylvain Vanhuysse
-- Short Description: table containing the number of offers received and accepted for each partner
-- Created on: 21/01/2019

DROP TABLE IF EXISTS bi.offers_acceptance;
CREATE TABLE bi.offers_acceptance as

SELECT

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
	MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
	a.sfid,
	a.name,
	a.company_name__c,
	a.delivery_areas__c,
	a.type__c,
	a.status__c,
	a.role__c,
	COUNT(o.suggested_partner__c) as offers_received,
	SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted


FROM

	salesforce.account a


LEFT JOIN

	salesforce.partner_offer_partner__c o
		
ON

	a.sfid = o.suggested_partner__c 
	
WHERE

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
	AND a.test__c IS FALSE
	
GROUP BY

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
	a.sfid,
	a.name,
	a.company_name__c,
	a.delivery_areas__c,
	a.type__c,
	a.status__c,
	a.role__c

ORDER BY

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
	a.name asc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: table containing the number of offers received and accepted for each partner
-- Created on: 23/01/2019

DROP TABLE IF EXISTS bi.churn_forecast;
CREATE TABLE bi.churn_forecast as
	
SELECT

	TO_CHAR(t1.confirmed_end__c,'YYYY-MM') as year_month,
	MIN(t1.confirmed_end__c) as confirmed_end,
	CASE WHEN t1.status__c IN ('RESIGNED', 'CANCELLED') THEN 'CHURNED' ELSE t1.status__C END as status,
	t1.sfid,
	t1.name,
	-- 'Offboarding' as status,
	COUNT(DISTINCT t1.sfid) as number_opps,
	SUM(t1.avg_last3_months) as amount_lost
	-- SUM(CASE WHEN t1.grand_total__c > t1.avg_last3_months THEN t1.grand_total__c ELSE t1.avg_last3_months END) as amount_lost
	
FROM	
	
	(SELECT
	
		o.sfid,
		o.name,
		o.status__c,
		o.stagename,
		o.delivery_area__c,
		o.grand_total__c,
		oo.potential as avg_last3_months,
		ooo.confirmed_end__c
		
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	WHERE
	
		o.status__c IN ('OFFBOARDING', 'RETENTION', 'RESIGNED', 'CANCELLED')
		AND o.locale__c LIKE 'de%'
		AND ooo.confirmed_end__c IS NOT NULL
		AND o.test__c IS FALSE
		AND ooo.service_type__c LIKE 'maintenance cleaning'
		-- AND ooo.active__c IS TRUE
		-- We consider that the last valid contract can be RESIGNED, CANCELLED, SIGNED or ACCEPTED
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')) as t1
		
WHERE

	CASE WHEN t1.grand_total__c > t1.avg_last3_months THEN t1.grand_total__c ELSE t1.avg_last3_months END IS NOT NULL
	
	
GROUP BY 

	year_month,
	t1.sfid,
	t1.name,
	CASE WHEN t1.status__c IN ('RESIGNED', 'CANCELLED') THEN 'CHURNED' ELSE t1.status__C END;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: new analysis regarding the churn, new filters on the contracts, the providers, the category of revenue, the cohort... (CHURNED OPPS PART)
-- Created on: 05/02/2019

DROP TABLE IF EXISTS bi.waiting_time_offers_partners;
CREATE TABLE bi.waiting_time_offers_partners as


SELECT

	o.createddate,
	o.sfid as sfid_offer,
	o.status__c,
	o.opportunity__c,
	oo.name,
	oo.grand_total__c,
	CASE WHEN oo.grand_total__c < 250 THEN 'Very Small'
		  WHEN oo.grand_total__c >= 250 AND oo.grand_total__c < 500 THEN 'Small'
		  WHEN oo.grand_total__c >= 500 AND oo.grand_total__c < 1000 THEN 'Medium'
		  WHEN oo.grand_total__c IS NULL THEN 'Unknown'
		  ELSE 'Key Account'
		  END as type_customer,
	oo.hours_weekly__c,
	oo.times_per_week__c,
	oo.office_bathrooms__c,
	oo.office_employees__c,
	oo.office_rooms__c,
	oo.office_kitchens__c,
	oo.office_bathrooms__c + oo.office_rooms__c + oo.office_kitchens__c as all_rooms,
	
	CASE WHEN oo.office_size__c <= 100 THEN '<= 100'
	     WHEN (oo.office_size__c > 100  AND oo.office_size__c <= 200) THEN '100 < x <= 200'
		  WHEN (oo.office_size__c > 200  AND oo.office_size__c <= 300) THEN '200 < x <= 300'
		  WHEN (oo.office_size__c > 300  AND oo.office_size__c <= 400) THEN '300 < x <= 400'
		  ELSE '> 400'
		  END as office_size,
	
	CASE WHEN oo.additional_services__c IS NULL THEN 'No' ELSE 'Yes' END as additional_service,
	oo.recurrency__c,
	o.partner__c as partner_won,
	new_dates.new_date,
	send_dates.send_date,
	pending_dates.pending_date,

	accepted_dates.accepted_date,
	cancelled_dates.cancelled_date,
	CASE WHEN (o.status__c IN ('ACCEPTED', 'SEND', 'CANCELLED', 'PENDING') AND accepted_dates.accepted_date IS NOT NULL) THEN accepted_dates.accepted_date::timestamp - o.createddate::timestamp
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NOT NULL) THEN accepted_dates.accepted_date - o.createddate::timestamp
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NULL) THEN cancelled_dates.cancelled_date - o.createddate::timestamp
		  WHEN o.status__c IN ('NEW', 'SEND', 'PENDING') THEN current_timestamp - o.createddate::timestamp
		  ELSE current_timestamp - o.createddate::timestamp
		  END as waiting_time,
		  
	(CASE WHEN o.status__c = 'ACCEPTED' THEN DATE_PART('day', accepted_dates.accepted_date::timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', accepted_dates.accepted_date::timestamp - o.createddate::timestamp) + DATE_PART('minute', accepted_dates.accepted_date::timestamp - o.createddate::timestamp)/60
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NOT NULL) THEN DATE_PART('day', accepted_dates.accepted_date - o.createddate::timestamp)*24 + DATE_PART('hour', accepted_dates.accepted_date - o.createddate::timestamp) + DATE_PART('minute', accepted_dates.accepted_date - o.createddate::timestamp)/60
		 
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NULL) THEN DATE_PART('day', cancelled_dates.cancelled_date - o.createddate::timestamp)*24 + DATE_PART('hour', cancelled_dates.cancelled_date - o.createddate::timestamp) + DATE_PART('minute', cancelled_dates.cancelled_date - o.createddate::timestamp)/60
		  
		  WHEN o.status__c IN ('NEW', 'SEND', 'PENDING') THEN DATE_PART('day', current_timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', current_timestamp - o.createddate::timestamp) + DATE_PART('minute', current_timestamp - o.createddate::timestamp)/60
		  ELSE DATE_PART('day', current_timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', current_timestamp - o.createddate::timestamp) + DATE_PART('minute', current_timestamp - o.createddate::timestamp)/60
		  END)/24 as waiting_time_2		  

FROM

	salesforce.partner_offer__c o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunity__c = oo.sfid
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as new_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.oldvalue = 'NEW'
		
	GROUP BY
	
		o.parentid) as new_dates
		
ON

	o.sfid = new_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as send_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'SEND'
		
	GROUP BY
	
		o.parentid) as send_dates
		
ON

	o.sfid = send_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as pending_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'PENDING'
		
	GROUP BY
	
		o.parentid) as pending_dates
		
ON

	o.sfid = pending_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as accepted_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'ACCEPTED'
		
	GROUP BY
	
		o.parentid) as accepted_dates
		
ON

	o.sfid = accepted_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as cancelled_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'CANCELLED'
		
	GROUP BY
	
		o.parentid) as cancelled_dates
		
ON

	o.sfid = cancelled_dates.sfid_offer
	
LEFT JOIN

	salesforce.contract__c ct
	
ON

	oo.sfid = ct.opportunity__c
	
WHERE

	LOWER(oo.name) NOT LIKE '%test%'
	AND oo.test__c IS FALSE
	AND ct.status__c IN ('SIGNED', 'ACCEPTED', 'CANCELLED', 'RESIGNED')
	AND new_dates.new_date IS NOT NULL
	AND ct.service_type__c LIKE 'maintenance cleaning'
	-- AND ct.active__c IS TRUE
	

ORDER BY

	createddate desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Case Backlog today (used for the backlog history tracking)
-- Created on: 15/05/2019

-- DROP 	TABLE IF EXISTS 	bi.PM_cases_today;
-- CREATE 	TABLE 				bi.PM_cases_today 	AS 

-- SELECT 		ca.sfid							case_ID
-- 			, ca.casenumber					case_number
-- 			, ca.createddate				case_createddate
-- 			, ca.isclosed					case_isclosed
-- 			, ca.ownerid					case_ownerid
-- 			, u.name						case_owner
-- 			, ca.origin						case_origin
-- 			, ca.type						case_type
-- 			, ca.reason						case_reason
-- 			, ca.status						case_status
-- 			, ca.contactid					contactid
-- 			, co.name						contact_name
-- 			, co.type__c					contact_type
-- 			, co.company_name__c			contact_companyname
-- 			, ca.order__c					orderid
-- 			, o.type						order_type
-- 			, ca.accountid					professionalid
-- 			, a.name						professional
-- 			, a.company_name__c				professional_companyname
-- 			
-- -- 			, ca.opportunity__c				opportunityid
-- -- 			, opp.name						opportunity
-- 			, opp.grand_total__c			grand_total
-- 			
-- FROM 		salesforce.case 		ca
-- LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
-- LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
-- LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
-- LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
-- LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (3 OR 4)

-- WHERE		ca.isclosed			= FALSE																					-- 1	
-- 	AND   	ca.test__c 			= FALSE
					
	--	excluded case owner	
-- 	AND((CASE	WHEN 	u.name 		LIKE 	'%Standke%' 			THEN 1 
-- 																	ELSE 0 END) = 0 									-- 2				
-- 		)
-- 	AND (	ca.type = 'PM' 																								-- 3
-- 		OR (	ca.type = 'Pool' 	AND ca.origin = 'PM - Team'	)														-- 4
-- 		)	
--		AND ca.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 

-- ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Backlog History (count) 
-- Created on: 15/05/2019

DELETE FROM 	bi.PM_cases 		WHERE date = CURRENT_DATE AND kpi = 'Open Cases';
INSERT INTO 	bi.PM_cases
 
SELECT		
			TO_CHAR (CURRENT_DATE,'YYYY-WW') 		AS date_part
			, MIN 	(CURRENT_DATE::date) 			AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Open Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6	
			, MIN 	(cases.case_createddate::date)	AS sub_kpi_7
			, cases.Opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.PM_cases_today	cases

GROUP BY 	case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Cases created yesterday (used for the case inbound history tracking)
-- Created on: 15/05/2019

DROP 	TABLE IF EXISTS 	bi.PM_cases_created_yesterday;
CREATE 	TABLE 				bi.PM_cases_created_yesterday 	AS 

SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.createddate				case_createddate
			, ca.isclosed					case_isclosed
			, ca.ownerid					case_ownerid
			, u.name						case_owner
			, ca.origin						case_origin
			, ca.type						case_type
			, ca.reason						case_reason
			, ca.status						case_status
			, ca.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, ca.order__c					orderid
			, o.type						order_type
			, ca.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, ca.opportunity__c				opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 2 AND (3 OR 4) AND 5

WHERE	ca.test__c 			= FALSE
						
	--	excluded case owner	
	AND((CASE	WHEN 	u.name 		LIKE 	'%Standke%' 			THEN 1 
																	ELSE 0 END) = 0 									-- 2				
		)
	AND (	ca.type = 'PM' 																								-- 3
		OR (	ca.type = 'Pool' 	AND ca.origin = 'PM - Team'	)														-- 4
		)	
		
	AND ca.createddate::date = 'YESTERDAY'																				-- 5
--		AND ca.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM NEW cases History (count)
-- Created on: 15/05/2019

DELETE FROM 	bi.PM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Created Cases';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((cases.case_createddate::date),'YYYY-WW') 		AS date_part
			, MIN 	(cases.case_createddate::date)					AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Created Cases' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7 
			, cases.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10			
			, COUNT(*)								AS value

FROM 		bi.PM_cases_created_yesterday	cases

GROUP BY 	cases.case_createddate::date
			, cases.case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Reopened Cases (count)
-- Created on: 16/05/2019

DELETE FROM 	bi.PM_cases			WHERE kpi = 'Reopened Cases';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Reopened Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE	cas.test__c 			= FALSE
						
					--	excluded case owner	
					AND((CASE	WHEN 	u.name 		LIKE 	'%Standke%' 			THEN 1 
																					ELSE 0 END) = 0 									-- 2				
						)
					AND (	cas.type = 'PM' 																							-- 3
						OR (	cas.type = 'Pool' 	AND cas.origin = 'PM - Team'	)													-- 4
						)	
						
				--		AND cas.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 	
						
					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2019-01-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Closed Cases (count)
-- Created on: 16/05/2019

DELETE FROM 	bi.PM_cases			WHERE kpi = 'Closed Cases';
--INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE	cas.test__c 			= FALSE
						
					--	excluded case owner	
					AND((CASE	WHEN 	u.name 		LIKE 	'%Standke%' 			THEN 1 
																					ELSE 0 END) = 0 									-- 2				
						)
					AND (	cas.type = 'PM' 																							-- 3
						OR (	cas.type = 'Pool' 	AND cas.origin = 'PM - Team'	)													-- 4
						)	
						
				--		AND cas.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 

					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Closed'
			AND hi.createddate::date >= '2019-01-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Case Type changes (count)
-- Created on: 17/05/2018

DELETE FROM 	bi.PM_cases			WHERE kpi = 'Cases Type changed';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((type_change.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(type_change.date::date)				AS date
			, CAST 	('-' 			AS varchar) 			AS locale
			, type_change.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)				AS type
			, CAST	('Cases Type changed' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)				AS sub_kpi_1
			, type_change.case_status						AS sub_kpi_2
			, type_change.case_reason						AS sub_kpi_3			
			, CASE 	WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NULL 		THEN 'unknown'
					WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN type_change.grand_total < 250 													THEN '<250€'
		  			WHEN type_change.grand_total >= 250 	AND type_change.grand_total < 500 			THEN '250€-500€'
		 			WHEN type_change.grand_total >= 500 	AND type_change.grand_total < 1000 			THEN '500€-1000€'
		  																								ELSE '>1000€'		END AS sub_kpi_4
			, type_change.opportunity						AS sub_kpi_5
			, type_change.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)				AS sub_kpi_7
			, type_change.opportunityid						AS sub_kpi_8
			, CAST 	('-' 			AS varchar)				AS sub_kpi_9
			, CAST 	('-' 			AS varchar)				AS sub_kpi_10			
			, COUNT(*)										AS value


FROM 		(


SELECT 		hi.createddate::date 							AS date
			, *
			-- , field
FROM salesforce.casehistory hi

LEFT JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
						
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE	cas.test__c 			= FALSE
						
					--	excluded case owner	
					AND((CASE	WHEN 	u.name 		LIKE 	'%Standke%' 			THEN 1 
																					ELSE 0 END) = 0 									-- 2				
						)
					AND (	cas.type = 'PM' 																							-- 3
						OR (	cas.type = 'Pool' 	AND cas.origin = 'PM - Team'	)													-- 4
						)	
						
				--		AND cas.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 	
					) 						ca 		ON hi.caseid			= ca.case_ID			
		
	-- reopened cases
WHERE				hi.field 			= 			'Type'
			AND 	hi.newvalue			= 			'PM'
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2019-01-01'
			AND 	ca.case_origin	 	NOT LIKE 	'PM - Team'
			
) AS type_change

GROUP BY 	type_change.date
			, type_change.case_origin
			, type_change.case_status	
			, type_change.case_reason
			, type_change.grand_total
			, type_change.opportunityid
			, type_change.opportunity	
			, type_change.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM B2B Inbound Calls (count)
-- Created on: 17/05/2018

DELETE FROM 	bi.PM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Inbound Calls';
INSERT INTO 	bi.PM_cases

 SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-WW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 			AS varchar)							AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493070014488')
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM B2B Outbound Calls (count)
-- Created on: 17/05/2018

DELETE FROM 	bi.PM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Outbound Calls';
 INSERT INTO 	bi.PM_cases

SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-WW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 				AS varchar) 					AS locale
			, CAST 	('BAT CM'			AS varchar) 					AS origin 
			, CAST 	('B2B'				AS varchar)						AS type
			, CAST	('Outbound Calls'	AS varchar)						AS kpi
			, CAST 	('Count'			AS varchar)						AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 				AS varchar)						AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 				AS varchar)						AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Outbound'
			AND n.department__c LIKE 'PM'
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.scorecard_partners1;
CREATE TABLE bi.scorecard_partners1 as 

SELECT

	block3.sfid,
	block3.partner,
	block3.city as delivery_areas__c,
	block3.hours_city as total_hours_city,
	block3.hours_executed_this_month as hours_executed,	
	block3.threshold_hours,
	block3.hours_executed_this_month/block3.hours_city as share_city,
	CASE WHEN (block3.hours_executed_this_month/block3.hours_city) < (block3.threshold_hours - 0.05) THEN '> 5% under threshold'
	 	  WHEN (block3.hours_executed_this_month/block3.hours_city) > block3.threshold_hours THEN 'Above threshold'
	 	  ELSE '< 5% under threshold'
	     END as light_city_share,
		     
	block3.revenue_last_month,
	block3.revenue_last_month2,
	block3.score_revenue,
	CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation'
				  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue'
				  ELSE 'Decreasing revenue'
				  END as trend,
	block3.operational_costs_last_month,
	block3.gpm as gpm_last_month,
	
	CASE WHEN block3.gpm <= 0 THEN 0
		  WHEN block3.gpm > 0 AND block3.gpm <= 0.02 THEN 1
		  WHEN block3.gpm > 0.02 AND block3.gpm <= 0.04 THEN 2
		  WHEN block3.gpm > 0.04 AND block3.gpm <= 0.06 THEN 3
		  WHEN block3.gpm > 0.06 AND block3.gpm <= 0.08 THEN 4
		  WHEN block3.gpm > 0.08 AND block3.gpm <= 0.1 THEN 5
		  WHEN block3.gpm > 0.1 AND block3.gpm <= 0.12 THEN 6
		  WHEN block3.gpm > 0.12 AND block3.gpm <= 0.14 THEN 7
		  WHEN block3.gpm > 0.14 AND block3.gpm <= 0.16 THEN 8
		  WHEN block3.gpm > 0.16 AND block3.gpm <= 0.18 THEN 9
		  WHEN block3.gpm > 0.18 AND block3.gpm <= 0.20 THEN 10
		  WHEN block3.gpm > 0.2 AND block3.gpm <= 0.22 THEN 11
		  WHEN block3.gpm > 0.22 AND block3.gpm <= 0.24 THEN 12
		  WHEN block3.gpm > 0.24 AND block3.gpm <= 0.26 THEN 13
		  WHEN block3.gpm > 0.26 AND block3.gpm <= 0.28 THEN 14
		  WHEN block3.gpm > 0.28 THEN 15
		  ELSE 0
		  END as score_gpm,

	block3.active_pro_this_month,
	block3.active_pro_last_month,
	block3.active_pro_last_month2,
	CASE WHEN block3.active_pro_last_month = 0 THEN 0
	     WHEN (block3.active_pro_last_month > 0 AND block3.active_pro_last_month <= 2) THEN 1
	     WHEN (block3.active_pro_last_month > 2 AND block3.active_pro_last_month <= 4) THEN 2
	     WHEN (block3.active_pro_last_month > 4 AND block3.active_pro_last_month <= 6) THEN 3
	     WHEN (block3.active_pro_last_month > 6 AND block3.active_pro_last_month <= 8) THEN 4
	     WHEN block3.active_pro_last_month > 8  THEN 5
	     ELSE 0
	     END as score_active_pro,
	CASE WHEN block3.active_pro_this_month > 0 
		  THEN block3.hours_executed_this_month/block3.active_pro_this_month
		  ELSE 0
		  END as hours_per_cleaner,
		  
	CASE WHEN (CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) = 0 THEN 0
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 0 AND (CASE WHEN block3.active_pro_this_month > 0 
													THEN block3.hours_executed_this_month/block3.active_pro_this_month
													ELSE 0
													END) <= 4) THEN 1
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 4 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 8) THEN 2
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 8 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 12) THEN 3
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 12 AND (CASE WHEN block3.active_pro_this_month > 0 
													 THEN block3.hours_executed_this_month/block3.active_pro_this_month
													 ELSE 0
													 END) <= 16) THEN 4
	     WHEN (CASE WHEN block3.active_pro_this_month > 0 
		  				 THEN block3.hours_executed_this_month/block3.active_pro_this_month
						 ELSE 0
						 END) > 16 THEN 5
					    ELSE 100
					    END as score_hours_cleaners,
	     
	CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
		  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
		  ELSE 'Decreasing # workers'
		  END as trend_active_pros,
	block3.churn_this_month,
	block3.churn_last_month,
	block3.churn_last_month2,
	block3.avg_churn as weighted_churn,
	
	CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
	     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
	     ELSE 0 
	     END as score_weighted_churn,
	
	block3.churn_this_month€,
	block3.churn_last_month€,
	block3.churn_last_month2€,  
	block3.avg_churn€ as weighted_churn_€,   
	
	CASE WHEN (block3.avg_churn€ = 0 OR block3.avg_churn€ IS NULL) THEN 10
        WHEN (block3.avg_churn€ > 0 AND block3.avg_churn€ <= 50) THEN 9
        WHEN (block3.avg_churn€ > 50 AND block3.avg_churn€ <= 100) THEN 8
        WHEN (block3.avg_churn€ > 100 AND block3.avg_churn€ <= 150) THEN 7
        WHEN (block3.avg_churn€ > 150 AND block3.avg_churn€ <= 200) THEN 6
        WHEN (block3.avg_churn€ > 200 AND block3.avg_churn€ <= 250) THEN 5
        WHEN (block3.avg_churn€ > 250 AND block3.avg_churn€ <= 300) THEN 4
        WHEN (block3.avg_churn€ > 300 AND block3.avg_churn€ <= 350) THEN 3
        WHEN (block3.avg_churn€ > 350 AND block3.avg_churn€ <= 400) THEN 2
        WHEN (block3.avg_churn€ > 400 AND block3.avg_churn€ <= 450) THEN 1
        WHEN block3.avg_churn€ > 450 THEN 0
   	  ELSE 0 
        END as score_weighted_churn_€,
	
	block3.offers_received_this_month,
	block3.offers_received_last_month,
	block3.offers_received_last_month2,
	block3.offers_accepted_this_month,
	block3.offers_accepted_last_month,	     
	block3.offers_accepted_last_month2,		
	block3.avg_offers_accepted as pc_accepted_weighted,
	
	CASE WHEN block3.avg_offers_accepted = 0 THEN 0
		  WHEN block3.avg_offers_accepted > 0 AND block3.avg_offers_accepted <= 0.1 THEN 1
		  WHEN block3.avg_offers_accepted > 0.1 AND block3.avg_offers_accepted <= 0.2 THEN 2
		  WHEN block3.avg_offers_accepted > 0.2 AND block3.avg_offers_accepted <= 0.3 THEN 3
		  WHEN block3.avg_offers_accepted > 0.3 AND block3.avg_offers_accepted <= 0.4 THEN 4
		  WHEN block3.avg_offers_accepted > 0.4 AND block3.avg_offers_accepted <= 0.5 THEN 5
		  WHEN block3.avg_offers_accepted > 0.5 AND block3.avg_offers_accepted <= 0.6 THEN 6
		  WHEN block3.avg_offers_accepted > 0.6 AND block3.avg_offers_accepted <= 0.7 THEN 7
		  WHEN block3.avg_offers_accepted > 0.7 AND block3.avg_offers_accepted <= 0.8 THEN 8
		  WHEN block3.avg_offers_accepted > 0.8 AND block3.avg_offers_accepted <= 0.9 THEN 9
		  WHEN block3.avg_offers_accepted > 0.9 AND block3.avg_offers_accepted <= 1 THEN 10
		  ELSE 11
		  END as score_acceptance_weighted,
	
	block3.orders_submitted_this_month,
	block3.orders_submitted_last_month,
	block3.orders_submitted_last_month2,
	block3.orders_checked_this_month,
	block3.orders_checked_last_month,
	block3.orders_checked_last_month2,	
	block3.avg_orders_checked as pc_checked_weighted,
		  
	CASE WHEN block3.avg_orders_checked = 0 THEN 0
		  WHEN block3.avg_orders_checked > 0 AND block3.avg_orders_checked <= 0.1 THEN 1
		  WHEN block3.avg_orders_checked > 0.1 AND block3.avg_orders_checked <= 0.2 THEN 2
		  WHEN block3.avg_orders_checked > 0.2 AND block3.avg_orders_checked <= 0.3 THEN 3
		  WHEN block3.avg_orders_checked > 0.3 AND block3.avg_orders_checked <= 0.4 THEN 4
		  WHEN block3.avg_orders_checked > 0.4 AND block3.avg_orders_checked <= 0.5 THEN 5
		  WHEN block3.avg_orders_checked > 0.5 AND block3.avg_orders_checked <= 0.6 THEN 6
		  WHEN block3.avg_orders_checked > 0.6 AND block3.avg_orders_checked <= 0.7 THEN 7
		  WHEN block3.avg_orders_checked > 0.7 AND block3.avg_orders_checked <= 0.8 THEN 8
		  WHEN block3.avg_orders_checked > 0.8 AND block3.avg_orders_checked <= 0.9 THEN 9
		  WHEN block3.avg_orders_checked > 0.9 AND block3.avg_orders_checked <= 1 THEN 10
		  ELSE 11
		  END as score_checked_weighted,
	
	block3.cases_this_month,
	block3.cases_last_month,
	block3.cases_last_month2,
	block3.avg_number_cases as number_cases


FROM	
	
	(SELECT
	
		block2.sfid,
		block2.partner,
		block2.city,
		hours_city.hours_executed_this_month as hours_city,
		block2.hours_executed_this_month,	
		CASE WHEN block2.city = 'de-berlin' THEN 0.2
			  WHEN block2.city = 'de-cologne' THEN 0.35
			  WHEN block2.city = 'de-dusseldorf' THEN 0.4
			  WHEN block2.city = 'de-frankfurt' THEN 0.5
			  WHEN block2.city = 'de-hamburg' THEN 0.35
			  WHEN block2.city = 'de-hannover' THEN 0.6
			  WHEN block2.city = 'de-leipzig' THEN 0.5
			  WHEN block2.city = 'de-munich' THEN 0.35
			  WHEN block2.city = 'de-stuttgart' THEN 0.5
			  ELSE 1
			  END as threshold_hours,
		block2.revenue_last_month,
		block2.revenue_last_month2,
		CASE WHEN block2.revenue_last_month = 0 THEN 0
				WHEN (block2.revenue_last_month > 0 AND block2.revenue_last_month <= 500) THEN 1
				WHEN (block2.revenue_last_month > 500 AND block2.revenue_last_month <= 1000) THEN 2
				WHEN (block2.revenue_last_month > 1000 AND block2.revenue_last_month <= 1500) THEN 3
				WHEN (block2.revenue_last_month > 1500 AND block2.revenue_last_month <= 2000) THEN 4
				WHEN (block2.revenue_last_month > 2000 AND block2.revenue_last_month <= 2500) THEN 5
				WHEN (block2.revenue_last_month > 2500 AND block2.revenue_last_month <= 3000) THEN 6
				WHEN (block2.revenue_last_month > 3000 AND block2.revenue_last_month <= 3500) THEN 7
				WHEN (block2.revenue_last_month > 3500 AND block2.revenue_last_month <= 4000) THEN 8
				WHEN (block2.revenue_last_month > 4000 AND block2.revenue_last_month <= 4500) THEN 9
				WHEN (block2.revenue_last_month > 4500 AND block2.revenue_last_month <= 5000) THEN 10
				WHEN (block2.revenue_last_month > 5000 AND block2.revenue_last_month <= 5500) THEN 11
				WHEN (block2.revenue_last_month > 5500 AND block2.revenue_last_month <= 6000) THEN 12
				WHEN (block2.revenue_last_month > 6000 AND block2.revenue_last_month <= 6500) THEN 13
				WHEN (block2.revenue_last_month > 6500 AND block2.revenue_last_month <= 7000) THEN 14
				WHEN (block2.revenue_last_month > 7000 AND block2.revenue_last_month <= 7500) THEN 15
				WHEN (block2.revenue_last_month > 7500 AND block2.revenue_last_month <= 8000) THEN 16
				WHEN (block2.revenue_last_month > 8000 AND block2.revenue_last_month <= 8500) THEN 17
				WHEN (block2.revenue_last_month > 8500 AND block2.revenue_last_month <= 9000) THEN 18
				WHEN (block2.revenue_last_month > 9000 AND block2.revenue_last_month <= 9500) THEN 19
				WHEN (block2.revenue_last_month > 9500) THEN 20
				ELSE 0
				END as score_revenue,
		block2.operational_costs_last_month,
		CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END as gpm,
	
		block2.active_pro_this_month,
		block2.active_pro_last_month,
		block2.active_pro_last_month2,
		block2.churn_this_month,
		block2.churn_last_month,
		block2.churn_last_month2,
		0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
		block2.churn_this_month€,
		block2.churn_last_month€,
		block2.churn_last_month2€,  
		0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
		block2.offers_received_this_month,
		block2.offers_received_last_month,
		block2.offers_received_last_month2,
		block2.offers_accepted_this_month,
		block2.offers_accepted_last_month,	     
		block2.offers_accepted_last_month2,
			
		0.6*(CASE WHEN block2.offers_received_this_month > 0 
		     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.offers_received_last_month > 0
			  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.offers_received_last_month2 > 0
		     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
			  ELSE 0
			  END) as avg_offers_accepted,
		
		block2.orders_submitted_this_month,
		block2.orders_submitted_last_month,
		block2.orders_submitted_last_month2,
		block2.orders_checked_this_month,
		block2.orders_checked_last_month,
		block2.orders_checked_last_month2,
		
		0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
		     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.orders_submitted_last_month > 0
			  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
		     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
			  ELSE 0
			  END) as avg_orders_checked,
			  
		block2.cases_this_month,
		block2.cases_last_month,
		block2.cases_last_month2,
		0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
	
	FROM
			
		(SELECT
		
			block1.sfid,
			block1.partner,
			-- block1.year_month,
			block1.city,
			SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
			     ELSE block1.revenue_last_month
			     END) as revenue_last_month,
			SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
			     ELSE block1.revenue_last_month2
			     END) as revenue_last_month2,
			SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
			     ELSE block1.operational_costs_last_month
			     END) as operational_costs_last_month,
			SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
			     ELSE block1.hours_executed_this_month
			     END) as hours_executed_this_month,
			SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
			     ELSE block1.active_pro_this_month
			     END) as active_pro_this_month,
			SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
			     ELSE block1.active_pro_last_month
			     END) as active_pro_last_month,
			SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
			     ELSE block1.active_pro_last_month2
			     END) as active_pro_last_month2,
			SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
			     ELSE block1.churn_this_month
			     END) as churn_this_month,
			SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
			     ELSE block1.churn_last_month
			     END) as churn_last_month,
			SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
			     ELSE block1.churn_last_month2
			     END) as churn_last_month2,
			SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
			     ELSE block1.churn_this_month€
			     END) as churn_this_month€,
			SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
			     ELSE block1.churn_last_month€
			     END) as churn_last_month€,
			SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
			     ELSE block1.churn_last_month2€
			     END) as churn_last_month2€,     
			SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
			     ELSE block1.offers_received_this_month
			     END) as offers_received_this_month,
			SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
			     ELSE block1.offers_received_last_month
			     END) as offers_received_last_month,
			SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_received_last_month2
			     END) as offers_received_last_month2,
			SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_this_month
			     END) as offers_accepted_this_month,
			SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month
			     END) as offers_accepted_last_month,	     
			SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month2
			     END) as offers_accepted_last_month2,
			SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_this_month
			     END) as orders_submitted_this_month,
			SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month
			     END) as orders_submitted_last_month,
			SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month2
			     END) as orders_submitted_last_month2,
			SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
			     ELSE block1.orders_checked_this_month
			     END) as orders_checked_this_month,
			SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month
			     END) as orders_checked_last_month,
			SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month2
			     END) as orders_checked_last_month2,	
			SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
			     ELSE block1.cases_this_month
			     END) as cases_this_month,
			SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
			     ELSE block1.cases_last_month
			     END) as cases_last_month,
			SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
			     ELSE block1.cases_last_month2
			     END) as cases_last_month2
				
		FROM	
			
			(SELECT
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month,
				in_kpi_master.city,
				MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
				MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
				MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
				MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
				MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
				MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
				MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
				MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
				
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_this_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month2,
										
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_this_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month2€,
				MAX(offers.offers_received_this_month) as offers_received_this_month,
				MAX(offers.offers_received_last_month) as offers_received_last_month,
				MAX(offers.offers_received_last_month2) as offers_received_last_month2,
				MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
				MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
				MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
				
				MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
				MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
				MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
				MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
				MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
				MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
				
				MAX(number_cases.cases_this_month) as cases_this_month,
				MAX(number_cases.cases_last_month) as cases_last_month,
				MAX(number_cases.cases_last_month2) as cases_last_month2
									
			FROM
				
				
				((SELECT -- Simple list of all the accounts with the role master
				
					a.sfid,
					a.delivery_areas__c,
					a.name as partner
				
				FROM
				
					salesforce.account a 
						
				WHERE
				
					a.test__c IS FALSE
					AND a.role__c = 'master') as t0
					
				LEFT JOIN
					
					(SELECT 
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner2,
						o.city,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as revenue_last_month,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as revenue_last_month2,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
										(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
				                        THEN o.value
							               ELSE 0 END) 
										      END) as cost_supply_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as operational_costs_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as active_pro_last_month2									
														
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
											
					GROUP BY
					
						o.date_part,
						partner2,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner2 asc) as t1
						
				ON
				
					t0.partner = t1.partner2) as in_kpi_master
					
			LEFT JOIN
			
				bi.deep_churn as deep_churn1
				
			ON
			
				in_kpi_master.partner = deep_churn1.company_name
				AND in_kpi_master.city = deep_churn1.city
				AND in_kpi_master.year_month = deep_churn1."year_month"
				
			LEFT JOIN
			
				(SELECT
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name as company_name__c,
					offers1.delivery_area__c,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
					
				
				FROM
				
					(SELECT
									
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
						MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
						a.sfid,
						a.name,
						a.company_name__c,
						ooo.delivery_area__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c,
						COUNT(o.suggested_partner__c) as offers_received,
						SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
					
					
					FROM
					
						salesforce.account a
					
					
					LEFT JOIN
					
						salesforce.partner_offer_partner__c o
							
					ON
					
						a.sfid = o.suggested_partner__c 
						
					LEFT JOIN
					
						salesforce.partner_offer__c oo
						
					ON 
					
						o.partner_offer__c = oo.sfid
						
					LEFT JOIN
					
						salesforce.opportunity ooo
						
					ON
					
						oo.opportunity__c = ooo.sfid
						
					WHERE
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
						AND a.test__c IS FALSE
						
					GROUP BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
						a.sfid,
						a.name,
						a.company_name__c,
						ooo.delivery_area__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c
					
					ORDER BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
						a.name asc) as offers1
						
				GROUP BY
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name,
					offers1.delivery_area__c) as offers
					
			ON
			
				in_kpi_master.partner = offers.company_name__c
				AND in_kpi_master.city = offers.delivery_area__c
				AND in_kpi_master.year_month = offers.year_month_offer
				
			LEFT JOIN
			
				(SELECT
				
					orders_checked1.year_month,
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.polygon,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
				
				FROM	
					
					(SELECT
											
					  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
					  MIN(effectivedate::date) as mindate,
					  MAX(effectivedate::date) as maxdate,
					  t4.delivery_area__c as polygon,
					  t3.subcon as name_partner,
					  t3.sfid_partner,
					  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
					  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
					
					FROM
					
					  (SELECT
					  
					     t5.*,
					     t2.name as subcon,
					     t2.sfid as sfid_partner
					     
					   FROM
					   
					   	Salesforce.Account t5
					   
						JOIN
					      
							Salesforce.Account t2
					   
						ON
					   
							(t2.sfid = t5.parentid)
					     
						WHERE 
						
							t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
							-- AND t5.type__c = 'partner'
							AND LEFT(t5.locale__c, 2) = 'de'
							-- AND t5.role__c = 'master'
							AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
							AND t5.name NOT LIKE '%Handyman Kovacs%'
							AND t5.name NOT LIKE '%BAT Business Services GmbH%'
					   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
					   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
					      
					JOIN 
					
					  salesforce.order t4
					  
					ON
					
					  (t3.sfid = t4.professional__c)
					  
					WHERE
					
					  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
					  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
					  and LEFT(t4.locale__c,2) IN ('de')
					  and t4.effectivedate < current_date
					  AND t4.type = 'cleaning-b2b'
					  AND t4.test__c IS FALSE
					
					GROUP BY
					
					  year_month,
					  polygon,
					  t3.subcon,
					  t3.sfid_partner
					  
					  
					ORDER BY
						
					  year_month desc,
					  polygon,
					  t3.subcon) as orders_checked1
					  
				GROUP BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month,
					orders_checked1.polygon
					
				ORDER BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month desc) as orders_checked
					
			ON
			
				in_kpi_master.partner = orders_checked.name_partner
				AND in_kpi_master.city = orders_checked.polygon
				AND in_kpi_master.year_month = orders_checked.year_month
				
			LEFT JOIN
			
				(SELECT
				
					t3.year_month,
					t3.partnerid,
					t4.name as name_partner,
					t3.delivery_area__c,
					SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 32)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 62)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
					
				FROM
							
					(SELECT
					
						TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
						CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
						t2.name_partner,
						t2.parentid,
						COUNT(DISTINCT t2.sfid_case) as number_cases,
						t2.delivery_area__c
					
					FROM	
					
						(SELECT
						
							t1.date_case as date_case,
							CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
							t1.sfid_case,
							a.name as name_partner,
							a.parentid,
							t1.delivery_area__c
							
						FROM	
							
							(SELECT 
							
								c.createddate as date_case,
								c.CaseNumber, 
								c.sfid as sfid_case, 
								o.sfid, a.sfid, 
								o.delivery_area__c,
								c.origin, 
								c.*
							
							FROM 
							
								salesforce.case c
								
							LEFT JOIN 
							
								salesforce.opportunity o
							
							ON
								o.sfid = c.opportunity__c
								
							LEFT JOIN 
							
								salesforce.account a on a.sfid = c.accountid
								
							WHERE 
							
								c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
								AND COALESCE(c.Origin, '') != 'System - Notification'
								AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
								AND c.type != 'CM B2C'
								AND c.CreatedDate >= (current_date - 103)
								AND c.test__c IS FALSE
								AND a.test__c IS FALSE
								AND COALESCE(o.name, '') NOT LIKE '%test%' 
								-- SFID from ## account
								AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
								-- SFID from BAT Business Services GmbH account
								AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
								AND c.parentid IS NULL) as t1
								
						LEFT JOIN
						
							salesforce.account a
							
						ON
						
							(t1.accountid = a.sfid)
							-- OR t1.partner__c = a.sfid)
							
						WHERE 
						
							a.name IS NOT NULL) as t2
							
					GROUP BY 
					
						TO_CHAR(t2.date_case, 'YYYY-MM'),
						t2.partner,
						t2.name_partner,
						t2.parentid,
						t2.delivery_area__c
						
					ORDER BY
					
						t2.name_partner,
						TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
						
				LEFT JOIN
				
					salesforce.account t4
					
				ON
				
					t3.partnerid = t4.sfid
					
				GROUP BY
				
					t3.year_month,
					t3.partnerid,
					t3.delivery_area__c,
					t4.name) as number_cases
					
			ON
			
				in_kpi_master.partner = number_cases.name_partner
				AND in_kpi_master.city = number_cases.delivery_area__c
				AND in_kpi_master.year_month = number_cases.year_month	
			
			GROUP BY
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month,
				in_kpi_master.city
				
			ORDER BY
			
				in_kpi_master.sfid,
				in_kpi_master.year_month desc) as block1
	
		WHERE
	
			block1.year_month IS NOT NULL
					
		GROUP BY
		
			block1.sfid,
			block1.partner,
			block1.city) as block2
			
	LEFT JOIN
	
		(SELECT 
	
			o.city,
			SUM(CASE WHEN 
				         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
				               THEN o.value
							      ELSE 0 END) IS NULL THEN 0
						ELSE 
						   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
			                  THEN o.value
						         ELSE 0 END) 
									END) as hours_executed_this_month								
											
		FROM
		
			bi.kpi_master o
			
		WHERE
		
			(o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
		
		GROUP BY
		
			o.city) as hours_city
			
	ON
	
		block2.city = hours_city.city
	
	WHERE
	
		block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3;				

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.scorecard_partners2;
CREATE TABLE bi.scorecard_partners2 as 


SELECT

	block3.sfid,
	block3.partner,
	block3.hours_executed_this_month as hours_executed,	

	block3.revenue_last_month,
	block3.revenue_last_month2,
	block3.score_revenue,
	CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation'
				  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue'
				  ELSE 'Decreasing revenue'
				  END as trend,
	block3.operational_costs_last_month,
	block3.gpm as gpm_last_month,
	
	CASE WHEN block3.gpm <= 0 THEN 0
		  WHEN block3.gpm > 0 AND block3.gpm <= 0.02 THEN 1
		  WHEN block3.gpm > 0.02 AND block3.gpm <= 0.04 THEN 2
		  WHEN block3.gpm > 0.04 AND block3.gpm <= 0.06 THEN 3
		  WHEN block3.gpm > 0.06 AND block3.gpm <= 0.08 THEN 4
		  WHEN block3.gpm > 0.08 AND block3.gpm <= 0.1 THEN 5
		  WHEN block3.gpm > 0.1 AND block3.gpm <= 0.12 THEN 6
		  WHEN block3.gpm > 0.12 AND block3.gpm <= 0.14 THEN 7
		  WHEN block3.gpm > 0.14 AND block3.gpm <= 0.16 THEN 8
		  WHEN block3.gpm > 0.16 AND block3.gpm <= 0.18 THEN 9
		  WHEN block3.gpm > 0.18 AND block3.gpm <= 0.20 THEN 10
		  WHEN block3.gpm > 0.2 AND block3.gpm <= 0.22 THEN 11
		  WHEN block3.gpm > 0.22 AND block3.gpm <= 0.24 THEN 12
		  WHEN block3.gpm > 0.24 AND block3.gpm <= 0.26 THEN 13
		  WHEN block3.gpm > 0.26 AND block3.gpm <= 0.28 THEN 14
		  WHEN block3.gpm > 0.28 THEN 15
		  ELSE 0
		  END as score_gpm,

	block3.active_pro_this_month,
	block3.active_pro_last_month,
	block3.active_pro_last_month2,
	CASE WHEN block3.active_pro_last_month = 0 THEN 0
	     WHEN (block3.active_pro_last_month > 0 AND block3.active_pro_last_month <= 2) THEN 1
	     WHEN (block3.active_pro_last_month > 2 AND block3.active_pro_last_month <= 4) THEN 2
	     WHEN (block3.active_pro_last_month > 4 AND block3.active_pro_last_month <= 6) THEN 3
	     WHEN (block3.active_pro_last_month > 6 AND block3.active_pro_last_month <= 8) THEN 4
	     WHEN block3.active_pro_last_month > 8  THEN 5
	     ELSE 0
	     END as score_active_pro,
	CASE WHEN block3.active_pro_this_month > 0 
		  THEN block3.hours_executed_this_month/block3.active_pro_this_month
		  ELSE 0
		  END as hours_per_cleaner,
		  
	CASE WHEN (CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) = 0 THEN 0
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 0 AND (CASE WHEN block3.active_pro_this_month > 0 
													THEN block3.hours_executed_this_month/block3.active_pro_this_month
													ELSE 0
													END) <= 4) THEN 1
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 4 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 8) THEN 2
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 8 AND (CASE WHEN block3.active_pro_this_month > 0 
												   THEN block3.hours_executed_this_month/block3.active_pro_this_month
												   ELSE 0
												   END) <= 12) THEN 3
	     WHEN ((CASE WHEN block3.active_pro_this_month > 0 
						  THEN block3.hours_executed_this_month/block3.active_pro_this_month
						  ELSE 0
						  END) > 12 AND (CASE WHEN block3.active_pro_this_month > 0 
													 THEN block3.hours_executed_this_month/block3.active_pro_this_month
													 ELSE 0
													 END) <= 16) THEN 4
	     WHEN (CASE WHEN block3.active_pro_this_month > 0 
		  				 THEN block3.hours_executed_this_month/block3.active_pro_this_month
						 ELSE 0
						 END) > 16 THEN 5
					    ELSE 100
					    END as score_hours_cleaners,
	     
	CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
		  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
		  ELSE 'Decreasing # workers'
		  END as trend_active_pros,
	block3.churn_this_month,
	block3.churn_last_month,
	block3.churn_last_month2,
	block3.avg_churn as weighted_churn,
	
	CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
	     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
	     ELSE 0 
	     END as score_weighted_churn,
	
	block3.churn_this_month€,
	block3.churn_last_month€,
	block3.churn_last_month2€,  
	block3.avg_churn€ as weighted_churn_€,   
	
	CASE WHEN (block3.avg_churn€ = 0 OR block3.avg_churn€ IS NULL) THEN 10
        WHEN (block3.avg_churn€ > 0 AND block3.avg_churn€ <= 50) THEN 9
        WHEN (block3.avg_churn€ > 50 AND block3.avg_churn€ <= 100) THEN 8
        WHEN (block3.avg_churn€ > 100 AND block3.avg_churn€ <= 150) THEN 7
        WHEN (block3.avg_churn€ > 150 AND block3.avg_churn€ <= 200) THEN 6
        WHEN (block3.avg_churn€ > 200 AND block3.avg_churn€ <= 250) THEN 5
        WHEN (block3.avg_churn€ > 250 AND block3.avg_churn€ <= 300) THEN 4
        WHEN (block3.avg_churn€ > 300 AND block3.avg_churn€ <= 350) THEN 3
        WHEN (block3.avg_churn€ > 350 AND block3.avg_churn€ <= 400) THEN 2
        WHEN (block3.avg_churn€ > 400 AND block3.avg_churn€ <= 450) THEN 1
        WHEN block3.avg_churn€ > 450 THEN 0
   	  ELSE 0 
        END as score_weighted_churn_€,
	
	block3.offers_received_this_month,
	block3.offers_received_last_month,
	block3.offers_received_last_month2,
	block3.offers_accepted_this_month,
	block3.offers_accepted_last_month,	     
	block3.offers_accepted_last_month2,		
	block3.avg_offers_accepted as pc_accepted_weighted,
	
	CASE WHEN block3.avg_offers_accepted = 0 THEN 0
		  WHEN block3.avg_offers_accepted > 0 AND block3.avg_offers_accepted <= 0.1 THEN 1
		  WHEN block3.avg_offers_accepted > 0.1 AND block3.avg_offers_accepted <= 0.2 THEN 2
		  WHEN block3.avg_offers_accepted > 0.2 AND block3.avg_offers_accepted <= 0.3 THEN 3
		  WHEN block3.avg_offers_accepted > 0.3 AND block3.avg_offers_accepted <= 0.4 THEN 4
		  WHEN block3.avg_offers_accepted > 0.4 AND block3.avg_offers_accepted <= 0.5 THEN 5
		  WHEN block3.avg_offers_accepted > 0.5 AND block3.avg_offers_accepted <= 0.6 THEN 6
		  WHEN block3.avg_offers_accepted > 0.6 AND block3.avg_offers_accepted <= 0.7 THEN 7
		  WHEN block3.avg_offers_accepted > 0.7 AND block3.avg_offers_accepted <= 0.8 THEN 8
		  WHEN block3.avg_offers_accepted > 0.8 AND block3.avg_offers_accepted <= 0.9 THEN 9
		  WHEN block3.avg_offers_accepted > 0.9 AND block3.avg_offers_accepted <= 1 THEN 10
		  ELSE 11
		  END as score_acceptance_weighted,
	
	block3.orders_submitted_this_month,
	block3.orders_submitted_last_month,
	block3.orders_submitted_last_month2,
	block3.orders_checked_this_month,
	block3.orders_checked_last_month,
	block3.orders_checked_last_month2,	
	block3.avg_orders_checked as pc_checked_weighted,
		  
	CASE WHEN block3.avg_orders_checked = 0 THEN 0
		  WHEN block3.avg_orders_checked > 0 AND block3.avg_orders_checked <= 0.1 THEN 1
		  WHEN block3.avg_orders_checked > 0.1 AND block3.avg_orders_checked <= 0.2 THEN 2
		  WHEN block3.avg_orders_checked > 0.2 AND block3.avg_orders_checked <= 0.3 THEN 3
		  WHEN block3.avg_orders_checked > 0.3 AND block3.avg_orders_checked <= 0.4 THEN 4
		  WHEN block3.avg_orders_checked > 0.4 AND block3.avg_orders_checked <= 0.5 THEN 5
		  WHEN block3.avg_orders_checked > 0.5 AND block3.avg_orders_checked <= 0.6 THEN 6
		  WHEN block3.avg_orders_checked > 0.6 AND block3.avg_orders_checked <= 0.7 THEN 7
		  WHEN block3.avg_orders_checked > 0.7 AND block3.avg_orders_checked <= 0.8 THEN 8
		  WHEN block3.avg_orders_checked > 0.8 AND block3.avg_orders_checked <= 0.9 THEN 9
		  WHEN block3.avg_orders_checked > 0.9 AND block3.avg_orders_checked <= 1 THEN 10
		  ELSE 11
		  END as score_checked_weighted,
	
	block3.cases_this_month,
	block3.cases_last_month,
	block3.cases_last_month2,
	block3.avg_number_cases as number_cases


FROM	
	
	(SELECT
	
		block2.sfid,
		block2.partner,
		block2.hours_executed_this_month,	
		block2.revenue_last_month,
		block2.revenue_last_month2,
		CASE WHEN block2.revenue_last_month = 0 THEN 0
				WHEN (block2.revenue_last_month > 0 AND block2.revenue_last_month <= 500) THEN 1
				WHEN (block2.revenue_last_month > 500 AND block2.revenue_last_month <= 1000) THEN 2
				WHEN (block2.revenue_last_month > 1000 AND block2.revenue_last_month <= 1500) THEN 3
				WHEN (block2.revenue_last_month > 1500 AND block2.revenue_last_month <= 2000) THEN 4
				WHEN (block2.revenue_last_month > 2000 AND block2.revenue_last_month <= 2500) THEN 5
				WHEN (block2.revenue_last_month > 2500 AND block2.revenue_last_month <= 3000) THEN 6
				WHEN (block2.revenue_last_month > 3000 AND block2.revenue_last_month <= 3500) THEN 7
				WHEN (block2.revenue_last_month > 3500 AND block2.revenue_last_month <= 4000) THEN 8
				WHEN (block2.revenue_last_month > 4000 AND block2.revenue_last_month <= 4500) THEN 9
				WHEN (block2.revenue_last_month > 4500 AND block2.revenue_last_month <= 5000) THEN 10
				WHEN (block2.revenue_last_month > 5000 AND block2.revenue_last_month <= 5500) THEN 11
				WHEN (block2.revenue_last_month > 5500 AND block2.revenue_last_month <= 6000) THEN 12
				WHEN (block2.revenue_last_month > 6000 AND block2.revenue_last_month <= 6500) THEN 13
				WHEN (block2.revenue_last_month > 6500 AND block2.revenue_last_month <= 7000) THEN 14
				WHEN (block2.revenue_last_month > 7000 AND block2.revenue_last_month <= 7500) THEN 15
				WHEN (block2.revenue_last_month > 7500 AND block2.revenue_last_month <= 8000) THEN 16
				WHEN (block2.revenue_last_month > 8000 AND block2.revenue_last_month <= 8500) THEN 17
				WHEN (block2.revenue_last_month > 8500 AND block2.revenue_last_month <= 9000) THEN 18
				WHEN (block2.revenue_last_month > 9000 AND block2.revenue_last_month <= 9500) THEN 19
				WHEN (block2.revenue_last_month > 9500) THEN 20
				ELSE 0
				END as score_revenue,
		block2.operational_costs_last_month,
		CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END as gpm,
	
		block2.active_pro_this_month,
		block2.active_pro_last_month,
		block2.active_pro_last_month2,
		block2.churn_this_month,
		block2.churn_last_month,
		block2.churn_last_month2,
		0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
		block2.churn_this_month€,
		block2.churn_last_month€,
		block2.churn_last_month2€,  
		0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
		block2.offers_received_this_month,
		block2.offers_received_last_month,
		block2.offers_received_last_month2,
		block2.offers_accepted_this_month,
		block2.offers_accepted_last_month,	     
		block2.offers_accepted_last_month2,
			
		0.6*(CASE WHEN block2.offers_received_this_month > 0 
		     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.offers_received_last_month > 0
			  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.offers_received_last_month2 > 0
		     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
			  ELSE 0
			  END) as avg_offers_accepted,
		
		block2.orders_submitted_this_month,
		block2.orders_submitted_last_month,
		block2.orders_submitted_last_month2,
		block2.orders_checked_this_month,
		block2.orders_checked_last_month,
		block2.orders_checked_last_month2,
		
		0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
		     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
			  ELSE 0
			  END) +
		0.3*(CASE WHEN block2.orders_submitted_last_month > 0
			  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
			  ELSE 0
			  END) +
		0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
		     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
			  ELSE 0
			  END) as avg_orders_checked,
			  
		block2.cases_this_month,
		block2.cases_last_month,
		block2.cases_last_month2,
		0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
	
	FROM
			
		(SELECT
		
			block1.sfid,
			block1.partner,
			-- block1.year_month,
			SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
			     ELSE block1.revenue_last_month
			     END) as revenue_last_month,
			SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
			     ELSE block1.revenue_last_month2
			     END) as revenue_last_month2,
			SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
			     ELSE block1.operational_costs_last_month
			     END) as operational_costs_last_month,
			SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
			     ELSE block1.hours_executed_this_month
			     END) as hours_executed_this_month,
			SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
			     ELSE block1.active_pro_this_month
			     END) as active_pro_this_month,
			SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
			     ELSE block1.active_pro_last_month
			     END) as active_pro_last_month,
			SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
			     ELSE block1.active_pro_last_month2
			     END) as active_pro_last_month2,
			SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
			     ELSE block1.churn_this_month
			     END) as churn_this_month,
			SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
			     ELSE block1.churn_last_month
			     END) as churn_last_month,
			SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
			     ELSE block1.churn_last_month2
			     END) as churn_last_month2,
			SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
			     ELSE block1.churn_this_month€
			     END) as churn_this_month€,
			SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
			     ELSE block1.churn_last_month€
			     END) as churn_last_month€,
			SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
			     ELSE block1.churn_last_month2€
			     END) as churn_last_month2€,     
			SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
			     ELSE block1.offers_received_this_month
			     END) as offers_received_this_month,
			SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
			     ELSE block1.offers_received_last_month
			     END) as offers_received_last_month,
			SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_received_last_month2
			     END) as offers_received_last_month2,
			SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_this_month
			     END) as offers_accepted_this_month,
			SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month
			     END) as offers_accepted_last_month,	     
			SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
			     ELSE block1.offers_accepted_last_month2
			     END) as offers_accepted_last_month2,
			SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_this_month
			     END) as orders_submitted_this_month,
			SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month
			     END) as orders_submitted_last_month,
			SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_submitted_last_month2
			     END) as orders_submitted_last_month2,
			SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
			     ELSE block1.orders_checked_this_month
			     END) as orders_checked_this_month,
			SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month
			     END) as orders_checked_last_month,
			SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
			     ELSE block1.orders_checked_last_month2
			     END) as orders_checked_last_month2,	
			SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
			     ELSE block1.cases_this_month
			     END) as cases_this_month,
			SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
			     ELSE block1.cases_last_month
			     END) as cases_last_month,
			SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
			     ELSE block1.cases_last_month2
			     END) as cases_last_month2
				
		FROM	
			
			(SELECT
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month,
				MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
				MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
				MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
				MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
				MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
				MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
				MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
				MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
				
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_this_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.value
							         ELSE 0 END) 
										END) as churn_last_month2,
										
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_this_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month€,
				SUM(CASE WHEN 
					         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
					               THEN deep_churn1.money
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
				                  THEN deep_churn1.money
							         ELSE 0 END) 
										END) as churn_last_month2€,
				MAX(offers.offers_received_this_month) as offers_received_this_month,
				MAX(offers.offers_received_last_month) as offers_received_last_month,
				MAX(offers.offers_received_last_month2) as offers_received_last_month2,
				MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
				MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
				MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
				
				MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
				MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
				MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
				MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
				MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
				MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
				
				MAX(number_cases.cases_this_month) as cases_this_month,
				MAX(number_cases.cases_last_month) as cases_last_month,
				MAX(number_cases.cases_last_month2) as cases_last_month2
									
			FROM
				
				
				((SELECT -- Simple list of all the accounts with the role master
				
					a.sfid,
					a.delivery_areas__c,
					a.name as partner
				
				FROM
				
					salesforce.account a 
						
				WHERE
				
					a.test__c IS FALSE
					AND a.role__c = 'master') as t0
					
				LEFT JOIN
					
					(SELECT 
					
						o.date_part as year_month,
						o.sub_kpi_2 as partner2,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as revenue_last_month,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as revenue_last_month2,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
										(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
				                        THEN o.value
							               ELSE 0 END) 
										      END) as cost_supply_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as operational_costs_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						         THEN o.value 
									ELSE 0 END) as active_pro_last_month2									
														
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
											
					GROUP BY
					
						o.date_part,
						partner2,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner2 asc) as t1
						
				ON
				
					t0.partner = t1.partner2) as in_kpi_master
					
			LEFT JOIN
			
				bi.deep_churn as deep_churn1
				
			ON
			
				in_kpi_master.partner = deep_churn1.company_name
				AND in_kpi_master.year_month = deep_churn1."year_month"
				
			LEFT JOIN
			
				(SELECT
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name as company_name__c,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
					SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 32)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
					SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - 64)::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
					
				
				FROM
				
					(SELECT
									
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
						MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
						a.sfid,
						a.name,
						a.company_name__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c,
						COUNT(o.suggested_partner__c) as offers_received,
						SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
					
					
					FROM
					
						salesforce.account a
					
					
					LEFT JOIN
					
						salesforce.partner_offer_partner__c o
							
					ON
					
						a.sfid = o.suggested_partner__c 
						
					LEFT JOIN
					
						salesforce.partner_offer__c oo
						
					ON 
					
						o.partner_offer__c = oo.sfid
						
					LEFT JOIN
					
						salesforce.opportunity ooo
						
					ON
					
						oo.opportunity__c = ooo.sfid
						
					WHERE
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
						AND a.test__c IS FALSE
						
					GROUP BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
						a.sfid,
						a.name,
						a.company_name__c,
						-- a.delivery_areas__c,
						a.type__c,
						a.status__c,
						a.role__c
					
					ORDER BY
					
						TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
						a.name asc) as offers1
						
				GROUP BY
				
					offers1.year_month_offer,
					offers1.sfid,
					offers1.name) as offers
					
			ON
			
				in_kpi_master.partner = offers.company_name__c
				AND in_kpi_master.year_month = offers.year_month_offer
				
			LEFT JOIN
			
				(SELECT
				
					orders_checked1.year_month,
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
					SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 32)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
					SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - 64)::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
				
				FROM	
					
					(SELECT
											
					  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
					  MIN(effectivedate::date) as mindate,
					  MAX(effectivedate::date) as maxdate,
					  t3.subcon as name_partner,
					  t3.sfid_partner,
					  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
					  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
					
					FROM
					
					  (SELECT
					  
					     t5.*,
					     t2.name as subcon,
					     t2.sfid as sfid_partner
					     
					   FROM
					   
					   	Salesforce.Account t5
					   
						JOIN
					      
							Salesforce.Account t2
					   
						ON
					   
							(t2.sfid = t5.parentid)
					     
						WHERE 
						
							t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
							-- AND t5.type__c = 'partner'
							AND LEFT(t5.locale__c, 2) = 'de'
							-- AND t5.role__c = 'master'
							AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
							AND t5.name NOT LIKE '%Handyman Kovacs%'
							AND t5.name NOT LIKE '%BAT Business Services GmbH%'
					   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
					   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
					      
					JOIN 
					
					  salesforce.order t4
					  
					ON
					
					  (t3.sfid = t4.professional__c)
					  
					WHERE
					
					  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
					  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
					  and LEFT(t4.locale__c,2) IN ('de')
					  and t4.effectivedate < current_date
					  AND t4.type = 'cleaning-b2b'
					  AND t4.test__c IS FALSE
					
					GROUP BY
					
					  year_month,
					  t3.subcon,
					  t3.sfid_partner
					  
					  
					ORDER BY
						
					  year_month desc,
					  t3.subcon) as orders_checked1
					  
				GROUP BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month
					
				ORDER BY
				
					orders_checked1.sfid_partner,
					orders_checked1.name_partner,
					orders_checked1.year_month desc) as orders_checked
					
			ON
			
				in_kpi_master.partner = orders_checked.name_partner
				AND in_kpi_master.year_month = orders_checked.year_month
				
			LEFT JOIN
			
				(SELECT
				
					t3.year_month,
					t3.partnerid,
					t4.name as name_partner,
					SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 32)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
					SUM(CASE WHEN t3.year_month = LEFT((current_date - 62)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
					
				FROM
							
					(SELECT
					
						TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
						CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
						t2.name_partner,
						t2.parentid,
						COUNT(DISTINCT t2.sfid_case) as number_cases
					
					FROM	
					
						(SELECT
						
							t1.date_case as date_case,
							CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
							t1.sfid_case,
							a.name as name_partner,
							a.parentid
							
						FROM	
							
							(SELECT 
							
								c.createddate as date_case,
								c.CaseNumber, 
								c.sfid as sfid_case, 
								o.sfid, 
								a.sfid,
								c.origin, 
								c.*
							
							FROM 
							
								salesforce.case c
								
							LEFT JOIN 
							
								salesforce.opportunity o
							
							ON
								o.sfid = c.opportunity__c
								
							LEFT JOIN 
							
								salesforce.account a on a.sfid = c.accountid
								
							WHERE 
							
								c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
								AND COALESCE(c.Origin, '') != 'System - Notification'
								AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
								AND c.type != 'CM B2C'
								AND c.CreatedDate >= (current_date - 103)
								AND c.test__c IS FALSE
								AND a.test__c IS FALSE
								AND COALESCE(o.name, '') NOT LIKE '%test%' 
								-- SFID from ## account
								AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
								-- SFID from BAT Business Services GmbH account
								AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
								AND c.parentid IS NULL) as t1
								
						LEFT JOIN
						
							salesforce.account a
							
						ON
						
							(t1.accountid = a.sfid)
							-- OR t1.partner__c = a.sfid)
							
						WHERE 
						
							a.name IS NOT NULL) as t2
							
					GROUP BY 
					
						TO_CHAR(t2.date_case, 'YYYY-MM'),
						t2.partner,
						t2.name_partner,
						t2.parentid
						
					ORDER BY
					
						t2.name_partner,
						TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
						
				LEFT JOIN
				
					salesforce.account t4
					
				ON
				
					t3.partnerid = t4.sfid
					
				GROUP BY
				
					t3.year_month,
					t3.partnerid,
					t4.name) as number_cases
					
			ON
			
				in_kpi_master.partner = number_cases.name_partner
				AND in_kpi_master.year_month = number_cases.year_month	
			
			GROUP BY
			
				in_kpi_master.sfid,
				in_kpi_master.partner,
				in_kpi_master.year_month
				
			ORDER BY
			
				in_kpi_master.sfid,
				in_kpi_master.year_month desc) as block1
	
		WHERE
	
			block1.year_month IS NOT NULL
					
		GROUP BY
		
			block1.sfid,
			block1.partner) as block2
	
	WHERE
	
		block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3;				



