	
	
SELECT	
	
	t1.opportunityid,
	t1.name,
	t1.last_valid_order,
	t1.confirmed_end,
	CASE WHEN last_valid_order > confirmed_end THEN 'To Check' ELSE 'Ok' END as comments


	
FROM
	
	(SELECT
	
		o.opportunityid,
		ooo.name,
		MAX(o.effectivedate) as last_valid_order,
		MAX(oo.confirmed_end__c) as confirmed_end
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.opportunityid = oo.opportunity__c
		
	LEFT JOIN
	
		salesforce.opportunity ooo
		
	ON
	
		o.opportunityid = ooo.sfid
		
	WHERE
	
		o.test__c IS FALSE
		AND o."status" IN ('INVOICED', 'FULFILLED', 'CANCELLED CUSTOMER', 'PENDING TO START')
		AND o."type" = 'cleaning-b2b'
		AND oo.status__c IN ('RESIGNED', 'CANCELLED', 'ACCEPTED', 'SIGNED')
		AND oo.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		o.opportunityid,
		ooo.name) as t1
		
WHERE
	
	CASE WHEN last_valid_order > confirmed_end THEN 'To Check' ELSE 'Ok' END = 'To Check'