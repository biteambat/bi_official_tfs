
SELECT

	in_kpi_master.sfid,
	in_kpi_master.partner,
	in_kpi_master.year_month,
	in_kpi_master.city,
	in_kpi_master.revenue_last_month,
	in_kpi_master.revenue_last_month2,
	in_kpi_master.cost_supply_last_month,
	in_kpi_master.operational_costs_last_month,
	in_kpi_master.hours_executed_this_month,
	in_kpi_master.active_pro_last_month,
	in_kpi_master.active_pro_last_month2,
	
	SUM(CASE WHEN 
		         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
		               THEN deep_churn1.value
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
	                  THEN deep_churn1.value
				         ELSE 0 END) 
							END) as churn_this_month,
	SUM(CASE WHEN 
		         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
		               THEN deep_churn1.value
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
	                  THEN deep_churn1.value
				         ELSE 0 END) 
							END) as churn_last_month,
	SUM(CASE WHEN 
		         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
		               THEN deep_churn1.value
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
	                  THEN deep_churn1.value
				         ELSE 0 END) 
							END) as churn_last_month2,
							
	SUM(CASE WHEN 
		         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
		               THEN deep_churn1.money
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
	                  THEN deep_churn1.money
				         ELSE 0 END) 
							END) as churn_this_month€,
	SUM(CASE WHEN 
		         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
		               THEN deep_churn1.money
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 32)::text, 7)) 
	                  THEN deep_churn1.money
				         ELSE 0 END) 
							END) as churn_last_month€,
	SUM(CASE WHEN 
		         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
		               THEN deep_churn1.money
					      ELSE 0 END) IS NULL THEN 0
				ELSE 
				   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - 64)::text, 7)) 
	                  THEN deep_churn1.money
				         ELSE 0 END) 
							END) as churn_last_month2€,
							
	SUM(CASE WHEN offers.mindate >= (current_date - 90) THEN offers.offers_received ELSE 0 END) as tot_offers_received,
	SUM(CASE WHEN offers.year_month_offer = LEFT(current_date::text, 7) THEN offers.offers_received ELSE 0 END) as offers_received_this_month,
	SUM(CASE WHEN offers.year_month_offer = LEFT(current_date::text, 7) THEN offers.offers_accepted ELSE 0 END) as offers_accepted_this_month
							
FROM
	
	
	((SELECT -- Simple list of all the accounts with the role master
	
		a.sfid,
		a.delivery_areas__c,
		a.name as partner
	
	FROM
	
		salesforce.account a 
			
	WHERE
	
		a.test__c IS FALSE
		AND a.role__c = 'master') as t0
		
	LEFT JOIN
		
		(SELECT 
		
			o.date_part as year_month,
			o.sub_kpi_2 as partner2,
			o.city,
			SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						THEN o.value 
						ELSE 0 END) as revenue_last_month,
			SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
			         THEN o.value 
						ELSE 0 END) as revenue_last_month2,
			SUM(CASE WHEN 
				         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
				               THEN o.value
							      ELSE 0 END) IS NULL THEN 0
						ELSE 
							(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
	                        THEN o.value
				               ELSE 0 END) 
							      END) as cost_supply_last_month,
			SUM(CASE WHEN 
				         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
				               THEN o.value
							      ELSE 0 END) IS NULL THEN 0
						ELSE 
						   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7)) 
			                  THEN o.value
						         ELSE 0 END) 
									END) as operational_costs_last_month,
			SUM(CASE WHEN 
				         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
				               THEN o.value
							      ELSE 0 END) IS NULL THEN 0
						ELSE 
						   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
			                  THEN o.value
						         ELSE 0 END) 
									END) as hours_executed_this_month,
			SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
						THEN o.value 
						ELSE 0 END) as active_pro_last_month,
			SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
			         THEN o.value 
						ELSE 0 END) as active_pro_last_month2									
											
		FROM
		
			bi.kpi_master o
			
		WHERE
		
			((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
			OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7))
			OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
			OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
			OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
			OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 32)::text, 7))
			OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - 62)::text, 7)))
								
		GROUP BY
		
			o.date_part,
			partner2,
			o.city
			
		ORDER BY
		
			year_month desc,
			partner2 asc) as t1
			
	ON
	
		t0.partner = t1.partner2) as in_kpi_master
		
LEFT JOIN

	bi.deep_churn as deep_churn1
	
ON

	in_kpi_master.partner = deep_churn1.company_name
	AND in_kpi_master.city = deep_churn1.city
	AND in_kpi_master.year_month = deep_churn1."year_month"
	
LEFT JOIN

	(SELECT
				
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
		MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
		a.sfid,
		a.name,
		a.company_name__c,
		a.delivery_areas__c,
		a.type__c,
		a.status__c,
		a.role__c,
		COUNT(o.suggested_partner__c) as offers_received,
		SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
	
	
	FROM
	
		salesforce.account a
	
	
	LEFT JOIN
	
		salesforce.partner_offer_partner__c o
			
	ON
	
		a.sfid = o.suggested_partner__c 
		
	WHERE
	
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
		AND a.test__c IS FALSE
		
	GROUP BY
	
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
		a.sfid,
		a.name,
		a.company_name__c,
		a.delivery_areas__c,
		a.type__c,
		a.status__c,
		a.role__c
	
	ORDER BY
	
		TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
		a.name asc) as offers
		
ON

	in_kpi_master.partner = offers.company_name__c
	AND in_kpi_master.city = offers.delivery_areas__c


GROUP BY

	in_kpi_master.sfid,
	in_kpi_master.partner,
	in_kpi_master.year_month,
	in_kpi_master.city,
	in_kpi_master.revenue_last_month,
	in_kpi_master.revenue_last_month2,
	in_kpi_master.cost_supply_last_month,
	in_kpi_master.operational_costs_last_month,
	in_kpi_master.hours_executed_this_month,
	in_kpi_master.active_pro_last_month,
	in_kpi_master.active_pro_last_month2
	
	
	