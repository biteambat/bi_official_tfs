SELECT		
			TO_CHAR ((closed.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(closed.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, closed.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, closed.case_status						AS sub_kpi_2
			, closed.case_reason						AS sub_kpi_3			
			, CASE 	WHEN closed.grand_total IS NULL	AND closed.opportunityid IS NULL 		THEN 'unknown'
					WHEN closed.grand_total IS NULL	AND closed.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN closed.grand_total < 250 											THEN '<250€'
		  			WHEN closed.grand_total >= 250 	AND closed.grand_total < 500 			THEN '250€-500€'
		 			WHEN closed.grand_total >= 500 	AND closed.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, closed.opportunity						AS sub_kpi_5
			, closed.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, closed.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			
FROM 		salesforce.casehistory 			hi

INNER JOIN 	bi.cm_cases_basis 				ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- closed cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Closed'
			AND hi.createddate::date >= '2018-11-01'

			
) AS closed

GROUP BY 	closed.date
			, closed.case_origin
			, closed.case_status	
			, closed.case_reason
			, closed.grand_total
			, closed.opportunityid
			, closed.opportunity	
			, closed.case_owner			
;