 SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, n.callringseconds__c							AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493030807264', '41435084849')
			-- AND n.call_start_date_time__c::date = 'YESTERDAY'
			AND ((n.callringseconds__c < 5 AND n.callconnectedcheckbox__c IS FALSE) IS FALSE)
			AND u.name = 'Natter Tiger'
		
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.callringseconds__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c