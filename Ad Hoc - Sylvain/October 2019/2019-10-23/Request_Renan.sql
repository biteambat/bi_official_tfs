
SELECT	
	
	block4.*,
	block5.total_cost,
	CASE WHEN (block5.total_cost/10000)*20 > 20 THEN 20 ELSE (block5.total_cost/10000)*20 END as score_cost_partner

FROm
	
	(SELECT
	
		block3.sfid,
		block3.partner,	
		0.6*block3.hours_executed_this_month + 0.3*block3.hours_executed_last_month + 0.1*block3.hours_executed_last_month2 as hours_executed,	
		block3.revenue_last_month,
		block3.revenue_last_month2,
		block3.revenue_last_month3,
		block3.score_revenue,
		CASE WHEN EXTRACT(DAY FROM current_date) > 7 
		     THEN
					CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation revenue €'
								  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue €'
								  ELSE 'Decreasing revenue €'
								  END
			  ELSE
			      CASE WHEN block3.revenue_last_month2 = block3.revenue_last_month3 THEN 'Stagnation revenue €'
								  WHEN block3.revenue_last_month2 > block3.revenue_last_month3 THEN 'Increasing revenue €'
								  ELSE 'Decreasing revenue €'
								  END
			  END as trend,
		block3.operational_costs_last_month,
		block3.operational_costs_last_month2,
		block3.gpm as gpm_last_month,
		
		CASE WHEN (block3.gpm*100/30)*15 > 15 THEN 15
		     WHEN (block3.gpm*100/30)*15 < 0 THEN 0
			  ELSE (block3.gpm*100/30)*15
			  END as score_gpm,
	
		block3.active_pro_this_month,
		block3.active_pro_last_month,
		block3.active_pro_last_month2,
		CASE WHEN (block3.active_pro_last_month/10)*5 > 5 THEN 5
		     WHEN block3.active_pro_last_month IS NULL THEN 0
		     ELSE (block3.active_pro_last_month/10)*5
		     END as score_active_pro,
		CASE WHEN block3.active_pro_this_month > 0 
			  THEN block3.hours_executed_this_month/block3.active_pro_this_month
			  ELSE 0
			  END as hours_per_cleaner,
			  
		CASE WHEN ((CASE WHEN block3.active_pro_this_month > 0 
							  THEN block3.hours_executed_this_month/block3.active_pro_this_month
							  ELSE 0
							  END)/20)*5 > 5 THEN 5
			  ELSE 
					 ((CASE WHEN block3.active_pro_this_month > 0 
							  THEN block3.hours_executed_this_month/block3.active_pro_this_month
							  ELSE 0
							  END)/20)*5
			  END as score_hours_cleaners,
		     
		CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
			  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
			  ELSE 'Decreasing # workers'
			  END as trend_active_pros,
		block3.churn_this_month,
		block3.churn_last_month,
		block3.churn_last_month2,
		block3.avg_churn as weighted_churn,
		
		CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
		     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
		     ELSE 0 
		     END as score_weighted_churn,
		
		block3.churn_this_month€,
		block3.churn_last_month€,
		block3.churn_last_month2€,  
		block3.avg_churn€ as weighted_churn_€,   
		
		CASE WHEN (block3.avg_churn€/500) = 0 THEN 10
		     WHEN (block3.avg_churn€/500) > 1 THEN 0
		     ELSE 10 - (block3.avg_churn€/500)
		     END as score_weighted_churn_€,
		
		block3.offers_received_this_month,
		block3.offers_received_last_month,
		block3.offers_received_last_month2,
		block3.offers_accepted_this_month,
		block3.offers_accepted_last_month,	     
		block3.offers_accepted_last_month2,		
		block3.avg_offers_accepted as pc_accepted_weighted,
		
		block3.avg_offers_accepted*10 as score_acceptance_weighted,
		
		block3.orders_submitted_this_month,
		block3.orders_submitted_last_month,
		block3.orders_submitted_last_month2,
		block3.orders_checked_this_month,
		block3.orders_checked_last_month,
		block3.orders_checked_last_month2,	
		block3.avg_orders_checked as pc_checked_weighted,
			  
		block3.avg_orders_checked*10 as score_checked_weighted,
		
		block3.cases_this_month,
		block3.cases_last_month,
		block3.cases_last_month2,
		block3.avg_number_cases as number_cases
	
	
	FROM	
		
		(SELECT
		
			block2.sfid,
			block2.partner,
			block2.hours_executed_this_month,
			block2.hours_executed_last_month,
			block2.hours_executed_last_month2,	
			block2.revenue_last_month,
			block2.revenue_last_month2,
			block2.revenue_last_month3,
			CASE WHEN EXTRACT(DAY FROM current_date) > 7
				  THEN CASE WHEN (block2.revenue_last_month/10000)*20 > 20 THEN 20 ELSE (block2.revenue_last_month/10000)*20 END 
			     ELSE 
				       CASE WHEN (block2.revenue_last_month2/10000)*20 > 20 THEN 20 ELSE (block2.revenue_last_month2/10000)*20 END 
				  END as score_revenue,
			block2.operational_costs_last_month,
			block2.operational_costs_last_month2,
			CASE WHEN EXTRACT(DAY FROM current_date) > 7 
			     THEN 
						CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END
	
				  ELSE 
						CASE WHEN (block2.revenue_last_month2) > 0 THEN (block2.revenue_last_month2 - block2.operational_costs_last_month2)/(block2.revenue_last_month2) ELSE 0 END
				  END as gpm,
			block2.active_pro_this_month,
			block2.active_pro_last_month,
			block2.active_pro_last_month2,
			block2.churn_this_month,
			block2.churn_last_month,
			block2.churn_last_month2,
			0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
			block2.churn_this_month€,
			block2.churn_last_month€,
			block2.churn_last_month2€,  
			0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
			block2.offers_received_this_month,
			block2.offers_received_last_month,
			block2.offers_received_last_month2,
			block2.offers_accepted_this_month,
			block2.offers_accepted_last_month,	     
			block2.offers_accepted_last_month2,
				
			0.6*(CASE WHEN block2.offers_received_this_month > 0 
			     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
				  ELSE 0
				  END) +
			0.3*(CASE WHEN block2.offers_received_last_month > 0
				  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
				  ELSE 0
				  END) +
			0.1*(CASE WHEN block2.offers_received_last_month2 > 0
			     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
				  ELSE 0
				  END) as avg_offers_accepted,
			
			block2.orders_submitted_this_month,
			block2.orders_submitted_last_month,
			block2.orders_submitted_last_month2,
			block2.orders_checked_this_month,
			block2.orders_checked_last_month,
			block2.orders_checked_last_month2,
			
			0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
			     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
				  ELSE 0
				  END) +
			0.3*(CASE WHEN block2.orders_submitted_last_month > 0
				  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
				  ELSE 0
				  END) +
			0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
			     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
				  ELSE 0
				  END) as avg_orders_checked,
				  
			block2.cases_this_month,
			block2.cases_last_month,
			block2.cases_last_month2,
			0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
		
		FROM
				
			(SELECT
			
				block1.sfid,
				block1.partner,
				-- block1.year_month,
				SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
				     ELSE block1.revenue_last_month
				     END) as revenue_last_month,
				SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
				     ELSE block1.revenue_last_month2
				     END) as revenue_last_month2,
				SUM(CASE WHEN block1.revenue_last_month3 IS NULL THEN -0
				     ELSE block1.revenue_last_month3
				     END) as revenue_last_month3,
				SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
				     ELSE block1.operational_costs_last_month
				     END) as operational_costs_last_month,
				SUM(CASE WHEN block1.operational_costs_last_month2 IS NULL THEN -0
				     ELSE block1.operational_costs_last_month2
				     END) as operational_costs_last_month2,
				SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
				     ELSE block1.hours_executed_this_month
				     END) as hours_executed_this_month,
				SUM(CASE WHEN block1.hours_executed_last_month IS NULL THEN -0
				     ELSE block1.hours_executed_last_month
				     END) as hours_executed_last_month,
				SUM(CASE WHEN block1.hours_executed_last_month2 IS NULL THEN -0
				     ELSE block1.hours_executed_last_month2
				     END) as hours_executed_last_month2,
				SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
				     ELSE block1.active_pro_this_month
				     END) as active_pro_this_month,
				SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
				     ELSE block1.active_pro_last_month
				     END) as active_pro_last_month,
				SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
				     ELSE block1.active_pro_last_month2
				     END) as active_pro_last_month2,
				SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
				     ELSE block1.churn_this_month
				     END) as churn_this_month,
				SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
				     ELSE block1.churn_last_month
				     END) as churn_last_month,
				SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
				     ELSE block1.churn_last_month2
				     END) as churn_last_month2,
				SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
				     ELSE block1.churn_this_month€
				     END) as churn_this_month€,
				SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
				     ELSE block1.churn_last_month€
				     END) as churn_last_month€,
				SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
				     ELSE block1.churn_last_month2€
				     END) as churn_last_month2€,     
				SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
				     ELSE block1.offers_received_this_month
				     END) as offers_received_this_month,
				SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
				     ELSE block1.offers_received_last_month
				     END) as offers_received_last_month,
				SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
				     ELSE block1.offers_received_last_month2
				     END) as offers_received_last_month2,
				SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
				     ELSE block1.offers_accepted_this_month
				     END) as offers_accepted_this_month,
				SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
				     ELSE block1.offers_accepted_last_month
				     END) as offers_accepted_last_month,	     
				SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
				     ELSE block1.offers_accepted_last_month2
				     END) as offers_accepted_last_month2,
				SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
				     ELSE block1.orders_submitted_this_month
				     END) as orders_submitted_this_month,
				SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
				     ELSE block1.orders_submitted_last_month
				     END) as orders_submitted_last_month,
				SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
				     ELSE block1.orders_submitted_last_month2
				     END) as orders_submitted_last_month2,
				SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
				     ELSE block1.orders_checked_this_month
				     END) as orders_checked_this_month,
				SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
				     ELSE block1.orders_checked_last_month
				     END) as orders_checked_last_month,
				SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
				     ELSE block1.orders_checked_last_month2
				     END) as orders_checked_last_month2,	
				SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
				     ELSE block1.cases_this_month
				     END) as cases_this_month,
				SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
				     ELSE block1.cases_last_month
				     END) as cases_last_month,
				SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
				     ELSE block1.cases_last_month2
				     END) as cases_last_month2
					
			FROM	
				
				(SELECT
				
					in_kpi_master.sfid,
					in_kpi_master.partner,
					in_kpi_master.year_month,
					MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
					MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
					MAX(in_kpi_master.revenue_last_month3) as revenue_last_month3,
					MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
					MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
					MAX(in_kpi_master.operational_costs_last_month2) as operational_costs_last_month2,
					MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
					MAX(in_kpi_master.hours_executed_last_month) as hours_executed_last_month,
					MAX(in_kpi_master.hours_executed_last_month2) as hours_executed_last_month2,
					MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
					MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
					MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
					
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_this_month,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_last_month,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_last_month2,
											
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_this_month€,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_last_month€,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_last_month2€,
					MAX(offers.offers_received_this_month) as offers_received_this_month,
					MAX(offers.offers_received_last_month) as offers_received_last_month,
					MAX(offers.offers_received_last_month2) as offers_received_last_month2,
					MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
					MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
					MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
					
					MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
					MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
					MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
					MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
					MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
					MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
					
					MAX(number_cases.cases_this_month) as cases_this_month,
					MAX(number_cases.cases_last_month) as cases_last_month,
					MAX(number_cases.cases_last_month2) as cases_last_month2
										
				FROM
					
					
					((SELECT -- Simple list of all the accounts with the role master
					
						a.sfid,
						a.delivery_areas__c,
						a.name as partner
					
					FROM
					
						salesforce.account a 
							
					WHERE
					
						a.test__c IS FALSE
						AND a.role__c = 'master') as t0
						
					LEFT JOIN
						
						(SELECT -- Query taking data regarding the partners from the table bi.kpi_master: Revenue, Cost Supply, Operational Costs, Hours Executed, Active Professionals
						
							o.date_part as year_month,
							o.sub_kpi_2 as partner2,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
										THEN o.value 
										ELSE 0 END) as revenue_last_month,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as revenue_last_month2,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as revenue_last_month3,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
											(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
					                        THEN o.value
								               ELSE 0 END) 
											      END) as cost_supply_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as operational_costs_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							                  THEN o.value
										         ELSE 0 END) 
													END) as operational_costs_last_month2,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_this_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_last_month2,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
										THEN o.value 
										ELSE 0 END) as active_pro_this_month,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
										THEN o.value 
										ELSE 0 END) as active_pro_last_month,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as active_pro_last_month2									
															
						FROM
						
							bi.kpi_master o
							
						WHERE
						
							((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
							OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) AND o.sub_kpi_5 = 'Scorecard'
							OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) AND o.sub_kpi_5 = 'Scorecard'
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
												
						GROUP BY
						
							o.date_part,
							partner2
							
						ORDER BY
						
							year_month desc,
							partner2 asc) as t1
							
					ON
					
						t0.partner = t1.partner2) as in_kpi_master
						
				LEFT JOIN
				
					bi.deep_churn as deep_churn1
					
				ON
				
					in_kpi_master.partner = deep_churn1.company_name
					AND in_kpi_master.year_month = deep_churn1."year_month"
					
				LEFT JOIN
				
					(SELECT  -- Query containing the number of offers sent to partners, and offers accepted by partners every month in the last 3 months
					
						offers1.year_month_offer,
						offers1.sfid,
						offers1.name as company_name__c,
						SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '1' month))::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '2' month))::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
						SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '1' month))::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '2' month))::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
						
					
					FROM
					
						(SELECT -- Query containing the number of offers sent to partners, and offers accepted by partners every month in the last 3 months
										
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
							MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
							a.sfid,
							a.name,
							a.company_name__c,
							-- a.delivery_areas__c,
							a.type__c,
							a.status__c,
							a.role__c,
							COUNT(o.suggested_partner__c) as offers_received,
							SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
						
						
						FROM
						
							salesforce.account a
						
						
						LEFT JOIN
						
							salesforce.partner_offer_partner__c o
								
						ON
						
							a.sfid = o.suggested_partner__c 
							
						LEFT JOIN
						
							salesforce.partner_offer__c oo
							
						ON 
						
							o.partner_offer__c = oo.sfid
							
						LEFT JOIN
						
							salesforce.opportunity ooo
							
						ON
						
							oo.opportunity__c = ooo.sfid
							
						WHERE
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
							AND a.test__c IS FALSE
							AND o.createddate >= (current_date - '3 MONTHS'::INTERVAL) -- Only offers created in the last 3 months
							
						GROUP BY
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
							a.sfid,
							a.name,
							a.company_name__c,
							-- a.delivery_areas__c,
							a.type__c,
							a.status__c,
							a.role__c
						
						ORDER BY
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
							a.name asc) as offers1
							
					GROUP BY
					
						offers1.year_month_offer,
						offers1.sfid,
						offers1.name) as offers
						
				ON
				
					in_kpi_master.partner = offers.company_name__c
					AND in_kpi_master.year_month = offers.year_month_offer
					
				LEFT JOIN
				
					(SELECT -- Query extracting the numbers of orders planned for the partners and the orders checked by the partners
					
						orders_checked1.year_month,
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
						SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
					
					FROM	
						
						(SELECT -- Query extracting the numbers of orders planned for the partners and the orders checked by the partners
												
						  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
						  MIN(effectivedate::date) as mindate,
						  MAX(effectivedate::date) as maxdate,
						  t3.subcon as name_partner,
						  t3.sfid_partner,
						  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
						  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
						
						FROM
						
						  (SELECT
						  
						     t5.sfid,
						     t5.name,
						     t2.name as subcon,
						     t2.sfid as sfid_partner
						     
						   FROM
						   
						   	Salesforce.Account t5
						   
							JOIN
						      
								Salesforce.Account t2
						   
							ON
						   
								(t2.sfid = t5.parentid)
						     
							WHERE 
							
								t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
								-- AND t5.type__c = 'partner'
								AND LEFT(t5.locale__c, 2) = 'de'
								-- AND t5.role__c = 'master'
								AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
								AND t5.name NOT LIKE '%Handyman Kovacs%'
								AND t5.name NOT LIKE '%BAT Business Services GmbH%'
						   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
						   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
						      
						JOIN 
						
						  salesforce.order t4
						  
						ON
						
						  (t3.sfid = t4.professional__c)
						  
						WHERE
						
						  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
						  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
						  and LEFT(t4.locale__c,2) IN ('de')
						  and t4.effectivedate < current_date
						  AND t4.type = 'cleaning-b2b'
						  AND t4.test__c IS FALSE
						
						GROUP BY
						
						  year_month,
						  t3.subcon,
						  t3.sfid_partner
						  
						  
						ORDER BY
							
						  year_month desc,
						  t3.subcon) as orders_checked1
						  
					GROUP BY
					
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.year_month
						
					ORDER BY
					
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.year_month desc) as orders_checked
						
				ON
				
					in_kpi_master.partner = orders_checked.name_partner
					AND in_kpi_master.year_month = orders_checked.year_month
					
				LEFT JOIN
				
					(SELECT -- Query extracting the cases of the partners (improvements, bad feedbacks...) in the last 3 months
					
						t3.year_month,
						t3.partnerid,
						t4.name as name_partner,
						SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
						SUM(CASE WHEN t3.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
						SUM(CASE WHEN t3.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
						
					FROM
								
						(SELECT
						
							TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
							CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
							t2.name_partner,
							t2.parentid,
							COUNT(DISTINCT t2.sfid_case) as number_cases
						
						FROM	
						
							(SELECT
							
								t1.date_case as date_case,
								CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
								t1.sfid_case,
								a.name as name_partner,
								a.parentid
								
							FROM	
								
								(SELECT -- Query containing the cases related to partners 
								
									c.createddate as date_case,
									c.CaseNumber, 
									c.sfid as sfid_case, 
									o.sfid, 
									a.sfid,
									c.origin, 
									c.*
								
								FROM 
								
									salesforce.case c
									
								LEFT JOIN 
								
									salesforce.opportunity o
								
								ON
									o.sfid = c.opportunity__c
									
								LEFT JOIN 
								
									salesforce.account a on a.sfid = c.accountid
									
								WHERE 
								
									c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement') -- Only bad cases
									AND COALESCE(c.Origin, '') != 'System - Notification'
									AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4') -- We take only bad feedbacks into account
									AND c.type != 'CM B2C' -- Only B2B related
									AND c.CreatedDate >= (current_date - '3 MONTHS'::INTERVAL) -- We consider only the last 3 months
									AND c.test__c IS FALSE
									AND a.test__c IS FALSE
									AND COALESCE(o.name, '') NOT LIKE '%test%' 
									-- SFID from ## account
									AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
									-- SFID from BAT Business Services GmbH account
									AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
									AND c.parentid IS NULL) as t1
									
							LEFT JOIN
							
								salesforce.account a
								
							ON
							
								(t1.accountid = a.sfid)
								-- OR t1.partner__c = a.sfid)
								
							WHERE 
							
								a.name IS NOT NULL) as t2
								
						GROUP BY 
						
							TO_CHAR(t2.date_case, 'YYYY-MM'),
							t2.partner,
							t2.name_partner,
							t2.parentid
							
						ORDER BY
						
							t2.name_partner,
							TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
							
					LEFT JOIN
					
						salesforce.account t4
						
					ON
					
						t3.partnerid = t4.sfid
						
					GROUP BY
					
						t3.year_month,
						t3.partnerid,
						t4.name) as number_cases
						
				ON
				
					in_kpi_master.partner = number_cases.name_partner
					AND in_kpi_master.year_month = number_cases.year_month	
				
				GROUP BY
				
					in_kpi_master.sfid,
					in_kpi_master.partner,
					in_kpi_master.year_month
					
				ORDER BY
				
					in_kpi_master.sfid,
					in_kpi_master.year_month desc) as block1
		
			WHERE
		
				block1.year_month IS NOT NULL
						
			GROUP BY
			
				block1.sfid,
				block1.partner) as block2
		
		WHERE
		
			block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3) as block4

LEFT JOIN

	(SELECT
	
		t3.served_by__c as sfid_partner,
		t3.partner,
		SUM(t3.final_partner_cost) as total_cost
	
	FROM	
		
		(SELECT	
			
			t1.*,
			t2.hours,
			CASE WHEN t1.partner_costs = 0 THEN t2.hours*t1.pph_partner ELSE t1.partner_costs END as final_partner_cost
			
		FROM
		
			(SELECT
			
				o.sfid as sfid_opp,
				o.name as name_opp,
				o.delivery_area__c,
				o.served_by__c,
				a.name as partner,
				a.pph__c as pph_partner,
				o.plan_pph__c,
				SUM(cont.grand_total__c) as grand_total,
				SUM(o.potential_partner_costs__c) as partner_costs
				
			FROM
			
				salesforce.opportunity o
				
			LEFT JOIN
			
				salesforce.contract__c cont
				
			ON
			
				o.sfid = cont.opportunity__c
				
			LEFT JOIN
			
				salesforce.account a
				
			ON
			
				o.served_by__c = a.sfid
				
			WHERE
			
				o.status__c NOT IN ('RESIGNED', 'CANCELLED')
				AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')
				AND o.test__c IS FALSE
				AND cont.test__c IS FALSE
				AND cont.active__c IS TRUE
				AND o.served_by__c IS NOT NULL
				AND a.name NOT LIKE '%BAT %'
				
			GROUP BY
			
				o.sfid,
				o.name,
				o.delivery_area__c,
				o.served_by__c,
				o.plan_pph__c,
				a.pph__c,
				a.name) as t1
				
		LEFT JOIN
		
			(SELECT
		
				o.opportunityid,
				SUM(o.order_duration__c) as hours
			
			
			FROM
			
				salesforce.order o
				
			WHERE
			
				o.test__c IS FALSE
				AND o."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
				AND o.effectivedate BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '4 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
				
			GROUP BY
			
				o.opportunityid) as t2
				
		ON
		
			t1.sfid_opp = t2.opportunityid) as t3
			
	GROUP BY	
	
		t3.served_by__c,
		t3.partner) as block5
		
ON

	block4.sfid = block5.sfid_partner;