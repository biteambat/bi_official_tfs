-- DROP TABLE IF EXISTS bi.test_kadir;

-- CREATE TABLE bi.test_kadir AS
-- The number of hours realized by cleaners, the number of cleaners of BAT in DE per month
WITH cte1 AS(
 
SELECT

    sub.*,
    SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END):: DECIMAL AS total_cancelled_orders
    
FROM

    salesforce.order o
    
LEFT JOIN (

    SELECT
    
        TO_CHAR(effectivedate, 'YYYY-MM') AS year_month,
        COUNT(DISTINCT(o.professional__c)) AS active_cleaners,
        SUM(o.order_duration__c) AS realized_hours
        
    FROM
    
        salesforce.order o
        
    LEFT JOIN 
     
        salesforce.account a 
         
    ON
        
        a.sfid = o.professional__c
          
    WHERE
    
        a.company_name__c ILIKE '%BAT%'
        AND LEFT(o.delivery_Area__c, 2) = 'de'
        AND effectivedate >= '2019-01-01'
        AND o.test__c = '0'
        AND a.test__c = '0'
        AND o.status IN( 'INVOICED', 'PENDING TO START', 'FULFILLED' )
        
    GROUP BY
    
        1 ) sub ON TO_CHAR(effectivedate, 'YYYY-MM') = sub.year_month
        
LEFT JOIN 

    salesforce.account a 
    
ON 

    a.sfid = o.professional__c
    
WHERE

    o.status IN ('CANCELLED PROFESSIONAL')
    AND a.company_name__c ILIKE '%BAT%'
    AND LEFT(o.delivery_Area__c, 2) = 'de'
    AND effectivedate >= '2019-01-01'
    AND o.test__c = '0'
    AND a.test__c = '0'
    
GROUP BY

    sub.year_month,
    sub.active_cleaners,
    sub.realized_hours ),
    
-- The number of sickness days by cleaners per month
 sickness AS(
 
SELECT

    TO_CHAR(hr.createddate, 'YYYY-MM') AS year_month,
    hr.account__c AS cleaner,
    SUM(hr.days__c) AS sickness_days
    
FROM

    salesforce.hr__c AS hr
    
LEFT JOIN 

    salesforce.order o ON hr.sfid = o.professional__c
    
WHERE

    hr.type__c = 'sickness'
    AND description__c NOT LIKE '%TEST%'
    AND description__c NOT LIKE '%test%'
    AND hr.createddate >= '2019-01-01'
    
GROUP BY

    cleaner,
    year_month )
    
SELECT

    cte1.year_month,
    cte1.active_cleaners,
    cte1.realized_hours,
    cte1.total_cancelled_orders,
    (cte1.total_cancelled_orders / cte1.active_cleaners)::DECIMAL AS cancelled_orders_per_active_cleaner,
    (cte1.realized_hours / cte1.active_cleaners) AS realized_hours_per_active_cleaner,
    SUM(sickness.sickness_days) / cte1.active_cleaners AS sickness_per_active_cleaner
    
FROM

    cte1
    
JOIN 

    sickness ON cte1.year_month = sickness.year_month
    
GROUP BY

    cte1.year_month,
    cte1.active_cleaners,
    cte1.realized_hours,
    cte1.total_cancelled_orders
    
ORDER BY
    cte1.year_month;



