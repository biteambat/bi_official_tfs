	
	
	
DROP TABLE IF EXISTS bi.calendar_matrix;
CREATE TABLE bi.calendar_matrix as 	

SELECT	
	
	t1.year_month,
	MAX(t1.number_days_this_month) as number_days_this_month,
	SUM(CASE WHEN t1.day_week = 'monday   ' THEN 1 ELSE 0 END) as mondays,
	SUM(CASE WHEN t1.day_week = 'tuesday  ' THEN 1 ELSE 0 END) as tuesdays,
	SUM(CASE WHEN t1.day_week = 'wednesday' THEN 1 ELSE 0 END) as wednesdays,
	SUM(CASE WHEN t1.day_week = 'thursday ' THEN 1 ELSE 0 END) as thursdays,
	SUM(CASE WHEN t1.day_week = 'friday   ' THEN 1 ELSE 0 END) as fridays,
	SUM(CASE WHEN t1.day_week = 'saturday ' THEN 1 ELSE 0 END) as saturdays,
	SUM(CASE WHEN t1.day_week = 'sunday   ' THEN 1 ELSE 0 END) as sundays


FROM
	
	(SELECT 
	
		LEFT(i.date::text, 7) as year_month,
		i.date::date,
		DATE_PART('days', DATE_TRUNC('month', i.date) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		TO_CHAR(i.date, 'day') as day_week
		
	FROM 
	
		generate_series(date '2018-01-01', date '2025-01-01', interval '1 day') as i) as t1
		
GROUP BY

	t1.year_month
	
ORDER BY

	t1.year_month
		
	
	       