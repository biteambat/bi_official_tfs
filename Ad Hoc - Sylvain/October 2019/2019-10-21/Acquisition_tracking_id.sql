


SELECT


	DISTINCT l.acquisition_tracking_id__c


FROM

	salesforce.likeli__c l
	
WHERE

	l.test__c IS FALSE
	AND (l.acquisition_tracking_id__c LIKE 'tfs %' OR l.acquisition_tracking_id__c LIKE 'rd %')