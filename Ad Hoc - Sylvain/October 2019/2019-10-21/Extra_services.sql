
	
SELECT

	'Extra Services' as kpi,
	t3.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	t3.service_type__c,
	t3.money as grand_total,
	SUM(t3.invoiced_this_month) as on_top_this_month

FROM	
			
	(SELECT
	
		TO_CHAR(NOW(), 'YYYY-MM') as this_month,
		t2.sfid,
		t2.opportunity__c,
		t2.name,
		t2.service_type__c,
		t2.recurrency__c,
		t2.note__c,
		t2.date_start,
		t2.money,
		t2.number_days_this_month,
		t2.days_to_invoice,
		CASE WHEN TO_CHAR(t2.date_start, 'YYYY-MM') = TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('water dispenser', 'coffee machine')
			  THEN t2.days_to_invoice*t2.money/30.4
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('water dispenser', 'coffee machine')
			  THEN t2.money
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('fruits') AND t2.recurrency__c = 'weekly'
			  THEN t2.money*cal.mondays
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('fruits') AND t2.recurrency__c = 'biweekly'
			  THEN t2.money*cal.mondays/2
			  
			  WHEN (TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c NOT IN ('water dispenser', 'coffee machine', 'fruits') AND t2.note__c LIKE '%' || TO_CHAR(NOW(), 'YYYY-MM') || '%'
			       OR TO_CHAR(t2.date_start, 'YYYY-MM') = TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c NOT IN ('water dispenser', 'coffee machine', 'fruits'))
			  THEN t2.money
			  
			  WHEN t2.sfid = 'a1C0J000008hzECUAY' THEN t2.money
			  
			  WHEN t2.sfid = 'a1C0J000008E8aBUAS' THEN 2303
			  
			  WHEN t2.sfid = 'a1C0J000008E8mMUAS' THEN 932.4
			  
			  WHEN t2.sfid = 'a1C0J000009ZpvyUAC' THEN 16
			  
			  ELSE 0
			  
			  END as invoiced_this_month
	
	FROM	
		
		(SELECT	
			
			t1.sfid,
			t1.opportunity__c,
			t1.name,
			t1.service_type__c,
			t1.recurrency__c,
			t1.note__c,
			t1.date_start,
			t1.money,
			DATE_PART('days', DATE_TRUNC('month', t1.date_start) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
			DATE_PART('days', DATE_TRUNC('month', t1.date_start) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.date_start) + 1 as days_to_invoice		
			
		FROM
			
			(SELECT
			
				o.sfid,
				o.opportunity__c,
				o.name,
				o.service_type__c,
				o.recurrency__c,
				o.note__c,
				CASE WHEN o.effective_start__c IS NULL THEN o.start__c ELSE o.effective_start__c END as date_start,
				SUM(o.grand_total__c) as money
			
			FROM
			
				salesforce.contract__c o
				
			WHERE
			
				o.service_type__c NOT LIKE 'maintenance cleaning'
				AND o.active__c IS TRUE
				AND o.test__c IS FALSE
				
			GROUP BY
			
				o.sfid,
				o.opportunity__c,
				o.name,
				o.service_type__c,
				o.effective_start__c,
				o.start__c,
				o.recurrency__c,
				o.note__c
				
			ORDER BY
			
				o.sfid,
				o.service_type__c,
				o.effective_start__c) as t1) as t2
				
	LEFT JOIN
	
		bi.calendar_matrix cal
		
	ON
	
		TO_CHAR(NOW(), 'YYYY-MM') = cal."year_month") as t3
		
LEFT JOIN

	salesforce.opportunity o
	
ON

	o.sfid = t3.opportunity__c
	
WHERE

	t3.invoiced_this_month > 0
	
GROUP BY

	t3.opportunity__c,
	o.name,
	o.delivery_area__c,
	t3.service_type__c,
	t3.money
	
			
