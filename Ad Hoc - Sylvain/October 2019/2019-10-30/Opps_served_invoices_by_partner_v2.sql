
SELECT

	t1.served_by__c,
	-- t1.service_type__c,
	COUNT(DISTINCT t1.opportunity__c) as opps_invoiced,
	COUNT(DISTINCT t1.sfid) as invoices,
	SUM(CASE WHEN t1.service_type__c = 'maintenance cleaning' THEN 1 ELSE 0 END) as invoices_maintenance,
	SUM(CASE WHEN t1.service_type__c NOT LIKE 'maintenance cleaning' THEN 1 ELSE 0 END) as invoices_others
	

FROM

	(SELECT
	
		o.served_by__c,
		inv.opportunity__c,
		inv.sfid,
		inv.service_type__c
		-- COUNT(DISTINCT o.opportunityid) as opps_served_last_month,
		-- COUNT(DISTINCT inv.sfid) as invoices_last_month,
		-- SUM(CASE WHEN inv.service_type__c = 'maintenance cleaning' THEN 1 ELSE 0 END) as invoices_maintenance,
		-- SUM(CASE WHEN inv.service_type__c NOT LIKE 'maintenance cleaning' THEN 1 ELSE 0 END) as invoices_others
		
	FROM
	
		salesforce.invoice__c inv
		
		
	LEFT JOIN
	
		salesforce.order o
		
	ON
	
		o.opportunityid = inv.opportunity__c
		
	WHERE
	
		o.test__c IS FALSE
		AND inv.test__c IS FALSE
		AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
		AND LEFT(o.effectivedate::text, 7) = LEFT((current_date - interval '1 MONTH')::text,7)
		AND LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 MONTH')::text,7)
		AND inv.parent_invoice__c IS NULL
		
	
		
	GROUP BY
	
		o.served_by__c,
		inv.opportunity__c,
		inv.sfid,
		inv.service_type__c) as t1
		
GROUP BY

	t1.served_by__c