
-- UR% Calculation

-- Working hours, worked hours and capped work hours for each cleaner by month
WITH t1 AS (
SELECT
    t1.*,
    CASE
        WHEN SUM(t1.working_hours) > 0 THEN SUM(t1.capped_work_hours) / SUM(t1.working_hours)
        ELSE 0
    END AS utilization
FROM
    (
    SELECT
        TO_CHAR(o.effectivedate, 'YYYY-MM') AS year_month,
        o.professional__c AS prof,
        SUM(o.order_duration__c) AS worked_hours,
        (a.hr_contract_weekly_hours_min__c * cal.number_days_this_month/7) AS working_hours,
        CASE
            WHEN SUM(o.order_duration__c) >= (a.hr_contract_weekly_hours_min__c*4.3) THEN (a.hr_contract_weekly_hours_min__c*4.3)
            ELSE SUM(o.order_duration__c)
        END AS capped_work_hours
    FROM
        salesforce.account a
    LEFT JOIN salesforce.order o ON
        a.sfid = o.professional__c
        
    LEFT JOIN 
    
    	bi.calendar_matrix cal
    	
    ON
   	
   	TO_CHAR(o.effectivedate, 'YYYY-MM')  = cal."year_month"
   	
    WHERE
        a.hr_contract_weekly_hours_min__c IS NOT NULL
        AND a.company_name__c ILIKE '%BAT%'
        AND LEFT(o.delivery_Area__c, 2) = 'de'
        AND effectivedate >= '2019-01-01'
        AND o.test__c = '0'
        AND a.test__c = '0'
        AND o.status IN( 'INVOICED',
        'PENDING TO START',
        'FULFILLED',
		  'CANCELLED CUSTOMER')
    GROUP BY
        TO_CHAR(o.effectivedate, 'YYYY-MM'),
        o.professional__c,
        a.hr_contract_weekly_hours_min__c,
		  cal.number_days_this_month) AS t1
GROUP BY
    t1.year_month,
    t1.prof,
    t1.working_hours,
    t1.worked_hours,
    t1.capped_work_hours),

-- Sick days and holidays for each cleaner by month
t2 AS(
SELECT

    TO_CHAR(hr.createddate, 'YYYY-MM') AS year_month,
    a.sfid AS prof,
    SUM(CASE WHEN hr.type__c IN ('sickness') THEN hr.days__c ELSE 0 END) AS sick_days,
    SUM(CASE WHEN hr.type__c IN ('holidays') THEN hr.days__c ELSE 0 END) AS holidays,
    MAX(a.hr_contract_weekly_hours_min__c) AS weekly_working_hours,
    
    CASE WHEN 
	 	((MAX(a.hr_contract_weekly_hours_min__c) / 5) * (SUM(CASE WHEN hr.type__c IN ('sickness') THEN hr.days__c ELSE 0 END) + SUM(CASE WHEN hr.type__c IN ('holidays') THEN hr.days__c ELSE 0 END))) IS NULL
	 THEN 0
	 ELSE  
	  	((MAX(a.hr_contract_weekly_hours_min__c) / 5) * (SUM(CASE WHEN hr.type__c IN ('sickness') THEN hr.days__c ELSE 0 END) + SUM(CASE WHEN hr.type__c IN ('holidays') THEN hr.days__c ELSE 0 END)))
	 END AS absence_hours
	 
FROM
	 salesforce.account a
    
LEFT JOIN salesforce.hr__c AS hr ON
    hr.account__c = a.sfid
WHERE
    description__c NOT ILIKE '%TEST%'
    AND hr.createddate >= '2019-01-01'
GROUP BY
    prof,
    year_month,
    a.hr_contract_weekly_hours_min__c
ORDER BY
    year_month)
    


SELECT

	block1.year_month,
	SUM(block1.total_working_hours) as total_working_hours,
	SUM(block1.total_worked_hours) as total_worked_hours,
	SUM(block1.absence_hours) as absence_hours,
	SUM(block1.absence_hours + block1.total_worked_hours) as all_hours,
	SUM(CASE WHEN (block1.absence_hours + block1.total_worked_hours) >= (block1.total_working_hours) THEN (block1.total_working_hours) ELSE (block1.absence_hours + block1.total_worked_hours) END) as capped_hours,
	SUM(CASE WHEN (block1.absence_hours + block1.total_worked_hours) >= (block1.total_working_hours) THEN (block1.total_working_hours) ELSE (block1.absence_hours + block1.total_worked_hours) END)/SUM(block1.total_working_hours) as ur,
	SUM(CASE WHEN (block1.total_worked_hours) >= (block1.total_working_hours - block1.absence_hours) THEN (block1.total_working_hours - block1.absence_hours) ELSE (block1.absence_hours + block1.total_worked_hours) END)/SUM(block1.total_working_hours) as ur2


FROM
	
	(SELECT
	    t1.year_month,
	    t1.prof,
	    -- CASE WHEN SUM(t1.working_hours) > 0 THEN  SUM(t1.capped_work_hours) / SUM(t1.working_hours) ELSE 0 END as ur,
	    SUM(t1.working_hours)::DECIMAL AS total_working_hours,
	    SUM(t1.worked_hours)::DECIMAL AS total_worked_hours,
	    SUM(CASE WHEN t2.absence_hours IS NULL THEN 0 ELSE t2.absence_hours END) as absence_hours
	    -- SUM(t1.worked_hours) - SUM(t2.absence_hours)::DECIMAL AS total_available_hours,
	    -- SUM(t1.worked_hours + t2.absence_hours) as all_hours
	    -- SUM(CASE WHEN (t1.worked_hours + t2.absence_hours) >= t1.working_hours THEN t1.working_hours ELSE (t1.worked_hours + t2.absence_hours) END) as capped_hours	   
	FROM
	
	    t1
	    
	LEFT JOIN 
		
		 t2 
	
	ON
	    t1.year_month = t2.year_month
	    AND t1.prof = t2.prof
	
	    
	GROUP BY
	
	    t1.year_month,
	    t1.prof
	    
	ORDER BY
	
	    t1.year_month) as block1
	
GROUP BY

	block1.year_month	
	
	
	
	
	
