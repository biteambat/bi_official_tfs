

SELECT

	o.served_by__c,
	COUNT(DISTINCT o.opportunityid) as opps_served_last_month,
	COUNT(DISTINCT inv.sfid) as invoices_last_month


FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.invoice__c inv
	
ON

	o.opportunityid = inv.opportunity__c
	
WHERE

	o.test__c IS FALSE
	AND inv.test__c IS FALSE
	AND LEFT(o.effectivedate::text, 7) = LEFT((current_date - interval '1 MONTH')::text,7)
	AND LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 MONTH')::text,7)
	AND inv.parent_invoice__c IS NULL
	

	
GROUP BY

	o.served_by__c

