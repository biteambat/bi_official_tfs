
SELECT

	TO_CHAR(o.effectivedate, 'YYYY-MM') AS year_month,
	
	SUM(CASE WHEN o."type" = 'cleaning-b2c' THEN o.order_duration__c ELSE 0 END) as hours_b2c,
	SUM(CASE WHEN o."type" = 'cleaning-b2b' THEN o.order_duration__c ELSE 0 END) as hours_b2b

  
FROM

  	salesforce.account a
  
LEFT JOIN 

	salesforce.order o 

ON

  a.sfid = o.professional__c
  
WHERE

  a.hr_contract_weekly_hours_min__c IS NOT NULL
  AND a.company_name__c ILIKE '%BAT%'
  AND LEFT(o.delivery_Area__c, 2) = 'de'
  AND effectivedate >= '2019-01-01'
  AND o.test__c = '0'
  AND a.test__c = '0'
  AND o.status IN( 'INVOICED', 'PENDING TO START', 'FULFILLED', 'CANCELLED CUSTOMER')
  
GROUP BY

  TO_CHAR(o.effectivedate, 'YYYY-MM')