
WITH cases AS
 (SELECT
	
		cas.opportunity__c,
		SUM(CASE WHEN cas.origin LIKE '%B2B customer%' 
		              OR cas.reason LIKE '%B2B customer%'
		              OR cas.reason LIKE 'Customer%'
		         THEN 1 
					ELSE 0 END) as cases_customer,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE 0 END) as feedbacks,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback: 1' THEN 1
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 2' THEN 2
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 3' THEN 3
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 4' THEN 4
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 5' THEN 5
		     ELSE 0
		     END) / SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE NULL END)::decimal as avg_feedback	
	FROM
	
		salesforce.case cas
		
	LEFT JOIN
	
		salesforce.opportunity o
		
	ON
	
		cas.opportunity__c = o.sfid
	
	WHERE
	
		o.stagename = 'WON'
		AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
		AND o.test__c IS FALSE
		
	GROUP BY	
	
		cas.opportunity__c)
		



SELECT

	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	o.closedate,
	t1.current_avg_traffic_light as current_avg_traffic_light,
	cases.cases_customer,
	cases.feedbacks,
	cases.avg_feedback

FROM 

	salesforce.opportunity o
	
LEFT JOIN

	bi.opportunity_traffic_light_tableau_v2 t1
	
ON

	o.sfid = t1.current_opportunity
	
LEFT JOIN

	cases
	
ON

	o.sfid = cases.opportunity__c
	
WHERE

	o.stagename = 'WON'
	AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	
GROUP BY
	
	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	o.closedate,
	t1.current_avg_traffic_light,
	cases.cases_customer,
	cases.feedbacks,
	cases.avg_feedback