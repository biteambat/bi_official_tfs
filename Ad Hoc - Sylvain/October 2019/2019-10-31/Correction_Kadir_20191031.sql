
-- Example of revenue loss for the current date


 SELECT
 
    current_date as today,
    TO_CHAR(current_date, 'YYYY-MM') AS year_month,
    SUM(CASE WHEN opps.status__c = 'OFFBOARDING' THEN 1 ELSE 0 END) AS no_opps_offboarding,
    SUM(CASE WHEN opps.status__c = 'RETENTION' THEN 1 ELSE 0 END) AS no_opps_retention,
    SUM(CASE WHEN opps.status__c = 'RUNNING' THEN 1 ELSE 0 END) AS no_opps_running,
    SUM(CASE WHEN opps.status__c = 'RETENTION' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * 4.3 * opps.hours_weekly__c ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS retention_revenue,
    SUM(CASE WHEN opps.status__c = 'OFFBOARDING' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * 4.3 * opps.hours_weekly__c ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS offboarding_revenue,
    SUM(CASE WHEN opps.status__c = 'RUNNING' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * 4.3 * opps.hours_weekly__c ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS running_revenue

FROM


   salesforce.opportunity opps
    
WHERE

	LEFT(opps.locale__c, 2) = 'de'
	AND opps.test__c IS FALSE

   
GROUP BY

    year_month
    
    