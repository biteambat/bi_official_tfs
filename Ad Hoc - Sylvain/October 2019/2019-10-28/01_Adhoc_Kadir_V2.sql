-- DROP TABLE IF EXISTS bi.test_kadir;

-- CREATE TABLE bi.test_kadir AS

-- The number of hours realized by cleaners, the number of cleaners of BAT in DE per month
-- The total number of cancelled orders by customers
-- Duration of cancelled orders by customers
-- Duration of cancelled orders by professinals

WITH cte1 AS(
SELECT
    sub.*,
    SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END):: DECIMAL AS cancelled_orders_by_prof,
    SUM(CASE WHEN o.status IN ('CANCELLED CUSTOMER') THEN 1 ELSE 0 END):: DECIMAL AS cancelled_orders_by_customers,
    SUM(CASE WHEN o.status IN ('CANCELLED CUSTOMER') THEN o.order_duration__c ELSE NULL END):: DECIMAL AS duration_cancelled_orders_by_customers,
    SUM(CASE WHEN o.status ILIKE ('CANCELLED PROFESSIONAL') THEN o.order_duration__c ELSE NULL END) AS not_realized_hours
    
FROM
    salesforce.order o
LEFT JOIN (
    
    SELECT
        TO_CHAR(effectivedate, 'YYYY-MM') AS year_month,
        COUNT(DISTINCT(o.professional__c)) AS active_cleaners,
        SUM(o.order_duration__c) AS realized_hours
      
    FROM
        salesforce.order o
    LEFT JOIN salesforce.account a ON
        a.sfid = o.professional__c
    WHERE
        a.company_name__c ILIKE '%BAT%'
        AND LEFT(o.delivery_Area__c, 2) = 'de'
        AND effectivedate >= '2019-01-01'
        AND o.test__c = '0'
        AND a.test__c = '0'
        AND o.status IN( 'INVOICED',
        'PENDING TO START',
        'FULFILLED' )

    GROUP BY
        year_month
        ) sub ON
    TO_CHAR(effectivedate, 'YYYY-MM') = sub.year_month
LEFT JOIN salesforce.account a ON
    a.sfid = o.professional__c
WHERE
    o.status IN ('CANCELLED PROFESSIONAL', 'CANCELLED CUSTOMER')
    AND a.company_name__c ILIKE '%BAT%'
    AND LEFT(o.delivery_Area__c, 2) = 'de'
    AND effectivedate >= '2019-01-01'
    AND o.test__c = '0'
    AND a.test__c = '0'
GROUP BY
    sub.year_month,
    sub.active_cleaners,
    sub.realized_hours),
    
-- the total minimum working hours of active cleaners
    
t1 AS (
SELECT
    t1.year_month,
    COUNT(DISTINCT t1.prof) AS active_cleaners,
    SUM(t1.total_min_working_hours*4.3) AS total_contract_hours,
    SUM(t1.total_min_working_hours)/ COUNT(DISTINCT t1.prof) AS hours_contract_per_cleaner
FROM
    (
    SELECT
        TO_CHAR(o.effectivedate, 'YYYY-MM') AS year_month,
        o.professional__c AS prof,
        a.hr_contract_weekly_hours_min__c AS total_min_working_hours
    FROM
        salesforce.account a
    LEFT JOIN salesforce.order o ON
        a.sfid = o.professional__c
    WHERE
        a.hr_contract_weekly_hours_min__c IS NOT NULL
        AND a.company_name__c ILIKE '%BAT%'
        AND LEFT(o.delivery_Area__c, 2) = 'de'
        AND effectivedate >= '2019-01-01'
        AND o.test__c = '0'
        AND a.test__c = '0'
        AND o.status IN( 'INVOICED',
        'PENDING TO START',
        'FULFILLED' )
    GROUP BY
        year_month,
        o.professional__c,
        a.hr_contract_weekly_hours_min__c) AS t1
GROUP BY
    t1.year_month),
    
-- The number of sickness days by cleaners per month
 sickness AS(
SELECT
    TO_CHAR(hr.createddate, 'YYYY-MM') AS year_month,
    hr.account__c AS cleaner,
    SUM(hr.days__c) AS sickness_days
FROM
    salesforce.hr__c AS hr
LEFT JOIN salesforce.order o ON
    hr.sfid = o.professional__c
WHERE
    hr.type__c = 'sickness'
    AND description__c NOT LIKE '%TEST%'
    AND description__c NOT LIKE '%test%'
    AND hr.createddate >= '2019-01-01'
GROUP BY
    cleaner,
    year_month )
    
SELECT
    cte1.year_month,
    cte1.active_cleaners,
    cte1.realized_hours,
    cte1.cancelled_orders_by_prof,
    cte1.cancelled_orders_by_customers,
    cte1.duration_cancelled_orders_by_customers,
    cte1.not_realized_hours,
    t1.total_contract_hours,
    t1.hours_contract_per_cleaner,
    (cte1.cancelled_orders_by_prof / cte1.active_cleaners)::DECIMAL AS cancelled_orders_per_active_cleaner,
    (cte1.realized_hours / cte1.active_cleaners) AS realized_hours_per_active_cleaner,
    SUM(sickness.sickness_days) / cte1.active_cleaners AS sickdays_per_active_cleaner,
    (cte1.realized_hours - cte1.not_realized_hours) / t1.total_contract_hours AS ur2,
	CASE
	    WHEN SUM(cte1.realized_hours) > 0 THEN SUM(CASE WHEN t1.total_contract_hours >= cte1.realized_hours THEN cte1.realized_hours ELSE t1.total_contract_hours END) / SUM(t1.total_contract_hours)
	    ELSE 0
	END AS ur
-- CASE
--    WHEN SUM(t1.total_contract_hours) > 0 THEN SUM(CASE WHEN cte1.realized_hours - (cte1.not_realized_hours + cte1.duration_cancelled_orders_by_customers) >= t1.total_contract_hours THEN t1.total_contract_hours ELSE cte1.realized_hours - (cte1.not_realized_hours + cte1.duration_cancelled_orders_by_customers) END)
--    / SUM(t1.total_contract_hours)
--    ELSE 0
-- END AS ur3

FROM
    cte1
JOIN sickness ON
    cte1.year_month = sickness.year_month
JOIN t1
ON cte1.year_month = t1.year_month
GROUP BY
    cte1.year_month,
    cte1.active_cleaners,
    cte1.realized_hours,
    cte1.cancelled_orders_by_prof,
    cte1.cancelled_orders_by_customers,
    cte1.duration_cancelled_orders_by_customers,
    cte1.not_realized_hours,
    t1.total_contract_hours,
    t1.hours_contract_per_cleaner
    
ORDER BY
    cte1.year_month;



