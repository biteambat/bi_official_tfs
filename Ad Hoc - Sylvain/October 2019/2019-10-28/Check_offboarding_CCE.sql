SELECT

	o.sfid,
	o.name,
	o.stagename,
	o.status__c,
	cont.status__c,
	cont.confirmed_end__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c cont
	
ON

	o.sfid = cont.opportunity__c
	
WHERE

	o.test__c IS FALSE
	AND o.status__c = 'OFFBOARDING'
	AND cont.confirmed_end__c < current_date
	AND cont.active__c IS TRUE
	
	
	
	
	
	