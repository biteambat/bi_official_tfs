SELECT
		
	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	CASE WHEN t2.sub_kpi_3 IS NULL THEN 'Partner' ELSE t2.sub_kpi_3 END as sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5,
	t2.opportunity__c,
	t2.opp_name,
	-- t2.first_invoice,
	-- (t2.amount_first_invoice)/1.19 as amount_first_invoice,
	-- t2.original_invoice__c,
	-- t2.number_invoices,
	-- SUM(t2.final_amount_invoiced)/1.19 - (t2.amount_first_invoice)/1.19 as correction,
	SUM(t2.final_amount_invoiced)/1.19 as value

FROM
	
	(SELECT	
		
		TO_CHAR(t1.date,'YYYY-MM') as date_part,
		MIN(t1.date) as date,
		t1.locale,
		t1.languages as languages,
		t1.city as city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		
		t1.name_first_invoice,
		t1.opportunity__c,
		t1.opp_name,
	
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c,
		-- t1.name_second_invoice,
		-- MAX(t1.date_second_invoice) as date_last_invoice,
		SUM(t1.first_amount)/COUNT(t1.first_invoice) as amount_first_invoice,
		SUM(t1.first_amount) as sum_first_amount,
		SUM(t1.second_amount) as sum_second_amount,
		COUNT(t1.first_invoice) as number_invoices,
		
		CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
		     ELSE MIN(t1.first_amount) + SUM(t1.second_amount)
			  END as final_amount_invoiced
			  
		-- CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
		--     ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
		--	  END as final_amount_invoiced
		
	FROM
	
		(SELECT

			-- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
			MIN(o.issued__c::date) as date,
			LEFT(o.locale__c,2) as locale,
			o.locale__c as languages,
			oooo.delivery_area__c as city,
			CAST('B2B' as varchar) as type,
			CAST('Revenue Adjusted' as varchar) as kpi,
			CAST('Netto' as varchar) as sub_kpi_1,
			CAST('Monthly' as varchar) as sub_kpi_2,
			op.provider as sub_kpi_3,
			o.service_type__c as sub_kpi_4,
			CAST('-' as varchar) as sub_kpi_5,
			o.sfid as first_invoice,
			o.name as name_first_invoice,			
			o.opportunity__c,
			oooo.name as opp_name,
			o.createddate as date_first_invoice,
			o.amount__c as first_amount,
			oo.original_invoice__c,
			oo.name as name_second_invoice,
			oo.createddate as date_second_invoice,
			oo.sfid as second_invoice,
			oo.amount__c as second_amount	
			
		FROM
			
			salesforce.invoice__c o
			
		LEFT JOIN
			
			salesforce.invoice__c oo
			
		ON
			
			o.sfid = oo.original_invoice__c
			
		LEFT JOIN
			
			salesforce.order ooo
			
		ON 
			
			o.opportunity__c = ooo.opportunityid

		LEFT JOIN
				
			salesforce.opportunity oooo
			
		ON 
		
			o.opportunity__c = oooo.sfid
			
		LEFT JOIN

			bi.order_provider op
			
		ON
		
			oooo.sfid = op.opportunityid
			
		WHERE
			
			o.opportunity__c IS NOT NULL
			AND o.test__c IS FALSE
			AND o.issued__c IS NOT NULL
			AND o.original_invoice__c IS NULL

			-- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
			
		GROUP BY
			
			-- TO_CHAR(o.issued__c,'YYYY-MM'),
			LEFT(o.locale__c,2),
			o.locale__c,
			oooo.delivery_area__c,
			o.name,
			o.opportunity__c,
			oooo.name,
			o.issued__c,
			o.createddate,
			o.sfid,
			o.amount__c,
			oo.original_invoice__c,
			oo.name,
			oo.createddate,
			oo.sfid,
			oo.amount__c,
			op.provider,
			o.service_type__c
			
		ORDER BY
		
			date desc) as t1
			
			
	GROUP BY
	
		TO_CHAR(t1.date,'YYYY-MM'),
		t1.locale,
		t1.languages,
		t1.city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		t1.name_first_invoice,
		t1.opportunity__c,
		t1.opp_name,
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c) as t2
		
		
GROUP BY

	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	t2.sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5,
	t2.opportunity__c,
	t2.opp_name
	-- t2.first_invoice,
	-- t2.amount_first_invoice,
	-- t2.number_invoices,
	-- t2.original_invoice__c
	
ORDER BY

	date_part desc;