



SELECT

	o.name,
	inv.customer__c,
	inv.opportunity__c,
	inv.issued__c,
	inv.amount__c,
	inv.amount_paid__c,
	inv.parent_invoice__c,
	inv.original_invoice__c,
	inv.sfid

FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	

WHERE

	inv.original_invoice__c = 'a000J00000yuETcQAM'
	OR inv.sfid = 'a000J00000yuETcQAM'
	
ORDER BY

	inv.issued__c desc