 (SELECT

 inv.issued__c,
 inv.opportunity__c,
 o.name,
 inv.sfid,
 inv.name,
 inv.balance__c,
 inv.amount__c,
 inv.service_type__c

FROM

    salesforce.invoice__c inv
    
LEFT JOIN

	salesforce.opportunity o
	
ON
	
	inv.opportunity__c = o.sfid
    
WHERE

	inv.test__c IS FALSE
	AND inv.balance__c > 0
	AND inv.opportunity__c IS NOT NULL
	AND inv.test__c IS FALSE)