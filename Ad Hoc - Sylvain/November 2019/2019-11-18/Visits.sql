


SELECT

	ev.activitydate,
	ev.whatid as opportunity,
	o.name,
	ev.subject,
	ev.ownerid,
	ev.event_reason__c,
	ev.description

FROM

	salesforce.event ev
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	ev.whatid = o.sfid
	
WHERE

	ev.whatid LIKE '006%'
	AND ev.event_reason__c LIKE 'Visit%'
	
