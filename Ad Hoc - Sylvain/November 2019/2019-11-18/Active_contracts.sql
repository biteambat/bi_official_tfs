

SELECT

	c.opportunity__c,
	o.name,
	c.name,
	c.status__c,
	c.effective_start__c,
	c.confirmed_end__c,
	c.service_type__c,
	c.grand_total__c,
	c.pph__c

FROM

	salesforce.contract__c c
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	c.opportunity__c = o.sfid
	
WHERE

	c.test__c IS FALSE
	AND c.active__c IS TRUE
	AND c.opportunity__c IS NOT NULL
	