

SELECT

	inv.sfid,
	inv.name,
	o.name,
	inv.amount__c,
	inv.amount_paid__c,
	inv.balance__c,
	inv.amount__c + inv.amount_paid__c as rest_to_pay
	
FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND inv.opportunity__c IS NOT NULL
	AND inv.balance__c <> ROUND(inv.amount__c + inv.amount_paid__c)
