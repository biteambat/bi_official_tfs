


SELECT

	'Extra Services Last Month' as kpi,
	inv.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END as grand_total,
	SUM(inv.amount__c) as revenue_last_month



FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND CASE WHEN RIGHT(current_date::text, 2)::integer < 10
	         THEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - '2 MONTH'::INTERVAL)::text, 7) 
	         ELSE LEFT(inv.issued__c::text, 7) = LEFT((current_date - '1 MONTH'::INTERVAL)::text, 7) 
	         END 
	AND inv.parent_invoice__c IS NULL
	
GROUP BY

	inv.opportunity__c,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END
	
	