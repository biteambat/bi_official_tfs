	
	
SELECT

	t1.sfid,
	t1.name,
	COUNT(t1.oldvalue) as number_renewed

FROM	
	
	(SELECT
	
		hi.parentid,
		hi.field,
		hi.oldvalue,
		hi.newvalue,
		o.sfid,
		o.name
	
	FROM
	
		contract__history hi
		
	LEFT JOIN
	
		salesforce.contract__c cont
		
	ON
	
		hi.parentid = cont.sfid
		
	LEFT JOIN
	
		salesforce.opportunity o
		
	ON
	
		cont.opportunity__c = o.sfid
		
	WHERE
	
		hi.field = 'end__c'
		AND hi.oldvalue IS NOT NULL
		AND cont.service_type__c = 'maintenance cleaning'
		AND o.test__c IS FALSE
		AND cont.test__c IS FALSE
		AND hi.newvalue IS NOT NULL) as t1
		
GROUP BY

	t1.sfid,
	t1.name
	
	
	
	
	
	
	
	
