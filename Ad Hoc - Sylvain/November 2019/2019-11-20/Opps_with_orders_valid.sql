


SELECT

	TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
	COUNT(DISTINCT o.opportunityid) as active_opps


FROM

	salesforce."order" o
	
WHERE

	o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
	-- AND o.service_type__c = 'maintenance cleaning'
	
GROUP BY

	TO_CHAR(o.effectivedate, 'YYYY-MM')