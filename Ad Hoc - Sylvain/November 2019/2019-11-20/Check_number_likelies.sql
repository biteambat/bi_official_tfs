


SELECT

	o.*
	-- TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	-- COUNT(o.sfid) as likelies_created,
	-- o.sfid,
	-- o.name


FROM

	salesforce.likeli__c o
	
WHERE

	o.type__c = 'B2B'
	AND o.acquisition_channel__c IN ('inbound', 'web')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.test__c IS FALSE
	AND (o.acquisition_tracking_id__c NOT LIKE '%raffle%' OR o.acquisition_tracking_id__c IS NULL)
	-- AND o.acquisition_tracking_id__c NOT LIKE '%news%'
	-- AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
	AND (o.acquisition_channel__c NOT LIKE 'outbound' OR o.acquisition_channel__c IS NULL)
	AND (o.company_name__c NOT LIKE '%test%' OR o.company_name__c IS NULL OR o.company_name__c NOT LIKE '%bookatiger%')
	AND (o.email__c NOT LIKE '%bookatiger%' OR o.email__c IS NULL)
	AND TO_CHAR(o.createddate, 'YYYY-MM') = '2019-11'
	-- AND o.name IN ('LK-000165651', 'LK-000165653', 'LK-000165654', 'LK-000165655')
	


ORDER BY

	TO_CHAR(o.createddate, 'YYYY-MM') desc
	
	
	