



SELECT

	cont.sfid as sfid_contract,
	cont.opportunity__c,
	o.name,
	o.stagename,
	o.shippingaddress_city__c,
	cont.service_type__c,
	cont.duration__c,
	cont.end__c,
	cont.notice__c,
	cont.notice_date__c,
	cont.status__c,
	cont.grand_total__c,
	o.potential_partner_costs__c,
	CASE WHEN cont.grand_total__c <> 0 THEN (cont.grand_total__c - o.potential_partner_costs__c)/cont.grand_total__c ELSE NULL END as potential_gpm,
	pot.potential as potential_last_3months,
	cont.pph__c,
	cont.hours_weekly__c,
	cont.recurrency__c as recurrency_contract,
	o.recurrency__c as recurrency_opp,
	cont.frequency__c,
	o.served_by__c,
	a.name as name_partner,
	prov.professional__c,
	prov.pro_name as last_cleaner,
	prov.provider


FROM

	salesforce.contract__c cont
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	cont.opportunity__c = o.sfid
	
LEFT JOIN

	salesforce.account a
	
ON

	o.served_by__c = a.sfid
	
LEFT JOIN

	bi.order_provider prov
	
ON

	cont.opportunity__c = prov.opportunityid
	
LEFT JOIN

	bi.potential_revenue_per_opp pot
	
ON

	cont.opportunity__c = pot.opportunityid
	
WHERE

	cont.test__c IS FALSE
	AND cont.service_type__c = 'maintenance cleaning'
	AND cont.active__c IS TRUE
	AND ((cont.duration__c IS NULL AND cont.end__c IS NULL) OR cont.duration__c IS NOT NULL)
	AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')
	