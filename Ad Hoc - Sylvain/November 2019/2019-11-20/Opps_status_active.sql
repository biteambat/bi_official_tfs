


SELECT

	o.sfid,
	o.name,
	o.delivery_area__c,
	o.stagename,
	o.status__c

FROM

	salesforce.opportunity o
	
	
WHERE

	o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	AND LEFT(o.delivery_area__c, 2) = 'de'
	AND o.test__c IS FALSE
	AND o.stagename IN ('WON', 'PENDING')