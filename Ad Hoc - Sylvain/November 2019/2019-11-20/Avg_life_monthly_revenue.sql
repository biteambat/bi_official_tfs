


SELECT

	inv.opportunity__c,
	SUM(inv.amount__c) as total_life_amount_brutto,
	COUNT(DISTINCT LEFT(inv.issued__c::text, 7)) as months_invoiced,
	SUM(inv.amount__c)/COUNT(DISTINCT LEFT(inv.issued__c::text, 7))/1.19 as avg_monthly_amount_netto
	
FROM

	salesforce.invoice__c inv
	
WHERE

	inv.test__c IS FALSE
	AND inv.original_invoice__c IS NULL
	AND inv.opportunity__c IS NOT NULL
	
GROUP BY

	inv.opportunity__c
	
	
	
	