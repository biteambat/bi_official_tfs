SELECT

	t4.year_month,
	MIN(t4.mindate) as mindate,
	SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as oldvalue,
	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) as newvalue,

	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) - SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as variation

FROM
			
	(SELECT
	
		t1.year_month,
		t1.opportunityid,
		t1.mindate,
		t1.maxdate,
		t2.oldvalue,
		t3.newvalue
				
	FROM
	
		
		(SELECT -- Here I create the list of first and last changes, every month, for every opportunity
			
			TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
			MIN(o.createddate) as mindate,
			MAX(o.createddate) as maxdate,
			o.opportunityid
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('1')
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_month,
			o.opportunityid
			
		ORDER BY
		
			year_month desc) as t1
			
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every month
			
			TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_month desc) as t2
		
	ON
	
		t1.mindate = t2.createddate
		AND t1.opportunityid = t2.opportunityid
	
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every month
			
			TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_month desc) as t3
		
	ON
	
		t1.maxdate = t3.createddate
		AND t1.opportunityid = t3.opportunityid
		
	ORDER BY
	
		t1.year_month desc) as t4
		
GROUP BY

	t4.year_month
	
ORDER BY 

	t4.year_month desc;