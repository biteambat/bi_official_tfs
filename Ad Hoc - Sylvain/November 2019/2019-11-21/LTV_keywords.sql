	
SELECT

	o.year_month,
	MIN(o.date) as mindate,
	o.locale__c,
	o.city,
	t3.opportunityid,
	t3.name,
	o.keyword,
	COUNT(DISTINCT o.sfid_likeli) as leads_created,
	SUM(CASE WHEN o.converted_in_opportunity = 'Yes' THEN 1 ELSE 0 END) as opps_created,
	SUM(CASE WHEN o.stage__c IN ('ENDED') THEN 1 ELSE 0 END) as lead_ended,
	SUM(CASE WHEN o.stage__c NOT IN ('ENDED', 'WON', 'PENDING') THEN 1 ELSE 0 END) as pipeline,
	SUM(CASE WHEN o.stage_opp IN ('WON', 'PENDING') THEN 1 ELSE 0 END) as customer_converted,
	MAX(o.spending) as spending,
	SUM(o.grand_total__c) as grand_total,
	t3.sub_kpi_2 as age_churn,
	t3.opportunityid,
	t3.name,
	t3.date_start,
	t3.date_churn

FROM
	
	(SELECT
	
		TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
		TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as date,
		ooo.sfid as opp_id,
		ooo.name,
		oo.working_day_number,
		oo.non_working_day_number,
		oo.day_type,
		oo.day_type_number,
		oo.number_working_days_in_month,
		o.createddate,
		o.sfid as sfid_likeli,
		CASE WHEN o.lost_reason__c LIKE 'invalid%'
		          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
		     ELSE 
		     	    'Valid'
		     END as validity,
		CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
		CASE WHEN o.opportunity__c IS NULL THEN 'No' ELSE 'Yes' END as converted_in_opportunity,
		ooo.stagename as stage_opp,
		o.lost_reason__c,
		o.locale__C,
		LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
		o.contact_name__c,
		o.acquisition_channel__c,
		o.acquisition_tracking_id__c,
		lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as keyword,
		t1.spending,
		o.customer__c,
		o.stage__c,
		o.type__c,
		o.company_name__c,
		o.opportunity__c,
		o.ownerid,
		u.name as owner_name,
		o.total_calls__c,
		o.delivery_area__c,
		o.grand_total__c		
		
	FROM
	
		salesforce.likeli__c o
		
	LEFT JOIN
	
		bi.working_days_monthly oo
		
	ON
	
		TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
		
	LEFT JOIN
	
		salesforce.opportunity ooo
		
	ON
	
		o.opportunity__c = ooo.sfid
	
	LEFT JOIN
	
		salesforce.user u
		
	ON
	
		o.ownerid = u.sfid
		
	LEFT JOIN
	
		(SELECT
	
			TO_CHAR(o.start_date, 'YYYY-MM') as year_month,
			o.sub_kpi_3 as keyword,
			o.city,
			SUM(o.value) as spending
			
			
		
		FROM
		
			public.marketing_spending o
			
		GROUP BY
		
			year_month,
			city,
			keyword
			
		ORDER BY
		
			year_month desc,
			keyword) as t1
	
	ON
	
		TO_CHAR(o.createddate, 'YYYY-MM') = t1.year_month
		AND lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) = t1.keyword
		AND LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) = t1.city
		
	WHERE
	
		o.type__c = 'B2B'
		AND o.acquisition_channel__c IN ('inbound', 'web')
		AND LEFT(o.locale__c, 2) = 'de'
		AND o.test__c IS FALSE
		AND (o.acquisition_tracking_id__c NOT LIKE '%raffle%' OR o.acquisition_tracking_id__c NOT LIKE '%news%' OR o.acquisition_tracking_id__c IS NULL)
		-- AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
		AND (o.acquisition_channel__c NOT LIKE 'outbound' OR o.acquisition_channel__c IS NULL)
		AND (o.company_name__c NOT LIKE '%test%' OR o.company_name__c IS NULL OR o.company_name__c NOT LIKE '%bookatiger%')
		AND (o.email__c NOT LIKE '%bookatiger%' OR o.email__c IS NULL)
		
	ORDER BY
	
		date desc) as o
		
LEFT JOIN

	(SELECT
	
		TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
		MIN(table1.date_churn::date) as date,
		LEFT(table1.country,2) as locale,
		table1.locale__c as languages,
		-- CAST('-' as varchar) as city,
		table1.delivery_area__c as city,
		CAST('B2B' as varchar) as type,
		CAST('Churn Beta' as varchar) as kpi,
		CAST('Count opps' as varchar) as sub_kpi_1,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
				  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
				  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
				  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
				  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
				  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
				  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
				  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
				  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
				  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
				  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
				  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
				  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
				  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
				  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
				  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
				  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
				  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
				  ELSE '>M18'
				  END as sub_kpi_2,
		
		CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
			  
		CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
		CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
		table1.opportunityid,
		table1.name,
		table1.date_start,
		table1.date_churn,
		COUNT(DISTINCT table1.opportunityid) as value
	
	FROM
		
		(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
		         -- It's the last day on which they are making money
			LEFT(oo.locale__c, 2) as country,
			oo.locale__c,
			oo.delivery_area__c,
			o.opportunityid,
			oo.name,
			ct.name as name_closer,
			oo.closed_by__c as closed_by,
			oo.ownerid,
			usr.name as name_owner,
			ooo.potential as grand_total,
			'last_order' as type_date,
			MIN(o.effectivedate) as date_start,
			MAX(t4.date_churn) as date_churn
			
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			o.opportunityid = oo.sfid
			
		LEFT JOIN
			
			salesforce.user ct
			
		ON
		
			oo.closed_by__c = ct.sfid
			
		LEFT JOIN
			
			salesforce.user usr
			
		ON
		
			oo.ownerid = usr.sfid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp ooo
			
		ON
		
			o.opportunityid = ooo.opportunityid
			
		LEFT JOIN
		
			(SELECT
			
				t3.*,
				CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
				CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
			
			FROM	
				
				(SELECT
				
					t1.country,
					t1.opportunityid,
					t1.date_last_order,
					t2.date_churn as contract_end
				
				FROM
				
					(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
					         -- It's the last day on which they are making money
						LEFT(o.locale__c, 2) as country,
						o.opportunityid,
						'last_order' as type_date,
						MAX(o.effectivedate) as date_last_order
						
					FROM
					
						salesforce.order o
						
					LEFT JOIN
					
						salesforce.opportunity oo
						
					ON 
					
						o.opportunityid = oo.sfid
						
					LEFT JOIN
	
						salesforce.contract__c cont
						
					ON
					
						oo.sfid = cont.opportunity__c
						
					WHERE
					
						o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
						AND oo.status__c IN ('RESIGNED', 'CANCELLED')
						AND o.type = 'cleaning-b2b'
						AND o.professional__c IS NOT NULL
						AND o.test__c IS FALSE
						AND oo.test__c IS FALSE
						AND cont.service_type__c = 'maintenance cleaning'
					
					GROUP BY
					
						LEFT(o.locale__c, 2),
						type_date,
						o.opportunityid) as t1
						
				LEFT JOIN
				
					(SELECT -- Here we make the list of opps with their confirmed end or end date when available
											
						o.opportunity__c as opportunityid,
						MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
					
					FROM
					
						salesforce.contract__c o
					
					WHERE
					
						o.test__c IS FALSE
						AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
						AND o.service_type__c LIKE 'maintenance cleaning'
						-- AND o.active__c IS TRUE 
						-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
						
					GROUP BY
					
						o.opportunity__c) as t2 
				
				ON
				
					t1.opportunityid = t2.opportunityid) as t3) as t4
					
		ON
		
			o.opportunityid = t4.opportunityid
			
		WHERE
		
			o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.status__c IN ('RESIGNED', 'CANCELLED')
			AND oo.test__c IS FALSE
			AND o.test__c IS FALSE
			AND o.professional__c IS NOT NULL
		
		GROUP BY
		
			LEFT(oo.locale__c, 2),
			oo.locale__c,
			oo.delivery_area__c,
			ct.name,
			oo.closed_by__c,
			oo.ownerid,
			oo.name,
			usr.name,
			type_date,
			grand_total,
			o.opportunityid) as table1
			
	GROUP BY
	
		TO_CHAR(table1.date_churn, 'YYYY-MM'),
		table1.country,
		table1.locale__c,
		table1.delivery_area__c,
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table1.opportunityid,
		table1.name,
		table1.date_start,
		table1.date_churn
		
	ORDER BY
	
		TO_CHAR(table1.date_churn, 'YYYY-MM') desc) as t3
	
ON

	o.opp_id = t3.opportunityid
	
WHERE

	t3.opportunityid IS NOT NULL
		
GROUP BY

	o.year_month,
	o.locale__c,
	o.city,
	o.keyword,
	t3.sub_kpi_2,
	t3.opportunityid,
	t3.name,
	t3.date_start,
	t3.date_churn
	
ORDER BY
	
	year_month desc;