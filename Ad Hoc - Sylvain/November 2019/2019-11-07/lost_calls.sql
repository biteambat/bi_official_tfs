


SELECT

	nat.opportunity__c,
	COUNT(DISTINCT nat.sfid) as number_lost_calls
	
FROM

	salesforce.natterbox_call_reporting_object__c nat
	
WHERE

	-- nat.opportunity__c IS NOT NULL
	nat.calldirection__c = 'Inbound'
	AND (nat.callconnectedcheckbox__c IS FALSE
	    OR nat.callconnected__c = 'No')
	    
GROUP BY

	nat.opportunity__c