
WITH cases AS
 (SELECT
	
		cas.opportunity__c,
		SUM(CASE WHEN cas.origin LIKE '%B2B customer%' 
		              OR cas.reason LIKE '%B2B customer%'
		              OR cas.reason LIKE 'Customer%'
		         THEN 1 
					ELSE 0 END) as cases_opp,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE 0 END) as feedbacks,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback: 1' THEN 1
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 2' THEN 2
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 3' THEN 3
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 4' THEN 4
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 5' THEN 5
		     ELSE 0
		     END) / SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE NULL END)::decimal as avg_feedback	
	FROM
	
		salesforce.case cas
		
	LEFT JOIN
	
		salesforce.opportunity o
		
	ON
	
		cas.opportunity__c = o.sfid
	
	WHERE
	
		o.stagename = 'WON'
		AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
		AND o.test__c IS FALSE
		
	GROUP BY	
	
		cas.opportunity__c),
		
owners AS (SELECT
			
				o.opportunityid,	
				COUNT(o.newvalue) as number_owners
			
			FROM
			
				salesforce.opportunityfieldhistory o
				
			WHERE
			
				o.field = 'Owner'
				
			GROUP BY
			
				o.opportunityid),
				
cleaners AS (SELECT
				
					o.opportunityid,
					COUNT(DISTINCT o.professional__c) as number_cleaners
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.test__c IS FALSE
					AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
					
				GROUP BY
				
					o.opportunityid),
					
canc_cust AS (SELECT
				
					o.opportunityid,
					COUNT(DISTINCT o.sfid) as canc_cust
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.test__c IS FALSE
					AND o."status" IN ('CANCELLED CUSTOMER')
					
				GROUP BY
				
					o.opportunityid)
		

SELECT

	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	o.closedate,
	o.traffic_light__c,
	cases.cases_opp,
	cases.feedbacks,
	cases.avg_feedback,
	owners.number_owners,
	cleaners.number_cleaners,
	canc_cust.canc_cust

FROM 

	salesforce.opportunity o
	
LEFT JOIN

	cases
	
ON

	o.sfid = cases.opportunity__c
	
LEFT JOIN

	owners
	
ON

	o.sfid = owners.opportunityid
	
LEFT JOIN

	cleaners
	
ON

	o.sfid = cleaners.opportunityid
	
LEFT JOIN

	canc_cust
	
ON 

	o.sfid = canc_cust.opportunityid
	
WHERE

	o.stagename = 'WON'
	AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	
GROUP BY
	
	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	o.closedate,
	o.traffic_light__c,
	cases.cases_opp,
	cases.feedbacks,
	cases.avg_feedback,
	owners.number_owners,
	cleaners.number_cleaners,
	canc_cust.canc_cust
	
	
	
	
	