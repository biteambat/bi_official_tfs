
-- Performance of Sales Managers

-- Desc: Weekly/monthly performance tracking of sales managers based on working days and non-working days

------------------------------------------------------------------------
-- # of leads assigned(received) 
-- # of leads qualified
-- # of leads not reached
-- # of invalid leads
-- % of invalid leads: SUM(Invalid)/Leads Received
-- # of suitable leads(valid)
-- # of leads won(leads to opps)
-- % Lead-to-opp (CVR1): SUM(leads_converted_opps) / SUM(leads_suitable)
-- % Lead-to-customer(CVR2): SUM(Leads Won) / SUM(leads_suitable)

WITH leads AS(

SELECT
    TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS leads_created_date,
    u.name AS owner_name,
    COUNT(li.sfid) AS leads_assigned,
    SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' AND li.lost_reason__c NOT LIKE 'not suitable%' OR li.lost_reason__c IS NULL THEN 1 ELSE 0 END) AS leads_suitable,
    SUM(CASE WHEN li.lost_reason__c LIKE 'invalid%' OR li.lost_reason__c LIKE 'not suitable%' THEN 1 ELSE 0 END) AS leads_invalid,
    SUM(CASE WHEN li.stage__c = 'QUALIFIED' THEN 1 ELSE 0 END) AS leads_qualified,
--    SUM(CASE WHEN li.direct_relation__c IS FALSE THEN 1 ELSE NULL END) AS leads_qualified,
    SUM(CASE WHEN li.stage__c = 'NOT REACHED' THEN 1 ELSE 0 END) AS leads_not_reached,
    SUM(CASE WHEN li.opportunity__c IS NOT NULL THEN 1 ELSE 0 END) AS leads_converted_opps
--    CAST(SUM(CASE WHEN li.opportunity__c IS NOT NULL THEN 1 ELSE 0 END) AS NUMERIC) / CAST(NULLIF(SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' AND li.lost_reason__c NOT LIKE 'not suitable%' OR li.lost_reason__c IS NULL THEN 1 ELSE 0 END), 0) AS NUMERIC) AS cvr1,
--    SUM(t1.leads_won) / NULLIF(SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' AND li.lost_reason__c NOT LIKE 'not suitable%' OR li.lost_reason__c IS NULL THEN 1 ELSE 0 END), 0) AS cvr2
    
FROM
    salesforce.likeli__c li
LEFT JOIN salesforce.user u ON
    li.ownerid = u.sfid

WHERE
    li.type__c = 'B2B'
    AND li.test__c IS FALSE
    AND li.company_name__c NOT LIKE '%test%'
    AND li.name NOT LIKE '%test%'
GROUP BY
    TO_CHAR(li.createddate, 'YYYY-MM'),
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
    u.name
ORDER BY
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE DESC),

    
-- # of closed deals (leads won)
-- # of lost_deals
-- total # of calls
-- # of connected_calls
-- sum of call duration(min)
-- avg of call duration(min)
-- avg ring duration(min)
calls AS(

SELECT 
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM') AS year_month,
    opps.closedate::DATE AS closedate,
    u.name AS owner_name,
    LEFT(li.locale__c, 2) AS locale,
    li.delivery_area__c,
    CASE WHEN opps.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
    SUM(CASE WHEN opps.stagename IN ('WON', 'PENDING') THEN 1 ELSE 0 END) AS closed_deals,
    SUM(CASE WHEN opps.stagename IN ('LOST', 'CONTRACT FAILED') THEN 1 ELSE 0 END) AS lost_deals,
    MAX(total_calls) AS total_calls,
    MAX(connected_calls) AS connected_calls,
    sum_call_duration,
    avg_call_duration,
    avg_ring_duration


FROM salesforce.likeli__c li
JOIN salesforce.opportunity opps ON
li.opportunity__c = opps.sfid

LEFT JOIN salesforce.user u ON
li.ownerid = u.sfid

LEFT JOIN (
SELECT
    TO_CHAR(t1.date::DATE, 'YYYY-MM') AS year_month,
    t1.date AS calldate,
    t1.name AS owner_name,
    SUM(t1.total_calls) AS total_calls,
    SUM(t1.connected_calls) AS connected_calls,
    SUM(t1.lost_calls) AS lost_calls,
    TO_CHAR((t1.sum_call_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS sum_call_duration,
    TO_CHAR((t1.avg_call_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS avg_call_duration,
    TO_CHAR((t1.avg_ring_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS avg_ring_duration
FROM
    (
    SELECT
        u.name,
        nc.systemmodstamp::DATE AS DATE,
        COUNT(*) AS total_calls,
        SUM(CASE WHEN nc.callconnected__c = 'Yes' THEN 1 ELSE 0 END) AS connected_calls,
        SUM(CASE WHEN nc.callconnected__c = 'No' AND nc.callconnectedcheckbox__c IS FALSE THEN 1 ELSE 0 END) AS lost_calls,
        SUM(nc.calltalkseconds__c) AS sum_call_duration_sec,
        AVG(nc.calltalkseconds__c) AS avg_call_duration_sec,
        AVG(nc.callringseconds__c) AS avg_ring_duration_sec
    FROM
        salesforce.natterbox_call_reporting_object__c nc
    JOIN salesforce.user u ON
        nc.ownerid = u.sfid
    WHERE
        nc.systemmodstamp > '2018-01-01'
        AND nc.wrapup_string_1__c LIKE '%B2B%'
    GROUP BY
        u.name,
        DATE) AS t1
GROUP BY
    owner_name,
    year_month,
    calldate,
    sum_call_duration,
    avg_call_duration,
    avg_ring_duration
ORDER BY
    calldate DESC ) t2 ON
    
t2.owner_name = u.name
AND TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD') = TO_CHAR(t2.calldate::DATE, 'YYYY-MM-DD')

WHERE
    opps.closedate <= CURRENT_DATE
    AND li.test__c IS FALSE
    AND li.type__c = 'B2B'
    AND opps.test__C IS FALSE
    AND total_calls IS NOT NULL
GROUP BY
    year_month,
    locale,
    li.delivery_area__c,
    u.name,
    opps.closedate,
    acquisition_type,
    sum_call_duration,
    avg_call_duration,
    avg_ring_duration
ORDER BY
    opps.closedate::DATE DESC
),    


-- productive days(number of days worked)
-- % productivity (productive days / working days in the month)

working_days AS (
SELECT
    t3.year_month,
    t3.leads_created_date,
    t3.owner_name,
    t3.number_working_days_in_month,
    MAX(t3.working_day_number) AS worked_days
FROM
    (
    SELECT
        u.name AS owner_name,
        TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS leads_created_date,
        wd.working_day_number,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month
    FROM
        salesforce.likeli__c li
    LEFT JOIN bi.working_days_monthly wd ON
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE = wd.date
    LEFT JOIN salesforce.user u ON
        li.ownerid = u.sfid
    WHERE
        li.type__c = 'B2B'
        AND li.test__c IS FALSE
        AND li.company_name__c NOT LIKE '%test%'
        AND li.name NOT LIKE '%test%'

        GROUP BY u.name,
        TO_CHAR(li.createddate, 'YYYY-MM'),
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
        wd.working_day_number,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month
    ORDER BY
        TO_CHAR(li.createddate, 'YYYY-MM') DESC) t3
GROUP BY
    t3.year_month,
    t3.leads_created_date,
    t3.owner_name,
    t3.number_working_days_in_month
ORDER BY
    t3.leads_created_date DESC )


SELECT
    calls.year_month,
    calls.closedate AS date,
    calls.owner_name,
    calls.locale,
    calls.delivery_area__c,
    calls.acquisition_type,
    leads.leads_assigned,
    leads.leads_suitable,
    leads.leads_invalid,
    leads.leads_qualified,
    leads.leads_not_reached,
    leads.leads_converted_opps,
    SUM(leads.leads_converted_opps) / NULLIF(SUM(leads.leads_suitable), 0) AS cvr1,
    SUM(calls.closed_deals) / NULLIF(SUM(leads.leads_suitable), 0) AS cvr2,
    calls.closed_deals,
    calls.lost_deals,
    calls.total_calls,
    calls.connected_calls,
    calls.total_calls / NULLIF((calls.closed_deals), 0) AS calls_to_deals,
    calls.sum_call_duration,
    calls.avg_call_duration,
    calls.avg_ring_duration,
    working_days.number_working_days_in_month,
    CASE WHEN calls.total_calls IS NOT NULL THEN working_days.worked_days ELSE 0 END AS productive_days,
    ROUND((working_days.worked_days::NUMERIC / working_days.number_working_days_in_month::NUMERIC), 2) AS productivity

FROM calls 
LEFT JOIN working_days ON 
calls.owner_name = working_days.owner_name
AND calls.closedate = working_days.leads_created_date
LEFT JOIN leads ON
leads.owner_name = calls.owner_name
AND leads.leads_created_date = calls.closedate

GROUP BY
    leads.leads_assigned,
    leads.leads_suitable,
    leads.leads_invalid,
    leads.leads_qualified,
    leads.leads_not_reached,
    leads.leads_converted_opps,
    calls.year_month,
    calls.closedate,
    calls.owner_name,
    calls.locale,
    calls.delivery_area__c,
    calls.acquisition_type,
    calls.closed_deals,
    calls.lost_deals,
    calls.total_calls,
    calls.connected_calls,
    calls.sum_call_duration,
    calls.avg_call_duration,
    calls.avg_ring_duration,
    working_days.worked_days,
    working_days.number_working_days_in_month

ORDER BY
    calls.closedate DESC
    
    








