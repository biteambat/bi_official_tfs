

-- Performance of Sales Managers

-- Leads metrics monthly
-- # of leads assigned(received) 
-- # of leads qualified
-- # of leads not reached
-- # of invalid leads
-- % of invalid leads: SUM(Invalid)/Leads Received
-- # of suitable leads(valid)
-- # of leads won(leads to opps)
-- % Lead-to-opp (CVR1): SUM(leads_converted_opps) / SUM(leads_suitable)
-- % Lead-to-customer(CVR2): SUM(Leads Won) / SUM(leads_suitable)

WITH leads AS(

SELECT
    TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
    COUNT(li.sfid) AS leads_assigned,
    SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' AND li.lost_reason__c NOT LIKE 'not suitable%' OR li.lost_reason__c IS NULL THEN 1 ELSE 0 END) AS leads_suitable,
    SUM(CASE WHEN li.lost_reason__c LIKE 'invalid%' OR li.lost_reason__c LIKE 'not suitable%' THEN 1 ELSE 0 END) AS leads_invalid,
    SUM(CASE WHEN li.stage__c = 'QUALIFIED' THEN 1 ELSE NULL END) AS leads_qualified,
--    SUM(CASE WHEN li.direct_relation__c IS FALSE THEN 1 ELSE NULL END) AS leads_qualified,
    SUM(CASE WHEN li.stage__c = 'NOT REACHED' THEN 1 ELSE 0 END) AS leads_not_reached,
    SUM(t1.leads_won) AS leads_won,
    SUM(CASE WHEN li.opportunity__c IS NOT NULL THEN 1 ELSE 0 END) AS leads_converted_opps,
    CAST(SUM(CASE WHEN li.opportunity__c IS NOT NULL THEN 1 ELSE 0 END) AS NUMERIC) / CAST(SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' OR li.lost_reason__c NOT LIKE 'not suitable%' THEN 1 ELSE 0 END) AS NUMERIC) AS cvr1,
    SUM(t1.leads_won) / SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' OR li.lost_reason__c NOT LIKE 'not suitable%' THEN 1 ELSE 0 END) AS cvr2
    
FROM
    salesforce.likeli__c li
LEFT JOIN (
    SELECT
        opps.sfid,
        SUM(CASE WHEN opps.stagename IN ('WON', 'PENDING') THEN 1 ELSE 0 END) AS leads_won
    FROM
        salesforce.opportunity opps
    WHERE
        opps.test__c IS FALSE
    GROUP BY
        opps.sfid) t1 ON
    li.opportunity__c = t1.sfid

WHERE
    li.type__c = 'B2B'
    AND li.test__c IS FALSE
    AND li.company_name__c NOT LIKE '%test%'
    AND li.name NOT LIKE '%test%'
GROUP BY
    TO_CHAR(li.createddate, 'YYYY-MM')
ORDER BY
    TO_CHAR(li.createddate, 'YYYY-MM') DESC),
    


-- productive days(number of days worked)
-- % days worked (productive days / working days)
    
working_days AS (
SELECT
    t3.year_month,
    --    t3.DATE,
 t3.owner_name,
    t3.number_working_days_in_month,
    MAX(t3.working_day_number) AS worked_days
FROM
    (
    SELECT
        u.name AS owner_name,
        TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS DATE,
        wd.working_day_number,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month
    FROM
        salesforce.likeli__c li
    LEFT JOIN bi.working_days_monthly wd ON
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE = wd.date
    LEFT JOIN salesforce.user u ON
        li.ownerid = u.sfid
    WHERE
        li.type__c = 'B2B'
        AND li.test__c IS FALSE
        AND li.company_name__c NOT LIKE '%test%'
        AND li.name NOT LIKE '%test%'

        GROUP BY u.name,
        TO_CHAR(li.createddate, 'YYYY-MM'),
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
        wd.working_day_number,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month
    ORDER BY
        TO_CHAR(li.createddate, 'YYYY-MM') DESC) t3
GROUP BY
    t3.year_month,
    --   t3.DATE,
 t3.owner_name,
    t3.number_working_days_in_month
ORDER BY
    t3.year_month DESC )





