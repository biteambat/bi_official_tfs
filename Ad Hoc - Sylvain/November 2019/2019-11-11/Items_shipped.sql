



SELECT

	p.opportunity__c,
	MAX(p.servicedate__c) as last_shipment,
	SUM(p.quantity__c) as items_shipped



FROM

	salesforce.productlineitem__c p
	
WHERE

	p.test__c IS FALSE
	AND p.opportunity__c IS NOT NULL
	
GROUP BY

	p.opportunity__c
	