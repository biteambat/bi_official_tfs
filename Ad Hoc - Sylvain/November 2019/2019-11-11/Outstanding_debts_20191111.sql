


SELECT

	inv.customer__c,
	inv.opportunity__c,
	o.name,
	o.status__c,
	inv.balance__c,
	inv.amount__c,
	inv.name,
	inv.locale__c,
	inv.issued__c,
	inv.sfid,
	inv.contract__c,
	inv.service_type__c

FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND inv.balance__c > 0
	AND inv.opportunity__c IS NOT NULL
	AND inv.service_type__c IS NOT NULL
	AND o.status__c NOT IN ('CANCELLED', 'RESIGNED')