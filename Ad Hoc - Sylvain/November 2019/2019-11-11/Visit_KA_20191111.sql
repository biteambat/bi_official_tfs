
WITH cases AS
 (SELECT
	
		cas.opportunity__c,
		SUM(CASE WHEN cas.origin LIKE '%B2B customer%' 
		              OR cas.reason LIKE '%B2B customer%'
		              OR cas.reason LIKE 'Customer%'
		         THEN 1 
					ELSE 0 END) as cases_opp,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE 0 END) as feedbacks,
		SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback: 1' THEN 1
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 2' THEN 2
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 3' THEN 3
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 4' THEN 4
		     WHEN cas.subject LIKE 'Satisfaction Feedback: 5' THEN 5
		     ELSE 0
		     END) / SUM(CASE WHEN cas.subject LIKE 'Satisfaction Feedback:%'
		         THEN 1 
					ELSE NULL END)::decimal as avg_feedback	
	FROM
	
		salesforce.case cas
		
	LEFT JOIN
	
		salesforce.opportunity o
		
	ON
	
		cas.opportunity__c = o.sfid
	
	WHERE
	
		o.stagename = 'WON'
		AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
		AND o.test__c IS FALSE
		
	GROUP BY	
	
		cas.opportunity__c),
		
owners AS (SELECT
			
				o.opportunityid,	
				COUNT(o.newvalue) as number_owners
			
			FROM
			
				salesforce.opportunityfieldhistory o
				
			WHERE
			
				o.field = 'Owner'
				
			GROUP BY
			
				o.opportunityid),
				
cleaners AS (SELECT
				
					o.opportunityid,
					COUNT(DISTINCT o.professional__c) as number_cleaners
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.test__c IS FALSE
					AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
					
				GROUP BY
				
					o.opportunityid),
					
orders AS (SELECT
				
					o.opportunityid,
					oo.closedate as closedate,
					SUM(CASE WHEN o.status = 'CANCELLED CUSTOMER' THEN 1 ELSE 0 END) as canc_cust,
					SUM(CASE WHEN o.status = 'NOSHOW PROFESSIONAL' THEN 1 ELSE 0 END) as nsw_pro,
					SUM(CASE WHEN o.status = 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) as canc_pro
						
									
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON
				
					o.opportunityid = oo.sfid
					
				WHERE
				
					o.test__c IS FALSE
				   AND oo.status__c NOT IN ('RESIGNED', 'CANCELLED')
				   AND oo.stagename IN ('WON', 'PENDING')
					AND oo.test__c IS FALSE
					
				GROUP BY
				
					o.opportunityid,
					oo.closedate),
					
supply AS (SELECT

				p.opportunity__c,
				MAX(p.servicedate__c) as last_shipment,
				SUM(p.quantity__c) as items_shipped
					
			FROM
			
				salesforce.productlineitem__c p
				
			WHERE
			
				p.test__c IS FALSE
				AND p.opportunity__c IS NOT NULL
				
			GROUP BY
			
				p.opportunity__c),

visits AS (SELECT

				ev.whatid as opportunity,
				COUNT(DISTINCT ev.sfid) as number_visits
			
			FROM
			
				salesforce.event ev
				
			WHERE
			
				ev.whatid LIKE '006%'
				AND ev.event_reason__c LIKE 'Visit%'
				
			GROUP BY

				ev.whatid),
				
invoices AS (SELECT

			    inv.*
			
				FROM
				
				    salesforce.invoice__c inv
				    
				WHERE
				
					inv.test__c IS FALSE)
			
			
-----------------------------------------------------------------------------------------------		
-----------------------------------------------------------------------------------------------	
-----------------------------------------------------------------------------------------------	
-----------------------------------------------------------------------------------------------		
								

SELECT

	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate)) as month_active,
	o.closedate,
	o.traffic_light__c,
	cases.cases_opp,
	cases.feedbacks,
	cases.avg_feedback,
	CASE WHEN owners.number_owners IS NULL THEN 1 ELSE owners.number_owners + 1 END as number_owners,
	cleaners.number_cleaners,
	orders.canc_cust,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN orders.canc_cust 
	     ELSE orders.canc_cust/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) 
		  END as can_cust_per_month,
	orders.canc_pro,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN orders.canc_pro 
	     ELSE orders.canc_pro/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate)))
		  END as can_pro_per_month,
	orders.nsw_pro,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN orders.nsw_pro 
	     ELSE orders.nsw_pro/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) 
		  END as nsw_per_month,
	SUM(inv.balance__c) as outstanding_debt,
	SUM(CASE WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '3 months')::text, 7) THEN inv.amount__c
				WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '2 months')::text, 7) THEN inv.amount__c
				WHEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 months')::text, 7) THEN inv.amount__c
				ELSE 0
				END) as last_3months_invoiced,
	SUM(CASE WHEN (inv.service_type__c NOT LIKE '%maintenance cleaning%' AND inv.parent_invoice__c IS NULL) THEN 1 ELSE 0 END) as special_services,
	COUNT(DISTINCT inv.parent_invoice__c) as corrected_invoices,
	supply.last_shipment,
	supply.items_shipped,
	CASE WHEN (EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) = 0 THEN supply.items_shipped
	     ELSE supply.items_shipped/(EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate))) 
		  END as items_per_month,
	visits.number_visits	 

FROM 

	salesforce.opportunity o
	
LEFT JOIN

	cases
	
ON

	o.sfid = cases.opportunity__c
	
LEFT JOIN

	owners
	
ON

	o.sfid = owners.opportunityid
	
LEFT JOIN

	cleaners
	
ON

	o.sfid = cleaners.opportunityid
	
LEFT JOIN

	orders
	
ON 

	o.sfid = orders.opportunityid
	
LEFT JOIN

	invoices as inv
	
ON

	o.sfid = inv.opportunity__c
	
LEFT JOIN

	supply
	
ON

	o.sfid = supply.opportunity__c
	
LEFT JOIN

	visits
	
ON

	o.sfid = visits.opportunity
	
WHERE

	o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	AND o.test__c IS FALSE
	AND o.stagename IN ('WON', 'PENDING')
	
GROUP BY
	
	o.sfid,
	o.name,
	o.customer__c,
	o.status__c,
	o.closedate,
	EXTRACT(year FROM age(current_date,o.closedate))*12 + EXTRACT(month FROM age(current_date,o.closedate)),
	o.traffic_light__c,
	cases.cases_opp,
	cases.feedbacks,
	cases.avg_feedback,
	owners.number_owners,
	cleaners.number_cleaners,
	orders.canc_cust,
	orders.nsw_pro,
	orders.canc_pro,
	supply.last_shipment,
	supply.items_shipped,
	visits.number_visits
	
	
	
	
	