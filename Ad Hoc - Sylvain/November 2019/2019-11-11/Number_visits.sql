



SELECT

	ev.whatid,
	COUNT(DISTINCT ev.sfid) as number_visits

FROM

	salesforce.event ev
	
WHERE

	ev.whatid LIKE '006%'
	AND ev.event_reason__c LIKE 'Visit%'
	
GROUP BY

	ev.whatid