

WITH 

orders as (SELECT

			  		*
			  FROM
			  
			  		salesforce.order o
	
			  WHERE
				
					o.status IN ('INVOICED',
				            'PENDING TO START',
				            'CANCELLED CUSTOMER',
				            'FULFILLED')
				   AND o.type = 'cleaning-b2b'
				   AND o.professional__c IS NOT NULL
				   AND o.test__c IS FALSE
				   AND LEFT(o.effectivedate::text, 7) = LEFT((current_date - interval '1 month')::text, 7)

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------


SELECT

	t2.year_month,
	t2.today,
	SUM(CASE WHEN t2.status__c NOT IN ('RESIGNED', 'CANCELLED', 'RETENTION', 'OFFBOARDING') THEN 1 ELSE 0 END)::DECIMAL AS running_opps,
	SUM(CASE WHEN t2.status__c NOT IN ('RESIGNED', 'CANCELLED', 'RETENTION', 'OFFBOARDING') THEN t2.revenue ELSE 0 END)::DECIMAL AS running_revenue,
	SUM(CASE WHEN t2.status__c NOT IN ('RESIGNED', 'CANCELLED', 'RETENTION', 'OFFBOARDING') THEN t2.revenue ELSE 0 END)::DECIMAL/SUM(CASE WHEN t2.status__c NOT IN ('RESIGNED', 'CANCELLED', 'RETENTION', 'OFFBOARDING') THEN 1 ELSE 0 END)::DECIMAL as basket_running,
	
	SUM(CASE WHEN t2.status__c = 'RETENTION' THEN 1 ELSE 0 END)::DECIMAL AS retention_opps,
	SUM(CASE WHEN t2.status__c = 'RETENTION' THEN t2.revenue ELSE 0 END)::DECIMAL AS retention_revenue,
	SUM(CASE WHEN t2.status__c = 'RETENTION' THEN t2.revenue ELSE 0 END)::DECIMAL/SUM(CASE WHEN t2.status__c = 'RETENTION' THEN 1 ELSE 0 END)::DECIMAL as basket_retention,
	
	SUM(CASE WHEN t2.status__c = 'OFFBOARDING' THEN 1 ELSE 0 END)::DECIMAL AS offboarding_opps,
	SUM(CASE WHEN t2.status__c = 'OFFBOARDING' THEN t2.revenue ELSE 0 END)::DECIMAL AS offboarding_revenue,
	SUM(CASE WHEN t2.status__c = 'OFFBOARDING' THEN t2.revenue ELSE 0 END)::DECIMAL/SUM(CASE WHEN t2.status__c = 'OFFBOARDING' THEN 1 ELSE 0 END)::DECIMAL as basket_offboarding
	

FROM


	(SELECT
	
		t1.*,
		CASE WHEN t1.grand_total__c IS NULL THEN t1.pph_money ELSE t1.grand_total__c END as revenue
			
	FROM	
		
		(SELECT
		 
			current_date as today,
			TO_CHAR(current_date, 'YYYY-MM') AS year_month,
			opps.sfid,
			opps.name,
			opps.closedate,
			opps.status__c,
			opps.grand_total__c,
			opps.plan_pph__c,
			SUM(o.order_duration__c) as order_duration,
			SUM(o.order_duration__c)*opps.plan_pph__c as pph_money
								    	
		FROM
		
		
		   salesforce.opportunity opps
		   
		LEFT JOIN
		
			orders o
			
		ON
		
			opps.sfid = o.opportunityid
		      
		WHERE
		
			LEFT(opps.locale__c, 2) = 'de'
			AND opps.test__c IS FALSE
		   AND opps.test__c IS FALSE
		   
		GROUP BY
		
		    year_month,
		    current_date,
		    opps.sfid,
		    opps.name,
		    opps.closedate,
		    opps.status__c,
		    opps.grand_total__c,
		    opps.plan_pph__c) as t1) as t2
		    
GROUP BY

	t2.year_month,
	t2.today
	