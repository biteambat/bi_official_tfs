


SELECT

	o.sfid,
	o.name,
	o.delivery_area__c,
	o.stagename,
	o.supplies__c,
	o.service__c,
	o.status__c

FROM

	salesforce.opportunity o
	
WHERE

	o.stagename IN ('PENDING', 'WON')
	AND o.test__c IS FALSE
	AND o.status__c IS NULL