


SELECT

	cont.start__c,
	cont.end__c,
	cont.sfid,
	cont.opportunity__c,
	cont.name,
	cont.service_type__c,
	cont.service_description__c,
	cont.grand_total__c,
	EXTRACT(DAY FROM cont.start__c) as day_first_order,
	DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
	DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM cont.start__c) + 1 as days_to_invoice,
	CASE WHEN LEFT(cont.end__c::text, 7) = LEFT(current_date::text, 7)
		  THEN cont.grand_total__c
		  ELSE
		  (cont.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM cont.start__c) + 1)
        END as on_top_this_month


FROM

	salesforce.contract__c cont
	
WHERE

	LEFT(cont.start__c::text, 7) = LEFT(current_date::text, 7)
	AND cont.active__c = TRUE
	AND cont.test__c IS FALSE
	AND cont.service_type__c NOT LIKE 'maintenance cleaning'