


SELECT

	cont.start__c,
	cont.end__c,
	cont.duration__c,
	CASE WHEN cont.duration__c IS NOT NULL THEN 'Recurring'
	     WHEN cont.start__c IS NOT NULL AND cont.end__c IS NULL THEN 'Indefinite'
	     WHEN cont.start__c IS NOT NULL AND cont.end__c IS NOT NULL AND cont.duration__c IS NULL THEN 'One-Off'
	     ELSE 'Unknown'
	     END as type_contract,
	     
	cont.sfid,
	cont.opportunity__c,
	cont.name,
	o.status__c,
	cont.service_type__c,
	cont.service_description__c,
	cont.grand_total__c,
	DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
	DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM cont.end__c) + 1 as days_to_invoice,
	CASE WHEN LEFT(cont.end__c::text, 7) = LEFT(current_date::text, 7)
		  THEN (cont.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM cont.end__c) + 1)
		  ELSE cont.grand_total__c
		  END as on_top_this_month

FROM

	salesforce.contract__c cont
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	cont.opportunity__c = o.sfid
	
WHERE

	cont.start__c < '2019-08-01'
	AND (cont.end__c >= '2019-08-01' OR cont.end__c IS NULL)
	AND cont.active__c = TRUE
	AND cont.test__c IS FALSE
	AND cont.service_type__c NOT LIKE 'maintenance cleaning'