
DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_new_v2;
CREATE TABLE 			bi.opportunity_traffic_light_new_v2 			AS 

SELECT

	date,
	locale,
	customer,
	opportunity_name,
	opportunity,
	status,
	opps,
	owner,
	delivery_area,
	operated_by,
	operated_by_detail,
	orders_nsp,
	nsp,
	nsp_rate,
	orders_cp,
	cp,
	cp_rate,
	orders_cc,
	cc,
	cc_rate,
	orders_nsp_cp,
	nsp_cp,
	nso_cp_rate,
	createdcases,
	retention_all,
	retention_closed,
	invoicecorrection_all,
	invoicecorrection_closed,
	professionalimprovement_all,
	professionalimprovement_closed,
	CAST(NULL AS numeric) as partnerimprovement_all,
	CAST(NULL AS numeric) as partnerimprovement_closed,
	all_inbound_calls as inbound_not_complaints,
	lost_inbound_calls,
	all_outbound,
	b2b_outbound,
	CAST(NULL AS numeric) as complaint_calls,
	CAST(NULL AS numeric) as no_complaint_calls,
	traffic_light_noshow,
	traffic_light_cancelled_pro,
	traffic_light_cancelled_customer,
	traffic_light_inbound_calls as traffic_light_inbound_not_complaints,
	CAST(NULL AS numeric) as traffic_light_complaint_calls,
	traffic_light_lost_calls,	
	traffic_light_createdcases,
	traffic_light_retention,
	traffic_light_invoicecorrection,
	traffic_light_professionalimprovement,
	CAST(NULL AS numeric) as traffic_light_partnerimprovement,
	avg_traffic_light
	
FROM

	bi.opportunity_traffic_light_new t1
	

	
