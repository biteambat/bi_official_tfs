



SELECT

	o.contact__c,
	o.customer_name__c,
	o.delivery_area__c,
	l.createddate as likeli_creation,
	MIN(o.effectivedate) as first_order
	
	

FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.likeli__c l
	
ON

	o.contact__c = l.customer__c
	
WHERE

	o.test__c IS FALSE
	AND o."type" = 'cleaning-b2c'
	AND l.opportunity__c IS NULL
	AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
	
GROUP BY

	o.contact__c,
	o.customer_name__c,
	o.delivery_area__c,
	l.createddate
	
	