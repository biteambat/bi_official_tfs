


SELECT

	*

FROM

	salesforce.opportunityfieldhistory o
	
WHERE

	o.newvalue IN ('RESIGNED', 'CANCELLED')
	AND TO_CHAR(o.createddate, 'YYYY-MM-DD') = TO_CHAR(current_date, 'YYYY-MM-DD')