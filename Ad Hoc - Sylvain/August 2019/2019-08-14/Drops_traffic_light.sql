			
	


SELECT

	t5.name,
	t3.opportunity,
	t5.delivery_area__c,
	t5.status__c,
	t5.stagename,
	t4.potential,
	CASE WHEN t4.potential < 250 THEN '< 250€'
	     WHEN t4.potential >= 250 AND t4.potential < 500 THEN '250-500€'
	     WHEN t4.potential >= 500 AND t4.potential < 1000 THEN '500-1000€'
	     ELSE 'KA'
	     END as category,
	t3.traffic_light_this_week - t3.traffic_light_week_min1 as variation_wow,
	t3.traffic_light_this_week - (CASE WHEN (CASE WHEN t3.traffic_light_week_min1 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min3 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min4 > 0 THEN 1 ELSE 0 END) > 0
												  THEN
												  (t3.traffic_light_week_min1 + t3.traffic_light_week_min2 + t3.traffic_light_week_min3 + t3.traffic_light_week_min4)/((CASE WHEN t3.traffic_light_week_min1 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min3 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min4 > 0 THEN 1 ELSE 0 END))
												  ELSE 0
												  END) as variation_wo4w,
	t3.traffic_light_this_week,
	t3.traffic_light_week_min1,
	t3.traffic_light_week_min2,
	t3.traffic_light_week_min3,
	t3.traffic_light_week_min4,
	CASE WHEN (CASE WHEN t3.traffic_light_week_min1 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min3 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min4 > 0 THEN 1 ELSE 0 END) > 0
	     THEN
	          (t3.traffic_light_week_min1 + t3.traffic_light_week_min2 + t3.traffic_light_week_min3 + t3.traffic_light_week_min4)/((CASE WHEN t3.traffic_light_week_min1 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min3 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t3.traffic_light_week_min4 > 0 THEN 1 ELSE 0 END))
	     ELSE 0
	     END as avg_traffic_light_last_4weeks
	
FROM
	
	(SELECT
	
		t2.opportunity,
		SUM(t2.traffic_light_this_week) as traffic_light_this_week,
		SUM(t2.traffic_light_week_min1) as traffic_light_week_min1,
		SUM(t2.traffic_light_week_min2) as traffic_light_week_min2,
		SUM(t2.traffic_light_week_min3) as traffic_light_week_min3,
		SUM(t2.traffic_light_week_min4) as traffic_light_week_min4
		
	
	FROM	
		
		(SELECT
		
			t1.week,
			t1.opportunity,
			SUM(CASE WHEN TO_CHAR(t1.mindate, 'YYYY-WW') = TO_CHAR(current_date, 'YYYY-WW')
					   THEN t1.avg_traffic_light 
						ELSE 0 
						END) as traffic_light_this_week,
			SUM(CASE WHEN TO_CHAR(t1.mindate, 'YYYY-WW') = TO_CHAR((current_date - 7), 'YYYY-WW')
					   THEN t1.avg_traffic_light 
						ELSE 0 
						END) as traffic_light_week_min1,
			SUM(CASE WHEN TO_CHAR(t1.mindate, 'YYYY-WW') = TO_CHAR((current_date - 14), 'YYYY-WW')
					   THEN t1.avg_traffic_light 
						ELSE 0 
						END) as traffic_light_week_min2,
			SUM(CASE WHEN TO_CHAR(t1.mindate, 'YYYY-WW') = TO_CHAR((current_date - 21), 'YYYY-WW')
					   THEN t1.avg_traffic_light 
						ELSE 0 
						END) as traffic_light_week_min3,		
			SUM(CASE WHEN TO_CHAR(t1.mindate, 'YYYY-WW') = TO_CHAR((current_date - 28), 'YYYY-WW')
					   THEN t1.avg_traffic_light 
						ELSE 0 
						END) as traffic_light_week_min4
		
			
		FROM	
			
			(SELECT
				
				TO_CHAR(o.date, 'YYYY-WW') as week,
				MIN(o.date) as mindate,
				o.opportunity,
				AVG(o.avg_traffic_light) as avg_traffic_light
			
			FROM
			
				bi.opportunity_traffic_light_new_v2 o
				
			GROUP BY
			
				TO_CHAR(o.date, 'YYYY-WW'),
				o.opportunity) as t1
				
		
		GROUP BY
		
			t1.week,
			t1.opportunity) as t2
			
	GROUP BY
	
		t2.opportunity) as t3
		
LEFT JOIN

	bi.potential_revenue_per_opp as t4
	
ON

	t3.opportunity = t4.opportunityid
	
LEFT JOIN

	salesforce.opportunity t5
	
ON

	t3.opportunity = t5.sfid
	
LEFT JOIN

	salesforce.contract__c as t6
	
ON

	t3.opportunity = t6.opportunity__c
	
WHERE

	t5.status__c NOT IN ('RESIGNED', 'CANCELLED', 'OFFBOARDING')
	AND t6.service_type__c = 'maintenance cleaning'
	AND t6.active__c IS TRUE
