	

SELECT

	t1.opportunityid,
	t1.name,
	t1.grand_total__c,
	SUM(inv.amount__c)/1.19 as invoiced,
	t1.confirmed_end__c,
	t1.first_order

FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.grand_total__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.grand_total__c,
		ooo.confirmed_end__c) as t1
		
LEFT JOIN

	salesforce.invoice__c inv
	
ON

	t1.opportunityid = inv.opportunity__c
		
WHERE
	
	LEFT(t1.first_order::text, 7) = '2019-07'
	AND LEFT(inv.issued__c::text, 7) = '2019-07'
	AND (LEFT(t1.confirmed_end__c::text, 7) NOT LIKE '2019-07' OR t1.confirmed_end__c IS NULL)
	
GROUP BY

	t1.opportunityid,
	t1.name,
	t1.grand_total__c,
	t1.confirmed_end__c,
	t1.first_order
	
	
