


SELECT

	'New Customers Last Month' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	-- t1.confirmed_end__c,
	-- t1.first_order,	
	t1.grand_total__c,
	SUM(inv.amount__c)/1.19 as invoiced_last_month,
	t1.grand_total__c - SUM(inv.amount__c)/1.19 as on_top_this_month
	

FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.delivery_area__c,
		oo.name,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c) as t1
		
LEFT JOIN

	salesforce.invoice__c inv
	
ON

	t1.opportunityid = inv.opportunity__c
		
WHERE
	
	LEFT(t1.first_order::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7)
	     OR LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
	     OR t1.confirmed_end__c IS NULL)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7) OR t1.confirmed_end__c IS NULL)
	-- AND inv.service_type__c = 'maintenance cleaning'
	
GROUP BY
	
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.grand_total__c
	-- t1.confirmed_end__c


