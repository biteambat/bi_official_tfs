

SELECT

	o.sfid,
	o.name,
	o.grand_total__c,
	SUM(ooo.amount__c)/1.19 as invoiced_last_month,
	oo.start__c,
	oo.end__c,
	oo.resignation_date__c,
	oo.confirmed_end__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND ooo.issued__c::text = '2019-07-31'
	AND LEFT(oo.confirmed_end__c::text, 7) = '2019-07'
	AND oo.service_type__c = 'maintenance cleaning'
	-- AND ooo.service_type__c = 'maintenance cleaning'
	
GROUP BY

	o.sfid,
	o.name,
	o.grand_total__c,
	oo.start__c,
	oo.duration__c,
	oo.end__c,
	oo.resignation_date__c,
	oo.confirmed_end__c