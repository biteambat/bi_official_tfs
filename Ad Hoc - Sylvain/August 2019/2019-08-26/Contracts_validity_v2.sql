

SELECT
				
	time_table.*,
	contract_dates.*,
	CASE WHEN (time_table.ymd >= contract_dates.start__c::date) AND (time_table.ymd <= contract_dates.confirmed_end__c) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category

FROM	
	
	(SELECT
		
		'1'::integer as key,	
		TO_CHAR(i, 'YYYY-MM') as year_month,
		MIN(i) as ymd,
		MAX(i) as ymd_max
		-- i::date as date 
		
	FROM
	
		generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
		
	GROUP BY
	
		key,
		year_month
		-- date
		
	ORDER BY 
	
		year_month desc) as time_table
		
LEFT JOIN

	(SELECT

		'1'::integer as key_link,
		cont.opportunity__c,
		cont.sfid,
		cont.service_type__c,
		cont.grand_total__c,
		CASE WHEN cont.start__c < cont.effective_start__c THEN cont.effective_start__c ELSE cont.start__c END start__c,
		CASE WHEN cont.confirmed_end__c IS NULL THEN (current_date + 365) ELSE cont.confirmed_end__c END as confirmed_end__c
		
	FROM
	
		salesforce.contract__c cont
		
	WHERE
	
		cont.test__c IS FALSE
		AND cont.status__c IN ('RESIGNED', 'CANCELLED', 'ACCEPTED', 'SIGNED')) as contract_dates
	
ON 
				
	time_table.key = contract_dates.key_link	
	
	
	
	
	
	
	
	