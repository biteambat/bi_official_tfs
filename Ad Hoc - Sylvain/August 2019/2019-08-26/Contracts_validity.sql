

SELECT

	cont.opportunity__c,
	cont.sfid,
	cont.grand_total__c,
	CASE WHEN cont.start__c < cont.effective_start__c THEN cont.effective_start__c ELSE cont.start__c END start__c,
	CASE WHEN cont.confirmed_end__c IS NULL THEN (current_date + 365) ELSE cont.confirmed_end__c END as confirmed_end__c
	
FROM

	salesforce.contract__c cont
	
WHERE

	cont.test__c IS FALSE
	AND cont.status__c IN ('RESIGNED', 'CANCELLED', 'ACCEPTED', 'SIGNED')



