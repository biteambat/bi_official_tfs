

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Calculations for the drops in TFL

DELETE FROM 	bi.opportunity_traffic_light_drops 		WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO 	bi.opportunity_traffic_light_drops

	SELECT		(CURRENT_DATE - INTERVAL '0 DAY')	::date						AS date -- the drop calculated today is based on what happened yesterday
				, traffic_lights.*
	
				-- AVG Traffic light:
				-- if opp status = RETENTION or OFFBOARDING -> traffic light has te be 1(red)
				-- Order related Traffic Lights are triple-weightes. 
				-- The rest are equally weighted.
				, ROUND (CASE WHEN 	Status 										= 'RETENTION' 
									OR Status 									= 'OFFBOARDING'
									OR traffic_light_Retention 				= 1 
									OR traffic_light_noshow	= 1
									OR traffic_light_bad_feedback = 1					THEN 1 
							ELSE	((	traffic_light_cancelled_pro*3 
										+ traffic_light_inbound_calls*1
										+ traffic_light_lost_calls*1
										+ traffic_light_complaint_calls*3
										+ traffic_light_CreatedCases*3
										+ traffic_light_InvoiceCorrection*3 
										+ traffic_light_ProfessionalImprovement*2
										+ traffic_light_PartnerImprovement*2) 
										/ 18) 
										END , 1) 															AS tfl_today
				, 3 - ROUND (CASE WHEN 	Status 										= 'RETENTION' 
									OR Status 									= 'OFFBOARDING'
									OR traffic_light_Retention 				= 1 
									OR traffic_light_noshow	= 1
									OR traffic_light_bad_feedback = 1					THEN 1 
							ELSE	((	traffic_light_cancelled_pro*3 
										+ traffic_light_inbound_calls*1
										+ traffic_light_lost_calls*1
										+ traffic_light_complaint_calls*3
										+ traffic_light_CreatedCases*3
										+ traffic_light_InvoiceCorrection*3 
										+ traffic_light_ProfessionalImprovement*2
										+ traffic_light_PartnerImprovement*2) 
										/ 18) 
										END , 1) as drop_today

	FROM	(
			SELECT		traffic_light_detail.*
						, ROUND ((CASE	WHEN	nsp_Rate > 0 THEN 1
										ELSE 3 
										END), 1)  															AS traffic_light_noshow
										
						, ROUND ((CASE	WHEN 	cp_Rate > 0	THEN 1
										ELSE 3 
										END), 1)  															AS traffic_light_cancelled_pro
						
						, ROUND ((CASE	WHEN 	cc_Rate 						>= 0.2 						THEN 1
										WHEN 	cc_Rate 						BETWEEN 0.005 AND 0.2 		THEN 2
										WHEN 	cc_Rate 						<= 0.005 					THEN 3 
										END), 1) 															AS traffic_light_cancelled_customer
														
						, ROUND ((CASE	WHEN 	all_inbound_calls 				>= 1 						THEN 1
										WHEN 	all_inbound_calls 				< 1 
											OR 	(all_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_inbound_calls
						, ROUND ((CASE	WHEN 	complaint_calls 				>= 1 						THEN 1
										WHEN 	complaint_calls 				< 1                     
											OR 	(complaint_calls IS NULL) 	= TRUE 					THEN 3 
										END), 1) 															AS traffic_light_complaint_calls
												
						, ROUND ((CASE	WHEN 	lost_inbound_calls				>= 1 						THEN 1
										WHEN 	lost_inbound_calls < 1 
											OR 	(lost_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_lost_calls
										
						, ROUND ((CASE	WHEN 	createdcases 					>= 1 						THEN 1
										WHEN 	createdcases					<1 
											OR 	(createdcases IS NULL) 			= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_CreatedCases
										
						, ROUND ((CASE	WHEN 	retention_all 					> 0 
											AND retention_closed 				<> retention_all 			THEN 1 -- Retention case open
										WHEN 	retention_all 					> 0 
											AND retention_closed 				= retention_all 			THEN 2 -- Retention case closed
										WHEN 	retention_all 					= 0 
											OR 	(retention_all IS NULL) 		= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_Retention
								
						, ROUND ((CASE	WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		<> invoicecorrection_all 	THEN 1
										WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		= invoicecorrection_all 	THEN 2
										WHEN 	invoicecorrection_all 			= 0 
											OR 	(invoicecorrection_all IS NULL) = TRUE 						THEN 3 
										END), 1) 															AS traffic_light_InvoiceCorrection
						
						, ROUND	((CASE	WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	<> professionalimprovement_all 	THEN 1
										WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	= professionalimprovement_all 	THEN 2
										WHEN 	professionalimprovement_all 	= 0 
											OR (professionalimprovement_all IS NULL) = TRUE 					THEN 3 
										END), 1) 															AS traffic_light_ProfessionalImprovement
										
						, ROUND	((CASE	WHEN 	partnerimprovement_all 	> 0 
											AND partnerimprovement_closed 	<> partnerimprovement_all 	THEN 1
										WHEN 	partnerimprovement_all 	> 0 
											AND partnerimprovement_closed 	= partnerimprovement_all 	THEN 2
										WHEN 	partnerimprovement_all 	= 0 
											OR (partnerimprovement_all IS NULL) = TRUE 					THEN 3 
										END), 1) 															AS traffic_light_PartnerImprovement
						, ROUND	((CASE	WHEN 	bad_feedback 	> 0	THEN 1
												ELSE 3 
										      END), 1) 													AS traffic_light_bad_feedback
										
			FROM 	(
					SELECT 	O.*
			
					-- orders
							, orders_final.orders_nsp
							, orders_final.nsp
							, (CASE 	WHEN orders_final.orders_nsp 	> 0 		THEN orders_final.nsp/orders_final.orders_nsp 			ELSE 0 	END) 	AS nsp_Rate
							
							, orders_final.orders_cp
							, orders_final.cp
							, (CASE 	WHEN orders_final.orders_cp 	> 0 		THEN orders_final.cp/orders_final.orders_cp 			ELSE 0 	END) 	AS cp_Rate
							
							, orders_final.orders_cc
							, orders_final.cc
							, (CASE 	WHEN orders_final.orders_cc 	> 0 		THEN orders_final.cc/orders_final.orders_cc 			ELSE 0 	END) 	AS cc_Rate
							
							, orders_final.orders_nsp_cp
							, orders_final.nsp_cp
							, (CASE 	WHEN orders_final.orders_nsp_cp > 0 		THEN orders_final.nsp_cp/orders_final.orders_nsp_cp 	ELSE 0 	END) 	AS nso_cp_Rate
							
					-- inbound cases
							, Inbound_Cases_final.createdcases
							
					-- internal cases
							, Intern_Cases_final.retention_all
							, Intern_Cases_final.retention_closed
							, Intern_Cases_final.invoicecorrection_all
							, Intern_Cases_final.invoicecorrection_closed
							, Intern_Cases_final.professionalimprovement_all
							, Intern_Cases_final.professionalimprovement_closed
							, Intern_Cases_final.partnerimprovement_all
							, Intern_Cases_final.partnerimprovement_closed
							, Intern_Cases_final.bad_feedback
							
					-- calls
							, Calls_final.all_inbound_calls
							, Calls_final.lost_inbound_calls
							, Calls_final.all_outbound
							, Calls_final.b2b_outbound
							, Calls_final.complaint_calls
							, Calls_final.no_complaint_calls
							


FROM 	bi.opportunitylist_traffic_light_v2 O


-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) = yesterday

LEFT JOIN		
		
		(
		SELECT
		Opp
		, SUM(Orders_NSP)			AS Orders_NSP
		, SUM(NSP)					AS NSP
		, SUM(Orders_CP)			AS Orders_CP
		, SUM(CP)					AS CP
		, SUM(Orders_CC)			AS Orders_CC
		, SUM(CC)					AS CC
		, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
		, SUM(NSP_CP)				AS NSP_CP
		
		FROM (
		SELECT 
		
		opportunityid AS Opp
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
						WHEN status like '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
									
		FROM
		salesforce."order" AS orders
		
		WHERE
		effectivedate::date = (current_date - 1)
											
		AND opportunityid IS NOT NULL
		
		GROUP BY
		Opp
		, delivery_area__c 
		
		) AS orders2
		
		GROUP BY
		Opp
		
) AS orders_final
		
ON (orders_final.opp = O.opportunity)

		
-- --
-- -- inbound cases
-- -- Cases created yesterday
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
-- 22/07/2019 --> MODIFICATIONS NEW TRAFFIC LIGHT -- We now take into account only the complaint cases
		
LEFT JOIN 	
LATERAL
		
		(
		SELECT 	Opportunity
				, SUM(createdcases)								AS createdcases
		
		FROM
			(
			SELECT 	Opps.*
					, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
									THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
												THEN 0.00 
												ELSE 1.0 * createdcases / Opps END)
									ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
												THEN 0.00
												ELSE 1.0 * createdcases END) END)::numeric, 2)		AS createdcases
		
			FROM 	bi.opportunitylist_traffic_light_v2 		Opps
		
			LEFT JOIN LATERAL
				(SELECT 	cas.contactid 						AS Contact
							, cas.opportunity__c 					AS Opportunity
							, COUNT(1) 							AS CreatedCases
		
				FROM 		salesforce.case 		cas
	
				WHERE	(cas.test__c				= FALSE  
						OR cas.test__c				IS NULL)
						
						AND (	
						
						-- old case setup (3 AND NOT 11)
								(
									(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
											WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
											WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
											WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
											WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
								AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
											WHEN 	cas.type 	= 'CLM'					THEN 1
											WHEN 	cas.type	= 'CM B2C'				THEN 1
											WHEN 	cas.type	= 'Sales'				THEN 1
											WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
								)
							OR	(
									(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
											WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
											WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
											WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
								AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
											WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
								)
								
						-- new case setup
							OR	(	
									(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
								AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
											WHEN 	cas.type 	= 'Pool'				THEN 1
											WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
								)
								
							OR 	(
									(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
											WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
								)
							)
	
				
							AND cas.createddate::date = (current_date - 1)
							AND cas.reason NOT IN ('Checkout')
							
							AND cas.createdbyid = '00520000003IiNCAA0' 
							AND cas.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint')
							AND cas.origin NOT IN ('System - Notification')
	
				GROUP BY  	cas.contactid
							, cas.opportunity__c
		
				) AS Inbound_Cases 			ON 		(CASE 	WHEN Inbound_Cases.Opportunity 	IS NULL 
															THEN Opps.Customer 				= Inbound_Cases.Contact 
															ELSE Inbound_Cases.Opportunity 	= Opps.Opportunity END)
				
			) AS Inbound_Cases_f
		
			GROUP BY Opportunity
		
		) AS Inbound_Cases_final 			ON 		(Inbound_Cases_final.Opportunity = O.Opportunity)

-- --
-- -- internal cases:
-- -- Cases created yesterday
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- -- Partner - Improvement

LEFT JOIN

		(SELECT
		Opportunity
		, SUM(retention_all)						AS retention_all
		, SUM(Retention_closed)						AS Retention_closed
		, SUM(InvoiceCorrection_All)				AS InvoiceCorrection_All
		, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
		, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
		, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
		, SUM(PartnerImprovement_All)			AS PartnerImprovement_All
		, SUM(PartnerImprovement_closed)		AS PartnerImprovement_closed
		, SUM(bad_feedback)		AS bad_feedback
		
		
		FROM
		(
		SELECT
		Opps.*
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * retention_all / Opps END)
							ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
											THEN 0.00
											ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * Retention_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
											THEN 0.00
											ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * PartnerImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * PartnerImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * PartnerImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * PartnerImprovement_All END) END)::numeric, 2)		AS PartnerImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * PartnerImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * PartnerImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * PartnerImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * PartnerImprovement_closed END) END)::numeric, 2) 	AS PartnerImprovement_closed
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * bad_feedback / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * bad_feedback / Opps END)
							ELSE (CASE 	WHEN 1.0 * bad_feedback IS NULL
											THEN 0.00
											ELSE 1.0 * bad_feedback END) END)::numeric, 2) 	AS bad_feedback
		
		FROM bi.opportunitylist_traffic_light_v2 Opps
		
	
		LEFT JOIN 
		LATERAL
		
		(
		SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity

		, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
		AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment / invoice%'
		OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment /invoice%'
		OR subject LIKE '%nvoice %orrection%'
		AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
						
		, SUM(CASE 	WHEN reason LIKE '%Partner - Improvement%' THEN 1 ELSE 0 END) 	AS PartnerImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Partner - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS PartnerImprovement_closed
		, SUM(CASE 	WHEN reason LIKE '%Feedback / Complaint%' THEN 1 ELSE 0 END) 	AS bad_feedback

		
		FROM
		Salesforce.case	interncases	
		
		WHERE
			-- Cases created yesterday
			CreatedDate::date  = (current_date - 1)	
			
			AND Isdeleted 	= FALSE
			AND (test__c 	= FALSE   OR test__c	IS NULL)
			
			-- internal cases: 
			-- 	Customer - Retention 		> communication with sales
			-- 	Order - Invoice editing 	> communication with finance 
			-- 	Professional - Improvement > communication with TO
			
			AND ((reason LIKE '%Customer - Retention%' 
						OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 
						
					OR ((reason LIKE '%Professional - Improvement%')
						AND (type LIKE '%TO%' OR type LIKE '%CLM%'))
					OR (reason LIKE '%Partner - Improvement%')
					OR ((reason LIKE '%Feedback / Complaint%') AND subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3'))
					))
		
		
		GROUP BY
		Contact
		, Opportunity
		
		) AS Intern
		
		ON (CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)
				
		)		AS Intern_f
		
		GROUP BY
		Opportunity
		
) AS Intern_Cases_final
		
ON (Intern_Cases_final.opportunity = O.Opportunity)


-- --
-- -- calls:


LEFT JOIN

(
SELECT
Opps.opportunity
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)										AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)										AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
, ROUND ((CASE WHEN 1.0 * complaint_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * complaint_calls / opps END) ::numeric, 2)												AS complaint_calls					
, ROUND ((CASE WHEN 1.0 * no_complaint_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * no_complaint_calls / opps END) ::numeric, 2)												AS no_complaint_calls					


FROM bi.opportunitylist_traffic_light_v2 Opps


		LEFT JOIN 
		LATERAL
		(		
		SELECT

		RelatedContact__c 													AS Contact
		
	
		, SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						THEN 1 
						ELSE 0 END) 											AS all_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END) 											AS lost_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
						THEN 1 
						ELSE 0 END)												AS all_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS b2b_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' 
		                 AND t4.wrapup_string_1__c IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
							  AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS complaint_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' 
		                 AND calldirection__c = 'Inbound' 
		                 AND t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
							  AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS no_complaint_calls
		
		FROM 
		Salesforce.natterbox_call_reporting_object__c t4
		
			LEFT JOIN 
			salesforce.contact t5
			
			ON
			(t4.relatedcontact__c = t5.sfid)
		WHERE
		-- -- Calls created yesterday
		call_start_date_date__c::date = (current_date - 1)
		
		AND 	(	(calldirection__c = 'Inbound' 	AND E164Callednumber__C IN ('493030807263', '493030807264'))
				OR (calldirection__c = 'Outbound' 	AND Callerfirstname__c LIKE 'CM%'))
				
		AND RelatedContact__c IS NOT NULL
		
		GROUP BY
		Contact) as Call
		
		ON
		(Opps.Customer = Call.Contact)	
		
) AS Calls_final

ON (Calls_final.Opportunity = O.Opportunity)


) AS traffic_light_detail

) AS traffic_lights

;
