	
	
SELECT

	t1.*

FROM	
	
	(SELECT
	
		o.opportunity__c,
		oo.status__c,
		COUNT(DISTINCT o.service_type__c) as types_contract,
		SUM(CASE WHEN o.service_type__c NOT LIKE '%maintenance cleaning%' THEN 1 ELSE 0 END) as not_maintenance,
		SUM(CASE WHEN o.service_type__c LIKE '%maintenance cleaning%' THEN 1 ELSE 0 END) as maintenance
	
	
	FROM
	
		salesforce.contract__c o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunity__c = oo.sfid
		
	WHERE
	
		o.active__c IS TRUE
		AND o.test__c IS FALSE
		-- AND oo.status__c IN 		('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')
		-- AND o.service_type__c NOT LIKE '%maintenance cleaning%'
		
	GROUP BY
	
		o.opportunity__c,
		oo.status__c) as t1
		
WHERE

	maintenance = 0