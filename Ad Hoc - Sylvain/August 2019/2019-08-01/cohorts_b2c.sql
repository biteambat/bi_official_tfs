		

SELECT

	t3.*,
	CASE 
  WHEN CAST(EXTRACT(YEAR FROM t3.mindate_order::date) as integer) = CAST(EXTRACT(YEAR FROM t3.first_order::date) as integer) THEN CAST(EXTRACT(MONTH FROM t3.mindate_order::date) as integer)- CAST(EXTRACT(MONTH FROM t3.first_order::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM t3.mindate_order::date) as integer) != CAST(EXTRACT(YEAR FROM t3.first_order::date) as integer) THEN (CAST(EXTRACT(YEAR FROM t3.mindate_order::date) as integer)-CAST(EXTRACT(YEAR FROM t3.first_order::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM t3.mindate_order::date) as integer)- CAST(EXTRACT(MONTH FROM t3.first_order::date) as integer) )
  ELSE 0 END as returning_month

FROM	
	
	(SELECT
		
		TO_CHAR(t1.first_order, 'YYYY-MM') as month_first_order,
		t1.first_order,
		t1.country,
		t1.sfid_cust_b2c,
		TO_CHAR(t2.effectivedate, 'YYYY-MM') as month_order,
		MIN(t2.effectivedate) as mindate_order,
		COUNT(DISTINCT t2.sfid) as orders,
		SUM(t2.gmv_eur_net) as money
	
	FROM
		
		(SELECT
		
			MIN(o.effectivedate) as first_order,
			LEFT(o.locale__c, 2) as country,
			o.customer_id__c as sfid_cust_b2c
		
		FROM
		
			bi.orders o
			
		WHERE
		
			o.test__c IS FALSE
			AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND o."type" = 'cleaning-b2c'
			
		GROUP BY	
		
			LEFT(o.locale__c, 2),
			o.customer_id__c) as t1
			
	LEFT JOIN
	
		bi.orders t2
		
	ON
		
		t1.sfid_cust_b2c = t2.customer_id__c
				
	WHERE
		
		t2.test__c IS FALSE
		AND t2."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND t2."type" = 'cleaning-b2c'	
		
	GROUP BY
	
		TO_CHAR(t1.first_order, 'YYYY-MM'),
		t1.first_order,
		t1.country,
		t1.sfid_cust_b2c,
		TO_CHAR(t2.effectivedate, 'YYYY-MM')) as t3
			
ORDER BY

	month_first_order desc