

DELIMITER //
CREATE OR REPLACE FUNCTION bi.sfunc_sector_analysis(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_sector_analysis';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_temp_v2017;
CREATE TABLE bi.sector_analysis_temp_v2017 as 

WITH

	t1 AS (SELECT -- Table containing the useful data of salesforce.opportunity
	
				*
			 FROM
			 
			 	salesforce.opportunity
			 	
			 WHERE
			 
			 	createddate >= '2017-01-01'
			 	AND createddate < '2018-01-01'
			 	AND test__c IS FALSE
				AND LEFT(locale__c, 2) = 'de'),
			 	
	t9 AS (SELECT -- Table containing the useful data of salesforce.case
	
				o.createddate,
				o.reason,
				o.ownerid,
				o.sfid,
				o.contactid,
				o.order__c,
				o.accountid,
				o.origin,
				o.type,
				o.opportunity__c,
				o.feedback_list__c,
				o.feedback_categories__c
			
			FROM
			
				salesforce.case o
			
			WHERE
			
				createddate >= '2017-01-01'
				AND createddate < current_date
				AND (o.createdbyid IN ('00520000003b8qzAAA', -- Ninja Tiger
				                      '00520000003IiNCAA0', -- API Tiger
											 '00520000003h5YhAAI', -- Marketing
											 '00520000003bc3gAAA') -- Accounting
					  OR LOWER(o.origin) LIKE '%b2b%'
					  OR o.type IN ('KA', 'B2B', 'Sales', 'PM')
					  OR o.opportunity__c IS NOT NULL)
			ORDER BY
			
				o.createddate desc),
				
	t7 AS (SELECT -- Table containing the useful data of salesforce.natterbox_call_reporting_object__c
			
				o.createddate,
				o.sfid,
				o.createdbyid,
				o.relatedcontact__c,
				o.calltalkseconds__c,
				o.wrapup_string_1__c,
				o.wrapup_string_2__c,
				o.department__c,
				o.owner__c,
				o.ownerid,
				o.opportunity__c
			
			FROM
			
				salesforce.natterbox_call_reporting_object__c o
				
			WHERE
			
				o.relatedcontact__c IS NOT NULL
				AND o.createddate >= '2017-01-01'
				AND createddate < current_date
				
			ORDER BY
			
				o.createddate desc)
				
(SELECT
		
	TO_CHAR(t1.createddate, 'YYYY-MM') as year_month_opp_creation,
	MIN(t5.createddate) as createddate_likeli,
	MIN(t1.createddate) as createddate_opp,
	MAX(t4.onboarded_date) as onboarded_date,
	t1.sfid as opportunity,
	t1.customer__c as customer,
	t1.stagename,
	t1.status__c,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END as sector,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END as object_type,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END as potential_€,
	t7.createddate as date_call,
	t7.sfid as sfid_call,
	t7.calltalkseconds__c,
	-- SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	-- SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	-- SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	-- SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_cm,
	COUNT(DISTINCT t9.sfid) as cases_created
	
FROM

	t1
	
LEFT JOIN
	
	bi.potential_revenue_per_opp t2
		
ON

	t1.sfid = t2.opportunityid
	
LEFT JOIN -- Now joining a list containing the date when an opp went to the status "PENDING"
	
	(SELECT
	
		o.opportunityid,
		MIN(o.createddate) as onboarded_date
	
	FROM
	
		salesforce.opportunityfieldhistory o
		
	WHERE
	
		o.field = 'StageName'
		AND o.newvalue IN ('PENDING', 'WON')
		
	GROUP BY
	
		o.opportunityid) as t4
		
ON
	
	t1.sfid = t4.opportunityid	
	
LEFT JOIN

	salesforce.likeli__c t5
	
ON

	t1.customer__c = t5.customer__c
	
LEFT JOIN

	t7
	
ON

	t1.customer__c = t7.relatedcontact__c
	-- AND TO_CHAR(t1.createddate, 'YYYY-MM') = TO_CHAR(t7.createddate, 'YYYY-MM')
	
LEFT JOIN

	bi.opportunity_traffic_light_new t8
	
ON 

	t1.sfid = t8.opportunity
	
LEFT JOIN

	t9
	
ON

	t1.customer__c = t9.contactid

	
GROUP BY

	TO_CHAR(t1.createddate, 'YYYY-MM'),
	t1.customer__c,
	t1.stagename,
	t1.status__c,
	t1.sfid,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END,
	t7.createddate,
	t7.sfid,
	t7.calltalkseconds__c);
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_temp_v2018;
CREATE TABLE bi.sector_analysis_temp_v2018 as 

WITH

	t1 AS (SELECT -- Table containing the useful data of salesforce.opportunity
	
				*
			 FROM
			 
			 	salesforce.opportunity
			 	
			 WHERE
			 
			 	createddate >= '2018-01-01'
			 	AND createddate < '2019-01-01'
			 	AND test__c IS FALSE
				AND LEFT(locale__c, 2) = 'de'),
			 	
	t9 AS (SELECT -- Table containing the useful data of salesforce.case
	
				o.createddate,
				o.reason,
				o.ownerid,
				o.sfid,
				o.contactid,
				o.order__c,
				o.accountid,
				o.origin,
				o.type,
				o.opportunity__c,
				o.feedback_list__c,
				o.feedback_categories__c
			
			FROM
			
				salesforce.case o
			
			WHERE
			
				createddate >= '2018-01-01'
				AND createddate < current_date
				AND (o.createdbyid IN ('00520000003b8qzAAA', -- Ninja Tiger
				                      '00520000003IiNCAA0', -- API Tiger
											 '00520000003h5YhAAI', -- Marketing
											 '00520000003bc3gAAA') -- Accounting
					  OR LOWER(o.origin) LIKE '%b2b%'
					  OR o.type IN ('KA', 'B2B', 'Sales', 'PM')
					  OR o.opportunity__c IS NOT NULL)
			ORDER BY
			
				o.createddate desc),
				
	t7 AS (SELECT -- Table containing the useful data of salesforce.natterbox_call_reporting_object__c
			
				o.createddate,
				o.sfid,
				o.createdbyid,
				o.relatedcontact__c,
				o.calltalkseconds__c,
				o.wrapup_string_1__c,
				o.wrapup_string_2__c,
				o.department__c,
				o.owner__c,
				o.ownerid,
				o.opportunity__c
			
			FROM
			
				salesforce.natterbox_call_reporting_object__c o
				
			WHERE
			
				o.relatedcontact__c IS NOT NULL
				AND o.createddate >= '2018-01-01'
				AND createddate < current_date
				
			ORDER BY
			
				o.createddate desc)
				
(SELECT
		
	TO_CHAR(t1.createddate, 'YYYY-MM') as year_month_opp_creation,
	MIN(t5.createddate) as createddate_likeli,
	MIN(t1.createddate) as createddate_opp,
	MAX(t4.onboarded_date) as onboarded_date,
	t1.sfid as opportunity,
	t1.customer__c as customer,
	t1.stagename,
	t1.status__c,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END as sector,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END as object_type,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END as potential_€,
	t7.createddate as date_call,
	t7.sfid as sfid_call,
	t7.calltalkseconds__c,
	-- SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	-- SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	-- SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	-- SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_cm,
	COUNT(DISTINCT t9.sfid) as cases_created
	
FROM

	t1
	
LEFT JOIN
	
	bi.potential_revenue_per_opp t2
		
ON

	t1.sfid = t2.opportunityid
	
LEFT JOIN -- Now joining a list containing the date when an opp went to the status "PENDING"
	
	(SELECT
	
		o.opportunityid,
		MIN(o.createddate) as onboarded_date
	
	FROM
	
		salesforce.opportunityfieldhistory o
		
	WHERE
	
		o.field = 'StageName'
		AND o.newvalue IN ('PENDING', 'WON')
		
	GROUP BY
	
		o.opportunityid) as t4
		
ON
	
	t1.sfid = t4.opportunityid	
	
LEFT JOIN

	salesforce.likeli__c t5
	
ON

	t1.customer__c = t5.customer__c
	
LEFT JOIN

	t7
	
ON

	t1.customer__c = t7.relatedcontact__c
	-- AND TO_CHAR(t1.createddate, 'YYYY-MM') = TO_CHAR(t7.createddate, 'YYYY-MM')
	
LEFT JOIN

	bi.opportunity_traffic_light_new t8
	
ON 

	t1.sfid = t8.opportunity
	
LEFT JOIN

	t9
	
ON

	t1.customer__c = t9.contactid

	
GROUP BY

	TO_CHAR(t1.createddate, 'YYYY-MM'),
	t1.customer__c,
	t1.stagename,
	t1.status__c,
	t1.sfid,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END,
	t7.createddate,
	t7.sfid,
	t7.calltalkseconds__c);					


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_temp_v2019;
CREATE TABLE bi.sector_analysis_temp_v2019 as 

WITH

	t1 AS (SELECT -- Table containing the useful data of salesforce.opportunity
	
				*
			 FROM
			 
			 	salesforce.opportunity
			 	
			 WHERE
			 
			 	createddate >= '2019-01-01'
			 	AND createddate < current_date
			 	AND test__c IS FALSE
				AND LEFT(locale__c, 2) = 'de'),
			 	
	t9 AS (SELECT -- Table containing the useful data of salesforce.case
	
				o.createddate,
				o.reason,
				o.ownerid,
				o.sfid,
				o.contactid,
				o.order__c,
				o.accountid,
				o.origin,
				o.type,
				o.opportunity__c,
				o.feedback_list__c,
				o.feedback_categories__c
			
			FROM
			
				salesforce.case o
			
			WHERE
			
				createddate >= '2019-01-01'
				AND createddate < current_date
				AND (o.createdbyid IN ('00520000003b8qzAAA', -- Ninja Tiger
				                      '00520000003IiNCAA0', -- API Tiger
											 '00520000003h5YhAAI', -- Marketing
											 '00520000003bc3gAAA') -- Accounting
					  OR LOWER(o.origin) LIKE '%b2b%'
					  OR o.type IN ('KA', 'B2B', 'Sales', 'PM')
					  OR o.opportunity__c IS NOT NULL)
			ORDER BY
			
				o.createddate desc),
				
	t7 AS (SELECT -- Table containing the useful data of salesforce.natterbox_call_reporting_object__c
			
				o.createddate,
				o.sfid,
				o.createdbyid,
				o.relatedcontact__c,
				o.calltalkseconds__c,
				o.wrapup_string_1__c,
				o.wrapup_string_2__c,
				o.department__c,
				o.owner__c,
				o.ownerid,
				o.opportunity__c
			
			FROM
			
				salesforce.natterbox_call_reporting_object__c o
				
			WHERE
			
				o.relatedcontact__c IS NOT NULL
				AND o.createddate >= '2019-01-01'
				AND createddate < current_date
				
			ORDER BY
			
				o.createddate desc)
				
(SELECT
		
	TO_CHAR(t1.createddate, 'YYYY-MM') as year_month_opp_creation,
	MIN(t5.createddate) as createddate_likeli,
	MIN(t1.createddate) as createddate_opp,
	MAX(t4.onboarded_date) as onboarded_date,
	t1.sfid as opportunity,
	t1.customer__c as customer,
	t1.stagename,
	t1.status__c,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END as sector,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END as object_type,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END as potential_€,
	t7.createddate as date_call,
	t7.sfid as sfid_call,
	t7.calltalkseconds__c,
	-- SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	-- SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	-- SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	-- SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_cm,
	COUNT(DISTINCT t9.sfid) as cases_created
	
FROM

	t1
	
LEFT JOIN
	
	bi.potential_revenue_per_opp t2
		
ON

	t1.sfid = t2.opportunityid
	
LEFT JOIN -- Now joining a list containing the date when an opp went to the status "PENDING"
	
	(SELECT
	
		o.opportunityid,
		MIN(o.createddate) as onboarded_date
	
	FROM
	
		salesforce.opportunityfieldhistory o
		
	WHERE
	
		o.field = 'StageName'
		AND o.newvalue IN ('PENDING', 'WON')
		
	GROUP BY
	
		o.opportunityid) as t4
		
ON
	
	t1.sfid = t4.opportunityid	
	
LEFT JOIN

	salesforce.likeli__c t5
	
ON

	t1.customer__c = t5.customer__c
	
LEFT JOIN

	t7
	
ON

	t1.customer__c = t7.relatedcontact__c
	-- AND TO_CHAR(t1.createddate, 'YYYY-MM') = TO_CHAR(t7.createddate, 'YYYY-MM')
	
LEFT JOIN

	bi.opportunity_traffic_light_new t8
	
ON 

	t1.sfid = t8.opportunity
	
LEFT JOIN

	t9
	
ON

	t1.customer__c = t9.contactid

	
GROUP BY

	TO_CHAR(t1.createddate, 'YYYY-MM'),
	t1.customer__c,
	t1.stagename,
	t1.status__c,
	t1.sfid,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END,
	t7.createddate,
	t7.sfid,
	t7.calltalkseconds__c);						
				
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_v2017_temp;
CREATE TABLE bi.sector_analysis_v2017_temp as 				
				
SELECT

	t6.year_month_opp_creation,
	t6.createddate_likeli,
	t6.createddate_opp,
	t6.onboarded_date,
	DATE_PART('day', t6.createddate_opp - t6.createddate_likeli)*24 + DATE_PART('hour', t6.createddate_opp - t6.createddate_likeli) as likeli_to_opp_hours,
	DATE_PART('day', t6.onboarded_date - t6.createddate_opp)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_opp) as opp_to_onboarded_hours,
	DATE_PART('day', t6.onboarded_date - t6.createddate_likeli)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_likeli) as likeli_to_onboarded_hours,
	t6.customer,
	t6.opportunity,
	t6.stagename,
	t6.status__c,
	t6.sector,
	t6.object_type,
	TRUNC(t6.potential_€::numeric, 1) as potential_€,
	SUM(CASE WHEN t6.date_call < t6.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	SUM(CASE WHEN t6.date_call < t6.onboarded_date THEN t6.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	SUM(CASE WHEN t6.date_call >= t6.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	SUM(CASE WHEN t6.date_call >= t6.onboarded_date THEN t6.calltalkseconds__c ELSE 0 END) as time_phone_cm,

	COUNT(DISTINCT t6.sfid_call) as total_calls,
	SUM(t6.calltalkseconds__c) as total_duration_calls,
	t6.cases_created
	
FROM

	bi.sector_analysis_temp_v2017 t6
	
GROUP BY

	t6.year_month_opp_creation,
	t6.createddate_likeli,
	t6.createddate_opp,
	t6.onboarded_date,
	DATE_PART('day', t6.createddate_opp - t6.createddate_likeli)*24 + DATE_PART('hour', t6.createddate_opp - t6.createddate_likeli),
	DATE_PART('day', t6.onboarded_date - t6.createddate_opp)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_opp),
	DATE_PART('day', t6.onboarded_date - t6.createddate_likeli)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_likeli),
	t6.customer,
	t6.opportunity,
	t6.stagename,
	t6.status__c,
	t6.sector,
	t6.object_type,
	TRUNC(t6.potential_€::numeric, 1),
	t6.cases_created;	
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_v2018_temp;
CREATE TABLE bi.sector_analysis_v2018_temp as 				
				
SELECT

	t6.year_month_opp_creation,
	t6.createddate_likeli,
	t6.createddate_opp,
	t6.onboarded_date,
	DATE_PART('day', t6.createddate_opp - t6.createddate_likeli)*24 + DATE_PART('hour', t6.createddate_opp - t6.createddate_likeli) as likeli_to_opp_hours,
	DATE_PART('day', t6.onboarded_date - t6.createddate_opp)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_opp) as opp_to_onboarded_hours,
	DATE_PART('day', t6.onboarded_date - t6.createddate_likeli)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_likeli) as likeli_to_onboarded_hours,
	t6.customer,
	t6.opportunity,
	t6.stagename,
	t6.status__c,
	t6.sector,
	t6.object_type,
	TRUNC(t6.potential_€::numeric, 1) as potential_€,
	SUM(CASE WHEN t6.date_call < t6.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	SUM(CASE WHEN t6.date_call < t6.onboarded_date THEN t6.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	SUM(CASE WHEN t6.date_call >= t6.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	SUM(CASE WHEN t6.date_call >= t6.onboarded_date THEN t6.calltalkseconds__c ELSE 0 END) as time_phone_cm,

	COUNT(DISTINCT t6.sfid_call) as total_calls,
	SUM(t6.calltalkseconds__c) as total_duration_calls,
	t6.cases_created
	
FROM

	bi.sector_analysis_temp_v2018 t6
	
GROUP BY

	t6.year_month_opp_creation,
	t6.createddate_likeli,
	t6.createddate_opp,
	t6.onboarded_date,
	DATE_PART('day', t6.createddate_opp - t6.createddate_likeli)*24 + DATE_PART('hour', t6.createddate_opp - t6.createddate_likeli),
	DATE_PART('day', t6.onboarded_date - t6.createddate_opp)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_opp),
	DATE_PART('day', t6.onboarded_date - t6.createddate_likeli)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_likeli),
	t6.customer,
	t6.opportunity,
	t6.stagename,
	t6.status__c,
	t6.sector,
	t6.object_type,
	TRUNC(t6.potential_€::numeric, 1),
	t6.cases_created;			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_v2019_temp;
CREATE TABLE bi.sector_analysis_v2019_temp as 				
				
SELECT

	t6.year_month_opp_creation,
	t6.createddate_likeli,
	t6.createddate_opp,
	t6.onboarded_date,
	DATE_PART('day', t6.createddate_opp - t6.createddate_likeli)*24 + DATE_PART('hour', t6.createddate_opp - t6.createddate_likeli) as likeli_to_opp_hours,
	DATE_PART('day', t6.onboarded_date - t6.createddate_opp)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_opp) as opp_to_onboarded_hours,
	DATE_PART('day', t6.onboarded_date - t6.createddate_likeli)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_likeli) as likeli_to_onboarded_hours,
	t6.customer,
	t6.opportunity,
	t6.stagename,
	t6.status__c,
	t6.sector,
	t6.object_type,
	TRUNC(t6.potential_€::numeric, 1) as potential_€,
	SUM(CASE WHEN t6.date_call < t6.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	SUM(CASE WHEN t6.date_call < t6.onboarded_date THEN t6.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	SUM(CASE WHEN t6.date_call >= t6.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	SUM(CASE WHEN t6.date_call >= t6.onboarded_date THEN t6.calltalkseconds__c ELSE 0 END) as time_phone_cm,

	COUNT(DISTINCT t6.sfid_call) as total_calls,
	SUM(t6.calltalkseconds__c) as total_duration_calls,
	t6.cases_created
	
FROM

	bi.sector_analysis_temp_v2019 t6
	
GROUP BY

	t6.year_month_opp_creation,
	t6.createddate_likeli,
	t6.createddate_opp,
	t6.onboarded_date,
	DATE_PART('day', t6.createddate_opp - t6.createddate_likeli)*24 + DATE_PART('hour', t6.createddate_opp - t6.createddate_likeli),
	DATE_PART('day', t6.onboarded_date - t6.createddate_opp)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_opp),
	DATE_PART('day', t6.onboarded_date - t6.createddate_likeli)*24 + DATE_PART('hour', t6.onboarded_date - t6.createddate_likeli),
	t6.customer,
	t6.opportunity,
	t6.stagename,
	t6.status__c,
	t6.sector,
	t6.object_type,
	TRUNC(t6.potential_€::numeric, 1),
	t6.cases_created;					
				
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_v2017;
CREATE TABLE bi.sector_analysis_v2017 as

SELECT

	t1.*,
	CASE WHEN 
			(DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24 IS NULL 
		  THEN 0
		  ELSE (DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24
		  END
	 as relationship_days
		
FROM
	
	bi.sector_analysis_v2017_temp t1
	
LEFT JOIN

	((SELECT
		
		LEFT(o.locale__c, 2) as country,
		o.locale__c,
		o.opportunityid,
		o.delivery_area__c,
		MIN(o.effectivedate) as date_start
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		
	GROUP BY
	
		o.opportunityid,
		o.locale__c,
		o.delivery_area__c,
		LEFT(o.locale__c, 2))) as t3
		
ON

	t1.opportunity = t3.opportunityid
	
LEFT JOIN

	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
						         -- It's the last day on which they are making money
		LEFT(o.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		-- AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(o.locale__c, 2),
		type_date,
		o.opportunityid) as t2
		
ON

	t1.opportunity = t2.opportunityid;
				
				
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_v2018;
CREATE TABLE bi.sector_analysis_v2018 as

SELECT

	t1.*,
	CASE WHEN 
			(DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24 IS NULL 
		  THEN 0
		  ELSE (DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24
		  END
	 as relationship_days
		
FROM
	
	bi.sector_analysis_v2018_temp t1
	
LEFT JOIN

	((SELECT
		
		LEFT(o.locale__c, 2) as country,
		o.locale__c,
		o.opportunityid,
		o.delivery_area__c,
		MIN(o.effectivedate) as date_start
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		
	GROUP BY
	
		o.opportunityid,
		o.locale__c,
		o.delivery_area__c,
		LEFT(o.locale__c, 2))) as t3
		
ON

	t1.opportunity = t3.opportunityid
	
LEFT JOIN

	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
						         -- It's the last day on which they are making money
		LEFT(o.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		-- AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(o.locale__c, 2),
		type_date,
		o.opportunityid) as t2
		
ON

	t1.opportunity = t2.opportunityid;	

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_v2019;
CREATE TABLE bi.sector_analysis_v2019 as

SELECT

	t1.*,
	CASE WHEN 
			(DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24 IS NULL 
		  THEN 0
		  ELSE (DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24
		  END
	 as relationship_days
		
FROM
	
	bi.sector_analysis_v2018_temp t1
	
LEFT JOIN

	((SELECT
		
		LEFT(o.locale__c, 2) as country,
		o.locale__c,
		o.opportunityid,
		o.delivery_area__c,
		MIN(o.effectivedate) as date_start
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		
	GROUP BY
	
		o.opportunityid,
		o.locale__c,
		o.delivery_area__c,
		LEFT(o.locale__c, 2))) as t3
		
ON

	t1.opportunity = t3.opportunityid
	
LEFT JOIN

	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
						         -- It's the last day on which they are making money
		LEFT(o.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		-- AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(o.locale__c, 2),
		type_date,
		o.opportunityid) as t2
		
ON

	t1.opportunity = t2.opportunityid;	
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sector_analysis_temp_v2017;
DROP TABLE IF EXISTS bi.sector_analysis_temp_v2018;
DROP TABLE IF EXISTS bi.sector_analysis_temp_v2019;
DROP TABLE IF EXISTS bi.sector_analysis_v2017_temp;
DROP TABLE IF EXISTS bi.sector_analysis_v2018_temp;
DROP TABLE IF EXISTS bi.sector_analysis_v2019_temp;
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$
LANGUAGE plpgsql		
				
				
				
				