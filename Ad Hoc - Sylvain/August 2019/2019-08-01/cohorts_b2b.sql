


SELECT

	t3.*,
	CASE 
  WHEN CAST(EXTRACT(YEAR FROM t3.mindate_order::date) as integer) = CAST(EXTRACT(YEAR FROM t3.first_order::date) as integer) THEN CAST(EXTRACT(MONTH FROM t3.mindate_order::date) as integer)- CAST(EXTRACT(MONTH FROM t3.first_order::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM t3.mindate_order::date) as integer) != CAST(EXTRACT(YEAR FROM t3.first_order::date) as integer) THEN (CAST(EXTRACT(YEAR FROM t3.mindate_order::date) as integer)-CAST(EXTRACT(YEAR FROM t3.first_order::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM t3.mindate_order::date) as integer)- CAST(EXTRACT(MONTH FROM t3.first_order::date) as integer) )
  ELSE 0 END as returning_month,
  CASE WHEN t3.money < 250 THEN '< 250€'
       WHEN t3.money >= 250 AND t3.money < 500 THEN '250-500€'
       ELSE '> 500€'
       END as category

FROM
		
	(SELECT
	
		TO_CHAR(t1.first_order, 'YYYY-MM') as month_first_order,
		t1.first_order,
		t1.country,
		t1.sfid_opp,
		t1.money,
		TO_CHAR(t2.effectivedate, 'YYYY-MM') as month_order,
		MIN(t2.effectivedate) as mindate_order,
		COUNT(DISTINCT t2.sfid) as orders
	
	FROM
		
		(SELECT
		
			MIN(oo.effectivedate) as first_order,
			LEFT(o.locale__c, 2) as country,
			o.delivery_area__c,
			o.sfid as sfid_opp,
			pot.potential as money
			
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce."order" oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp pot
			
		ON
		
			o.sfid = pot.opportunityid
			
		WHERE
		
			o.test__c IS FALSE
			AND oo.test__c IS FALSE
			AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND oo."type" = 'cleaning-b2b'
			
		GROUP BY
		
			LEFT(o.locale__c, 2),
			o.delivery_area__c,
			o.sfid,
			pot.potential) as t1
			
	LEFT JOIN
	
		salesforce."order" t2
			
	ON
	
		t1.sfid_opp = t2.opportunityid
			
	WHERE
		
		t2.test__c IS FALSE
		AND t2."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND t2."type" = 'cleaning-b2b'	
		
	GROUP BY
	
		TO_CHAR(t1.first_order, 'YYYY-MM'),
		t1.first_order,
		t1.country,
		t1.sfid_opp,
		t1.money,
		TO_CHAR(t2.effectivedate, 'YYYY-MM')) as t3	
		
			