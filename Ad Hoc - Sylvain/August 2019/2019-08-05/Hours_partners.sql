




SELECT

	TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
	o.served_by__c,
	a.name,
	o.delivery_area__c,
	SUM(o.order_duration__c) as hours


FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.account a
	
ON

	o.served_by__c = a.sfid
	
WHERE

	o."type" = 'cleaning-b2b'
	AND o.served_by__c NOT IN ('0012000001TDMgGAAX')
	AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
	AND o.test__c IS FALSE
	
GROUP BY

	o.delivery_area__c,
	o.served_by__c,
	a.name,
	TO_CHAR(o.effectivedate, 'YYYY-MM') 
