	


SELECT

	t1.opportunity_name,
	t1.opportunity,
	SUM(CASE WHEN TO_CHAR(o.date, 'YYYY-WW') = TO_CHAR(current_date, 'YYYY-WW')
	              AND t1.day_name LIKE '%monday%'
			   THEN t1.avg_traffic_light 
				ELSE 0 
				END) as traffic_light_this_week,
	SUM(CASE WHEN TO_CHAR(o.date, 'YYYY-WW') = TO_CHAR((current_date - 7), 'YYYY-WW')
	              AND t1.day_name LIKE '%sunday%'
			   THEN t1.avg_traffic_light 
				ELSE 0 
				END) as traffic_light_this_week,
	


FROM	
	
	(SELECT
		
		TO_CHAR(o.date, 'YYYY-WW')::text as week,
		TO_CHAR(o.date, 'day')::text as day_name,
		o.date,
		o.opportunity_name,
		o.opportunity,
		o.avg_traffic_light
	
	FROM
	
		bi.opportunity_traffic_light_new_v2 o
		
	WHERE
	
		TO_CHAR(o.date, 'day') LIKE '%monday%'
		OR TO_CHAR(o.date, 'day') LIKE '%sunday%'
		
	LIMIT 10000) as t1
		
	
		
