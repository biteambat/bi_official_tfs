


SELECT

	TO_CHAR(inv.issued__c, 'YYYY-MM') as year_month,
	COUNT(DISTINCT inv.sfid) as invoices,
	SUM(inv.amount__c) as revenue_brutto,
	SUM(inv.amount__c)/1.19 as revenue_netto

FROM

	salesforce.invoice__c inv
	
WHERE

	inv.opportunity__c IS NOT NULL
	AND inv.original_invoice__c IS NULL
	AND inv.parent_invoice__c IS NULL
	AND inv.test__c IS FALSE
	AND inv.issued__c IS NOT NULL
	
GROUP BY

	TO_CHAR(inv.issued__c, 'YYYY-MM') 
	
ORDER BY

	TO_CHAR(inv.issued__c, 'YYYY-MM') desc;