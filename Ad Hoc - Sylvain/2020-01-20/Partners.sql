



SELECT
	
	o.effectivedate,
	o.served_by__c,
	CASE WHEN o.served_by__c IS NULL THEN 'BAT Business Services GmbH' ELSE a.name END as name,
	COUNT(DISTINCT o.professional__c) as cleaners,
	COUNT(DISTINCT o.sfid) as orders,
	SUM(o.order_duration__c) as hours



FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.account a
	
ON

	o.served_by__c = a.sfid
	
WHERE

	o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
	AND o.effectivedate >= '2019-06-01'
	AND o.test__c IS FALSE
	AND a.test__c IS FALSE
	
GROUP BY

	o.effectivedate,
	o.served_by__c,
	a.name