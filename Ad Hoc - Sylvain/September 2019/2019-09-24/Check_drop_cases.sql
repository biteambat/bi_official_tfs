(SELECT 	
			
				createddate,
				Opportunity,
				sfid,
				SUM(createdcases)	as createdcases
		
			FROM
			
				(SELECT 	
				
					Opps.*,
					Inbound_Cases.sfid,
					Inbound_Cases.createddate,
					ROUND((CASE WHEN Inbound_Cases.opportunity IS NULL
									THEN (CASE WHEN 1.0 * createdcases / Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * createdcases / Opps END)
									ELSE (CASE WHEN 1.0 * createdcases IS NULL
												  THEN 0.00
												  ELSE 1.0 * createdcases END) END)::numeric, 2) as createdcases
		
				FROM 	
				
					bi.opportunitylist_traffic_light_v2 Opps
		
			   LEFT JOIN LATERAL
			   
					(SELECT 	
					
						cas.sfid,
						cas.createddate,
						cas.contactid as Contact,
						cas.opportunity__c as Opportunity,
						COUNT(1) as CreatedCases
		
					FROM 		
					
						salesforce.case cas
	
					WHERE	
					
						(cas.test__c = FALSE OR cas.test__c	IS NULL)				
						AND cas.createddate::date = (current_date - 8)
						-- AND cas.reason NOT IN ('Checkout')
						-- AND cas.createdbyid = '00520000003IiNCAA0' 
						AND cas.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint')
						AND cas.origin NOT IN ('System - Notification')
						AND opportunity__c = '0060J00000rxA0sQAE'
	
					GROUP BY  	
					
						cas.sfid,
						cas.createddate,
						cas.contactid,
						cas.opportunity__c) as Inbound_Cases 	
								
				ON 		
				
					(CASE WHEN Inbound_Cases.Opportunity IS NULL THEN Opps.Customer = Inbound_Cases.Contact 
							ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)) as Inbound_Cases_f
							
				WHERE
				
					Opportunity = '0060J00000rxA0sQAE'
		
			   GROUP BY Opportunity, 
				createddate,
				sfid)