	(SELECT
	 
	 	sfid,
	 	createddate,
		Contactid AS Contact,
		opportunity__c AS Opportunity,

		SUM(CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) as Cases_without_Opp,
		--
		SUM(CASE WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) as Retention_All,
		--
		SUM(CASE WHEN reason LIKE '%Customer - Retention%' 
		              AND isclosed = 'true' THEN 1 ELSE 0 END) as Retention_closed,
		--
		SUM(CASE WHEN reason LIKE '%Order - Invoice editing%' 
		              OR reason LIKE '%Order - payment / invoice%' 
						  OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) as InvoiceCorrection_All,
		--
		SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		                 OR reason LIKE '%Order - payment /invoice%'
		                 OR subject LIKE '%nvoice %orrection%'
		                 AND isclosed = 'true' THEN 1 ELSE 0 END) as InvoiceCorrection_closed,
		--
		SUM(CASE WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) as ProfessionalImprovement_All,
		SUM(CASE WHEN reason LIKE '%Professional - Improvement%'
						  AND isclosed = 'true' THEN 1 ELSE 0 END) as ProfessionalImprovement_closed,
						
		SUM(CASE WHEN reason LIKE '%Partner - Improvement%' THEN 1 ELSE 0 END) as PartnerImprovement_All,
		SUM(CASE WHEN reason LIKE '%Partner - Improvement%'
						  AND isclosed = 'true' THEN 1 ELSE 0 END) as PartnerImprovement_closed,
		SUM(CASE WHEN subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3') THEN 1 ELSE 0 END) as bad_feedback

		
	FROM

		Salesforce.case	interncases	

	WHERE
	
		-- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
		CreatedDate::date = (current_date - 8)
		AND Isdeleted 	= FALSE
		AND (test__c 	= FALSE   OR test__c	IS NULL)
		
		-- internal cases: 
		-- 	Customer - Retention 		> communication with sales
		-- 	Order - Invoice editing 	> communication with finance 
		-- 	Professional - Improvement > communication with TO
		
		AND (reason LIKE '%Customer - Retention%' 
			 OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 	
			 OR ((reason LIKE '%Professional - Improvement%'))
			 OR (reason LIKE '%Partner - Improvement%')
			 OR subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3'))
		AND opportunity__c = '0060J00000rxA0sQAE'
				


GROUP BY

	sfid,
	createddate,
	Contact,
	Opportunity) 