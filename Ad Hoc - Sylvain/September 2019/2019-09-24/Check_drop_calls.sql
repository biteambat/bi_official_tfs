SELECT
			
				Opps.opportunity,
				call.contact,
				ROUND ((CASE WHEN 1.0 * all_inbound_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * all_inbound_calls/opps END) ::numeric, 2) as all_inbound_calls,
				ROUND ((CASE WHEN 1.0 * lost_inbound_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * lost_inbound_calls/opps END) ::numeric, 2)	as lost_inbound_calls,
				ROUND ((CASE WHEN 1.0 * all_outbound/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * all_outbound/opps END) ::numeric, 2)	as all_outbound,
				ROUND ((CASE WHEN 1.0 * b2b_outbound/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * b2b_outbound/opps END) ::numeric, 2)	as b2b_outbound,
				ROUND ((CASE WHEN 1.0 * complaint_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * complaint_calls/opps END) ::numeric, 2)	as complaint_calls,					
				ROUND ((CASE WHEN 1.0 * no_complaint_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * no_complaint_calls/opps END) ::numeric, 2)	as no_complaint_calls					
					
			FROM 
			
				bi.opportunitylist_traffic_light_v2 Opps
			
			
			LEFT JOIN 
			LATERAL
			
				(SELECT
			
					RelatedContact__c as Contact,
					SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' AND t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
						      THEN 1 
						      ELSE 0 END) as all_inbound_calls,
					SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						      THEN 1 
						      ELSE 0 END) as lost_inbound_calls,
					SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
								THEN 1 
								ELSE 0 END)	as all_outbound,
					SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
								THEN 1 
								ELSE 0 END) as b2b_outbound,
					SUM(CASE WHEN t4.wrapup_string_1__c IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
								THEN 1 
								ELSE 0 END) as complaint_calls,
					SUM(CASE WHEN t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
								THEN 1 
								ELSE 0 END) as no_complaint_calls
		
				FROM 
				
					Salesforce.natterbox_call_reporting_object__c t4
				
				LEFT JOIN 
					
					salesforce.contact t5
			
				ON
				
					(t4.relatedcontact__c = t5.sfid)
					
				WHERE
				
					-- -- Calls created yesterday
					call_start_date_date__c::date = (current_date - 6)	
					-- call_start_date_date__c::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')					
					AND   t4.notes__c LIKE 'Call%'
					AND   t4.wrapup_string_2__c IN ('reached', 'not reached')					
					AND RelatedContact__c IS NOT NULL
				
				GROUP BY
				
					Contact) as Call
				
			ON
				
				(Opps.Customer = Call.Contact)
				
			WHERE
			
				opps.opportunity = '0060J00000wmQX5QAM'