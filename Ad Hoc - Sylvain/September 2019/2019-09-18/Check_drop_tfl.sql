


SELECT

	*

FROM

	bi.opportunity_traffic_light_drops o
	
WHERE

	o.opportunity LIKE '0060J00000x85kfQAA'
	AND EXTRACT(WEEK FROM o.date) = EXTRACT(WEEK FROM (current_date - (interval '1 week')))
	AND EXTRACT(YEAR FROM o.date) = EXTRACT(YEAR FROM (current_date - (interval '1 week')))