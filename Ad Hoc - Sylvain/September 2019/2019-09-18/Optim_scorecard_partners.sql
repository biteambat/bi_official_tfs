(SELECT -- START PART 2 -- This part is extracting from the table kpi_master. We create a table containing, for every partner, in every city:
						                                                      -- The revenue of last month
						                                                      -- The revenue of month - 2
						                                                      -- The cost of supply last month
						                                                      -- The operational costs of last month
						                                                      -- The hours executed of this month
						                                                      -- The active cleaners of this month
						                                                      -- The active cleaners of last month
						                                                      -- The active cleaners of month - 2
			
						o.date_part as year_month,
						o.sub_kpi_2 as partner2,
						o.city,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
									THEN o.value 
									ELSE 0 END) as revenue_last_month,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
						         THEN o.value 
									ELSE 0 END) as revenue_last_month2,
						SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
						         THEN o.value 
									ELSE 0 END) as revenue_last_month3,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
										(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
				                        THEN o.value
							               ELSE 0 END) 
										      END) as cost_supply_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as operational_costs_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_this_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_last_month,
						SUM(CASE WHEN 
							         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
							               THEN o.value
										      ELSE 0 END) IS NULL THEN 0
									ELSE 
									   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
						                  THEN o.value
									         ELSE 0 END) 
												END) as hours_executed_last_month2,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_this_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
									THEN o.value 
									ELSE 0 END) as active_pro_last_month,
						SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
						         THEN o.value 
									ELSE 0 END) as active_pro_last_month2									
														
					FROM
					
						bi.kpi_master o
						
					WHERE
					
						((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
						OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
						OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
						OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
						OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
						OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
											
					GROUP BY
					
						o.date_part,
						partner2,
						o.city
						
					ORDER BY
					
						year_month desc,
						partner2 asc)