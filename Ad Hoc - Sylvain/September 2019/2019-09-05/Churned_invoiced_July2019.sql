

SELECT

	o.sfid,
	o.name,
	o.grand_total__c,
	ooo.service_type__c,
	ooo.amount__c as amount_invoiced,
	MAX(oo.confirmed_end__c) as confirmed_end

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND ooo.issued__c::text = '2019-07-31'
	AND LEFT(oo.confirmed_end__c::text, 7) = '2019-07'
	
GROUP BY

	o.sfid,
	o.name,
	o.grand_total__c,
	ooo.service_type__c,
	ooo.amount__c