	

SELECT

	t1.opportunityid,
	t1.name,
	t1.grand_total__c,
	t1.start__c,
	t1.duration__c,
	t1.end__c,
	t1.resignation_date__c,
	t1.confirmed_end__c,
	t1.first_order

FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c) as t1
		
WHERE
	
	LEFT(t1.first_order::text, 7) = '2019-08'
	
	
