		
		
SELECT

	t3.served_by__c as sfid_partner,
	t3.partner,
	t3.delivery_area__c,
	SUM(t3.final_partner_cost) as total_cost

FROM	
	
	(SELECT	
		
		t1.*,
		t2.hours,
		CASE WHEN t1.partner_costs = 0 THEN t2.hours*t1.pph_partner ELSE t1.partner_costs END as final_partner_cost
		
	FROM
	
		(SELECT
		
			o.sfid as sfid_opp,
			o.name as name_opp,
			o.delivery_area__c,
			o.served_by__c,
			a.name as partner,
			a.pph__c as pph_partner,
			o.plan_pph__c,
			SUM(cont.grand_total__c) as grand_total,
			SUM(o.potential_partner_costs__c) as partner_costs
			
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.served_by__c = a.sfid
			
		WHERE
		
			o.status__c NOT IN ('RESIGNED', 'CANCELLED')
			AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')
			AND o.test__c IS FALSE
			AND cont.test__c IS FALSE
			AND cont.active__c IS TRUE
			AND o.served_by__c IS NOT NULL
			AND a.name NOT LIKE '%BAT %'
			
		GROUP BY
		
			o.sfid,
			o.name,
			o.delivery_area__c,
			o.served_by__c,
			o.plan_pph__c,
			a.pph__c,
			a.name) as t1
			
	LEFT JOIN
	
		(SELECT
	
			o.opportunityid,
			SUM(o.order_duration__c) as hours
		
		
		FROM
		
			salesforce.order o
			
		WHERE
		
			o.test__c IS FALSE
			AND o."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
			AND o.effectivedate BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '4 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
			
		GROUP BY
		
			o.opportunityid) as t2
			
	ON
	
		t1.sfid_opp = t2.opportunityid) as t3
		
GROUP BY	

	t3.served_by__c,
	t3.partner,
	t3.delivery_area__c
