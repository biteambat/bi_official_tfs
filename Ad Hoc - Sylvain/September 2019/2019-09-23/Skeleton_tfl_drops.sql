DELETE FROM 	bi.opportunity_traffic_light_new_v2 		WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO 	bi.opportunity_traffic_light_new_v2

SELECT		

	(CURRENT_DATE - INTERVAL '0 DAY')::date as date,
	-- , traffic_lights.*
	locale,
	customer,
	opportunity_name,
	opportunity,
	status,
	opps,
	owner,
	delivery_area,
	operated_by,
	operated_by_detail,
	orders_nsp,
	nsp,
	nsp_rate,
	orders_cp,
	cp,
	cp_rate,
	orders_cc,
	cc,
	cc_rate,
	orders_nsp_cp,
	nsp_cp,
	nso_cp_rate,
	createdcases,
	retention_all,
	retention_closed,
	invoicecorrection_all,
	invoicecorrection_closed,
	professionalimprovement_all,
	professionalimprovement_closed,
	partnerimprovement_all,
	partnerimprovement_closed,
	bad_feedback,
	all_inbound_calls,
	lost_inbound_calls,
	all_outbound,
	b2b_outbound,
	complaint_calls,
	no_complaint_calls,
	traffic_light_noshow,
	traffic_light_cancelled_pro,
	traffic_light_cancelled_customer,
	traffic_light_inbound_calls,
	traffic_light_complaint_calls,
	traffic_light_lost_calls,
	traffic_light_createdcases,
	traffic_light_retention,
	traffic_light_invoicecorrection,
	traffic_light_professionalimprovement,
	traffic_light_partnerimprovement,
	traffic_light_bad_feedback,
	
	-- AVG Traffic light:
	-- if opp status = RETENTION or OFFBOARDING -> traffic light has te be 1(red), same for noshow and bad feedback
	
	ROUND (CASE WHEN 	Status = 'RETENTION' 
						OR Status = 'OFFBOARDING'
						OR traffic_light_Retention = 1 
						OR traffic_light_noshow	= 1
						OR traffic_light_bad_feedback = 1 THEN 1 
				ELSE	((	traffic_light_cancelled_pro*3 
							+ traffic_light_inbound_calls*1
							+ traffic_light_lost_calls*1
							+ traffic_light_complaint_calls*3
							+ traffic_light_CreatedCases*3
							+ traffic_light_InvoiceCorrection*3 
							+ traffic_light_improvementsall*2
							) 
							/ 16) 
							END , 1) as AVG_Traffic_Light,
	traffic_light_improvementsall

FROM	

	(SELECT		
	
		traffic_light_detail.*,
		ROUND((CASE WHEN	nsp_Rate > 0 THEN 1
						ELSE 3 
						END), 1) as traffic_light_noshow,
		ROUND((CASE WHEN cp_Rate > 0 THEN 1
						ELSE 3 
						END), 1) as traffic_light_cancelled_pro,
		ROUND((CASE WHEN cc_Rate >= 0.2 THEN 1
						WHEN cc_Rate BETWEEN 0.005 AND 0.2 THEN 2
						WHEN cc_Rate <= 0.005 THEN 3 
						END), 1) as traffic_light_cancelled_customer,
		ROUND((CASE	WHEN all_inbound_calls >= 1 THEN 1
						WHEN all_inbound_calls < 1 OR (all_inbound_calls IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_inbound_calls,
		ROUND((CASE	WHEN complaint_calls >= 1 THEN 1
						WHEN complaint_calls < 1 OR (complaint_calls IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_complaint_calls,
		ROUND((CASE	WHEN lost_inbound_calls >= 1 THEN 1
						WHEN lost_inbound_calls < 1 OR (lost_inbound_calls IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_lost_calls,
		ROUND((CASE	WHEN createdcases >= 1 THEN 1
						WHEN createdcases	< 1 OR (createdcases IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_CreatedCases,
		ROUND((CASE	WHEN retention_all > 0 AND retention_closed <> retention_all THEN 1 -- Retention case open
						WHEN retention_all > 0 AND retention_closed = retention_all THEN 2 -- Retention case closed
						WHEN retention_all = 0 OR 	(retention_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_Retention,
		ROUND((CASE	WHEN invoicecorrection_all > 0 AND invoicecorrection_closed <> invoicecorrection_all THEN 1
						WHEN invoicecorrection_all > 0 AND invoicecorrection_closed = invoicecorrection_all THEN 2
						WHEN invoicecorrection_all = 0 OR (invoicecorrection_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_InvoiceCorrection,
		ROUND((CASE	WHEN professionalimprovement_all > 0 AND professionalimprovement_closed <> professionalimprovement_all THEN 1
						WHEN professionalimprovement_all > 0 AND professionalimprovement_closed = professionalimprovement_all THEN 2
						WHEN professionalimprovement_all = 0 OR (professionalimprovement_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_ProfessionalImprovement,					
		ROUND((CASE WHEN partnerimprovement_all > 0 AND partnerimprovement_closed <> partnerimprovement_all THEN 1
						WHEN partnerimprovement_all > 0 AND partnerimprovement_closed 	= partnerimprovement_all THEN 2
						WHEN 	partnerimprovement_all 	= 0 OR (partnerimprovement_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_PartnerImprovement,
		ROUND((CASE	WHEN (partnerimprovement_all > 0 or professionalimprovement_all > 0) AND (partnerimprovement_closed <> partnerimprovement_all OR professionalimprovement_closed <> professionalimprovement_all) THEN 1
						WHEN 	(partnerimprovement_all > 0) AND partnerimprovement_closed = partnerimprovement_all THEN 2
						WHEN 	(professionalimprovement_all > 0) AND professionalimprovement_closed = professionalimprovement_all THEN 2
						WHEN 	(partnerimprovement_all = 0 AND professionalimprovement_all = 0) OR (partnerimprovement_all IS null and professionalimprovement_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_Improvementsall,
		ROUND((CASE WHEN bad_feedback > 0 THEN 1
						ELSE 3 
						END), 1) as traffic_light_bad_feedback
									
	FROM 	
	
		(SELECT 	
		
			O.*,
			
			-- orders
			orders_final.orders_nsp,
			orders_final.nsp,
			(CASE WHEN orders_final.orders_nsp > 0 THEN orders_final.nsp/orders_final.orders_nsp ELSE 0 END) as nsp_Rate,
			orders_final.orders_cp,
			orders_final.cp,
			(CASE WHEN orders_final.orders_cp > 0 THEN orders_final.cp/orders_final.orders_cp ELSE 0 END) as cp_Rate,
			orders_final.orders_cc,
			orders_final.cc,
			(CASE WHEN orders_final.orders_cc > 0 THEN orders_final.cc/orders_final.orders_cc ELSE 0 END) as cc_Rate,
			orders_final.orders_nsp_cp,
			orders_final.nsp_cp,
			(CASE WHEN orders_final.orders_nsp_cp > 0 THEN orders_final.nsp_cp/orders_final.orders_nsp_cp ELSE 0 END) as nso_cp_Rate,
					
			-- inbound cases
			Inbound_Cases_final.createdcases,
					
			-- internal cases
			Intern_Cases_final.retention_all,
			Intern_Cases_final.retention_closed,
			Intern_Cases_final.invoicecorrection_all,
			Intern_Cases_final.invoicecorrection_closed,
			Intern_Cases_final.professionalimprovement_all,
			Intern_Cases_final.professionalimprovement_closed,
			Intern_Cases_final.partnerimprovement_all,
			Intern_Cases_final.partnerimprovement_closed,
			Intern_Cases_final.bad_feedback,
					
			-- calls
			Calls_final.all_inbound_calls,
			Calls_final.lost_inbound_calls,
			Calls_final.all_outbound,
			Calls_final.b2b_outbound,
			Calls_final.complaint_calls,
			Calls_final.no_complaint_calls
						


		FROM 	
		
			bi.opportunitylist_traffic_light_v2 O

		-- --
		-- -- orders
		-- -- Orders Order Start Date (effectivedate) within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')

		LEFT JOIN		
			
			(SELECT
			
				Opp,
				SUM(Orders_NSP) as Orders_NSP,
				SUM(NSP)	as NSP,
				SUM(Orders_CP)	as Orders_CP,
				SUM(CP) as CP,
				SUM(Orders_CC)	as Orders_CC,
				SUM(CC) as CC,
				SUM(Orders_NSP_CP) as Orders_NSP_CP,
				SUM(NSP_CP)	as NSP_CP
				
			 FROM 
			 
			 	(SELECT 
				
					opportunityid AS Opp,
					SUM(CASE WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) as Orders_NSP,
					SUM(CASE WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 ELSE 0 END) as NSP,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN status like '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) as Orders_CP,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 ELSE 0 END) as CP,
					SUM(CASE WHEN status LIKE '%CANCELLED CUSTOMER%' THEN 1 
								WHEN status LIKE '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) 	as Orders_CC,
					SUM(CASE WHEN status LIKE '%CANCELLED CUSTOMER%' THEN 1 ELSE 0 END) as CC,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) as Orders_NSP_CP,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 ELSE 0 END) as NSP_CP
									
				FROM
				
					salesforce."order" as orders
		
				WHERE
				
					effectivedate::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
					AND opportunityid IS NOT NULL
				
				GROUP BY
					
					Opp,
					delivery_area__c) as orders2
		
			GROUP BY
				
				Opp) as orders_final
		
		ON 
		
			(orders_final.opp = O.opportunity)		
-- --
-- -- inbound cases
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
-- 22/07/2019 --> MODIFICATIONS NEW TRAFFIC LIGHT -- We now take into account only the complaint cases
		
		LEFT JOIN 
		LATERAL
		
			(SELECT 	
			
				Opportunity,
				SUM(createdcases)	as createdcases
		
			FROM
			
				(SELECT 	
				
					Opps.*,
					ROUND((CASE WHEN Inbound_Cases.opportunity IS NULL
									THEN (CASE WHEN 1.0 * createdcases / Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * createdcases / Opps END)
									ELSE (CASE WHEN 1.0 * createdcases IS NULL
												  THEN 0.00
												  ELSE 1.0 * createdcases END) END)::numeric, 2) as createdcases
		
				FROM 	
				
					bi.opportunitylist_traffic_light_v2 Opps
		
			   LEFT JOIN LATERAL
			   
					(SELECT 	
					
						cas.contactid as Contact,
						cas.opportunity__c as Opportunity,
						COUNT(1) as CreatedCases
		
					FROM 		
					
						salesforce.case cas
	
					WHERE	
					
						(cas.test__c = FALSE OR cas.test__c	IS NULL)				
						AND cas.createddate::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
						-- AND cas.reason NOT IN ('Checkout')
						-- AND cas.createdbyid = '00520000003IiNCAA0' 
						AND cas.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint')
						AND cas.origin NOT IN ('System - Notification')
	
					GROUP BY  	
					
						cas.contactid,
						cas.opportunity__c) as Inbound_Cases 	
								
				ON 		
				
					(CASE WHEN Inbound_Cases.Opportunity IS NULL THEN Opps.Customer = Inbound_Cases.Contact 
							ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)) as Inbound_Cases_f
		
			   GROUP BY Opportunity) as Inbound_Cases_final 			
				
				
			ON 		
			
				(Inbound_Cases_final.Opportunity = O.Opportunity)

-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- -- Partner - Improvement

			LEFT JOIN

			(SELECT
			
				Opportunity,
				SUM(retention_all) as retention_all,
				SUM(Retention_closed) as Retention_closed,
				SUM(InvoiceCorrection_All) as InvoiceCorrection_All,
				SUM(InvoiceCorrection_closed)	as InvoiceCorrection_closed,
				SUM(ProfessionalImprovement_All)	as ProfessionalImprovement_All,
				SUM(ProfessionalImprovement_closed)	as ProfessionalImprovement_closed,
				SUM(PartnerImprovement_All) as PartnerImprovement_All,
				SUM(PartnerImprovement_closed) as PartnerImprovement_closed,
				SUM(bad_feedback) as bad_feedback
			
			
			FROM
			
				(SELECT
					
					Opps.*,
				   ROUND((CASE WHEN Intern.opportunity IS NULL
									THEN (CASE WHEN 1.0 * retention_all/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * retention_all/Opps END)
									ELSE (CASE WHEN 1.0 * retention_all IS NULL
												  THEN 0.00
												  ELSE 1.0 * retention_all END) END)::numeric, 2) as retention_all,
					ROUND((CASE WHEN Intern.opportunity IS NULL
								   THEN (CASE WHEN 1.0 * Retention_closed/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * Retention_closed / Opps END)
								   ELSE (CASE WHEN 1.0 * Retention_closed IS NULL
												  THEN 0.00
												  ELSE 1.0 * Retention_closed END) END)::numeric, 2) as Retention_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
								   THEN (CASE WHEN 1.0 * InvoiceCorrection_All/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * InvoiceCorrection_All/Opps END)
									ELSE (CASE WHEN 1.0 * InvoiceCorrection_All IS NULL
												  THEN 0.00
												  ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2) as InvoiceCorrection_All,
			 		ROUND((CASE WHEN Intern.opportunity IS NULL
								   THEN (CASE WHEN 1.0 * InvoiceCorrection_closed/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * InvoiceCorrection_closed/Opps END)
								   ELSE (CASE WHEN 1.0 * InvoiceCorrection_closed IS NULL
												  THEN 0.00
												  ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2) as InvoiceCorrection_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
									THEN (CASE WHEN 1.0 * ProfessionalImprovement_All/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * ProfessionalImprovement_All/Opps END)
							      ELSE (CASE WHEN 1.0 * ProfessionalImprovement_All IS NULL
												  THEN 0.00
												  ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2) as ProfessionalImprovement_All,
					ROUND((CASE WHEN Intern.opportunity IS NULL
							      THEN (CASE WHEN 1.0 * ProfessionalImprovement_closed/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * ProfessionalImprovement_closed/Opps END)
							      ELSE (CASE WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											     THEN 0.00
											     ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) as ProfessionalImprovement_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
							      THEN (CASE WHEN 1.0 * PartnerImprovement_All/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * PartnerImprovement_All/Opps END)
							      ELSE (CASE WHEN 1.0 * PartnerImprovement_All IS NULL
											     THEN 0.00
											     ELSE 1.0 * PartnerImprovement_All END) END)::numeric, 2) as PartnerImprovement_All,
					ROUND((CASE WHEN Intern.opportunity IS NULL
									THEN (CASE WHEN 1.0 * PartnerImprovement_closed/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * PartnerImprovement_closed/Opps END)
							      ELSE (CASE WHEN 1.0 * PartnerImprovement_closed IS NULL
											     THEN 0.00
											     ELSE 1.0 * PartnerImprovement_closed END) END)::numeric, 2) as PartnerImprovement_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
							      THEN (CASE WHEN 1.0 * bad_feedback/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * bad_feedback / Opps END)
							      ELSE (CASE WHEN 1.0 * bad_feedback IS NULL
											     THEN 0.00
											     ELSE 1.0 * bad_feedback END) END)::numeric, 2) as bad_feedback
		
				FROM 
				
					bi.opportunitylist_traffic_light_v2 Opps
		
	
				LEFT JOIN 
				LATERAL
				
					(SELECT
					 
						Contactid AS Contact,
						opportunity__c AS Opportunity,
				
						SUM(CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) as Cases_without_Opp,
						--
						SUM(CASE WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) as Retention_All,
						--
						SUM(CASE WHEN reason LIKE '%Customer - Retention%' 
						              AND isclosed = 'true' THEN 1 ELSE 0 END) as Retention_closed,
						--
						SUM(CASE WHEN reason LIKE '%Order - Invoice editing%' 
						              OR reason LIKE '%Order - payment / invoice%' 
										  OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) as InvoiceCorrection_All,
						--
						SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
						                 OR reason LIKE '%Order - payment /invoice%'
						                 OR subject LIKE '%nvoice %orrection%'
						                 AND isclosed = 'true' THEN 1 ELSE 0 END) as InvoiceCorrection_closed,
						--
						SUM(CASE WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) as ProfessionalImprovement_All,
						SUM(CASE WHEN reason LIKE '%Professional - Improvement%'
										  AND isclosed = 'true' THEN 1 ELSE 0 END) as ProfessionalImprovement_closed,
										
						SUM(CASE WHEN reason LIKE '%Partner - Improvement%' THEN 1 ELSE 0 END) as PartnerImprovement_All,
						SUM(CASE WHEN reason LIKE '%Partner - Improvement%'
										  AND isclosed = 'true' THEN 1 ELSE 0 END) as PartnerImprovement_closed,
						SUM(CASE WHEN subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3') THEN 1 ELSE 0 END) as bad_feedback
				
						
					FROM
		
						Salesforce.case	interncases	
		
					WHERE
					
						-- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
						CreatedDate::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
												AND (CURRENT_DATE - INTERVAL '0 DAY')	
						AND Isdeleted 	= FALSE
						AND (test__c 	= FALSE   OR test__c	IS NULL)
						
						-- internal cases: 
						-- 	Customer - Retention 		> communication with sales
						-- 	Order - Invoice editing 	> communication with finance 
						-- 	Professional - Improvement > communication with TO
						
						AND (reason LIKE '%Customer - Retention%' 
							 OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 	
							 OR ((reason LIKE '%Professional - Improvement%'))
							 OR (reason LIKE '%Partner - Improvement%')
							 OR subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3'))
								
		
		
				GROUP BY
				
					Contact,
					Opportunity) as Intern
		
			ON 
			
				(CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)) as Intern_f
		
		GROUP BY
		
			Opportunity) as Intern_Cases_final
		

	   ON 
		
			(Intern_Cases_final.opportunity = O.Opportunity)

-- --
-- -- calls:


		LEFT JOIN

			(SELECT
			
				Opps.opportunity,
				ROUND((CASE WHEN 1.0 * all_inbound_calls/opps IS NULL  
								THEN 0.00 
								ELSE 1.0 * all_inbound_calls/opps END) ::numeric, 2) as all_inbound_calls,
				ROUND((CASE WHEN 1.0 * lost_inbound_calls/opps IS NULL  
								THEN 0.00 
								ELSE 1.0 * lost_inbound_calls/opps END) ::numeric, 2)	as lost_inbound_calls,
				ROUND((CASE WHEN 1.0 * all_outbound/opps IS NULL  
								THEN 0.00 
								ELSE 1.0 * all_outbound/opps END) ::numeric, 2)	as all_outbound,
				ROUND((CASE WHEN 1.0 * b2b_outbound/opps IS NULL  
								THEN 0.00 
								ELSE 1.0 * b2b_outbound/opps END) ::numeric, 2)	as b2b_outbound,
				ROUND((CASE WHEN 1.0 * complaint_calls/opps IS NULL  
								THEN 0.00 
								ELSE 1.0 * complaint_calls/opps END) ::numeric, 2)	as complaint_calls,					
				ROUND((CASE WHEN 1.0 * no_complaint_calls/opps IS NULL  
								THEN 0.00 
								ELSE 1.0 * no_complaint_calls/opps END) ::numeric, 2)	as no_complaint_calls					
					
			FROM 
			
				bi.opportunitylist_traffic_light_v2 Opps
			
			
			LEFT JOIN 
			LATERAL
			
				(SELECT
			
					RelatedContact__c as Contact,
					SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						      THEN 1 
						      ELSE 0 END) as all_inbound_calls,
					SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						      THEN 1 
						      ELSE 0 END) as lost_inbound_calls,
					SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
								THEN 1 
								ELSE 0 END)	as all_outbound,
					SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
								THEN 1 
								ELSE 0 END) as b2b_outbound,
					SUM(CASE WHEN t4.wrapup_string_1__c IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
								THEN 1 
								ELSE 0 END) as complaint_calls,
					SUM(CASE WHEN t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
								THEN 1 
								ELSE 0 END) as no_complaint_calls
		
				FROM 
				
					Salesforce.natterbox_call_reporting_object__c t4
				
				LEFT JOIN 
					
					salesforce.contact t5
			
				ON
				
					(t4.relatedcontact__c = t5.sfid)
					
				WHERE
				
					-- -- Calls created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
					call_start_date_date__c::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')					
					AND 	t4.notes__c LIKE 'Call%'
					AND   t4.wrapup_string_2__c IN ('reached', 'not reached')					
					AND RelatedContact__c IS NOT NULL
				
				GROUP BY
				
					Contact) as Call
				
			ON
				
				(Opps.Customer = Call.Contact)) as Calls_final

		ON (Calls_final.Opportunity = O.Opportunity)) as traffic_light_detail) as traffic_lights
;

-- ----------------------------------------------------------------- 
-- Author: Sylvain Vanhuysse
-- Short Description: Opportunity Traffic Light TODAY. Synced with Salesforce Marketing Cloud. 
-- Created on: July 2019

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_today_v2;
CREATE TABLE 			bi.opportunity_traffic_light_today_v2	AS 

	SELECT		*
	FROM 		bi.opportunity_traffic_light_new_v2
	WHERE		date 												= (CURRENT_DATE - INTERVAL '0 DAY') 
;

-- ----------------------------------------------------------------- 
-- Author: Sylvain Vanhuysse
-- Short Description: 	Opportunity Traffic Light TODAY for tableau 
-- -------------------	(History and current AVG result in one table for tableau) 

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_tableau_v2;
CREATE TABLE 			bi.opportunity_traffic_light_tableau_v2 		AS 

	SELECT		history.date										AS history_date
				, history.locale									AS history_locale
				, history.customer									AS history_customer
				--, history.opportunity_name							AS history_opportunity_name
				, history.opportunity								AS history_opportunity
				, history.opps										AS history_opps
				, history.traffic_light_noshow						AS history_traffic_light_noshow
				, history.traffic_light_cancelled_pro				AS history_traffic_light_cancelled_pro
				, history.traffic_light_cancelled_customer			AS history_traffic_light_cancelled_customer
				, history.traffic_light_inbound_not_complaints				AS history_traffic_light_inbound_calls
				, history.traffic_light_lost_calls					AS history_traffic_light_lost_calls
				, history.traffic_light_complaint_calls         AS history_traffic_light_complaint_calls
				, history.traffic_light_createdcases				AS history_traffic_light_createdcases
				, history.traffic_light_retention					AS history_traffic_light_retention
				, history.traffic_light_invoicecorrection			AS history_traffic_light_invoicecorrection
				, history.traffic_light_professionalimprovement		AS history_traffic_light_professionalimprovement
				, history.traffic_light_partnerimprovement		AS history_traffic_light_improvementsall
				, history.traffic_light_improvementsall		AS history_traffic_light_partnerimprovement
				, history.traffic_light_bad_feedback		AS history_traffic_light_bad_feedback
				, history.avg_traffic_light							AS history_avg_traffic_light
	
				, current.date										AS current__date
				, current.opportunity								AS current_opportunity
				, current.opportunity_name							AS current_opportunity_name
				, current.delivery_area								AS current_delivery_area
				, current.status									AS current_status
				, current.owner										AS current_owner
				, current.operated_by								AS operated_by
				, current.operated_by_detail						AS operated_by_detail
				, current.traffic_light_noshow						AS current_traffic_light_noshow
				, current.traffic_light_cancelled_pro				AS current_traffic_light_cancelled_pro
				, current.traffic_light_cancelled_customer			AS current_traffic_light_cancelled_customer
				, current.traffic_light_inbound_not_complaints				AS current_traffic_light_inbound_calls
				, current.traffic_light_lost_calls					AS current_traffic_light_lost_calls
				, current.traffic_light_createdcases				AS current_traffic_light_createdcases
				, current.traffic_light_retention					AS current_traffic_light_retention
				, current.traffic_light_invoicecorrection			AS current_traffic_light_invoicecorrection
				, current.traffic_light_professionalimprovement		AS current_traffic_light_professionalimprovement
				, current.traffic_light_partnerimprovement		AS current_traffic_light_partnerimprovement
				, current.traffic_light_improvementsall		AS current_traffic_light_improvementsall
				, current.traffic_light_bad_feedback		AS current_traffic_light_bad_feedback
				, current.avg_traffic_light							AS current_avg_traffic_light
	
	FROM		bi.opportunity_traffic_light_new_v2 AS history
	
	LEFT JOIN 	(
				SELECT		*
				FROM		bi.opportunity_traffic_light_new_v2
				WHERE		date 							= (CURRENT_DATE - INTERVAL '0 DAY')
				) AS 		current							ON (history.opportunity = current.opportunity)
	
	WHERE		history.date 								BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
	
	;					




-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Calculations for the drops in TFL

DELETE FROM bi.opportunity_traffic_light_drops WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO bi.opportunity_traffic_light_drops

SELECT		

	(CURRENT_DATE - INTERVAL '0 DAY')::date as date, -- the drop calculated today is based on what happened yesterday
	-- , traffic_lights.*
	locale,
	customer,
	opportunity_name,
	opportunity,
	status,
	opps,
	owner,
	delivery_area,
	operated_by,
	operated_by_detail,
	orders_nsp,
	nsp,
	nsp_rate,
	orders_cp,
	cp,
	cp_rate,
	orders_cc,
	cc,
	cc_rate,
	orders_nsp_cp,
	nsp_cp,
	nso_cp_rate,
	createdcases,
	retention_all,
	retention_closed,
	invoicecorrection_all,
	invoicecorrection_closed,
	professionalimprovement_all,
	professionalimprovement_closed,
	partnerimprovement_all,
	partnerimprovement_closed,
	bad_feedback,
	all_inbound_calls,
	lost_inbound_calls,
	all_outbound,
	b2b_outbound,
	complaint_calls,
	no_complaint_calls,
	traffic_light_noshow,
	traffic_light_cancelled_pro,
	traffic_light_cancelled_customer,
	traffic_light_inbound_calls,
	traffic_light_complaint_calls,
	traffic_light_lost_calls,
	traffic_light_createdcases,
	traffic_light_retention,
	traffic_light_invoicecorrection,
	traffic_light_professionalimprovement,
	traffic_light_partnerimprovement,
	traffic_light_bad_feedback,
	-- AVG Traffic light:
	-- if opp status = RETENTION or OFFBOARDING -> traffic light has te be 1(red)
	-- The rest are equally weighted.
	ROUND(CASE WHEN Status = 'RETENTION' 
						OR Status = 'OFFBOARDING'
						OR traffic_light_Retention = 1 
						OR traffic_light_noshow	= 1
						OR traffic_light_bad_feedback = 1 THEN 1 
				  ELSE ((traffic_light_cancelled_pro*3 
							+ traffic_light_inbound_calls*1
							+ traffic_light_lost_calls*1
							+ traffic_light_complaint_calls*3
							+ traffic_light_CreatedCases*3
							+ traffic_light_InvoiceCorrection*3 
							+ traffic_light_Improvementsall*2) 
							/ 16) 
							END , 2)	as tfl_today,
	3 - ROUND(CASE WHEN 	Status = 'RETENTION' 
								OR Status = 'OFFBOARDING'
								OR traffic_light_Retention = 1 
								OR traffic_light_noshow	= 1
								OR traffic_light_bad_feedback = 1 THEN 1 
				      ELSE ((traffic_light_cancelled_pro*3 
								+ traffic_light_inbound_calls*1
								+ traffic_light_lost_calls*1
								+ traffic_light_complaint_calls*3
								+ traffic_light_CreatedCases*3
								+ traffic_light_InvoiceCorrection*3 
								+ traffic_light_Improvementsall*2) 
								/ 16) 
						END , 2)as drop_today,
	pot.potential

FROM	

	(SELECT		
	
		traffic_light_detail.*,
		ROUND((CASE WHEN	nsp_Rate > 0 THEN 1
						ELSE 3 
						END), 1) as traffic_light_noshow,
		ROUND((CASE WHEN cp_Rate > 0 THEN 1
						ELSE 3 
						END), 1) as traffic_light_cancelled_pro,
		ROUND((CASE WHEN cc_Rate >= 0.2 THEN 1
						WHEN cc_Rate BETWEEN 0.005 AND 0.2 THEN 2
						WHEN cc_Rate <= 0.005 THEN 3 
						END), 1) as traffic_light_cancelled_customer,
		ROUND((CASE	WHEN all_inbound_calls >= 1 THEN 1
						WHEN all_inbound_calls < 1 OR (all_inbound_calls IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_inbound_calls,
		ROUND((CASE	WHEN complaint_calls >= 1 THEN 1
						WHEN complaint_calls < 1 OR (complaint_calls IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_complaint_calls,
		ROUND((CASE	WHEN lost_inbound_calls >= 1 THEN 1
						WHEN lost_inbound_calls < 1 OR (lost_inbound_calls IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_lost_calls,
		ROUND((CASE	WHEN createdcases >= 1 THEN 1
						WHEN createdcases	< 1 OR (createdcases IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_CreatedCases,
		ROUND((CASE	WHEN retention_all > 0 AND retention_closed <> retention_all THEN 1 -- Retention case open
						WHEN retention_all > 0 AND retention_closed = retention_all THEN 2 -- Retention case closed
						WHEN retention_all = 0 OR 	(retention_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_Retention,
		ROUND((CASE	WHEN invoicecorrection_all > 0 AND invoicecorrection_closed <> invoicecorrection_all THEN 1
						WHEN invoicecorrection_all > 0 AND invoicecorrection_closed = invoicecorrection_all THEN 2
						WHEN invoicecorrection_all = 0 OR (invoicecorrection_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_InvoiceCorrection,
		ROUND((CASE	WHEN professionalimprovement_all > 0 AND professionalimprovement_closed <> professionalimprovement_all THEN 1
						WHEN professionalimprovement_all > 0 AND professionalimprovement_closed = professionalimprovement_all THEN 2
						WHEN professionalimprovement_all = 0 OR (professionalimprovement_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_ProfessionalImprovement,					
		ROUND((CASE WHEN partnerimprovement_all > 0 AND partnerimprovement_closed <> partnerimprovement_all THEN 1
						WHEN partnerimprovement_all > 0 AND partnerimprovement_closed 	= partnerimprovement_all THEN 2
						WHEN 	partnerimprovement_all 	= 0 OR (partnerimprovement_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_PartnerImprovement,
		ROUND((CASE	WHEN (partnerimprovement_all > 0 or professionalimprovement_all > 0) AND (partnerimprovement_closed <> partnerimprovement_all OR professionalimprovement_closed <> professionalimprovement_all) THEN 1
						WHEN 	(partnerimprovement_all > 0) AND partnerimprovement_closed = partnerimprovement_all THEN 2
						WHEN 	(professionalimprovement_all > 0) AND professionalimprovement_closed = professionalimprovement_all THEN 2
						WHEN 	(partnerimprovement_all = 0 AND professionalimprovement_all = 0) OR (partnerimprovement_all IS null and professionalimprovement_all IS NULL) = TRUE THEN 3 
						END), 1) as traffic_light_Improvementsall,
		ROUND((CASE WHEN bad_feedback > 0 THEN 1
						ELSE 3 
						END), 1) as traffic_light_bad_feedback
									
	FROM 	
	
		(SELECT 	
		
			O.*,
			
			-- orders
			orders_final.orders_nsp,
			orders_final.nsp,
			(CASE WHEN orders_final.orders_nsp > 0 THEN orders_final.nsp/orders_final.orders_nsp ELSE 0 END) as nsp_Rate,
			orders_final.orders_cp,
			orders_final.cp,
			(CASE WHEN orders_final.orders_cp > 0 THEN orders_final.cp/orders_final.orders_cp ELSE 0 END) as cp_Rate,
			orders_final.orders_cc,
			orders_final.cc,
			(CASE WHEN orders_final.orders_cc > 0 THEN orders_final.cc/orders_final.orders_cc ELSE 0 END) as cc_Rate,
			orders_final.orders_nsp_cp,
			orders_final.nsp_cp,
			(CASE WHEN orders_final.orders_nsp_cp > 0 THEN orders_final.nsp_cp/orders_final.orders_nsp_cp ELSE 0 END) as nso_cp_Rate,
					
			-- inbound cases
			Inbound_Cases_final.createdcases,
					
			-- internal cases
			Intern_Cases_final.retention_all,
			Intern_Cases_final.retention_closed,
			Intern_Cases_final.invoicecorrection_all,
			Intern_Cases_final.invoicecorrection_closed,
			Intern_Cases_final.professionalimprovement_all,
			Intern_Cases_final.professionalimprovement_closed,
			Intern_Cases_final.partnerimprovement_all,
			Intern_Cases_final.partnerimprovement_closed,
			Intern_Cases_final.bad_feedback,
					
			-- calls
			Calls_final.all_inbound_calls,
			Calls_final.lost_inbound_calls,
			Calls_final.all_outbound,
			Calls_final.b2b_outbound,
			Calls_final.complaint_calls,
			Calls_final.no_complaint_calls
						


		FROM 	
		
			bi.opportunitylist_traffic_light_v2 O

		-- --
		-- -- orders
		-- -- Orders Order Start Date = yesterday

		LEFT JOIN		
			
			(SELECT
				Opp,
				SUM(Orders_NSP) as Orders_NSP,
				SUM(NSP)	as NSP,
				SUM(Orders_CP)	as Orders_CP,
				SUM(CP) as CP,
				SUM(Orders_CC)	as Orders_CC,
				SUM(CC) as CC,
				SUM(Orders_NSP_CP) as Orders_NSP_CP,
				SUM(NSP_CP)	as NSP_CP
				
			 FROM 
			 
			 	(SELECT 
				
					opportunityid AS Opp,
					SUM(CASE WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) as Orders_NSP,
					SUM(CASE WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 ELSE 0 END) as NSP,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN status like '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) as Orders_CP,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 ELSE 0 END) as CP,
					SUM(CASE WHEN status LIKE '%CANCELLED CUSTOMER%' THEN 1 
								WHEN status LIKE '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) 	as Orders_CC,
					SUM(CASE WHEN status LIKE '%CANCELLED CUSTOMER%' THEN 1 ELSE 0 END) as CC,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%INVOICED%' THEN 1 
								WHEN status LIKE '%FULFILLED%' THEN 1 ELSE 0 END) as Orders_NSP_CP,
					SUM(CASE WHEN status LIKE '%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN status LIKE '%NOSHOW PROFESSIONAL%' THEN 1 ELSE 0 END) as NSP_CP
									
				FROM
				
					salesforce."order" as orders
		
				WHERE
				
					effectivedate::date = (current_date - 1)
					AND opportunityid IS NOT NULL
				
				GROUP BY
					
					Opp,
					delivery_area__c) as orders2
		
			GROUP BY
				
				Opp) as orders_final
		
		ON 
		
			(orders_final.opp = O.opportunity)		
-- --
-- -- inbound cases
-- -- Complaint cases created yesterday
-- 22/07/2019 --> MODIFICATIONS NEW TRAFFIC LIGHT -- We now take into account only the complaint cases
		
		LEFT JOIN 
		LATERAL
		
			(SELECT 	
			
				Opportunity,
				SUM(createdcases)	as createdcases
		
			FROM
			
				(SELECT 	
				
					Opps.*,
					ROUND((CASE WHEN Inbound_Cases.opportunity IS NULL
									THEN (CASE WHEN 1.0 * createdcases / Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * createdcases / Opps END)
									ELSE (CASE WHEN 1.0 * createdcases IS NULL
												  THEN 0.00
												  ELSE 1.0 * createdcases END) END)::numeric, 2) as createdcases
		
				FROM 	
				
					bi.opportunitylist_traffic_light_v2 Opps
		
			   LEFT JOIN LATERAL
			   
					(SELECT 	
					
						cas.contactid as Contact,
						cas.opportunity__c as Opportunity,
						COUNT(1) as CreatedCases
		
					FROM 		
					
						salesforce.case cas
	
					WHERE	
					
						(cas.test__c = FALSE OR cas.test__c	IS NULL)				
						AND cas.createddate::date = (current_date - 1)
						-- AND cas.reason NOT IN ('Checkout')
						-- AND cas.createdbyid = '00520000003IiNCAA0' 
						AND cas.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint')
						AND cas.origin NOT IN ('System - Notification')
	
					GROUP BY  	
					
						cas.contactid,
						cas.opportunity__c) as Inbound_Cases 	
								
				ON 		
				
					(CASE WHEN Inbound_Cases.Opportunity IS NULL THEN Opps.Customer = Inbound_Cases.Contact 
							ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)) as Inbound_Cases_f
		
			   GROUP BY Opportunity) as Inbound_Cases_final 			
				
				
			ON 		
			
				(Inbound_Cases_final.Opportunity = O.Opportunity)

-- --
-- -- internal cases:
-- -- Cases created yesterday
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- -- Partner - Improvement

			LEFT JOIN

			(SELECT
			
				Opportunity,
				SUM(retention_all) as retention_all,
				SUM(Retention_closed) as Retention_closed,
				SUM(InvoiceCorrection_All) as InvoiceCorrection_All,
				SUM(InvoiceCorrection_closed)	as InvoiceCorrection_closed,
				SUM(ProfessionalImprovement_All)	as ProfessionalImprovement_All,
				SUM(ProfessionalImprovement_closed)	as ProfessionalImprovement_closed,
				SUM(PartnerImprovement_All) as PartnerImprovement_All,
				SUM(PartnerImprovement_closed) as PartnerImprovement_closed,
				SUM(bad_feedback) as bad_feedback
			
			
			FROM
			
				(SELECT
					
					Opps.*,
				   ROUND((CASE WHEN Intern.opportunity IS NULL
									THEN (CASE WHEN 1.0 * retention_all/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * retention_all/Opps END)
									ELSE (CASE WHEN 1.0 * retention_all IS NULL
												  THEN 0.00
												  ELSE 1.0 * retention_all END) END)::numeric, 2) as retention_all,
					ROUND((CASE WHEN Intern.opportunity IS NULL
								   THEN (CASE WHEN 1.0 * Retention_closed/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * Retention_closed / Opps END)
								   ELSE (CASE WHEN 1.0 * Retention_closed IS NULL
												  THEN 0.00
												  ELSE 1.0 * Retention_closed END) END)::numeric, 2) as Retention_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
								   THEN (CASE WHEN 1.0 * InvoiceCorrection_All/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * InvoiceCorrection_All/Opps END)
									ELSE (CASE WHEN 1.0 * InvoiceCorrection_All IS NULL
												  THEN 0.00
												  ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2) as InvoiceCorrection_All,
			 		ROUND((CASE WHEN Intern.opportunity IS NULL
								   THEN (CASE WHEN 1.0 * InvoiceCorrection_closed/Opps IS NULL  
												  THEN 0.00 
												  ELSE 1.0 * InvoiceCorrection_closed/Opps END)
								   ELSE (CASE WHEN 1.0 * InvoiceCorrection_closed IS NULL
												  THEN 0.00
												  ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2) as InvoiceCorrection_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
									THEN (CASE WHEN 1.0 * ProfessionalImprovement_All/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * ProfessionalImprovement_All/Opps END)
							      ELSE (CASE WHEN 1.0 * ProfessionalImprovement_All IS NULL
												  THEN 0.00
												  ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2) as ProfessionalImprovement_All,
					ROUND((CASE WHEN Intern.opportunity IS NULL
							      THEN (CASE WHEN 1.0 * ProfessionalImprovement_closed/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * ProfessionalImprovement_closed/Opps END)
							      ELSE (CASE WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											     THEN 0.00
											     ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) as ProfessionalImprovement_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
							      THEN (CASE WHEN 1.0 * PartnerImprovement_All/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * PartnerImprovement_All/Opps END)
							      ELSE (CASE WHEN 1.0 * PartnerImprovement_All IS NULL
											     THEN 0.00
											     ELSE 1.0 * PartnerImprovement_All END) END)::numeric, 2) as PartnerImprovement_All,
					ROUND((CASE WHEN Intern.opportunity IS NULL
									THEN (CASE WHEN 1.0 * PartnerImprovement_closed/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * PartnerImprovement_closed/Opps END)
							      ELSE (CASE WHEN 1.0 * PartnerImprovement_closed IS NULL
											     THEN 0.00
											     ELSE 1.0 * PartnerImprovement_closed END) END)::numeric, 2) as PartnerImprovement_closed,
					ROUND((CASE WHEN Intern.opportunity IS NULL
							      THEN (CASE WHEN 1.0 * bad_feedback/Opps IS NULL  
											     THEN 0.00 
											     ELSE 1.0 * bad_feedback / Opps END)
							      ELSE (CASE WHEN 1.0 * bad_feedback IS NULL
											     THEN 0.00
											     ELSE 1.0 * bad_feedback END) END)::numeric, 2) as bad_feedback
		
				FROM 
				
					bi.opportunitylist_traffic_light_v2 Opps
		
	
				LEFT JOIN 
				LATERAL
				
					(SELECT
					 
						Contactid AS Contact,
						opportunity__c AS Opportunity,
				
						SUM(CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) as Cases_without_Opp,
						--
						SUM(CASE WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) as Retention_All,
						--
						SUM(CASE WHEN reason LIKE '%Customer - Retention%' 
						              AND isclosed = 'true' THEN 1 ELSE 0 END) as Retention_closed,
						--
						SUM(CASE WHEN reason LIKE '%Order - Invoice editing%' 
						              OR reason LIKE '%Order - payment / invoice%' 
										  OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) as InvoiceCorrection_All,
						--
						SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
						                 OR reason LIKE '%Order - payment /invoice%'
						                 OR subject LIKE '%nvoice %orrection%'
						                 AND isclosed = 'true' THEN 1 ELSE 0 END) as InvoiceCorrection_closed,
						--
						SUM(CASE WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) as ProfessionalImprovement_All,
						SUM(CASE WHEN reason LIKE '%Professional - Improvement%'
										  AND isclosed = 'true' THEN 1 ELSE 0 END) as ProfessionalImprovement_closed,
										
						SUM(CASE WHEN reason LIKE '%Partner - Improvement%' THEN 1 ELSE 0 END) as PartnerImprovement_All,
						SUM(CASE WHEN reason LIKE '%Partner - Improvement%'
										  AND isclosed = 'true' THEN 1 ELSE 0 END) as PartnerImprovement_closed,
						SUM(CASE WHEN subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3') THEN 1 ELSE 0 END) as bad_feedback
				
						
					FROM
		
						Salesforce.case	interncases	
		
					WHERE
					
						-- Cases created yesterday
						CreatedDate::date = (current_date - 1)	
						AND Isdeleted 	= FALSE
						AND (test__c 	= FALSE   OR test__c	IS NULL)
						
						-- internal cases: 
						-- 	Customer - Retention 		> communication with sales
						-- 	Order - Invoice editing 	> communication with finance 
						-- 	Professional - Improvement > communication with TO
						
						AND (reason LIKE '%Customer - Retention%' 
							 OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 	
							 OR ((reason LIKE '%Professional - Improvement%'))
							 OR (reason LIKE '%Partner - Improvement%')
							 OR subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3'))
								
		
		
				GROUP BY
				
					Contact,
					Opportunity) as Intern
		
			ON 
			
				(CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)) as Intern_f
		
		GROUP BY
		
			Opportunity) as Intern_Cases_final
		

	   ON 
		
			(Intern_Cases_final.opportunity = O.Opportunity)

-- --
-- -- calls:


		LEFT JOIN

			(SELECT
			
				Opps.opportunity,
				ROUND ((CASE WHEN 1.0 * all_inbound_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * all_inbound_calls/opps END) ::numeric, 2) as all_inbound_calls,
				ROUND ((CASE WHEN 1.0 * lost_inbound_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * lost_inbound_calls/opps END) ::numeric, 2)	as lost_inbound_calls,
				ROUND ((CASE WHEN 1.0 * all_outbound/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * all_outbound/opps END) ::numeric, 2)	as all_outbound,
				ROUND ((CASE WHEN 1.0 * b2b_outbound/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * b2b_outbound/opps END) ::numeric, 2)	as b2b_outbound,
				ROUND ((CASE WHEN 1.0 * complaint_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * complaint_calls/opps END) ::numeric, 2)	as complaint_calls,					
				ROUND ((CASE WHEN 1.0 * no_complaint_calls/opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * no_complaint_calls/opps END) ::numeric, 2)	as no_complaint_calls					
					
			FROM 
			
				bi.opportunitylist_traffic_light_v2 Opps
			
			
			LEFT JOIN 
			LATERAL
			
				(SELECT
			
					RelatedContact__c as Contact,
					SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						      THEN 1 
						      ELSE 0 END) as all_inbound_calls,
					SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						      THEN 1 
						      ELSE 0 END) as lost_inbound_calls,
					SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
								THEN 1 
								ELSE 0 END)	as all_outbound,
					SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
								THEN 1 
								ELSE 0 END) as b2b_outbound,
					SUM(CASE WHEN t4.wrapup_string_1__c IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
								THEN 1 
								ELSE 0 END) as complaint_calls,
					SUM(CASE WHEN t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
								THEN 1 
								ELSE 0 END) as no_complaint_calls
		
				FROM 
				
					Salesforce.natterbox_call_reporting_object__c t4
				
				LEFT JOIN 
					
					salesforce.contact t5
			
				ON
				
					(t4.relatedcontact__c = t5.sfid)
					
				WHERE
				
					-- -- Calls created yesterday
					call_start_date_date__c::date = (current_date - 1)				
					AND 	t4.notes__c LIKE 'Call%'
					AND   t4.wrapup_string_2__c IN ('reached', 'not reached')					
					AND RelatedContact__c IS NOT NULL
				
				GROUP BY
				
					Contact) as Call
				
			ON
				
				(Opps.Customer = Call.Contact)) as Calls_final

		ON (Calls_final.Opportunity = O.Opportunity)) as traffic_light_detail) as traffic_lights

LEFT JOIN

	bi.potential_revenue_per_opp as pot

ON

	traffic_lights.opportunity = pot.opportunityid

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Second table allowing to calculate the drops of traffic light
-- Created on: 12/09/2019

DROP TABLE IF EXISTS bi.tfl_drops;
CREATE TABLE bi.tfl_drops AS 

SELECT

	year,
	week,
	mindate,
	opportunity_name,
	opportunity,
	status,
	owner,
	delivery_area,
	potential,
	CASE WHEN drop_tfl > 2 THEN 2 ELSE drop_tfl END as drop_tfl

FROM
	
	(SELECT
	
		date_part('year', o.date) as year,
		date_part('week', o.date) as week,
		MIN(o.date) as mindate,
		o.opportunity_name,
		o.opportunity,
		o."status",
		o.owner,
		o.delivery_area,
		MAX(o.potential) as potential,
		SUM(o.drop_today) as drop_tfl
			
	FROM
	
		bi.opportunity_traffic_light_drops o
		
	GROUP BY
	
		date_part('year', o.date),
		date_part('week', o.date),
		o.opportunity_name,
		o.opportunity,
		o."status",
		o.owner,
		o.delivery_area
		
	ORDER BY
	
		o.opportunity_name) as t1;
