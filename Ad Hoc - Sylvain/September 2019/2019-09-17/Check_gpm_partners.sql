


SELECT

	o.sfid,
	o.name,
	o.grand_total__c,
	o.potential_partner_costs__c,
	o.grand_total__c - o.potential_partner_costs__c as gp,
	CASE WHEN grand_total__c != 0 
	     THEN (o.grand_total__c - o.potential_partner_costs__c)/o.grand_total__c
	     ELSE -1000
		  END as gpm


FROM

	salesforce.opportunity o
	
WHERE

	o.potential_partner_costs__c IS NOT NULL
	AND o.test__c IS FALSE
	AND (CASE WHEN grand_total__c != 0 
	     THEN (o.grand_total__c - o.potential_partner_costs__c)/o.grand_total__c
	     ELSE -1000
		  END) != -1000
	AND o.potential_partner_costs__c > 0
	AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')