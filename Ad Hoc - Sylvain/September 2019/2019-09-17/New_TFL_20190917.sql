
DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_others_ops(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_others_ops';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- This function contains Queries related to the performacne of the Operations-team
-- For example the 
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: list of all opps which should get an traffic light
-- Created on: 04/06/2019
DROP TABLE IF EXISTS 	bi.opportunitylist_traffic_light;
CREATE TABLE 			bi.opportunitylist_traffic_light 			AS 

SELECT		locale__c 															AS locale
			, customer__c														AS Customer
			, opp.name															AS Opportunity_Name
			, opp.sfid															AS Opportunity
			, status__c															AS Status
			, CAST (COUNT (opp.sfid) OVER (PARTITION BY customer__c) 	AS BIGINT)				AS Opps
			, tuser.name														AS Owner		
--			, delivery_area__c													AS delivery_area_opp
			, CASE 	WHEN Opp.delivery_area__c 	IS NOT NULL 	THEN Opp.delivery_area__c
															ELSE 'unknown' 	END AS delivery_area
			, last_order.operated_by
			, last_order.operated_by_detail

FROM 		Salesforce.opportunity 			Opp
		
LEFT JOIN 	salesforce.user 				tuser 		ON (Opp.ownerid = tuser.sfid)

-- --
-- -- last order details
-- -- served by BAT (TFS) or Partner

LEFT JOIN

		(SELECT DISTINCT ON (opportunityid)
				opportunityid AS Opp
				, effectivedate::date 
				, sfid AS orderid
				, delivery_area__c
				, professional__c  AS Professional
				, operated_by
				, operated_by_detail

		FROM   salesforce."order" 			AS orders
		
		LEFT JOIN
			(SELECT 	Professional.sfid 																		AS professional
				, CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE 'Partner' 		END AS operated_by
				, CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE Partner.name 	END AS operated_by_detail
		
			FROM 		Salesforce.Account Professional
		
			LEFT JOIN 	Salesforce.Account Partner 		ON (Professional.parentid = Partner.sfid)
			
			)			AS ProfessionalDetails 			ON (orders.professional__c = ProfessionalDetails.professional)
		
		
		WHERE 	opportunityid 		IS NOT NULL
		
		ORDER  BY 	opportunityid 
					, effectivedate::date DESC
		
		) AS last_order 								ON (last_order.Opp = Opp.sfid)


WHERE 	test__c = false 
		AND status__c 		IN 		('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')

GROUP BY 	locale__c
			, opp.delivery_area__c
			, customer__c
			, opp.name		
			, opp.sfid	
			, status__c
			, owner
			, last_order.operated_by
			, last_order.operated_by_detail
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- new Opportunity Traffic Light based on Opportunities NOT contacts

DELETE FROM 	bi.opportunity_traffic_light_new 		WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO 	bi.opportunity_traffic_light_new

	SELECT		(CURRENT_DATE - INTERVAL '0 DAY')	::date						AS date
				, traffic_lights.*
	
				-- AVG Traffic light:
				-- if opp status = RETENTION or OFFBOARDING -> traffic light has te be 1(red)
				-- Order related Traffic Lights are trible-weightes. 
				-- The rest are equally weighted.
				, ROUND (CASE WHEN 	Status 										= 'RETENTION' 
									OR Status 									= 'OFFBOARDING'
									OR 	traffic_light_Retention 				= 1 						THEN 1 
							ELSE	((	( traffic_light_noshow + traffic_light_cancelled_pro)*3 
										+ traffic_light_inbound_calls 
										+ traffic_light_lost_calls 
										+ traffic_light_CreatedCases
										+ traffic_light_InvoiceCorrection 
										+ traffic_light_ProfessionalImprovement 
										+ (CASE WHEN traffic_light_Retention = 2 							THEN 1 
												ELSE traffic_light_Retention END )) 
										/ 12) 
										END , 1) 															AS AVG_Traffic_Light

	FROM	(
			SELECT		traffic_light_detail.*
						, ROUND ((CASE	WHEN	nsp_Rate 						>= 0.05 					THEN 1
										WHEN	nsp_Rate 						BETWEEN 0.005 AND 0.05 		THEN 2
										WHEN	nsp_Rate 						<= 0.005 					THEN 3 
										END), 1)  															AS traffic_light_noshow
										
						, ROUND ((CASE	WHEN 	cp_Rate 						>= 0.1 						THEN 1
										WHEN 	cp_Rate 						BETWEEN 0.005 AND 0.1 		THEN 2
										WHEN 	cp_Rate 						<= 0.005 					THEN 3 
										END), 1)  															AS traffic_light_cancelled_pro
						
						, ROUND ((CASE	WHEN 	cc_Rate 						>= 0.2 						THEN 1
										WHEN 	cc_Rate 						BETWEEN 0.005 AND 0.2 		THEN 2
										WHEN 	cc_Rate 						<= 0.005 					THEN 3 
										END), 1) 															AS traffic_light_cancelled_customer
														
						, ROUND ((CASE	WHEN 	all_inbound_calls 				>= 3 						THEN 1
										WHEN 	all_inbound_calls 				BETWEEN 1 AND 3 			THEN 2
										WHEN 	all_inbound_calls 				<= 1 
											OR 	(all_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_inbound_calls
										
						, ROUND ((CASE	WHEN 	lost_inbound_calls				>= 3 						THEN 1
										WHEN 	lost_inbound_calls 				BETWEEN 1 AND 4 			THEN 2
										WHEN 	lost_inbound_calls <= 1 
											OR 	(lost_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_lost_calls
										
						, ROUND ((CASE	WHEN 	createdcases 					>= 3 						THEN 1
										WHEN 	createdcases 					BETWEEN 1 AND 3 			THEN 2
										WHEN 	createdcases					<=1 
											OR 	(createdcases IS NULL) 			= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_CreatedCases
										
						, ROUND ((CASE	WHEN 	retention_all 					> 0 
											AND retention_closed 				<> retention_all 			THEN 1 -- Retention case open
										WHEN 	retention_all 					> 0 
											AND retention_closed 				= retention_all 			THEN 2 -- Retention case closed
										WHEN 	retention_all 					= 0 
											OR 	(retention_all IS NULL) 		= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_Retention
								
						, ROUND ((CASE	WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		<> invoicecorrection_all 	THEN 1
										WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		= invoicecorrection_all 	THEN 1
										WHEN 	invoicecorrection_all 			= 0 
											OR 	(invoicecorrection_all IS NULL) = TRUE 						THEN 3 
										END), 1) 															AS traffic_light_InvoiceCorrection
						
						, ROUND	((CASE	WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	<> professionalimprovement_all 	THEN 1
										WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	= professionalimprovement_all 	THEN 1
										WHEN 	professionalimprovement_all 	= 0 
											OR (professionalimprovement_all IS NULL) = TRUE 					THEN 3 
										END), 1) 															AS traffic_light_ProfessionalImprovement


			FROM 	(
					SELECT 	O.*
			
					-- orders
							, orders_final.orders_nsp
							, orders_final.nsp
							, (CASE 	WHEN orders_final.orders_nsp 	> 0 		THEN orders_final.nsp/orders_final.orders_nsp 			ELSE 0 	END) 	AS nsp_Rate
							
							, orders_final.orders_cp
							, orders_final.cp
							, (CASE 	WHEN orders_final.orders_cp 	> 0 		THEN orders_final.cp/orders_final.orders_cp 			ELSE 0 	END) 	AS cp_Rate
							
							, orders_final.orders_cc
							, orders_final.cc
							, (CASE 	WHEN orders_final.orders_cc 	> 0 		THEN orders_final.cc/orders_final.orders_cc 			ELSE 0 	END) 	AS cc_Rate
							
							, orders_final.orders_nsp_cp
							, orders_final.nsp_cp
							, (CASE 	WHEN orders_final.orders_nsp_cp > 0 		THEN orders_final.nsp_cp/orders_final.orders_nsp_cp 	ELSE 0 	END) 	AS nso_cp_Rate
							
					-- inbound cases
							, Inbound_Cases_final.createdcases
							
					-- internal cases
							, Intern_Cases_final.retention_all
							, Intern_Cases_final.retention_closed
							, Intern_Cases_final.invoicecorrection_all
							, Intern_Cases_final.invoicecorrection_closed
							, Intern_Cases_final.professionalimprovement_all
							, Intern_Cases_final.professionalimprovement_closed
							
					-- calls
							, Calls_final.all_inbound_calls
							, Calls_final.lost_inbound_calls
							, Calls_final.all_outbound
							, Calls_final.b2b_outbound


FROM 	bi.opportunitylist_traffic_light O


-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')

LEFT JOIN		
		
		(
		SELECT
		Opp
		, SUM(Orders_NSP)			AS Orders_NSP
		, SUM(NSP)					AS NSP
		, SUM(Orders_CP)			AS Orders_CP
		, SUM(CP)					AS CP
		, SUM(Orders_CC)			AS Orders_CC
		, SUM(CC)					AS CC
		, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
		, SUM(NSP_CP)				AS NSP_CP
		
		FROM (
		SELECT 
		
		opportunityid AS Opp
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
						WHEN status like '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
									
		FROM
		salesforce."order" AS orders
		
		WHERE
		effectivedate::date 	BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
									AND (CURRENT_DATE - INTERVAL '0 DAY')
											
		AND opportunityid IS NOT NULL
		
		GROUP BY
		Opp
		, delivery_area__c 
		
		) AS orders2
		
		GROUP BY
		Opp
		
) AS orders_final
		
ON (orders_final.opp = O.opportunity)

		
-- --
-- -- inbound cases
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
		
LEFT JOIN 	
LATERAL
		
		(
		SELECT 	Opportunity
				, SUM(createdcases)								AS createdcases
		
		FROM
			(
			SELECT 	Opps.*
					, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
									THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
												THEN 0.00 
												ELSE 1.0 * createdcases / Opps END)
									ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
												THEN 0.00
												ELSE 1.0 * createdcases END) END)::numeric, 2)		AS createdcases
		
			FROM 	bi.opportunitylist_traffic_light 		Opps
		
			LEFT JOIN LATERAL
				(SELECT 	cas.contactid 						AS Contact
							, cas.opportunity__c 					AS Opportunity
							, COUNT(1) 							AS CreatedCases
		
				FROM 		salesforce.case 		cas
	
				WHERE	(cas.test__c				= FALSE  
						OR cas.test__c				IS NULL)
						
						AND (	
						
						-- old case setup (3 AND NOT 11)
								(
									(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
											WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
											WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
											WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
											WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
								AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
											WHEN 	cas.type 	= 'CLM'					THEN 1
											WHEN 	cas.type	= 'CM B2C'				THEN 1
											WHEN 	cas.type	= 'Sales'				THEN 1
											WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
								)
							OR	(
									(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
											WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
											WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
											WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
								AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
											WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
								)
								
						-- new case setup
							OR	(	
									(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
								AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
											WHEN 	cas.type 	= 'Pool'				THEN 1
											WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
								)
								
							OR 	(
									(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
											WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
								)
							)
	
				
							AND cas.createddate::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
							AND cas.reason NOT IN ('Checkout')
							
							AND cas.createdbyid = '00520000003IiNCAA0' 
	
				GROUP BY  	cas.contactid
							, cas.opportunity__c
		
				) AS Inbound_Cases 			ON 		(CASE 	WHEN Inbound_Cases.Opportunity 	IS NULL 
															THEN Opps.Customer 				= Inbound_Cases.Contact 
															ELSE Inbound_Cases.Opportunity 	= Opps.Opportunity END)
				
			) AS Inbound_Cases_f
		
			GROUP BY Opportunity
		
		) AS Inbound_Cases_final 			ON 		(Inbound_Cases_final.Opportunity = O.Opportunity)

-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO

LEFT JOIN

		(SELECT
		Opportunity
		, SUM(retention_all)						AS retention_all
		, SUM(Retention_closed)						AS Retention_closed
		, SUM(InvoiceCorrection_All)				AS InvoiceCorrection_All
		, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
		, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
		, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
		
		FROM
		(
		SELECT
		Opps.*
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * retention_all / Opps END)
							ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
											THEN 0.00
											ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * Retention_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
											THEN 0.00
											ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed
		
		FROM bi.opportunitylist_traffic_light Opps
		
	
		LEFT JOIN 
		LATERAL
		
		(
		SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity

		, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
		AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment / invoice%'
		OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment /invoice%'
		OR subject LIKE '%nvoice %orrection%'
		AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
		
		FROM
		Salesforce.case	interncases	
		
		WHERE
			-- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
			CreatedDate::date between (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
									AND (CURRENT_DATE - INTERVAL '0 DAY')	
			
			AND Isdeleted 	= FALSE
			AND (test__c 	= FALSE   OR test__c	IS NULL)
			
			-- internal cases: 
			-- 	Customer - Retention 		> communication with sales
			-- 	Order - Invoice editing 	> communication with finance 
			-- 	Professional - Improvement > communication with TO
			
			AND ((reason LIKE '%Customer - Retention%' 
						OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 
						
					OR (reason LIKE '%Professional - Improvement%' 
						AND (type LIKE '%TO%' OR type LIKE '%CLM%'))))
		
		
		GROUP BY
		Contact
		, Opportunity
		
		) AS Intern
		
		ON (CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)
				
		)		AS Intern_f
		
		GROUP BY
		Opportunity
		
) AS Intern_Cases_final
		
ON (Intern_Cases_final.opportunity = O.Opportunity)


-- --
-- -- calls:


LEFT JOIN

(
SELECT
Opps.opportunity
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)										AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)										AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
					

FROM bi.opportunitylist_traffic_light Opps


		LEFT JOIN 
		LATERAL
		(		
		SELECT

		RelatedContact__c 													AS Contact
		
	
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END)
			+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						THEN 1 
						ELSE 0 END) 											AS all_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END) 											AS lost_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
						THEN 1 
						ELSE 0 END)												AS all_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS b2b_outbound
		
		FROM 
		Salesforce.natterbox_call_reporting_object__c t4
		
			LEFT JOIN 
			salesforce.contact t5
			
			ON
			(t4.relatedcontact__c = t5.sfid)
		WHERE
		-- -- Calls created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
		call_start_date_date__c::date 	BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
													AND (CURRENT_DATE - INTERVAL '0 DAY')
		
		AND 	(	(calldirection__c = 'Inbound' 	AND E164Callednumber__C IN ('493030807263', '493030807264'))
				OR (calldirection__c = 'Outbound' 	AND Callerfirstname__c LIKE 'CM%'))
				
		AND RelatedContact__c IS NOT NULL
		
		GROUP BY
		Contact) as Call
		
		ON
		(Opps.Customer = Call.Contact)	
		
) AS Calls_final

ON (Calls_final.Opportunity = O.Opportunity)


) AS traffic_light_detail

) AS traffic_lights

;

-- ----------------------------------------------------------------- 
-- Author: Christina Janson
-- Short Description: Opportunity Traffic Light TODAY. Synced with Salesforce Marketing Cloud. 
-- Created on: 19/10/2018

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_today;
CREATE TABLE 			bi.opportunity_traffic_light_today 			AS 

	SELECT		*
	FROM 		bi.opportunity_traffic_light_new
	WHERE		date 												= (CURRENT_DATE - INTERVAL '0 DAY') 
;

-- ----------------------------------------------------------------- 
-- Author: Christina Janson
-- Short Description: 	Opportunity Traffic Light TODAY for tableau 
-- -------------------	(History and current AVG result in one table for tableau) 

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_tableau;
CREATE TABLE 			bi.opportunity_traffic_light_tableau 		AS 

	SELECT		history.date										AS history_date
				, history.locale									AS history_locale
				, history.customer									AS history_customer
				--, history.opportunity_name							AS history_opportunity_name
				, history.opportunity								AS history_opportunity
				, history.opps										AS history_opps
				, history.traffic_light_noshow						AS history_traffic_light_noshow
				, history.traffic_light_cancelled_pro				AS history_traffic_light_cancelled_pro
				, history.traffic_light_cancelled_customer			AS history_traffic_light_cancelled_customer
				, history.traffic_light_inbound_calls				AS history_traffic_light_inbound_calls
				, history.traffic_light_lost_calls					AS history_traffic_light_lost_calls
				, history.traffic_light_createdcases				AS history_traffic_light_createdcases
				, history.traffic_light_retention					AS history_traffic_light_retention
				, history.traffic_light_invoicecorrection			AS history_traffic_light_invoicecorrection
				, history.traffic_light_professionalimprovement		AS history_traffic_light_professionalimprovement
				, history.avg_traffic_light							AS history_avg_traffic_light
	
				, current.date										AS current__date
				, current.opportunity								AS current_opportunity
				, current.opportunity_name							AS current_opportunity_name
				, current.delivery_area								AS current_delivery_area
				, current.status									AS current_status
				, current.owner										AS current_owner
				, current.operated_by								AS operated_by
				, current.operated_by_detail						AS operated_by_detail
				, current.traffic_light_noshow						AS current_traffic_light_noshow
				, current.traffic_light_cancelled_pro				AS current_traffic_light_cancelled_pro
				, current.traffic_light_cancelled_customer			AS current_traffic_light_cancelled_customer
				, current.traffic_light_inbound_calls				AS current_traffic_light_inbound_calls
				, current.traffic_light_lost_calls					AS current_traffic_light_lost_calls
				, current.traffic_light_createdcases				AS current_traffic_light_createdcases
				, current.traffic_light_retention					AS current_traffic_light_retention
				, current.traffic_light_invoicecorrection			AS current_traffic_light_invoicecorrection
				, current.traffic_light_professionalimprovement		AS current_traffic_light_professionalimprovement
				, current.avg_traffic_light							AS current_avg_traffic_light
	
	FROM		bi.opportunity_traffic_light_new AS history
	
	LEFT JOIN 	(
				SELECT		*
				FROM		bi.opportunity_traffic_light_new
				WHERE		date 							= (CURRENT_DATE - INTERVAL '0 DAY')
				) AS 		current							ON (history.opportunity = current.opportunity)
	
	WHERE		history.date 								BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
						
;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Cases, open and closed since 2018-11-01 (new case setup)
-- Created on: 24/05/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_basis;
CREATE 	TABLE 				bi.CM_cases_basis 	AS 

SELECT 		cas.sfid						case_ID
			, cas.casenumber				case_number
			, cas.createddate				case_createddate
			, cas.isclosed					case_isclosed
			, cas.ownerid					case_ownerid
			, u.name						case_owner
			, cas.origin					case_origin
			, cas.type						case_type
			, cas.reason					case_reason
			, cas.status					case_status
			, cas.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, cas.order__c					orderid
			, o.type						order_type
			, cas.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, cas.opportunity__c			opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
FROM salesforce.case 				cas
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

WHERE

(opp.test__c				= FALSE  
OR opp.test__c				IS NULL)

-- just CM B2B
--	excluded case owner	
AND ((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
			WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
			WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1
			WHEN 	u.name 		LIKE 	'%Frank_Wendt%' 		THEN 1  
			WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
			WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
			WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 
			WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
			WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
			WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
			WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
			WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1 ELSE 0 END) = 0  									--2				
	)
AND (	

-- old case setup (3 AND NOT 11)
		(
			(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
					WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
					WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
					WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
					WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
		AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
					WHEN 	cas.type 	= 'CLM'					THEN 1
					WHEN 	cas.type	= 'CM B2C'				THEN 1
					WHEN 	cas.type	= 'Sales'				THEN 1
					WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
		)
	OR	(
			(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
					WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
					WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
					WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
		AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
					WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
		)
		
-- new case setup
	OR	(	
			(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
		AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
					WHEN 	cas.type 	= 'Pool'				THEN 1
					WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
		)
		
	OR 	(
			(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
					WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
		)
	)	
AND cas.createddate::date >= '2018-11-01'
;					

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Backlog today (used for the backlog history tracking)
-- Created on: 01/11/2018

DROP 	TABLE IF EXISTS 	bi.CM_cases_today;
CREATE 	TABLE 				bi.CM_cases_today 	AS 

SELECT 		*
			
FROM 		bi.CM_cases_basis 		ca

WHERE		ca.case_isclosed		= FALSE	
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Cases created yesterday (used for the case inbound history tracking)
-- Created on: 07/11/2018

DROP 	TABLE IF EXISTS 	bi.CM_cases_created_yesterday;
CREATE 	TABLE 				bi.CM_cases_created_yesterday 	AS 

SELECT 	*

FROM 	bi.cm_cases_basis 	ca

WHERE 	ca.case_createddate::date = 'YESTERDAY'
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Backlog History (count) 
-- Created on: 01/11/2018

DELETE FROM 	bi.CM_cases 		WHERE date = CURRENT_DATE AND kpi = 'Open Cases';
INSERT INTO 	bi.CM_cases
SELECT		
			TO_CHAR (CURRENT_DATE,'YYYY-IW') 		AS date_part
			, MIN 	(CURRENT_DATE::date) 			AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Open Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6	
			, MIN 	(cases.case_createddate::date)	AS sub_kpi_7
			, cases.Opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.cm_cases_today	cases

GROUP BY 	case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM NEW cases History (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Created Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((cases.case_createddate::date),'YYYY-IW') 		AS date_part
			, MIN 	(cases.case_createddate::date)					AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Created Cases' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, cases.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10			
			, COUNT(*)								AS value

FROM 		bi.cm_cases_created_yesterday	cases

GROUP BY 	cases.case_createddate
			, cases.case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Reopened Cases (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Reopened Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Reopened Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			
FROM 		salesforce.casehistory hi

INNER JOIN 	bi.cm_cases_basis 				ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2018-11-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Closed Cases (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Closed Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((closed.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(closed.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, closed.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, closed.case_status						AS sub_kpi_2
			, closed.case_reason						AS sub_kpi_3			
			, CASE 	WHEN closed.grand_total IS NULL	AND closed.opportunityid IS NULL 		THEN 'unknown'
					WHEN closed.grand_total IS NULL	AND closed.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN closed.grand_total < 250 											THEN '<250€'
		  			WHEN closed.grand_total >= 250 	AND closed.grand_total < 500 			THEN '250€-500€'
		 			WHEN closed.grand_total >= 500 	AND closed.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, closed.opportunity						AS sub_kpi_5
			, closed.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, closed.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			
FROM 		salesforce.casehistory 			hi

INNER JOIN 	bi.cm_cases_basis 				ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- closed cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Closed'
			AND hi.createddate::date >= '2018-11-01'

			
) AS closed

GROUP BY 	closed.date
			, closed.case_origin
			, closed.case_status	
			, closed.case_reason
			, closed.grand_total
			, closed.opportunityid
			, closed.opportunity	
			, closed.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Type changes (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Cases Type changed';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((type_change.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(type_change.date::date)				AS date
			, CAST 	('-' 			AS varchar) 			AS locale
			, type_change.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)				AS type
			, CAST	('Cases Type changed' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)				AS sub_kpi_1
			, type_change.case_status						AS sub_kpi_2
			, type_change.case_reason						AS sub_kpi_3			
			, CASE 	WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NULL 		THEN 'unknown'
					WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN type_change.grand_total < 250 													THEN '<250€'
		  			WHEN type_change.grand_total >= 250 	AND type_change.grand_total < 500 			THEN '250€-500€'
		 			WHEN type_change.grand_total >= 500 	AND type_change.grand_total < 1000 			THEN '500€-1000€'
		  																								ELSE '>1000€'		END AS sub_kpi_4
			, type_change.opportunity						AS sub_kpi_5
			, type_change.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)				AS sub_kpi_7
			, type_change.opportunityid						AS sub_kpi_8
			, CAST 	('-' 			AS varchar)				AS sub_kpi_9
			, CAST 	('-' 			AS varchar)				AS sub_kpi_10			
			, COUNT(*)										AS value


FROM 		(


SELECT 		hi.createddate::date 							AS date
			, *
			
FROM salesforce.casehistory 			hi

LEFT JOIN 	bi.cm_cases_basis 			ca 		ON hi.caseid			= ca.case_ID			
		
	-- reopened cases
WHERE				hi.field 			= 			'Type'
			AND 	hi.newvalue			= 			'CM B2B'
			AND		hi.oldvalue			NOT IN 		('B2B','KA')
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2018-11-01'
			AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
			AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
			
) AS type_change

GROUP BY 	type_change.date
			, type_change.case_origin
			, type_change.case_status	
			, type_change.case_reason
			, type_change.grand_total
			, type_change.opportunityid
			, type_change.opportunity	
			, type_change.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Inbound Calls (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Inbound Calls';
INSERT INTO 	bi.CM_cases

 SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 			AS varchar)							AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493030807264', '41435084849')
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Outbound Calls (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Outbound Calls';
INSERT INTO 	bi.CM_cases

SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 				AS varchar) 					AS locale
			, CAST 	('BAT CM'			AS varchar) 					AS origin 
			, CAST 	('B2B'				AS varchar)						AS type
			, CAST	('Outbound Calls'	AS varchar)						AS kpi
			, CAST 	('Count'			AS varchar)						AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 				AS varchar)						AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 				AS varchar)						AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Outbound'
			AND n.department__c LIKE 'CM'
			AND co.type__c NOT LIKE 'customer-b2c'
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Kpis Traffic Lights (count) 
-- Created on: 05/12/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Traffic Light';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR (traffic_lights.date,'YYYY-IW') AS date_part
			, MIN 	(traffic_lights.date::date) 	AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, CAST 	('-' 			AS varchar)		AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Traffic Light' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, traffic_lights.avg_traffic_light		AS sub_kpi_2
			, CASE 	WHEN traffic_lights.avg_traffic_light IS NULL						THEN 'red'
					WHEN traffic_lights.avg_traffic_light <= 2.4 						THEN 'red'
					WHEN traffic_lights.avg_traffic_light > 2.4 
						AND traffic_lights.avg_traffic_light <= 2.9 					THEN 'yellow'
					WHEN traffic_lights.avg_traffic_light >2.9							THEN 'green' 
																						ELSE '-' 			END AS sub_kpi_3																	
			, CASE 	WHEN traffic_lights.avg_traffic_light IS NULL						THEN 1
					WHEN traffic_lights.avg_traffic_light <= 2.4 						THEN 1
					WHEN traffic_lights.avg_traffic_light > 2.4 
						AND traffic_lights.avg_traffic_light <= 2.9 					THEN 2
					WHEN traffic_lights.avg_traffic_light >2.9							THEN 3 
																						ELSE 0 			END AS sub_kpi_4																					
			, CAST 	('-' 			AS varchar)		AS sub_kpi_5
			, CAST 	('-' 			AS varchar)		AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, CAST 	('-' 			AS varchar)		AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.opportunity_traffic_light_new	traffic_lights

GROUP BY 	traffic_lights.date
			, traffic_lights.avg_traffic_light
;
--------------------------------------------
--------------------------------------------
--------------------------------------------

-- Author: Christina Janson
-- CM B2B Case details - could get out
-- Created on: 22/01/2019

-- DROP 	TABLE IF EXISTS 	bi.CM_cases_details;
-- CREATE 	TABLE 				bi.CM_cases_details 	AS 

-- SELECT 	cas.sfid						case_ID
-- 		, cas.casenumber				case_number
-- 		, cas.createddate				case_createddate
-- 		, cas.origin 					case_origin
-- 		, u.name						case_owner

-- FROM 		salesforce.case 		cas
-- LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

-- WHERE
-- just CM B2B
--	excluded case owner	
-- 		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Frank Wendt%' 		THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 ELSE 0 END) = 0 									--2				
-- 		)
-- 	AND (	
	
	-- old case setup (3 AND NOT 11)
-- 			(
-- 				(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
-- 						WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
-- 						WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
-- 						WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
-- 						WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
-- 			AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
-- 						WHEN 	cas.type 	= 'CLM'					THEN 1
-- 						WHEN 	cas.type	= 'CM B2C'				THEN 1
-- 						WHEN 	cas.type	= 'Sales'				THEN 1
-- 						WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
-- 			)
-- 		OR	(
-- 				(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
-- 						WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
-- 						WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
-- 						WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
-- 			AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
-- 						WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
-- 			)
			
	-- new case setup
-- 		OR	(	
-- 				(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
-- 			AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
-- 						WHEN 	cas.type 	= 'Pool'				THEN 1
-- 						WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
-- 			)
			
-- 		OR 	
-- 				(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
-- 						WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			
-- 		)		
-- ;

-- Author: Christina Janson
-- CM B2B Service Level SVL calculation: case history table when case closed after the 2018-01-01
-- Created on: 05/06/2019
DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis_closed;
CREATE 	TABLE 				bi.CM_cases_service_level_basis_closed 	AS 

SELECT 		cahi.caseid																					case_ID
			, cahi.createddate
			, cahi.createdbyid																			AS event_by
			
FROM 	 salesforce.casehistory		cahi	

WHERE 	cahi.field 				IN 	('created','Status') 
	AND cahi.newvalue 			IN 	('Closed')
	AND cahi.createddate::date 	>= 	'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 

;

-- Author: Christina Janson
-- CM B2B Service Level SVL calculation
-- Created on: 11/01/2019
DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, cahi.event_by																			AS event_by
			
FROM 		bi.CM_cases_basis 		ca

-- date2													 														
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID 			= 	cahi.case_ID
 														AND cahi.createddate 	>= 	ca.case_createddate

WHERE 		ca.case_createddate::date 					>= 		'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate	
			, cahi.event_by			

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value
			, cahi.event_by																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_basis 		ca 				ON hi.caseid			= 	ca.case_ID	
																					
-- date2 														
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID 			= 	cahi.case_ID
 														AND cahi.createddate 	>= 	hi.createddate
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, cahi.event_by

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, cahi.event_by																				AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_basis 			ca 				ON hi.caseid			= 	ca.case_ID	

-- date 2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	hi.createddate						
 					
WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								= 		'CM B2B'
			AND	hi.oldvalue								NOT IN 	('B2B','KA')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2018-01-01'
			AND ca.case_origin							NOT LIKE '%B2B customer%' 	
			AND ca.case_origin	 						NOT LIKE 'CM - Team'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, cahi.event_by

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, hi.createdbyid																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_basis 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
 														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, hi.createdbyid

ORDER BY 	case_id
			, date1_opened
			, date2_closed
			, event_by
;

-- Author: Christina Janson
-- CM B2B Case Service Level
-- Created on: 11/01/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level;
CREATE 	TABLE 				bi.CM_cases_service_level 	AS 

SELECT 	basis.*
-- ---------------------------------------------------------------------------------- difference: case opened / reopened / type change -> case closed		
-- -
-- - working h Mo-Fr 08:00:00 - 17:00:00 bi
-- - summertime timezone difference of 1 h

		, CASE 	WHEN basis.date1_opened::date 			IS NULL 							THEN 'rule 0.1' -- opened date missing
				WHEN basis.date2_closed::date 			IS NULL 							THEN 'rule 0.2' -- still open case
				WHEN basis.date1_opened::timestamp 	> basis.date2_closed::timestamp			THEN 'rule 1.0' -- open after closed
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN -- is same date
					 (CASE WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 2.1' 	 -- opened befor working day start
																							ELSE 'rule 2.2' END) -- 
				ELSE (CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 				THEN 'rule 3.1' -- opened after working day start
							WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') 	THEN 'rule 3.2' -- opened at the weekend
							WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 3.3' -- opened before working day start
																							ELSE 'rule 3.4' -- opened at a weekday within working hours
				END) END 																	AS rule_set 
				
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN 
		
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
					(CASE WHEN basis.date1_opened::time < TIME '07:00:00.0'					THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
						+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) END)
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
	-- minutes pased on the owner change date UNTIL END OF WORK
				-- owner changed after working day
						(CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 			THEN 0 
						-- owner change at the weekend
								WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') THEN 0
				ELSE
					-- owner changed before working day
						(CASE 	WHEN basis.date1_opened::time < TIME '07:00:00.0' 			THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60
								ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - basis.date1_opened::time) * 60 
										+ DATE_PART 	('minute', TIME '16:00:00.0' - basis.date1_opened::time) END) END)
					+
					-- minutes pased on the first contact date beginning at the morning working time
						(CASE 	WHEN basis.date2_closed::time < TIME '07:00:00.0' 			THEN 0 
								WHEN basis.date2_closed::time < TIME '16:00:00.0' 			THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60 END)
					+
					
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days and SUBTRACT 2 days 
	-- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) -2	) 			> 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END) END 																AS SVL_Minutes
			
-- ------------------------------------------------------------------------------------------------------------------------------------------------
-- - SVL Customer, dont care about working hours
-- -
				
-- if the case opened was after the case closed date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if case opened date and case closed date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					
				THEN 		DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
							+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) 
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
				
				-- date opened to EOD in minutes
				CASE WHEN date_part('dow', basis.date1_opened) IN ('6','0')	THEN 0 ELSE  
				(DATE_PART 	('hour', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened) * 60 
						+ DATE_PART 	('minute', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened)) END 
						
				-- + minutes on the case closed date		
				+	(DATE_PART 	('hour', basis.date2_closed) * 60  + DATE_PART 	('minute', basis.date2_closed))		
				+
					
	-- create a list of all date incl. case opened date and case closed date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) - 2)			> 0 
			
			-- in case the working days are > 0 -> workind days * 24 * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNT(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) - CASE WHEN date_part('dow', basis.date1_opened) IN ('6')	THEN 1 ELSE 2 END) * 24 * 60
			ELSE 0 END)  	END
																								AS SVL_Customer
			, cas.createddate				case_createddate
			, cas.isclosed					case_isclosed
			, cas.ownerid					case_ownerid
			, u.name						case_owner
			, cas.origin					case_origin
			, cas.type						case_type
			, cas.reason					case_reason
			, cas.status					case_status
			, cas.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, cas.order__c					orderid
			, o.type						order_type
			, cas.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, cas.opportunity__c			opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			, u2.name 						event_user
		
FROM bi.cm_cases_service_level_basis basis

LEFT JOIN 	salesforce.case 		cas 	ON basis.case_id		= cas.sfid
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 
LEFT JOIN 	salesforce.user 		u2 		ON basis.event_by 		= u2.sfid

-- WHERE basis.case_id = '5000J00001Q5PmtQAF'
-- LIMIT 100
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- CM B2C Case details
-- Created on: 18/03/2019

DROP 	TABLE IF EXISTS 	bi.CM_B2C_cases_details;
CREATE 	TABLE 				bi.CM_B2C_cases_details 	AS 

SELECT 	cas.sfid						case_ID
		, cas.casenumber				case_number
		, cas.createddate				case_createddate
		, cas.origin 					case_origin
		, u.name						case_owner

FROM 		salesforce.case 		cas
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

WHERE (cas.test__c				= FALSE  
		OR cas.test__c				IS NULL)
-- just CM B2C
--	excluded case owner	
	AND	((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
				WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Frank_Wendt%' 		THEN 1
				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- new case setup 
			( (CASE 	WHEN 	cas.type	= 'CM B2C'				THEN 1 ELSE 0 END) = 1									-- 11
			)
			
	-- old case setup
		OR	(	
				(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1 
						WHEN 	cas.origin 	LIKE 'B2C Customer%'	THEN 1 
						WHEN 	cas.origin 	LIKE 'Insurance'		THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	cas.type	= 'General'				THEN 1
						WHEN 	cas.type 	= 'Pool'				THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		)	
	AND cas.test__c IS NOT TRUE	
	AND cas.createddate::date >= '2018-01-01'	
;


DROP 	TABLE IF EXISTS 	bi.CM_B2C_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_B2C_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			
FROM 		bi.CM_B2C_cases_details 		ca

-- date2	
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	ca.case_createddate													
-- LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_id			= 	cahi.caseid 
--														AND cahi.field 			IN 	('created','Status') 
--														AND cahi.newvalue 		IN 	('Closed')
--														AND cahi.createddate::date >= ca.case_createddate::date

WHERE 		ca.case_createddate::date 					>= 		'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate				

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_B2C_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	hi.createddate	

-- LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
--														AND cahi.field 			IN 	('created','Status') 
--														AND cahi.newvalue 		IN 	('Closed')	
 --														AND cahi.createddate 	>= 	hi.createddate
 														
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_B2C_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
																AND ca.case_origin	NOT LIKE 'CM%' 	
																AND ca.case_origin	NOT LIKE 'B2C Customer'
																AND ca.case_origin	NOT LIKE 'Insurance'
							
-- date 2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	hi.createddate	

-- LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
--														AND cahi.field 			IN 	('created','Status') 
--														AND cahi.newvalue 		IN 	('Closed')	
 --														AND cahi.createddate 	>= 	hi.createddate

WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								= 		'CM B2C'
		--	AND	hi.oldvalue								NOT IN 	('PM')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2018-01-01'
			AND ca.case_origin							NOT LIKE 'CM%' 	
			AND ca.case_origin	 						NOT LIKE 'B2C Customer'
			AND ca.case_origin	 						NOT LIKE 'Insurance'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_B2C_cases_details 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

ORDER BY 	case_id
			, date1_opened
			, date2_closed
;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: weekly CM Team and Agent Performance (B2B)
-- Created on: 14/05/2019

DROP TABLE IF EXISTS bi.cm_performance_weekly;
CREATE TABLE bi.cm_performance_weekly AS 

SELECT *
		,CASE WHEN performance.inbound_calls > 0 		THEN  performance.inbound_calls >= (performance.target_inbound_calls * performance.FTE) END		AS performance_inbound_calls
		,CASE WHEN performance.cases_closed > 0 		THEN  performance.cases_closed >= (performance.target_cases_closed * performance.FTE) END		AS performance_cases_closed
		,CASE WHEN performance.svl_share_within_24h > 0 THEN  performance.svl_share_within_24h >= (performance.targert_svl * performance.FTE) END		AS performance_svl



FROM
(
SELECT 	
		 cases.date_part 				AS cw 
		, MIN(cases.date) 				AS date
		, cases.sub_kpi_6	 			AS agent

-- FTE of every CM agent to calculate the share of the targets		
		, CASE 	WHEN cases.sub_kpi_6 = 'Katharina Kühner' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Katharina Kuehner' 	THEN 1 
				WHEN cases.sub_kpi_6 = 'Sercan Tas'	 		THEN 1
				WHEN cases.sub_kpi_6 = 'André Bauß' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Carmen Haas' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Julian Schäfer'		THEN 1
				WHEN cases.sub_kpi_6 = 'Susanne Marino' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Danny Taszarek'		THEN 1
				WHEN cases.sub_kpi_6 = 'Felix Liedtke'		THEN 1
				WHEN cases.sub_kpi_6 = 'Vivien Greve' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Daniela Kaim'		THEN 1
				WHEN cases.sub_kpi_6 = 'Salim Abdoulaye'	THEN 1
				WHEN cases.sub_kpi_6 = 'Lennart Bär'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Karla Sorgato'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'CM Support2'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Marleen Dreyer'		THEN 0.5
															ELSE 0 END 	AS FTE
		
		, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.value ELSE 0 END)	AS inbound_calls
		, CAST 	('20' 	AS numeric)					AS target_inbound_calls

		, SUM(CASE WHEN cases.kpi = 'Closed Cases' 	THEN cases.value ELSE 0 END) 	AS cases_closed
		, CAST 	('90'	AS numeric)					AS target_cases_closed

		, svl_final.svl_share_within_24h 			AS svl_share_within_24h
		, svl_final.cases							AS svl_casesclosed
		, svl_final.svl_cases_within_24h			AS svl_cases_within_24h
		, CAST 	('0.95'	AS numeric)					AS targert_svl


FROM bi.cm_cases 		cases

LEFT JOIN 
		(
			SELECT 	
					cw
					, MIN(svl_details.opened_date) 				AS MinDate
					, agent
					, COUNT(*)									AS cases
					, SUM(svl_details.svl_within_24h) 			AS svl_cases_within_24h
					, ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4)	AS svl_share_within_24h
			
			FROM
					(
					SELECT 	TO_CHAR (svl.date1_opened, 'YYYY-IW')	AS cw
							, svl.date1_opened::date 				AS opened_date
							, svl.event_user						AS agent
							, svl.case_id 							AS case_id
							, svl.case_number						AS case_number
							, svl.type 								AS svl_type
							, svl.closed 							AS svl_has_closeddate
							, svl.case_isclosed						AS case_stil_closed
							, svl.case_type							AS case_type
							, svl.case_origin						AS case_origin
							, svl.case_reason						AS case_reason
							, svl.case_status						AS case_status	
							, svl.svl_customer						AS svl_customer
							
					-- IMPROVEMENT: maybe the following should be included in the SVL function!
							, CASE WHEN svl.svl_customer/60 <= 24 THEN 1 ELSE 0 END AS svl_within_24h
							
							, CASE WHEN svl.svl_customer = 0 		AND svl.type = 'Reopened Case' 					THEN 1 ELSE 0 END AS exclude_reopened_closed_due_mail 	-- exclude when 1
							, CASE WHEN svl.type = 'New Case' 		AND svl.case_origin = 'direct email outbound' 	THEN 1 ELSE 0 END AS exclude_direct_email_outbound	 	-- exclude when 1
							, CASE WHEN svl.type = 'Damage' 		OR svl.case_reason = '%Damage%'					THEN 1 ELSE 0 END AS exclude_damage_cases				-- exclude when 1
							, CASE WHEN svl.case_reason = 'Opportunity - Onboarding'								THEN 1 ELSE 0 END AS exclude_onboarding_cases			-- exclude when 1
							, CASE WHEN svl.case_status = 'Closed' AND svl.case_isclosed IS FALSE					THEN 1 ELSE 0 END AS exclude_created_within_clean_up	-- exclude when 1
					
					FROM 	bi.cm_cases_service_level 	svl
					WHERE 	NOT svl.type 	= '# Reopened'
							AND svl.date1_opened::date >= '2019-01-01'
					
					) AS svl_details
			
			 WHERE 
			 		svl_details.exclude_reopened_closed_due_mail 		= 0
					AND svl_details.exclude_direct_email_outbound 		= 0
					AND svl_details.exclude_damage_cases 				= 0
					AND svl_details.exclude_onboarding_cases 			= 0
					AND svl_details.exclude_created_within_clean_up 	= 0
			
			GROUP BY 	
						cw
						, agent
						
						
			) AS svl_final  ON (svl_final.cw = cases.date_part AND svl_final.agent = cases.sub_kpi_6)

WHERE cases.date 	>= '2019-01-01'

GROUP BY 	cases.date_part 				
			, cases.sub_kpi_6	
			, svl_final.svl_share_within_24h
			, svl_final.cases							
			, svl_final.svl_cases_within_24h
) AS performance
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: monthly CM Team and Agent Performance (B2B)
-- Created on: 11/06/2019

DROP TABLE IF EXISTS bi.cm_performance_monthly;
CREATE TABLE bi.cm_performance_monthly AS 

SELECT *
		,CASE WHEN performance.inbound_calls > 0 		THEN  performance.inbound_calls >= (performance.target_inbound_calls * performance.FTE) END		AS performance_inbound_calls
		,CASE WHEN performance.cases_closed > 0 		THEN  performance.cases_closed >= (performance.target_cases_closed * performance.FTE) END		AS performance_cases_closed
		,CASE WHEN performance.svl_share_within_24h > 0 THEN  performance.svl_share_within_24h >= (performance.targert_svl * performance.FTE) END		AS performance_svl



FROM
(
SELECT 	MIN(cases.date) 				AS date
		, cases.sub_kpi_6	 			AS agent

-- FTE of every CM agent to calculate the share of the targets		
		, CASE 	WHEN cases.sub_kpi_6 = 'Katharina Kühner' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Katharina Kuehner' 	THEN 1 
				WHEN cases.sub_kpi_6 = 'Sercan Tas'	 		THEN 1
				WHEN cases.sub_kpi_6 = 'André Bauß' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Carmen Haas' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Julian Schäfer'		THEN 1
				WHEN cases.sub_kpi_6 = 'Susanne Marino' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Danny Taszarek'		THEN 1
				WHEN cases.sub_kpi_6 = 'Felix Liedtke'		THEN 1
				WHEN cases.sub_kpi_6 = 'Vivien Greve' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Daniela Kaim'		THEN 1
				WHEN cases.sub_kpi_6 = 'Salim Abdoulaye'	THEN 1
				WHEN cases.sub_kpi_6 = 'Lennart Bär'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Karla Sorgato'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'CM Support2'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Marleen Dreyer'		THEN 0.5
															ELSE 0 END 	AS FTE
		
		, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.value ELSE 0 END)	AS inbound_calls
		, CAST 	('20' 	AS numeric)		/5 * 21		AS target_inbound_calls

		, SUM(CASE WHEN cases.kpi = 'Closed Cases' 	THEN cases.value ELSE 0 END) 	AS cases_closed
		, CAST 	('90'	AS numeric)		/5 * 21		AS target_cases_closed

		, svl_final.svl_share_within_24h 			AS svl_share_within_24h
		, svl_final.cases							AS svl_casesclosed
		, svl_final.svl_cases_within_24h			AS svl_cases_within_24h
		, CAST 	('0.95'	AS numeric)					AS targert_svl


FROM bi.cm_cases 		cases

LEFT JOIN 
		(
			SELECT 	svl_details.month 							AS month
					, MIN(svl_details.opened_date) 				AS MinDate
					, agent
					, COUNT(*)									AS cases
					, SUM(svl_details.svl_within_24h) 			AS svl_cases_within_24h
					, ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4)	AS svl_share_within_24h
			
			FROM
					(
					SELECT 
					--		*
							TO_CHAR(svl.date1_opened, 'YYYY-MM') 	AS month
							, svl.date1_opened::date 				AS opened_date
							, svl.event_user						AS agent
							, svl.case_id 							AS case_id
							, svl.case_number						AS case_number
							, svl.type 								AS svl_type
							, svl.closed 							AS svl_has_closeddate
							, svl.case_isclosed						AS case_stil_closed
							, svl.case_type							AS case_type
							, svl.case_origin						AS case_origin
							, svl.case_reason						AS case_reason
							, svl.case_status						AS case_status	
							, svl.svl_customer						AS svl_customer
							
					-- IMPROVEMENT: maybe the following should be included in the SVL function!
							, CASE WHEN svl.svl_customer/60 <= 24 THEN 1 ELSE 0 END AS svl_within_24h
							
							, CASE WHEN svl.svl_customer = 0 		AND svl.type = 'Reopened Case' 					THEN 1 ELSE 0 END AS exclude_reopened_closed_due_mail 	-- exclude when 1
							, CASE WHEN svl.type = 'New Case' 		AND svl.case_origin = 'direct email outbound' 	THEN 1 ELSE 0 END AS exclude_direct_email_outbound	 	-- exclude when 1
							, CASE WHEN svl.type = 'Damage' 		OR svl.case_reason = '%Damage%'					THEN 1 ELSE 0 END AS exclude_damage_cases				-- exclude when 1
							, CASE WHEN svl.case_reason = 'Opportunity - Onboarding'								THEN 1 ELSE 0 END AS exclude_onboarding_cases			-- exclude when 1
							, CASE WHEN svl.case_status = 'Closed' AND svl.case_isclosed IS FALSE					THEN 1 ELSE 0 END AS exclude_created_within_clean_up	-- exclude when 1
					
					FROM 	bi.cm_cases_service_level 	svl
					WHERE 	NOT svl.type 	= '# Reopened'
							AND svl.date1_opened::date >= '2019-01-01'
					
					) AS svl_details
			
			 WHERE 
			 		svl_details.exclude_reopened_closed_due_mail 		= 0
					AND svl_details.exclude_direct_email_outbound 		= 0
					AND svl_details.exclude_damage_cases 				= 0
					AND svl_details.exclude_onboarding_cases 			= 0
					AND svl_details.exclude_created_within_clean_up 	= 0
			
			GROUP BY 	
						month
						, agent
						
						
			) AS svl_final  ON (svl_final.month = TO_CHAR (cases.date,'YYYY-MM') AND svl_final.agent = cases.sub_kpi_6)

WHERE cases.date 	>= '2019-01-01'

GROUP BY 	cases.sub_kpi_6	
			, svl_final.svl_share_within_24h
			, svl_final.cases							
			, svl_final.svl_cases_within_24h
) AS performance
;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
DROP 	TABLE IF EXISTS 	bi.PM_cases_basis;
CREATE 	TABLE 				bi.PM_cases_basis 	AS 

SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
 			, ca.createddate				case_createddate
 			, ca.isclosed					case_isclosed
 			, ca.ownerid					case_ownerid
 			, u.name						case_owner
 			, ca.origin						case_origin
 			, ca.type						case_type
 			, ca.reason						case_reason
 			, ca.status						case_status
 			, ca.contactid					contactid
 			, co.name						contact_name
 			, co.type__c					contact_type
 			, co.company_name__c			contact_companyname
 			, ca.order__c					orderid
 			, o.type						order_type
 			, ca.accountid					professionalid
 			, a.name						professional
 			, a.company_name__c				professional_companyname
 			
 			, ca.opportunity__c				opportunityid
 			, opp.name						opportunity
 			, opp.grand_total__c			grand_total
 			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 2 AND (3 OR 4)

WHERE		(ca.test__c				= FALSE  
			OR ca.test__c				IS NULL)
					
	--	excluded case owner	
	AND((CASE	WHEN 	u.name 		LIKE 	'%Standke%' 			THEN 1 
																	ELSE 0 END) = 0 									-- 2				
		)
	AND (	ca.type = 'PM' 																								-- 3
	OR (	ca.type = 'Pool' 	AND ca.origin = 'PM - Team'	)														-- 4
 		)	
	AND ca.createddate::date >= '2018-01-01'
--		AND ca.casenumber = '00517627' 								- in case you are looking for a case -- its case^2 
;

-- Author: Christina Janson
-- PM B2B Case details - could get out
-- Created on: 18/03/2019

--DROP 	TABLE IF EXISTS 	bi.PM_cases_details;
-- CREATE 	TABLE 				bi.PM_cases_details 	AS 

-- SELECT 	cas.sfid						case_ID
-- 		, cas.casenumber				case_number
-- 		, cas.createddate				case_createddate
-- 		, cas.origin 					case_origin
-- 		, u.name						case_owner

-- FROM 		salesforce.case 		cas
-- LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

-- WHERE
-- just CM B2B
--	excluded case owner	
-- 		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
			--	WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Stolzenburg%' 		THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Feldhaus%' 			THEN 1 
-- 				WHEN 	u.name 		LIKE 	'%Wendt%' 				THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Adorador%' 			THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Devrient%' 			THEN 1
-- 				WHEN 	u.name 		LIKE 	'%Heesch-Müller%' 		THEN 1 ELSE 0 END) = 0 									--2				
-- 		)
-- 	AND (	
	
	-- new case setup 
-- 			( (CASE 	WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 1									-- 11
-- 			)
			
	-- old case setup
-- 		OR	(	
-- 				(CASE 	WHEN 	cas.origin 	LIKE 'TFS PM%'			THEN 1 
-- 						WHEN 	cas.origin 	LIKE 'PM -Team%'		THEN 1 ELSE 0 END) = 1									-- 3
-- 			AND (CASE 	WHEN 	cas.type	= 'General'				THEN 1
-- 						WHEN 	cas.type 	= 'Pool'				THEN 1 ELSE 0 END) = 1									-- 11
-- 			)
			
-- 		)	
-- 	AND cas.test__c IS NOT TRUE
-- 	AND cas.createddate::date >= '2018-01-01'		
--;

DROP 	TABLE IF EXISTS 	bi.PM_cases_service_level_basis;
CREATE 	TABLE 				bi.PM_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			
FROM 		bi.PM_cases_basis 		ca

-- date2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	ca.case_createddate	

-- LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_id			= 	cahi.caseid 
--														AND cahi.field 			IN 	('created','Status') 
--														AND cahi.newvalue 		IN 	('Closed')
--														AND cahi.createddate::date >= ca.case_createddate::date

WHERE 		ca.case_createddate::date 					>= 		'2018-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate				

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.PM_cases_basis 			ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	hi.createddate	

--LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
--														AND cahi.field 			IN 	('created','Status') 
--														AND cahi.newvalue 		IN 	('Closed')	
 --														AND cahi.createddate 	>= 	hi.createddate
 														
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.PM_cases_basis 			ca 				ON hi.caseid			= 	ca.case_ID	
															AND ca.case_origin							NOT LIKE 'TFS PM' 	
															AND ca.case_origin	 						NOT LIKE 'PM - Team'

							
-- date 2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	hi.createddate	

--LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
--														AND cahi.field 			IN 	('created','Status') 
--														AND cahi.newvalue 		IN 	('Closed')	
 --														AND cahi.createddate 	>= 	hi.createddate

WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								= 		'PM'
			AND	hi.oldvalue								NOT IN 	('PM')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2018-01-01'
			AND ca.case_origin							NOT LIKE 'TFS PM' 	
			AND ca.case_origin	 						NOT LIKE 'PM - Team'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.PM_cases_basis 	 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
 														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2018-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate

ORDER BY 	case_id
			, date1_opened
			, date2_closed

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Case Backlog today (used for the backlog history tracking)
-- Created on: 15/05/2019

DROP 	TABLE IF EXISTS 	bi.PM_cases_today;
CREATE 	TABLE 				bi.PM_cases_today 	AS 

SELECT 		*

FROM 		bi.PM_cases_basis ca

WHERE		ca.case_isclosed			= FALSE		
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Cases created yesterday (used for the case inbound history tracking)
-- Created on: 15/05/2019

DROP 	TABLE IF EXISTS 	bi.PM_cases_created_yesterday;
CREATE 	TABLE 				bi.PM_cases_created_yesterday 	AS 

SELECT 		*

FROM 		bi.PM_cases_basis 	ca 

WHERE	ca.case_createddate::date = 'YESTERDAY'																				-- 5 
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Backlog History (count) 
-- Created on: 15/05/2019

DELETE FROM 	bi.PM_cases 		WHERE date = CURRENT_DATE AND kpi = 'Open Cases';
INSERT INTO 	bi.PM_cases
 
SELECT		
			TO_CHAR (CURRENT_DATE,'YYYY-IW') 		AS date_part
			, MIN 	(CURRENT_DATE::date) 			AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Open Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6	
			, MIN 	(cases.case_createddate::date)	AS sub_kpi_7
			, cases.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.PM_cases_today	cases

GROUP BY 	case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM NEW cases History (count)
-- Created on: 15/05/2019

DELETE FROM 	bi.PM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Created Cases';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((cases.case_createddate::date),'YYYY-IW') 		AS date_part
			, MIN 	(cases.case_createddate::date)					AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Created Cases' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7 
			, cases.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10			
			, COUNT(*)								AS value

FROM 		bi.PM_cases_created_yesterday	cases

GROUP BY 	cases.case_createddate::date
			, cases.case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Reopened Cases (count)
-- Created on: 16/05/2019

DELETE FROM 	bi.PM_cases			WHERE kpi = 'Reopened Cases';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Reopened Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	bi.PM_cases_basis				ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2019-01-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Closed Cases (count)
-- Created on: 16/05/2019

DELETE FROM 	bi.PM_cases			WHERE kpi = 'Closed Cases';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM bi.CM_cases_service_level_basis_closed  hi --not just Cm contains all closed cases

INNER JOIN 	bi.PM_cases_basis 						ca 		ON hi.case_id			= ca.case_ID			

WHERE		hi.createddate::date >= '2019-01-01'
			) AS reopened
			
			
GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner				
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM Case Type changes (count)
-- Created on: 17/05/2018

DELETE FROM 	bi.PM_cases			WHERE kpi = 'Cases Type changed';
INSERT INTO 	bi.PM_cases 

SELECT		
			TO_CHAR ((type_change.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(type_change.date::date)				AS date
			, CAST 	('-' 			AS varchar) 			AS locale
			, type_change.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)				AS type
			, CAST	('Cases Type changed' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)				AS sub_kpi_1
			, type_change.case_status						AS sub_kpi_2
			, type_change.case_reason						AS sub_kpi_3			
			, CASE 	WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NULL 		THEN 'unknown'
					WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN type_change.grand_total < 250 													THEN '<250€'
		  			WHEN type_change.grand_total >= 250 	AND type_change.grand_total < 500 			THEN '250€-500€'
		 			WHEN type_change.grand_total >= 500 	AND type_change.grand_total < 1000 			THEN '500€-1000€'
		  																								ELSE '>1000€'		END AS sub_kpi_4
			, type_change.opportunity						AS sub_kpi_5
			, type_change.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)				AS sub_kpi_7
			, type_change.opportunityid						AS sub_kpi_8
			, CAST 	('-' 			AS varchar)				AS sub_kpi_9
			, CAST 	('-' 			AS varchar)				AS sub_kpi_10			
			, COUNT(*)										AS value


FROM 		(


SELECT 		hi.createddate::date 							AS date
			, *
			-- , field
FROM salesforce.casehistory hi

LEFT JOIN 	bi.PM_cases_basis 						ca 		ON hi.caseid			= ca.case_ID			
		
	-- reopened cases
WHERE				hi.field 			= 			'Type'
			AND 	hi.newvalue			= 			'PM'
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2019-01-01'
			AND 	ca.case_origin	 	NOT LIKE 	'PM - Team'
			
) AS type_change

GROUP BY 	type_change.date
			, type_change.case_origin
			, type_change.case_status	
			, type_change.case_reason
			, type_change.grand_total
			, type_change.opportunityid
			, type_change.opportunity	
			, type_change.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM B2B Inbound Calls (count)
-- Created on: 17/05/2018

DELETE FROM 	bi.PM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Inbound Calls';
INSERT INTO 	bi.PM_cases

 SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 			AS varchar)							AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493070014488')
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: PM B2B Outbound Calls (count)
-- Created on: 17/05/2018

DELETE FROM 	bi.PM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Outbound Calls';
INSERT INTO 	bi.PM_cases

SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 				AS varchar) 					AS locale
			, CAST 	('BAT CM'			AS varchar) 					AS origin 
			, CAST 	('B2B'				AS varchar)						AS type
			, CAST	('Outbound Calls'	AS varchar)						AS kpi
			, CAST 	('Count'			AS varchar)						AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 				AS varchar)						AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 				AS varchar)						AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Outbound'
			AND n.department__c LIKE 'PM'
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: partner invoice forecast details for the invoice check done by PM (OLD)
-- Created on: 30/11/2018

DROP TABLE IF EXISTS 	bi.partner_invoice_check;
CREATE TABLE 			bi.partner_invoice_check AS 

SELECT 		TO_CHAR(o.effectivedate, 'YYYY-MM') AS  year_month
			, CASE 	WHEN 	a.locale		LIKE ('ch-%') THEN 'ch'
					WHEN 	a.locale		LIKE ('de-%') THEN 'de'
					ELSE 'unknown' END AS 	locale 
			, a.partnername					partner
			, a.partnerid					partnerID
			, a.partnerpph					partnerpph
			, opp.name						opportunity
            , opp.sfid                      opportunityID
			, opp.monthly_partner_costs__c	ppp_monthly_partner_costs
			, opp.grand_total__c			opp_grand_total
			, invoiced.amount__c/1.19 as 	amount_invoiced
			--, o.effectivedate				order_effectivedate
			--, o.professional__c
			--, o.status					order_status
			--, o.order_duration__c	        order_duration
			--,*
			
			-- number of order
			, COUNT(*)								orders
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN 1 ELSE 0 END )			AS FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN 1 ELSE 0 END )				AS INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN 1 ELSE 0 END )		AS CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN 1 ELSE 0 END )		AS CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN 1 ELSE 0 END )		AS CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN 1 ELSE 0 END )			AS NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN 1 ELSE 0 END )		AS NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN 1 ELSE 0 END )		AS PENDING_ALLOCATION						
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN 1 ELSE 0 END )			AS PENDING_TO_START	
			
			-- hours (order duration)
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN o.order_duration__c ELSE 0 END )			AS h_FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN o.order_duration__c ELSE 0 END )				AS h_INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN o.order_duration__c ELSE 0 END )			AS h_NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN o.order_duration__c ELSE 0 END )		AS h_NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN o.order_duration__c ELSE 0 END )		AS h_PENDING_ALLOCATION	
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN o.order_duration__c ELSE 0 END )			AS h_PENDING_TO_START						
			
FROM		salesforce.order 		o
			
LEFT JOIN	salesforce.opportunity	opp 	ON o.opportunityid 	= opp.sfid
LEFT JOIN	salesforce.invoice__c	invoiced 	ON (o.opportunityid 	= invoiced.opportunity__c AND TO_CHAR(o.effectivedate, 'YYYY-MM') = TO_CHAR(invoiced.issued__c, 'YYYY-MM'))
LEFT JOIN 	(	SELECT 			pro.sfid			proid
								, pro.name			proname
								, pro.parentid		parentid
								, partner.sfid		partnerid
								, partner.name		partnername
								, pro.locale__c		locale
								, partner.pph__c	partnerpph
								
				FROM 			salesforce.account 	pro 
				LEFT JOIN 		salesforce.account 	partner 	ON 	pro.parentid = partner.sfid 
				WHERE 			pro.parentid		IS NOT NULL
			) 						AS a	ON o.professional__c		= a.proid


-- LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid


WHERE 		o.test__c 						= FALSE
			AND o.effectivedate::date 		>= '2018-01-01'
			--AND o.effectivedate::date 		<= '2018-10-31'
			AND o.type						IN ('cleaning-b2b','cleaning-window')
			AND (a.locale					LIKE ('de-%') OR a.locale  LIKE ('ch-%'))
			AND a.parentid 					IS NOT NULL


GROUP BY 	 a.partnername
			, a.locale
			, a.partnerid
			, a.partnerpph
			, opp.name
            , opp.sfid  
			, opp.monthly_partner_costs__c
			, opp.grand_total__c
			, invoiced.amount__c
			, TO_CHAR(o.effectivedate, 'YYYY-MM')
			--, o.status
;

-- Author: Christina Janson
-- Short Description: partner invoice forecast details for the invoice check done by PM Version 2
-- Created on: 30/11/2018
-- latest change: 23/04/2019

DROP TABLE IF EXISTS 	bi.partner_invoice_check_v2;
CREATE TABLE 			bi.partner_invoice_check_v2 AS 

SELECT 		TO_CHAR(o.effectivedate, 'YYYY-MM') AS  year_month
			, CASE 	WHEN 	a.locale		LIKE ('ch-%') THEN 'ch'
					WHEN 	a.locale		LIKE ('de-%') THEN 'de'
					ELSE 'unknown' END AS 	locale 
			, a.partnername					partner
			, a.partnerid					partnerID
			, a.partnerpph					partnerpph
			, COUNT(opp.sfid) OVER (PARTITION BY opp.sfid, TO_CHAR(o.effectivedate, 'YYYY-MM')) AS partner_per_opp
			, opp.name						opportunity
            , opp.sfid                      opportunityID
            , opp.status__c					opportunity_status
			, opp.monthly_partner_costs__c	ppp_monthly_partner_costs
			, opp.grand_total__c			opp_grand_total
			, invoice.net 					amount_invoice
			, opp.hours_weekly__c			opp_weekly_hours
			, opp.hours_weekly__c*4.33		opp_AVG_monthly_hours
			, oo.max_hours					opp_MAX_monthly_hours
			, MIN 		(o.effectivedate::date) 	AS first_order_date_monthly
			, first_month.first_order		first_month
			, first_month.last_order 		last_order
			, one_offs.acquistion_type		AS oneoff_acquistion_type
			, one_offs.package				AS oneoff_package
			
			-- number of order
			, COUNT(*)								orders
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN 1 ELSE 0 END )			AS FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN 1 ELSE 0 END )				AS INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN 1 ELSE 0 END )		AS CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	
							AND (o.quick_note__c ILIKE '%public_holiday%'
									OR o.error_note__c ILIKE '%public_holiday%')	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE_public_holidays

			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN 1 ELSE 0 END )		AS CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN 1 ELSE 0 END )		AS CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN 1 ELSE 0 END )			AS NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN 1 ELSE 0 END )		AS NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN 1 ELSE 0 END )		AS PENDING_ALLOCATION						
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN 1 ELSE 0 END )			AS PENDING_TO_START	
			
			-- hours (order duration)
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 		THEN o.order_duration__c ELSE 0 END )			AS h_FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 		THEN o.order_duration__c ELSE 0 END )				AS h_INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	
							AND (o.quick_note__c ILIKE '%public_holiday%'
									OR o.error_note__c ILIKE '%public_holiday%') THEN o.order_duration__c ELSE 0 END )	AS h_CANCELLED_MISTAKE_public_holidays
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'	THEN o.order_duration__c ELSE 0 END )			AS h_NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN o.order_duration__c ELSE 0 END )		AS h_NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN o.order_duration__c ELSE 0 END )		AS h_PENDING_ALLOCATION	
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'	THEN o.order_duration__c ELSE 0 END )			AS h_PENDING_TO_START	
			
			, extra_orders.extra_hours 																					AS h_extra_hours					
			
FROM		salesforce.order 		o
			
LEFT JOIN	salesforce.opportunity	opp 		ON o.opportunityid 	= opp.sfid

LEFT JOIN  
			( 	SELECT 	month
						, opp
						, SUM(amount__c)/1.19 AS net

				FROM
					(
						SELECT 	TO_CHAR(i.issued__c, 'YYYY-MM') AS month
								, i.opportunity__c AS opp
								, i.amount__c
		
						FROM 	salesforce.invoice__c i
						LEFT JOIN salesforce.invoice__c i2 ON i.sfid = i2.original_invoice__c
						WHERE 	i.original_invoice__c IS NULL 
								AND i.opportunity__c IS NOT NULL

					UNION

						SELECT 	TO_CHAR(i.issued__c, 'YYYY-MM') AS month
								, i2.opportunity__c AS opp
								, i2.amount__c
		
						FROM 	salesforce.invoice__c i
						LEFT JOIN salesforce.invoice__c i2 ON i.sfid = i2.original_invoice__c
						WHERE 	i.opportunity__c IS NOT NULL

					) As t1

				GROUP BY month
						, opp
			) as invoice 		ON o.opportunityid 	= invoice.opp AND TO_CHAR(o.effectivedate, 'YYYY-MM') = month

--LEFT JOIN 	(	SELECT 			pro.sfid			proid
--								, partner.sfid 		partnerid
--								, partner.name		partnername
--								, pro.locale__c		locale
--								, partner.pph__c	partnerpph
--								
--				FROM 			salesforce.account 	pro 
--				LEFT JOIN 		salesforce.account 	partner 	ON 	pro.parentid = partner.sfid 
--				WHERE 			pro.parentid		IS NOT NULL
--			) 						AS a	ON o.served_by__c		= a.partnerid


-- update: 2019-06-14 NOT IN DOCUMENTATION 

-- in case the opp was served by the same partner but different professionals, the previos JOIN created multiple rows per opp which ended up in MPC*2 in tableau.
-- there is no need for professional fields and meanwhile the account.served_by__c field is mapped =) 	
		
LEFT JOIN 	( SELECT 			partner.sfid 		partnerid
								, partner.name		partnername
								, partner.locale__c	locale
								, partner.pph__c	partnerpph
				FROM 			salesforce.account partner 
			) AS a ON o.served_by__c		= a.partnerid
				
-- max monthly hours and first order date monthly

LEFT JOIN
				(SELECT 	max_hours.opportunityid
							, max_hours.year_month
							, MIN (max_hours.first_order_date_monthly) 	AS first_order_date_monthly
							, SUM (max_hours.max_h) 					AS max_hours
				FROM
		
						(SELECT basic.* 
						      , SUM (CASE WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday 	THEN 1 ELSE 0 END) 	AS CW_max
						      , basic.duration * CEIL(SUM (CASE WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday 	THEN 1.0 ELSE 0.0 END) /
						      	CASE 	WHEN basic.recurrency = 7 THEN 1.0
						      			WHEN basic.recurrency = 14 THEN 2.0
						      			WHEN basic.recurrency = 21 THEN 3.0
						      			WHEN basic.recurrency = 28 THEN 4.0 ELSE 1 END)	 AS max_h
		
						FROM 	(SELECT TO_CHAR (t1.order_date::date, 'YYYY-MM') 	AS year_month
										, t1.opportunityid							AS opportunityid
										, MIN(t1.order_date::date) 					AS first_order_date_monthly
										, t1.recurrency								AS recurrency
										, t1.order_duration							AS duration
						    			, ROW_NUMBER() OVER (PARTITION BY t1.opportunityid, TO_CHAR (order_date::date, 'YYYY-MM') ORDER BY order_date::date ) AS r
						   
		    					FROM 	
		    						(SELECT opportunityid
		    								, MIN (effectivedate::date)					AS order_date
		    								, SUM(order_duration__c) 					AS order_duration
		    								, recurrency__c								AS recurrency
		    								
		    								
		    						FROM 	salesforce."order" 	orders		  		
									
									WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
											AND orders.opportunityid IS NOT NULL   
											AND orders.recurrency__c <> '0'
										
									GROUP BY opportunityid
											, effectivedate::date
											, recurrency__c		 
									) t1
										
							  	GROUP BY t1.opportunityid, TO_CHAR (t1.order_date::date, 'YYYY-MM') , t1.order_duration, t1.order_date, t1.recurrency
							   	
							    ORDER BY t1.opportunityid, year_month 
							    
							    ) basic
		    
						LEFT JOIN	salesforce.opportunity			opp2				ON  basic.opportunityid 	= opp2.sfid 
						LEFT JOIN 	bi.working_days_monthly 		days 				ON (basic.year_month = days.year_month) 
		
						WHERE 	basic.r <= opp2.times_per_week__c
						 
						GROUP BY basic.opportunityid
								, basic.duration
								, basic.first_order_date_monthly
								, basic.year_month
								, basic.r
								, basic.recurrency
		 
						) as max_hours
		 
		 		GROUP BY max_hours.opportunityid
						, max_hours.year_month
				) AS oo																	ON (o.opportunityid = oo.opportunityid 	AND oo.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))				
				
-- first month / last month
LEFT JOIN 
			( 	SELECT 	o.opportunityid			opp
						, MIN 		(effectivedate::date) 			AS first_order
						, MAX 		(effectivedate::date) 			AS last_order
				FROM 	salesforce."order" 		o			  		
							
				WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
									AND o.opportunityid IS NOT NULL
				GROUP BY o.opportunityid
									
			) AS first_month ON first_month.opp = o.opportunityid

-- extra hours			
LEFT JOIN 
			( 	SELECT 	TO_CHAR (effectivedate::date, 'YYYY-MM') 	AS year_month
						, o.opportunityid							AS opportunityid
						, COUNT(*)									AS extra_orders
						, SUM( o.order_duration__c)					AS extra_hours

				FROM 	salesforce.order o

				WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
						AND o.opportunityid IS NOT NULL
					--	AND TO_CHAR (o.effectivedate::date, 'YYYY-MM') = '2019-01'
						AND o.recurrency__c = 0
						AND o.pph__c = '29.9'
													
				GROUP BY o.opportunityid
						, TO_CHAR (effectivedate::date, 'YYYY-MM') 
						
			) AS extra_orders 					ON (extra_orders.opportunityid 	= o.opportunityid 
													AND extra_orders.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))
													
-- one offs													
LEFT JOIN
			(	SELECT opportunity 									AS opportunityid
					, TO_CHAR 	(closed_date::date, 'YYYY-MM') 		AS year_month
					, acquisition_type								AS acquistion_type
					, package										AS package
					 
				FROM bi.b2b_additional_booking one_off
			
				WHERE type = 'one-off'
			) AS one_offs 						ON	(one_offs.opportunityid = o.opportunityid
													AND one_offs.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))											


WHERE 		o.test__c 						= FALSE
			AND o.effectivedate::date 		>= '2018-01-01'
			AND o.type						IN ('cleaning-b2b','cleaning-window')
			AND (a.locale					LIKE ('de-%') OR a.locale  LIKE ('ch-%'))
			AND a.partnerid 				IS NOT NULL

GROUP BY 	 a.partnername
			, a.locale
			, a.partnerid
			, a.partnerpph
			, opp.name
            , opp.sfid  
			, opp.monthly_partner_costs__c
			, opp.grand_total__c
			, TO_CHAR(o.effectivedate, 'YYYY-MM')
			, opp.hours_weekly__c
			, invoice.net
			, oo.max_hours	
			, first_month.first_order
			, opp.status__c	
			, first_month.last_order
			, extra_orders.extra_hours
			, one_offs.acquistion_type
			, one_offs.package					
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


													-- THE NEW TRAFFIC LIGHT STARTS HERE.
													-- Created by: Sylvain Vanhuysse
													-- First integrated here on 07/08/2019

DROP TABLE IF EXISTS 	bi.opportunitylist_traffic_light_v2;
CREATE TABLE 			bi.opportunitylist_traffic_light_v2 			AS 

SELECT

	t1.locale
	, t1.Customer
	, t1.Opportunity_Name
	, t1.Opportunity
	, t1.Status
	, t1.Opps
	, t1.Owner		
	, t1.delivery_area
	, t1.operated_by
	, t1.operated_by_detail

FROM

	(SELECT		opp.locale__c 															AS locale
				, opp.customer__c														AS Customer
				, opp.name															AS Opportunity_Name
				, opp.sfid															AS Opportunity
				, opp.status__c															AS Status
				, CAST (COUNT (opp.sfid) OVER (PARTITION BY opp.customer__c) 	AS BIGINT)				AS Opps
				, tuser.name														AS Owner		
	--			, delivery_area__c													AS delivery_area_opp
				, CASE 	WHEN Opp.delivery_area__c 	IS NOT NULL 	THEN Opp.delivery_area__c
																ELSE 'unknown' 	END AS delivery_area
				, last_order.operated_by
				, last_order.operated_by_detail
				, SUM(CASE WHEN cont.service_type__c NOT LIKE '%maintenance cleaning%' THEN 1 ELSE 0 END) as not_maintenance
			   , SUM(CASE WHEN cont.service_type__c LIKE '%maintenance cleaning%' THEN 1 ELSE 0 END) as maintenance
	
	FROM 		Salesforce.opportunity 			Opp
			
	LEFT JOIN 	salesforce.user 				tuser 		ON (Opp.ownerid = tuser.sfid)
	
	-- --
	-- -- last order details
	-- -- served by BAT (TFS) or Partner
	
	LEFT JOIN
	
			(SELECT DISTINCT ON (opportunityid)
					opportunityid AS Opp
					, effectivedate::date 
					, sfid AS orderid
					, delivery_area__c
					, professional__c  AS Professional
					, operated_by
					, operated_by_detail
	
			FROM   salesforce."order" 			AS orders
			
			LEFT JOIN
				(SELECT 	Professional.sfid 																		AS professional
					, CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE 'Partner' 		END AS operated_by
					, CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE Partner.name 	END AS operated_by_detail
			
				FROM 		Salesforce.Account Professional
			
				LEFT JOIN 	Salesforce.Account Partner 		ON (Professional.parentid = Partner.sfid)
				
				)			AS ProfessionalDetails 			ON (orders.professional__c = ProfessionalDetails.professional)
			
			
			WHERE 	opportunityid 		IS NOT NULL
			
			ORDER  BY 	opportunityid 
						, effectivedate::date DESC
			
			) AS last_order 								ON (last_order.Opp = Opp.sfid)
			
	LEFT JOIN
	
		salesforce.contract__c cont
		
	ON
	
		opp.sfid = cont.opportunity__c
	
	
	WHERE 	opp.test__c = false 
			AND opp.status__c 		IN 		('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION', 'OFFBOARDING')
			AND cont.active__c IS TRUE
			AND cont.test__c IS FALSE
	
	GROUP BY 	opp.locale__c
				, opp.delivery_area__c
				, opp.customer__c
				, opp.name		
				, opp.sfid	
				, opp.status__c
				, owner
				, last_order.operated_by
				, last_order.operated_by_detail) as t1
				
WHERE

 	t1.maintenance > 0
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- new Opportunity Traffic Light based on Opportunities NOT contacts

DELETE FROM 	bi.opportunity_traffic_light_new_v2 		WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO 	bi.opportunity_traffic_light_new_v2

	SELECT		(CURRENT_DATE - INTERVAL '0 DAY')	::date						AS date
				, traffic_lights.*
	
				-- AVG Traffic light:
				-- if opp status = RETENTION or OFFBOARDING -> traffic light has te be 1(red), same for noshow and bad feedback
		
				, ROUND (CASE WHEN 	Status 										= 'RETENTION' 
									OR Status 									= 'OFFBOARDING'
									OR traffic_light_Retention 				= 1 
									OR traffic_light_noshow	= 1
									OR traffic_light_bad_feedback = 1					THEN 1 
							ELSE	((	traffic_light_cancelled_pro*3 
										+ traffic_light_inbound_calls*1
										+ traffic_light_lost_calls*1
										+ traffic_light_complaint_calls*3
										+ traffic_light_CreatedCases*3
										+ traffic_light_InvoiceCorrection*3 
										+ traffic_light_ProfessionalImprovement*2
										+ traffic_light_PartnerImprovement*2) 
										/ 18) 
										END , 1) 															AS AVG_Traffic_Light

	FROM	(
			SELECT		traffic_light_detail.*
						, ROUND ((CASE	WHEN	nsp_Rate > 0 THEN 1
										ELSE 3 
										END), 1)  															AS traffic_light_noshow
										
						, ROUND ((CASE	WHEN 	cp_Rate > 0	THEN 1
										ELSE 3 
										END), 1)  															AS traffic_light_cancelled_pro
						
						, ROUND ((CASE	WHEN 	cc_Rate 						>= 0.2 						THEN 1
										WHEN 	cc_Rate 						BETWEEN 0.005 AND 0.2 		THEN 2
										WHEN 	cc_Rate 						<= 0.005 					THEN 3 
										END), 1) 															AS traffic_light_cancelled_customer
														
						, ROUND ((CASE	WHEN 	all_inbound_calls 				>= 1 						THEN 1
										WHEN 	all_inbound_calls 				< 1 
											OR 	(all_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_inbound_calls
						, ROUND ((CASE	WHEN 	complaint_calls 				>= 1 						THEN 1
										WHEN 	complaint_calls 				< 1                     
											OR 	(complaint_calls IS NULL) 	= TRUE 					THEN 3 
										END), 1) 															AS traffic_light_complaint_calls
												
						, ROUND ((CASE	WHEN 	lost_inbound_calls				>= 1 						THEN 1
										WHEN 	lost_inbound_calls < 1 
											OR 	(lost_inbound_calls IS NULL) 	= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_lost_calls
										
						, ROUND ((CASE	WHEN 	createdcases 					>= 1 						THEN 1
										WHEN 	createdcases					<1 
											OR 	(createdcases IS NULL) 			= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_CreatedCases
										
						, ROUND ((CASE	WHEN 	retention_all 					> 0 
											AND retention_closed 				<> retention_all 			THEN 1 -- Retention case open
										WHEN 	retention_all 					> 0 
											AND retention_closed 				= retention_all 			THEN 2 -- Retention case closed
										WHEN 	retention_all 					= 0 
											OR 	(retention_all IS NULL) 		= TRUE 						THEN 3 
										END), 1) 															AS traffic_light_Retention
								
						, ROUND ((CASE	WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		<> invoicecorrection_all 	THEN 1
										WHEN 	invoicecorrection_all 			> 0 
											AND invoicecorrection_closed 		= invoicecorrection_all 	THEN 2
										WHEN 	invoicecorrection_all 			= 0 
											OR 	(invoicecorrection_all IS NULL) = TRUE 						THEN 3 
										END), 1) 															AS traffic_light_InvoiceCorrection
						
						, ROUND	((CASE	WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	<> professionalimprovement_all 	THEN 1
										WHEN 	professionalimprovement_all 	> 0 
											AND professionalimprovement_closed 	= professionalimprovement_all 	THEN 2
										WHEN 	professionalimprovement_all 	= 0 
											OR (professionalimprovement_all IS NULL) = TRUE 					THEN 3 
										END), 1) 															AS traffic_light_ProfessionalImprovement
										
						, ROUND	((CASE	WHEN 	partnerimprovement_all 	> 0 
											AND partnerimprovement_closed 	<> partnerimprovement_all 	THEN 1
										WHEN 	partnerimprovement_all 	> 0 
											AND partnerimprovement_closed 	= partnerimprovement_all 	THEN 2
										WHEN 	partnerimprovement_all 	= 0 
											OR (partnerimprovement_all IS NULL) = TRUE 					THEN 3 
										END), 1) 															AS traffic_light_PartnerImprovement
						, ROUND	((CASE	WHEN 	bad_feedback 	> 0	THEN 1
												ELSE 3 
										      END), 1) 													AS traffic_light_bad_feedback
										
			FROM 	(
					SELECT 	O.*
			
					-- orders
							, orders_final.orders_nsp
							, orders_final.nsp
							, (CASE 	WHEN orders_final.orders_nsp 	> 0 		THEN orders_final.nsp/orders_final.orders_nsp 			ELSE 0 	END) 	AS nsp_Rate
							
							, orders_final.orders_cp
							, orders_final.cp
							, (CASE 	WHEN orders_final.orders_cp 	> 0 		THEN orders_final.cp/orders_final.orders_cp 			ELSE 0 	END) 	AS cp_Rate
							
							, orders_final.orders_cc
							, orders_final.cc
							, (CASE 	WHEN orders_final.orders_cc 	> 0 		THEN orders_final.cc/orders_final.orders_cc 			ELSE 0 	END) 	AS cc_Rate
							
							, orders_final.orders_nsp_cp
							, orders_final.nsp_cp
							, (CASE 	WHEN orders_final.orders_nsp_cp > 0 		THEN orders_final.nsp_cp/orders_final.orders_nsp_cp 	ELSE 0 	END) 	AS nso_cp_Rate
							
					-- inbound cases
							, Inbound_Cases_final.createdcases
							
					-- internal cases
							, Intern_Cases_final.retention_all
							, Intern_Cases_final.retention_closed
							, Intern_Cases_final.invoicecorrection_all
							, Intern_Cases_final.invoicecorrection_closed
							, Intern_Cases_final.professionalimprovement_all
							, Intern_Cases_final.professionalimprovement_closed
							, Intern_Cases_final.partnerimprovement_all
							, Intern_Cases_final.partnerimprovement_closed
							, Intern_Cases_final.bad_feedback
							
					-- calls
							, Calls_final.all_inbound_calls
							, Calls_final.lost_inbound_calls
							, Calls_final.all_outbound
							, Calls_final.b2b_outbound
							, Calls_final.complaint_calls
							, Calls_final.no_complaint_calls
							


FROM 	bi.opportunitylist_traffic_light_v2 O


-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')

LEFT JOIN		
		
		(
		SELECT
		Opp
		, SUM(Orders_NSP)			AS Orders_NSP
		, SUM(NSP)					AS NSP
		, SUM(Orders_CP)			AS Orders_CP
		, SUM(CP)					AS CP
		, SUM(Orders_CC)			AS Orders_CC
		, SUM(CC)					AS CC
		, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
		, SUM(NSP_CP)				AS NSP_CP
		
		FROM (
		SELECT 
		
		opportunityid AS Opp
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
						WHEN status like '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
									
		FROM
		salesforce."order" AS orders
		
		WHERE
		effectivedate::date 	BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
									AND (CURRENT_DATE - INTERVAL '0 DAY')
											
		AND opportunityid IS NOT NULL
		
		GROUP BY
		Opp
		, delivery_area__c 
		
		) AS orders2
		
		GROUP BY
		Opp
		
) AS orders_final
		
ON (orders_final.opp = O.opportunity)

		
-- --
-- -- inbound cases
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
-- 22/07/2019 --> MODIFICATIONS NEW TRAFFIC LIGHT -- We now take into account only the complaint cases
		
LEFT JOIN 	
LATERAL
		
		(
		SELECT 	Opportunity
				, SUM(createdcases)								AS createdcases
		
		FROM
			(
			SELECT 	Opps.*
					, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
									THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
												THEN 0.00 
												ELSE 1.0 * createdcases / Opps END)
									ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
												THEN 0.00
												ELSE 1.0 * createdcases END) END)::numeric, 2)		AS createdcases
		
			FROM 	bi.opportunitylist_traffic_light_v2 		Opps
		
			LEFT JOIN LATERAL
				(SELECT 	cas.contactid 						AS Contact
							, cas.opportunity__c 					AS Opportunity
							, COUNT(1) 							AS CreatedCases
		
				FROM 		salesforce.case 		cas
	
				WHERE	(cas.test__c				= FALSE  
						OR cas.test__c				IS NULL)
						
						AND (	
						
						-- old case setup (3 AND NOT 11)
								(
									(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
											WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
											WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
											WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
											WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
								AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
											WHEN 	cas.type 	= 'CLM'					THEN 1
											WHEN 	cas.type	= 'CM B2C'				THEN 1
											WHEN 	cas.type	= 'Sales'				THEN 1
											WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
								)
							OR	(
									(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
											WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
											WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
											WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
								AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
											WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
								)
								
						-- new case setup
							OR	(	
									(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
								AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
											WHEN 	cas.type 	= 'Pool'				THEN 1
											WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
								)
								
							OR 	(
									(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
											WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
								)
							)
	
				
							AND cas.createddate::date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
							AND cas.reason NOT IN ('Checkout')
							
							AND cas.createdbyid = '00520000003IiNCAA0' 
							AND cas.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint')
							AND cas.origin NOT IN ('System - Notification')
	
				GROUP BY  	cas.contactid
							, cas.opportunity__c
		
				) AS Inbound_Cases 			ON 		(CASE 	WHEN Inbound_Cases.Opportunity 	IS NULL 
															THEN Opps.Customer 				= Inbound_Cases.Contact 
															ELSE Inbound_Cases.Opportunity 	= Opps.Opportunity END)
				
			) AS Inbound_Cases_f
		
			GROUP BY Opportunity
		
		) AS Inbound_Cases_final 			ON 		(Inbound_Cases_final.Opportunity = O.Opportunity)

-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- -- Partner - Improvement

LEFT JOIN

		(SELECT
		Opportunity
		, SUM(retention_all)						AS retention_all
		, SUM(Retention_closed)						AS Retention_closed
		, SUM(InvoiceCorrection_All)				AS InvoiceCorrection_All
		, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
		, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
		, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
		, SUM(PartnerImprovement_All)			AS PartnerImprovement_All
		, SUM(PartnerImprovement_closed)		AS PartnerImprovement_closed
		, SUM(bad_feedback)		AS bad_feedback
		
		
		FROM
		(
		SELECT
		Opps.*
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * retention_all / Opps END)
							ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
											THEN 0.00
											ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * Retention_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
											THEN 0.00
											ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * PartnerImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * PartnerImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * PartnerImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * PartnerImprovement_All END) END)::numeric, 2)		AS PartnerImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * PartnerImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * PartnerImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * PartnerImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * PartnerImprovement_closed END) END)::numeric, 2) 	AS PartnerImprovement_closed
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * bad_feedback / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * bad_feedback / Opps END)
							ELSE (CASE 	WHEN 1.0 * bad_feedback IS NULL
											THEN 0.00
											ELSE 1.0 * bad_feedback END) END)::numeric, 2) 	AS bad_feedback
		
		FROM bi.opportunitylist_traffic_light_v2 Opps
		
	
		LEFT JOIN 
		LATERAL
		
		(
		SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity

		, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
		AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment / invoice%'
		OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment /invoice%'
		OR subject LIKE '%nvoice %orrection%'
		AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
						
		, SUM(CASE 	WHEN reason LIKE '%Partner - Improvement%' THEN 1 ELSE 0 END) 	AS PartnerImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Partner - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS PartnerImprovement_closed
		, SUM(CASE 	WHEN reason LIKE '%Feedback / Complaint%' THEN 1 ELSE 0 END) 	AS bad_feedback

		
		FROM
		Salesforce.case	interncases	
		
		WHERE
			-- Cases created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
			CreatedDate::date between (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
									AND (CURRENT_DATE - INTERVAL '0 DAY')	
			
			AND Isdeleted 	= FALSE
			AND (test__c 	= FALSE   OR test__c	IS NULL)
			
			-- internal cases: 
			-- 	Customer - Retention 		> communication with sales
			-- 	Order - Invoice editing 	> communication with finance 
			-- 	Professional - Improvement > communication with TO
			
			AND ((reason LIKE '%Customer - Retention%' 
						OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 
						
					OR ((reason LIKE '%Professional - Improvement%')
						AND (type LIKE '%TO%' OR type LIKE '%CLM%'))
					OR (reason LIKE '%Partner - Improvement%')
					OR ((reason LIKE '%Feedback / Complaint%') AND subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3'))
					))
		
		
		GROUP BY
		Contact
		, Opportunity
		
		) AS Intern
		
		ON (CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)
				
		)		AS Intern_f
		
		GROUP BY
		Opportunity
		
) AS Intern_Cases_final
		
ON (Intern_Cases_final.opportunity = O.Opportunity)


-- --
-- -- calls:


LEFT JOIN

(
SELECT
Opps.opportunity
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)										AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)										AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
, ROUND ((CASE WHEN 1.0 * complaint_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * complaint_calls / opps END) ::numeric, 2)												AS complaint_calls					
, ROUND ((CASE WHEN 1.0 * no_complaint_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * no_complaint_calls / opps END) ::numeric, 2)												AS no_complaint_calls					


FROM bi.opportunitylist_traffic_light_v2 Opps


		LEFT JOIN 
		LATERAL
		(		
		SELECT

		RelatedContact__c 													AS Contact
		
	
		, SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						THEN 1 
						ELSE 0 END) 											AS all_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END) 											AS lost_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
						THEN 1 
						ELSE 0 END)												AS all_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS b2b_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' 
		                 AND t4.wrapup_string_1__c IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
							  AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS complaint_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' 
		                 AND calldirection__c = 'Inbound' 
		                 AND t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
							  AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS no_complaint_calls
		
		FROM 
		Salesforce.natterbox_call_reporting_object__c t4
		
			LEFT JOIN 
			salesforce.contact t5
			
			ON
			(t4.relatedcontact__c = t5.sfid)
		WHERE
		-- -- Calls created within the last 8 weeks incl. (CURRENT_DATE - INTERVAL '0 DAY')
		call_start_date_date__c::date 	BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' 
													AND (CURRENT_DATE - INTERVAL '0 DAY')
		
		AND 	(	(calldirection__c = 'Inbound' 	AND E164Callednumber__C IN ('493030807263', '493030807264'))
				OR (calldirection__c = 'Outbound' 	AND Callerfirstname__c LIKE 'CM%'))
				
		AND RelatedContact__c IS NOT NULL
		
		GROUP BY
		Contact) as Call
		
		ON
		(Opps.Customer = Call.Contact)	
		
) AS Calls_final

ON (Calls_final.Opportunity = O.Opportunity)


) AS traffic_light_detail

) AS traffic_lights

;

-- ----------------------------------------------------------------- 
-- Author: Sylvain Vanhuysse
-- Short Description: Opportunity Traffic Light TODAY. Synced with Salesforce Marketing Cloud. 
-- Created on: July 2019

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_today_v2;
CREATE TABLE 			bi.opportunity_traffic_light_today_v2	AS 

	SELECT		*
	FROM 		bi.opportunity_traffic_light_new_v2
	WHERE		date 												= (CURRENT_DATE - INTERVAL '0 DAY') 
;

-- ----------------------------------------------------------------- 
-- Author: Sylvain Vanhuysse
-- Short Description: 	Opportunity Traffic Light TODAY for tableau 
-- -------------------	(History and current AVG result in one table for tableau) 

DROP TABLE IF EXISTS 	bi.opportunity_traffic_light_tableau_v2;
CREATE TABLE 			bi.opportunity_traffic_light_tableau_v2 		AS 

	SELECT		history.date										AS history_date
				, history.locale									AS history_locale
				, history.customer									AS history_customer
				--, history.opportunity_name							AS history_opportunity_name
				, history.opportunity								AS history_opportunity
				, history.opps										AS history_opps
				, history.traffic_light_noshow						AS history_traffic_light_noshow
				, history.traffic_light_cancelled_pro				AS history_traffic_light_cancelled_pro
				, history.traffic_light_cancelled_customer			AS history_traffic_light_cancelled_customer
				, history.traffic_light_inbound_not_complaints				AS history_traffic_light_inbound_calls
				, history.traffic_light_lost_calls					AS history_traffic_light_lost_calls
				, history.traffic_light_complaint_calls         AS history_traffic_light_complaint_calls
				, history.traffic_light_createdcases				AS history_traffic_light_createdcases
				, history.traffic_light_retention					AS history_traffic_light_retention
				, history.traffic_light_invoicecorrection			AS history_traffic_light_invoicecorrection
				, history.traffic_light_professionalimprovement		AS history_traffic_light_professionalimprovement
				, history.traffic_light_partnerimprovement		AS history_traffic_light_partnerimprovement
				, history.traffic_light_bad_feedback		AS history_traffic_light_bad_feedback
				, history.avg_traffic_light							AS history_avg_traffic_light
	
				, current.date										AS current__date
				, current.opportunity								AS current_opportunity
				, current.opportunity_name							AS current_opportunity_name
				, current.delivery_area								AS current_delivery_area
				, current.status									AS current_status
				, current.owner										AS current_owner
				, current.operated_by								AS operated_by
				, current.operated_by_detail						AS operated_by_detail
				, current.traffic_light_noshow						AS current_traffic_light_noshow
				, current.traffic_light_cancelled_pro				AS current_traffic_light_cancelled_pro
				, current.traffic_light_cancelled_customer			AS current_traffic_light_cancelled_customer
				, current.traffic_light_inbound_not_complaints				AS current_traffic_light_inbound_calls
				, current.traffic_light_lost_calls					AS current_traffic_light_lost_calls
				, current.traffic_light_createdcases				AS current_traffic_light_createdcases
				, current.traffic_light_retention					AS current_traffic_light_retention
				, current.traffic_light_invoicecorrection			AS current_traffic_light_invoicecorrection
				, current.traffic_light_professionalimprovement		AS current_traffic_light_professionalimprovement
				, current.traffic_light_partnerimprovement		AS current_traffic_light_partnerimprovement
				, current.traffic_light_bad_feedback		AS current_traffic_light_bad_feedback
				, current.avg_traffic_light							AS current_avg_traffic_light
	
	FROM		bi.opportunity_traffic_light_new_v2 AS history
	
	LEFT JOIN 	(
				SELECT		*
				FROM		bi.opportunity_traffic_light_new_v2
				WHERE		date 							= (CURRENT_DATE - INTERVAL '0 DAY')
				) AS 		current							ON (history.opportunity = current.opportunity)
	
	WHERE		history.date 								BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
	
	;					




-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Calculations for the drops in TFL

DELETE FROM 	bi.opportunity_traffic_light_drops 		WHERE date = (CURRENT_DATE - INTERVAL '0 DAY');
INSERT INTO 	bi.opportunity_traffic_light_drops

SELECT		(CURRENT_DATE - INTERVAL '0 DAY')	::date						AS date -- the drop calculated today is based on what happened yesterday
		, traffic_lights.*

		-- AVG Traffic light:
		-- if opp status = RETENTION or OFFBOARDING -> traffic light has te be 1(red)
		-- The rest are equally weighted.
		, ROUND(CASE WHEN 	Status 										= 'RETENTION' 
							OR Status 									= 'OFFBOARDING'
							OR traffic_light_Retention 				= 1 
							OR traffic_light_noshow	= 1
							OR traffic_light_bad_feedback = 1					THEN 1 
					ELSE	((	traffic_light_cancelled_pro*3 
								+ traffic_light_inbound_calls*1
								+ traffic_light_lost_calls*1
								+ traffic_light_complaint_calls*3
								+ traffic_light_CreatedCases*3
								+ traffic_light_InvoiceCorrection*3 
								+ traffic_light_ProfessionalImprovement*2
								+ traffic_light_PartnerImprovement*2) 
								/ 18) 
								END , 2)															AS tfl_today
		, 3 - ROUND(CASE WHEN 	Status 										= 'RETENTION' 
							OR Status 									= 'OFFBOARDING'
							OR traffic_light_Retention 				= 1 
							OR traffic_light_noshow	= 1
							OR traffic_light_bad_feedback = 1					THEN 1 
					ELSE	((	traffic_light_cancelled_pro*3 
								+ traffic_light_inbound_calls*1
								+ traffic_light_lost_calls*1
								+ traffic_light_complaint_calls*3
								+ traffic_light_CreatedCases*3
								+ traffic_light_InvoiceCorrection*3 
								+ traffic_light_ProfessionalImprovement*2
								+ traffic_light_PartnerImprovement*2) 
								/ 18) 
								END , 2)as drop_today
		, pot.potential

FROM	(
	SELECT		traffic_light_detail.*
				, ROUND ((CASE	WHEN	nsp_Rate > 0 THEN 1
								ELSE 3 
								END), 1)  															AS traffic_light_noshow
								
				, ROUND ((CASE	WHEN 	cp_Rate > 0	THEN 1
								ELSE 3 
								END), 1)  															AS traffic_light_cancelled_pro
				
				, ROUND ((CASE	WHEN 	cc_Rate 						>= 0.2 						THEN 1
								WHEN 	cc_Rate 						BETWEEN 0.005 AND 0.2 		THEN 2
								WHEN 	cc_Rate 						<= 0.005 					THEN 3 
								END), 1) 															AS traffic_light_cancelled_customer
												
				, ROUND ((CASE	WHEN 	all_inbound_calls 				>= 1 						THEN 1
								WHEN 	all_inbound_calls 				< 1 
									OR 	(all_inbound_calls IS NULL) 	= TRUE 						THEN 3 
								END), 1) 															AS traffic_light_inbound_calls
				, ROUND ((CASE	WHEN 	complaint_calls 				>= 1 						THEN 1
								WHEN 	complaint_calls 				< 1                     
									OR 	(complaint_calls IS NULL) 	= TRUE 					THEN 3 
								END), 1) 															AS traffic_light_complaint_calls
										
				, ROUND ((CASE	WHEN 	lost_inbound_calls				>= 1 						THEN 1
								WHEN 	lost_inbound_calls < 1 
									OR 	(lost_inbound_calls IS NULL) 	= TRUE 						THEN 3 
								END), 1) 															AS traffic_light_lost_calls
								
				, ROUND ((CASE	WHEN 	createdcases 					>= 1 						THEN 1
								WHEN 	createdcases					<1 
									OR 	(createdcases IS NULL) 			= TRUE 						THEN 3 
								END), 1) 															AS traffic_light_CreatedCases
								
				, ROUND ((CASE	WHEN 	retention_all 					> 0 
									AND retention_closed 				<> retention_all 			THEN 1 -- Retention case open
								WHEN 	retention_all 					> 0 
									AND retention_closed 				= retention_all 			THEN 2 -- Retention case closed
								WHEN 	retention_all 					= 0 
									OR 	(retention_all IS NULL) 		= TRUE 						THEN 3 
								END), 1) 															AS traffic_light_Retention
						
				, ROUND ((CASE	WHEN 	invoicecorrection_all 			> 0 
									AND invoicecorrection_closed 		<> invoicecorrection_all 	THEN 1
								WHEN 	invoicecorrection_all 			> 0 
									AND invoicecorrection_closed 		= invoicecorrection_all 	THEN 2
								WHEN 	invoicecorrection_all 			= 0 
									OR 	(invoicecorrection_all IS NULL) = TRUE 						THEN 3 
								END), 1) 															AS traffic_light_InvoiceCorrection
				
				, ROUND	((CASE	WHEN 	professionalimprovement_all 	> 0 
									AND professionalimprovement_closed 	<> professionalimprovement_all 	THEN 1
								WHEN 	professionalimprovement_all 	> 0 
									AND professionalimprovement_closed 	= professionalimprovement_all 	THEN 2
								WHEN 	professionalimprovement_all 	= 0 
									OR (professionalimprovement_all IS NULL) = TRUE 					THEN 3 
								END), 1) 															AS traffic_light_ProfessionalImprovement
								
				, ROUND	((CASE	WHEN 	partnerimprovement_all 	> 0 
									AND partnerimprovement_closed 	<> partnerimprovement_all 	THEN 1
								WHEN 	partnerimprovement_all 	> 0 
									AND partnerimprovement_closed 	= partnerimprovement_all 	THEN 2
								WHEN 	partnerimprovement_all 	= 0 
									OR (partnerimprovement_all IS NULL) = TRUE 					THEN 3 
								END), 1) 															AS traffic_light_PartnerImprovement
				, ROUND	((CASE	WHEN 	bad_feedback 	> 0	THEN 1
										ELSE 3 
								      END), 1) 													AS traffic_light_bad_feedback
								
	FROM 	(
			SELECT 	O.*
	
			-- orders
					, orders_final.orders_nsp
					, orders_final.nsp
					, (CASE 	WHEN orders_final.orders_nsp 	> 0 		THEN orders_final.nsp/orders_final.orders_nsp 			ELSE 0 	END) 	AS nsp_Rate
					
					, orders_final.orders_cp
					, orders_final.cp
					, (CASE 	WHEN orders_final.orders_cp 	> 0 		THEN orders_final.cp/orders_final.orders_cp 			ELSE 0 	END) 	AS cp_Rate
					
					, orders_final.orders_cc
					, orders_final.cc
					, (CASE 	WHEN orders_final.orders_cc 	> 0 		THEN orders_final.cc/orders_final.orders_cc 			ELSE 0 	END) 	AS cc_Rate
					
					, orders_final.orders_nsp_cp
					, orders_final.nsp_cp
					, (CASE 	WHEN orders_final.orders_nsp_cp > 0 		THEN orders_final.nsp_cp/orders_final.orders_nsp_cp 	ELSE 0 	END) 	AS nso_cp_Rate
					
			-- inbound cases
					, Inbound_Cases_final.createdcases
					
			-- internal cases
					, Intern_Cases_final.retention_all
					, Intern_Cases_final.retention_closed
					, Intern_Cases_final.invoicecorrection_all
					, Intern_Cases_final.invoicecorrection_closed
					, Intern_Cases_final.professionalimprovement_all
					, Intern_Cases_final.professionalimprovement_closed
					, Intern_Cases_final.partnerimprovement_all
					, Intern_Cases_final.partnerimprovement_closed
					, Intern_Cases_final.bad_feedback
					
			-- calls
					, Calls_final.all_inbound_calls
					, Calls_final.lost_inbound_calls
					, Calls_final.all_outbound
					, Calls_final.b2b_outbound
					, Calls_final.complaint_calls
					, Calls_final.no_complaint_calls
					


FROM 	bi.opportunitylist_traffic_light_v2 O


-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) = yesterday

LEFT JOIN		

(
SELECT
Opp
, SUM(Orders_NSP)			AS Orders_NSP
, SUM(NSP)					AS NSP
, SUM(Orders_CP)			AS Orders_CP
, SUM(CP)					AS CP
, SUM(Orders_CC)			AS Orders_CC
, SUM(CC)					AS CC
, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
, SUM(NSP_CP)				AS NSP_CP

FROM (
SELECT 

opportunityid AS Opp

, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP

, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
				WHEN status like '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP

, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
				
, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
				WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
				
, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
				WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
							
FROM
salesforce."order" AS orders

WHERE
effectivedate::date = (current_date - 1)
									
AND opportunityid IS NOT NULL

GROUP BY
Opp
, delivery_area__c 

) AS orders2

GROUP BY
Opp

) AS orders_final

ON (orders_final.opp = O.opportunity)


-- --
-- -- inbound cases
-- -- Cases created yesterday
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
-- 22/07/2019 --> MODIFICATIONS NEW TRAFFIC LIGHT -- We now take into account only the complaint cases

LEFT JOIN 	
LATERAL

(
SELECT 	Opportunity
		, SUM(createdcases)								AS createdcases

FROM
	(
	SELECT 	Opps.*
			, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
										THEN 0.00 
										ELSE 1.0 * createdcases / Opps END)
							ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
										THEN 0.00
										ELSE 1.0 * createdcases END) END)::numeric, 2)		AS createdcases

	FROM 	bi.opportunitylist_traffic_light_v2 		Opps

	LEFT JOIN LATERAL
		(SELECT 	cas.contactid 						AS Contact
					, cas.opportunity__c 					AS Opportunity
					, COUNT(1) 							AS CreatedCases

		FROM 		salesforce.case 		cas

		WHERE	(cas.test__c				= FALSE  
				OR cas.test__c				IS NULL)
		
					AND cas.createddate::date = (current_date - 1)
					-- AND cas.reason NOT IN ('Checkout')
					
					-- AND cas.createdbyid = '00520000003IiNCAA0' 
					AND cas.reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint')
					AND cas.origin NOT IN ('System - Notification')

		GROUP BY  	cas.contactid
					, cas.opportunity__c

		) AS Inbound_Cases 			ON 		(CASE 	WHEN Inbound_Cases.Opportunity 	IS NULL 
													THEN Opps.Customer 				= Inbound_Cases.Contact 
													ELSE Inbound_Cases.Opportunity 	= Opps.Opportunity END)
		
	) AS Inbound_Cases_f

	GROUP BY Opportunity

) AS Inbound_Cases_final 			ON 		(Inbound_Cases_final.Opportunity = O.Opportunity)

-- --
-- -- internal cases:
-- -- Cases created yesterday
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- -- Partner - Improvement

LEFT JOIN

(SELECT
Opportunity
, SUM(retention_all)						AS retention_all
, SUM(Retention_closed)						AS Retention_closed
, SUM(InvoiceCorrection_All)				AS InvoiceCorrection_All
, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
, SUM(PartnerImprovement_All)			AS PartnerImprovement_All
, SUM(PartnerImprovement_closed)		AS PartnerImprovement_closed
, SUM(bad_feedback)		AS bad_feedback


FROM
(
SELECT
Opps.*
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * retention_all / Opps END)
					ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
									THEN 0.00
									ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * Retention_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
									THEN 0.00
									ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * InvoiceCorrection_All / Opps END)
					ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
									THEN 0.00
									ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * InvoiceCorrection_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
									THEN 0.00
									ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * ProfessionalImprovement_All / Opps END)
					ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
									THEN 0.00
									ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
									
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
									THEN 0.00
									ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed
									
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * PartnerImprovement_All / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * PartnerImprovement_All / Opps END)
					ELSE (CASE 	WHEN 1.0 * PartnerImprovement_All IS NULL
									THEN 0.00
									ELSE 1.0 * PartnerImprovement_All END) END)::numeric, 2)		AS PartnerImprovement_All
									
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * PartnerImprovement_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * PartnerImprovement_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * PartnerImprovement_closed IS NULL
									THEN 0.00
									ELSE 1.0 * PartnerImprovement_closed END) END)::numeric, 2) 	AS PartnerImprovement_closed
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * bad_feedback / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * bad_feedback / Opps END)
					ELSE (CASE 	WHEN 1.0 * bad_feedback IS NULL
									THEN 0.00
									ELSE 1.0 * bad_feedback END) END)::numeric, 2) 	AS bad_feedback

FROM bi.opportunitylist_traffic_light_v2 Opps


LEFT JOIN 
LATERAL

(
SELECT 
Contactid 																								AS Contact
, opportunity__c 																						AS Opportunity

, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
--
, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
--
, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
--
, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
OR reason LIKE '%Order - payment / invoice%'
OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
--
, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
OR reason LIKE '%Order - payment /invoice%'
OR subject LIKE '%nvoice %orrection%'
AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
--
, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
				AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
				
, SUM(CASE 	WHEN reason LIKE '%Partner - Improvement%' THEN 1 ELSE 0 END) 	AS PartnerImprovement_All
, SUM(CASE 	WHEN reason LIKE '%Partner - Improvement%'
				AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS PartnerImprovement_closed
, SUM(CASE 	WHEN subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3') THEN 1 ELSE 0 END) 	AS bad_feedback


FROM
Salesforce.case	interncases	

WHERE
	-- Cases created yesterday
	CreatedDate::date  = (current_date - 1)	
	
	AND Isdeleted 	= FALSE
	AND (test__c 	= FALSE   OR test__c	IS NULL)
	
	-- internal cases: 
	-- 	Customer - Retention 		> communication with sales
	-- 	Order - Invoice editing 	> communication with finance 
	-- 	Professional - Improvement > communication with TO
	
	AND ((reason LIKE '%Customer - Retention%' 
				OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 
				
			OR ((reason LIKE '%Professional - Improvement%')
				AND (type LIKE '%TO%' OR type LIKE '%CLM%'))
			OR (reason LIKE '%Partner - Improvement%')
			OR (-- (reason LIKE '%Feedback / Complaint%') AND 
			     subject IN ('Satisfaction Feedback: 1', 'Satisfaction Feedback: 2', 'Satisfaction Feedback: 3'))
			))


GROUP BY
Contact
, Opportunity

) AS Intern

ON (CASE WHEN Intern.Opportunity IS NULL 
		THEN Opps.Customer = Intern.Contact 
		ELSE Intern.Opportunity = Opps.Opportunity END)
		
)		AS Intern_f

GROUP BY
Opportunity

) AS Intern_Cases_final

ON (Intern_Cases_final.opportunity = O.Opportunity)


-- --
-- -- calls:


LEFT JOIN

(
SELECT
Opps.opportunity
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
			THEN 0.00 
			ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)										AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
			THEN 0.00 
			ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)										AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
			THEN 0.00 
			ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
			THEN 0.00 
			ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
, ROUND ((CASE WHEN 1.0 * complaint_calls / opps IS NULL  
			THEN 0.00 
			ELSE 1.0 * complaint_calls / opps END) ::numeric, 2)												AS complaint_calls					
, ROUND ((CASE WHEN 1.0 * no_complaint_calls / opps IS NULL  
			THEN 0.00 
			ELSE 1.0 * no_complaint_calls / opps END) ::numeric, 2)												AS no_complaint_calls					


FROM bi.opportunitylist_traffic_light_v2 Opps


LEFT JOIN 
LATERAL
(		
SELECT

RelatedContact__c 													AS Contact


, SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
				THEN 1 
				ELSE 0 END) 											AS all_inbound_calls
, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
				THEN 1 
				ELSE 0 END) 											AS lost_inbound_calls
, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
				THEN 1 
				ELSE 0 END)												AS all_outbound
, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
				THEN 1 
				ELSE 0 END) 											AS b2b_outbound
, SUM(CASE 	WHEN t4.wrapup_string_1__c IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
				THEN 1 
				ELSE 0 END) 											AS complaint_calls
, SUM(CASE 	WHEN t4.wrapup_string_1__c NOT IN ('Order - Feedback / Complaint', 'Order - Noshow Professional', 'Customer - Damage', 'Professional - Damage')
				THEN 1 
				ELSE 0 END) 											AS no_complaint_calls

FROM 
Salesforce.natterbox_call_reporting_object__c t4

	LEFT JOIN 
	salesforce.contact t5
	
	ON
	(t4.relatedcontact__c = t5.sfid)
WHERE
-- -- Calls created yesterday
call_start_date_date__c::date = (current_date - 1)

AND 	t4.notes__c LIKE 'Call%'
AND   t4.wrapup_string_2__c IN ('reached', 'not reached')
		
AND RelatedContact__c IS NOT NULL

GROUP BY
Contact) as Call

ON
(Opps.Customer = Call.Contact)	

) AS Calls_final

ON (Calls_final.Opportunity = O.Opportunity)


) AS traffic_light_detail

) AS traffic_lights

LEFT JOIN

bi.potential_revenue_per_opp as pot

ON

traffic_lights.opportunity = pot.opportunityid

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Second table allowing to calculate the drops of traffic light
-- Created on: 12/09/2019

DROP TABLE IF EXISTS bi.tfl_drops;
CREATE TABLE bi.tfl_drops AS 

SELECT

	year,
	week,
	mindate,
	opportunity_name,
	opportunity,
	status,
	owner,
	delivery_area,
	potential,
	CASE WHEN drop_tfl > 2 THEN 2 ELSE drop_tfl END as drop_tfl

FROM
	
	(SELECT
	
		date_part('year', o.date) as year,
		date_part('week', o.date) as week,
		MIN(o.date) as mindate,
		o.opportunity_name,
		o.opportunity,
		o."status",
		o.owner,
		o.delivery_area,
		MAX(o.potential) as potential,
		SUM(o.drop_today) as drop_tfl
			
	FROM
	
		bi.opportunity_traffic_light_drops o
		
	GROUP BY
	
		date_part('year', o.date),
		date_part('week', o.date),
		o.opportunity_name,
		o.opportunity,
		o."status",
		o.owner,
		o.delivery_area
		
	ORDER BY
	
		o.opportunity_name) as t1;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'
;			
