

SELECT

	o.sfid as sfid_opp,
	o.name as name_opp,
	o.served_by__c,
	cont.sfid as sfid_contract,
	cont.grand_total__c,
	o.potential_partner_costs__c

	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c cont
	
ON

	o.sfid = cont.opportunity__c
	

	
WHERE

	o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')
	AND o.test__c IS FALSE
	AND cont.test__c IS FALSE
	AND cont.active__c IS TRUE
	
	
