

SELECT

	o.sfid,
	o.name,
	o.grand_total__c,
	oo.pph__c,
	SUM(ooo.amount__c)/1.19 amount_invoiced_last_month,
	oo.confirmed_end__c,
	DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
	EXTRACT(DAY FROM oo.confirmed_end__c) as days_to_invoice,
	CASE WHEN o.grand_total__c IS NOT NULL THEN
	          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
	     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
	     END as to_pay_this_month,
	CASE WHEN o.grand_total__c IS NOT NULL THEN
	          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
	     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
	     END
	- SUM(ooo.amount__c)/1.19 as on_top_this_month

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c
	
WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED', 'OFFBOARDING')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date)::text, 7)
	AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND oo.service_type__c = 'maintenance cleaning'
	AND ooo.service_type__c = 'maintenance cleaning'
	
GROUP BY

	o.sfid,
	o.name,
	o.grand_total__c,
	oo.pph__c,
	oo.confirmed_end__c