	
SELECT

	t2.service_type__c,
	SUM(t2.grand_total__c)
	
FROM	
	
	(SELECT
	
		o.service_type__c,
		o.grand_total__c,
		t1.first_order
		
		
	FROM
	
		salesforce.contract__c o
		
	LEFT JOIN
	
		(SELECT
		
			oo.opportunityid,
			MIN(oo.effectivedate) as first_order,
			o.grand_total__c
			
		 FROM
		 
		 	salesforce."order" oo
		 	
		 LEFT JOIN
		 
		 	salesforce.opportunity o
		 	
		ON
		
			oo.opportunityid = o.sfid
		 	
		 WHERE
		 
		 	oo.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		 	AND oo.opportunityid IS NOT NULL
		 	
		 GROUP BY
		 
		 	oo.opportunityid,
			o.grand_total__c) as t1
		 	
	ON
	
	 	o.opportunity__c = t1.opportunityid
		
	WHERE
	
		o.service_type__c NOT LIKE '%maintenance cleaning%'
		AND o.test__c IS FALSE
		AND o.active__c IS TRUE
		
	GROUP BY
	
		o.service_type__c,
		o.grand_total__c,
		t1.first_order) as t2
		
WHERE

	LEFT(t2.first_order::text, 7) = '2019-09'

GROUP BY

	t2.service_type__c