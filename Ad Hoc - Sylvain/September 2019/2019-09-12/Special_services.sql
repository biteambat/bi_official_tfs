


SELECT

	o.service_type__c,
	SUM(o.amount__c) as money,
	t1.first_order
	
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	(SELECT
	
		oo.opportunityid,
		MIN(oo.effectivedate) as first_order
		
	 FROM
	 
	 	salesforce."order" oo
	 	
	 WHERE
	 
	 	oo.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
	 	AND oo.opportunityid IS NOT NULL
	 	
	 GROUP BY
	 
	 	oo.opportunityid) as t1
	 	
ON

 	o.opportunity__c = t1.opportunityid
	
WHERE

	o.service_type__c NOT LIKE '%maintenance cleaning%'
	AND o.test__c IS FALSE
	AND o.issued__c = '2019-08-31'
	
GROUP BY

	o.service_type__c,
	t1.first_order