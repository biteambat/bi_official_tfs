	
SELECT

	'New Customers This month' as kpi,
	t2.opportunityid,
	t2.name,
	t2.delivery_area__c,
	t2.grand_total__c,
	t2.on_top_this_month

FROM		
	
	(SELECT
	
		t1.opportunityid,
		t1.name,
		t1.delivery_area__c,
		t1.confirmed_end__c,
		t1.first_order,
		t1.grand_total__c,
		EXTRACT(DAY FROM t1.first_order) as day_first_order,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1 as days_to_invoice,
		(t1.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1) as on_top_this_month
	        
		
	
	FROM	
		
		(SELECT
		
			o.opportunityid,
			oo.name,
			oo.delivery_area__c,
			oo.grand_total__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c,
			MIN(o.effectivedate) as first_order
		
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
		
		LEFT JOIN
	
			salesforce.contract__c ooo
		
		ON
	
			o.opportunityid = ooo.opportunity__c
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND LEFT(o.locale__c, 2) = 'de'
			-- AND (ooo.active__c IS TRUE )
			AND ooo.service_type__c = 'maintenance cleaning'
			AND oo.test__c IS FALSE
			AND (LEFT(ooo.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
		       OR ooo.confirmed_end__c IS NULL)
			    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
					
		GROUP BY
		
			o.opportunityid,
			oo.name,
			oo.grand_total__c,
			oo.delivery_area__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c) as t1
			
	WHERE
		
		LEFT(t1.first_order::text, 7) = LEFT((current_date)::text, 7)
		
	GROUP BY
		
		t1.opportunityid,
		t1.name,
		t1.grand_total__c,
		t1.delivery_area__c,
		t1.confirmed_end__c,
		t1.first_order) as t2
	
