

DROP TABLE IF EXISTS bi.forecast_revenue;
CREATE TABLE bi.forecast_revenue as 

SELECT

	'Churned Customers Last Month' as kpi,
	o.sfid,
	o.name,
	o.delivery_area__c,
	-- oo.confirmed_end__c,
	o.grand_total__c,
	-- SUM(ooo.amount__c)/1.19 amount_invoiced,

	-SUM(ooo.amount__c)/1.19 as on_top_this_month

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND oo.service_type__c = 'maintenance cleaning'
	AND ooo.service_type__c = 'maintenance cleaning'
	
GROUP BY

	o.sfid,
	o.name,
	o.delivery_area__c,
	o.grand_total__c
	-- oo.confirmed_end__c
	;
	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
	
SELECT

	'Churned Customers This month' as kpi,
	t1.sfid,
	t1.name,
	t1.delivery_area__c,
	t1.grand_total__c,
	t1.on_top_this_month

FROM	
	
	(SELECT
	
		o.sfid,
		o.name,
		o.delivery_area__c,
		o.grand_total__c,
		oo.pph__c,
		SUM(ooo.amount__c)/1.19 amount_invoiced_last_month,
		oo.confirmed_end__c,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		EXTRACT(DAY FROM oo.confirmed_end__c) as days_to_invoice,
		CASE WHEN o.grand_total__c IS NOT NULL THEN
		          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     END as to_pay_this_month,
		CASE WHEN o.grand_total__c IS NOT NULL THEN
		          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     END
		- SUM(ooo.amount__c)/1.19 as on_top_this_month
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.sfid = oo.opportunity__c
		
	LEFT JOIN
	
		salesforce.invoice__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
		
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c IN ('RESIGNED', 'CANCELLED', 'OFFBOARDING')
		AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		-- AND ooo.issued__c::text IS NULL
		AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date)::text, 7)
		AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
		AND oo.service_type__c = 'maintenance cleaning'
		AND ooo.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.delivery_area__c,
		o.grand_total__c,
		oo.pph__c,
		oo.confirmed_end__c) as t1;
		
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
		
SELECT

	'New Customers Last Month' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	-- t1.confirmed_end__c,
	-- t1.first_order,	
	t1.grand_total__c,
	-- SUM(inv.amount__c)/1.19 as invoiced_last_month,
	t1.grand_total__c - SUM(inv.amount__c)/1.19 as on_top_this_month
	
FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.delivery_area__c,
		oo.name,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c) as t1
		
LEFT JOIN

	salesforce.invoice__c inv
	
ON

	t1.opportunityid = inv.opportunity__c
		
WHERE
	
	LEFT(t1.first_order::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7)
	     OR LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
	     OR t1.confirmed_end__c IS NULL)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7) OR t1.confirmed_end__c IS NULL)
	AND inv.service_type__c = 'maintenance cleaning'
	
GROUP BY
	
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.grand_total__c
	-- t1.confirmed_end__c
	;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'New Customers This month' as kpi,
	t2.opportunityid,
	t2.name,
	t2.delivery_area__c,
	t2.grand_total__c,
	t2.on_top_this_month

FROM		
	
	(SELECT
	
		t1.opportunityid,
		t1.name,
		t1.delivery_area__c,
		t1.confirmed_end__c,
		t1.first_order,
		t1.grand_total__c,
		EXTRACT(DAY FROM t1.first_order) as day_first_order,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1 as days_to_invoice,
		(t1.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1) as on_top_this_month
	        	
	FROM	
		
		(SELECT
		
			o.opportunityid,
			oo.name,
			oo.delivery_area__c,
			oo.grand_total__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c,
			MIN(o.effectivedate) as first_order
		
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
		
		LEFT JOIN
	
			salesforce.contract__c ooo
		
		ON
	
			o.opportunityid = ooo.opportunity__c
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND LEFT(o.locale__c, 2) = 'de'
			-- AND (ooo.active__c IS TRUE )
			AND ooo.service_type__c = 'maintenance cleaning'
			AND oo.test__c IS FALSE
			AND (LEFT(ooo.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
		       OR ooo.confirmed_end__c IS NULL)
			    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
					
		GROUP BY
		
			o.opportunityid,
			oo.name,
			oo.grand_total__c,
			oo.delivery_area__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c) as t1
			
	WHERE
		
		LEFT(t1.first_order::text, 7) = LEFT((current_date)::text, 7)
		
	GROUP BY
		
		t1.opportunityid,
		t1.name,
		t1.grand_total__c,
		t1.delivery_area__c,
		t1.confirmed_end__c,
		t1.first_order) as t2;
	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
		
SELECT

	'Difference PPH' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.pph__c,
	t1.invoiced_sept - t1.invoiced_august as on_top_this_month
	
FROM

	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.pph__c,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END) as hours_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END) as hours_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END) as hours_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END) as hours_oct,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_oct
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN
	
		salesforce.contract__c ooo
	
	ON
	
		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		AND oo.grand_total__c IS NULL
		AND ooo.pph__c IS NOT NULL
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.pph__c) as t1;		

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Invoiced Maintenance Cleaning Last Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	494351 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Cancellations This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	-10179 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Other Discounts This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	-5541 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Extra Orders This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	827 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Manual Invoices This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	3877 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;


---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Blocked Not Invoiced This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	-2312 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Grand Total Adjustments This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	NULL as grand_total__c,
	624 as on_top_this_month
	
FROM

	salesforce.contact
	
LIMIT 1;



