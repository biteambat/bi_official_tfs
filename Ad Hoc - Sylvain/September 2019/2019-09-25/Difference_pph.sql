
SELECT

	'Difference PPH' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.pph__c,
	t1.invoiced_sept - t1.invoiced_august as on_top_this_month
	
FROM

	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.pph__c,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END) as hours_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END) as hours_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END) as hours_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END) as hours_oct,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_oct
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN
	
		salesforce.contract__c ooo
	
	ON
	
		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		AND oo.grand_total__c IS NULL
		AND ooo.pph__c IS NOT NULL
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.pph__c) as t1