SELECT

	'Churned Customers Last Month' as kpi,
	o.sfid,
	o.name,
	o.delivery_area__c,
	-- oo.confirmed_end__c,
	o.grand_total__c,
	-- SUM(ooo.amount__c)/1.19 amount_invoiced,

	-SUM(ooo.amount__c)/1.19 as on_top_this_month

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND oo.service_type__c = 'maintenance cleaning'
	AND ooo.service_type__c = 'maintenance cleaning'
	
GROUP BY

	o.sfid,
	o.name,
	o.delivery_area__c,
	o.grand_total__c
	-- oo.confirmed_end__c