

SELECT

	relatedcontact__c,
	COUNT(DISTINCT o.sfid) as calls,
	sum(o.calltalkseconds__c) as time


FROM

	salesforce.natterbox_call_reporting_object__c o
	
WHERE

	o.relatedcontact__c = '0030J000020GC8yQAG'
				-- AND o.calldirection__c = 'Inbound'
				AND o.createddate >= '2017-01-01'
	
group by

	relatedcontact__c