

SELECT

	o.createddate,
	o.sfid,
	o.createdbyid,
	o.relatedcontact__c,
	o.calltalkseconds__c,
	o.wrapup_string_1__c,
	o.wrapup_string_2__c,
	o.department__c,
	o.owner__c,
	o.ownerid,
	o.opportunity__c

FROM

	salesforce.natterbox_call_reporting_object__c o
	
WHERE

	o.relatedcontact__c IS NOT NULL
	AND o.calldirection__c = 'Inbound'
	AND o.createddate >= '2018-01-01'
	
ORDER BY

	o.createddate desc