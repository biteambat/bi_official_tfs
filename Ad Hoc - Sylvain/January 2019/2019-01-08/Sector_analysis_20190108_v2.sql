SELECT

	t1.*,
	CASE WHEN 
			(DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24 IS NULL 
		  THEN 0
		  ELSE (DATE_PART('day', t2.date_churn::timestamp - t3.date_start::timestamp)*24 + DATE_PART('hour', t2.date_churn::timestamp - t3.date_start::timestamp))/24
		  END
	 as relationship_days
	
	
FROM
	
	bi.sector_analysis t1
	
LEFT JOIN

	((SELECT
		
		LEFT(o.locale__c, 2) as country,
		o.locale__c,
		o.opportunityid,
		o.delivery_area__c,
		MIN(o.effectivedate) as date_start
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		
	GROUP BY
	
		o.opportunityid,
		o.locale__c,
		o.delivery_area__c,
		LEFT(o.locale__c, 2))) as t3
		
ON

	t1.opportunity = t3.opportunityid
	
LEFT JOIN

	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
						         -- It's the last day on which they are making money
		LEFT(o.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		-- AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(o.locale__c, 2),
		type_date,
		o.opportunityid) as t2
		
ON

	t1.opportunity = t2.opportunityid
		
		
		
		
		