SELECT
	
	o.createddate,
	o.reason,
	o.ownerid,
	o.sfid,
	o.contactid,
	o.order__c,
	o.accountid,
	o.origin,
	o.type,
	o.opportunity__c,
	o.feedback_list__c,
	o.feedback_categories__c

FROM

	salesforce.case o

WHERE

	createddate > '2017-12-31'
	AND (o.createdbyid IN ('00520000003b8qzAAA', -- Ninja Tiger
	                      '00520000003IiNCAA0', -- API Tiger
								 '00520000003h5YhAAI', -- Marketing
								 '00520000003bc3gAAA') -- Accounting
		  OR LOWER(o.origin) LIKE '%b2b%'
		  OR o.type IN ('KA', 'B2B', 'Sales', 'PM')
		  OR o.opportunity__c IS NOT NULL)