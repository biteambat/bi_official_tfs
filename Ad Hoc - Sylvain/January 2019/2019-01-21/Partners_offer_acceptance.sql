	
	
SELECT

	TO_CHAR(t2.createddate, 'YYYY-MM') as year_month_offer,
	t1.*,
	SUM(CASE WHEN ((t2.suggested_partner1__c = t1.sfid
				  OR t2.suggested_partner2__c = t1.sfid
				  OR t2.suggested_partner3__c = t1.sfid
				  OR t2.suggested_partner4__c = t1.sfid
				  OR t2.suggested_partner5__c = t1.sfid))
		      THEN 1
		      ELSE 0
				END) as offers_received,
	SUM(CASE WHEN (t2.partner__c = t1.sfid AND t2.status__c = 'ACCEPTED') THEN 1 ELSE 0 END) as offers_accepted

FROM	
	
	(SELECT
	
		a.sfid,
		a.name,
		a.company_name__c,
		a.delivery_areas__c,
		a.type__c,
		a.status__c,
		a.role__c
		
	
	FROM
	
		salesforce.account a
		
	WHERE
	
		a.type__c = 'partner'
		AND a.test__c IS FALSE
		AND a.status__c IN ('ACTIVE', 'BETA')
		AND LEFT(a.locale__c, 2) = 'de'
		AND a.role__c = 'master'
		AND a.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
		AND a.name NOT LIKE '%Handyman Kovacs%'
		AND a.name NOT LIKE '%BAT Business Services GmbH%'
		
	ORDER BY
	
		a.name) as t1
		
LEFT JOIN

	salesforce.partner_offer__c t2
	
	
ON

	t2.suggested_partner5__c = t1.sfid
	OR t2.suggested_partner4__c = t1.sfid
	OR t2.suggested_partner3__c = t1.sfid
	OR t2.suggested_partner2__c = t1.sfid
	OR t2.suggested_partner1__c = t1.sfid

WHERE

	TO_CHAR(t2.createddate, 'YYYY-MM') IS NOT NULL
	
GROUP BY

	TO_CHAR(t2.createddate, 'YYYY-MM'),
	t1.sfid,
	t1.name,
	t1.company_name__c,
	t1.delivery_areas__c,
	t1.type__c,
	t1.status__c,
	t1.role__c
	
ORDER BY

	TO_CHAR(t2.createddate, 'YYYY-MM') desc
	
	
	