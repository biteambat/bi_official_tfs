


select 

-- distinct o.delivery_area__c

	sfid,
	name

from

salesforce.opportunity o

WHERE

	o.delivery_area__c IS NULL
	AND o.stagename IN ('WON', 'PENDING')
	AND o.status__c NOT IN ('RESIGNED', 'CANCELLED')
	AND o.test__c IS FALSE