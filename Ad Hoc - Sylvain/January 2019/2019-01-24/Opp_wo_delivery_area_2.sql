

SELECT

	o.opportunityid,
	o.shippingcity
	
FROM

	salesforce."order" o
	
WHERE

	(o."status" = 'INVOICED'
	OR o."status" = 'PENDING TO START')
	AND o.delivery_area__c IS NULL
	AND o.effectivedate > '2018-12-31'
	AND o.test__c IS FALSE
	AND o.locale__c LIKE 'de%'
	-- o."type" = 'cleaning-b2b'
	
GROUP BY
	
	o.opportunityid,
	o.shippingcity