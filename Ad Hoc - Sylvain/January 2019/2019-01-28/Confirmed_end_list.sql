

SELECT

	t1.opportunityid,
	t1.status__c,
	t1.sfid,
	t2.number_contracts

FROM


	(SELECT
	
		o.opportunity__c as opportunityid,
		o.status__c,
		o.sfid,
		COUNT(DISTINCT o.sfid) as number_contracts
		-- CASE WHEN o.confirmed_end__c IS NULL THEN (current_date + 365) ELSE o.confirmed_end__c END as date_churn
	
	FROM
	
		salesforce.contract__c o
		
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		
	GROUP BY
	
		o.opportunity__c,
		o.sfid,
		o.status__c) as t1
	
LEFT JOIN

	(SELECT

		o.opportunity__c as opportunityid,
		COUNT(DISTINCT o.sfid) as number_contracts
		-- CASE WHEN o.confirmed_end__c IS NULL THEN (current_date + 365) ELSE o.confirmed_end__c END as date_churn
	
	FROM
	
		salesforce.contract__c o
		
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		
	GROUP BY
	
		o.opportunity__c) as t2
		
ON

	t1.opportunityid = t2.opportunityid


