SELECT

	o.opportunity__c

FROM

	salesforce.contract__c o
	
WHERE

	o.status__c IN ('RESIGNED', 'CANCELLED')
	AND o.confirmed_end__c IS NULL
	AND o.test__c IS FALSE
	
GROUP BY

	o.opportunity__c
	