SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	-- CAST('-' as varchar) as city,
	table1.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn Beta' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
			  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
			  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
			  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
			  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
			  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
			  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
			  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
			  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
			  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
			  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
			  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
			  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
			  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
			  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
			  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
			  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
			  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
			  ELSE '>M18'
			  END as sub_kpi_2,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
		  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
		  ELSE 'Unlimited'
		  END as sub_kpi_3,
		  
	CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  WHEN table1.grand_total IS NULL THEN 'Unknown'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
	CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  WHEN table1.grand_total IS NULL THEN 'Unknown'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
	-- table1.opportunityid,
	SUM(table1.grand_total) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		o.delivery_area__c,
		o.opportunityid,
		ooo.potential as grand_total,
		'last_order' as type_date,
		MIN(o.effectivedate) as date_start,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunityid = ooo.opportunityid
		
	LEFT JOIN
	
		(SELECT
		
			t3.*,
			CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
			CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
		
		FROM	
			
			(SELECT
			
				t1.country,
				t1.opportunityid,
				t1.date_last_order,
				t2.date_churn as contract_end
			
			FROM
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
				         -- It's the last day on which they are making money
					LEFT(o.locale__c, 2) as country,
					o.opportunityid,
					'last_order' as type_date,
					MAX(o.effectivedate) as date_last_order
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND oo.test__c IS FALSE
				
				GROUP BY
				
					LEFT(o.locale__c, 2),
					type_date,
					o.opportunityid) as t1
					
			LEFT JOIN
			
				(SELECT -- Here we make the list of opps with their confirmed end or end date when available
										
					o.opportunity__c as opportunityid,
					MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
				
				FROM
				
					salesforce.contract__c o
				
				WHERE
				
					o.test__c IS FALSE
					AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
					-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
					
				GROUP BY
				
					o.opportunity__c) as t2 
			
			ON
			
				t1.opportunityid = t2.opportunityid) as t3) as t4
				
	ON
	
		o.opportunityid = t4.opportunityid
		
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.test__c IS FALSE
		AND o.professional__c IS NOT NULL
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		o.delivery_area__c,
		ooo.potential,
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	-- CASE WHEN oooo.grand_total__c IS NULL THEN oooo.pph__c*4.3*oooo.hours_weekly__c ELSE oooo.grand_total__c END,
	table1.country,
	table1.locale__c,
	table1.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	-- table1.opportunityid
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;