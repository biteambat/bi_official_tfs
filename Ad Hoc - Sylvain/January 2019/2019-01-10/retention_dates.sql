

SELECT

	*
	
FROM

	salesforce.opportunityfieldhistory t8
	
WHERE

	t8.field = 'status__c'
	AND (t8.newvalue = 'RETENTION' OR t8.oldvalue = 'RETENTION')