SELECT

	o.sfid,
	o.name,
	o.status__c,
	oo.confirmed_end__c

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
WHERE

	o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.confirmed_end__c IS NOT NULL