

WITH

	t1 AS (SELECT
	
				*
			 FROM
			 
			 	salesforce.opportunity
			 	
			 WHERE
			 
			 	createddate >= '2017-01-01'
			 	AND test__c IS FALSE),
			 	
	t9 AS (SELECT
	
				*
			 
			 FROM
			 
			 	salesforce.case
			 	
			 WHERE
			 
			 	createddate > '2017-12-31')

SELECT
	
	TO_CHAR(t1.createddate, 'YYYY-MM') as year_month_opp_creation,
	MIN(t5.createddate) as createddate_likeli,
	MIN(t1.createddate) as createddate_opp,
	MAX(t4.onboarded_date) as onboarded_date,
	t1.sfid as opportunity,
	t1.customer__c,
	t1.stagename,
	t1.status__c,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END as sector,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END as object_type,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END as potential_€,
	SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN 1 ELSE 0 END) as calls_sales,
	SUM(CASE WHEN t7.createddate < t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_sales,
	SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN 1 ELSE 0 END) as calls_cm,
	SUM(CASE WHEN t7.createddate >= t4.onboarded_date THEN t7.calltalkseconds__c ELSE 0 END) as time_phone_cm,
	COUNT(DISTINCT t9.sfid) as cases_created
	
FROM

	t1
	
LEFT JOIN
	
	bi.potential_revenue_per_opp t2
		
ON

	t1.sfid = t2.opportunityid
	
LEFT JOIN -- Now joining a list containing the date when an opp went to the status "PENDING"
	
	(SELECT
	
		o.opportunityid,
		MIN(o.createddate) as onboarded_date
	
	FROM
	
		salesforce.opportunityfieldhistory o
		
	WHERE
	
		o.field = 'StageName'
		AND o.newvalue IN ('PENDING', 'WON')
		
	GROUP BY
	
		o.opportunityid) as t4
		
ON
	
	t1.sfid = t4.opportunityid	
	
LEFT JOIN

	salesforce.likeli__c t5
	
ON

	t1.customer__c = t5.customer__c
	
LEFT JOIN

	salesforce.natterbox_call_reporting_object__c t7
	
ON

	t1.customer__c = t7.relatedcontact__c
	
LEFT JOIN

	bi.opportunity_traffic_light_new t8
	
ON 

	t1.sfid = t8.opportunity
	
LEFT JOIN

	t9
	
ON

	t1.customer__c = t9.contactid

	
GROUP BY

	TO_CHAR(t1.createddate, 'YYYY-MM'),
	t1.customer__c,
	t1.stagename,
	t1.status__c,
	t1.sfid,
	CASE WHEN t1.sector__c IS NULL THEN 'Unknown' ELSE t1.sector__c END,
	CASE WHEN t1.object_type__c IS NULL THEN 'Unknown' ELSE t1.object_type__c END,
	CASE WHEN t2.potential < 150 THEN t2.grand_total__c ELSE t2.potential END
	
	
	
	
	
	
	
	