
SELECT

	t4.year_month,
	t4.locale,
	t4.type,
	t4.hr_contract_weekly_hours_min__c,
	t4.sfid,
	SUM(t4.hours_done) as hours_done,
	-- SUM(CASE WHEN t4.tot_hours_done >= t4.hr_contract_weekly_hours_min__c*4 THEN t4.tot_hours_done*12.9 ELSE t4.tot_hours_done*12.9 END) as tot_salary_paid,
	SUM(t4.hours_done)*12.9 as partial_salary,
	SUM(t4.revenue_netto) as partial_revenue_netto,
	SUM(t4.revenue_netto)/SUM(t4.hours_done) as pph_netto,
	SUM(t4.revenue_brutto)/SUM(t4.hours_done) as pph_brutto,
	(SUM(t4.revenue_netto) - SUM(t4.hours_done)*12.9)/SUM(t4.revenue_netto) as gpm
		
FROM
		
	(SELECT
	
		t2.year_month,
		t2.locale,
		t2.company_name__c,
		t2.sfid,
		t2.hr_contract_weekly_hours_min__c,
		t2.type,
		t2.hours_done,
		CASE WHEN t2.type = 'cleaning-b2b' THEN t2.revenue ELSE t2.revenue/1.19 END as revenue_netto,
		CASE WHEN t2.type = 'cleaning-b2b' THEN t2.revenue*1.19 ELSE t2.revenue END as revenue_brutto,
		SUM(t3.hours_done) as tot_hours_done
		-- SUM(CASE WHEN t2.hours_done >= t2.hr_contract_weekly_hours_min__c THEN t2.hours_done*12.9 ELSE t2.hr_contract_weekly_hours_min__c*12.9 END) as salary_paid
	
	FROM	
		
		(SELECT
		
			TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
			LEFT(o.locale__c, 2) as locale,
			t1.company_name__c,
			t1.sfid,
			a.hr_contract_weekly_hours_min__c,
			o."type",
			SUM(o.order_duration__c) as hours_done,
			SUM(CASE WHEN o."type" = 'cleaning-b2b' THEN o.order_duration__c*o.eff_pph ELSE o.order_duration__c*o.pph__c END) as revenue
			-- SUM(o.gmv_eur_net) as revenue
		
		
		FROM
		
			bi.list_company_cleaners t1
			
		LEFT JOIN
		
			bi.orders o
			
		ON
		
			t1.sfid = o.professional__c
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			t1.sfid = a.sfid
		
		
		WHERE
		
			t1.company_name__c = 'BAT Business Services GmbH'
			AND o.effectivedate >= '2018-01-01'
			AND o.effectivedate < '2019-01-01'
			AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
			AND o.type NOT IN ('training')
			-- AND LEFT(o.locale__c, 2) IN ('de')
			
		
		GROUP BY
		
			TO_CHAR(o.effectivedate, 'YYYY-MM'),
			t1.sfid,
			LEFT(o.locale__c, 2),
			t1.company_name__c,
			a.hr_contract_weekly_hours_min__c,
			o."type") as t2
			
	LEFT JOIN
	
		(SELECT
		
			TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
			t1.company_name__c,
			t1.sfid,
			a.hr_contract_weekly_hours_min__c,
			o."type",
			SUM(o.order_duration__c) as hours_done,
			SUM(o.order_duration__c*o.eff_pph) as revenue
			-- SUM(o.gmv_eur_net) as revenue
				
		FROM
		
			bi.list_company_cleaners t1
			
		LEFT JOIN
		
			bi.orders o
			
		ON
		
			t1.sfid = o.professional__c
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			t1.sfid = a.sfid
		
		
		WHERE
		
			t1.company_name__c = 'BAT Business Services GmbH'
			AND o.effectivedate >= '2018-01-01'
			AND o.effectivedate < '2019-01-01'
			AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
			AND o.type NOT IN ('training')
			-- AND LEFT(o.locale__c, 2) IN ('de')
					
		GROUP BY
		
			TO_CHAR(o.effectivedate, 'YYYY-MM'),
			t1.sfid,
			t1.company_name__c,
			a.hr_contract_weekly_hours_min__c,
			o."type") as t3
			
	ON
	
		t2.sfid = t3.sfid
		AND t2.year_month = t3.year_month
		
	GROUP BY
	
		t2.year_month,
		t2.company_name__c,
		t2.sfid,
		t2.locale,
		t2.hr_contract_weekly_hours_min__c,
		t2."type",
		t2.hours_done,
		t2.revenue) as t4
		
WHERE

	t4.locale = 'de'

GROUP BY

	t4.year_month,
	t4.type,
	t4.locale,
	t4.hr_contract_weekly_hours_min__c,
	t4.sfid
	
ORDER BY

	t4.year_month