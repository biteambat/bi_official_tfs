	
	
SELECT

	t1.*,
	CASE WHEN t1.grand_total__c IS NULL THEN t1.potential ELSE t1.grand_total__c END as revenue

FROM	
	
	(SELECT
	
		o.sfid,
		pot.potential,
		o.grand_total__c,
		TO_CHAR(o.closedate,'YYYY-MM') as year_month_closed,
		o.closedate,
		MIN(oo.effectivedate) as first_order
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp pot
		
	ON
	
		o.sfid = pot.opportunityid
		
	WHERE
	
		o.test__c IS FALSE
		AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND o.stagename IN ('WON', 'PENDING')
		
	GROUP BY
		
		o.sfid,
		o.grand_total__c,
		TO_CHAR(o.closedate,'YYYY-MM'),
		o.closedate,
		pot.potential) AS t1
		
		