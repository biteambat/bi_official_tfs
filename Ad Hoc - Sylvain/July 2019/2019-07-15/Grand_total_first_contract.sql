	
SELECT

	t3.*,
	t4.grand_total__c,
	t4.pph__c

FROM
	
	(SELECT
	
		t1.opportunity__c,
		t1.contractid,
		t2.date_first_contract
	
	FROM	
		
		(SELECT
		
			o.opportunity__c,
			oo.parentid as contractid,
			MIN(oo.createddate) as date_first_contract
			
		FROM
		
			salesforce.contract__c o
		
		
		LEFT JOIN
		
			salesforce.contract__history oo
			
		ON
		
			o.sfid = oo.parentid
			
		WHERE
		
			oo.newvalue IN ('SIGNED', 'ACCEPTED')
			AND oo.field = 'status__c'
			AND o.test__c IS FALSE
			
		GROUP BY
		
			o.opportunity__c,
			oo.parentid
			
		ORDER BY
		
			o.opportunity__c,
			oo.parentid) as t1
			
	LEFT JOIN
		
		(SELECT
		
			o.opportunity__c,
			MIN(oo.createddate) as date_first_contract
			
		FROM
		
			salesforce.contract__c o
		
		
		LEFT JOIN
		
			salesforce.contract__history oo
			
		ON
		
			o.sfid = oo.parentid
			
		WHERE
		
			oo.newvalue IN ('SIGNED', 'ACCEPTED')
			AND oo.field = 'status__c'
			AND o.test__c IS FALSE
			
		GROUP BY
		
			o.opportunity__c
			
		ORDER BY
		
			o.opportunity__c) as t2
			
	ON
	
		t1.opportunity__c = t2.opportunity__c
		AND t1.date_first_contract = t2.date_first_contract
		
	WHERE
	
		t2.date_first_contract IS NOT NULL) as t3
		
LEFT JOIN

	salesforce.contract__c t4
	
ON

	t3.contractid = t4.sfid
	
WHERE

	t4.test__c IS FALSE
	-- AND t3.opportunity__c = '0060J00000nbLg5QAE'
		
	
		