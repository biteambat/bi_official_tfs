

SELECT 	

	o.sfid as "Id",
	o.lost_date__c,
	o.grand_total__c,
	o.contract_duration__c,
	o.createddate,
	o.ownerid,
	u.name,
	o.lastactivitydate,
	li.acquisition_tracking_id__c,
	o.contact_name__c,
	o.email__c,
	o.supplies__c	

FROM 	

	salesforce.opportunity o
	
LEFT JOIN	

	salesforce.contact 		co

ON 			

	o.customer__c = co.sfid
	
LEFT JOIN	

	salesforce.likeli__c li
	
ON

	o.sfid = li.opportunity__c

LEFT JOIN

	salesforce.user u
	
ON

	o.ownerid = u.sfid
WHERE 	

	o.stagename LIKE 	'%LOST%'
	AND o.stagename NOT LIKE '%WON%'