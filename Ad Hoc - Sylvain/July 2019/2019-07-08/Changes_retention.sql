


SELECT

	TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
	MIN(o.createddate) as mindate,
	o.opportunityid,
	oo.name,
	oo.status__c,
	1 as numb_opps

FROM

	salesforce.opportunityfieldhistory o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON
	
	o.opportunityid = oo.sfid
	
WHERE

	o.newvalue IN ('RETENTION', 'OFFBOARDING', 'RESIGNED', 'CANCELLED')
	-- o.newvalue IN ('RETENTION')
	AND o.createddate >= '2019-06-10'
	AND oo.status__c NOT LIKE '%RUNNING%'
	AND oo.test__c IS FALSE
	
GROUP BY

	TO_CHAR(o.createddate, 'YYYY-WW'),
	o.opportunityid,
	oo.name,
	oo.status__c