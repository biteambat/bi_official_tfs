

SELECT

	o.opportunityid as opp_sfid,
	oo.sfid as opp_opp,
	oo.customer__c,
	oo.name,
	oo.acquisition_channel__c,
	i.acquisition_tracking_id__c,
	oo.stagename,
	oo.status__c,
	oo.sector__c,
	oo.object_type__c,
	oo.closedate,
	ooo.potential,
	oo.grand_total__c,
	oo.plan_pph__c,
	oo.office_size__c,
	oo.office_employees__c,
	oo.times_per_week__c,
	oo.recurrency__c,
	oo.hours_weekly__c,
	oo.supplies__c,
	oo.contract_duration__c,
	oo.shippingaddress_city__c,
	oo.shippingaddress_postalcode__c,
	oo.delivery_area__c	

FROM

	salesforce.order o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
LEFT JOIN

	bi.potential_revenue_per_opp ooo
	
ON

	o.opportunityid = ooo.opportunityid
	
LEFT JOIN

	salesforce.likeli__c i
	
ON

	o.opportunityid = i.opportunity__c
	
WHERE

	o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
	AND o.effectivedate >= '2019-07-01'
	AND o.effectivedate <= '2019-07-31'
	AND LEFT(o.locale__c, 2) LIKE '%de%'
	
GROUP BY

	o.opportunityid,
	oo.sfid,
	oo.acquisition_channel__c,
	i.acquisition_tracking_id__c,
	oo.customer__c,
	oo.name,
	oo.stagename,
	oo.status__c,
	oo.sector__c,
	oo.object_type__c,
	oo.closedate,
	ooo.potential,
	oo.grand_total__c,
	oo.plan_pph__c,
	oo.office_size__c,
	oo.office_employees__c,
	oo.times_per_week__c,
	oo.recurrency__c,
	oo.hours_weekly__c,
	oo.supplies__c,
	oo.contract_duration__c,
	oo.shippingaddress_city__c,
	oo.shippingaddress_postalcode__c,
	oo.delivery_area__c