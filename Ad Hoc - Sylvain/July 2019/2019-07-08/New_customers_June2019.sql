	

SELECT

	t1.opportunityid,
	t1.name,
	t1.status__c,
	t1.grand_total__c,
	t1.resignation_date__c,
	t1.confirmed_end__c,
	t1.first_order

FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.status__c,
		ooo.status__c,
		oo.grand_total__c,
		ooo.start__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		AND o.test__c IS FALSE
		AND ooo.service_type__c = 'maintenance cleaning'
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		ooo.start__c,
		ooo.status__c,
		oo.grand_total__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c) as t1
		
WHERE
	
	LEFT(t1.first_order::text, 7) = '2019-06'
	
GROUP BY

		t1.opportunityid,
	t1.name,
	t1.status__c,
	t1.grand_total__c,
	t1.resignation_date__c,
	t1.confirmed_end__c,
	t1.first_order
	
	
