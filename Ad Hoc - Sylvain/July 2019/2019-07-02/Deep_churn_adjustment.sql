SELECT

	table3.year_month,
	table3.year_month_start,
	table3.mindate_churn as date,
	-- max_date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	
	table3.pricing,
	table3.money,
	table3.churn_reason,
	table3.contract_duration,
	
	table3.value,
	
	CASE WHEN table6.provider IS NULL THEN 'Not matched yet' ELSE table6.provider END as provider,
	table6.company_name,
	CASE WHEN table3.churn_reason LIKE '%one off%' THEN 'One-Off'
		  ELSE 'Recurrent'
		  END as acquisition_type,
	MIN(CASE WHEN (table7.name NOT LIKE '%S' AND table7.name NOT LIKE '%G' AND table7.name NOT LIKE '%N') THEN table7.amount__c ELSE 0 END)/1.19 as amount__c

FROM	
	
	(SELECT
	
		TO_CHAR((table1.confirmed_end)::date,'YYYY-MM') as year_month,
		TO_CHAR(table1.date_start,'YYYY-MM') as year_month_start,
		MIN(table1.confirmed_end) as mindate_churn,
		LEFT(table1.country,2) as locale,
		table1.contract_when_accepted__c,
		table1.closedate,
		table1.locale__c as languages,
		-- CAST('-' as varchar) as city,
		table1.delivery_area__c as city,
		table1.opportunityid,
		table1.name,
		CAST('B2B' as varchar) as type,
		CAST('Churn' as varchar) as kpi,
		CAST('Count opps' as varchar) as sub_kpi_1,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
				  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
				  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
				  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
				  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
				  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
				  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
				  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
				  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
				  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
				  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
				  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
				  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
				  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
				  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
				  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
				  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
				  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
				  ELSE '>M18'
				  END as sub_kpi_2,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			  
		CASE WHEN table1.money < 250 THEN '<250€'
			  WHEN table1.money >= 250 AND table1.money < 500 THEN '250€-500€'
			  WHEN table1.money >= 500 AND table1.money < 1000 THEN '500€-1000€'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
		CASE WHEN table1.money < 250 THEN 'Very Small'
			  WHEN table1.money >= 250 AND table1.money < 500 THEN 'Small'
			  WHEN table1.money >= 500 AND table1.money < 1000 THEN 'Medium'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
		table1.pricing,
		table1.money,
		table1.churn_reason__c as churn_reason,
		MAX(table1.contract_duration) contract_duration,
		COUNT(DISTINCT table1.opportunityid) as value
	
	FROM
		
		(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
		         -- It's the last day on which they are making money
			LEFT(oo.locale__c, 2) as country,
			oo.locale__c,
			o.delivery_area__c,
			o.opportunityid,
			oo.name,
			ooooo.potential as money,
			CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END as pricing,
			'last_order' as type_date,
			MIN(o.effectivedate) as date_start,
			MAX(o.effectivedate) as date_churn,
			oooo.confirmed_end__c as confirmed_end,
			MIN(oo.contract_when_accepted__c) as contract_when_accepted__c,
			MIN(oo.closedate) as closedate,
			oo.churn_reason__c,
			CASE WHEN oooo.grand_total__c = '0' THEN 'irregular' ELSE
			(CASE 
				-- duration: unlimited
				WHEN oooo.additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
				-- duration: 12 month
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
				-- duration: 6 month
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
				-- duration: 3 month
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
				-- duration: 1 month
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
				ELSE (CASE WHEN oooo.duration__c IS NULL THEN '1' ELSE oooo.duration__c END)END) END as contract_duration
			
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			o.opportunityid = oo.sfid
			
		LEFT JOIN
		
			bi.b2borders ooo
			
		ON
		
			o.opportunityid = ooo.opportunity_id
			
		LEFT JOIN
		
			salesforce.contract__c oooo
		
		ON
		
			o.opportunityid = oooo.opportunity__c

		LEFT JOIN

			bi.potential_revenue_per_opp ooooo

		ON

			o.opportunityid = ooooo.opportunityid
			
		WHERE
		
			o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.status__c IN ('RESIGNED', 'CANCELLED')
			-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
			AND oo.test__c IS FALSE
			AND o.test__c IS FALSE
			AND o.professional__c IS NOT NULL
			AND oooo.service_type__c LIKE 'maintenance cleaning'
			AND oooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND o.effectivedate >= '2018-01-01'
			-- AND oooo.active__c IS TRUE
		
		GROUP BY
		
			LEFT(oo.locale__c, 2),
			oo.locale__c,
			o.delivery_area__c,
			type_date,
			ooooo.potential,
			CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END,
			oo.churn_reason__c,
			o.opportunityid,
			oo.name,
			oooo.confirmed_end__c,
			contract_duration) as table1
			
	GROUP BY
	
		TO_CHAR((table1.confirmed_end)::date,'YYYY-MM'),
		TO_CHAR(table1.date_start,'YYYY-MM'),
		table1.country,
		table1.locale__c,
		table1.delivery_area__c,
		table1.opportunityid,
		table1.name,
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table1.pricing,
		table1.money,
		-- table2.operated_by,
		-- table2.operated_by_detail,
		table1.contract_when_accepted__c,
		table1.churn_reason__c,
		-- table1.contract_duration,
		table1.closedate
		
	ORDER BY
	
		TO_CHAR(table1.confirmed_end::date,'YYYY-MM') desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_new table4
	
ON
	
	table3.opportunityid = table4.opportunity
	
LEFT JOIN

	bi.b2b_additional_booking table5
	
ON 
	
	table3.opportunityid = table5.opportunity
		
LEFT JOIN

	bi.order_provider table6
	
ON
	
	table3.opportunityid = table6.opportunityid
	
LEFT JOIN

	salesforce.invoice__c table7
	
ON

	table3.opportunityid = table7.opportunity__c
	AND table3.year_month = TO_CHAR(table7.issued__c, 'YYYY-MM')
	
GROUP BY

	table3.year_month,
	table3.year_month_start,
	table3.mindate_churn,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.pricing,
	table3.money,
	table3.churn_reason,
	table3.contract_duration,
	table3.value,
	table6.provider,
	table5.acquisition_type,
	table6.company_name
	
ORDER BY

	table3.year_month desc;