		
SELECT

	t4.year_month,
	t4.professional__c,
	t4.hours_invoiced,
	t4.monthly_hours,
	CASE WHEN t4.hours_invoiced >= t4.monthly_hours THEN t4.hours_invoiced*12.9 ELSE t4.monthly_hours*12.9 END as operational_costs,
	t4.netto_invoiced,
	(t4.netto_invoiced - (CASE WHEN t4.hours_invoiced >= t4.monthly_hours THEN t4.hours_invoiced*12.9 ELSE t4.monthly_hours*12.9 END)) as gp,
	CASE WHEN t4.netto_invoiced <> 0 THEN
		  (t4.netto_invoiced - (CASE WHEN t4.hours_invoiced >= t4.monthly_hours THEN t4.hours_invoiced*12.9 ELSE t4.monthly_hours*12.9 END))/t4.netto_invoiced
	     ELSE -1000
		  END as gpm

FROM

		
	(SELECT
	
		t3.year_month,
		t3.professional__c,
		t3.monthly_hours,
		SUM(t3.final_hours) as hours_invoiced,
		SUM(t3.netto_invoiced) as netto_invoiced
	
	FROM	
		
		(SELECT
		
		 opps.date_part as year_month,
		 opps.locale,
		 opps.city,
		 opps.opportunity__c,
		 opps.professional__c,
		 opps.final_amount_invoiced as brutto_invoiced,
		 opps.final_amount_invoiced/1.19 as netto_invoiced,
		 opps.hours_invoiced1,
		 opps.hours_invoiced2,
		 CASE WHEN opps.hours_invoiced2 IS NULL THEN opps.hours_invoiced1 ELSE opps.hours_invoiced2 END as final_hours,
		 (CASE WHEN opps.hours_invoiced2 IS NULL THEN opps.hours_invoiced1 ELSE opps.hours_invoiced2 END)*12.9 as operational_costs,
		 CASE WHEN opps.final_amount_invoiced/1.19 <> 0 THEN
		 		(opps.final_amount_invoiced/1.19 - (CASE WHEN opps.hours_invoiced2 IS NULL THEN opps.hours_invoiced1 ELSE opps.hours_invoiced2 END)*12.9)/(opps.final_amount_invoiced/1.19)
				ELSE -1000
				END as gpm,
		 o.monthly_partner_costs__c,
		 acc.hr_contract_weekly_hours_min__c*4.3 as monthly_hours
		
		
		FROM
		
			(SELECT	
				
				TO_CHAR(t1.date,'YYYY-MM') as date_part,
				MIN(t1.date) as date,
				t1.locale,
				t1.languages as languages,
				t1.city as city,
				t1.type,
				t1.kpi,
				t1.sub_kpi_1,
				t1.sub_kpi_2,
				t1.sub_kpi_3,
				t1.sub_kpi_4,
				t1.sub_kpi_5,
				
				t1.name_first_invoice,
				t1.opportunity__c,
				t1.professional__c,
			
				t1.date_first_invoice,
				t1.first_invoice,
				t1.first_amount,
				t1.original_invoice__c,
				-- t1.name_second_invoice,
				-- MAX(t1.date_second_invoice) as date_last_invoice,
				SUM(t1.first_amount)/COUNT(t1.first_invoice) as amount_first_invoice,
				SUM(t1.first_amount) as sum_first_amount,
				SUM(t1.second_amount) as sum_second_amount,
				COUNT(t1.first_invoice) as number_invoices,
				CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
				     ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
					  END as final_amount_invoiced,
				SUM(t1.invoiced1) as hours_invoiced1,
				SUM(t1.invoiced2) as hours_invoiced2
				
			FROM
			
				(SELECT
			
					-- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
					MIN(o.issued__c::date) as date,
					LEFT(o.locale__c,2) as locale,
					o.locale__c as languages,
					oooo.delivery_area__c as city,
					CAST('B2B' as varchar) as type,
					CAST('Revenue Adjusted' as varchar) as kpi,
					CAST('Netto' as varchar) as sub_kpi_1,
					CAST('Monthly' as varchar) as sub_kpi_2,
					op.provider as sub_kpi_3,
					op.professional__c,
					o.service_type__c as sub_kpi_4,
					CAST('-' as varchar) as sub_kpi_5,
					o.sfid as first_invoice,
					o.name as name_first_invoice,			
					o.opportunity__c,
					o.createddate as date_first_invoice,
					o.amount__c as first_amount,
					oo.original_invoice__c,
					oo.name as name_second_invoice,
					oo.createddate as date_second_invoice,
					oo.sfid as second_invoice,
					oo.amount__c as second_amount,
					o.total_invoiced_hours_count__c as invoiced1,
					oo.total_invoiced_hours_count__c	as invoiced2
					
				FROM
					
					salesforce.invoice__c o
					
				LEFT JOIN
					
					salesforce.invoice__c oo
					
				ON
					
					o.sfid = oo.original_invoice__c
					
				LEFT JOIN
					
					salesforce.order ooo
					
				ON 
					
					o.opportunity__c = ooo.opportunityid
			
				LEFT JOIN
						
					salesforce.opportunity oooo
					
				ON 
				
					o.opportunity__c = oooo.sfid
					
				LEFT JOIN
			
					bi.order_provider op
					
				ON
				
					oooo.sfid = op.opportunityid
					
				WHERE
					
					o.opportunity__c IS NOT NULL
					AND o.test__c IS FALSE
					AND o.issued__c IS NOT NULL
					AND o.original_invoice__c IS NULL
					AND o.service_type__c = 'maintenance cleaning'
			
					-- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
					
				GROUP BY
					
					-- TO_CHAR(o.issued__c,'YYYY-MM'),
					LEFT(o.locale__c,2),
					o.locale__c,
					oooo.delivery_area__c,
					o.name,
					o.opportunity__c,
					o.issued__c,
					o.createddate,
					o.sfid,
					o.amount__c,
					oo.original_invoice__c,
					oo.name,
					oo.createddate,
					oo.sfid,
					oo.amount__c,
					op.provider,
					op.professional__c,
					o.service_type__c,
					o.total_invoiced_hours_count__c,
					oo.total_invoiced_hours_count__c
					
				ORDER BY
				
					date desc) as t1
					
					
			GROUP BY
			
				TO_CHAR(t1.date,'YYYY-MM'),
				t1.locale,
				t1.languages,
				t1.city,
				t1.type,
				t1.kpi,
				t1.sub_kpi_1,
				t1.sub_kpi_2,
				t1.sub_kpi_3,
				t1.sub_kpi_4,
				t1.sub_kpi_5,
				t1.name_first_invoice,
				t1.opportunity__c,
				t1.professional__c,
				t1.date_first_invoice,
				t1.first_invoice,
				t1.first_amount,
				t1.original_invoice__c) as opps
				
		LEFT JOIN
		
			salesforce.opportunity o
			
		ON
		
			opps.opportunity__c = o.sfid
			
		LEFT JOIN
		
			salesforce.account acc
			
		ON
		
			opps.professional__c = acc.sfid
			
		WHERE
		
			opps.sub_kpi_3 = 'BAT'
			
		ORDER BY
		
			year_month desc) as t3
			
	GROUP BY
	
		t3.year_month,
		t3.professional__c,
		t3.monthly_hours) as t4
		
ORDER BY

	year_month desc