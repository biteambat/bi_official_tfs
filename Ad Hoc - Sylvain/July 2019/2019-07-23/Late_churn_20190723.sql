



SELECT

	o.opportunityid,
	MAX(o.createddate) as churn_date,
	cont.confirmed_end__c,
	o.newvalue,
	oo.status__c,
	oo.name,
	oo.grand_total__c


FROM

	salesforce.opportunityfieldhistory o
	
LEFT JOIN

	salesforce.contract__c cont
	
ON

	o.opportunityid = cont.opportunity__c

LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
WHERE

	o.newvalue IN ('RESIGNED', 'CANCELLED')
	AND cont.status__c IN ('RESIGNED', 'CANCELLED')
	AND cont.service_type__c = 'maintenance cleaning'
	AND cont.test__c IS FALSE
	AND cont.confirmed_end__c < '2019-07-01'
	AND o.createddate >= '2019-07-01'
	
GROUP BY

	o.opportunityid,
	o.newvalue,
	oo.status__c,
	oo.name,
	oo.grand_total__c,
	cont.confirmed_end__c