(SELECT

	o.opportunity__c,
	oo.parentid as contractid,
	MIN(oo.createddate) as date_first_contract
	
FROM

	salesforce.contract__c o


LEFT JOIN

	salesforce.contract__history oo
	
ON

	o.sfid = oo.parentid
	
WHERE

	oo.newvalue IN ('SIGNED', 'ACCEPTED')
	AND oo.field = 'status__c'
	AND o.test__c IS FALSE
	AND o.opportunity__c = '0060J00000nbLg5QAE'
	
GROUP BY

	o.opportunity__c,
	oo.parentid
	
ORDER BY

	o.opportunity__c,
	oo.parentid)