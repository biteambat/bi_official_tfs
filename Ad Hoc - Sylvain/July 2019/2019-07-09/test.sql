(SELECT
	
		o.opportunity__c,
		MIN(oo.createddate) as date_first_contract
	
	
	FROM
	
		salesforce.contract__c o
	
	
	LEFT JOIN
	
		salesforce.contract__history oo
		
	ON
	
		o.sfid = oo.parentid
		
	WHERE
	
		oo.newvalue IN ('SIGNED', 'ACCEPTED')
		AND oo.field = 'status__c'
		
	GROUP BY
	
		o.opportunity__c)