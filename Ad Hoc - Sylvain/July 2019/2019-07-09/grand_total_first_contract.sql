	
	
SELECT

	t1.opportunity__c,
	t1.date_first_contract,
	oo.grand_total__c,
	-- oo.pph__c,
	ct.createddate,
	ct.when_accepted__c,
	ct.grand_total__c	as grand_total_first_contract

FROM	
	
	(SELECT
	
		o.opportunity__c,
		MIN(oo.createddate) as date_first_contract
	
	
	FROM
	
		salesforce.contract__c o
	
	
	LEFT JOIN
	
		salesforce.contract__history oo
		
	ON
	
		o.sfid = oo.parentid
		
	WHERE
	
		oo.newvalue IN ('SIGNED', 'ACCEPTED')
		AND oo.field = 'status__c'
		
	GROUP BY
	
		o.opportunity__c) as t1
		
LEFT JOIN

	salesforce.contract__c ct
	
ON
	
	(LEFT(ct.when_accepted__c::text,10) = LEFT(t1.date_first_contract::text, 10) OR LEFT(ct.createddate::text,10) = LEFT(t1.date_first_contract::text, 10))
	AND t1.opportunity__c = ct.opportunity__c
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	t1.opportunity__c = oo.sfid
	
	
	
		
		
		