SELECT

	table3.year_month,
	table3.year_month_start,
	table3.date,
	-- max_date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	
	table3.pricing,
	table3.money,
	table3.churn_reason__c,
	table3.contract_duration,
	
	table3.value,
		
	CASE WHEN table6.provider IS NULL THEN 'Not matched yet' ELSE table6.provider END as provider,
	table6.company_name,
	CASE WHEN table3.churn_reason__c LIKE '%one off%' THEN 'One-Off'
		  ELSE 'Recurrent'
		  END as acquisition_type,
	MIN(CASE WHEN (table7.name NOT LIKE '%S' AND table7.name NOT LIKE '%G' AND table7.name NOT LIKE '%N') THEN table7.amount__c ELSE 0 END)/1.19 as amount__c

FROM	
	
	(SELECT
	
		year_month,
		year_month_start,
		date,
		-- max_date,
		locale,
		contract_when_accepted__c,
		closedate,
		languages,
		city,
		table2.opportunityid,
		table2.name,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		
		table2.pricing,
		table2.grand_total as money,
		table2.churn_reason__c,
		table2.contract_duration,
		
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			TO_CHAR(table1.date_start,'YYYY-MM') as year_month_start,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			table1.contract_when_accepted__c,
			table1.closedate,
			table1.churn_reason__c,
			table1.contract_duration,
			
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps' as varchar) as kpi,
			CAST('Count' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.name,
			table1.category,
			table1.pricing,
			table1.grand_total
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				ooooo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.name,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							MIN(CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date 
							         ELSE t2.confirmed_end__c
								     END) as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.confirmed_end__c, 'YYYY-MM') || '-01' END as year_month_churn,
							t1.contract_when_accepted__c,
							t1.closedate,
							t1.churn_reason__c,
							MAX(t1.money) as money,
							t1.pricing,
							MAX(t1.contract_duration) as contract_duration
																								
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								oo.name,
								o.delivery_area__c,
								MIN(o.effectivedate) as date_start,
								MIN(oo.contract_when_accepted__c) as contract_when_accepted__c,
								MIN(oo.closedate) as closedate,
								oo.churn_reason__c,
								ooooo.potential as money,
								CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END as pricing,
								CASE WHEN oooo.grand_total__c = '0' THEN 'irregular' ELSE
								(CASE 
									-- duration: unlimited
									WHEN oooo.additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
									-- duration: 12 month
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
									-- duration: 6 month
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
									-- duration: 3 month
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
									-- duration: 1 month
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
									ELSE (CASE WHEN oooo.duration__c IS NULL THEN '1' ELSE oooo.duration__c END)END) END as contract_duration
							
							FROM
							
								salesforce.order o	
								
							LEFT JOIN
			
								salesforce.opportunity oo
				
							ON 
			
								o.opportunityid = oo.sfid
								
							LEFT JOIN
			
								salesforce.contract__c oooo
							
							ON
							
								o.opportunityid = oooo.opportunity__c

							LEFT JOIN

								bi.potential_revenue_per_opp ooooo

							ON

								o.opportunityid = ooooo.opportunityid
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
								AND o.test__c IS FALSE
								AND oooo.service_type__c LIKE 'maintenance cleaning'
								AND oooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
								AND o.effectivedate >= '2017-01-01'
			                    -- AND oooo.active__c IS TRUE
								
							GROUP BY
							
								o.opportunityid,
								CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END,
								oooo.confirmed_end__c,
								oo.name,
								ooooo.potential,
								o.locale__c,
								contract_duration,
								oo.churn_reason__c,
								o.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn,
								oooo.confirmed_end__c
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							LEFT JOIN
			
								salesforce.contract__c oooo
							
							ON
							
								o.opportunityid = oooo.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND oo.test__c IS FALSE
								AND o.effectivedate >= '2017-01-01'
								AND oooo.service_type__c LIKE 'maintenance cleaning'
								AND oooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								oooo.confirmed_end__c,
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid
							
						GROUP BY
						
							
							t1.country,
							t1.locale__c,
							t1.delivery_area__c,
							t1.opportunityid,
							t1.name,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01',
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.confirmed_end__c, 'YYYY-MM') || '-01' END,
							t1.contract_when_accepted__c,
							t1.closedate,
							t1.churn_reason__c,
							-- t1.money,
							t1.pricing) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid

			LEFT JOIN

				bi.potential_revenue_per_opp ooooo

			ON

				t2.opportunityid = ooooo.opportunityid
	
			WHERE 
	
				o.test__c IS FALSE
				
			GROUP BY
			
					ooooo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.name,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category,
					t2.contract_when_accepted__C,
					t2.closedate,
					t2.churn_reason__c,
					t2.money,
					t2.pricing,
					t2.contract_duration				
				) as table1
	
		WHERE
	
			table1.opportunityid IS NOT NULL
				
		GROUP BY
		
			table1.year_month,
			TO_CHAR(table1.date_start,'YYYY-MM'),
			table1.ymd_max,
			table1.contract_when_accepted__c,
			table1.closedate,
			table1.churn_reason__c,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid,
			table1.name,
			table1.pricing,
			table1.grand_total,
			table1.contract_duration
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	GROUP BY
	
		year_month,
		year_month_start,
		date,
		-- max_date,
		locale,
		contract_when_accepted__c,
		closedate,
		churn_reason__c,
		languages,
		city,
		type,
		kpi,
		opportunityid,
		name,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.pricing,
		table2.grand_total,
		table2.contract_duration
		
	ORDER BY 
	
		year_month desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_new table4
	
ON
	
	table3.opportunityid = table4.opportunity
	
LEFT JOIN

	bi.b2b_additional_booking table5
	
ON 
	
	table3.opportunityid = table5.opportunity
	
LEFT JOIN

	bi.order_provider table6
	
ON
	
	table3.opportunityid = table6.opportunityid
	
LEFT JOIN

	salesforce.invoice__c table7
	
ON

	table3.opportunityid = table7.opportunity__c
	AND table3.year_month = TO_CHAR(table7.issued__c, 'YYYY-MM')
	
WHERE

	table3.value = 1
	-- AND table3.year_month = '2018-09'
	-- AND table3.locale = 'de'
	
GROUP BY

	table3.year_month,
	table3.year_month_start,
	table3.date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.pricing,
	table3.money,
	table3.churn_reason__c,
	table3.contract_duration,
	table3.value,
	table6.provider,
	table5.acquisition_type,
	table6.company_name
	
ORDER BY

	table3.year_month desc;