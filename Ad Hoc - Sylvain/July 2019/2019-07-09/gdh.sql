(SELECT
	
		t1.opp_sfid,
		t1.opp_opp,
		t1.customer__c,
		t1.name,
		t1.acquisition_channel__c,
		t1.acquisition_tracking_id__c,
		t1.stagename,
		t1.status__c,
		t1.sector__c,
		t1.object_type__c,
		t1.closedate,
		t1.potential,
		t1.grand_total__c,
		t1.plan_pph__c,
		t1.office_size__c,
		t1.office_employees__c,
		t1.times_per_week__c,
		t1.recurrency__c,
		t1.hours_weekly__c,
		t1.supplies__c,
		t1.contract_duration__c,
		t1.shippingaddress_city__c,
		t1.shippingaddress_postalcode__c,
		t1.delivery_area__c,
		SUM(CASE WHEN t1.active__c IS TRUE THEN 1 ELSE 0 END) as number_active_contracts,
		SUM(CASE WHEN (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN 1 ELSE 0 END) as number_inactive_contracts,
		SUM(CASE WHEN t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as all_active_services€,
		SUM(CASE WHEN (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED'))THEN t1.grand_total_contract ELSE 0 END) as all_inactive_services€,
		SUM(CASE WHEN t1.service_type__c = 'maintenance cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_maintenance_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'temporary maintenance staff service' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_temporary_maintenance€,
		SUM(CASE WHEN t1.service_type__c = 'intensive cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_intensive_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'carpet cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_carpet_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'final construction cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_final_construction_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'window cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_window_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'coffee machine' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_coffee_machine€,
		SUM(CASE WHEN t1.service_type__c = 'deep cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_deep_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'facade cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_facade_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'water dispenser' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as active_water_dispenser€,
		
		SUM(CASE WHEN t1.service_type__c = 'maintenance cleaning' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_maintenance_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'temporary maintenance staff service' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_temporary_maintenance€,
		SUM(CASE WHEN t1.service_type__c = 'intensive cleaning' AND t1.active__c IS TRUE THEN t1.grand_total_contract ELSE 0 END) as inactive_intensive_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'carpet cleaning' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_carpet_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'final construction cleaning' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_final_construction_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'window cleaning' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_window_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'coffee machine' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_coffee_machine€,
		SUM(CASE WHEN t1.service_type__c = 'deep cleaning' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_deep_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'facade cleaning' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_facade_cleaning€,
		SUM(CASE WHEN t1.service_type__c = 'water dispenser' AND (t1.active__c IS FALSE AND t1.status_contract IN ('RESIGNED', 'CANCELLED')) THEN t1.grand_total_contract ELSE 0 END) as inactive_water_dispenser€
			
	FROM
	
		
		(SELECT
		
			o.opportunityid as opp_sfid,
			oo.sfid as opp_opp,
			oo.customer__c,
			oo.name,
			oo.acquisition_channel__c,
			i.acquisition_tracking_id__c,
			oo.stagename,
			oo.status__c,
			oo.sector__c,
			oo.object_type__c,
			oo.closedate,
			ooo.potential,
			oo.grand_total__c,
			oo.plan_pph__c,
			oo.office_size__c,
			oo.office_employees__c,
			oo.times_per_week__c,
			oo.recurrency__c,
			oo.hours_weekly__c,
			oo.supplies__c,
			oo.contract_duration__c,
			oo.shippingaddress_city__c,
			oo.shippingaddress_postalcode__c,
			oo.delivery_area__c,
			ct.sfid as sfid_contract,
			ct.active__c,
			ct.status__c as status_contract,
			ct.grand_total__c as grand_total_contract,
			ct.service_type__c
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp ooo
			
		ON
		
			o.opportunityid = ooo.opportunityid
			
		LEFT JOIN
		
			salesforce.likeli__c i
			
		ON
		
			o.opportunityid = i.opportunity__c
			
		LEFT JOIN
		
			salesforce.contract__c ct
			
		ON
		
			o.opportunityid = ct.opportunity__c
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
			AND o.effectivedate >= '2019-07-01'
			AND o.effectivedate <= '2019-07-31'
			AND LEFT(o.locale__c, 2) LIKE '%de%'
			AND ct.status__c IN ('RESIGNED', 'CANCELLED', 'SIGNED', 'ACCEPTED')
			AND ct.test__c IS FALSE
			
		GROUP BY
		
			o.opportunityid,
			oo.sfid,
			oo.acquisition_channel__c,
			i.acquisition_tracking_id__c,
			oo.customer__c,
			oo.name,
			oo.stagename,
			oo.status__c,
			oo.sector__c,
			oo.object_type__c,
			oo.closedate,
			ooo.potential,
			oo.grand_total__c,
			oo.plan_pph__c,
			oo.office_size__c,
			oo.office_employees__c,
			oo.times_per_week__c,
			oo.recurrency__c,
			oo.hours_weekly__c,
			oo.supplies__c,
			oo.contract_duration__c,
			oo.shippingaddress_city__c,
			oo.shippingaddress_postalcode__c,
			oo.delivery_area__c,
			ct.sfid,
			ct.active__c,
			ct.grand_total__c,
			ct.service_type__c,
			ct.status__c) as t1
			
	GROUP BY
	
		t1.opp_sfid,
		t1.opp_opp,
		t1.customer__c,
		t1.name,
		t1.acquisition_channel__c,
		t1.acquisition_tracking_id__c,
		t1.stagename,
		t1.status__c,
		t1.sector__c,
		t1.object_type__c,
		t1.closedate,
		t1.potential,
		t1.grand_total__c,
		t1.plan_pph__c,
		t1.office_size__c,
		t1.office_employees__c,
		t1.times_per_week__c,
		t1.recurrency__c,
		t1.hours_weekly__c,
		t1.supplies__c,
		t1.contract_duration__c,
		t1.shippingaddress_city__c,
		t1.shippingaddress_postalcode__c,
		t1.delivery_area__c)