


SELECT

	o.sfid,
	o.name


FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
WHERE

	o.status__c = 'OFFBOARDING'
	AND o.test__c IS FALSE
	AND oo.confirmed_end__c < current_date
	AND oo.service_type__c = 'maintenance cleaning'