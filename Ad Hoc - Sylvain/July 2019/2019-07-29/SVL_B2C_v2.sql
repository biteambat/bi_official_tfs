




SELECT

	TO_CHAR(cas.createddate, 'YYYY-MM') as year_month,
	u.name,
	u.sfid,
	COUNT(DISTINCT cas.sfid) as cases_total,
	SUM(CASE WHEN cas."status" = 'Closed' THEN 1 ELSE 0 END) as cases_closed,
	
	SUM(CASE WHEN cas."status" = 'Closed' THEN 1 ELSE 0 END)::decimal/COUNT(DISTINCT cas.sfid) as SVL



FROM

	salesforce.case cas
	
LEFT JOIN	

	salesforce.user u 		
	
ON 

	cas.ownerid = u.sfid
	
WHERE

	((CASE	WHEN 	u.name 	LIKE 	'%Marleen Dreyer%' 			THEN 1 
			WHEN  u.name 		LIKE 	'%Vivien Greve%' 				THEN 1ELSE 0 END) = 1									--2				
	)
	
	AND cas.createddate::date >= '2019-01-01'
	AND cas.test__c IS FALSE
	AND cas.origin NOT LIKE '%outbound%'
	-- AND cas.isclosed = TRUE
	
GROUP BY

	TO_CHAR(cas.createddate, 'YYYY-MM'),
	u.name,
	u.sfid