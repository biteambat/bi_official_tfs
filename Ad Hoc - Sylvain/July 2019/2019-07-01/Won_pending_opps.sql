


SELECT

	o.name,
	o.sfid
	
FROM

	salesforce.opportunity o
	
WHERE

	o.stagename IN ('WON', 'PENDING')