-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Cases, open and closed since 2018-11-01 (new case setup)
-- Created on: 24/05/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_basis;
CREATE 	TABLE 				bi.CM_cases_basis 	AS 

SELECT 		cas.sfid						case_ID
			, cas.casenumber				case_number
			, cas.createddate				case_createddate
			, cas.isclosed					case_isclosed
			, cas.ownerid					case_ownerid
			, u.name						case_owner
			, cas.origin					case_origin
			, cas.type						case_type
			, cas.reason					case_reason
			, cas.status					case_status
			, cas.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, cas.order__c					orderid
			, o.type						order_type
			, cas.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, cas.opportunity__c			opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
FROM salesforce.case 				cas
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

WHERE

(opp.test__c				= FALSE  
OR opp.test__c				IS NULL)

-- just CM B2B
--	excluded case owner	
AND ((CASE	WHEN 	u.name 	LIKE 	'%Marleen Dreyer%' 			THEN 1 
			WHEN  u.name 		LIKE 	'%Vivien Greve%' 				THEN 1ELSE 0 END) = 1									--2				
	)
	
AND cas.createddate::date >= '2019-01-01'
;					

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Backlog today (used for the backlog history tracking)
-- Created on: 01/11/2018

DROP 	TABLE IF EXISTS 	bi.CM_cases_today;
CREATE 	TABLE 				bi.CM_cases_today 	AS 

SELECT 		*
			
FROM 		bi.CM_cases_basis 		ca

WHERE		ca.case_isclosed		= FALSE	
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Cases created yesterday (used for the case inbound history tracking)
-- Created on: 07/11/2018

DROP 	TABLE IF EXISTS 	bi.CM_cases_created_yesterday;
CREATE 	TABLE 				bi.CM_cases_created_yesterday 	AS 

SELECT 	*

FROM 	bi.cm_cases_basis 	ca

WHERE 	ca.case_createddate::date = 'YESTERDAY'
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Backlog History (count) 
-- Created on: 01/11/2018

DELETE FROM 	bi.CM_cases 		WHERE date = CURRENT_DATE AND kpi = 'Open Cases';
INSERT INTO 	bi.CM_cases
SELECT		
			TO_CHAR (CURRENT_DATE,'YYYY-IW') 		AS date_part
			, MIN 	(CURRENT_DATE::date) 			AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Open Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6	
			, MIN 	(cases.case_createddate::date)	AS sub_kpi_7
			, cases.Opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.cm_cases_today	cases

GROUP BY 	case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM NEW cases History (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Created Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((cases.case_createddate::date),'YYYY-IW') 		AS date_part
			, MIN 	(cases.case_createddate::date)					AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Created Cases' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3			
			, CASE 	WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NULL 	THEN 'unknown'
					WHEN cases.grand_total IS NULL	AND cases.opportunityid IS NOT NULL THEN 'PPH'
					WHEN cases.grand_total < 250 										THEN '<250€'
		  			WHEN cases.grand_total >= 250 	AND cases.grand_total < 500 		THEN '250€-500€'
		 			WHEN cases.grand_total >= 500 	AND cases.grand_total < 1000 		THEN '500€-1000€'
		  																				ELSE '>1000€'		END AS sub_kpi_4
			, cases.opportunity						AS sub_kpi_5
			, cases.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, cases.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10			
			, COUNT(*)								AS value

FROM 		bi.cm_cases_created_yesterday	cases

GROUP BY 	cases.case_createddate
			, cases.case_origin
			, cases.case_status	
			, cases.case_reason
			, cases.grand_total
			, cases.opportunityid
			, cases.opportunity	
			, cases.case_owner
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Reopened Cases (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Reopened Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Reopened Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			
FROM 		salesforce.casehistory hi

INNER JOIN 	bi.cm_cases_basis 				ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2019-01-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Closed Cases (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Closed Cases';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((closed.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(closed.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, closed.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, closed.case_status						AS sub_kpi_2
			, closed.case_reason						AS sub_kpi_3			
			, CASE 	WHEN closed.grand_total IS NULL	AND closed.opportunityid IS NULL 		THEN 'unknown'
					WHEN closed.grand_total IS NULL	AND closed.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN closed.grand_total < 250 											THEN '<250€'
		  			WHEN closed.grand_total >= 250 	AND closed.grand_total < 500 			THEN '250€-500€'
		 			WHEN closed.grand_total >= 500 	AND closed.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, closed.opportunity						AS sub_kpi_5
			, closed.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, closed.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			
FROM 		salesforce.casehistory 			hi

INNER JOIN 	bi.cm_cases_basis 				ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- closed cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Closed'
			AND hi.createddate::date >= '2019-01-01'

			
) AS closed

GROUP BY 	closed.date
			, closed.case_origin
			, closed.case_status	
			, closed.case_reason
			, closed.grand_total
			, closed.opportunityid
			, closed.opportunity	
			, closed.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Type changes (count)
-- Created on: 23/11/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Cases Type changed';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR ((type_change.date::date),'YYYY-IW') 	AS date_part
			, MIN 	(type_change.date::date)				AS date
			, CAST 	('-' 			AS varchar) 			AS locale
			, type_change.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)				AS type
			, CAST	('Cases Type changed' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)				AS sub_kpi_1
			, type_change.case_status						AS sub_kpi_2
			, type_change.case_reason						AS sub_kpi_3			
			, CASE 	WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NULL 		THEN 'unknown'
					WHEN type_change.grand_total IS NULL	AND type_change.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN type_change.grand_total < 250 													THEN '<250€'
		  			WHEN type_change.grand_total >= 250 	AND type_change.grand_total < 500 			THEN '250€-500€'
		 			WHEN type_change.grand_total >= 500 	AND type_change.grand_total < 1000 			THEN '500€-1000€'
		  																								ELSE '>1000€'		END AS sub_kpi_4
			, type_change.opportunity						AS sub_kpi_5
			, type_change.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)				AS sub_kpi_7
			, type_change.opportunityid						AS sub_kpi_8
			, CAST 	('-' 			AS varchar)				AS sub_kpi_9
			, CAST 	('-' 			AS varchar)				AS sub_kpi_10			
			, COUNT(*)										AS value


FROM 		(


SELECT 		hi.createddate::date 							AS date
			, *
			
FROM salesforce.casehistory 			hi

LEFT JOIN 	bi.cm_cases_basis 			ca 		ON hi.caseid			= ca.case_ID			
		
	-- reopened cases
WHERE				hi.field 			= 			'Type'
			AND 	hi.newvalue			IN 			('CM B2B', 'CM B2C')
			AND		hi.oldvalue			NOT IN 		('B2B','KA')
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2019-01-01'
			AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
			AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
			
) AS type_change

GROUP BY 	type_change.date
			, type_change.case_origin
			, type_change.case_status	
			, type_change.case_reason
			, type_change.grand_total
			, type_change.opportunityid
			, type_change.opportunity	
			, type_change.case_owner			
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Inbound Calls (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Inbound Calls';
INSERT INTO 	bi.CM_cases

 SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 			AS varchar)							AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493030807264', '41435084849', '493030807403')
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Outbound Calls (count)
-- Created on: 07/11/2018

DELETE FROM 	bi.CM_cases			WHERE date = 'YESTERDAY' AND kpi = 'Outbound Calls';
INSERT INTO 	bi.CM_cases

SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 				AS varchar) 					AS locale
			, CAST 	('BAT CM'			AS varchar) 					AS origin 
			, CAST 	('B2B'				AS varchar)						AS type
			, CAST	('Outbound Calls'	AS varchar)						AS kpi
			, CAST 	('Count'			AS varchar)						AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.type__c
					WHEN n.account__c 				IS NOT NULL 	THEN a.type__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN 'lead' 
					ELSE 'unknown' END 									AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN co.name
					WHEN n.account__c 				IS NOT NULL 	THEN a.name
					WHEN n.lead__c 					IS NOT NULL 	THEN l.name
					ELSE 'unknown' END 									AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 				AS varchar)						AS sub_kpi_7 -- not used
			, CASE 	WHEN n.relatedcontact__c		IS NOT NULL 	THEN n.relatedcontact__c
					WHEN n.account__c 				IS NOT NULL 	THEN n.account__c 
					WHEN n.lead__c 					IS NOT NULL 	THEN n.lead__c 
					ELSE 'unknown' END 									AS sub_kpi_8
			, n.number_not_in_salesforce__c								AS sub_kpi_9
			, CAST 	('-' 				AS varchar)						AS sub_kpi_10 -- not used		
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Outbound'
			AND n.department__c LIKE 'CM'
			AND n.call_start_date_time__c::date = 'YESTERDAY'
			
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, co.type__c
			, a.type__c
			, co.name
			, a.name
			, l.name
			, u.name
			, n.number_not_in_salesforce__c
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Kpis Traffic Lights (count) 
-- Created on: 05/12/2018

DELETE FROM 	bi.CM_cases			WHERE kpi = 'Traffic Light';
INSERT INTO 	bi.CM_cases 

SELECT		
			TO_CHAR (traffic_lights.date,'YYYY-IW') AS date_part
			, MIN 	(traffic_lights.date::date) 	AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, CAST 	('-' 			AS varchar)		AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Traffic Light' 	AS varchar)	AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, traffic_lights.avg_traffic_light		AS sub_kpi_2
			, CASE 	WHEN traffic_lights.avg_traffic_light IS NULL						THEN 'red'
					WHEN traffic_lights.avg_traffic_light <= 2.4 						THEN 'red'
					WHEN traffic_lights.avg_traffic_light > 2.4 
						AND traffic_lights.avg_traffic_light <= 2.9 					THEN 'yellow'
					WHEN traffic_lights.avg_traffic_light >2.9							THEN 'green' 
																						ELSE '-' 			END AS sub_kpi_3																	
			, CASE 	WHEN traffic_lights.avg_traffic_light IS NULL						THEN 1
					WHEN traffic_lights.avg_traffic_light <= 2.4 						THEN 1
					WHEN traffic_lights.avg_traffic_light > 2.4 
						AND traffic_lights.avg_traffic_light <= 2.9 					THEN 2
					WHEN traffic_lights.avg_traffic_light >2.9							THEN 3 
																						ELSE 0 			END AS sub_kpi_4																					
			, CAST 	('-' 			AS varchar)		AS sub_kpi_5
			, CAST 	('-' 			AS varchar)		AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, CAST 	('-' 			AS varchar)		AS sub_kpi_8
			, CAST 	('-' 			AS varchar)		AS sub_kpi_9
			, CAST 	('-' 			AS varchar)		AS sub_kpi_10		
			, COUNT(*)								AS value

FROM 		bi.opportunity_traffic_light_new	traffic_lights

GROUP BY 	traffic_lights.date
			, traffic_lights.avg_traffic_light
;
--------------------------------------------
--------------------------------------------
--------------------------------------------

-- Author: Christina Janson
-- CM B2B Service Level SVL calculation: case history table when case closed after the 2018-01-01
-- Created on: 05/06/2019
DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis_closed;
CREATE 	TABLE 				bi.CM_cases_service_level_basis_closed 	AS 

SELECT 		cahi.caseid																					case_ID
			, cahi.createddate
			, cahi.createdbyid																			AS event_by
			
FROM 	 salesforce.casehistory		cahi	

WHERE 	cahi.field 				IN 	('created','Status') 
	AND cahi.newvalue 			IN 	('Closed')
	AND cahi.createddate::date 	>= 	'2019-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 

;

-- Author: Christina Janson
-- CM B2B Service Level SVL calculation
-- Created on: 11/01/2019
DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_cases_service_level_basis 	AS 

SELECT 		ca.case_id																					case_ID
			, ca.case_number																			case_number
			, ca.case_createddate																		date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate)	END date2_closed
			, CAST('New Case' as varchar) 																AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, cahi.event_by																			AS event_by
			
FROM 		bi.CM_cases_basis 		ca

-- date2													 														
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID 			= 	cahi.case_ID
 														AND cahi.createddate 	>= 	ca.case_createddate

WHERE 		ca.case_createddate::date 					>= 		'2019-01-01'			
--			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2 
		
GROUP BY 	ca.case_id						
			, ca.case_number					
			, ca.case_createddate	
			, cahi.event_by			

UNION ALL
 
SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Reopened Case' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END closed
			, COUNT(*)																					AS value
			, cahi.event_by																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_basis 		ca 				ON hi.caseid			= 	ca.case_ID	
																					
-- date2 														
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID 			= 	cahi.case_ID
 														AND cahi.createddate 	>= 	hi.createddate
WHERE		-- reopened cases
			hi.field 									= 		'Status'
			AND (hi.newvalue 							LIKE 	'Reopened'
				OR hi.newvalue							LIKE 	'In Progress'
				OR hi.newvalue							LIKE 	'New'
				OR hi.newvalue 							LIKE 	'Escalated')
			AND hi.oldvalue								LIKE 	'Closed'
			AND hi.createddate::date 					>= 		'2019-01-01'
--			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, cahi.event_by

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('Type Change' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, cahi.event_by																				AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_basis 			ca 				ON hi.caseid			= 	ca.case_ID	

-- date 2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi	ON ca.case_ID 			= 	cahi.case_ID
															AND cahi.createddate 	>= 	hi.createddate						
 					
WHERE		-- type change		
			hi.field 									= 		'Type'
			AND hi.newvalue								IN 		('CM B2B', 'CM B2C')
			AND	hi.oldvalue								NOT IN 	('B2B','KA')
			AND	hi.createddate::date 					<> 		ca.case_createddate::date
			AND hi.createddate::date 					>= 		'2019-01-01'
			AND ca.case_origin							NOT LIKE '%B2B customer%' 	
			AND ca.case_origin	 						NOT LIKE 'CM - Team'
				
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, cahi.event_by

UNION ALL

SELECT 		ca.case_ID																					case_ID
			, ca.case_number																			case_number
			, hi.createddate  																			date1_opened
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE ELSE MIN(cahi.createddate) 	END date2_closed
			, CAST('# Reopened' as varchar) 															AS type
			, CASE WHEN MIN(cahi.createddate) IS NULL THEN 'open' 		ELSE 'closed' 				END	closed
			, COUNT(*)																					AS value
			, hi.createdbyid																			AS event_by

FROM 		salesforce.casehistory 		hi
INNER JOIN 	bi.CM_cases_basis 		ca 				ON hi.caseid			= 	ca.case_ID	
							
-- date 2
LEFT JOIN 	salesforce.casehistory		cahi			ON 	ca.case_ID			= 	cahi.caseid 
														AND cahi.field 			IN 	('Status') 
														AND cahi.newvalue 		IN 	('Reopened')	
 														AND cahi.createddate 	<= 	hi.createddate

WHERE		-- count reopened cases in the past 
			hi.field 									= 		'Status'
			AND hi.newvalue 							LIKE 	'Reopened'
			AND hi.createddate::date 					>= 		'2019-01-01'
--			AND ca.case_number	 = '00530089'
			
GROUP BY 	ca.case_id
			, ca.case_number
			, hi.createddate
			, hi.createdbyid

ORDER BY 	case_id
			, date1_opened
			, date2_closed
			, event_by
;

-- Author: Christina Janson
-- CM B2B Case Service Level
-- Created on: 11/01/2019

DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level;
CREATE 	TABLE 				bi.CM_cases_service_level 	AS 

SELECT 	basis.*
-- ---------------------------------------------------------------------------------- difference: case opened / reopened / type change -> case closed		
-- -
-- - working h Mo-Fr 08:00:00 - 17:00:00 bi
-- - summertime timezone difference of 1 h

		, CASE 	WHEN basis.date1_opened::date 			IS NULL 							THEN 'rule 0.1' -- opened date missing
				WHEN basis.date2_closed::date 			IS NULL 							THEN 'rule 0.2' -- still open case
				WHEN basis.date1_opened::timestamp 	> basis.date2_closed::timestamp			THEN 'rule 1.0' -- open after closed
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN -- is same date
					 (CASE WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 2.1' 	 -- opened befor working day start
																							ELSE 'rule 2.2' END) -- 
				ELSE (CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 				THEN 'rule 3.1' -- opened after working day start
							WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') 	THEN 'rule 3.2' -- opened at the weekend
							WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 3.3' -- opened before working day start
																							ELSE 'rule 3.4' -- opened at a weekday within working hours
				END) END 																	AS rule_set 
				
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN 
		
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
					(CASE WHEN basis.date1_opened::time < TIME '07:00:00.0'					THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
						+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) END)
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
	-- minutes pased on the owner change date UNTIL END OF WORK
				-- owner changed after working day
						(CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 			THEN 0 
						-- owner change at the weekend
								WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') THEN 0
				ELSE
					-- owner changed before working day
						(CASE 	WHEN basis.date1_opened::time < TIME '07:00:00.0' 			THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60
								ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - basis.date1_opened::time) * 60 
										+ DATE_PART 	('minute', TIME '16:00:00.0' - basis.date1_opened::time) END) END)
					+
					-- minutes pased on the first contact date beginning at the morning working time
						(CASE 	WHEN basis.date2_closed::time < TIME '07:00:00.0' 			THEN 0 
								WHEN basis.date2_closed::time < TIME '16:00:00.0' 			THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60 END)
					+
					
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days and SUBTRACT 2 days 
	-- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) -2	) 			> 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END) END 																AS SVL_Minutes
			
-- ------------------------------------------------------------------------------------------------------------------------------------------------
-- - SVL Customer, dont care about working hours
-- -
				
-- if the case opened was after the case closed date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if case opened date and case closed date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					
				THEN 		DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
							+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) 
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
				
				-- date opened to EOD in minutes
				CASE WHEN date_part('dow', basis.date1_opened) IN ('6','0')	THEN 0 ELSE  
				(DATE_PART 	('hour', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened) * 60 
						+ DATE_PART 	('minute', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened)) END 
						
				-- + minutes on the case closed date		
				+	(DATE_PART 	('hour', basis.date2_closed) * 60  + DATE_PART 	('minute', basis.date2_closed))		
				+
					
	-- create a list of all date incl. case opened date and case closed date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) - 2)			> 0 
			
			-- in case the working days are > 0 -> workind days * 24 * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNT(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) - CASE WHEN date_part('dow', basis.date1_opened) IN ('6')	THEN 1 ELSE 2 END) * 24 * 60
			ELSE 0 END)  	END
																								AS SVL_Customer
			, cas.createddate				case_createddate
			, cas.isclosed					case_isclosed
			, cas.ownerid					case_ownerid
			, u.name						case_owner
			, cas.origin					case_origin
			, cas.type						case_type
			, cas.reason					case_reason
			, cas.status					case_status
			, cas.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, cas.order__c					orderid
			, o.type						order_type
			, cas.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, cas.opportunity__c			opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			, u2.name 						event_user
		
FROM bi.cm_cases_service_level_basis basis

LEFT JOIN 	salesforce.case 		cas 	ON basis.case_id		= cas.sfid
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 
LEFT JOIN 	salesforce.user 		u2 		ON basis.event_by 		= u2.sfid

-- WHERE basis.case_id = '5000J00001Q5PmtQAF'
-- LIMIT 100
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: weekly CM Team and Agent Performance (B2B)
-- Created on: 14/05/2019

DROP TABLE IF EXISTS bi.cm_performance_weekly;
CREATE TABLE bi.cm_performance_weekly AS 

SELECT *
		,CASE WHEN performance.inbound_calls > 0 		THEN  performance.inbound_calls >= (performance.target_inbound_calls * performance.FTE) END		AS performance_inbound_calls
		,CASE WHEN performance.cases_closed > 0 		THEN  performance.cases_closed >= (performance.target_cases_closed * performance.FTE) END		AS performance_cases_closed
		,CASE WHEN performance.svl_share_within_24h > 0 THEN  performance.svl_share_within_24h >= (performance.targert_svl * performance.FTE) END		AS performance_svl



FROM
(
SELECT 	
		 cases.date_part 				AS cw 
		, MIN(cases.date) 				AS date
		, cases.sub_kpi_6	 			AS agent

-- FTE of every CM agent to calculate the share of the targets		
		, CASE 	WHEN cases.sub_kpi_6 = 'Katharina Kühner' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Katharina Kuehner' 	THEN 1 
				WHEN cases.sub_kpi_6 = 'Sercan Tas'	 		THEN 1
				WHEN cases.sub_kpi_6 = 'André Bauß' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Carmen Haas' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Julian Schäfer'		THEN 1
				WHEN cases.sub_kpi_6 = 'Susanne Marino' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Danny Taszarek'		THEN 1
				WHEN cases.sub_kpi_6 = 'Felix Liedtke'		THEN 1
				WHEN cases.sub_kpi_6 = 'Vivien Greve' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Daniela Kaim'		THEN 1
				WHEN cases.sub_kpi_6 = 'Salim Abdoulaye'	THEN 1
				WHEN cases.sub_kpi_6 = 'Lennart Bär'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Karla Sorgato'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'CM Support2'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Marleen Dreyer'		THEN 0.5
															ELSE 0 END 	AS FTE
		
		, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.value ELSE 0 END)	AS inbound_calls
		, CAST 	('20' 	AS numeric)					AS target_inbound_calls

		, SUM(CASE WHEN cases.kpi = 'Closed Cases' 	THEN cases.value ELSE 0 END) 	AS cases_closed
		, CAST 	('90'	AS numeric)					AS target_cases_closed

		, svl_final.svl_share_within_24h 			AS svl_share_within_24h
		, svl_final.cases							AS svl_casesclosed
		, svl_final.svl_cases_within_24h			AS svl_cases_within_24h
		, CAST 	('0.95'	AS numeric)					AS targert_svl


FROM bi.cm_cases 		cases

LEFT JOIN 
		(
			SELECT 	
					cw
					, MIN(svl_details.opened_date) 				AS MinDate
					, agent
					, COUNT(*)									AS cases
					, SUM(svl_details.svl_within_24h) 			AS svl_cases_within_24h
					, ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4)	AS svl_share_within_24h
			
			FROM
					(
					SELECT 	TO_CHAR (svl.date1_opened, 'YYYY-IW')	AS cw
							, svl.date1_opened::date 				AS opened_date
							, svl.event_user						AS agent
							, svl.case_id 							AS case_id
							, svl.case_number						AS case_number
							, svl.type 								AS svl_type
							, svl.closed 							AS svl_has_closeddate
							, svl.case_isclosed						AS case_stil_closed
							, svl.case_type							AS case_type
							, svl.case_origin						AS case_origin
							, svl.case_reason						AS case_reason
							, svl.case_status						AS case_status	
							, svl.svl_customer						AS svl_customer
							
					-- IMPROVEMENT: maybe the following should be included in the SVL function!
							, CASE WHEN svl.svl_customer/60 <= 24 THEN 1 ELSE 0 END AS svl_within_24h
							
							, CASE WHEN svl.svl_customer = 0 		AND svl.type = 'Reopened Case' 					THEN 1 ELSE 0 END AS exclude_reopened_closed_due_mail 	-- exclude when 1
							, CASE WHEN svl.type = 'New Case' 		AND svl.case_origin = 'direct email outbound' 	THEN 1 ELSE 0 END AS exclude_direct_email_outbound	 	-- exclude when 1
							, CASE WHEN svl.type = 'Damage' 		OR svl.case_reason = '%Damage%'					THEN 1 ELSE 0 END AS exclude_damage_cases				-- exclude when 1
							, CASE WHEN svl.case_reason = 'Opportunity - Onboarding'								THEN 1 ELSE 0 END AS exclude_onboarding_cases			-- exclude when 1
							, CASE WHEN svl.case_status = 'Closed' AND svl.case_isclosed IS FALSE					THEN 1 ELSE 0 END AS exclude_created_within_clean_up	-- exclude when 1
					
					FROM 	bi.cm_cases_service_level 	svl
					WHERE 	NOT svl.type 	= '# Reopened'
							AND svl.date1_opened::date >= '2019-01-01'
					
					) AS svl_details
			
			 WHERE 
			 		svl_details.exclude_reopened_closed_due_mail 		= 0
					AND svl_details.exclude_direct_email_outbound 		= 0
					AND svl_details.exclude_damage_cases 				= 0
					AND svl_details.exclude_onboarding_cases 			= 0
					AND svl_details.exclude_created_within_clean_up 	= 0
			
			GROUP BY 	
						cw
						, agent
						
						
			) AS svl_final  ON (svl_final.cw = cases.date_part AND svl_final.agent = cases.sub_kpi_6)

WHERE cases.date 	>= '2019-01-01'

GROUP BY 	cases.date_part 				
			, cases.sub_kpi_6	
			, svl_final.svl_share_within_24h
			, svl_final.cases							
			, svl_final.svl_cases_within_24h
) AS performance
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: monthly CM Team and Agent Performance (B2B)
-- Created on: 11/06/2019

DROP TABLE IF EXISTS bi.cm_performance_monthly;
CREATE TABLE bi.cm_performance_monthly AS 

SELECT *
		,CASE WHEN performance.inbound_calls > 0 		THEN  performance.inbound_calls >= (performance.target_inbound_calls * performance.FTE) END		AS performance_inbound_calls
		,CASE WHEN performance.cases_closed > 0 		THEN  performance.cases_closed >= (performance.target_cases_closed * performance.FTE) END		AS performance_cases_closed
		,CASE WHEN performance.svl_share_within_24h > 0 THEN  performance.svl_share_within_24h >= (performance.targert_svl * performance.FTE) END		AS performance_svl



FROM
(
SELECT 	MIN(cases.date) 				AS date
		, cases.sub_kpi_6	 			AS agent

-- FTE of every CM agent to calculate the share of the targets		
		, CASE 	WHEN cases.sub_kpi_6 = 'Katharina Kühner' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Katharina Kuehner' 	THEN 1 
				WHEN cases.sub_kpi_6 = 'Sercan Tas'	 		THEN 1
				WHEN cases.sub_kpi_6 = 'André Bauß' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Carmen Haas' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Julian Schäfer'		THEN 1
				WHEN cases.sub_kpi_6 = 'Susanne Marino' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' 	THEN 1
				WHEN cases.sub_kpi_6 = 'Danny Taszarek'		THEN 1
				WHEN cases.sub_kpi_6 = 'Felix Liedtke'		THEN 1
				WHEN cases.sub_kpi_6 = 'Vivien Greve' 		THEN 1
				WHEN cases.sub_kpi_6 = 'Daniela Kaim'		THEN 1
				WHEN cases.sub_kpi_6 = 'Salim Abdoulaye'	THEN 1
				WHEN cases.sub_kpi_6 = 'Lennart Bär'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Karla Sorgato'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'CM Support2'		THEN 0.5
				WHEN cases.sub_kpi_6 = 'Marleen Dreyer'		THEN 0.5
															ELSE 0 END 	AS FTE
		
		, SUM(CASE WHEN cases.kpi = 'Inbound Calls' AND  cases.sub_kpi_2 = 'true' THEN cases.value ELSE 0 END)	AS inbound_calls
		, CAST 	('20' 	AS numeric)		/5 * 21		AS target_inbound_calls

		, SUM(CASE WHEN cases.kpi = 'Closed Cases' 	THEN cases.value ELSE 0 END) 	AS cases_closed
		, CAST 	('90'	AS numeric)		/5 * 21		AS target_cases_closed

		, svl_final.svl_share_within_24h 			AS svl_share_within_24h
		, svl_final.cases							AS svl_casesclosed
		, svl_final.svl_cases_within_24h			AS svl_cases_within_24h
		, CAST 	('0.95'	AS numeric)					AS targert_svl


FROM bi.cm_cases 		cases

LEFT JOIN 
		(
			SELECT 	svl_details.month 							AS month
					, MIN(svl_details.opened_date) 				AS MinDate
					, agent
					, COUNT(*)									AS cases
					, SUM(svl_details.svl_within_24h) 			AS svl_cases_within_24h
					, ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4)	AS svl_share_within_24h
			
			FROM
					(
					SELECT 
					--		*
							TO_CHAR(svl.date1_opened, 'YYYY-MM') 	AS month
							, svl.date1_opened::date 				AS opened_date
							, svl.event_user						AS agent
							, svl.case_id 							AS case_id
							, svl.case_number						AS case_number
							, svl.type 								AS svl_type
							, svl.closed 							AS svl_has_closeddate
							, svl.case_isclosed						AS case_stil_closed
							, svl.case_type							AS case_type
							, svl.case_origin						AS case_origin
							, svl.case_reason						AS case_reason
							, svl.case_status						AS case_status	
							, svl.svl_customer						AS svl_customer
							
					-- IMPROVEMENT: maybe the following should be included in the SVL function!
							, CASE WHEN svl.svl_customer/60 <= 48 THEN 1 ELSE 0 END AS svl_within_24h
							
							, CASE WHEN svl.svl_customer = 0 		AND svl.type = 'Reopened Case' 					THEN 1 ELSE 0 END AS exclude_reopened_closed_due_mail 	-- exclude when 1
							, CASE WHEN svl.type = 'New Case' 		AND svl.case_origin = 'direct email outbound' 	THEN 1 ELSE 0 END AS exclude_direct_email_outbound	 	-- exclude when 1
							, CASE WHEN svl.type = 'Damage' 		OR svl.case_reason = '%Damage%'					THEN 1 ELSE 0 END AS exclude_damage_cases				-- exclude when 1
							, CASE WHEN svl.case_reason = 'Opportunity - Onboarding'								THEN 1 ELSE 0 END AS exclude_onboarding_cases			-- exclude when 1
							, CASE WHEN svl.case_status = 'Closed' AND svl.case_isclosed IS FALSE					THEN 1 ELSE 0 END AS exclude_created_within_clean_up	-- exclude when 1
					
					FROM 	bi.cm_cases_service_level 	svl
					WHERE 	NOT svl.type 	= '# Reopened'
							AND svl.date1_opened::date >= '2019-01-01'
					
					) AS svl_details
			
			 WHERE 
			 		svl_details.exclude_reopened_closed_due_mail 		= 0
					AND svl_details.exclude_direct_email_outbound 		= 0
					AND svl_details.exclude_damage_cases 				= 0
					AND svl_details.exclude_onboarding_cases 			= 0
					AND svl_details.exclude_created_within_clean_up 	= 0
			
			GROUP BY 	
						month
						, agent
						
						
			) AS svl_final  ON (svl_final.month = TO_CHAR (cases.date,'YYYY-MM') AND svl_final.agent = cases.sub_kpi_6)

WHERE cases.date 	>= '2019-01-01'

GROUP BY 	cases.sub_kpi_6	
			, svl_final.svl_share_within_24h
			, svl_final.cases							
			, svl_final.svl_cases_within_24h
) AS performance