



SELECT

	h.caseid,
	h.createddate,
	h.oldvalue,
	h.newvalue,
	u.name
	


FROM

	salesforce.casehistory h
	
LEFT JOIN

	salesforce.user u
	
ON

	h.createdbyid = u.sfid
	
WHERE

	(h.newvalue = 'Closed'
	AND h.field = 'Status'
	AND h.createdbyid IN ('0050J000008oCO8QAM', '0050J000008hYcXQAU')
	)
	OR
	((h.oldvalue = 'Closed' OR h.oldvalue = 'New' OR h.oldvalue = 'Reopened')
	AND h.field = 'Status')
	AND h.createddate > '2019-06-30'
	
ORDER BY

	h.caseid,
	h.createddate asc
		