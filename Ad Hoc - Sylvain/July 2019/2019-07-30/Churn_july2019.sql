	
SELECT

	t1.*,
	t1.confirmed_end__c - t1.first_order as life

FROM		
	
	(SELECT
	
		o.sfid,
		o.name,
		pot.potential,
		o.delivery_area__c,
		o.churn_reason__c,
		o.ownerid,
		u.name as owner,
		prov.provider,
		prov.company_name,
		cont.confirmed_end__c,
		cont.resignation_date__c,
		MIN(oo.effectivedate) as first_order,
		COUNT(DISTINCT oo.sfid) as valid_orders
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c cont
		
	ON
	
		o.sfid = cont.opportunity__c
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	LEFT JOIN
	
		salesforce.user u
		
	ON
	
		o.ownerid = u.sfid
		
	LEFT JOIN
	
		bi.order_provider prov
		
	ON
	
		o.sfid = prov.opportunityid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp pot
		
	ON
	
		o.sfid = pot.opportunityid
		
	WHERE
	
		cont.service_type__c = 'maintenance cleaning'
		AND o.test__c IS FALSE
		AND oo.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND (
				(o.status__c IN ('RESIGNED', 'CANCELLED') AND cont.status__c IN ('RESIGNED', 'CANCELLED') AND TO_CHAR(cont.confirmed_end__c, 'YYYY-MM') = '2019-07')
				OR
				(o.status__c IN ('OFFBOARDING') AND cont.active__c IS TRUE AND TO_CHAR(cont.confirmed_end__c, 'YYYY-MM') = '2019-07')
				-- OR
				-- (o.status__c IN ('RETENTION') AND cont.active__c IS TRUE AND TO_CHAR(cont.confirmed_end__c, 'YYYY-MM') = '2019-07')
				)
				
	GROUP BY
	
		o.sfid,
		o.name,
		pot.potential,
		o.delivery_area__c,
		o.churn_reason__c,
		o.ownerid,
		u.name,
		prov.provider,
		prov.company_name,
		cont.confirmed_end__c,
		cont.resignation_date__c
		
	ORDER BY
	
		o.name,
		pot.potential,
		o.delivery_area__c,
		o.churn_reason__c,
		cont.confirmed_end__c) as t1