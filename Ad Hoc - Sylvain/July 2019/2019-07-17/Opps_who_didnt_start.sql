



SELECT

	
	o.sfid,
	o.closedate,
	SUM(CASE WHEN oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as valid_orders,
	SUM(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as invalid_orders,
	COUNT(DISTINCT oo.sfid) as orders

FROM

	salesforce.opportunity o
	
FULL JOIN

	salesforce.order oo
	
ON

	o.sfid = oo.opportunityid
	
WHERE

	o.test__c IS FALSE
	AND oo.test__c IS FALSE
	AND LEFT(o.locale__c, 2) = 'de'
	
GROUP BY

	o.sfid,
	o.closedate