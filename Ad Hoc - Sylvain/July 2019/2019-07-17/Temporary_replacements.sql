SELECT

	i.sfid as contract,
	i.opportunity__c,
	i.service_type__c,
	i.grand_total__c,
	i.start__c,
	i.end__c

FROM

	salesforce.contract__c i
	
WHERE

	i.service_type__c LIKE '%replacement%'
	AND i.test__c IS FALSE
	
GROUP BY

	i.sfid,
	i.opportunity__c,
	i.service_type__c,
	i.grand_total__c,
	i.start__c,
	i.end__c
