	
	
SELECT

	t1.*,
	cont.grand_total__c,
	pot.potential,
	t1.amount__c - cont.grand_total__c as deficit_grand_total,
	t1.amount__c - pot.potential as deficit_potential

FROM	
	
	(SELECT 
	
		i.sfid as invoice_id, 
		i.opportunity__c,
		i.issued__c,
		i.amount__c/1.19 as amount__c,
		jsonpack_unpack(json__c)#>>'{contract,Id}' as contract_id
		
	FROM 
	
		salesforce.invoice__c i
		
	WHERE
	
		i.opportunity__c IS NOT NULL
		AND i.type__c IS NULL
		AND i.test__c IS FALSE
		AND i.issued__c >= '2018-01-01'
		AND LEFT(i.locale__c, 2) = 'de') as t1
		
LEFT JOIN

	salesforce.contract__c cont
	
ON

	t1.contract_id = cont.sfid
	
LEFT JOIN

	bi.potential_revenue_per_opp pot
	
ON

	t1.opportunity__c = pot.opportunityid
	
WHERE

	(cont.active__c IS TRUE AND cont.service_type__c = 'maintenance cleaning')
	OR (cont.status__c IN ('RESIGNED', 'CANCELLED', 'CANCELLED MISTAKE') AND cont.service_type__c = 'maintenance cleaning')
	
