
-- Requested KPIs for all opportunities

-- # of orders cancelled by customers
-- # of orders cancelled by professionals
-- # of orders with no show professional
WITH orders AS (
SELECT
    opps.sfid,
    EXTRACT(YEAR FROM AGE(opps.closedate)) * 12 + EXTRACT(MONTH FROM AGE(opps.closedate)) AS month_active,
    SUM(CASE WHEN t1.status = 'CANCELLED CUSTOMER' THEN 1 ELSE 0 END) AS orders_cancelled_customer,
    SUM(CASE WHEN t1.status = 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) AS orders_cancelled_pro,
    SUM(CASE WHEN t1.status = 'NOSHOW PROFESSIONAL' THEN 1 ELSE 0 END) AS orders_noshow_pro
FROM
    salesforce.opportunity opps
LEFT JOIN(
    SELECT
        o.opportunityid,
        o.status
    FROM
        salesforce.ORDER o
    WHERE
        o.test__c IS FALSE
        AND o.opportunityid IS NOT NULL
    GROUP BY
        o.opportunityid,
        o.status) AS t1 ON
    opps.sfid = t1.opportunityid
WHERE
    opps.test__c IS FALSE
    AND opps.status__c NOT IN ('RESIGNED',
    'CANCELLED')
    AND opps.test__c IS FALSE
    AND opps.stagename IN ('WON',
    'PENDING')
GROUP BY
    opps.sfid,
    EXTRACT(YEAR FROM AGE(opps.closedate)) * 12 + EXTRACT(MONTH FROM AGE(opps.closedate))),
    
-- # of cleaners
cleaners AS (

SELECT
    o.opportunityid,
    COUNT(DISTINCT o.professional__c) AS number_cleaners
FROM
    salesforce.order o
WHERE
    o.test__c IS FALSE
    AND o.status IN ('INVOICED',
    'PENDING TO START',
    'FULFILLED')
GROUP BY
    o.opportunityid),  


-- # of owners of the opportunities    
owners AS (

SELECT
    o.opportunityid,
    CASE WHEN COUNT(o.newvalue) > 2 THEN COUNT(newvalue) - 1 ELSE COUNT(newvalue) END number_owners
    --   COUNT(o.newvalue) AS number_owners

        FROM salesforce.opportunityfieldhistory o
    WHERE
        o.field = 'Owner'
    GROUP BY
        o.opportunityid),
        
-- # of cases with origin from customer 
-- # of feedback surveys responded
-- avg feedback score
cases AS(

SELECT
    c.opportunity__c,
    SUM(CASE WHEN c.origin LIKE '%B2B customer%' OR c.reason LIKE '%B2B customer%' OR c.reason LIKE 'Customer%' THEN 1 ELSE 0 END) AS cases_origin_customers,
    SUM(CASE WHEN c.subject LIKE 'Satisfaction Feedback:%' THEN 1 ELSE 0 END) AS feedbacks,
    SUM(CASE WHEN c.subject LIKE 'Satisfaction Feedback: 1' THEN 1 WHEN c.subject LIKE 'Satisfaction Feedback: 2' THEN 2 WHEN c.subject LIKE 'Satisfaction Feedback: 3' THEN 3 WHEN c.subject LIKE 'Satisfaction Feedback: 4' THEN 4 WHEN c.subject LIKE 'Satisfaction Feedback: 5' THEN 5 ELSE 0 END) / SUM(CASE WHEN c.subject LIKE 'Satisfaction Feedback:%' THEN 1 ELSE NULL END)::DECIMAL AS avg_feedback
FROM
    salesforce.case c
LEFT JOIN salesforce.opportunity o ON
    c.opportunity__c = o.sfid
WHERE
    o.stagename = 'WON'
    AND o.status__c NOT IN ('RESIGNED',
    'CANCELLED')
    AND o.test__c IS FALSE
GROUP BY
    c.opportunity__c),

-- sum of outstanding debt (not paid amount)
-- # of invoiced service types except maintenance cleaning
-- # of invoices corrected
invoices AS(

SELECT
    inv.opportunity__c,
    SUM(inv.balance__c) AS outstanding_debt,
    COUNT(CASE WHEN inv.service_type__c != 'maintenance cleaning' THEN 1 ELSE NULL END) AS number_services_exc_mcleaning,
    COUNT(CASE WHEN inv.type__c = 'correction' THEN 1 ELSE NULL END) AS invoices_corrected
FROM
    salesforce.invoice__c inv
--  LEFT JOIN salesforce.opportunity opps ON
--  opps.sfid = inv.opportunity__c
WHERE
 -- opps.test__c IS FALSE
    inv.test__c IS FALSE
    AND inv.opportunity__c IS NOT NULL
GROUP BY
    inv.opportunity__c),

    
-- sum of last 3-month invoice amount
invoice_last3 AS (

SELECT
    inv.opportunity__c,
    SUM(CASE WHEN inv.amount__c IS NULL THEN opps.plan_pph__c * inv.total_invoiced_hours_count__c ELSE inv.amount__c END)::DECIMAL AS invoiced_amount
FROM
    salesforce.opportunity opps
LEFT JOIN salesforce.invoice__c inv ON
    opps.sfid = inv.opportunity__c
WHERE
    opps.test__c IS FALSE
    AND inv.test__c IS FALSE
    AND inv.issued__c::TIMESTAMP >= DATE_TRUNC('month', CURRENT_DATE - INTERVAL '3 month')
    AND inv.issued__c::TIMESTAMP < DATE_TRUNC('month', CURRENT_DATE)
GROUP BY
    inv.opportunity__c),


-- # of supply items shipped per month
-- last shipment month of supply items
supply AS(
SELECT
    p.opportunity__c,
    SUM(quantity__c) AS number_items,
    t1.last_shipment
FROM
    (
    SELECT
        p.opportunity__c,
        TO_CHAR(MAX(p.servicedate__c), 'YYYY-MM') AS last_shipment
    FROM
        salesforce.productlineitem__c p
    WHERE
        p.test__c IS FALSE
        AND p.opportunity__c IS NOT NULL
    GROUP BY
        p.opportunity__c ) t1
LEFT JOIN salesforce.productlineitem__c p ON
    p.opportunity__c = t1.opportunity__c
WHERE
    p.test__c IS FALSE
GROUP BY
    t1.last_shipment,
    p.opportunity__c),

-- number of visits 
visits AS
(
SELECT
    e.whatid AS opportunity,
    COUNT(DISTINCT e.sfid) AS number_visits
FROM
    salesforce.event e
WHERE
    e.whatid LIKE '006%'
    AND e.event_reason__c LIKE 'Visit%'
GROUP BY
    e.whatid)
    
SELECT
    opps.sfid,
    opps.name,
    opps.status__c AS status,
    opps.closedate,
    orders.month_active,
    opps.traffic_light__c AS traffic_light,
    cleaners.number_cleaners,
    cases.cases_origin_customers,
    cases.feedbacks,
    cases.avg_feedback,
    CASE WHEN owners.number_owners IS NULL THEN 1 ELSE owners.number_owners END AS number_owners,
    invoices.outstanding_debt,
    invoices.number_services_exc_mcleaning,
    invoices.invoices_corrected,
    invoice_last3.invoiced_amount,
    orders.orders_cancelled_customer,
    CASE
        WHEN orders.month_active = 0 THEN orders_cancelled_customer
        ELSE ROUND((orders_cancelled_customer / orders.month_active)::NUMERIC, 2) END AS orders_cancelled_customer_per_month,
        orders.orders_cancelled_pro,
        CASE
            WHEN orders.month_active = 0 THEN orders.orders_cancelled_pro
            ELSE ROUND((orders.orders_cancelled_pro / orders.month_active)::NUMERIC, 2) END AS can_pro_per_month,
            orders.orders_noshow_pro,
            CASE
                WHEN orders.month_active = 0 THEN orders.orders_noshow_pro
                ELSE ROUND((orders_noshow_pro / orders.month_active)::NUMERIC, 2) END AS orders_noshow_pro_per_month,
                supply.number_items,
                supply.last_shipment,
                CASE
                    WHEN orders.month_active = 0 THEN supply.number_items
                    ELSE ROUND(supply.number_items::NUMERIC / orders.month_active::NUMERIC, 2) END AS shipped_items_per_month,
                    visits.number_visits
                FROM
                    salesforce.opportunity opps
                LEFT JOIN orders ON 
                    opps.sfid = orders.sfid
                LEFT JOIN cleaners ON
                    opps.sfid = cleaners.opportunityid
                LEFT JOIN owners ON
                    opps.sfid = owners.opportunityid
                LEFT JOIN cases ON
                    opps.sfid = cases.opportunity__c
                LEFT JOIN invoices ON
                    opps.sfid = invoices.opportunity__c
                LEFT JOIN invoice_last3 ON
                    opps.sfid = invoice_last3.opportunity__c
                LEFT JOIN supply ON
                    opps.sfid = supply.opportunity__c
                LEFT JOIN visits ON
                    opps.sfid = visits.opportunity
                WHERE
                opps.status__c NOT IN ('RESIGNED',
                'CANCELLED')
                AND opps.test__c IS FALSE
                AND opps.stagename IN ('WON',
                'PENDING')
                AND (opps.sfid = '0060J00000lc6XSQAY' OR opps.sfid = '0060J00000lcAQMQA2')
                
                
                GROUP BY
                    opps.sfid,
                    opps.name,
                    opps.status__c,
                    opps.closedate,
                    orders.month_active,
                    cleaners.number_cleaners,
                    supply.number_items,
                    supply.last_shipment,
                    opps.traffic_light__c,
                    cases.cases_origin_customers,
                    cases.feedbacks,
                    cases.avg_feedback,
                    owners.number_owners,
                    invoices.outstanding_debt,
                    invoices.number_services_exc_mcleaning,
                    invoices.invoices_corrected,
                    invoice_last3.invoiced_amount,
                    orders.orders_cancelled_customer,
                    orders.orders_cancelled_pro,
                    orders.orders_noshow_pro,
                    visits.number_visits
                
                
                
 