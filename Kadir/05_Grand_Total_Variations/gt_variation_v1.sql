
-- Grand total variations month over month

SELECT  
    t3.invoice_issued_date,
    t3.year_month,
    ROUND(t3.grand_total_prev_month::NUMERIC, 2) AS grand_total_prev_month,
    ROUND(t3.grand_total_variation::NUMERIC, 2) AS grand_total_variation,
    ROUND((t3.grand_total_cont - t3.grand_total_prev_month)::NUMERIC / NULLIF((t3.grand_total_prev_month), 0)::NUMERIC, 2) AS percent_change
FROM 
(
SELECT 
    t2.invoice_issued_date,
    TO_CHAR(t2.invoice_issued_date, 'YYYY-MM') AS year_month,
    ROUND(t2.grand_total_cont::NUMERIC, 2) AS grand_total_cont,
    COALESCE(LAG(t2.grand_total_cont, 1) OVER (ORDER BY t2.invoice_issued_date), 0) AS grand_total_prev_month, 
    COALESCE(t2.grand_total_cont - LAG(t2.grand_total_cont, 1) OVER (ORDER BY t2.invoice_issued_date), 0) as grand_total_variation
FROM

(
SELECT 
    t1.issued__c AS invoice_issued_date,
    SUM(t1.grand_total_contract) AS grand_total_cont
FROM
(
SELECT
    i.sfid,
    i.name,
    i.service_type__c,
    i.amount__c,
    i.balance__c,
    i.amount_paid__c,
    i.opportunity__c,
    o.name AS opp_name,
    o.delivery_area__c,
    LEFT(o.locale__c, 2) AS country,
    o.locale__c,
    o.stagename,
    o.status__c,
    o.grand_total__c AS opp_grand_total,
    o.plan_pph__c,
    i.issued__c,
    i.contract__c,
    cont.status__c,
    cont.service_type__c,
    cont.grand_total__c AS grand_total_contract
FROM
    salesforce.invoice__c i
LEFT JOIN salesforce.opportunity o ON
    i.opportunity__c = o.sfid
LEFT JOIN salesforce.contract__c cont ON
    i.contract__c = cont.sfid
WHERE
    i.opportunity__c IS NOT NULL
    AND i.test__c IS FALSE
    AND i.issued__c >= '2019-01-01'
    AND i.service_type__c = 'maintenance cleaning'
    AND LEFT(o.locale__c, 2) = 'de'
    AND i.original_invoice__c IS NULL
    AND o.test__c IS FALSE
    AND cont.test__c IS FALSE
    AND cont.service_type__c = 'maintenance cleaning') AS t1
    
GROUP BY 
    t1.issued__c) AS t2) AS t3
    
GROUP BY 
    t3.invoice_issued_date,
    t3.year_month,
    t3.grand_total_prev_month,
    t3.grand_total_variation,
    t3.grand_total_cont
    
    
   
    