
-- DROP TABLE IF EXISTS bi.test_kadir;

-- CREATE TABLE bi.test_kadir AS

-- Example of the revenue loss for the current date

SELECT
    CURRENT_DATE AS today,
    TO_CHAR(CURRENT_DATE, 'YYYY-MM') AS year_month,
    COUNT(CASE WHEN opps.status__c = 'OFFBOARDING' THEN opps.sfid ELSE NULL END) AS no_opps_offboarding,
    COUNT(CASE WHEN opps.status__c = 'RETENTION' THEN opps.sfid ELSE NULL END) AS no_opps_retention,
    COUNT(CASE WHEN opps.status__c = 'RUNNING' THEN opps.sfid ELSE NULL END) AS no_opps_running,
    SUM(CASE WHEN opps.status__c = 'RETENTION' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * t1.invoiced_hours ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS retention_revenue,
    SUM(CASE WHEN opps.status__c = 'OFFBOARDING' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * t1.invoiced_hours ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS offboarding_revenue,
    SUM(CASE WHEN opps.status__c = 'RUNNING' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * t1.invoiced_hours ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS running_revenue
FROM
    salesforce.opportunity opps
LEFT JOIN( -- Invoiced hours last month for each opportunities 
    SELECT
        TO_CHAR(effectivedate, 'YYYY-MM') AS year_month,
        SUM(o.Order_Duration__c)::DECIMAL AS invoiced_hours,
        o.opportunityid AS opportunities
           
    FROM
        salesforce.order o

    WHERE
        o.status IN ('INVOICED',
        'PENDING TO START',
        'CANCELLED CUSTOMER',
        'FULFILLED')
        AND o.type = 'cleaning-b2b'
        AND o.professional__c IS NOT NULL
        AND o.test__c IS FALSE
        AND LEFT(o.locale__C, 2) = 'de'
        AND o.effectivedate::TIMESTAMP >= DATE_TRUNC('month', CURRENT_DATE - INTERVAL '1 month')
        AND o.effectivedate::TIMESTAMP < DATE_TRUNC('month', CURRENT_DATE)

    GROUP BY
        year_month,
        o.opportunityid) AS t1 ON
    t1.opportunities = opps.sfid
    -- AND t1.year_month = TO_CHAR(CURRENT_DATE, 'YYYY-MM')
WHERE
    opps.test__c IS FALSE
    AND LEFT(opps.locale__c, 2) = 'de'

GROUP BY
    TO_CHAR(CURRENT_DATE, 'YYYY-MM')
        
