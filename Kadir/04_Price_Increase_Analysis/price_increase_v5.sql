
-- Active maintenance cleaning contracts started before 2019-12-01 based on first invoiced order date

SELECT 
    t2.year_month,
    t2.effectivedate,
    t2.opportunity__c,
    t2.opp_name,
    t2.contract_name,
    t2.owner_name,
    t2.city,
    t2.shipping_city,
    t2.region,
    t2.subscription_model,
    t2.supply_package,
    t2.month_active,
    t2.hours_weekly__c,
    t2.recurrence,
    t2.grand_total__c,
    t2.pph__c AS pph_amount,
    t2.total_invoiced_amount,
    t2.total_invoiced_hours,
    ROUND(t2.avg_monthly_revenue_flatfee_subscription::NUMERIC, 2) AS avg_monthly_revenue_flatfee_subscription,
    ROUND(t2.avg_monthly_revenue_pph_subscription::NUMERIC, 2) AS avg_monthly_revenue_pph_subscription,
    t2.pph_effective,
    CASE WHEN t2.hours_weekly__c > 0 AND t2.subscription_model = 'flat_fee_subscription' AND t2.recurrence = 'weekly_recurrence' 
        THEN ROUND((t2.grand_total__c::NUMERIC / (t2.hours_weekly__c * 4.33)::NUMERIC), 2) END AS pph_contractual_weekly_rec,
    CASE WHEN t2.hours_weekly__c > 0 AND t2.subscription_model = 'flat_fee_subscription' AND t2.recurrence = 'biweekly_recurrence' 
        THEN ROUND((t2.grand_total__c::NUMERIC / (t2.hours_weekly__c * 2.16)::NUMERIC), 2) END AS pph_contractual_biweekly_rec,
        
    CASE WHEN t2.supply_package = 'Standard' AND t2.recurrence = 'weekly_recurrence' AND t2.subscription_model = 'flat_fee_subscription'
        THEN  ROUND((t2.grand_total__c - (t2.grand_total__c * 0.05))::NUMERIC / (t2.hours_weekly__c * 4.33)::NUMERIC , 2) END AS pph_contractual_weekly_rec_standard_MC,
    CASE WHEN t2.supply_package = 'Standard' AND t2.recurrence = 'biweekly_recurrence'
        THEN ROUND((t2.grand_total__c - (t2.grand_total__c * 0.05))::NUMERIC / (t2.hours_weekly__c * 2.16)::NUMERIC) END AS pph_contractual_biweekly_rec_standard_MC,
    CASE WHEN t2.supply_package = 'Standard' AND t2.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t2.total_invoiced_amount - (t2.total_invoiced_amount * 0.05))::NUMERIC / (t2.total_invoiced_hours), 2) END AS pph_effective_standard_MC,
        
    CASE WHEN t2.supply_package = 'Premium' AND t2.recurrence = 'weekly_recurrence' AND t2.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t2.grand_total__c - (t2.grand_total__c * 0.10))::NUMERIC  / (t2.hours_weekly__c * 4.33)::NUMERIC, 2) END AS pph_contractual_weekly_rec_premium_MC,
    CASE WHEN t2.supply_package = 'Premium' AND t2.recurrence = 'biweekly_recurrence'
        THEN ROUND((t2.grand_total__c - (t2.grand_total__c * 0.10))::NUMERIC / (t2.hours_weekly__c * 2.16)::NUMERIC, 2) END AS pph_contractual_biweekly_rec_premium_MC,        
    CASE WHEN t2.supply_package = 'Premium' AND t2.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t2.total_invoiced_amount - (t2.total_invoiced_amount * 0.10))::NUMERIC / (t2.total_invoiced_hours), 2) END AS pph_effective_premium_MC,
        
    CASE WHEN t2.supply_package = 'Economy' AND t2.recurrence = 'weekly_recurrence' AND t2.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t2.grand_total__c)::NUMERIC / (t2.hours_weekly__c * 4.33)::NUMERIC, 2) END AS pph_contractual_weekly_economy,
    CASE WHEN t2.supply_package = 'Economy' AND t2.recurrence = 'biweekly_recurrence' AND t2.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t2.grand_total__c)::NUMERIC / (t2.hours_weekly__c * 2.16)::NUMERIC, 2) END AS pph_contractual_biweekly_economy,    
    CASE WHEN t2.supply_package = 'Economy' AND t2.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t2.total_invoiced_amount)::NUMERIC / (t2.total_invoiced_hours)::NUMERIC, 2) END AS pph_effective_economy


FROM
(
SELECT 
    t1.year_month,
    t1.effectivedate,
    t1.opportunity__c,
    t1.opp_name,
    t1.contract_name,
    t1.owner_name,
    INITCAP(t1.city) AS city,
    t1.shipping_city,
    CASE WHEN t1.city IN ('dresden', 'erfurt', 'leipzig', 'potsdam', 'rostock') THEN 'East'
    WHEN t1.city IN ('berlin', 'bielefeld', 'bochum', 'bonn', 'bremen', 'cologne',
        'darmstadt', 'dortmund', 'duisburg','dusseldorf', 'essen', 'frankfurt', 'hamburg',
        'hannover', 'kiel', 'lubeck', 'luneburg', 'mainz', 'manheim', 'munich', 'nuremberg',
        'stuttgart', 'wuppertal') THEN 'West'
        ELSE 'Other' END AS region,
    t1.subscription_model,
    t1.supply_package,
    t1.month_active,
    t1.hours_weekly__c,
    t1.monthly_hours::DECIMAL,
    CASE WHEN t1.adjusted_recurrence = '7' THEN 'weekly_recurrence'
        WHEN t1.adjusted_recurrence = '14' THEN 'biweekly_recurrence'
        ELSE 'others'
        END AS recurrence,
    t1.grand_total__c,
    t1.pph__c,
    t1.total_invoiced_amount::DECIMAL,
    t1.total_invoiced_hours::DECIMAL,
    CASE WHEN t1.subscription_model = 'flat_fee_subscription' AND t1.month_active = 0 THEN t1.total_invoiced_amount
        WHEN t1.subscription_model = 'flat_fee_subscription' AND t1.month_active > 0 
            THEN SUM(t1.total_invoiced_amount) / t1.month_active END AS avg_monthly_revenue_flatfee_subscription,
    CASE WHEN t1.subscription_model = 'pph_subscription' AND t1.month_active = 0 THEN t1.total_invoiced_amount
        WHEN t1.subscription_model = 'pph_subscription' AND t1.month_active > 0 
            THEN t1.total_invoiced_amount / t1.month_active END AS avg_monthly_revenue_pph_subscription,
    CASE WHEN t1.subscription_model = 'flat_fee_subscription'
        THEN ROUND((t1.total_invoiced_amount::NUMERIC / (t1.total_invoiced_hours)::NUMERIC), 2) END AS pph_effective        
        
FROM 
(
SELECT
    TO_CHAR(t3.effectivedate, 'YYYY-MM') AS year_month,
    t3.effectivedate,
    c.opportunity__c,
    c.name AS contract_name,
    opps.name AS opp_name,
    u.name AS owner_name,
    LEFT(opps.locale__c, 2) AS country,
    SPLIT_PART(opps.delivery_area__c, '-' , 2) AS city,
    opps.shippingaddress_city__c AS shipping_city,    
    CASE WHEN c.pph__c IS NOT NULL AND c.pph__c != 0 THEN 'pph_subscription' ELSE 'flat_fee_subscription' END AS subscription_model,
    EXTRACT(YEAR FROM AGE(t3.effectivedate)) * 12 + EXTRACT(MONTH FROM AGE(t3.effectivedate)) AS month_active,
    c.grand_total__c,
    SUM(inv.amount__c) AS total_invoiced_amount,
    SUM(inv.total_invoiced_hours_count__c) AS total_invoiced_hours,
    t3.invoiced_hours,
    opps.hours_weekly__c,
    opps.hours_weekly__c * 4.33 AS monthly_hours,
    CASE WHEN opps.recurrency__c IS NULL THEN 
        (CASE WHEN opps.recurrent1_recurrency__c IS NULL OR opps.recurrent1_recurrency__c = 0 
            THEN opps.recurrent2_recurrency__c::TEXT ELSE opps.recurrent1_recurrency__c::TEXT END) ELSE opps.recurrency__c
            END AS adjusted_recurrence,
    opps.recurrency__c,
    opps.recurrent1_recurrency__c::text,
    opps.recurrent2_recurrency__c::text,
    CASE WHEN opps.supplies__c ILIKE 'Standard%' OR opps.supplies__c SIMILAR TO 'SATNDARD%|STADNARD%|stabdard%|Standart%|Staandard%|standarad%' 
    OR opps.supplies__c ILIKE 'Paying for standard but No Supplies Needed at first' THEN 'Standard'
        WHEN opps.supplies__c ILIKE 'Premium%' THEN 'Premium'
        WHEN opps.supplies__c ILIKE 'Economy%' THEN 'Economy' END supply_package,
    c.pph__c,
    opps.plan_pph__c,
    opps.supplies__c
    
FROM
    salesforce.opportunity opps
LEFT JOIN salesforce.contract__c c ON
    c.opportunity__c = opps.sfid
LEFT JOIN salesforce.invoice__c inv ON
    opps.sfid = inv.opportunity__c
LEFT JOIN salesforce.USER u ON 
    opps.ownerid = u.sfid
    
LEFT JOIN (
SELECT 
    o.opportunityid,
    MIN(o.effectivedate) AS effectivedate,
    SUM(o.order_duration__c) AS invoiced_hours
FROM
    salesforce.ORDER o
WHERE
    o.status IN ('INVOICED')
    AND o.type = 'cleaning-b2b'
    AND o.test__c IS FALSE
    AND LEFT(o.locale__C, 2) = 'de'
    AND o.effectivedate < '2019-12-01'
GROUP BY
    o.opportunityid) AS t3 ON
t3.opportunityid = opps.sfid

WHERE
    c.test__c IS FALSE
    AND opps.status__c NOT IN ('OFFBOARDING', 'RETENTION', 'CANCELLED', 'RESIGNED')
    AND c.active__c IS TRUE 
    AND c.status__c IN ('ACCEPTED', 'SIGNED')
    AND t3.effectivedate < '2019-12-01'
    AND c.service_type__c = 'maintenance cleaning'
    AND LEFT(opps.locale__c, 2) = 'de'
    AND inv.service_type__c = 'maintenance cleaning'
    AND opps.test__c IS FALSE
    AND inv.test__c IS FALSE
    AND inv.original_invoice__c IS NULL
    AND inv.parent_invoice__c IS NULL
    AND opps.supplies__c NOT ILIKE 'special%' AND opps.supplies__c NOT ILIKE 'temporary replacement%'
 --   AND opps.recurrency__c <> opps.recurrent1_recurrency__c::text

GROUP BY 
    t3.effectivedate,
    TO_CHAR(t3.effectivedate, 'YYYY-MM'),
    c.opportunity__c,
    c.name,
    u.name,
    c.grand_total__c,
    opps.name,
    c.effective_start__c,
    EXTRACT(YEAR FROM AGE(t3.effectivedate)) * 12 + EXTRACT(MONTH FROM AGE(t3.effectivedate)),
    c.pph__c,
    opps.plan_pph__c,
    c.hours_weekly__c,
    opps.hours_weekly__c,
    opps.supplies__c,
    LEFT(opps.locale__c, 2),
    SPLIT_PART(opps.delivery_area__c, '-' , 2),
    opps.recurrency__c,
    opps.recurrent1_recurrency__c,
    opps.recurrent2_recurrency__c,
    opps.shippingaddress_city__c,
    t3.invoiced_hours
ORDER BY 
    t3.effectivedate DESC) AS t1
    
GROUP BY 
    t1.year_month,
    t1.effectivedate,
    t1.opportunity__c,
    t1.opp_name,
    t1.owner_name,
    t1.shipping_city,
    t1.city,
    t1.subscription_model,
    t1.month_active,
    t1.monthly_hours,
    t1.adjusted_recurrence,
    t1.hours_weekly__c,
    t1.grand_total__c,
    t1.pph__c,
    t1.total_invoiced_amount,
    t1.supply_package,
    t1.contract_name,
    t1.total_invoiced_hours,
    t1.hours_weekly__c

ORDER BY 
    t1.effectivedate DESC) AS t2
    
GROUP BY 
    t2.year_month,
    t2.effectivedate,
    t2.opportunity__c,
    t2.opp_name,
    t2.contract_name,
    t2.owner_name,
    t2.city,
    t2.shipping_city,
    t2.region,
    t2.subscription_model,
    t2.supply_package,
    t2.month_active,
    t2.monthly_hours,
    t2.recurrence,
    t2.grand_total__c,
    t2.pph__c,
    t2.total_invoiced_amount,
    t2.total_invoiced_hours,
    t2.avg_monthly_revenue_flatfee_subscription,
    t2.avg_monthly_revenue_pph_subscription,
    t2.pph_effective,
    t2.hours_weekly__c

ORDER BY 
    t2.effectivedate DESC 

    