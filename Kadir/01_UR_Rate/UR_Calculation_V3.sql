
-- UR% Calculation

-- Working hours, worked hours and capped work hours for each cleaner by month
WITH t1 AS (
SELECT
    t1.*,
    CASE
        WHEN SUM(t1.working_hours) > 0 THEN SUM(t1.capped_work_hours) / SUM(t1.working_hours)
        ELSE 0
    END AS utilization
FROM
    (
    SELECT
        TO_CHAR(o.effectivedate, 'YYYY-MM') AS year_month,
        o.professional__c AS prof,
        SUM(o.order_duration__c) AS worked_hours,
        (a.hr_contract_weekly_hours_min__c * 4.3) AS working_hours,
        CASE
            WHEN SUM(o.order_duration__c) >= (a.hr_contract_weekly_hours_min__c*4.3) THEN (a.hr_contract_weekly_hours_min__c*4.3)
            ELSE SUM(o.order_duration__c)
        END AS capped_work_hours
    FROM
        salesforce.account a
    LEFT JOIN salesforce.order o ON
        a.sfid = o.professional__c
    WHERE
        a.hr_contract_weekly_hours_min__c IS NOT NULL
        AND a.company_name__c ILIKE '%BAT%'
        AND LEFT(o.delivery_Area__c, 2) = 'de'
        AND effectivedate >= '2019-01-01'
        AND o.test__c = '0'
        AND a.test__c = '0'
        AND o.status IN( 'INVOICED',
        'PENDING TO START',
        'FULFILLED')
    GROUP BY
        year_month,
        o.professional__c,
        a.hr_contract_weekly_hours_min__c) AS t1
GROUP BY
    t1.year_month,
    t1.prof,
    t1.working_hours,
    t1.worked_hours,
    t1.capped_work_hours),

-- Sick days, holidays and absence hours for each cleaner by month
t2 AS(
SELECT
    TO_CHAR(hr.createddate, 'YYYY-MM') AS year_month,
    hr.account__c AS prof,
    SUM(CASE WHEN hr.type__c IN ('sickness') THEN hr.days__c ELSE 0 END) AS sick_days,
    SUM(CASE WHEN hr.type__c IN ('holidays') THEN hr.days__c ELSE 0 END) AS holidays,
    MAX(a.hr_contract_weekly_hours_min__c) AS weekly_working_hours,
    CASE
        WHEN ((MAX(a.hr_contract_weekly_hours_min__c) / 5) * (SUM(CASE WHEN hr.type__c IN ('sickness') THEN hr.days__c ELSE 0 END) + SUM(CASE WHEN hr.type__c IN ('holidays') THEN hr.days__c ELSE 0 END))) IS NULL THEN 0
        ELSE ((MAX(a.hr_contract_weekly_hours_min__c) / 5) * (SUM(CASE WHEN hr.type__c IN ('sickness') THEN hr.days__c ELSE 0 END) + SUM(CASE WHEN hr.type__c IN ('holidays') THEN hr.days__c ELSE 0 END)))
    END AS absence_hours
FROM
    salesforce.account a
LEFT JOIN salesforce.hr__c AS hr ON
    hr.account__c = a.sfid
WHERE
    description__c NOT ILIKE '%TEST%'
    AND hr.createddate >= '2019-01-01'
GROUP BY
    prof,
    year_month,
    a.hr_contract_weekly_hours_min__c
ORDER BY
    year_month)

-- UR, total working hours, total worked hours, total available hours, total absence hours and number of active cleaners by month
SELECT
    t1.year_month,
    CASE
        WHEN SUM(t1.working_hours) - SUM(t2.absence_hours) > 0 THEN SUM(t1.capped_work_hours) / (SUM(t1.working_hours) - SUM(t2.absence_hours))
        ELSE 0
    END AS ur,
    SUM(t1.working_hours)::DECIMAL AS total_working_hours,
    SUM(t1.worked_hours)::DECIMAL AS total_worked_hours,
    SUM(t1.working_hours) - SUM(t2.absence_hours)::DECIMAL AS total_available_hours,
    SUM(t2.absence_hours) AS total_absence_hours,
    COUNT(t1.prof) AS active_cleaners
FROM
    t1
LEFT JOIN t2 ON
    t1.year_month = t2.year_month
    AND t1.prof = t2.prof
GROUP BY
    t1.year_month
ORDER BY
    t1.year_month



