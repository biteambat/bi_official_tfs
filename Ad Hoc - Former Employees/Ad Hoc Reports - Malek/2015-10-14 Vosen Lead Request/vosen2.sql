

-- from: not Rejected, not Avoid, not Never reached, not Contact/ Account created
-- to: Rejected, Avoid, Never reached, Contact/ Account created


select *
from bi."LeadHistory"
where "Field" = 'Status'
and "OldValue" not in ('Rejected', 'Avoid', 'Never reached', 'Contact/Account created')
and "NewValue" in ('Rejected', 'Avoid', 'Never reached', 'Contact/Account created')

-- limit 10

;

select distinct "NewValue"
from bi."LeadHistory"
where "Field" = 'Status'
;


create or replace view reports.Lead_consumption_by_Country as

select b.lead_id, a."CreatedDate" as Date_Of_Status_Change
from bi."LeadHistory" a
left join main.lead b

on a."LeadId" = b.lead_id

where "Field" = 'Status'
and "OldValue" not in ('Rejected', 'Avoid', 'Never reached', 'Contact/Account created')
and "NewValue" in ('Rejected', 'Avoid', 'Never reached', 'Contact/Account created')
and cast(a."CreatedDate" as date) > current_date-30
order by 2

;