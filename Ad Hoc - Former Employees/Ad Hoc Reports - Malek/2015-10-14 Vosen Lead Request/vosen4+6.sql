CREATE OR REPLACE VIEW reports.Lead_Consumption_per_Agent as

/* 
6) Leadverbrauch per Agent pro Onboarding
=> # Accounts created/ # lead Status Changes pro Woche auf EndStatus: Rejected, Avoid, Never reached

aka Anzahl an zugewiesenen Lead und Anzahl der erfolglosen Leads
*/

select a."CreatedDate", a."LeadId", a.EmployeeID, b."Name", "Lead assigned", "Lead Not Converted", "Lead Converted"
from (

	-- #accounts created pro mitarbeiter
	-- New Value ist ID des mitarbeiters der lead zugewiesen bekommen hat
	select "CreatedDate", "LeadId", "NewValue" as EmployeeID, cast(1 as integer) as "Lead assigned", cast(null as integer) as "Lead Not Converted", cast(null as integer) as "Lead Converted"
	from bi."LeadHistory"
	where "Field" = 'Owner'
	and "NewValue" like '0052%'
	
	union
	
	-- # lead Status Changes auf EndStatus: Rejected, Avoid, Never reached - Pro Mitarbeiter
	-- createdbyid ist mitarbeiter der statusänderung durchgeführt hat
	select "CreatedDate", "LeadId", "CreatedById" as EmployeeID, cast(null as integer) as "Lead assigned", cast(1 as integer) as "Lead Not Converted", cast(null as integer) as "Lead Converted"
	from bi."LeadHistory"
	where "Field" = 'Status'
	and "NewValue" in ('Rejected', 'Avoid', 'Never reached')
	
	union
	
	-- #of converted Onboardings
	-- wechsel des Status auf contact/account created
	select "CreatedDate", "LeadId", "CreatedById" as EmployeeID, cast(null as integer) as "Lead assigned", cast(null as integer) as "Lead Not Converted", cast(1 as integer) as "Lead Converted"
	from bi."LeadHistory"
	where "Field" = 'Status'
	and "NewValue" = 'Contact/Account created'
	
	) a
	left join (
		select "Id", "Name"
			from bi."User") b
	on a.EmployeeID = b."Id"