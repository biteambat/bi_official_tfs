
-- Anzahl Cleaner und das Datum an dem sie ihren ersten Job erledigt haben, nur invoice, ca. 500 nie einen Job angenommen

-- select cleaner_id, count(*) from (
-- select * from (

select 
		  cast(x.created_at as date) as Onboarding_Date
		, x.cleaner_id		
		, x.working_city
		, First_Job_Date
		, cast(x.created_at as date)+30 as "Onboarding_Date+30"
		, First_Job_Date - cast(x.created_at as date) as Days_Till_First_Job
		, z.Order_Count
	from main.account x
	left join (
					select cleaner_id, min(order_start_date_utc) as First_Job_Date
					from main.order
					where order_status = 'INVOICED' and is_test_order = false
					group by cleaner_id
				) y
		on x.cleaner_id = y.cleaner_id
	left join (
					select 
							  a.cleaner_id
							, count(*) as Order_count
					from main.account a
						inner join main.order b
							on a.cleaner_id = b.cleaner_id
							and b.order_start_date_utc between cast(a.created_at as date) and cast(a.created_at as date)+30
					where b.order_status = 'INVOICED' and b.is_test_order = false
					group by  a.cleaner_id
				) z
		on x.cleaner_id = z.cleaner_id
	where is_test_account = false
	and (x.created_at) < current_date-30
	
	;
	select current_date-30

	
-- ) xy 	where first_job_date is null
	
	
	
-- ) xx group by cleaner_id having count(*) > 1
;

-- ----------------------------------------------------------------------------------

-- alle order die der Cleaner zwischen seinem Onboarding Date und den nächsten 30 Tagen zustande gekommen sind
select 
			  x.cleaner_id
			, count(*) as Order_count
from main.account x
	inner join main.order y
		on x.cleaner_id = y.cleaner_id
		and y.order_start_date_utc between cast(x.created_at as date) and cast(x.created_at as date)+30
where y.order_status = 'INVOICED' and y.is_test_order = false
group by  x.cleaner_id
;
select cleaner_id, count(*) from (

	select
			  a.cleaner_id
			, a.order_start_date_utc as First_Job_Date
			, a.order_status
			, a.payment_method
	from main.order a
		inner join (
						select cleaner_id, min(order_start_date_utc) as order_start_date_utc
							from main.order
							group by cleaner_id
							) b
			on a.cleaner_id = b.cleaner_id
			and a.order_start_date_utc = b.order_start_date_utc
	where a.order_status = 'INVOICED'
) xx group by cleaner_id having count(*) > 1
				-- where cleaner_id = '0012000001F7A6cAAF'
				
				
				
;

	select cleaner_id, order_start_date_utc, order_id, order_id2, order_status, created_at
	-- min(order_start_date_utc) as order_start_date_utc
		from main.order
		where cleaner_id = '0012000001FZRtjAAH'
		-- where cleaner_id = '0012000001FTdOGAA1'
		
	-- group by cleaner_id