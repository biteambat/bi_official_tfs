
/*
Karte alle Kunden 
welche in den letzten 3 Monaten in folgenden Zipcode Bereichen 
ihre recurrent order gecancelled haben (Cancelled Terminated events.sodium table):

- 8
- 5
- 6
auf Locale = 'DE' filtern 
wo waren diese Kunden located

*/
select customer_id, shipping_postal_code, shipping_latitude, shipping_longitude
			from main.order
			where recurrency > 0
			and order_status = 'CANCELLED TERMINATED'
			and created_at > '2015-11-23'
			and is_test_order is false
			and left(shipping_postal_code,1) in ('5','6','8')
			and left(locale,2) = 'de'			
;



/*
select * from 
events.sodium
where created_at > 
limit 10;
*/