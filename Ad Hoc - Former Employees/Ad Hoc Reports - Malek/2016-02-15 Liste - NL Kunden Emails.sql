
select *  
from main.order
limit 10

;


-- where working_city__c like '%Amsterdam%'
limit 100

;


select x.customer_id, customer_name, email, y.locale__c
from 	(
				-- selektiert nl-amsterdamkunden per shipping code, da keine working city vorhandne
				select customer_id, customer_name
				from main.order
				where LEFT(Locale,2) = 'nl' AND LEFT(shipping_postal_code,2) in ('10','11','14','15','21') 			-- codes for amsterdam
				and is_test_order = false
				and order_status = 'INVOICED'
				group by customer_id, customer_name
		) x
inner join (
				-- selektiert nl kunden die sich nicht aus mailbenachrichtigungen ausgetragen haben; join mit oberem query da keine working city vorhanden
				select sfid, email, name, hasoptedoutofemail, locale__c
				from salesforce.contact
				where left(locale__c,2) = 'nl'
				and hasoptedoutofemail = false
				) y
		on x.customer_id = y.sfid
		