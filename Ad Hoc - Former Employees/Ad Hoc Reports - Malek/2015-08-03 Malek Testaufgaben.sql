-- 1) Anzahl Cleaner pro Working City, welche nicht den Status LEFT oder TERMINATED haben
-- working_city__c

select working_city__c, count(*) as Cleaner_Count
from salesforce.account
where status__c not in ('LEFT', 'TERMINATED')
group by working_city__c
order by 2 desc
;

-- 2) Anzahl Onboarded Cleaner by Month
select to_char(createddate,'YYYY-MM') as Month, count(*) as Onboarded_Cleaner_Count
from salesforce.account
group by 1
order by 1
;

-- 3) Conversion pro Leadsource in den Monaten März bis Juni (Conversion = Account Created)
select leadsource, count(*) as Conversion_Count
	from	(
			select  sfid, to_char(createddate,'YYYY-MM') as Conversion_Month
			from salesforce.account
			where to_char(createddate,'YYYY-MM') between '2015-03' and '2015-06'
			) a
	inner join (
					select convertedaccountid, leadsource
					from salesforce.lead
					where isconverted = true	
					) l
	on a.sfid = l.convertedaccountid
	group by 1
	order by 2 desc
;
	
-- 4) Anzahl Invoiced Stunden pro Cleaner by WorkingCity von Cleaner
-- (JOIN Account mit Order Table über sfid & professional__c)
select working_city__c
		, InvoiceMonth
		, count(distinct(professional__c)) as Cleaner_Count
		, sum(order_duration__c) as Total_Hours
		, sum(order_duration__c)/count(distinct(professional__c)) as AVG_Hours
from (
		select o.professional__c, to_char(o.effectivedate,'YYYY-MM') as InvoiceMonth
				, o.status, o.order_duration__c, a.working_city__c
		from salesforce.order o
		inner join account a
		on o.professional__c = a.sfid
		where status = 'INVOICED' 
		and to_char(effectivedate,'YYYY-MM') between '2015-03' and '2015-08'
		) x
	group by working_city__c, InvoiceMonth
	order by 1,2
;

-- #5 ohne workingcity + topline (-> ohne Workingcity)
select 
		  InvoiceMonth
		, count(distinct(professional__c)) as Cleaner_Count
		, sum(order_duration__c) as Total_Hours
		, sum(order_duration__c)/count(distinct(professional__c)) as AVG_Hours
	from (
			select o.professional__c
					, to_char(o.effectivedate,'YYYY-MM') as InvoiceMonth
					, o.status
					, o.order_duration__c
					, case
						when a.working_city__c = 'Stuttgart +'
						then 'Stuttgart'
						when a.working_city__c = 'Saarbrucken'
						then 'Saarbrücken'
						else a.working_city__c
					end as working_city__c
			from salesforce.order o
			inner join account a
			on o.professional__c = a.sfid
			where status = 'INVOICED' 
			and to_char(effectivedate,'YYYY-MM') between '2015-03' and '2015-07'
		) x
	group by InvoiceMonth
	order by 1,2
;
	
-- #6 Conversion Rate (count pro lead / count conversion)
SELECT 
		  Month
		, leadsource
		, count(*)
		, sum(converted) as Converted_Cleaner
FROM (
		SELECT 
				  to_char(createddate,'YYYY-MM') as Month
				, case when leadsource is null
						then ''
						else leadsource
						end as leadsource
				, STATUS
				, CASE WHEN STATUS = 'Contact/Account created' THEN 1 ELSE 0 END AS Converted
		FROM salesforce.lead
		where to_char(createddate,'YYYY-MM') between '2015-03' and '2015-08'
		) x
group by Month,leadsource
order by 1,2
