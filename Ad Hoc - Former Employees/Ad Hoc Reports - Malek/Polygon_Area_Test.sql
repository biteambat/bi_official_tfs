drop table main.testtest;
create table main.testtest as
select y.*, ST_GeomFromGeoJSON(cut2) as polygon1
from (
		select key__c, replace(cut, '},"properties":{}}', ',"crs":{"type":"name","properties":{"name":"EPSG:4326"}}}') as cut2, polygon__c
		from (
				select key__c, replace(polygon__c, '{"type":"Feature","geometry":', '') as cut, polygon__c::json
				from salesforce.delivery_area__c
				where polygon__c like '%"Polygon"%' and key__c <> 'de-berlin'
				) x
		) y;

select *
from main.testtest;


select key__c, polygon__c
				from salesforce.delivery_area__c