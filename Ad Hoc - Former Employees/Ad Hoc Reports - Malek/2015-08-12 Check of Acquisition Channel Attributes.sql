-- Check of Acquisition Channel Attributes

select sum(x.counting)
from  (
		select (phone), count(*) as counting 
		from salesforce.lead
		where status <> 'Double Entry'
		group by phone
		having count(*) > 1
		-- order by 2 desc 
		) x
;

-- availability_daily__c, availability_weekly__c, projected_working_hours__c
select  distinct(projected_working_hours__c), count(*)
from salesforce.lead
group by projected_working_hours__c
;

select
acquisition_channel__c
,acquisition_channel_params__c
,acquisition_channel_ref__c
from salesforce.lead
;

select acquisition_channel__c, count(*)
from salesforce.lead
group by acquisition_channel__c
;

select acquisition_channel_params__c, count(*)
from salesforce.lead
group by acquisition_channel_params__c
order by 2 desc
;

select acquisition_channel_ref__c, count(*)
from salesforce.lead
group by acquisition_channel_ref__c
order by 2 desc
;


