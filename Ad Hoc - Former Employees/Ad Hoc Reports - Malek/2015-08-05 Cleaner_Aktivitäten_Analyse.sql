Select distinct(status__c)
from salesforce.account
;

-- zählt verschiedene Statuse jedes Cleaners
select cleaner_id, count(distinct(Status))
from (
		select professional_json::json->>'Id' as cleaner_id, professional_json::json->>'Status__c' as Status, created_at
		from bi.events_sodium_20150703
		) x
	group by cleaner_id
	order by 2 desc
;

-- Anzahl der Unique Statuse
select
		distinct professional_json::json->>'Status__c', count(*)
from events.sodium
group by professional_json::json->>'Status__c'
;

select cleaner_id,status, created_at
from (	
			select
						professional_json::json->>'Id' as cleaner_id
					,  professional_json::json->>'Status__c' as Status
					,  min(created_at) as created_at
			from events.sodium
			group by professional_json::json->>'Id', professional_json::json->>'Status__c'
			limit 3000
	) x
order by 1,3
;

select 
			professional_json::json->>'Id' as cleaner_id
		,  professional_json::json->>'Status__c' as Status
		, created_at as created_at
		, event_name
from events.sodium
where	professional_json::json->>'Id' = '0012000001APNw5AAH'
order by 3
;

select 
			professional_json::json->>'Id' as cleaner_id
		,  professional_json::json->>'Status__c' as Status
		, created_at as created_at
		, event_name
from events.sodium
where	professional_json::json->>'Id' = '0012000001APNw5AAH'
order by 3
;