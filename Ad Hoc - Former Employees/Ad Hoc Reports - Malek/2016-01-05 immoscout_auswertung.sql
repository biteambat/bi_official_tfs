-- immoscout auswertung
select count(*) as Count_Acquisitions_all, sum(Not_Cancelled_Orders) as Count_Acquisitions_not_cancelled
from (
			select 
					  created_at
					, order_id
					, customer_id
					, order_status
					, is_new_customer, voucher
					, case when order_status not like 'CANCELLED%' and order_status <> 'ERROR GEO' then 1 else 0 end as Not_Cancelled_Orders
			from main."order"
			-- geo error & cancelled
			where voucher in ('IMMOTIGER', 'IMMOTIGER20', 'PUTZPARTNER20', 'CSIS512C20', 'CSXU328C15')
			and is_new_customer is true
		) x