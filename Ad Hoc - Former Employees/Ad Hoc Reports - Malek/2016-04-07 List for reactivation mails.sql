/*
list for the upcoming reactivation emails

x All countries (for NL, only the ZIP codes starting by: 10,11,12,14 )
x Had one invoiced order or more
x Had no order in the last 40 days
x No order in the future
x No opt out

*/

select a.customer_id , left(a.locale,2) as locale, right(a.locale,2) as language, a.customer_name, a.customer_email
from main.order a
inner join (
				-- customer_id aus de, ch, at, und nl (nl auf PLZ 10,11,12,14 beschränkt)
				-- Kunden mit mindestens einer invoiced order 
				select max(order_id) as order_id, customer_id
				from main.order
				where (
						left(locale,2) in ('at','ch','de') OR
						(left(locale,2) = 'nl' and left(shipping_postal_code,2) in ('10','11','12','14'))
						)
						and order_status = 'INVOICED'
				group by customer_id
				) b
on a.order_id = b.order_id and a.customer_id = b.customer_id

and a.customer_id not in (
								-- Kunden die in den letzten 40 Tage eine Order hatten (werden ausgeschlossen)
								select distinct customer_id
								from main.order
								where order_status = 'INVOICED'
								and order_start_date_utc between current_date-41 and current_date-1
								)
and a.customer_id not in (
								-- Kunden die in Zukunft offene orders haben (werden ausgeschlossen)
								select distinct customer_id
								from main.order
								where order_start_date_utc > '2016-04-08'
								and is_test_order is false
								and customer_id is not null

							)
and a.customer_id in (
								-- ausschluss von kunden die sich nicht von newsletter ausgetragen haben (opt in)
								select sfid
								from salesforce.contact
								where hasoptedoutofemail = false
							)
;