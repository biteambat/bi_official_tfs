SELECT attnum, attname AS column, format_type(atttypid, atttypmod) AS type
FROM   pg_attribute
WHERE  attrelid = 'events.sodium'::regclass   -- optionally schema-qualified
AND    NOT attisdropped
AND    attnum > 0
ORDER  BY attnum;
