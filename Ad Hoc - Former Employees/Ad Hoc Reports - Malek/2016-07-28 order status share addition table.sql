select *
from (
		-- number of holiday & sickness last week #4, #5
		select b.delivery_areas__c as delivery_area, sum(holiday_flag) as holiday_flag, sum(sickness_flag) as sickness_flag
		from bi.holidays_cleaner_3 a
		LEFT JOIN Salesforce.Account b
		on a.account__c = b.sfid
		and EXTRACT(week from a.date) = EXTRACT(week from current_date)-1
		and left(b.delivery_areas__c,2) = 'de'
		group by b.delivery_areas__c
) t1

left join (
			-- UR #2
			select 
					  delivery_area as area2
					, sum(worked_hours) as worked_hours
					, sum(weekly_capped) as weekly_capped
			from (
					select
							  EXTRACT(week from mindate)
							, delivery_area
							, professional__c
							, worked_hours 
						 	, case
							 		when worked_hours > weekly_hours
							 		then worked_hours
									else weekly_hours
							  end as weekly_capped
						
					from bi.gpm_weekly_cleaner
					where EXTRACT(week from mindate) = EXTRACT(week from current_date)-1
					) x
			group by delivery_area
			) t2
	on t1.delivery_area = t2.area2

left join (
			-- muffin cleaner contract of last week #3
			select 
			  delivery_areas__c as area3, count(*) beta_cleaner_count
			from Salesforce.Account
			where status__c = 'BETA'
			and hr_contract_end__c >= (cast(date_trunc('week', current_date) as date)-1)
			and type__c like '%60%'
			and left(delivery_areas__c,2) = 'de'
			group by delivery_areas__c
			) t3
	on t1.delivery_area = t3.area3

left join (
				-- variance #7, #8, #9
				select 	
						  polygon as area4
						, sum( case
									when status in ('INVOICED', 'NOSHOW PROFESSIONAL', 'NOSHOW CUSTOMER', 'CANCELLED NO MANPOWER', 'CANCELLED NOT THERE YET', 'PENDING TO START')
									and EXTRACT(week from effectivedate) = EXTRACT(week from current_date)-1
									then order_duration__c
									else 0 end
								)
							as Var_Demand_Week_1
						, sum(
								case
									when status = 'CANCELLED TERMINATED'
										and EXTRACT(week from effectivedate) = EXTRACT(week from current_date)-1
									then order_duration__c
									else 0 end
								)
							as Var_Terminated_Week_1
						, sum(
								case
									when status = 'CANCELLED SKIPPED'
										and EXTRACT(week from effectivedate) = EXTRACT(week from current_date)-1
									then order_duration__c
									else 0 end
								)
							as Var_Skipped_Week_1
						, sum( case
									when status in ('INVOICED', 'NOSHOW PROFESSIONAL', 'NOSHOW CUSTOMER', 'CANCELLED NO MANPOWER', 'CANCELLED NOT THERE YET', 'PENDING TO START')
									and EXTRACT(week from effectivedate) = EXTRACT(week from current_date)-2
									then order_duration__c
									else 0 end
								)
							as Var_Demand_Week_2
						, sum(
								case
									when status = 'CANCELLED TERMINATED'
										and EXTRACT(week from effectivedate) = EXTRACT(week from current_date)-2
									then order_duration__c
									else 0 end
								)
							as Var_Terminated_Week_2
						, sum(
								case
									when status = 'CANCELLED SKIPPED'
										and EXTRACT(week from effectivedate) = EXTRACT(week from current_date)-2
									then order_duration__c
									else 0 end
								)
							as Var_Skipped_Week_2
				from bi.orders
				where left(polygon,2) = 'de'
				and order_type = 1
				group by polygon
			) t4
	on t1.delivery_area = t4.area4