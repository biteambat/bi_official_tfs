
-- growth
select f.*, hour_prediction_base + hour_prediction_base*growth_agg as predicted_hour
from (
select 
			  concat('CW',EXTRACT(WEEK from first_day_of_cw)) as calendarweek
			, z.*
			, CASE WHEN ranking >= 9 THEN sum(growth_rate) OVER (ORDER BY delivery_area, first_day_of_cw ROWS BETWEEN 8 PRECEDING AND 4 PRECEDING)
				ELSE NULL END AS growth_agg
from (
		select y.*, CASE WHEN round(cast((growth/pre_hours) as numeric),2) > 0.05 or round(cast((growth/pre_hours) as numeric),2) < -0.05 THEN NULL
							ELSE round(cast((growth/pre_hours) as numeric),2) END as growth_rate
		from (
				select x.*
				 		, rank() OVER (PARTITION BY delivery_area ORDER BY first_day_of_cw asc) as ranking
				 		, lag(hours) OVER (PARTITION BY delivery_area ORDER BY first_day_of_cw asc) as pre_hours
				 		, lag(hours,5) OVER (ORDER BY delivery_area, first_day_of_cw) as hour_prediction_base
				 		, hours - lag(hours) OVER (ORDER BY delivery_area, first_day_of_cw) as growth
				 		, avg(hours) OVER(ORDER BY delivery_area rows 7 preceding) AS mean7d
				from (
						select 
									  a.first_day_of_cw
									, a.delivery_area
									, b.hours
									, b.hours_mean
						from (
								select 
									  cast(date as date) as first_day_of_cw
									  ,unnest(array (
															select distinct delivery_area
													from bi.orders)) as delivery_area
									from generate_series(
									  '2016-01-04'::date,
									  now()::date+28,
									  '1 week'::interval
									  ) date	
									) a		  
						left join (
										select 
												cast(date_trunc('week', order_date) as date) as first_day_of_cw
												, delivery_area
												, sum(test) as hours_mean
												, sum(hours) as hours
										from (
										select 
												    a.*
												  , rank() OVER (PARTITION BY delivery_area ORDER BY order_date asc) as ranking
												  , avg(hours) OVER (PARTITION BY dow, delivery_area ORDER BY order_date asc ROWS BETWEEN 2 PRECEDING AND 0 FOLLOWING) as test
										from (
													select
															  delivery_area
															, date_part('dow',  cast(order_start__c as date)) as dow
															, cast(order_start__c as date) as order_date
															, sum(order_duration__c) hours
													from bi.orders
													where order_start__c between '2016-01-04' and cast(date_trunc('week', now()) as date)+7
													and "status" = 'INVOICED'	
													group by delivery_area, order_date
													) a
											) x
										group by first_day_of_cw, delivery_area
									) b
						ON a.first_day_of_cw = b.first_day_of_cw AND a.delivery_area = b.delivery_area
					) x
			) y
	) z	
) f
;