/*
	Liste aller Kunden und den Infos zu ihrer ersten Bestellung, plus binärattribut ob sie später erneut gebucht haben
*/

select
			  a.*
			, case 
					when booking_weekday_number = 1 then 'Monday'
					when booking_weekday_number = 2 then 'Tuesday'
					when booking_weekday_number = 3 then 'Wednesday'
					when booking_weekday_number = 4 then 'Thursday'
					when booking_weekday_number = 5 then 'Friday'
					when booking_weekday_number = 6 then 'Saturday'
					when booking_weekday_number = 7 then 'Sunday'
					else null
					end as booking_weekday
			, case 
					when order_date_weekday_number = 1 then 'Monday'
					when order_date_weekday_number = 2 then 'Tuesday'
					when order_date_weekday_number = 3 then 'Wednesday'
					when order_date_weekday_number = 4 then 'Thursday'
					when order_date_weekday_number = 5 then 'Friday'
					when order_date_weekday_number = 6 then 'Saturday'
					when order_date_weekday_number = 7 then 'Sunday'
					else null
					end as order_date_weekday
			, case
					when email_domain_b not in ('gmail','googlemail','gmx','web','hotmail','yahoo','t-online','aol','arcor','me','freenet','live','outlook','icloud','chello','bluewin','online') then 'other'
					when email_domain_b = 'googlemail' then 'gmail'
					when email_domain_b is null then 'other'
					else email_domain_b
					end as email_domain
			, b.marketing_channel
			, b.working_city

from (
			select 
						-- ids
						  d.created_at
						, d.customer_id
						, d.order_id
						, d.order_status
						, e.is_returning_customer
						, e.is_returning_customer_bool
						, left(d.locale,2) as locale
			
						-- attribute
						, date_part('DOW', created_at) as booking_weekday_number -- (Monday=1 .. Sunday=7)
						, date_part('Hour', created_at) as booking_hour
						, payment_method
						, replace(substring(substring(customer_email from position('@'in customer_email)+1 for length(customer_email)) from 1 for position('.' in substring(customer_email from position('@'in customer_email)+1 for length(customer_email)))),'.','') as email_domain_b
						, order_start_date_utc
						, date_part('DOW', order_start_date_utc) as order_date_weekday_number
						, order_start_time_utc
						, date_part('Hour', order_start_time_utc) as order_date_hour
						, case when recurrency = 0 or recurrency is null then 'no'	else 'yes' end as has_recurrency
						, case when setting_flexible_time is null then 'no' else 'yes' end as has_set_flexible_time
						, case when setting_gender is null 			then 'no' else 'yes' end as has_set_gender
						, case when rating_professional is null then 'no' else 'yes' end as has_rated_professional
						, case when rating_service is null then 'no' else 'yes' end as has_rated_service
						, setting_flexible_time
						, setting_gender
						, rating_professional
						, rating_service
						, order_duration
						, case when voucher is null then 'no' else 'yes' end as used_voucher
						, discount_amount
						, gmv_net_amount_eur
						, price_per_hour
						, auto_matched				-- ok
						, case
								when user_agent like '%Windows%' then 'Windows'
								when user_agent like '%Linux%Android%' then 'Android'
								when user_agent like '%Android%Tablet%' then 'Android'
								when user_agent like '%Linux%' then 'Linux'
								when user_agent like '%Macintosh%' then 'MacOSX'
								when user_agent like 'bookatiger%' then 'iOS_App'
								when user_agent like 'android%' or user_agent like 'BAT_Android%' then 'Android_App'
								when user_agent like '%iPhone%' then 'iOS'
								when user_agent like '%iPad%' then 'iOS'
								else 'unknown'
							end as platform
						, case
								when user_agent like '%Opera%' then 'Opera'
								when user_agent like '%Trident%' then 'Internet Explorer'
								when user_agent like '%Firefox%' then 'Firefox'
								when user_agent like '%Chrome%' then 'Chrome'
								when user_agent like '%Safari%' then 'Safari'
								else 'unknown'
							end as browser
			from main.order d
			
			inner join (
							-- selecting data basis (first time customers + attribute if the returned or not)
							select  
									  x.customer_id
									, x.order_id
									, x.order_status as status_A
									, y.order_status as status_B
									, case when y.order_status is null then 'no' else 'yes' end as is_returning_customer
									, case when y.order_status is null then false else true end as is_returning_customer_bool
							from (
									-- selecting first order of a customer, limited only to invoiced orders
									select created_at, customer_id, order_id, order_status
									from main.order
									where is_new_customer is true
									and order_status = 'INVOICED'
									) x
							left join (
											-- selecting second order of a customer (if existing), limiting to certain statuses (see below)
											select * 
											from (select customer_id, order_status, rank() OVER (PARTITION BY customer_id ORDER BY created_at ASC)
											from main.order ) b
											where rank = 2
											and order_status in ('CANCELLED NO MANPOWER', 'CANCELLED PROFESSIONAL', 'CANCELLED PROFESSIONAL SHORTTERM', 'INVOICED', 'NOSHOW PROFESSIONAL', 'PENDING TO START')
										) y
								on x.customer_id = y.customer_id
							) e
			on d.customer_id = e.customer_id and d.order_id = e.order_id
			) a

	left join (
					select sfid as order_id, contact__c as customer_id, marketing_channel, city as working_city
					from bi.orders 
				) b
		on a.customer_id = b.customer_id and a.order_id = b.order_id
;