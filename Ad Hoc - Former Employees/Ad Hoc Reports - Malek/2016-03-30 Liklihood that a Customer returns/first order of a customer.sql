/*
	Queries um Kunde und jeweilige erste OrderID zu ermitteln
*/

-- Versuch 1: kunden unique + order id der ersten bestellung - nicht gut, teils wird mit MIN-Funktion nicht die erste Bestellung ausgewählt (warum auch immer)
select customer_id, min(order_id2) as order_id2, count(*)
from main.order
group by customer_id
;
-- Versuch 2: kunden nach order-date durchnummeriert
select order_status, count(*) from (
select created_at, customer_id, customer_name, order_id, order_id2, order_status, rank() OVER (PARTITION BY customer_id ORDER BY created_at ASC)
from main.order
) x
where rank = 2
group by order_status
;

-- Versuch 3: nutzt vorhandenes Attribut in DB was neue Kunden flaggt (funktioniert jedoch auch nich 100%ig)
select created_at, customer_id, order_id, order_status
from main.order
where is_new_customer is true
and order_status = 'INVOICED'
;