
select distinct customer_id, customer_id2, customer_name, customer_email
from main.order
where created_at > cast(now() as date)-14
and LEFT(locale,2) = 'de'
and (
		LEFT(shipping_postal_code,2) in ('20','22') 
		or LEFT(shipping_postal_code,5) in ('21031','21075','21077','21079''21107','21109','21129')
		)
and customer_id2 in (
							select customer_id
							from bi.recurrent_temp1
							)

;