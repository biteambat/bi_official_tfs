select
		  o.sfid
		, contact__c
		, status
		, cancellation_reason__c
		, EXTRACT(week from effectivedate) as week
		, effectivedate
		, polygon
		, upper(left(o.polygon,2)) as country
from bi.orders o

where status = 'CANCELLED TERMINATED'
and cancellation_reason__c like 'Customer%'
and effectivedate between current_date-90 and current_date - cast(extract(dow from current_date) as int)

limit 10
;