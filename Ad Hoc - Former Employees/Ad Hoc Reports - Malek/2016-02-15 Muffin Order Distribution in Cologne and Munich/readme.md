Muffin Order Distribution in Cologne and Munich

Shows the development of bookings before and after the muffin launch in the cities cologne and munich. The assumption was, that the number of bookings outside of the cities has been dramatically reduced, which was confirmed.
Used start date for munich was 01.08.2015 and for cologne 01.09.2015. Every order in the pictures before the muffin start date are marked as orange. The one's after the start in blue. 