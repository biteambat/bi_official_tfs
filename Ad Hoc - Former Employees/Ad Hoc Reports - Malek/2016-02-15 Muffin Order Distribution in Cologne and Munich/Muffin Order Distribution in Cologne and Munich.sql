
select 
		  order_start_date_utc
		, case when working_city = 'DE-Munich' and order_start_date_utc < '12-01-2015' THEN 'before'
				 when working_city = 'DE-Munich' and order_start_date_utc >= '12-01-2015' THEN 'after'
				 when working_city = 'DE-Cologne' and order_start_date_utc < '01-01-2016' THEN 'before'
				 when working_city = 'DE-Cologne' and order_start_date_utc >= '01-01-2016' THEN 'after'
				 else null
			end as asdf
		, order_status
		, order_id
		, shipping_postal_code
		, shipping_city
		, shipping_latitude
		, shipping_longitude
		, working_city
		
from main.order x
left join (
			select cleaner_id, working_city
			from main.account) y
	on x.cleaner_id = y.cleaner_id
where order_status in ('INVOICED','CANCELLED NO MANPOWER')
AND (
	working_city = 'DE-Munich' and order_start_date_utc > '08-01-2015'
	or
	working_city = 'DE-Cologne' and order_start_date_utc > '09-01-2015'
	)