﻿-- creating a networkgraph


-- creates a list of nodes
select *
from (

select cleaner_id as id, 'cleaner' as type, latitude, longtitude
from main.radius_cleaner

union

select customer_id as id, 'customer' as type, latitude, longtitude
from main.radius_customer

union

select lead_id as id, 'lead' as type, latitude, longtitude
from main.radius_lead

) x
where x.latitude is not null
order by 3

-- creates edges
select *
from main.radius_relation_cleaner_customer

union

select *
from main.radius_relation_lead_customer