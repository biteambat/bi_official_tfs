﻿-- relation Cleaner <-> Customer
DROP TABLE IF EXISTS main.radius_relation_Cleaner_Customer;
CREATE TABLE main.radius_relation_Cleaner_Customer as
select * from (
SELECT
	  a.cleaner_id
	, b.customer_id
	, ST_Distance_Sphere(a.cleaner_location, b.customer_location) as distance
	, ST_MakeLine(a.cleaner_location, b.customer_location) as distance_line
	FROM main.radius_cleaner a
	left join main.radius_customer b
		ON ST_DWithin(a.cleaner_location::geography, b.customer_location::geography, a.action_radius*1000)
		) x where customer_id is not null
;

-- relation Lead <-> Customer
DROP TABLE IF EXISTS main.radius_relation_Lead_Customer;
CREATE TABLE main.radius_relation_Lead_Customer as
select * from (
select 
	  a.lead_id
	, b.customer_id
	, ST_Distance_Sphere(a.lead_location, b.customer_location) as distance
	, ST_MakeLine(a.lead_location, b.customer_location) as distance_line
	from main.radius_lead a
	left join main.radius_customer b
	on ST_DWithin(a.lead_location::geography, b.customer_location::geography, a.action_radius*1000)
		) x where customer_id is not null
;

-- relation Cleaner <-> Lead
DROP TABLE IF EXISTS main.radius_relation_Cleaner_Lead;
CREATE TABLE main.radius_relation_Cleaner_Lead as

select distinct a.lead_id, b.cleaner_id
from main.radius_relation_Lead_Customer a
inner join main.radius_relation_Cleaner_Customer b
on a.customer_id = b.customer_id