﻿-- Cleaner Radius --
-- only Cleaner who did (and finished -> "INVOICED") a job in the last 30 days AND have the status active or beta
-- no testorders
DROP TABLE IF EXISTS main.radius_cleaner;
CREATE TABLE main.radius_cleaner as

select 
	  a.cleaner_id
	, ST_Point(location_longitude, location_latitude) as cleaner_location
	, case
			when effective_radius is null and has_vehicle is false
			then 5
			when effective_radius is null and has_vehicle is true
			then 5
		else effective_radius
		end as action_radius
	-- zum testen:
	, billing_street
	, billing_postal_code
	, billing_city
	, Point(location_longitude, location_latitude) as cleaner_point
	, location_longitude as Longtitude
	, location_latitude as Latitude
from main.account a
inner join (
		select distinct cleaner_id
		from main.order
		where cast(created_at as date) > current_date-30
		and order_status = 'INVOICED'
		and cleaner_id <> ''
		and is_test_order = false
	) b
on a.cleaner_id = b.cleaner_id
where status in ('ACTIVE','BETA')
;

-- Customer Radius --
-- Customer who placed a order in the last 14 days, if a customer placed more than one order the latest has been taken
-- Orders mit Status "Cancelled Terminated" werden ausgeschlossen, da es sich um autom. generierte Folgebestellungen handelt
-- ebenfalls ausgeschlossen: Testorder, fake und mistake
DROP TABLE IF EXISTS main.radius_customer;
CREATE TABLE main.radius_customer as

select 
	
	  a.customer_id
	, a.order_id
	, ST_Point(shipping_longitude, shipping_latitude) as customer_location
	, order_duration
	-- zum Testen:
	, shipping_street
	, shipping_postal_code
	, shipping_city
	, Point(shipping_longitude, shipping_latitude) as customer_point
	, shipping_longitude as Longtitude
	, shipping_latitude as Latitude
	, order_status
from main.order a
inner join (
		select customer_id, max(order_id2) as order_id2
		from main.order
		where cast(created_at as date) between current_date-14 and current_date
		and order_status not in ('CANCELLED TERMINATED', 'CANCELLED FAKED','CANCELLED MISTAKE')
		and is_test_order = false
		group by customer_id
		) b
on a.customer_id = b.customer_id
and a.order_id2 = b.order_id2
;

-- Lead Radius
-- lead cannot give a radius, therefore we assume 5km if the have a vehicle and 5km if they dont have one
-- no testleads
DROP TABLE IF EXISTS main.radius_lead;
CREATE TABLE main.radius_lead as

select 
	  lead_id
	, created_at
	, status
	, working_city
	, ST_Point(longitude, latitude) as lead_location
	, case
			when has_vehicle is false
			then 5
			when has_vehicle is true
			then 5
		else 0
		end as action_radius
	-- zum Testen:
	, street
	, postal_code
	, city
	, Point(longitude, latitude) as lead_point
	, longitude as Longtitude
	, latitude as Latitude	
from main.lead
where status in ('New lead', 'First contact outstanding', 'Rejected', 'Never reached')
and working_city in ('Munich', 'Frankfurt am Main','Cologne')
and cast(created_at as date) > '2015-07-01'
and is_test_lead = false
;