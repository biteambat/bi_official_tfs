﻿drop table main.grid;

CREATE TABLE main.grid AS

SELECT 
(ystep/1000 + 1) * 5 - 5 + xstep/1000 + 1 as key, -- number grids in order across the top first, then down

st_setsrid( 
    st_makebox2d(
        st_translate(topleft,xstep,ystep*-1), 
        st_translate(topleft,xstep + 1000,ystep*-1 - 1000)
    ), 
    900913
) as way 

FROM 
(
    SELECT 
    xstep,
    ystep,
    st_point(1490373.83, 6895774.25) AS topleft -- http://twcc.fr/en/#
    -- st_point(52.54003,13.37304) AS topleft -- Top left starting point
    FROM
    ( SELECT generate_series(0,4000,1000) as xstep) as xstepq,  -- series from 0 to 4000, with 1000 meter steps
    ( SELECT generate_series(0,4000,1000) as ystep) as ystepq
) AS factors ;