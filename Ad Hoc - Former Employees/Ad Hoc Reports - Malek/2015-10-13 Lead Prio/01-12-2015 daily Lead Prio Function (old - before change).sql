﻿CREATE OR REPLACE FUNCTION bi.daily$lead_prio(crunchdate date) RETURNS void AS 
$BODY$
BEGIN

				-- +++++ A +++++
-- Cleaner Radius --
-- only Cleaner who did (and finished -> "INVOICED") a job in the last 30 days AND have the status active or beta
-- no testorders
DROP TABLE IF EXISTS main.radius_cleaner;
CREATE TABLE main.radius_cleaner as

select 
	  a.cleaner_id
	, ST_SetSRID(ST_Point(location_longitude, location_latitude),4326)  as cleaner_location
	, case
			when effective_radius is null and has_vehicle is false
			then 5
			when effective_radius is null and has_vehicle is true
			then 5
		else 5 -- effective_radius
		end as action_radius
	-- zum testen:
	, billing_street
	, billing_postal_code
	, billing_city
	, Point(location_longitude, location_latitude) as cleaner_point
	, location_longitude as Longtitude
	, location_latitude as Latitude
from main.account a
inner join (
		select distinct cleaner_id
		from main.order
		where cast(created_at as date) > current_date-30
		and order_status = 'INVOICED'
		and cleaner_id <> ''
		and is_test_order = false
	) b
on a.cleaner_id = b.cleaner_id
where status in ('ACTIVE','BETA')
;

-- Customer Radius --
-- Customer who placed a order in the last 14 days, if a customer placed more than one order the latest has been taken
-- Orders mit Status "Cancelled Terminated" werden ausgeschlossen, da es sich um autom. generierte Folgebestellungen handelt
-- ebenfalls ausgeschlossen: Testorder, fake und mistake
DROP TABLE IF EXISTS main.radius_customer;
CREATE TABLE main.radius_customer as

select 
	
	  a.customer_id
	, a.order_id
	, ST_SetSRID(ST_Point(shipping_longitude, shipping_latitude),4326) as customer_location
	, order_duration
	-- zum Testen:
	, shipping_street
	, shipping_postal_code
	, shipping_city
	, Point(shipping_longitude, shipping_latitude) as customer_point
	, shipping_longitude as Longtitude
	, shipping_latitude as Latitude
	, order_status
from main.order a
inner join (
		select customer_id, max(order_id2) as order_id2
		from main.order
		where cast(created_at as date) between current_date-14 and current_date
		and order_status not in ('CANCELLED TERMINATED', 'CANCELLED FAKED','CANCELLED MISTAKE')
		and is_test_order = false
		group by customer_id
		) b
on a.customer_id = b.customer_id
and a.order_id2 = b.order_id2
;

-- Lead Radius
-- lead cannot give a radius, therefore we assume 25km if the have a vehicle and 10km if they dont have one
-- no testleads
DROP TABLE IF EXISTS main.radius_lead;
CREATE TABLE main.radius_lead as

select 
	  lead_id
	, created_at
	, ST_SetSRID(ST_Point(longitude, latitude),4326) as lead_location
	, case
			when has_vehicle is false
			then 5
			when has_vehicle is true
			then 5
		else 5
		end as action_radius
	-- zum Testen:
	, street
	, postal_code
	, city
	, upper(left(locale,2)) as locale
	, other_phone
	, Point(longitude, latitude) as lead_point
	, longitude as Longtitude
	, latitude as Latitude	
from main.lead
where cast(created_at as date) between current_date-10 and current_date
and is_test_lead = false
;

				-- +++++ B +++++

-- relation Cleaner <-> Customer
DROP TABLE IF EXISTS main.radius_relation_Cleaner_Customer;
CREATE TABLE main.radius_relation_Cleaner_Customer as
select * from (
SELECT
	  a.cleaner_id
	, b.customer_id
	, ST_Distance_Sphere(a.cleaner_location, b.customer_location) as distance
	, ST_MakeLine(a.cleaner_location, b.customer_location) as distance_line
	FROM main.radius_cleaner a
	left join main.radius_customer b
		ON ST_DWithin(a.cleaner_location::geography, b.customer_location::geography, a.action_radius*1000)
		) x where customer_id is not null
;

-- relation Lead <-> Customer
DROP TABLE IF EXISTS main.radius_relation_Lead_Customer;
CREATE TABLE main.radius_relation_Lead_Customer as
select * from (
select 
	  a.lead_id
	, b.customer_id
	, ST_Distance_Sphere(a.lead_location, b.customer_location) as distance
	, ST_MakeLine(a.lead_location, b.customer_location) as distance_line
	from main.radius_lead a
	left join main.radius_customer b
	on ST_DWithin(a.lead_location::geography, b.customer_location::geography, a.action_radius*1000)
		) x where customer_id is not null
;

-- relation Cleaner <-> Lead
DROP TABLE IF EXISTS main.radius_relation_Cleaner_Lead;
CREATE TABLE main.radius_relation_Cleaner_Lead as

select distinct a.lead_id, b.cleaner_id
from main.radius_relation_Lead_Customer a
inner join main.radius_relation_Cleaner_Customer b
on a.customer_id = b.customer_id
;

				-- +++++ C +++++
-- cleaner/customer ratio:
			--  if > 1 = more customer than cleaner
			--  if < 1 = more cleaner than customer
DROP TABLE IF EXISTS reports.Lead_Prio_Overview;
CREATE TABLE reports.Lead_Prio_Overview as
select 
		  created_at
		, a.lead_id
		, action_radius
		, a.street
		, a.postal_code
		, a.city
		, a.locale
		, a.other_phone
		, COALESCE(b.cleaner_count,0) as cleaner_count
		, COALESCE(c.customer_count,0) as customer_count
		, cast(COALESCE(b.cleaner_count::numeric/c.customer_count::numeric,0) as numeric(7,2)) as Cleaner_Customer_Ratio
		, a.lead_point
from main.radius_lead a
	left join (	
			select lead_id, count(cleaner_id) as Cleaner_Count
			from main.radius_relation_cleaner_lead
			group by lead_id
		) b
		on a.lead_id = b.lead_id
	left join (
			select c.lead_id, count(c.customer_id) as Customer_Count, sum(d.order_duration) as Total_Demanded_Hours
			from main.radius_relation_lead_customer c
			left join main.radius_customer d
			on c.customer_id = d.customer_id
			group by lead_id
		) c
		on a.lead_id = c.lead_id
;
END;
$BODY$ LANGUAGE 'plpgsql'