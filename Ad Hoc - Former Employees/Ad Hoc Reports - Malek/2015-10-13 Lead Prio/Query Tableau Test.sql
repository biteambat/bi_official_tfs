﻿-- LEAD
select 
		  a.lead_id as id
		, 'Lead' as type
		, action_radius
		, a.latitude
		, a.longtitude
		, a.city
		, COALESCE(b.cleaner_count,0) as cleaner_count
		, COALESCE(c.customer_count,0) as customer_count
		, c.Total_Demanded_Hours
		, cast(COALESCE(b.cleaner_count::numeric/c.customer_count::numeric,0) as numeric(7,2)) as Cleaner_Customer_Ratio
		, null as order_status
			
from main.radius_lead a
	left join (	
			select lead_id, count(cleaner_id) as Cleaner_Count
			from main.radius_relation_cleaner_lead
			group by lead_id
		) b
		on a.lead_id = b.lead_id
	left join (
			select c.lead_id, count(c.customer_id) as Customer_Count, sum(d.order_duration) as Total_Demanded_Hours
			from main.radius_relation_lead_customer c
			left join main.radius_customer d
			on c.customer_id = d.customer_id
			group by lead_id
		) c
		on a.lead_id = c.lead_id

union

select
	  cleaner_id as id
	, 'Cleaner' as type
	, action_radius
	, latitude
	, longtitude
	, billing_city as city
	, null as cleaner_count
	, null as customer_count
	, null Total_Demanded_Hours
	, null as Cleaner_Customer_Ratio
	, null as order_status
from main.radius_cleaner 
-- where cleaner_id in (
-- select cleaner_id
-- from main.radius_relation_Cleaner_Lead
-- where lead_id = '00Q2000000moPGHEA2')

union

select 
	  customer_id as id
	, 'Customer' as type
	, null as action_radius
	, latitude
	, longtitude
	, shipping_city as city
	, null as cleaner_count
	, null as customer_count
	, null Total_Demanded_Hours
	, null as Cleaner_Customer_Ratio
	, order_status as order_status
from main.radius_customer
-- where customer_id in (
-- select customer_id
-- from main.radius_relation_Lead_Customer
-- where lead_id = '00Q2000000moPGHEA2')
;