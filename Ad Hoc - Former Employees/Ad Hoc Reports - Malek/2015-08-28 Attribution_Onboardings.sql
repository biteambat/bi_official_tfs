-- select sum(costs)  from (

select
			  COALESCE(a.date,b.date) as date
			, COALESCE(a.locale,b.locale) as locale
			, COALESCE(a.leadsource,b.leadsource) as leadsource
			, COALESCE(a.Count_Onboardings,0) as Count_Onboardings
			, COALESCE(b.costs,0) as costs

from (
		select date, locale, leadsource, count(*) as Count_Onboardings
		from (
			select
					  cast(a.created_at as date) as date
					, upper(LEFT(locale,2)) as locale
					, case
							when b.new_lead_source = 'SEM' or b.new_lead_source = 'Brand' then 'Search'
							when b.new_lead_source = 'Landing Page' or b.new_lead_source = 'Newsletter' then 'Others'
							when b.new_lead_source = 'Outbound lead gen' then 'Outbound Lead gen'
							else b.new_lead_source
						end as leadsource
			from main.account a
				inner join main.lead_source_new b
				on a.cleaner_id = b.cleaner_id
				where a.is_test_account <> true
			) xy
			
			group by date, locale, leadsource
		) a

full join (
				select 
						  to_date(date, 'mm/dd/yyyy') as date
						, locale
						, leadsource
						, sum(costs) as costs
				from bi.leadcosts
				group by to_date(date, 'mm/dd/yyyy'), locale, leadsource
				) b
		on a.date = b.date
		and a.locale = b.locale
		and a.leadsource = b.leadsource
		
 -- ) fff  where date > '2015-05-01'
;