select v.*
from (
-- #1 demand h & demand h invoiced + nmp h & cancelled h for rate calculation
select 
		cast(date_trunc('week', order_start__c) as date) as first_day_of_cw
		, delivery_area
		, upper(left(locale__c,2)) as locale
		, professional__c as CleanerID
		, case when status not in ('CANCELLED CUSTOMER', 'CANCELLED CUSTOMER SHORTTERM', 'CANCELLED FAKED','CANCELLED MISTAKE', 'CANCELLED NOT THERE YET', 'CANCELLED PAYMENT', 'CANCELLED SKIPPED', 'CANCELLED TERMINATED')
					then order_duration__c else 0 end as demand_h
		, case when status = 'INVOICED'
					then order_duration__c else 0 end as demand_h_invoiced
		, null Total_Number_Active_Cleaner
		, null worked_hours
		, 0 as capacity_hours_min
		, 0 as capacity_hours_max
		, 0 as Cleaner_A_Count
		, 0 as Cleaner_B_Count
		, 0 as Cleaner_C_Count
		, CASE WHEN status in ('CANCELLED PROFESSIONAL', 'CANCELLED PROFESSIONAL SHORTTERM') THEN order_duration__c ELSE null END as cancelled_h
		, CASE WHEN status = 'CANCELLED NO MANPOWER'	THEN order_duration__c ELSE null END as nmp_h
		, CASE WHEN commuting_time__c > 0 THEN 1 ELSE null END as commuting_time_count
		, commuting_time__c as commuting_time
		, 0 as Cases_Count
		, null as Cleaner_Count
		, cast(null as numeric) as rating_service
		, cast(null as numeric) as rating_professional
		, 0 as holiday_hours
		, 0 as sickness_hours
		, cast(null as numeric) as min_hours_per_day
		, 1 as subquery
from bi.orders
where sfid <> '80120000000TD4zAAG'
		
UNION ALL

-- #2 (worked_hours & contract_hours or h_capacity) for UR%, h_capacity, #A-Cleaner, #B-Cleaner, #C-Cleaner
select 
		  first_day_of_cw
		, delivery_area
		, upper(left(delivery_area,2)) as locale
		, cleanerid as CleanerID
		, 0 as demand_h
		, 0 as demand_h_invoiced
		, cleanerid as Total_Number_Active_Cleaner
		, worked_hours
		, contract_hours as capacity_hours_min
		, availability_hours as capacity_hours_max
		, Cleaner_a
		, Cleaner_b
		, Cleaner_c
		, 0 as cancelled_h
		, 0 as nmp_h
		, 0 as commuting_time_count
		, 0 as commuting_time
		, 0 as Cases_Count
		, null as Cleaner_Count
		, cast(null as numeric) as rating_service
		, cast(null as numeric) as rating_professional
		, 0 as holiday_hours
		, 0 as sickness_hours
		, cast(null as numeric) as min_hours_per_day
		, 2 as subquery
from (
		select 
				  first_day_of_cw, l.delivery_areas__c as delivery_area, cleanerid, worked_hours, contract_hours, availability_hours
				, COALESCE((worked_hours)/(contract_hours),0) as ur
				, CASE WHEN COALESCE((worked_hours)/(contract_hours),0) > 0.9 THEN 1 ELSE null END as Cleaner_A
				, CASE WHEN COALESCE((worked_hours)/(contract_hours),0) between 0.5 and 0.9 THEN 1 ELSE null END as Cleaner_B
				, CASE WHEN COALESCE((worked_hours)/(contract_hours),0) < 0.5 THEN 1 ELSE null END as Cleaner_C
		from bi.utalization_per_cleaner_weekly k
		left join salesforce.account l
		on k.cleanerid = l.sfid
		where first_day_of_cw < contract_end and start_date <= first_day_of_cw
	) x

UNION ALL

-- #3 #cases
select
		  cast(date_trunc('week', createddate) as date) as first_day_of_cw
		, delivery_area
		, upper(left(delivery_area,2)) as locale
		, f.sfid as CleanerID
		, 0 as demand_h
		, 0 as demand_h_invoiced
		, null as Total_Number_Active_Cleaner
		, null worked_hours
		, 0 as capacity_hours_min
		, 0 as capacity_hours_max
		, 0 as Cleaner_A_Count
		, 0 as Cleaner_B_Count
		, 0 as Cleaner_C_Count
		, 0 as cancelled_h
		, 0 as nmp_h
		, 0 as commuting_time_count
		, 0 as commuting_time
		, 1 as Cases_Count
		, e.accountid as Cleaner_Count
		, cast(null as numeric) as rating_service
		, cast(null as numeric) as rating_professional
		, 0 as holiday_hours
		, 0 as sickness_hours
		, cast(null as numeric) as min_hours_per_day
		, 3 as subquery
from salesforce.case e
inner join (
				select sfid, delivery_areas__c as delivery_area
				from salesforce.account
				) f
	on e.accountid =f.sfid

UNION ALL

-- #4 rating service & rating professional
select
		  cast(date_trunc('week', effectivedate) as date) as first_day_of_cw
		, delivery_area
		, upper(left(locale__c, 2)) as locale
		, professional__c as CleanerID
		, 0 as demand_h
		, 0 as demand_h_invoiced
		, null as Total_Number_Active_Cleaner
		, null worked_hours
		, 0 as capacity_hours_min
		, 0 as capacity_hours_max
		, 0 as Cleaner_A_Count
		, 0 as Cleaner_B_Count
		, 0 as Cleaner_C_Count
		, 0 as cancelled_h
		, 0 as nmp_h
		, 0 as commuting_time_count
		, 0 as commuting_time
		, 0 as Cases_Count
		, null as Cleaner_Count
		, rating_service__c as rating_service
		, rating_professional__c as rating_professional
		, 0 as holiday_hours
		, 0 as sickness_hours
		, cast(null as numeric) as min_hours_per_day
		, 4 as subquery
from bi.orders
where rating_professional__c is not null or rating_service__c is not null

UNION ALL

-- #5 holiday & sickness hours
select
		  cast(date_trunc('week', xx.date1) as date) as first_day_of_cw
		, yy.delivery_area
		, upper(left(delivery_area,2)) as locale
		, xx.account__c as Cleanerid
		, 0 as demand_h
		, 0 as demand_h_invoiced
		, null as Total_Number_Active_Cleaner
		, null worked_hours
		, 0 as capacity_hours_min
		, 0 as capacity_hours_max
		, 0 as Cleaner_A_Count
		, 0 as Cleaner_B_Count
		, 0 as Cleaner_C_Count
		, 0 as cancelled_h
		, 0 as nmp_h
		, 0 as commuting_time_count
		, 0 as commuting_time
		, 0 as Cases_Count
		, null as Cleaner_Count
		, cast(null as numeric) as rating_service
		, cast(null as numeric) as rating_professional
		, holiday_flag * min_hours_per_day as holiday_hours
		, sickness_flag * min_hours_per_day as sickness_hours
		, min_hours_per_day
		, 5 as subquery
from (
		select date as date1, account__c, holiday_flag, sickness_flag
		from bi.holidays_cleaner_3
		) xx
inner join (
				select sfid, delivery_areas__c as delivery_area,  hr_contract_weekly_hours_min__c/5 as min_hours_per_day
				from salesforce.account
				) yy
	on xx.account__c = yy.sfid

) v
left join (
				select 
				sfid as cleanerid, type__c
				from salesforce.account
) w
on (v.Cleanerid = w.cleanerid and w.type__c = '60')