/*
 compares working city and delivery_area by splitting order count and gmv 
 by the area where both fields are the same ("inner circle"/efficiency radius)
 and the where working_city is defined but not the delivery area ("outer circle"/
 "working city-efficiency radius")
 */

select 
		  Date
		, working_city
		, area
		, order_status
		, order_type
		, count(*) as total_order_count
		, sum(gmv_eur) as gmv
	from (
			-- basic data
			select
				  sfid
				, city as working_city
				, delivery_area
				, effectivedate
				, to_char(effectivedate, 'YYYY-MM') as Date
				, status as order_status
				, case
					when delivery_area = lower(city) then 'efficiency radius'
					when delivery_area <> city or delivery_area is null then 'working city-efficiency radius'
					else null
					end as area
				, CASE
						WHEN acquisition_channel__c != 'recurrent' and acquisition_New_Customer__c = '1' THEN 'acquisition'
						WHEN acquisition_channel__c != 'recurrent' and acquisition_New_Customer__c = '0' THEN 'rebooking'
						WHEN acquisition_channel__c = 'recurrent'	THEN 'recurrent'
						ELSE NULL
						END AS order_type
				, gmv_eur
			from bi.orders
			where status in ('INVOICED', 'CANCELLED NO MANPOWER', 'CANCELLED NOT THERE YET')
			and effectivedate between '2015-10-01' and '2016-05-31'
				order by 5
  ) x1
group by Date
		, working_city
		, area
		, order_status
		, order_type
order by 1,2 asc
;