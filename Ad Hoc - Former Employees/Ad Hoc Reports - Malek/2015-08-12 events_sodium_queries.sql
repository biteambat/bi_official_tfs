

-- view
select created_at, professional_json::json->>'Id' as Cleaner_ID,  professional_json::json->>'Status__c' as Status
from events.sodium
	where event_name in (
								'Account Event:LEFT'
								,'Account Event:TERMINATED'
								,'Account Event:SUSPENDED')
;

-- by event group
select event_name, count(*)
from events.sodium
where event_name like 'Order Event%'
group by event_name
order by 1;


select Event_Type, min(created_at) as FirstDate, sum(Event_Count)
from (
		select event_name
				, case
						when event_name like 'Account%' 			then 'Account'
						when event_name like 'Customer%' 		then 'Customer'
						when event_name like 'MyJobs%' 			then 'MyJobs'
						when event_name like 'Order%'				then 'Order'
						when event_name like 'Offer%'				then 'Order'
						when event_name like 'Professional%'	then 'Professional'
						when event_name like 'Twilio%'			then 'Twilio'
						when event_name like 'Waiting%'			then 'Order'
						else 'ka'
						end as Event_Type				
				, min(cast(created_at as date)) as created_at
				, count(*) as Event_Count
		from events.sodium
		group by event_name
	) x
	group by Event_Type
	order by 2
;

select *
from events.sodium
where event_name in (
/*
'Order Event:CANCELLED CUSTOMER',
'Order Event:CANCELLED CUSTOMER SHORTTERM',
'Order Event:CANCELLED-FAKED',
'Order Event:CANCELLED-MISTAKE',
'Order Event:CANCELLED-NO-MANPOWER',
'Order Event:CANCELLED-NOT-THERE-YET',
'Order Event:CANCELLED-PAYMENT',
'Order Event:CANCELLED-PROFESSIONAL',
'Order Event:CANCELLED-PROFESSIONAL-SHORTTERM',
'Order Event:CANCELLED-SKIPPED',
'Order Event:CANCELLED TERMINATED'
*/

 'Order Event:NOSHOW-CUSTOMER'
,'Order Event:NOSHOW-PROFESSIONAL'
,'Order Event:PENDING TO START'
,'Order Event:PENDING TO START Push'
,'Order Event:RESCHEDULED'
,'Order Event:WAITING-CONFIRMATION'
,'Order Event:WAITING FOR ACCEPTANCE'
,'Order Event:WAITING-FOR-RESCHEDULE'

)
order by 2,3
;