select 
contact__c, customer_name__c
, count(*)
from salesforce.order
group by contact__c,customer_name__c  order by 3 desc
;

select x.contact__c, count(*)
from (
		select 
		contact__c,acquisition_new_customer__c
		,count(*)
		from salesforce.order
		group by contact__c,acquisition_new_customer__c
		) x
group by x.contact__c
having count(*) >1
order by 2 desc
;

-- acquisition_new_customer__c
select 
order_creation__c
, sfid as Order_ID
,order_id__c
,contact__c
,customer_email__c
,customer_id__c
,customer_name__c
,status
,customer_email__c
,allocation_attempts_last__c -- raus
,allocation_attempts__c
from salesforce.order
-- where contact__c = '0032000001HjEy5AAF'
order by order_creation__c desc
;

select
order_creation__c
,sfid
,status
,refunded_amount__c
,order_duration__c
,order_start__c
,order_time__c
,order_end__c
,order_creation__c
from salesforce.order
where grand_total_eur__c <> web_grand_total__c
order by 1 desc
;

-- zählt häufigkeiten eines attributs (z.b. Kundenemail) je Kundennummer in Order Tabelle
select contact__c, count(*)
from (
			select 
			contact__c,shippingcity
			,count(*)
			from salesforce.order
			group by contact__c,shippingcity
		)x
group by x.contact__c
having count(*)>1
order by 2 desc
;