

-- query from Sylvain

SELECT
 o.sfid,
 o.effectivedate,
 EXTRACT(week from o.effectivedate) as week,
 EXTRACT(month from o.effectivedate) as month,
 upper(left(o.polygon,2)) as country,
 o.polygon,
 o.status,
 o.order_duration__c,
 o.professional__c,
 a.name,
 a.status__c

FROM bi.orders o 

LEFT JOIN Salesforce.Account a

ON (o.professional__c = a.sfid)

WHERE o.test__c = '0'
 AND o.effectivedate between current_date-90 and current_date - cast(extract(dow from current_date) as int)
 AND o.polygon IS NOT NULL 
 AND o.status in ('INVOICED','PENDING TO START','CANCELLED NOT THERE YET','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL', 'CANCELLED CUSTOMER SHORT TERM', 'CANCELLED TERMINATED', 'CANCELLED SKIPPED')
 AND professional__c IS NOT NULL

GROUP BY o.sfid,
 o.effectivedate,
 upper(left(o.polygon,2)),
 o.polygon,
 o.status,
 o.order_duration__c,
 o.professional__c,
 EXTRACT(month from o.effectivedate),
 a.name,
 a.status__c
;