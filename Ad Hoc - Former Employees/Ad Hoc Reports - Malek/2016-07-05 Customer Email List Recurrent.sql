select distinct customer_name__c
		, customer_email__c
		,customer_language
from (
select 
		  order_creation__c
		, contact__c
		, customer_name__c
		, customer_email__c
		, effectivedate
		, status
		, right(locale__c, 2) as customer_language
		, rank() OVER (PARTITION BY contact__c ORDER BY effectivedate asc) as ranking
from bi.orders
where order_creation__c between '2016-06-24' and '2016-07-01'
and recurrency__c is not null
and recurrency__c <> 0
and billingcountry = 'Germany'
) y
where ranking > 1
and status not like 'CANCELLED%'