/*
	List of customers for sending a christmas greetings cards
*/
select customer_id, customer_email, customer_name, shipping_street, shipping_postal_code, shipping_city
from main.order
where is_new_customer is true
and customer_id in (

							select x.customer_id
							from (
									select customer_id, count(*)
									from main.order
									where order_start_date_utc between current_date-60 and current_date
									and order_status = 'INVOICED'
									and recurrency > 0
									and left(locale,2) = 'de'
									and is_test_order is false
									group by customer_id
									having count(*) >= 3
							) x
							where customer_id in 
														( 
														select distinct(customer_id) as customer_id
														from main.order
														where order_start_date_utc between current_date and current_date+60
														and recurrency > 0
														and order_status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','ALLOCATION AUTO','ALLOCATION PAUSED')
														and is_test_order is false
														and left(locale,2) = 'de'
														)
					)
					
;