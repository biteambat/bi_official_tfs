select
				effectivedate,
				sfid,
				delivery_area,
				efficiency_area,
				case
					when delivery_area = efficiency_area then 1
					else null end as Efficiency_Area_Count,
				case
					when delivery_area <> efficiency_area then 1
				else null
				end as Delivery_Area_Count
from bi.orders
where effectivedate > '2016-01-01'
and status = 'INVOICED'