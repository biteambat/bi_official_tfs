
-- select customer_id, count(*) from (

select distinct
		  d.customer_name
		, a.customer_id2 as customer_id
		, d.customer_email
		, b.Last_Order_Date
		, b.Order_Count
		, b.AVG_Rating_Professional
		, b.Recurrency
		, c.Count_Cancelled

from (
		select distinct customer_id2
		from main."order"
		where upper(left(locale,2)) = 'CH'
		and order_status = 'INVOICED'
	) a

left join  (
			select 
					  customer_id2
					,max(customer_name) as customer_name
					,max(customer_email) as customer_email
					from main."order"
					where order_status = 'INVOICED'
					group by customer_id2
			)d
	on a.customer_id2 = d.customer_id2

left join  (
			select 
					  customer_id2
					, max(Order_Start_Date_utc) as Last_Order_Date
					, count(*) as Order_Count
					, max(recurrency) as Recurrency
					, avg(rating_professional) as AVG_Rating_Professional
					from main."order"
					where order_status = 'INVOICED'
					group by customer_id2
			)b
	on a.customer_id2 = b.customer_id2

left join (
			select customer_id2,
					count(*) Count_Cancelled
			from main."order"
			where order_status = 'CANCELLED TERMINATED'
			group by customer_id2
		) c
	on a.customer_id2 = c.customer_id2


-- )x group by customer_id having count(*) > 1

;