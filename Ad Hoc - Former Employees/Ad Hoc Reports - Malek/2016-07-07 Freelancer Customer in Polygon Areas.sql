select
		  a.customer_id__c
		, a.shippingcountry
		, a.shippingcity
		, a.shippingpostalcode
		, a.customer_name__c
		, b.nb_invoiced_orders
		, b.nb_open_orders
		, a.order_start__c
		, to_char(a.order_start__c, 'day') as order_start_weekday
		, a.order_time__c
		, c.next_order_gmv
		, c.next_order_duration
		, c.next_order_recurrency
		, c.next_order_cleaner
		, c.next_order_type
		, c.next_order_status
		, c.payment_method
		, b.gmv_next_30d
from (
		select x.customer_id__c, shippingcountry, shippingcity, shippingpostalcode, customer_name__c, order_start__c, order_creation__c as order_time__c
		from bi.orders x
		inner join (				select customer_id__c, max(sfid) as sfid
										from bi.orders 										
										where type in ('1','2','3','4')
										and polygon is not null
										and shippingcountry = 'Germany'
										group by customer_id__c
										) y
					on x. sfid = y.sfid and x.customer_id__c = y.customer_id__c
		) a
	left join (
					select
							  customer_id__c
							, sum(case when status = 'INVOICED' then 1 else 0 end) as nb_invoiced_orders
							, sum(case when status like 'PENDING%' then 1 else 0 end) as nb_open_orders
							, sum(case when order_start__c between current_date and current_date+30 then gmv__c else 0 end) as gmv_next_30d
					from bi.orders
					group by customer_id__c
					) b
	on a.customer_id__c = b.customer_id__c
	inner join (
					select z.* from (
					select
							  customer_id__c
							, order_start__c
							, rank() OVER (PARTITION BY customer_id__c ORDER BY sfid asc) as ranking
							, gmv__c as next_order_gmv
							, order_duration__c as next_order_duration
							, recurrency__c as next_order_recurrency
							, professional__c as next_order_cleaner
							, type as next_order_type
							, status as next_order_status
							, payment_method__c as payment_method
					from bi.orders
						where type in ('1','2','3','4')
						and polygon is not null
						and shippingcountry = 'Germany'
						and order_start__c > '2016-07-07'
						) z where ranking = 1
	) c
	on a.customer_id__c = c.customer_id__c
;