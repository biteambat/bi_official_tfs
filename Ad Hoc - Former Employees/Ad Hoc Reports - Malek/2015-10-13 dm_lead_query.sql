﻿
select 
	  a.lead_id
	, created_at
	, is_converted
	, gender
	, birthdate
	, email
	, has_email_access
	, languages
	, nationality
	, phone_availability
	, has_cleaning_background
	, cleaning_experience
	, cleaning_experience2
	, employment_status
	, has_vehicle
	, has_tradelicense
	, has_backgroundcheck
	, has_working_permit
	, availability_daily
	, availability_weekly
	, working_city
	, city
	, country
	, mobile_device
	, acquisition_channel
	, acquisition_ref
	, lead_channel
	, lead_source	
	, new_lead_source
from main.lead a
inner join main.lead_source_new b
on a.lead_id = b.lead_id

where is_test_lead = false
and gender is not null
and gender <> 'N'
and gender <> ''
and languages <> ''
and phone_availability <> ''
and phone_availability is not null
and employment_status is not null
and employment_status <> ''
and cleaning_experience is not null
and cleaning_experience <> ''
and cleaning_experience2 is not null
and cleaning_experience2 <> ''
and availability_daily is not null
and availability_daily <> ''
and availability_weekly is not null
and availability_weekly <> ''

order by 2 asc

;