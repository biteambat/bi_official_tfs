-- 3. Mail to all customers in Munich that have orders between 21.12. - 03.01.

select sfid, order_id, name, email, upper(left(a.locale__c,2)) as locale, hasoptedoutofemail, b.order_count 
from salesforce.contact a

inner join (	-- count orders
				select customer_id, min(order_id) as order_id, count(*) as order_count
				from main.order
				where order_start_date_utc between '2015-12-21' and '2016-01-03'
				and order_status not like 'CANCELLED%'
				and (shipping_city = 'München' or shipping_city = 'Munich')
				group by customer_id 
				) b
		on a.sfid = b.customer_id
and upper(left(a.locale__c,2)) in ('DE')
;