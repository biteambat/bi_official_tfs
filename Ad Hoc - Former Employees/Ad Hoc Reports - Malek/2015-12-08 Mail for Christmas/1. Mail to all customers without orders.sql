
-- 1a
-- Segment: CH/DE customers, that booked in the past 2 months, but don't have a booking between 21.12. and 3.1., including Munich
select sfid, name, email, upper(left(a.locale__c,2)) as locale, hasoptedoutofemail, b.order_count 
from salesforce.contact a

inner join (	-- count orders
				select customer_id, count(*) as order_count
				from main.order
				where 
				(order_start_date_utc between current_date-60 and current_date-1 and order_status = 'INVOICED')
				or
				(order_start_date_utc between '2015-12-21' and '2016-01-03' and order_status not like 'CANCELLED%')
				group by customer_id 
				) b
		on a.sfid = b.customer_id

where sfid not in (
				select customer_id
				from main.order
				where order_start_date_utc between '2015-12-21' and '2016-01-03'
				and order_status not like 'CANCELLED%'
				)
and upper(left(a.locale__c,2)) in ('CH','DE')
;

-- 1b
-- Segment: NL customers, that booked in the past, but don't have a booking between 21.12. and 3.1.

select sfid, name, email, upper(left(a.locale__c,2)) as locale, hasoptedoutofemail, b.order_count 
from salesforce.contact a

inner join (	-- count orders
				select customer_id, count(*) as order_count
				from main.order
				where 
				(order_start_date_utc < current_date and order_status = 'INVOICED')
				or
				(order_start_date_utc between '2015-12-21' and '2016-01-03' and order_status not like 'CANCELLED%')
				group by customer_id 
				) b
		on a.sfid = b.customer_id

where sfid not in (
				select customer_id
				from main.order
				where order_start_date_utc between '2015-12-21' and '2016-01-03'
				and order_status not like 'CANCELLED%'
				)
and upper(left(a.locale__c,2)) = 'NL'