From Original Mail:

1. Mail to all customers without orders

1a: CH/DE
Segment: CH/DE customers, that booked in the past 2 months, but don't have a booking between 21.12. and 3.1., including Munich.

Languages: DE/EN/DE-CH (and FR?)

Content:

- CM Opening hours during Christmas holidays
- Please be aware that less cleaners may be available during the holidays if you plan to do a booking

1b: NL
Segment: NL customers, that booked in the past, but don't have a booking between 21.12. and 3.1.

Languages: NL

Content:

- Please be aware that less cleaners may be available during the holidays if you plan to do a booking



2. Mail to all customers with orders between 21.12. - 03.01.,without Munich

2a: CH/DE
Segment: CH/DE customers, that have bookings between 21.12.-03.01., without Munich customers with bookings

Languages: DE/EN/DE-CH (and FR?)

Content:

- CM Opening hours during Christmas holidays (except for the NL versions, opening hours will be the same)
- Please check your bookings (we will include a link to the individual booking management page as a button?) during the holidays and skip them if you won't be there.

2b: NL

Segment: NL customers, that have bookings between 21.12.-03.01., without Munich customers with bookings

Languages: NL

Content:

- Please check your bookings (we will include a link to the individual booking management page as a button?) during the holidays and skip them if you won't be there.

3. Mail to all customers in Munich that have orders between 21.12. - 03.01.

Segment: CH/NL/DE csutomers, that have bookings between 21.12.-03.01.,

Languages: DE

Content:

- CM Opening hours during Christmas holidays
- Please check your bookings (we will include a link to the individual booking management page as a button?) during the holidays and skip them if you won't be there.
- Please be aware that less cleaners may be available during the holidays if you plan to do a booking and that you won't be able to book on 25.12., 26.12. and 01.01.16 and that we will cancel appointments on those days.