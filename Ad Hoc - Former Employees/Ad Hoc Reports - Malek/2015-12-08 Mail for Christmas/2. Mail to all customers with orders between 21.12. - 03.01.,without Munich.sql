-- 2. Mail to all customers with orders between 21.12. - 03.01.,without Munich

-- 2a: CH/DE
-- Segment: CH/DE customers, that have bookings between 21.12.-03.01., without Munich customers with bookings

select sfid, order_id, name, email, upper(left(a.locale__c,2)) as locale, hasoptedoutofemail, b.order_count 
from salesforce.contact a

inner join (	-- count orders
				select customer_id, min(order_id) as order_id, count(*) as order_count
				from main.order
				where order_start_date_utc between '2015-12-21' and '2016-01-03'
				and order_status not like 'CANCELLED%'
				and left(shipping_postal_code,1) <> '8'
				group by customer_id 
				) b
		on a.sfid = b.customer_id

and upper(left(a.locale__c,2)) in ('DE')
;

-- 2b: NL
-- Segment: NL customers, that have bookings between 21.12.-03.01., without Munich customers with bookings

select sfid, order_id, name, email, upper(left(a.locale__c,2)) as locale, hasoptedoutofemail, b.order_count 
from salesforce.contact a

inner join (	-- count orders
				select customer_id, min(order_id) as order_id, count(*) as order_count
				from main.order
				where order_start_date_utc between '2015-12-21' and '2016-01-03'
				and order_status not like 'CANCELLED%'
				group by customer_id 
				) b
		on a.sfid = b.customer_id
and upper(left(a.locale__c,2)) = 'NL'