select *
from main.account_history
where Changed_Field =  'Status__c';

select distinct(cleaner_id)
from main.account_history
where Changed_Field =  'Status__c';

-- Übersicht aller möglichen Statusänderungen die Terminated oder Left enthalten
select Old_Value, New_Value, count(*)
from main.account_history
where Changed_Field =  'Status__c'
	and Old_Value in ('TERMINATED', 'LEFT')
	or New_Value in ('TERMINATED', 'LEFT')
group by Old_Value, New_Value
order by 1,2;


select *
from (
	select
			  x.created_at
			, x.cleaner_id
			, case
					when old_status = 'NOT_ACTIVE' and new_status = 'ACTIVE'	then 'REACTIVATED'
					when old_status = 'ACTIVE' and new_status = 'NOT_ACTIVE'	then 'CHURNED'
				end as test
	from (
			select 	
					  created_at
					, cleaner_id
					, old_value
					, case
							when old_value = 'ACTIVE' or old_value = 'BETA' then 'ACTIVE' 
							when old_value = 'LEFT' or old_value = 'TERMINATED' then 'NOT_ACTIVE'
						end as old_status
					, new_value				
					, case
							when new_Value = 'ACTIVE' or new_Value = 'BETA' then 'ACTIVE' 
							when new_Value = 'LEFT' or new_Value = 'TERMINATED' then 'NOT_ACTIVE'
						end as new_status
			from main.account_history
			where Changed_Field =  'Status__c'
		) x
) y
where y.test is not null
;


-- reactivation
select cast(created_at as date) as created_at, cleaner_id, 'Reactivation' as Change
from main.account_history
where old_value in ('LEFT','TERMINATED')
and new_value not in  ('LIMBO','LEFT','TERMINATED','SUSPENDED')
union

-- churn
select cast(created_at as date) as created_at, cleaner_id, 'Churn' as Change
from main.account_history
where new_value in ('LEFT','TERMINATED')