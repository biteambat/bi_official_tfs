
select sfid
		, concat('https://emea.salesforce.com/', sfid) as SF_Link
		
from salesforce.account
where on_babysitting__c is true
;

-- anzahl jobs invoiced, last30days
select professional__c, count(*) as Jobs_Invoiced_L30
from salesforce.order

where createddate between createddate- interval '30 days' and now()
and status = 'INVOICED' 

group by professional__c