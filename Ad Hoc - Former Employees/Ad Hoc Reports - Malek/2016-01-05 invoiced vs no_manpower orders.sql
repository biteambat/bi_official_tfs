select
		  x.postal_code
		, x.country
		, sum(No_Manpower_Orders) as No_Manpower_Orders
		, sum(Invoiced_Orders) as Invoiced_Orders
from (
			select 
					  order_start_date_utc
					, order_id
					, order_status
					, left(billing_postal_code,2) as postal_code
					, upper(left(locale,2)) as country
					, case when order_status = 'CANCELLED NO MANPOWER' then 1 else 0 end as No_Manpower_Orders
					, case when order_status = 'INVOICED' then 1 else 0 end as Invoiced_Orders
			from main."order"
			where order_start_date_utc between '2015-12-01' and '2015-12-31'
			and (order_status = 'CANCELLED NO MANPOWER' or order_status = 'INVOICED')
	) x
	
group by x.postal_code, x.country