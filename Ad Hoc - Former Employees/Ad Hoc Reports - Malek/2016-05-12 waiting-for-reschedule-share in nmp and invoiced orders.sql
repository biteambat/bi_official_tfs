
select x.*, case when y.order_id is null then false else true end as has_waiting_status
from (
			select created_at, order_id, order_status
			from main.order
			where order_status in ('INVOICED', 'CANCELLED NO MANPOWER')
			and created_at between '2016-01-01' and '2016-05-12'
			and is_test_order is false
			) x
		left join (
						select order_Json->>'Id' as order_id
						from events.sodium x
						where event_name = 'Order Event:WAITING-FOR-RESCHEDULE'
						) y
		on x.order_id = y.order_id
		
		