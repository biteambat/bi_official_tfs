select sfid as customer_id, email, name, locale__c
from salesforce.contact
where sfid in (
					select distinct customer_id
					from main.order
					
					where customer_id in (
											-- nur Kunden in 3 Städten (siehe unten)
											select contact__c
											from bi.orders
											where city in ('DE-Cologne', 'DE-Frankfurt am Main', 'DE-Nuremberg')
											)
					
					and customer_id in (
												-- nur kunden mit 2 oder mehr invoiced orders
												select customer_id
												from main.order
												where order_status = 'INVOICED'
												and (recurrency = 0 or recurrency is null)
												group by customer_id
												having count(*) > 2
												)
					and customer_id not in (
														-- keine kunden mit offenen Orders
														select distinct customer_id
														from main.order
														where order_start_date_utc > now()
														and customer_id is not null
														)									
					and customer_id in (
												-- ausschluss von kunden die sich von newsletter ausgetragen haben
												select sfid
												from salesforce.contact
												where left(locale__c,2) = 'de'
												and hasoptedoutofemail = false
												)
					
					)