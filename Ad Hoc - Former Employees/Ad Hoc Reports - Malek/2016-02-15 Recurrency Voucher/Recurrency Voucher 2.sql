
select sfid as customer_id, email, name, locale__c
from salesforce.contact
where sfid in (
						
						select distinct customer_id
						from main.order
						
						where customer_id in (
												-- nur Kunden in 4 Städten (siehe unten)
												select contact__c
												from bi.orders
												where city in ('DE-Cologne', 'DE-Frankfurt am Main', 'DE-Nuremberg', 'DE-Hamburg')
												)
						
						and customer_id in (
													-- kunden mit mehr als 2 invoiced orders
													select customer_id
													from main.order
													where order_status = 'INVOICED'
													group by customer_id
													having count(*) > 1
													)
						and customer_id not in (
															-- kunden mit offenen Orders
															select distinct customer_id
															from main.order
															where order_start_date_utc > now()
															and customer_id is not null
															)									
						and customer_id in (
													-- ausschluss von kunden die sich von newsletter ausgetragen haben
													select sfid
													from salesforce.contact
													where left(locale__c,2) = 'de'
													and hasoptedoutofemail = false
													)
)