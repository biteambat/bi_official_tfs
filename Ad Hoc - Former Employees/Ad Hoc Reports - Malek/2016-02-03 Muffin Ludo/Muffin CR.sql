-- Querys for Conversion Rate Calculation for Muffin Cleaner
/*
select cast(created_at as date), lead_id, converted_date, 
from main.lead
;*/

-- CR allgemein
select cast(created_at as date) as date, lead_id as id, working_city, upper(left(locale,2)) as locale, new_lead_source, 1 as lead_count, null as cleaner_count
from main.lead
where lead_id in (
select lead_id
from main.lead
where 
	(working_city in ('Munich', 'DE-Munich') and created_at >= '2015-11-03')
	or
	(working_city in ('Cologne', 'DE-Cologne') and created_at >= '2015-11-24')
	or
	(working_city in ('DE- am Main','DE-Mönchengladbach','DE-Bonn','DE-Bonn+','DE-Dortmund','DE-Dortmund+',
							'DE-Duisburg','DE-Duisburg+','DE-Dusseldorf','DE-Dusseldorf+','DE-Essen','DE-Essen+','DE-Frankfurt am Main+',
							'DE-Hamburg','DE-Hamburg+','DE-Mainz','DE-Mainz+','DE-Mannheim','DE-Mannheim+','DE-Mönchengladbach+',
							'DE-Nuremberg','DE-Nuremberg+','DE-Stuttgart','DE-Stuttgart+','DE-Wuppertal','Dusseldorf','Stuttgart +',
							'Bonn','Dortmund','Duisburg','Essen','Frankfurt am Main','Hamburg','Mainz','Mannheim','Mönchengladbach',
							'Nuremberg','Stuttgart','Wuppertal') and created_at >= '2015-12-25')

UNION

select cast(created_at as date) as date, a.cleaner_id as id, working_city, upper(left(locale,2)) as locale, new_lead_source, null as lead_count, 1 as cleaner_count
from main.account a
left join (select cleaner_id, new_lead_source from main.lead) l
on a.cleaner_id = l.cleaner_id
where cleaner_type like '%60%'
;

-- 
-- CR Ludo 
-- 
select
 		  cast(a."CreatedDate" as date) as creation_date
		, cast(d.hr_contract_start as date) as contract_start_date
		, "LeadId"
		, b.cleaner_id
		, b.working_city as working_city
		, "Field"
		, case	
				when b.working_city not like 'DE%' then 'DE-'||b.working_city
				when b.working_city like '%+' then trim(trailing '+' from b.working_city)
		else b.working_city
		end as working_city_grouped
		, b.locale
		, b.lead_source
		, b.lead_source_comment
		, c."Name" as Leadowner
		, case when "NewValue" in ('Avoid', 'Documents before contract','Interview to be scheduled', 'Interview scheduled', 'Never reached', 'Rejected') or "Field" = 'leadConverted' then "LeadId" else null end as "Lead Contacted" -- in Tableau renamed to "Leads after FCO"
		, case when "NewValue" not in  ('Never reached') 			then "LeadId" else null end as "Leads reached"
		, case when "NewValue" in ('Documents before contract') 	then "LeadId" else null end as "Documents before contract"
		, case when "NewValue" in ('Interview scheduled') 			then "LeadId" else null end as "Interview scheduled"
		, case when "Field" = 'leadConverted' then "LeadId" else null end as "Contact/Account created"
		, case when "Field" = 'created' then "LeadId" else null end as "Leads created"
from bi."LeadHistory" a
inner join (
				select lead_id, cleaner_id, working_city, upper(left(locale,2)) as locale, lead_source, lead_source_comment, is_test_lead
						from main.lead
						where lead_id in (
												select distinct "LeadId"
												from bi."LeadHistory" x
												inner join main.lead y on x."LeadId" = y.lead_id 
												where "Field" in ('Status','created','leadConverted')
												and (							
														(working_city in ('Munich', 'DE-Munich') and "CreatedDate" >= '2015-11-03')
														or
														(working_city in ('Cologne', 'DE-Cologne') and "CreatedDate" >= '2015-11-24')
														or
														(working_city in ('Berlin', 'DE-Berlin') and "CreatedDate" >= '2016-02-08')
														or
														(working_city in ('DE-Frankfurt am Main','DE-Mönchengladbach','DE-Bonn','DE-Bonn+','DE-Dortmund','DE-Dortmund+',
																				'DE-Duisburg','DE-Duisburg+','DE-Dusseldorf','DE-Dusseldorf+','DE-Essen','DE-Essen+','DE-Frankfurt am Main+',
																				'DE-Hamburg','DE-Hamburg+','DE-Mainz','DE-Mainz+','DE-Mannheim','DE-Mannheim+','DE-Mönchengladbach+',
																				'DE-Nuremberg','DE-Nuremberg+','DE-Stuttgart','DE-Stuttgart+','DE-Wuppertal','Dusseldorf','Stuttgart +',
																				'Bonn','Dortmund','Duisburg','Essen','Frankfurt am Main','Hamburg','Mainz','Mannheim','Mönchengladbach',
																				'Nuremberg','Stuttgart','Wuppertal') and "CreatedDate" >= '2015-12-25')
														)
												)
				) b
			on a."LeadId" = b.lead_id

left join bi."User" c
on a."CreatedById" = c."Id"

left join main.account d
on b.cleaner_id = d.cleaner_id

where ("Field" = 'Status'
		and "NewValue" in ('Avoid', 'Documents before contract','Interview to be scheduled', 'Interview scheduled', 'Never reached', 'Rejected')) or
		"Field" = 'created' or "Field" = 'leadConverted'
;

