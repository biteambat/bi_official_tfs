select 
			  createddate
			, sfid as order_id
			, contact__c as customer_id
			, marketing_channel
			, case when marketing_channel in ('SEM Brand', 'SEO Brand', 'DTI') then 'Brand_Marketing'
				else 'Others'
				end as marketing_channel_2
			, status
			, case when to_char(createddate, 'yyyy-mm') in ('2015-11', '2016-01') then 'yes'
					 when to_char(createddate, 'yyyy-mm') in ('2015-12', '2016-02') then 'no'					 
					 else null
					 end as tv_advertising
			, locale__c
			, city
from bi.orders
where createddate between '2015-11-01' and '2016-02-28'
and status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
and acquisition_new_customer__c is true
and left(locale__c,2) = 'de'
;