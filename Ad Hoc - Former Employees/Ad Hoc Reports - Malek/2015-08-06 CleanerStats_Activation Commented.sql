-- DROP TABLE IF EXISTS bi.cleanerreceivedsms;
-- CREATE TABLE bi.cleanerreceivedsms as 
	-- extrahiert cleaner_id aus professional jason + eventname (gefilter auf 3 typen)
	-- Granularität: mehrere Cleaner, 3 Typen von Events
SELECT
	replace(CAST(professional_json->'Id' as varchar),'"', '') AS professional,
	event_name
FROM
	events.sodium
WHERE
	created_at::date >= '2015-06-14'
	and event_name in ('Offer-SMS','Offer-Push','Order Action:ACCEPT Taken');

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- DROP TABLE IF EXISTS bi.smspercleaner2;
-- CREATE TABLE bi.smspercleaner2	as
	-- fasst die Tabelle oben auf Cleander-ID Ebene zusammen udn zählt bestimmte Events
SELECT
	professional,
	SUM(CASE WHEN event_name in ('Offer-SMS','Offer-Push') THEN 1 ELSE 0 END) as Offer_SMS,
	SUM(CASE WHEN event_name in ('Order Action:ACCEPT Taken') THEN 1 ELSE 0 END) as Accept_SMS
FROM
	 bi.cleanerreceivedsms
GROUP BY
	professional;
	
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- DROP TABLE IF EXISTS bi.cleaners_with_jobs;
-- CREATE TABLE bi.cleaners_with_jobs as 
	-- Account xx Orders, aufsummiert nach Cleander_id, summe von 2 Orderarten + einige Attribute aus Account
SELECT
	t1.sfid,
	t1.billingpostalcode,
	t1.working_city__c,
	t1.name,
	rating,
	ending_reason__c,
	SUM(CASE WHEN Status = 'INVOICED' THEN 1 ELSE 0 END) as InvoicedOrder_L30,
	SUM(CASE WHEN Status = 'PENDING TO START' THEN 1 ELSE 0 END) as PENDINGTOSTART_L30,
	t1.status__c,
	t1.LastActivitydate,
	t1.in_test_period__c,
	COUNT(DISTINCT(Order_Id__c)) as Orders
FROM
	salesforce.account t1
LEFT JOIN
  Salesforce.Order t2
ON
	(t1.sfid = t2.professional__c and t1.status__c not in ('LEFT','TERMINATED') and CAST(Effectivedate as date) > cast((cast(crunchdate as date) - interval '2 days') as date) - interval '30 days')
WHERE
	t1.status__c not in ('LEFT','TERMINATED') 
GROUP BY
	t1.sfid,
	t1.billingpostalcode,
	t1.working_city__c,
	t1.name,
	rating,
	t1.ending_reason__c,
	t1.status__c,
	t1.in_test_period__c,
	t1.LastActivitydate;

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- DROP TABLE IF EXISTS bi.cleanerstats;
-- CREATE TABLE bi.cleanerstats as
	-- nimmt Tabellen oben und joint diese mit 
SELECT
	*
FROM
	bi.cleaners_with_jobs t1
LEFT JOIN
	bi.smspercleaner2 t2
ON
	(t1.sfid = t2.professional);

-- DROP TABLE IF EXISTS bi.cleanerreceivedsms;
-- DROP TABLE IF EXISTS bi.smspercleaner2;
-- DROP TABLE IF EXISTS bi.cleaners_with_jobs;