

select distinct "Field"
from bi."LeadHistory" 
;

select distinct cast("CreatedDate" as date)
from bi."LeadHistory" 
order by 1
;

select "CreatedById", count(*)
from bi."LeadHistory" 
group by "CreatedById"

;

select	
		*
	from (
			select "CreatedById", count(*)
			from bi."LeadHistory" 
			where "Field" = 'created'
			group by "CreatedById"
						) a

	left join bi."User" b
		on a."CreatedById" = b."Id"
					
;

select *
from bi."User"