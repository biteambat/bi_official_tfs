
/* 
-- Eventname List & Anzahl
select distinct(event_name), count(*)
from bi.events_sodium_20150703
group by event_name
order by 1

-- bestimmte events (siehe where) und deren Anzahl pro Monat
select to_char(created_at,'YYYY-MM'), count(*)
from bi.events_sodium_20150703
where event_name = 'Account Event:LEFT'
or event_name = 'Account Event:TERMINATED'
group by to_char(created_at,'YYYY-MM')
order by 1
*/

-- anzahl von lead, neu-accounts & accountlöschungen nach Monat
select startdate from var;
select m1.Month, cleaner_lead, cleaner_onboarded, cleaner_terminated
	from (
			-- neue accounts
			select to_char(createddate,'YYYY-MM') as Month, count(*) as Cleaner_Onboarded
				from salesforce.account
				group by  to_char(createddate,'YYYY-MM')
				) m1

	left join (
			-- accountlöschungen nach monat
			select to_char(created_at,'YYYY-MM') as Month, count(*) as Cleaner_Terminated
				from bi.events_sodium_20150703
				where event_name = 'Account Event:LEFT'
				or event_name = 'Account Event:TERMINATED'
				group by to_char(created_at,'YYYY-MM')
			) m2
		on m1.Month = m2.Month
		
	left join (
			-- anzahl leads
			select to_char(createddate,'YYYY-WW') as Month, count(*) as Cleaner_Lead
				from salesforce.lead
				group by to_char(createddate,'YYYY-WW')
			) m3
		on m1.Month = m3.Month