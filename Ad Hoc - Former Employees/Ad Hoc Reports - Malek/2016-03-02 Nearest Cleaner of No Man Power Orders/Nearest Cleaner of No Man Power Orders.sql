﻿/*
	takes all no-man-power order
	joins the nearest cleaner
*/
select order_start_date_utc as effective_date, customer_id, cleaner_id, case when distance is null then '99999' else distance end as distance
from (
	select order_start_date_utc, customer_id, customer_location, shipping_street, shipping_postal_code, shipping_city,

		cleaner_id, cleaner_location, billing_street, billing_postal_code, billing_city,

		distance, ST_MakeLine(customer_location, cleaner_location) as distance_line,

		rank() OVER (PARTITION BY order_id ORDER BY distance ASC)
	from (

		select a.*, b.*, ST_Distance_Sphere(b.cleaner_location, a.customer_location) as distance
		from (
			select 
				  x.customer_id
				, x.order_start_date_utc
				, x.order_id
				, ST_SetSRID(ST_Point(shipping_longitude, shipping_latitude),4326) as customer_location
				, order_duration
				-- zum Testen:
				, shipping_street
				, shipping_postal_code
				, shipping_city
				, Point(shipping_longitude, shipping_latitude) as customer_point
				, shipping_longitude as Longtitude
				, shipping_latitude as Latitude
			from main.order x
			inner join (
					-- alle no man power des jahres 2016
					select customer_id, max(order_id2) as order_id2
					from main.order
					where is_test_order = false
					and order_status = 'CANCELLED NO MANPOWER'
					and created_at > '2016-01-06'
					group by customer_id
					) y
			on x.customer_id = y.customer_id
			and x.order_id2 = y.order_id2
			) a
			
		left join main.radius_cleaner b
			ON ST_DWithin(b.cleaner_location::geography, a.customer_location::geography,20000)
		) z
) asdf 
where rank =1
;