/*
		+++ Stammkunden die Oster Karte erhalten sollen +++
für eine Marketing Aktion brauchen wir eine Liste mit allen Kunden, 
welche mindestens 3 Recurrent Buchungen in den letzten 2 Monaten hatten
und in der Zukunft auch noch welche offen haben.
Ziel der Marketing Aktion ist es eine Weihnachtskarte an unsere Stammkunden zu schicken. 
*/

select a.customer_id , customer_email, customer_name, shipping_street, shipping_postal_code, shipping_city
from main.order a
inner join (
				select max(order_id) as order_id, customer_id from main.order
				group by customer_id
				) b
on a.order_id = b.order_id and a.customer_id = b.customer_id

where a.customer_id in(
							-- Kunden die mindestens 3 recurrent buchungen in den letzten 2 Monaten hatten
							select customer_id
							from main.order
							where recurrency > 0
							and order_status = 'INVOICED'
							and order_start_date_utc between '2015-12-29' and '2016-02-23'
							and is_test_order is false
							and left(locale,2) = 'de'
							group by customer_id
							having count(*) >= 3
						)
and a.customer_id in (
							-- Kunden die in zukunft offene recurrent order haben
							select distinct customer_id -- , recurrency, order_start_date_utc
							from main.order
							where recurrency > 0
							and order_start_date_utc > '2016-02-23'
							and is_test_order is false
							and left(locale,2) = 'de'
							and customer_id is not null
						)
and a.customer_id in (
							-- ausschluss von kunden die sich von newsletter ausgetragen haben
							select sfid
							from salesforce.contact
							where left(locale__c,2) = 'de'
							and hasoptedoutofemail = false
							)