﻿sd-- relation Cleaner <-> Other_Customer
DROP TABLE IF EXISTS main.z_radius_relation_Cleaner_other_Customer;
CREATE TABLE main.z_radius_relation_Cleaner_other_Customer as
SELECT
	  a.cleaner_id
	, b.other_customer_id
	, ST_Distance_Sphere(a.cleaner_location, b.customer_location) as distance
	FROM main.z_radius_cleaner a
	left join main.z_radius_other_customer b
		ON ST_DWithin(a.cleaner_location::geography, b.customer_location::geography, a.action_radius*1000)
;

-- relation target_customer <-> other_Customer
DROP TABLE IF EXISTS main.z_radius_relation_target_Customer_other_Customer;
CREATE TABLE main.z_radius_relation_target_Customer_other_Customer as
select 
	  c.target_customer_id
	, d.other_customer_id
	, ST_Distance_Sphere(c.customer_location, d.customer_location) as distance
	from main.z_radius_target_customer c
	left join main.z_radius_other_customer d
	on ST_DWithin(c.customer_location::geography, d.customer_location::geography, c.action_radius*1000)
;

-- relation Cleaner <-> target_Customer
DROP TABLE IF EXISTS main.z_radius_relation_Cleaner_target_Customer;
CREATE TABLE main.z_radius_relation_Cleaner_target_Customer as

select distinct e.target_customer_id, f.cleaner_id
from main.z_radius_relation_target_Customer_other_Customer e
inner join main.z_radius_relation_Cleaner_other_Customer f
on e.other_customer_id = f.other_customer_id