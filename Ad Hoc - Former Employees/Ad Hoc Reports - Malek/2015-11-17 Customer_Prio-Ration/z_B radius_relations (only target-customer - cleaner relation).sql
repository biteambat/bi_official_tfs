﻿-- relation Cleaner <-> target_Customer
DROP TABLE IF EXISTS main.z_radius_relation_Cleaner_target_Customer;
CREATE TABLE main.z_radius_relation_Cleaner_target_Customer as

select * from (
select 
	  a.target_customer_id
	, b.cleaner_id
	, cast(ST_Distance_Sphere(a.customer_location, b.cleaner_location) as integer) as distance_m
	, ST_MakeLine(a.customer_location, b.cleaner_location) as distance_line
	from main.z_radius_target_customer a
	left join main.z_radius_cleaner b
	on ST_DWithin(ST_Transform(a.customer_location,2100),ST_Transform(b.cleaner_location,2100), b.action_radius*1000)
	) x where cleaner_id is not null
;


