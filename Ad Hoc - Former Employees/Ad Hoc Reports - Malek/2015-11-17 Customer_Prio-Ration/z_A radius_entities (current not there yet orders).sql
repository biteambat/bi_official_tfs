﻿-- Cleaner Radius --
-- only Cleaner who did (and finished -> "INVOICED") a job in the last 30 days AND have the status active or beta
-- no testorders
DROP TABLE IF EXISTS main.z_radius_cleaner;
CREATE TABLE main.z_radius_cleaner as

select 
	  a.cleaner_id
	, ST_SetSRID(ST_Point(location_longitude, location_latitude),4326) as cleaner_location
	, case
			when effective_radius is null and has_vehicle is false
			then 5
			when effective_radius is null and has_vehicle is true
			then 5
		else 5
		end as action_radius
	-- zum testen:
	, billing_street
	, billing_postal_code
	, billing_city
	, Point(location_longitude, location_latitude) as cleaner_point
	, location_longitude as Longtitude
	, location_latitude as Latitude
from main.account a
inner join (
		select distinct cleaner_id
		from main.order
		where cast(created_at as date) > current_date-30
		and order_status = 'INVOICED'
		and cleaner_id <> ''
		and is_test_order = false
	) b
on a.cleaner_id = b.cleaner_id
where status in ('ACTIVE','BETA')
;

-- Target Customer Radius
-- only customer orders with status "No manpower
DROP TABLE IF EXISTS main.z_radius_target_customer;
CREATE TABLE main.z_radius_target_customer as

select 
	  a.customer_id as target_customer_id
	, a.order_id
	, ST_SetSRID(ST_Point(shipping_longitude, shipping_latitude),4326) as customer_location
	, 5 as action_radius
	, order_duration
	-- zum Testen:
	, shipping_street
	, shipping_postal_code
	, shipping_city
	, Point(shipping_longitude, shipping_latitude) as customer_point
	, shipping_longitude as Longtitude
	, shipping_latitude as Latitude
	, order_status
from main.order a
inner join (
		select customer_id, max(order_id2) as order_id2
		from main.order
		where order_status = 'CANCELLED NOT THERE YET'
		and cast(created_at as date) between current_date-90 and current_date - 1
		-- no manpower conditions
		-- where cast(created_at as date) between current_date-30 and current_date
		-- and order_status = 'CANCELLED NO MANPOWER'
		and is_test_order = false
		group by customer_id
		) b
on a.customer_id = b.customer_id
and a.order_id2 = b.order_id2
;

-- Other Customer Radius --
-- Customer who placed a order in the last 30 days, if a customer placed more than one order the latest has been taken
-- Orders mit Status "Cancelled Terminated" werden ausgeschlossen, da es sich um autom. generierte Folgebestellungen handelt
-- ebenfalls ausgeschlossen: Testorder, fake, mistake und:
-- no manpower da target customer
-- ebenfalls ausgeschlossen werden alle Customer_IDs die sich in target_customer befinden
DROP TABLE IF EXISTS main.z_radius_other_customer;
CREATE TABLE main.z_radius_other_customer as

select 	
	  a.customer_id as other_customer_id
	, a.order_id
	, ST_SetSRID(ST_Point(shipping_longitude, shipping_latitude),4326) as customer_location
	, 5 as action_radius
	, order_duration
	-- zum Testen:
	, shipping_street
	, shipping_postal_code
	, shipping_city
	, Point(shipping_longitude, shipping_latitude) as customer_point
	, shipping_longitude as Longtitude
	, shipping_latitude as Latitude
	, order_status
from main.order a
inner join (
		select customer_id, max(order_id2) as order_id2
		from main.order
		where cast(created_at as date) between current_date-30 and current_date
		and order_status not in ('CANCELLED TERMINATED', 'CANCELLED FAKED','CANCELLED MISTAKE', 'CANCELLED NOT THERE YET')
		and is_test_order = false
		and customer_id not in (select target_customer_id from main.z_radius_target_customer)
		group by customer_id
		) b
on a.customer_id = b.customer_id
and a.order_id2 = b.order_id2
;