﻿-- TARGET CUSTOMER
select 
		  a.target_customer_id as id
		, 'Target Customer' as type
		, action_radius
		, a.latitude
		, a.longtitude
		, a.shipping_street as street
		, a.shipping_postal_code as postal_code
		, a.shipping_city as city
		, COALESCE(b.cleaner_count,0) as cleaner_count
		, a.order_status as order_status
			
from main.z_radius_target_customer a
	left join (	
			select target_customer_id, count(cleaner_id) as Cleaner_Count
			from main.z_radius_relation_cleaner_target_customer
			group by target_customer_id
		) b
		on a.target_customer_id = b.target_customer_id

union 

select
	  cleaner_id as id
	, 'Cleaner' as type
	, action_radius
	, latitude
	, longtitude
	, billing_street as street
	, billing_postal_code as postal_code
	, billing_city as city
	, null as cleaner_count
	, null as order_status
from main.z_radius_cleaner 

