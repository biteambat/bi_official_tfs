﻿
-- cleaner/customer ratio:
			--  if > 1 = more customer than cleaner
			--  if < 1 = more cleaner than customer
select 
		  created_at
		, a.lead_id
		, action_radius
		, a.street
		, a.postal_code
		, a.city
		, COALESCE(b.cleaner_count,0) as cleaner_count
		, COALESCE(c.customer_count,0) as customer_count
		, c.Total_Demanded_Hours
		, cast(COALESCE(b.cleaner_count::numeric/c.customer_count::numeric,0) as numeric(7,2)) as Cleaner_Customer_Ratio
		, a.lead_point
			
from main.radius_lead a

left join (	
		select lead_id, count(cleaner_id) as Cleaner_Count
		from main.radius_relation_cleaner_lead
		group by lead_id
	) b
	on a.lead_id = b.lead_id

left join (
		select c.lead_id, count(c.customer_id) as Customer_Count, sum(d.order_duration) as Total_Demanded_Hours
		from main.radius_relation_lead_customer c
		left join main.radius_customer d
		on c.customer_id = d.customer_id
		group by lead_id
	) c
	on a.lead_id = c.lead_id

-- where a.lead_id = '00Q2000000moPGHEA2'

order by 1