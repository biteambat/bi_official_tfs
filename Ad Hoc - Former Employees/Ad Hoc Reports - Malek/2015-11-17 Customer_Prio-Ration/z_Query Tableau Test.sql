﻿-- TARGET CUSTOMER
select 
		  a.target_customer_id as id
		-- , 'Target Customer' as type
		-- , action_radius
		-- , a.latitude
		-- , a.longtitude
		, a.shipping_street
		, a.shipping_postal_code
		, a.shipping_city
		, COALESCE(b.cleaner_count,0) as cleaner_count
		-- , COALESCE(c.other_customer_count,0) as other_customer_count
		-- , c.Total_Demanded_Hours
		-- , cast(COALESCE(b.cleaner_count::numeric/c.other_customer_count::numeric,0) as numeric(7,2)) as Cleaner_other_Customer_Ratio
		, a.order_status as order_status
			
from main.z_radius_target_customer a
	left join (	
			select target_customer_id, count(cleaner_id) as Cleaner_Count
			from main.z_radius_relation_cleaner_target_customer
			group by target_customer_id
		) b
		on a.target_customer_id = b.target_customer_id
	left join (
			select c.target_customer_id, count(c.other_customer_id) as other_customer_count, sum(d.order_duration) as Total_Demanded_Hours
			from main.z_radius_relation_target_customer_other_customer c
			left join main.z_radius_other_customer d
			on c.other_customer_id = d.other_customer_id
			group by target_customer_id
		) c
		on a.target_customer_id = c.target_customer_id
/*
union

select
	  cleaner_id as id
	, 'Cleaner' as type
	, action_radius
	, latitude
	, longtitude
	, billing_city as city
	, null as cleaner_count
	, null as other_customer_count
	, null Total_Demanded_Hours
	, null as Cleaner_other_Customer_Ratio
	, null as order_status
from main.z_radius_cleaner 

union

select 
	  other_customer_id as id
	, 'Other Customer' as type
	, null as action_radius
	, latitude
	, longtitude
	, shipping_city as city
	, null as cleaner_count
	, null as other_customer_count
	, null Total_Demanded_Hours
	, null as Cleaner_other_Customer_Ratio
	, order_status as order_status
from main.z_radius_other_customer
;

*/