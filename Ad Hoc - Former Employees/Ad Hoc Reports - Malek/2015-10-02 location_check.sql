

select order_status, count(*)
from main.order
group by order_status;

select 
		  billing_street
		, billing_postal_code
		, billing_city
		, billing_country
		, location_latitude
		, location_longitude
		, Point(account.location_longitude, account.location_latitude)
		, ST_SetSRID(ST_MakePoint(location_longitude,location_latitude),4326)
from main.account;

select radius_effective__c
from salesforce.account