/* 
	prüft Anzahl von lead nach verschiedenen Attributen
	momentanes standardattribut: status = 'Contact/Account created'
*/
	select 'anzahl_account', count(*)
	from salesforce.account
	where to_char(createddate,'YYYY-MM') between '2015-03' and '2015-08'
union
	select 'convertedcontactid',count(*)
	from salesforce.lead
	where convertedcontactid <> '' and convertedcontactid is not null
	and to_char(createddate,'YYYY-MM') between '2015-03' and '2015-08'
union
	select 'isconverted',count(*)
	from salesforce.lead
	where isconverted = true
	and to_char(createddate,'YYYY-MM') between '2015-03' and '2015-08'
	group by isconverted
union
	select 'status_acc_created',count(*)
	from salesforce.lead
	where status = 'Contact/Account created'
	and to_char(createddate,'YYYY-MM') between '2015-03' and '2015-08';