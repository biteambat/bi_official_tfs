select distinct

extract(DOW from o.order_creation__c) as creation_date,
extract(DOW from o.effectivedate) as effective_date,

case 
when extract(Day from (o.effectivedate - o.order_creation__c)) = 1 then 2
when extract(Day from (o.effectivedate - o.order_creation__c)) > 1 and extract(Day from (o.effectivedate - o.order_creation__c)) < 7 then to_char(extract(Day from (o.effectivedate - o.order_creation__c)),'9')
when extract(Day from (o.effectivedate - o.order_creation__c)) >= 7 and extract(Day from (o.effectivedate - o.order_creation__c)) < 14 then '7+'
when extract(Day from (o.effectivedate - o.order_creation__c)) >= 14 and extract(Day from (o.effectivedate - o.order_creation__c)) < 21 then '14+'
when extract(Day from (o.effectivedate - o.order_creation__c)) >= 21 then '21+'
end as diff_to_start_date,

left(o.locale__c,2) as locale,
count(1)

from salesforce.order o 

where o.acquisition_channel__c = 'web' 
and o.status not like '%cancelled%' --or 'faked'
and o.order_creation__c > (current_date - interval '3 months')
and o.test__c = '0' -- Always keep it => means that we ignore orders from QA tests etc...
and extract(Day from (o.effectivedate - o.order_creation__c)) > 0

group by extract(DOW from o.order_creation__c), extract(DOW from o.effectivedate), left(o.locale__c,2), diff_to_start_date

order by extract(DOW from o.order_creation__c), extract(DOW from o.effectivedate), left(o.locale__c,2), diff_to_start_date

--NB: it also takes into account orders which are created 2 weeks before the effective date, does it make part of what we want to measure ?