SELECT

left(o.city,2) as country,
o.city,

sum(case when o.acquisition_channel__c in ('web') then 1 else 0 end) as booking_via_website

FROM

bi.orders_w_marketing o

WHERE o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
AND o.test__c = '0'

GROUP BY o.city
ORDER BY o.city asc