﻿SELECT 

(SELECT 
	sum(LeadCount)
FROM
	bi.immoscout2) as total_immoscout_leads,

(SELECT 
	sum(CASE WHEN o.voucher__c in ('IMMOTIGER','IMMOTIGER20','PUTZPARTNER20','CSIS512C20','CSXU328C15') THEN 1 ELSE 0 END)

FROM 
	bi.orders_w_marketing o

WHERE 
	o.test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and acquisition_new_customer__c = '1') as total_immoscout_acquisitions,



Round((SELECT sum(CASE WHEN o.voucher__c in ('IMMOTIGER','IMMOTIGER20','PUTZPARTNER20','CSIS512C20','CSXU328C15') THEN 1 ELSE 0 END) FROM bi.orders_w_marketing o WHERE 
	o.test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and acquisition_new_customer__c = '1') 

/

(SELECT sum(LeadCount) FROM bi.immoscout2) * 100 , 2) || '%'
as conversion_rate