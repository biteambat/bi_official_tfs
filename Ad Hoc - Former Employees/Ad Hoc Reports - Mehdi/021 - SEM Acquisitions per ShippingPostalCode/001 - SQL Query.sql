﻿Select
	o.order_creation__c::timestamp::date as order_creation_date,
	left(o.shippingpostalcode,2) as shippingpostalcode,
	sum(case when (o.acquisition_new_customer__c = 'true' and o.marketing_channel in ('SEM')) then 1 else 0 end) as nb_acquisitions
	--count(distinct o.id) as nb_sem_acquisitions

From bi.orders_w_marketing o

Where
	o.test__c = '0'
	and o.status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	--and o.marketing_channel in ('SEM')
	and (o.order_creation__c::timestamp::date = '2016-02-22' or o.order_creation__c::timestamp::date = '2016-02-15')
	--and o.acquisition_new_customer__c = 'true'

Group by o.order_creation__c::timestamp::date, left(o.shippingpostalcode,2) 

Order by o.order_creation__c::timestamp::date desc, nb_acquisitions desc --nb_sem_acquisitions desc