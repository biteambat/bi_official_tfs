-- View: bi.orders_w_marketing

-- DROP VIEW bi.orders_w_marketing;

CREATE OR REPLACE VIEW bi.orders_w_marketing AS 
 SELECT t.refunded__c,
    t.suggested_time_customer__c,
    t.invoice_number_us__c,
    t.effectivedate,
    t.conditions__c,
    t.id,
    t.customer_email__c,
    t.web_grand_total__c,
    t.billingcountry,
    t.enddate,
    t.grand_total_eur__c,
    t.acquisition_new_customer__c,
    t.createddate,
    t.order_creation__c,
    t.order_origin__c,
    t.type,
    t.in_test_period__c,
    t.shippingcity,
    t.professionalslot3__c,
    t.acquisition_channel__c,
    t.description,
    t.professionalslot1__c,
    t.ordernumber,
    t.companyauthorizedbyid,
    t.referred_by__c,
    t.recurrency__c,
    t.systemmodstamp,
    t.lastreferenceddate,
    t.loss_internal__c,
    t.suggested_when_customer__c,
    t.voucher__c,
    t.shippingstreet,
    t.default_professional__c,
    t.discount__c,
    t.professionalslot2__c,
    t.quick_note__c,
    t.professionalslot5__c,
    t.suggested_date_customer__c,
    t.professional__c,
    t.accountid,
    t.scoring__c,
    t.order_end__c,
    t._hc_err,
    t.extras__c,
    t.order_start__c,
    t.invoice_link__c,
    t.contractid,
    t.order_time__c,
    t.settled__c,
    t.job_details__c,
    t.shippingcountrycode,
    t.ip__c,
    t.shippingpostalcode,
    t.acquisition_tracking_id__c,
    t.suggested_date_professional__c,
    t.billingstreet,
    t.loss_due_cleaner__c,
    t.rating_service__c,
    t.debit_charge_sent__c,
    t.suggested_time_professional__c,
    t.customer_phone__c,
    t.shippinglongitude,
    t.commission__c,
    t.shippingcountry,
    t.tracking_life__c,
    t.acquisition_customer_creation__c,
    t.customer_name__c,
    t.shippinglatitude,
    t._hc_lastop,
    t.loss_due_customer__c,
    t.test__c,
    t.setting_gender__c,
    t.setting_flexible_time__c,
    t.shippingstatecode,
    t.payment_method__c,
    t.activatedbyid,
    t.billingcity,
    t.gmv__c,
    t.billinglongitude,
    t.activateddate,
    t.error_note__c,
    t.status,
    t.statuscode,
    t.acquisition_channel_params__c,
    t.shippingstate,
    t.createdbyid,
    t.rating_professional__c,
    t.billingcountrycode,
    t.invoiced__c,
    t.working_city__c,
    t.cancellation_reason__c,
    t.acquisition_channel_ref__c,
    t.auto_matched__c,
    t.suggested_when_professional__c,
    t.paid__c,
    t.order_next__c,
    t.order_duration__c,
    t.originalorderid,
    t.sfid,
    t.billingstate,
    t.isdeleted,
    t.user_agent__c,
    t.pricebook2id,
    t.selfmatched__c,
    t.refunded_amount__c,
    t.billingpostalcode,
    t.pph__c,
    t.billingstatecode,
    t.isreductionorder,
    t.lastmodifieddate,
    t.invoice_number_professional__c,
    t.lastmodifiedbyid,
    t.contact__c,
    t.commissioned__c,
    t.order_id__c,
    t.professionalslot4__c,
    t.locale__c,
    t.acquisition_createdat__c,
    t.billinglatitude,
    t.customerauthorizedbyid,
    t.totalamount,
    t.lastvieweddate,
    t.customer_id__c,
    t.setting_pets__c,
    t.allocation_attempts__c,
    t.professionalslot6__c,
    t.professionalslot10__c,
    t.professionalslot8__c,
    t.professionalslot7__c,
    t.professionalslot9__c,
    t.allocation_attempts_last__c,
    t.density_professionals__c,
    t.density_check__c,
    t.stripe_refund_id__c,
    t.stripe_charge_id__c,
    t.paid_amount__c,
    t.mollie_charge_id__c,
    t.paypal_charge_id__c,
    t.payment_error__c,
    t.checkout__c,
    t.checkin__c,
    t.distance__c,
    t.checkout_geolocation__latitude__s,
    t.checkin_distance__c,
    t.checkin_geolocation__longitude__s,
    t.checkin_geolocation__latitude__s,
    t.checkout_distance__c,
    t.checkout_geolocation__longitude__s,
    t.commuting_time__c,
    t.payrexx_charge_id__c,
        CASE
            WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%b'::text) AND (t.voucher__c IS NULL OR t.voucher__c::text = ''::text OR t.voucher__c::text ~~ '%WISCHMOP%'::text OR t.voucher__c::text = 'WISCH1SEB'::text OR t.voucher__c::text = 'WISCH1SEM'::text OR t.voucher__c::text = 'TIGER1SEB'::text) AND t.acquisition_channel_params__c::text !~~ '%disp%'::text OR t.voucher__c::text = 'WISCH1SEB'::text OR t.voucher__c::text = 'SEBTIGER20'::text OR t.voucher__c::text = 'SEBTIGER40'::text OR t.voucher__c::text = 'TIGER1SEB'::text THEN 'SEM Brand'::text
            WHEN (t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) AND t.acquisition_channel_params__c::text !~~ '%disp%'::text AND (t.voucher__c::text = 'WISCH1SEM'::text OR t.voucher__c IS NULL OR t.voucher__c::text = ''::text OR t.voucher__c::text = 'WISCHMOP3C'::text OR t.voucher__c::text = 'WISCHMOP3A'::text) OR t.voucher__c::text = 'WISCH1SEM'::text OR t.voucher__c::text = 'TIGER1SEM'::text THEN 'SEM'::text
            WHEN t.acquisition_channel_params__c::text ~~ '%disp%'::text AND t.acquisition_channel_params__c::text !~~ '%remarketing%'::text AND t.acquisition_channel_params__c::text !~~ '%Facebook%'::text AND (t.voucher__c::text = 'WISCH1GDN'::text OR t.voucher__c IS NULL OR t.voucher__c::text = ''::text OR t.voucher__c::text = 'WISCH1SOC'::text OR t.voucher__c::text = 'WISCH1GSP'::text) OR t.voucher__c::text = 'WISCH1SOC'::text OR t.voucher__c::text = 'WISCH1GSP'::text OR t.voucher__c::text = 'WISCH1GDN'::text OR t.voucher__c::text = 'WISCH1CR'::text OR t.voucher__c::text = 'DSPUTZ20'::text OR t.voucher__c::text = 'TIGER1GDN'::text OR t.voucher__c::text = 'GDTIGER20'::text OR t.voucher__c::text = 'GDTIG20'::text OR t.voucher__c::text = 'SPRINGDS20'::text OR t.voucher__c::text = 'SPRINGDS50'::text OR t.voucher__c::text = 'SPRINGDS3P'::text OR t.voucher__c::text = 'AMTIGER20'::text OR t.voucher__c::text = 'AMTIGER1H'::text OR t.voucher__c::text = 'LNKTIGER20'::text OR t.voucher__c::text = 'XNGTIGER20'::text OR t.voucher__c::text = 'GUT20'::text OR t.voucher__c::text = 'RAB20'::text THEN 'Display'::text
            WHEN t.acquisition_channel_params__c::text ~~ '%disp%'::text AND t.acquisition_channel_params__c::text ~~ '%remarketing%'::text AND t.acquisition_channel_params__c::text !~~ '%Facebook%'::text THEN 'Display Remarketing'::text
            WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text AND (t.voucher__c IS NULL OR t.voucher__c::text = ''::text OR t.voucher__c::text = 'WISCH1YOU'::text) OR t.voucher__c::text = 'WISCH1YOU'::text THEN 'Youtube Paid'::text
            WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text AND t.acquisition_channel_params__c::text !~~ '%goog%'::text AND t.acquisition_channel_params__c::text !~~ '%goob%'::text AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text AND t.acquisition_channel_params__c::text !~~ '%disp%'::text AND (t.voucher__c IS NULL OR t.voucher__c::text = ''::text) THEN 'SEO'::text
            WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text AND t.acquisition_channel_params__c::text !~~ '%goog%'::text AND t.acquisition_channel_params__c::text !~~ '%goob%'::text AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text AND t.acquisition_channel_params__c::text !~~ '%disp%'::text AND (t.voucher__c IS NULL OR t.voucher__c::text = ''::text OR t.voucher__c::text = 'WISCH1ABC'::text OR t.voucher__c::text = 'TIGER1ABC'::text OR t.voucher__c::text = 'TIGERABC20'::text OR t.voucher__c::text = 'TIGERABC40'::text) OR t.voucher__c::text = 'WISCH1ABC'::text OR t.voucher__c::text = 'TIGER1ABC'::text OR t.voucher__c::text = 'TIGERABC20'::text OR t.voucher__c::text = 'TIGERABC40'::text THEN 'SEO Brand'::text
            WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text OR t.acquisition_channel_params__c::text ~~ '%batfb%'::text OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) AND (t.acquisition_channel_params__c::text ~~ '%utm%'::text OR t.acquisition_channel_ref__c::text ~~ '%utm_campaign%'::text OR t.acquisition_channel_params__c::text ~~ '%lead-gen%'::text OR t.acquisition_channel_params__c::text ~~ '%fblg%'::text) AND (t.voucher__c IS NULL OR t.voucher__c::text = ''::text OR t.voucher__c::text = 'WISCHMOP3C'::text OR t.voucher__c::text = 'WISCH1FB'::text OR t.voucher__c::text = 'RABATT20'::text OR t.voucher__c::text = 'FBPRIMA20'::text OR t.voucher__c::text = 'FBPUTZCH40'::text OR t.voucher__c::text = 'FBPUTZ20'::text OR t.voucher__c::text = 'FBPUTZ15'::text OR t.voucher__c::text = 'FBTIGER50'::text OR t.voucher__c::text = 'FBTIGER20'::text OR t.voucher__c::text = 'STAR50'::text OR t.voucher__c::text = 'FBTIGER20'::text OR t.voucher__c::text = 'FBTIGER30'::text OR t.voucher__c::text = 'FBTIGER40'::text OR t.voucher__c::text = 'FBTIGER15'::text OR t.voucher__c::text = 'FBTIG70'::text OR t.voucher__c::text = 'SPRINGFB20'::text OR t.voucher__c::text = 'SPRINGFB50'::text OR t.voucher__c::text = 'SPRINGFB3P'::text) OR t.voucher__c::text = 'WISCH1FB'::text OR t.voucher__c::text = 'RABATT20'::text OR t.voucher__c::text = 'FBPRIMA20'::text OR t.voucher__c::text = 'FBPUTZCH40'::text OR t.voucher__c::text = 'FBPUTZ20'::text OR t.voucher__c::text = 'TIGER1FB'::text OR t.voucher__c::text = 'FBTIGER15'::text OR t.voucher__c::text = 'FBPUTZ15'::text OR t.voucher__c::text = 'FBTIGER50'::text OR t.voucher__c::text = 'FBTIGER20'::text OR t.voucher__c::text = 'STAR50'::text OR t.voucher__c::text = 'FBTIGER30'::text OR t.voucher__c::text = 'FBTIGER40'::text OR t.voucher__c::text = 'FBTIGER15'::text OR t.voucher__c::text = 'FBTIG70'::text THEN 'Facebook'::text
            WHEN t.acquisition_channel_params__c::text !~~ '%goog%'::text AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text AND t.acquisition_channel_params__c::text !~~ '%disp%'::text AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text AND t.acquisition_channel_params__c::text !~~ '%fb%'::text AND t.voucher__c::text > '0'::text THEN 'Voucher Campaigns'::text
            WHEN t.acquisition_channel_params__c::text !~~ '%batfb%'::text AND t.acquisition_channel_ref__c::text !~~ '%batfb%'::text AND (t.acquisition_channel_params__c::text ~~ '%facebook%'::text OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) AND t.voucher__c IS NULL THEN 'Facebook Organic'::text
            WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text OR t.acquisition_channel_params__c::text ~~ '%email%'::text OR t.acquisition_channel_params__c::text ~~ '%vero%'::text OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text THEN 'Newsletter'::text
            WHEN t.acquisition_channel_params__c::text !~~ '%goog%'::text AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text AND t.acquisition_channel_params__c::text !~~ '%disp%'::text AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text AND t.acquisition_channel_params__c::text !~~ '%fb%'::text AND t.acquisition_channel_params__c::text !~~ '%clid%'::text AND t.acquisition_channel_params__c::text !~~ '%utm%'::text AND t.acquisition_channel_params__c::text <> ''::text THEN 'DTI'::text
            ELSE 'Unattributed'::text
        END AS marketing_channel,
        CASE
            WHEN "left"(t.shippingpostalcode::text, 2) = '52'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Aachen'::text
            WHEN (("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['10'::text, '11'::text, '12'::text, '13'::text, '14'::text, '15'::text])) OR ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['140'::text, '141'::text, '144'::text, '145'::text, '153'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Berlin'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = '320'::text OR "left"(t.shippingpostalcode::text, 2) = '33'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Bielefeld'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['530'::text, '531'::text, '532'::text, '533'::text, '537'::text, '538'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Bonn'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '28'::text OR "left"(t.shippingpostalcode::text, 3) = '277'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Bremen'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['502'::text, '503'::text, '504'::text, '505'::text, '506'::text, '507'::text, '508'::text, '509'::text, '510'::text, '511'::text, '513'::text, '514'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Cologne'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['642'::text, '643'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Darmstadt'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '44'::text OR (t.shippingpostalcode::text = ANY (ARRAY['441'::character varying::text, '442'::character varying::text, '443'::character varying::text, '445'::character varying::text, '457'::character varying::text, '580'::character varying::text, '582'::character varying::text, '583'::character varying::text, '584'::character varying::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Dortmund'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '01'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Dresden'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['454'::text, '461'::text, '470'::text, '471'::text, '472'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Duisburg'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '47'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Duisburg+'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '40'::text OR ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['414'::text, '415'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Dusseldorf'::text
            WHEN (("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['44866'::text, '44867'::text, '46047'::text, '46236'::text, '46238'::text, '46240'::text])) OR "left"(t.shippingpostalcode::text, 2) = '45'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Essen'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['446'::text, '448'::text, '462'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Essen+'::text
            WHEN (("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['61118'::text, '61440'::text, '61449'::text, '63065'::text, '63067'::text, '63069'::text, '63071'::text, '63073'::text, '63075'::text, '63150'::text, '63165'::text, '63263'::text, '63303'::text, '63477'::text, '65451'::text, '65760'::text, '65824'::text, '65843'::text, '65929'::text, '65931'::text, '65933''65934'::text, '65936'::text])) OR "left"(t.shippingpostalcode::text, 2) = '60'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Frankfurt am Main'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['611'::text, '612'::text, '613'::text, '614'::text, '630'::text, '631'::text, '632'::text, '633'::text, '634'::text, '654'::text, '657'::text, '658'::text, '659'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Frankfurt am Main+'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '79'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Freiburg'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '59'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Halle'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '58'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hagen'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['20'::text, '22'::text])) OR ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['21031'::text, '21075'::text, '21077'::text, '21079''21107'::text, '21109'::text, '21129'::text])) THEN 'DE-Hamburg'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '21'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hamburg+'::text
            WHEN "left"(t.shippingpostalcode::text, 3) = '483'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hamm'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '30'::text OR "left"(t.shippingpostalcode::text, 3) = '311'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hanover'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '24'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Kiel'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '76'::text OR ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['751'::text, '750'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Kalsruhe'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['041'::text, '042'::text, '043'::text, '044'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Leipzig'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '23'::text OR "left"(t.shippingpostalcode::text, 3) = '192'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Lübeck'::text
            WHEN ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['55116'::text, '55118'::text, '55120'::text, '55122'::text, '55124'::text, '55126'::text, '55127'::text, '55128'::text, '55129'::text, '55130'::text, '55131'::text, '55246'::text, '55252'::text, '55257'::text, '55294'::text, '65201'::text, '65203'::text, '65239'::text, '65462'::text, '65474'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mainz'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['550'::text, '551'::text, '552'::text, '650'::text, '651'::text, '652'::text, '653'::text, '655'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mainz+'::text
            WHEN ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['67059'::text, '67061'::text, '67063'::text, '67065'::text, '67067'::text, '67069'::text, '67071'::text, '67122'::text, '67141'::text, '68159'::text, '68161'::text, '68163'::text, '68165'::text, '68167'::text, '68169'::text, '68199'::text, '68219'::text, '68229'::text, '68239'::text, '68259'::text, '68305'::text, '68307'::text, '68309'::text, '68549'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mannheim'::text
            WHEN (("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['670'::text, '671'::text, '672'::text, '673'::text, '690'::text, '691'::text, '692'::text, '694'::text, '699'::text])) OR "left"(t.shippingpostalcode::text, 2) = '68'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mannheim+'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '39'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Magdeburg'::text
            WHEN ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['41061'::text, '41063'::text, '41065'::text, '41066'::text, '41068'::text, '41069'::text, '41169'::text, '41179'::text, '41189'::text, '41199'::text, '41236'::text, '41238'::text, '41239'::text, '41352'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mönchengladbach'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '41'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mönchengladbach+'::text
            WHEN "left"(t.locale__c::text, 2) = 'de'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['80'::text, '81'::text])) OR ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['82008'::text, '82024'::text, '82031'::text, '82041'::text, '82049'::text, '82061'::text, '82166'::text, '85521'::text, '85579'::text, '85609'::text, '85737'::text, '85774'::text])) THEN 'DE-Munich'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['480'::text, '481'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Munster'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '90'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Nuremberg'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['910'::text, '911'::text, '912'::text, '913'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Nuremberg+'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['930'::text, '931'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Regensburg'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['180'::text, '181'::text, '182'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Rostock'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['660'::text, '661'::text, '662'::text, '663'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Saarbrücken'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '70'::text OR ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['71254'::text, '73728'::text, '73732'::text, '73733'::text, '73734'::text, '73760'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Stuttgart'::text
            WHEN (("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['710'::text, '712'::text, '716'::text, '737'::text, '720'::text, '721'::text, '725'::text, '726'::text, '727'::text, '730'::text, '732'::text, '736'::text, '743'::text, '752'::text, '753'::text, '754'::text])) OR "left"(t.shippingpostalcode::text, 2) = '71'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Stuttgart+'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = '312'::text OR "left"(t.shippingpostalcode::text, 2) = '38'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-WBurgBschweig'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '42'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Wuppertal'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['970'::text, '971'::text, '972'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Würzburg'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 1) = '1'::text THEN 'AT-Vienna'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '50'::text THEN 'AT-Salzburg'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '60'::text THEN 'AT-Insbruck'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '40'::text THEN 'AT-Linz'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '80'::text THEN 'AT-Graz'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '90'::text THEN 'AT-Klagenfurt'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND ("left"(t.shippingpostalcode::text, 1) = ANY (ARRAY['2'::text, '3'::text, '4'::text, '5'::text, '6'::text, '7'::text, '8'::text, '9'::text])) THEN 'AT-Other'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['420'::text, '422'::text, '424'::text, '440'::text, '441'::text, '442'::text])) THEN 'CH-Basel'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['341'::text, '350'::text, '351'::text])) THEN 'CH-Bern'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['111'::text, '112'::text, '150'::text, '151'::text, '160'::text, '161'::text, '167'::text, '169'::text, '180'::text])) THEN 'CH-Lausanne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['564'::text, '610'::text, '620'::text, '627'::text, '628'::text, '630'::text, '631'::text, '634'::text, '635'::text, '636'::text, '637'::text, '638'::text, '640'::text, '641'::text])) THEN 'CH-Lucerne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['520'::text, '910'::text, '911'::text, '920'::text, '921'::text, '922'::text, '923'::text, '924'::text, '930'::text, '931'::text, '932'::text, '940'::text, '941'::text, '942'::text, '943'::text, '945'::text])) THEN 'CH-St.Gallen'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['542'::text, '543'::text, '545'::text, '562'::text, '824'::text, '830'::text, '831'::text, '832'::text, '833'::text, '835'::text, '840'::text, '841'::text, '842'::text, '844'::text, '845'::text, '847'::text, '848'::text, '849'::text, '860'::text, '861'::text, '870'::text, '880'::text, '881'::text, '890'::text, '891'::text, '893'::text, '894'::text, '895'::text, '896'::text, '954'::text])) THEN 'CH-Zurich'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['40'::text, '41'::text])) THEN 'CH-Basel'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['30'::text, '31'::text, '32'::text, '33'::text, '34'::text])) THEN 'CH-Bern'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '25'::text THEN 'CH-Biel'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '12'::text THEN 'CH-Geneva'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['10'::text, '13'::text])) THEN 'CH-Lausanne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '60'::text THEN 'CH-Lucerne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '90'::text THEN 'CH-St.Gallen'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['80'::text, '81'::text, '85'::text])) THEN 'CH-Zurich'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 1) = ANY (ARRAY['1'::text, '2'::text, '3'::text, '4'::text, '5'::text, '6'::text, '7'::text, '8'::text, '9'::text])) THEN 'CH-Other'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '18'::text THEN 'NL-Alkmaar'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '13'::text THEN 'NL-Almere'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '38'::text THEN 'NL-Amersfoort'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['10'::text, '11'::text, '14'::text, '15'::text, '21'::text])) THEN 'NL-Amsterdam'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['48'::text, '50'::text])) THEN 'NL-Breda-Tillburg'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['54'::text, '55'::text, '56'::text, '57'::text])) THEN 'NL-Eindhoven'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['27'::text, '28'::text])) THEN 'NL-Gouda'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['19'::text, '20'::text])) THEN 'NL-Haarlem'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '52'::text THEN 'NL-Hertogenbosch'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '12'::text THEN 'NL-Hilversum'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '23'::text THEN 'NL-Leiden'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '62'::text THEN 'NL-Maastricht'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['30'::text, '31'::text])) THEN 'NL-Rotterdam'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['22'::text, '24'::text, '25'::text, '26'::text])) THEN 'NL-The Hague'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['34'::text, '35'::text, '36'::text, '37'::text, '39'::text])) THEN 'NL-Utrecht'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['3'::text, '4'::text, '5'::text, '6'::text, '7'::text, '8'::text, '9'::text])) THEN 'NL-Other'::text
            ELSE 'DE-Other'::text
        END AS city,
        CASE
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date < '2015-10-01'::date THEN t.gmv__c * 0.96::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2015-10-01'::date THEN t.gmv__c * 0.92::double precision
            ELSE t.gmv__c
        END AS gmv_eur
   FROM salesforce."order" t;

ALTER TABLE bi.orders_w_marketing
  OWNER TO u6fbredvlcomqt;
