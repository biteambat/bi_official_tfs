﻿SELECT 
	o.voucher__c,
	case when o.acquisition_new_customer__c = true then 'Acquisition' else 'Rebooking' end as acquisition_status,
	count(distinct o.id) as totalorders

FROM bi.orders_w_marketing o

WHERE
	o.test__c = '0'
	and o.effectivedate > '2015-11-22'
	and o.marketing_channel = 'Voucher Campaigns'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')

GROUP BY
	o.voucher__c, acquisition_status
	
ORDER BY
	totalorders desc, acquisition_status asc