﻿SELECT
	replace(o.city,'+','') as city,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.pph__c,
	sum(o.order_duration__c),
	
	Case 
		when o.type = '1' then 'Economy'
		when o.type = '2' then 'Economy Plus'
		when o.type = '3' then 'Premium'
		when o.type = '4' then 'Premium Pro'
		when o.type is NULL and pph__c = 15 then 'Economy (No type but PPH = 15)'
	End as Quality_level

FROM bi.orders_w_marketing o

WHERE
	Replace(o.city,'+','') in ('DE-Dresden','DE-Leipzig','DE-Hanover')
	and o.status in ('INVOICED')
	and (o.effectivedate >= '2015-12-01' and o.effectivedate <= '2016-01-31')
	and o.test__c = '0'

GROUP BY
	o.city,
	o.pph__c,
	Quality_level,
	recurrency

ORDER BY
	o.city asc,
	pph__c asc,
	recurrency asc