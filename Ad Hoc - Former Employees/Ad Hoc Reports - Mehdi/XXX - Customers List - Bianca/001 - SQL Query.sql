SELECT
	t1.customer_name__c,
	t1.customer_email__c,
	t1.customer_phone__c,
	t1.locale,
	t1.polygon,
	t1.shippingstreet,
	t1.shippingpostalcode,
	t1.shippingcity,
	t1.shippingstreet || ', ' || t1.shippingpostalcode || ' ' || t1.shippingcity as fulladdress


FROM

(
	SELECT
		o.customer_name__c,
		o.customer_email__c,
		o.customer_phone__c,
		o.polygon,
		o.shippingcity as shippingcity,
		o.shippingstreet as shippingstreet,
		o.shippingpostalcode,
		LEFT(o.locale__c, 2) as locale,
		COUNT(DISTINCT CASE WHEN o.status = 'INVOICED' AND o.effectivedate < current_date THEN o.sfid ELSE NULL END) as nb_invoiced_orders,
		COUNT(DISTINCT CASE WHEN o.status NOT LIKE ('%CANCELLED%') AND o.effectivedate > current_date AND o.recurrency__c > 0 THEN o.sfid ELSE NULL END) as nb_future_orders_planned

	FROM bi.orders o

	WHERE o.test__c = '0'
		AND o.order_type = '1'
		AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')

	GROUP BY
		o.customer_name__c,
		o.customer_email__c,
		o.customer_phone__c,
		o.polygon,
		o.shippingcity,
		o.shippingstreet,
		o.shippingpostalcode,
		LEFT(o.locale__c, 2)
) as t1

WHERE t1.nb_invoiced_orders > 0
	AND t1.nb_future_orders_planned > 0
	AND t1.polygon IS NOT NULL 

ORDER BY 
	t1.polygon asc,
	t1.shippingpostalcode asc,
	t1.customer_name__c asc
