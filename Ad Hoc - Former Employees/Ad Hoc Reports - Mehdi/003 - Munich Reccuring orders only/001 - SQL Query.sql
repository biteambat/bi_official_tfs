﻿select

owm.id as order_id,
owm.order_creation__c::timestamp::date as order_creation,
owm.recurrency__c,
case when owm.pph__c is null then null else owm.pph__c end,
owm.shippingcity,
owm.city


from bi.orders_w_marketing owm

where owm.order_creation__c >= '2016-01-01'
and owm.acquisition_channel__c = 'web'
and owm.test__c = '0'
and left(owm.locale__c,2) = 'de'
and owm.billingpostalcode like '8%'
and owm.city = 'DE-Munich'

order by owm.order_creation__c::timestamp::date asc