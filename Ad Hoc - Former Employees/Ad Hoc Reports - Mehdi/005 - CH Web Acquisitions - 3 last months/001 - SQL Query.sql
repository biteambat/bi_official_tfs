﻿select
o.id as order_id,
o.order_creation__c::timestamp::date as order_created,
left(o.city,2) as country_ISO,
o.gmv_eur as gmv_in_€,
left(o.shippingpostalcode,2) as postcode_left2,
o.shippingcity as shipping_city,
o.shippinglatitude,
o.shippinglongitude

from 

bi.orders_w_marketing o

where o.test__c = '0' --no test of course
and o.order_creation__c >= (current_date - interval '3 months') --for the last three months
and left(o.city,2)='CH'--for switzerland 
and o.acquisition_channel__c = 'web'-- all web acquisitions
and o.status not in ('CANCELLED MISTAKE','CANCELLED FAKED')

order by o.order_creation__c asc, o.id asc