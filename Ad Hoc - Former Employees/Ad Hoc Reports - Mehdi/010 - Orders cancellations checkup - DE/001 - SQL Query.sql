﻿select distinct

o.id as order_id,
o.sfid as sfid,

o.order_creation__c::timestamp::date as creation_date,
extract(week from o.order_creation__c) as creation_week,
extract(year from o.order_creation__c) as creation_year,
extract(DOW from o.order_creation__c) as creation_DOW,

o.order_start__c::timestamp::date as order_start_date,
extract(hour from o.order_start__c) || ':' || extract(minute from o.order_start__c) as order_start_hour,
extract(week from o.order_start__c) as order_start_week,
extract(year from o.order_start__c) as order_start_year,
extract(DOW from o.order_start__c) as order_start_DOW,

o.acquisition_channel__c as acquisition_channel,
o.marketing_channel as marketing_channel,
o.payment_method__c as payment_method,
o.status as status,
o.city,
replace(cast(o.gmv_eur as text),'.',',') as gmv_eur

from bi.orders_w_marketing o
where o.acquisition_channel__c in ('web','recurrent')
and o.order_start__c > '2015-12-01' -- starting date
and o.test__c = '0' -- Always keep it => we ignore orders from QA tests etc...
and left(o.locale__c,2) = 'de' -- country = germany

order by creation_date asc