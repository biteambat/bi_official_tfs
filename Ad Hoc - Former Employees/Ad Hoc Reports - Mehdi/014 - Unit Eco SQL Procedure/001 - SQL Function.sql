CREATE OR REPLACE FUNCTION reports.citystats(crunchdate date) RETURNS void AS 
$BODY$
BEGIN

-- Structure
-- 1) date 2) locale 3) city 4)kpi 5)value

-- Invoiced GMV

DROP TABLE IF EXISTS reports.city_kpis;	
CREATE TABLE reports.city_kpis as 

SELECT
	Effectivedate::date as date,
	left(o.city,2) as locale,
	replace(o.city,'+','') as city_group,
	CAST('Invoiced GMV' as varchar) as kpi,
	round(cast(sum(case when o.status in ('INVOICED','CANCELLED CUSTOMER SHORTTERM','CANCELLED PROFESSIONAL SHORTTERM','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') then o.gmv_eur else 0 end) as numeric),2) as value
FROM
	bi.orders_w_marketing o
WHERE 
	o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
	AND o.test__c = '0'
GROUP BY 
	city_group,
  locale,
  date
ORDER BY
	locale asc,
	city_group asc,
	date desc;

-- Number of Cancelled orders EXCEPT FAKED AND MISTAKE

INSERT INTO reports.city_kpis

SELECT
	Effectivedate::date as date,
	left(o.city,2) as locale,
	replace(o.city,'+','') as city_group,
	CAST('Cancelled orders'  as varchar) as kpi,
	cast(sum(case when o.status like '%CANCELLED%' then 1 else 0 end) as numeric) as value
FROM
	bi.orders_w_marketing o
WHERE 
	o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
	AND o.test__c = '0'
GROUP BY 
	city_group,
  locale,
	Effectivedate
ORDER BY
	left(o.city,2) asc,
	city_group asc,
	Effectivedate desc;

-- NB Invoiced + Cancelled orders EXCEPT FAKED AND MISTAKE

INSERT INTO reports.city_kpis

SELECT
	Effectivedate::date as date,
	left(o.city,2) as locale,
	replace(o.city,'+','') as city_group,
	CAST('Invoiced + Cancelled orders' as varchar) as kpi,
	cast(sum(case when (o.status like 'INVOICED' or o.status like '%CANCELLED%') then 1 else 0 end) as numeric) as value
	--cast(count(o.id) as numeric) as value
FROM
	bi.orders_w_marketing o
WHERE 
	--(o.status like 'INVOICED' or o.status like '%CANCELLED%')
	/*AND*/ o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
GROUP BY 
	city_group,
  locale,
	date
ORDER BY 
	locale asc,
	city_group asc,
	date desc;


-- Nb of Cancelled NMP 

INSERT INTO reports.city_kpis

SELECT
	Effectivedate::date as date,
	left(o.city,2) as locale,
	replace(o.city,'+','') as city_group,
	CAST('Cancelled NMP rate' as varchar) as kpi,
	CAST(sum(case when o.status in ('CANCELLED NO MANPOWER') then 1 else 0 end) as numeric) as value
FROM
	bi.orders_w_marketing o
WHERE 
	o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
	AND o.test__c = '0'
GROUP BY 
	city_group,
  locale,
	date
ORDER BY 
	locale asc,
	city_group asc,
	date desc;

-- NB Invoiced + Cancelled NMP orders

INSERT INTO reports.city_kpis

SELECT
	Effectivedate::date as date,
	left(o.city,2) as locale,
	replace(o.city,'+','') as city_group,
	CAST('Invoiced + NMP orders' as varchar) as kpi,
	CAST(sum(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER') THEN 1 ELSE 0 END) as numeric) as value
	--cast(count(o.id) as numeric) as value
FROM
	bi.orders_w_marketing o
WHERE 
	--o.status IN ('INVOICED','CANCELLED NO MANPOWER')
	o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
	AND o.test__c = '0'
GROUP BY 
	city_group,
  locale,
	date
ORDER BY 
	locale asc,
	city_group asc,
	date desc;


-- Bookings via website

INSERT INTO reports.city_kpis

SELECT
	Effectivedate::date as date,
	left(o.city,2) as locale,
	replace(o.city,'+','') as city_group,
	CAST('Bookings via website' as varchar) as kpi,
	sum(case when o.acquisition_channel__c in ('web') then 1 else 0 end) as value
FROM
	bi.orders_w_marketing o
WHERE 
	o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
	AND o.test__c = '0'
GROUP BY 
	city_group,
  locale,
	date
ORDER BY 
	locale asc,
	city_group asc,
	date desc;

-- Nb active cleaners

INSERT INTO reports.city_kpis

SELECT 
	b.Startofweek as date,
	b.country as locale,
	replace(b.city_groupby,'+','') as city_group,
	'Nb active cleaners' as kpi,
	b.DistinctCleaner as value
FROM(
SELECT
	EXTRACT(WEEK FROM date) as Week,
  min(date) as Startofweek,
	a.country,
  a.city_groupby,
  COUNT(DISTINCT(cleanerid)) as DistinctCleaner
FROM
(SELECT
	Effectivedate::date as date,
	left(o.city,2) as country,
	replace(o.city,'+','') as city_groupby,
	professional__c as cleanerid
FROM
	bi.orders_w_marketing o
WHERE 
	o.test__c = '0'
	AND o.status IN ('INVOICED')
  and effectivedate::date > '2015-12-01'::date
GROUP BY 
	city_groupby,
  country,
	date,
  professional__c
ORDER BY 
	country asc,
	city_groupby asc,
	Effectivedate desc) 
as a GROUP BY week,country,city_groupby) as b group by city_group, locale, date, DistinctCleaner --date, country, city, DistinctCleaner
 ORDER BY locale asc, city_group asc, date desc;


-- Nb onboarding cleaners per week : Nb of workers STARTING THIS WEEK

INSERT INTO reports.city_kpis

SELECT 
	sf.hr_contract_start__c::date as date,
	left(sf.working_city__c,2) as locale,
	replace(sf.working_city__c,'+','') as city_group,
	'Nb of onboarding cleaners' as kpi,
	count(distinct sf.id) as value

FROM 
	salesforce.account sf

WHERE 
	sf.type__c = '60'
	AND sf.test__c = '0'

GROUP BY
	city_group,
	locale,
	date

ORDER BY
	locale asc,
	city_group asc,
	date desc;

-- Nb cleaners leaving 

INSERT INTO reports.city_kpis

SELECT 
	sf.hr_contract_end__c::date as date,
	left(sf.working_city__c,2) as locale,
	replace(sf.working_city__c,'+','') as city_group,
	'Nb cleaners end of contract' as kpi,
	count(distinct sf.id) as value

FROM 
	salesforce.account sf

WHERE 
	sf.type__c = '60'
	AND sf.test__c = '0'

GROUP BY
	city_group,
	locale,
	date

ORDER BY
	locale asc,
	city_group asc,
	date desc;

 END;

$BODY$ LANGUAGE 'plpgsql'