﻿SELECT 
	date_trunc('month',o.effectivedate) as order_month,
	--o.city as city,
	count(distinct o.customer_id__c) as nb_active_users,
	count(distinct o.referred_by__c) as nb_referrers,
	Round((count(distinct o.referred_by__c) / cast(count(distinct o.customer_id__c) as numeric))*100,2) as percentage_active_users_referring,
	sum(case when o.voucher__c = 'FRF' and o.acquisition_new_customer__c = 'TRUE' then 1 else 0 end) as nb_FRF_acquisitions,
	Round(Cast(avg(case when o.voucher__c = 'FRF' and o.acquisition_new_customer__c = 'TRUE' then o.gmv_eur else null end) as numeric),2) as avg_basket_FRF_orders
FROM 
	bi.orders_w_marketing o

WHERE
	o.test__c = '0'
	and o.status = 'INVOICED'
	and left(o.locale__c, 2) = 'de'
	and o.effectivedate > current_date - interval '6 months'

GROUP BY 
	order_month
	--city

ORDER BY
	--city asc,
	order_month desc
;