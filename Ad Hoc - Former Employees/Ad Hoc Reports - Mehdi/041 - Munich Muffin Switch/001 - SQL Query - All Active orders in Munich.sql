﻿select

o.customer_id__c,
o.effectivedate as order_date,
o.order_time__c,
o.order_duration__c,
o.recurrency__c,
o.payment_method__c,
case when o.acquisition_new_customer__c = '1' then 'acquisition' else 'rebooking' end as acquisition,
o.type as cleaner_type,
o.status,
o.city,
o.shippingpostalcode,
--o.billing_zip_code__c,
o.customer_name__c,
o.customer_email__c as email_address,
o.professional__c,
s.name as cleaner_name

from bi.orders_w_marketing o
left join salesforce.account s on s.sfid = o.professional__c

where o.city like ('DE-Munich%') and o.status not like '%CANCELLED%' and o.effectivedate >= '2016-03-10' and o.test__c = '0'

order by order_date asc