DROP TABLE IF EXISTS bi.temp_coststranform;

CREATE TABLE bi.temp_coststranform AS

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'SEM'::text as channel,
		SUM(cost.sem_non_brand) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'SEM Brand'::text as channel,
		SUM(cost.sem_brand) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'SEO'::text as channel,
		SUM(cost.seo) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'SEO'::text as channel,
		SUM(cost.seo) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'Facebook'::text as channel,
		SUM(cost.facebook) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'Criteo'::text as channel,
		SUM(cost.criteo) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'Sociomantic'::text as channel,
		SUM(cost.sociomantic) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'Display'::text as channel,
		SUM(cost.gdn) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'Youtube'::text as channel,
		SUM(cost.youtube) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'TV'::text as channel,
		SUM(cost.tvcampaign) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

INSERT INTO bi.temp_coststranform 

	SELECT

		cost.orderdate as dates,
		cost.locale as locale,
		'Offline'::text as channel,
		SUM(cost.offline_marketing) as cost

	FROM bi.marketingspending_locale cost

	GROUP BY locale, dates

	ORDER BY locale asc, dates desc
;

DROP TABLE IF EXISTS bi.temp_acquisitions_pday;
CREATE TABLE bi.temp_acquisitions_pday AS

	SELECT
		o.order_creation__c::timestamp::date as orderdate,
		left(o.locale__c,2) as country,
		o.marketing_channel as marketing_channel,
		COUNT(distinct o.ordernumber) as nb_acquisitions

	FROM bi.orders_w_marketing o

	WHERE o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
		and o.test__c = '0'
		and o.acquisition_new_customer__c = '1'

	GROUP BY orderdate, country, marketing_channel

	ORDER BY orderdate desc, country asc, marketing_channel asc

;

DROP TABLE IF EXISTS bi.cpa_forecast_datagroup;
CREATE TABLE bi.cpa_forecast_datagroup AS

	SELECT 
		t1.dates,
		t1.locale,
		t1.channel,
		t1.cost,
		t2.nb_acquisitions
	FROM bi.temp_coststranform t1
		JOIN bi.temp_acquisitions_pday t2
			ON t1.dates = t2.orderdate AND t1.locale = t2.country AND t1.channel = t2.marketing_channel

	GROUP BY dates, locale, channel, orderdate, country, marketing_channel, cost, nb_acquisitions

	ORDER BY dates desc, locale asc, channel asc

;

SELECT * FROM bi.cpa_forecast_datagroup;