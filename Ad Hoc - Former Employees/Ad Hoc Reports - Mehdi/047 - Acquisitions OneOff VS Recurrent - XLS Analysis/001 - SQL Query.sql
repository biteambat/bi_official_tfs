
DROP TABLE IF EXISTS bi.temp_oneoffacq_calcul;
CREATE TABLE bi.temp_oneoffacq_calcul AS

	SELECT DISTINCT 
		o.customer_id__c as customer_id,
		o.marketing_channel,
		o.voucher__c,
		o.order_creation__c::timestamp::date as order_creation

	FROM bi.orders_w_marketing o

	WHERE o.acquisition_new_customer__c = '1' 
		and o.recurrency__c = '0' 
		and o.test__c = '0'
		and o.status in ('INVOICED')
;

-------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_oneoffacq_calcul_2;
CREATE TABLE bi.temp_oneoffacq_calcul_2 AS

SELECT DISTINCT

	t1.customer_id,
	t1.order_creation,
	t1.marketing_channel,
	t1.voucher__c,
	Min(Case When t2.acquisition_new_customer__c = '0' and t2.recurrency__c > '0' then t2.order_creation__c else null end)::timestamp::date as date_recurrency_switch,
	Min(Case When t2.acquisition_new_customer__c = '0' and t2.recurrency__c > '0' then t2.order_creation__c else null end)::timestamp::date  - t1.order_creation as time_before_switch

FROM bi.temp_oneoffacq_calcul t1

JOIN bi.orders_w_marketing t2 ON t1.customer_id = t2.customer_id__c

WHERE t2.acquisition_new_customer__c = '0' 
	and t2.status not like ('%CANCELLED%') 
	and t2.test__c = '0'

GROUP BY t1.customer_id, t1.order_creation, t1.marketing_channel, t1.voucher__c

ORDER BY t1.customer_id asc, t1.order_creation desc
;

----------------------------------------------------

SELECT
	x.customer_id,
	x.order_creation,
	x.marketing_channel as acquisition_channel,
	x.voucher__c,
	x.date_recurrency_switch,
	x.time_before_switch,
	SUM(CASE WHEN (y.order_creation__c::timestamp::date > x.order_creation AND y.order_creation__c::timestamp::date < x.date_recurrency_switch) THEN 1 ELSE 0 END) + 1 as ordersinv_before_switch

FROM bi.temp_oneoffacq_calcul_2 x

JOIN bi.orders_w_marketing y ON x.customer_id = y.customer_id__c

WHERE y.acquisition_new_customer__c = '0' 
	and y.status = 'INVOICED' 
	and y.test__c = '0'
	and x.date_recurrency_switch is not NULL
	and x.time_before_switch > '0'


GROUP BY x.customer_id,
	x.order_creation,
	x.date_recurrency_switch,
	x.time_before_switch,
	x.marketing_channel,
	x.voucher__c

ORDER BY x.order_creation desc, x.customer_id asc

;