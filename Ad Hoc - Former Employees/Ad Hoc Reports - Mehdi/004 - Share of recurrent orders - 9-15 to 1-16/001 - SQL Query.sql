select

owm.id as order_id,
owm.order_creation__c::timestamp::date as order_creation,
owm.recurrency__c as recurrency,
owm.marketing_channel as marketing_channel

from bi.orders_w_marketing owm

where owm.order_creation__c >= '2015-09-01'
and owm.acquisition_channel__c = 'web'
and owm.test__c = '0'

order by owm.order_creation__c::timestamp::date asc