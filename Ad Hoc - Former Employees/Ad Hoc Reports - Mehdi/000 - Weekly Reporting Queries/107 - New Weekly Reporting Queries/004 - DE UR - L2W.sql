SELECT
	EXTRACT(WEEK from mindate) as weeknum,
	LEFT(delivery_area,2) as locale,
	SUM(CASE WHEN worked_hours < weekly_hours THEN worked_hours ELSE weekly_hours END) / SUM(weekly_hours) as ur

FROM bi.gpm_weekly_cleaner

WHERE EXTRACT(WEEK from mindate) >= (EXTRACT(WEEK from current_date)-2)
	AND EXTRACT(WEEK from mindate) < (EXTRACT(WEEK from current_date))
	AND EXTRACT(YEAR from mindate) = EXTRACT(YEAR from current_date)

GROUP BY weeknum, locale

ORDER BY locale asc, weeknum asc