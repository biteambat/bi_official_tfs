SELECT
	EXTRACT(WEEK from effectivedate) as weeknum,
	LEFT(locale__c,2) as locale,
	((SUM(gmv_eur_net)/SUM(order_duration__c))-(24.42))/(SUM(gmv_eur_net)/SUM(order_duration__c)) as gpm

FROM bi.orders

WHERE test__c = '0'
	AND status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESIONNAL')
	AND EXTRACT(WEEK from effectivedate) >= (EXTRACT(WEEK from current_date)-2)
	AND EXTRACT(WEEK from effectivedate) < (EXTRACT(WEEK from current_date))
	AND EXTRACT(YEAR from effectivedate) = EXTRACT(YEAR from current_date)
	AND LEFT(locale__c,2) = 'ch'
	AND order_type = '1'

GROUP BY weeknum, locale

ORDER BY locale asc, weeknum asc