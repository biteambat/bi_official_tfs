SELECT
	EXTRACT(WEEK from effectivedate) as weeknum,
	LEFT(locale__c,2) as locale,
	SUM(gmv_eur_net) as invoiced_gmv_net

FROM bi.orders

WHERE test__c = '0'
	AND status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESIONNAL')
	AND EXTRACT(WEEK from effectivedate) >= (EXTRACT(WEEK from current_date)-2)
	AND EXTRACT(WEEK from effectivedate) < (EXTRACT(WEEK from current_date))
	AND EXTRACT(YEAR from effectivedate) = EXTRACT(YEAR from current_date)

GROUP BY weeknum, locale

ORDER BY locale asc, weeknum asc