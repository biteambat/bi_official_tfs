SELECT
	EXTRACT(WEEK from mindate) as weeknum,
	LEFT(delivery_area,2) as locale,
	CASE WHEN SUM(salary_payed) > 0 THEN (SUM(revenue)-SUM(salary_payed))/SUM(salary_payed) ELSE NULL END as gpm

FROM bi.gpm_weekly_cleaner

WHERE EXTRACT(WEEK from mindate) >= (EXTRACT(WEEK from current_date)-2)
	AND EXTRACT(WEEK from mindate) < (EXTRACT(WEEK from current_date))
	AND EXTRACT(YEAR from mindate) = EXTRACT(YEAR from current_date)

GROUP BY weeknum, locale

ORDER BY locale asc, weeknum asc