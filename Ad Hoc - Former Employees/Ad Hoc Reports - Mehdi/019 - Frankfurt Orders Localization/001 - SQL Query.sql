﻿SELECT 
	o.shippingcity,
	count(distinct o.id)
FROM
	bi.orders_w_marketing o
WHERE
	o.test__c = '0'
	and o.status in ('INVOICED')
	and o.effectivedate >= '2015-12-22'
	and (o.shippingpostalcode like '60%' or o.shippingpostalcode like '63%' or o.shippingpostalcode like '65%')
	and o.city like 'DE-Frankfurt am Main%'

GROUP BY o.shippingcity

ORDER BY o.shippingcity asc