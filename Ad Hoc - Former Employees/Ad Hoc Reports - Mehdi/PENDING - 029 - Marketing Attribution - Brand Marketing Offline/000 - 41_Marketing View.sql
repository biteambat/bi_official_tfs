DROP VIEW IF EXISTS bi.Orders_w_Marketing;
CREATE VIEW bi.Orders_w_Marketing as 
SELECT
  t.*,
  CASE
  WHEN ((Acquisition_Channel_Params__c like '%goob%' or Acquisition_Channel_Params__c like '%ysmb%b') and (Voucher__c is null or Voucher__c = '' or Voucher__c like '%WISCHMOP%' or Voucher__c = 'WISCH1SEB' or Voucher__c = 'WISCH1SEM' or Voucher__c = 'TIGER1SEB') and Acquisition_Channel_Params__c not like '%disp%')  or Voucher__c = 'WISCH1SEB' or Voucher__c = 'SEBTIGER20' or Voucher__c  = 'SEBTIGER40' or Voucher__c = 'TIGER1SEB' THEN 'SEM Brand'
  WHEN ((Acquisition_Channel_Params__c like '%goog%' or Acquisition_Channel_Params__c like '%ysm%' or Acquisition_Channel_Ref__c like '%clid=goog%') and Acquisition_Channel_Params__c not like '%disp%'  and (Voucher__c = 'WISCH1SEM' or Voucher__c is null or Voucher__c = '' or VOucher__c = 'WISCHMOP3C' or Voucher__c = 'WISCHMOP3A')) or ((Voucher__c = 'WISCH1SEM' or Voucher__C = 'TIGER1SEM' ))THEN 'SEM'
  WHEN ((Acquisition_Channel_Params__c like '%disp%' and Acquisition_Channel_Params__c not like '%remarketing%' and Acquisition_Channel_Params__c not like '%Facebook%') and (Voucher__c = 'WISCH1GDN' or Voucher__c is null or Voucher__c = '' or Voucher__c = 'WISCH1SOC' or Voucher__c ='WISCH1GSP')) or Voucher__c = 'WISCH1SOC' or Voucher__c ='WISCH1GSP' or Voucher__c = 'WISCH1GDN' or Voucher__c = 'WISCH1CR' or Voucher__c = 'DSPUTZ20' or Voucher__c = 'TIGER1GDN' or Voucher__c = 'GDTIGER20' or Voucher__c = 'GDTIG20' THEN 'Display'
  WHEN Acquisition_Channel_Params__c like '%disp%' and Acquisition_Channel_Params__c  like '%remarketing%' and Acquisition_Channel_Params__c not like '%Facebook%' THEN 'Display Remarketing'
  WHEN (Acquisition_Channel_Params__c like '%ytbe%' and (Voucher__c is null or Voucher__c = '' or Voucher__c = 'WISCH1YOU')) or Voucher__c = 'WISCH1YOU' THEN 'Youtube Paid' 
  WHEN (Acquisition_Channel_Ref__c like '%google%' or Acquisition_Channel_Ref__c like '%yahoo%' or Acquisition_Channel_Ref__c like '%bing%' or Acquisition_Channel_Ref__c like '%yandex%' or 
       Acquisition_Channel_Ref__c like '%naver%' or Acquisition_Channel_Ref__c like '%baidu%' or Acquisition_Channel_Ref__c like '%ask%' or Acquisition_Channel_Ref__c like '%duckduckgo%' )
       AND Acquisition_Channel_Ref__c not like '%tiger%' and Acquisition_Channel_Params__c not like '%goog%'and Acquisition_Channel_Params__c not like '%goob%' and Acquisition_Channel_Ref__c not like '%goob%' and Acquisition_Channel_Ref__c not like '%goob%'
       AND Acquisition_Channel_Params__c not like '%ysm%' and Acquisition_Channel_Params__c not like '%ysmb%'and Acquisition_Channel_Params__c not like '%disp%' and (Voucher__c is null or Voucher__c = '')THEN 'SEO'      
  WHEN ((Acquisition_Channel_Ref__c like '%google%' or Acquisition_Channel_Ref__c like '%yahoo%' or Acquisition_Channel_Ref__c like '%bing%' or Acquisition_Channel_Ref__c like '%yandex%' or 
       Acquisition_Channel_Ref__c like '%naver%' or Acquisition_Channel_Ref__c like '%baidu%' or Acquisition_Channel_Ref__c like '%ask%' or Acquisition_Channel_Ref__c like '%duckduckgo%')
       AND Acquisition_Channel_Ref__c like '%tiger%' and Acquisition_Channel_Params__c not like '%goog%' and Acquisition_Channel_Params__c not like '%goob%' and Acquisition_Channel_Ref__c not like '%goob%'
            and Acquisition_Channel_Params__c not like '%ysm%' and Acquisition_Channel_Params__c not like '%ysmb%' and Acquisition_Channel_Params__c not like '%disp%' and (Voucher__c is null or Voucher__c = '' or Voucher__c = 'WISCH1ABC' or Voucher__C = 'TIGER1ABC')) or Voucher__c = 'WISCH1ABC' or Voucher__C = 'TIGER1ABC'   THEN 'SEO Brand'
  WHEN ((Acquisition_Channel_Params__c like '%batfb%' or Acquisition_Channel_Params__c like '%batfb%' or Acquisition_Channel_Ref__c like '%batfb%' or Acquisition_Channel_Params__c like '%facebook%' or Acquisition_Channel_Ref__c like '%facebook%') and (Acquisition_Channel_Params__c like '%utm%' or Acquisition_Channel_Ref__c like '%utm_campaign%' or Acquisition_Channel_Params__c like '%lead-gen%' or Acquisition_Channel_Params__c like '%fblg%') and (Voucher__c is null or Voucher__c = '' or Voucher__c = 'WISCHMOP3C' or Voucher__c = 'WISCH1FB' or Voucher__c = 'RABATT20' or Voucher__c = 'FBPRIMA20' or Voucher__c = 'FBPUTZCH40' or Voucher__c = 'FBPUTZ20' or Voucher__c = 'FBPUTZ15' or Voucher__c = 'FBTIGER50' or Voucher__c = 'FBTIGER20' or Voucher__c = 'STAR50' or Voucher__c = 'FBTIGER20' or Voucher__c = 'FBTIGER30' or Voucher__c = 'FBTIGER40' or Voucher__c = 'FBTIGER15' or Voucher__C = 'FBTIG70' )) or Voucher__c = 'WISCH1FB' or Voucher__c = 'RABATT20' or Voucher__c = 'FBPRIMA20' or Voucher__c = 'FBPUTZCH40' or Voucher__c = 'FBPUTZ20' or Voucher__c = 'TIGER1FB' or Voucher__c = 'FBTIGER15' OR Voucher__c = 'FBPUTZ15' or Voucher__c = 'FBTIGER50' or Voucher__c = 'FBTIGER20' or Voucher__c = 'STAR50' or Voucher__c = 'FBTIGER30' or Voucher__c = 'FBTIGER40' or Voucher__c = 'FBTIGER15' or Voucher__C = 'FBTIG70' THEN 'Facebook'  
  WHEN Acquisition_Channel_Params__c not like '%goog%' and Acquisition_Channel_Params__c not like '%ysm%' and Acquisition_Channel_Params__c not like '%disp%' and Acquisition_Channel_Params__c not like '%ytbe%' and Acquisition_Channel_Params__c not like '%fb%' and (Voucher__c > '0') then 'Voucher Campaigns'
  WHEN (Acquisition_Channel_Params__c not like '%batfb%' and Acquisition_Channel_Ref__c not like '%batfb%' and (Acquisition_Channel_Params__c like '%facebook%' or Acquisition_Channel_Ref__c like '%facebook%')) and Voucher__c is null  THEN 'Facebook Organic'  
  WHEN Acquisition_Channel_Params__c like '%newsletter%' or Acquisition_Channel_Params__c like '%email%' or Acquisition_Channel_Params__c like '%vero%' or Acquisition_Channel_Params__c like '%batnl%' or Acquisition_Channel_Params__c like '%fullname%' or Acquisition_Channel_Params__c like '%invoice%'THEN 'Newsletter'
  WHEN Acquisition_Channel_Params__c not like '%goog%' AND Acquisition_Channel_Params__c not like '%ysm%' AND Acquisition_Channel_Params__c not like '%disp%' AND Acquisition_Channel_Params__c not like '%ytbe%' AND Acquisition_Channel_Params__c not like '%fb%'  AND Acquisition_Channel_Params__c not like '%clid%' AND Acquisition_Channel_Params__c not like '%utm%' and Acquisition_Channel_Params__c != '' THEN 'DTI'
  ELSE 'Unattributed'
  END as Marketing_Channel,
  
  	CASE
	-- ++++++++++ DE ++++++++++
	WHEN LEFT(ShippingPostalCode,2) in ('52') and LEFT(Locale__c,2) = 'de' THEN 'DE-Aachen'
	WHEN (LEFT(ShippingPostalCode,2) in ('10','11','12','13','14','15') or LEFT(ShippingPostalCode,3) in ('140','141','144','145','153')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Berlin'
	WHEN (LEFT(ShippingPostalCode,3) in ('320') or LEFT(ShippingPostalCode,2) in ('33')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Bielefeld'
	WHEN LEFT(ShippingPostalCode,3) in ('530','531','532','533','537','538') and LEFT(Locale__c,2) = 'de' THEN 'DE-Bonn'
	WHEN (LEFT(ShippingPostalCode,2) = '28' or LEFT(ShippingPostalcode,3) in ('277')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Bremen'
	WHEN LEFT(ShippingPostalCode,3) in ('502','503','504','505','506','507','508','509','510','511','513','514') and LEFT(Locale__c,2) = 'de' THEN 'DE-Cologne'
	WHEN LEFT(ShippingPostalCode,3) in ('642','643') and LEFT(Locale__c,2) = 'de' THEN 'DE-Darmstadt'
	WHEN (LEFT(ShippingPostalCode,2) = '44' or ShippingPostalCode in ('441','442','443','445','457','580','582','583','584')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Dortmund'
	WHEN LEFT(ShippingPostalCode,2) = '01' and LEFT(Locale__c,2) = 'de' THEN 'DE-Dresden'
	WHEN LEFT(ShippingPostalCode,3) in ('454','461','470','471','472') and LEFT(Locale__c,2) = 'de' THEN 'DE-Duisburg'
	WHEN LEFT(ShippingPostalCode,2) = '47' and LEFT(Locale__c,2) = 'de' THEN 'DE-Duisburg+'
	WHEN (LEFT(ShippingPostalCode,2) = '40' or LEFT(ShippingPostalCode,3) in ('414','415')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Dusseldorf'
	WHEN (LEFT(ShippingPostalCode,5) in ('44866','44867','46047','46236','46238','46240') or LEFT(ShippingPostalCode,2) = '45') and LEFT(Locale__c,2) = 'de' THEN 'DE-Essen'
	WHEN LEFT(ShippingPostalCode,3) in ('446','448','462') and LEFT(Locale__c,2) = 'de' THEN 'DE-Essen+'
	WHEN (LEFT(ShippingPostalCode,5) in ('61118','61440','61449','63065','63067','63069','63071','63073','63075','63150','63165','63263','63303','63477','65451','65760','65824','65843','65929','65931','65933''65934','65936') OR LEFT(ShippingPostalCode,2) = '60') and LEFT(Locale__c,2) = 'de' THEN 'DE-Frankfurt am Main'
	WHEN (LEFT(ShippingPostalCode,3) in ('611','612','613','614','630','631','632','633','634','654','657','658','659') and LEFT(Locale__c,2) = 'de') THEN 'DE-Frankfurt am Main+'
	WHEN LEFT(ShippingPostalCode,2) = '79' and LEFT(Locale__c,2) = 'de' THEN 'DE-Freiburg'
	WHEN LEFT(ShippingPostalCode,2) = '59' and LEFT(Locale__c,2) = 'de' THEN 'DE-Halle'
	WHEN LEFT(ShippingPostalCode,2) = '58' and LEFT(Locale__c,2) = 'de' THEN 'DE-Hagen'
	WHEN LEFT(ShippingPostalCode,2) in ('20','22') or LEFT(ShippingPostalCode,5) in ('21031','21075','21077','21079''21107','21109','21129') THEN 'DE-Hamburg'
	WHEN LEFT(ShippingPostalCode,2) in ('21') and LEFT(Locale__c,2) = 'de' THEN 'DE-Hamburg+' 
	WHEN LEFT(ShippingPostalCode,3) = '483' and LEFT(Locale__c,2) = 'de' THEN 'DE-Hamm'
	WHEN (LEFT(ShippingPostalCode,2) = '30' or LEFT(ShippingPostalCode,3) = '311') and LEFT(Locale__c,2) = 'de' THEN 'DE-Hanover'
	WHEN LEFT(ShippingPostalCode,2) = '24' and LEFT(Locale__c,2) = 'de' THEN 'DE-Kiel' 
	WHEN (LEFT(ShippingPostalCode,2) = '76' or LEFT(ShippingPostalCode,3) in ('751','750')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Kalsruhe'
	WHEN LEFT(ShippingPostalCode,3) in ('041','042','043','044') and LEFT(Locale__c,2) = 'de' THEN 'DE-Leipzig'
	WHEN (LEFT(ShippingPostalCode,2) in ('23') or LEFT(ShippingPostalCode,3) in ('192')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Lübeck'
	WHEN (LEFT(ShippingPostalCode,5) in ('55116','55118','55120','55122','55124','55126','55127','55128','55129','55130','55131','55246','55252','55257','55294','65201','65203','65239','65462','65474')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Mainz'
	WHEN LEFT(ShippingPostalCode,3) in ('550','551','552','650','651','652','653','655') and LEFT(Locale__c,2) = 'de' THEN 'DE-Mainz+'
	WHEN LEFT(ShippingPostalCode,5) in ('67059',	'67061',	'67063',	'67065',	'67067',	'67069',	'67071',	'67122',	'67141',	'68159',	'68161',	'68163',	'68165',	'68167',	'68169',	'68199',	'68219',	'68229',	'68239',	'68259',	'68305',	'68307',	'68309',	'68549') and LEFT(Locale__c,2) = 'de' THEN 'DE-Mannheim'
	WHEN (LEFT(ShippingPostalCode,3) in ('670',	'671',	'672',	'673',	'690',	'691',	'692',	'694',	'699') or LEFT(ShippingPOstalCode,2) = '68') and LEFT(Locale__c,2) = 'de' THEN 'DE-Mannheim+'
	WHEN LEFT(ShippingPostalCode,2) = '39' and LEFT(Locale__c,2) = 'de' THEN 'DE-Magdeburg'
	WHEN LEFT(ShippingPostalCode,5) in ('41061',	'41063',	'41065',	'41066',	'41068',	'41069',	'41169',	'41179',	'41189',	'41199',	'41236',	'41238',	'41239',	'41352') and LEFT(Locale__c,2) = 'de' THEN 'DE-Mönchengladbach'
	WHEN LEFT(ShippingPostalCode,2) in ('41') and LEFT(Locale__c,2) = 'de' THEN 'DE-Mönchengladbach+'
	WHEN LEFT(locale__c,2) = 'de' and LEFT(ShippingPostalCode,2) in ('80','81') or LEFT(ShippingPostalCode,5) in ('82008',	'82024',	'82031',	'82041',	'82049',	'82061',	'82166',	'85521',	'85579',	'85609',	'85737',	'85774') THEN 'DE-Munich'
	WHEN LEFT(ShippingPostalCode,3) in  ('480','481') and LEFT(Locale__c,2) = 'de' THEN 'DE-Munster' 
	WHEN LEFT(ShippingPostalCode,2) = '90' and LEFT(Locale__c,2) = 'de' THEN 'DE-Nuremberg'
	WHEN LEFT(ShippingPostalCode,3) in ('910','911','912','913') and LEFT(Locale__c,2) = 'de' THEN 'DE-Nuremberg+'
	WHEN LEFT(ShippingPostalCode,3) in ('930','931') and LEFT(Locale__c,2) = 'de' THEN 'DE-Regensburg'
	WHEN LEFT(ShippingPostalCode,3) in ('180','181','182') and LEFT(Locale__c,2) = 'de' THEN 'DE-Rostock'
	WHEN LEFT(ShippingPostalCode,3) in ('660','661','662','663') and LEFT(Locale__c,2) = 'de' THEN 'DE-Saarbrücken'
	WHEN (LEFT(ShippingPostalCode,2) in ('70') or LEFT(ShippingPostalCode,5) in ('71254',	'73728',	'73732',	'73733',	'73734',	'73760')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Stuttgart'
	WHEN (LEFT(ShippingPostalCode,3) in ('710',	'712',	'716',	'737',		'720',	'721',	'725',	'726',	'727',	'730',	'732',	'736',	'743',	'752',	'753',	'754') OR LEFT(ShippingPostalCode,2) in ('71')) and LEFT(Locale__c,2) = 'de' THEN 'DE-Stuttgart+'
	WHEN (LEFT(ShippingPostalCode,3) in ('312') or LEFT(ShippingPostalCode,2) in ('38')) and LEFT(Locale__c,2) = 'de' THEN 'DE-WBurgBschweig'
	WHEN LEFT(ShippingPostalCode,2) in ('42') and LEFT(Locale__c,2) = 'de' THEN 'DE-Wuppertal'
	WHEN LEFT(ShippingPostalCode,3) in ('970','971','972') and LEFT(Locale__c,2) = 'de' THEN 'DE-Würzburg'
	
	-- ++++++++++ AT ++++++++++
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,1) = '1' THEN 'AT-Vienna'
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,2) = '50' THEN 'AT-Salzburg'
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,2) = '60' THEN 'AT-Insbruck'
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,2) = '40' THEN 'AT-Linz'
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,2) = '80' THEN 'AT-Graz'
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,2) = '90' THEN 'AT-Klagenfurt'
   WHEN LEFT(Locale__c,2) = 'at' AND LEFT(ShippingPostalCode,1) in ('2','3','4','5','6','7','8','9') THEN 'AT-Other'
   
	-- ++++++++++ CH ++++++++++  
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,3) in ('420','422','424','440','441','442') THEN 'CH-Basel'
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,3) in ('341','350','351') THEN 'CH-Bern'
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,3) in ('111','112','150','151','160','161','167','169','180') THEN 'CH-Lausanne'
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,3) in ('564','610','620','627','628','630','631','634','635','636','637','638','640','641') THEN 'CH-Lucerne'
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,3) in ('520','910','911','920','921','922','923','924','930','931','932','940','941','942','943','945') THEN 'CH-St.Gallen'
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,3) in ('542','543','545','562','824','830','831','832','833','835','840','841','842','844','845','847','848','849','860','861','870','880','881','890','891','893','894','895','896','954') THEN 'CH-Zurich'
   WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) in ('40','41') THEN 'CH-Basel'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) in ('30','31','32','33','34') THEN 'CH-Bern'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) = '25' THEN 'CH-Biel'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) = '12' THEN 'CH-Geneva'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) in ('10','13') THEN 'CH-Lausanne'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) = '60' THEN 'CH-Lucerne'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) = '90' THEN 'CH-St.Gallen'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,2) in ('80','81','85') THEN 'CH-Zurich'
	WHEN LEFT(Locale__c,2) = 'ch' AND LEFT(ShippingPostalCode,1) in ('1','2','3','4','5','6','7','8','9') THEN 'CH-Other'
	
	-- ++++++++++ NL ++++++++++
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '18' THEN 'NL-Alkmaar'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '13' THEN 'NL-Almere'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '38' THEN 'NL-Amersfoort'	   
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('10','11','14','15','21') THEN 'NL-Amsterdam'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('48','50') THEN 'NL-Breda-Tillburg'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('54','55','56','57') THEN 'NL-Eindhoven'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('27','28') THEN 'NL-Gouda'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('19','20') THEN 'NL-Haarlem'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '52' THEN 'NL-Hertogenbosch'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '12' THEN 'NL-Hilversum'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '23' THEN 'NL-Leiden'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) = '62' THEN 'NL-Maastricht'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('30','31') THEN 'NL-Rotterdam'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('22','24','25','26') THEN 'NL-The Hague'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('34','35','36','37','39') THEN 'NL-Utrecht'
   WHEN LEFT(Locale__c,2) = 'nl' AND LEFT(ShippingPostalCode,2) in ('3','4','5','6','7','8','9') THEN 'NL-Other'
  ELSE 'DE-Other'
  END as City,
  CASE 
  WHEN LEFT(locale__c,2) = 'ch' and Order_Creation__c::date < '2015-10-01' THEN GMV__c*0.96
  WHEN LEFT(locale__c,2) = 'ch' and Order_Creation__c::date >= '2015-10-01' THEN GMV__c*0.92 ELSE GMV__c END GMV_Eur

FROM
  Salesforce.Order t