﻿Select 

o.effectivedate,
left(o.locale__c, 2) as locale,
o.recurrency__c,
o.acquisition_new_customer__c as acquisition,
SUM(Case when (o.acquisition_channel__c != 'recurrent' and o.recurrency__c >= '7' and o.status not in ('CANCELLED FAKED','CANCELLED CUSTOMER')) then 1 else NULL END) as new_recurrent_orders,
SUM(Case when (o.acquisition_channel__c != 'recurrent' and o.recurrency__c >= '7' and o.status not in ('CANCELLED FAKED','CANCELLED CUSTOMER','CANCELLED CUSTOMER','CANCELLED TERMINATED','ERROR GEO')) then 1 else NULL END) as new_valid_recurrent_orders

From bi.orders_w_marketing o

where o.test__c = '0'
and recurrency__c > '0'
and o.effectivedate <= current_date
and o.effectivedate >= current_date - interval '6 months'

group by o.effectivedate, locale, o.recurrency__c, acquisition

order by o.effectivedate desc, locale asc, o.recurrency__c asc