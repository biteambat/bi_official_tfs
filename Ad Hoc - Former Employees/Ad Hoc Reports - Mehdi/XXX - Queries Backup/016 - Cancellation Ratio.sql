select
	sum(case when o.status like '%CANCELLED%' then 1 else null end) as Nb_Cancels,
	sum(case when (o.status like 'INVOICED' or o.status like '%CANCELLED%') then 1 else null end) as invoicedandcancels
	
from bi.orders_w_marketing o
where 
	o.city in ('DE-Munich')
	and o.effectivedate::date >= '2016-01-04' and o.effectivedate::date < '2016-01-11'
	and o.test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')