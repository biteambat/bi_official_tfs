DROP TABLE IF EXISTS bi.cancelledorders_temp1;
CREATE TABLE bi.cancelledorders_temp1 as 
SELECT
	order_json->>'Id' as sfid,
	event_name,
	created_at as time_of_cancellation
FROM
	events.sodium
WHERE
	 event_name in ('Order Event:CANCELLED CUSTOMER')
	 and created_at::date >= '2015-12-01'::date;

SELECT

can.time_of_cancellation::date as cancellation_date,
count(can.sfid) as number_cancelled

FROM 

bi.cancelledorders_temp1 can

GROUP BY can.time_of_cancellation::date
ORDER BY can.time_of_cancellation::date desc