SELECT

left(o.city,2) as country,
o.city,

round(cast(sum(
	case when o.status in ('INVOICED')
		then o.gmv_eur 
		else 0 end) as numeric),2) as "invoiced_gmv"

FROM

bi.orders_w_marketing o

WHERE o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
AND o.test__c = '0'

GROUP BY o.city
ORDER BY o.city asc