select

owm.city as city,
extract(week from owm.effectivedate) as "week",
extract(month from owm.effectivedate) as "month",
extract(year from owm.effectivedate) as "year",
round(cast(sum(case when (owm.recurrency__c > 6) then owm.gmv_eur else 0 end) as numeric),2) as "recurrent_GMV_€",
round(cast(sum(case when (owm.recurrency__c <= 6) then owm.gmv_eur else 0 end) as numeric),2) as "one_off_GMV_€",
round(cast(sum(owm.gmv_eur) as numeric),2) as "total_GMV_€",
round(cast(sum(case when (owm.recurrency__c > 6) then owm.gmv_eur else 0 end) / sum(owm.gmv_eur) as numeric),4) as "recurrent_share",
round(cast(sum(owm.gmv_eur)/sum(owm.order_duration__c) as numeric),2) as "PPH Overall €",
round(cast(sum(case when (owm.recurrency__c > 6) then owm.gmv_eur else null end) / sum(case when (owm.recurrency__c > 6) then owm.order_duration__c else null end) as numeric),2) as "PPH Recurrent €",
round(cast(sum(case when (owm.recurrency__c <= 6) then owm.gmv_eur else null end) / sum(case when (owm.recurrency__c <= 6) then owm.order_duration__c else null end) as numeric),2) as "PPH One-off €",
round(cast(sum(owm.order_duration__c) / count(distinct owm.professional__c) as numeric), 2) as "hours_p_active_cleaner"

from bi.orders_w_marketing owm

where owm.effectivedate >= '2015-11-01' and owm.effectivedate <= '2016-01-31'
and owm.acquisition_channel__c = 'web'
and owm.status in ('INVOICED')
and owm.test__c = '0'
and left(owm.city,2)='DE'

group by owm.city, extract(month from owm.effectivedate), extract(year from owm.effectivedate), extract(week from owm.effectivedate)
order by owm.city asc, extract(year from owm.effectivedate) asc, extract(month from owm.effectivedate) asc, extract(week from owm.effectivedate) asc