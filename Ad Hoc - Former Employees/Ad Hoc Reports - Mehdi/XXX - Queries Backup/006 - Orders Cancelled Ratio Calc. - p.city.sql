SELECT

left(o.city,2) as country,
o.city,

cast(sum(case when o.status like '%CANCELLED%' then 1 else 0 end) as numeric) as orders cancelled,
cast(count(o.id) as numeric) as numeric) as total_orders
FROM

bi.orders_w_marketing o

WHERE o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
AND o.test__c = '0'

GROUP BY o.city
ORDER BY left(o.city,2) asc, o.city asc