select
	sum(case when o.status in ('CANCELLED NO MANPOWER') then 1 else null end) as Nb_No_MNP,
	sum(case when o.status in ('INVOICED','CANCELLED NO MANPOWER') then 1 else null end) as invoiced__NO_MNP
	
from bi.orders_w_marketing o
where 
	o.city in ('DE-Essen')
	and o.effectivedate::date >= '2016-01-04' and o.effectivedate::date < '2016-01-11'
	and o.test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')