SELECT
	extract(WEEK from o.order_creation__c) as acquisition_week,
	extract(YEAR from o.order_creation__c) as acquisition_year,
	count(1) as SEM_acquisitions,
	replace(o.city,'+','') as city
	
FROM

bi.orders_w_marketing o

WHERE
	o.marketing_channel like 'SEM'
	and test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and o.order_creation__c >= '2015-12-01'
	and acquisition_new_customer__c = '1'
	and o.acquisition_channel__c in ('web')
	and replace(o.city,'+','') in ('DE-Berlin','DE-Hamburg','DE-Frankfurt am Main','DE-Cologne','DE-Dusseldorf','DE-Stuttgart','DE-Nuremberg')

GROUP BY
	replace(o.city,'+',''),
	acquisition_year,
	acquisition_week

ORDER BY
	replace(o.city,'+','') asc,
	acquisition_year desc,
	acquisition_week desc
	