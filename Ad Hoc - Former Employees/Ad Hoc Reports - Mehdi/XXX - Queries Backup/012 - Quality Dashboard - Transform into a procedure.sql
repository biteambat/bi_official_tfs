DROP TABLE IF EXISTS OWM_w_Muffins_Names;
CREATE TABLE OWM_w_Muffins_Names as

SELECT 
	o.id as "orderID",
	o.status,	
	o.effectivedate,
	o.rating_professional__c, -- Ask Alex whether this is accurate or not !!! 
-- Manually entered ratings through phone calls are not taken into account in Salesforce' averages.
-- <=> IF the manual ratings are in OWM then it's correct
	o.city, -- See with Alex how come we have a guy appearing in Bonn, Cologne, and Aachen. Check whether we should take the working_city of the cleaner instead of the order city.
	o.professional__c,
	acc.name,
	acc.email__c,
	acc.hr_contract_start__c::timestamp::date
	
FROM
	bi.orders_w_marketing o

JOIN
	salesforce.account acc
		ON o.professional__c = acc.sfid 

WHERE 
	o.type = '60'
	and o.test__c = '0'
	and o.status in ('INVOICED','CANCELLED PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM','NO SHOW PROFESSIONAL')

ORDER BY 
	acc.name asc,
	o.effectivedate desc

;