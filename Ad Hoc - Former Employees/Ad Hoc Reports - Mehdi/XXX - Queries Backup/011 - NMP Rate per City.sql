SELECT

left(o.city,2) as country,
o.city,

CASE WHEN 
	sum(case when o.status in ('INVOICED','CANCELLED NO MANPOWER') then 1 else 0 end) = 0 THEN 0 
	ELSE round(cast(sum(case when o.status like 'CANCELLED NO MANPOWER' then 1 else 0 end) as numeric) /  sum(case when o.status in ('INVOICED','CANCELLED NO MANPOWER') then 1 else 0 end),2) END as NMP_rate

FROM

bi.orders_w_marketing o

WHERE o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
AND o.test__c = '0'

GROUP BY o.city
ORDER BY left(o.city,2) asc, o.city asc