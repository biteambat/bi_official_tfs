SELECT

cast(count(o.id) as numeric) as total_invoiced_orders,
cast(count(distinct o.professional__c) as numeric) as total_active_cleaners,
round(cast(count(o.id) as numeric) / cast(count(distinct o.professional__c) as numeric),2) as ratio_whole_period,
round(cast(count(o.id) as numeric) / cast(count(distinct o.professional__c) as numeric) / (cast((current_date::date - '2016-01-01'::date)as numeric)/7),4) as ratio_per_week,
round(cast(count(o.id) as numeric) / cast(count(distinct o.professional__c) as numeric) / (current_date::date - '2016-01-01'::date),4) as ratio_per_day

FROM bi.orders_w_marketing o

where o.test__c = '0'
and o.status in ('INVOICED')
and left(o.city,2) in ('NL')
and o.effectivedate between '2016-01-01' and current_date