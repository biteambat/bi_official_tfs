SELECT 
	sf.hr_contract_start__c::date as date,
	left(sf.working_city__c,2) as locale,
	replace(sf.working_city__c,'+','') as city_group,
	'Nb of onboarding cleaners' as kpi,
	count(distinct sf.id) as value

FROM 
	salesforce.account sf

WHERE 
	sf.type__c = '60'
	AND sf.test__c = '0'

GROUP BY
	city_group,
	locale,
	date

ORDER BY
	locale asc,
	city_group asc,
	date desc