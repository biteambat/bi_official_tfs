SELECT
	sum(o.gmv_eur)
FROM
	bi.orders_w_marketing o
WHERE
	o.status in ('INVOICED','CANCELLED CUSTOMER SHORTTERM','CANCELLED PROFESSIONAL SHORTTERM','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
	AND o.city = 'DE-Berlin'
	AND o.effectivedate::date >= '2016-01-04' and o.effectivedate::date < '2016-01-11'
	AND o.test__c = '0'