DROP TABLE IF EXISTS bi.cancelledorders_temp1;
CREATE TABLE bi.cancelledorders_temp1 as 
SELECT
	order_json->>'Id' as sfid,
	event_name,
	created_at as time_of_cancellation
FROM
	events.sodium
WHERE
	 event_name in ('Order Event:CANCELLED CUSTOMER')
	 and created_at::date >= '2015-12-01'::date;

SELECT
	t1.sfid,
	t2.order_creation__c::timestamp::date as creation_date,
	t2.effectivedate,
	t2.order_start__c as order_start_date_full,
	t2.order_start__c::date as order_start_date,
	extract(hour from t2.order_start__c) as order_start_hour,
	extract(minute from t2.order_start__c) as order_start_min,
	time_of_cancellation,
	time_of_cancellation::date as cancellation_date,
	extract(hour from time_of_cancellation) as cancellation_hour,
	extract(minute from time_of_cancellation) as cancellation_minute,
	replace(cast(t2.gmv_eur as text),'.',',') as gmv_eur,

	extract(week from t2.order_creation__c) as creation_week,
	extract(year from t2.order_creation__c) as creation_year,
	extract(DOW from t2.order_creation__c) as creation_DOW,

	t2.acquisition_channel__c as acquisition_channel,
	t2.marketing_channel as marketing_channel,
	t2.payment_method__c as payment_method,
	t2.status as status,
	t2.city
	
FROM
	bi.cancelledorders_temp1 t1
JOIN
	bi.orders_w_marketing t2
ON
	(t1.sfid = t2.sfid)
WHERE t2.order_start__c > '2015-12-01'
	and t2.test__c = '0'
	and left(t2.locale__c,2) = 'de'
	and t2.acquisition_channel__c in ('web','recurrent')