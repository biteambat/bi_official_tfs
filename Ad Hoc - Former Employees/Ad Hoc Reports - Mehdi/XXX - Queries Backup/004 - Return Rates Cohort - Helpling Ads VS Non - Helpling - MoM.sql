----------------------------------------------------- Creating the first temporary table

DROP TABLE IF EXISTS bi.temp_cust_acquired_SEM;
CREATE TABLE bi.temp_cust_acquired_SEM AS

	SELECT
		o.order_creation__c as date,
		extract(month from o.order_creation__c) as month,
		extract(year from o.order_creation__c) as year,
		o.customer_id__c as customer_id,
		CASE WHEN o.Acquisition_Channel_Ref__c like '%helpling%' THEN 'helpling' ELSE 'non-helpling' END as SEM_acquisition

	FROM 
		bi.orders_w_marketing o

	WHERE
		o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
		and test__c = '0'
		and o.acquisition_new_customer__c = '1'
		and o.marketing_channel = 'SEM'	
		and acquisition_channel__c in ('web')
		and Voucher__c > '0'
								--------------------> ARE THESE FILTERS OK ? 
		
	ORDER BY 
		year desc,
		month desc;

--------------------------------------------------------- Creating the final table

DROP TABLE IF EXISTS temp_pre_cohort_helpling;
CREATE TABLE temp_pre_cohort_helpling AS

	SELECT
		tempo.SEM_acquisition as SEM_acquisition,
		tempo.date as acquisition_date,
		tempo.customer_id,
		o.order_creation__c as rebooking_date,
		extract(month from o.order_creation__c) as rebooking_month,
		extract(year from o.order_creation__c) as rebooking_year

	FROM

		temp_cust_acquired_SEM tempo

	JOIN bi.orders_w_marketing o
		ON (tempo.customer_id = o.customer_id__c)
WHERE
	o.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
	GROUP BY
		tempo.month,
		tempo.year,
		acquisition_date,
		tempo.customer_id,
		rebooking_date,
		rebooking_month,
		rebooking_year,
		SEM_acquisition

	ORDER BY
		SEM_acquisition asc,
		tempo.customer_id asc,
		tempo.year desc,
		tempo.month desc,
		rebooking_year asc,
		rebooking_month asc,
		acquisition_date asc;