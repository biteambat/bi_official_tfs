SELECT

	extract(month from o.order_creation__c) as ordermonth,
	extract(year from o.order_creation__c) as orderyear,
	SUM(CASE WHEN o.Acquisition_Channel_Params__c like '%helpling%' THEN 1 ELSE 0 END) as helpling,
	SUM(CASE WHEN o.Acquisition_Channel_Params__c not like '%helpling%' THEN 1 ELSE 0 END) as non_helpling


FROM

	bi.orders_w_marketing o

WHERE

	o.marketing_channel like 'SEM'
	and o.acquisition_channel__c in ('web')
	and o.order_creation__c >= '2015-12-01'
	and test__c = '0'
	and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and o.acquisition_new_customer__c = '1'

GROUP BY 
	ordermonth, orderyear

ORDER BY 
	orderyear desc, ordermonth desc