SELECT

left(o.city,2) as country,
o.city,

count(distinct o.professional__c) as Nb_active_cleaners

FROM

bi.orders_w_marketing o

WHERE o.test__c = '0'
AND o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE') 

GROUP BY o.city
ORDER BY left(o.city,2) asc, o.city asc