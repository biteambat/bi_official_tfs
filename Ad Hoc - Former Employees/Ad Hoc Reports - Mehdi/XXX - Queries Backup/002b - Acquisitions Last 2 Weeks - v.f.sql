﻿SELECT

o.marketing_channel,

(cast(current_date as date) - interval '14 days')::timestamp::date as previousW_starting_at,
(cast(current_date as date) - interval '7 days')::timestamp::date as currentW_starting_at,

Sum
(case when o.order_creation__c between 
		(cast(current_date as date) - interval '14 days')
		and 
		(cast(current_date as date) - interval '7 days')
				then 1 else 0 end)
		as "acquisitions_previous_week",

Sum
(case when o.order_creation__c between 
		(cast(current_date as date) - interval '7 days')
		and 
		(cast(current_date as date))
				then 1 else 0 end)
		as "acquisitions_current_week"

FROM 

bi.orders_w_marketing o

where o.test__c = '0'
and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
--and o.acquisition_channel__c in ('web')
and o.acquisition_new_customer__c = true
--and ((extract(week from o.order_creation__c) = extract(week from current_date)) or (extract(week from o.order_creation__c) = extract(week from current_date)-1))

group by o.marketing_channel
order by o.marketing_channel asc