SELECT
	sum(case when o.acquisition_channel__c in ('web') then 1 else 0 end) as value
FROM
	bi.orders_w_marketing o
WHERE
  o.city = 'DE-Berlin'
	AND o.effectivedate::date >= '2016-01-04' and o.effectivedate::date < '2016-01-11'
	AND o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
	AND o.test__c = '0'