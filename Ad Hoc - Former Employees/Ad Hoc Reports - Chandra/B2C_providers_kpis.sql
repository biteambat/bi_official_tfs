-- Author: Chandrasen
-- Date of creation: 07/03/2018
-- Description: this query returns different kpis of providers(cleaners) as requested by Claude (to fill the excel file) which was asked by Investors


SELECT year_month,
		country,
		count(distinct professional__c) as distinct_professional_count,
		round(sum(monthly_contractual_hours)::numeric, 2) as total_contractual_hours,
		sum(worked_hours) as total_actual_hours,
		sum(sickness) as sickness,
		round((sum(sickness)/count(distinct professional__c))::numeric, 2) as sick_days_per_provider,
		count(rating) as rating,
		round(count(rating)/ count(distinct professional__c)::numeric, 2) as rating_count_per_provider,
		round((sum(rating)/ count(distinct professional__c))::numeric, 2) as avg_rating_per_provider
FROM
(
	SELECT year_month,
			left(delivery_area, 2) as country,
			professional__c,
			weekly_hours * 4.3 as monthly_contractual_hours,
			worked_hours,
			sickness,
			rating
	FROM bi.margin_per_cleaner
	WHERE left(delivery_area, 2) IN ('de', 'ch', 'nl')
) as t
GROUP BY year_month, country
ORDER BY country, year_month
