-- Author: Chandrasen
-- Name: 
-- Created on: 27-03-2018
-- Description: This file contains all the views which are used to create B2B Sales Head Dashboard


-- Table1
-- total net executed revenue today, -1 week, -2 week .....
-- Logic of this query is from bi.daily_reporting which is in the sql file sfunc_mktg_dailyreport

-- DROP VIEW IF EXISTS bi.b2b_daily_revenue 
-- CREATE OR REPLACE IF NOT EXISTS VIEW bi.b2b_daily_revenue AS;

SELECT
	1::numeric as id,
	CAST('Today' as text) as labels,
	(current_date)::date as date,
	SUM(CASE WHEN 
		(o.effectivedate = (current_date)::date 
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) 
	THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')

UNION
-- total net executed revenue 7 days back
SELECT
	2::numeric as id,
	CAST('-1 Week' as text) as labels,
	(current_date - 7)::date as date,
	SUM(CASE WHEN 
		(o.effectivedate = (current_date - 7)::date 
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) 
	THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')

UNION
-- total net executed revenue 14 days back
SELECT
	3::numeric as id,
	CAST('-2 Week' as text) as labels,
	(current_date - 14)::date as date,
	SUM(CASE WHEN 
		(o.effectivedate = (current_date - 14)::date 
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) 
	THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')

UNION
-- total net executed revenue 21 days back
SELECT
	4::numeric as id,
	CAST('-3 Week' as text) as labels,
	(current_date - 21)::date as date,
	SUM(CASE WHEN 
		(o.effectivedate = (current_date - 21)::date 
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) 
	THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	
UNION
-- total net executed revenue 28 days back
SELECT
	5::numeric as id,
	CAST('-4 Week' as text) as labels,
	(current_date - 28)::date as date,
	SUM(CASE WHEN 
		(o.effectivedate = (current_date - 28)::date 
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) 
	THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	
	
-- Table 2
-- total net executed revenue this week, -1 week, -2 week .....
-- Logic of this query is from bi.daily_reporting which is in the sql file sfunc_mktg_dailyreport

-- DROP VIEW IF EXISTS bi.b2b_weekly_revenue 
-- CREATE OR REPLACE IF NOT EXISTS VIEW bi.b2b_weekly_revenue AS;

-- total net executed revenue current week
SELECT
	1::numeric as id,
	CAST('This Week' as text) as labels,
	-- Concatinating year and week 
	date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) as year_week,
	SUM(CASE WHEN 
		-- concatinating year and week of effective date of the order and matching it with the desired concatination of year and week using current_date
		(date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date)
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	AND date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date)
	AND o.effectivedate <= current_date
GROUP BY date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate)

UNION
-- total net executed revenue last week
SELECT
	2::numeric as id,
	CAST('-1 Week' as text) as labels,
	-- Concatinating year and week
	date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) as year_week,
	SUM(CASE WHEN 
		-- concatinating year and week of effective date of the order and matching it with the desired concatination of year and week using current_date
		(date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 1
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	AND date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 1
GROUP BY date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate)

UNION
-- total net executed revenue 2 weeks back
SELECT
	3::numeric as id,
	CAST('-2 Week' as text) as labels,
	-- Concatinating year and week
	date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) as year_week,
	SUM(CASE WHEN 
		-- concatinating year and week of effective date of the order and matching it with the desired concatination of year and week using current_date
		(date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 2
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	AND date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 2
GROUP BY date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate)

UNION
-- total net executed revenue 3 weeks back
SELECT
	4::numeric as id,
	CAST('-3 Week' as text) as labels,
	-- Concatinating year and week
	date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) as year_week,
	SUM(CASE WHEN 
		-- concatinating year and week of effective date of the order and matching it with the desired concatination of year and week using current_date
		(date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 3
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	AND date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 3
GROUP BY date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate)

UNION
-- total net executed revenue 4 weeks back
SELECT
	5::numeric as id,
	CAST('-4 Week' as text) as labels,
	-- Concatinating year and week
	date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) as year_week,
	SUM(CASE WHEN 
		-- concatinating year and week of effective date of the order and matching it with the desired concatination of year and week using current_date
		(date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 4
		AND o.status in ('INVOICED','PENDING TO START','FULFILLED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
		) THEN o.gmv_eur_net ELSE 0 END)::numeric
	as net_executed_revenue
FROM bi.orders o
WHERE 
	o.test__c = '0'
	AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	AND LEFT(o.locale__c,3) NOT LIKE 'at-%'
	AND o."type" IN ('cleaning-b2c', 'cleaning-b2b')
	AND date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate) = date_part('year', current_date) || '-'::text || date_part('week', current_date) - 4
GROUP BY date_part('year', o.effectivedate) || '-'::text || date_part('week', o.effectivedate)

ORDER BY id
