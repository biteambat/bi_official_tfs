-- Author: Chandrasen, Christina 
-- Date of Creation: 22-03-2018
-- Short Description: This is a query to get all the CH churned customers with their finance outstanding balance

SELECT 	c.sfid as customer_id,
			t1.date as termination_date,
			t1.min_orderdate,
			t1.min_order_id,
			c.name,
			c.billing_street__c,
			c.billing_zip_code__c,
			c.billing_city__c,
			left(c.locale__c, 2) as locale,
			c.type__c,
			c.payment_method__c,
			c.finance_outstanding_balance__c,
			future_orders.future_orders
FROM salesforce.contact c
-- This join is to get the info of only the terminated customers who have a finance outstanding
INNER JOIN
	LATERAL	
			(
				SELECT	date,
							event_name,
							customer_id,
							min_orderdate,
							min_order_id
				FROM
				(
					SELECT	date,
								event_name,
								customer_id,
								-- To get the minimum orderdate using window function
								min(orderdate) OVER (PARTITION BY customer_id, date) as min_orderdate,
								-- To get the minimum order id using window function
								FIRST_VALUE(order_id) OVER (PARTITION BY customer_id, date ORDER BY orderdate) as min_order_id
					FROM
					(
					SELECT
					      created_at::date as date,
					      event_name,
					      (order_Json->>'Order_Start__c') as orderdate,
					      order_Json->>'Locale__c' as Locale__c,
					      order_Json->>'Order_Id__c' as Order_id,
					      order_Json->>'Contact__c' as customer_id,
					      order_Json->>'Recurrency__c' as recurrency,
					      order_Json->>'Type' as ordertype
					
					FROM events.sodium
					WHERE (event_name in ('Order Event:CANCELLED TERMINATED'))
					   and created_at >= '2016-01-01'
					   and order_Json->>'Recurrency__c' > '0'
					) sodium
				) _inn			
				GROUP BY date,
							event_name,
							customer_id,
							min_orderdate,
							min_order_id
			) t1
ON c.sfid = t1.customer_id
-- This join is to get the number of future orders that the churned customers has
LEFT JOIN
		LATERAL
				(
				SELECT	contact__c,
							COUNT(1) as future_orders
				FROM salesforce.order
				WHERE status not like '%CANCELLED%'
					AND effectivedate::date > current_date
				GROUP BY	contact__c
			) future_orders
ON future_orders.contact__c = c.sfid
WHERE c.payment_method__c = 'wiretransfer'
	AND left(c.locale__c, 2) = 'ch'
	AND c.type__c NOT IN ('customer-b2b')
	AND t1.date >= '2016-01-01'
ORDER BY customer_id,date, min_orderdate;

