-- Author: Chandrasen
-- Date: 08/03/2018
-- Description: This query creates the view which returns the cleaners who are booked in the current week

DROP VIEW IF EXISTS bi.cleaners_booked;
CREATE VIEW bi.cleaners_booked AS 
 
 SELECT ordermuffin_temp1.city,
    ordermuffin_temp1.order_start_date,
    ordermuffin_temp1.locale,
	 ordermuffin_temp1.type__c,
    ordermuffin_temp1.hour,
    date_part('dow'::text, ordermuffin_temp1.order_start_date) AS dow,
    sum(
        CASE
            WHEN (((date_part('dow'::text, ordermuffin_temp1.order_start_date) = (ordermuffin_temp1.day)::double precision) AND ((ordermuffin_temp1.hour)::double precision >= date_part('hour'::text, ordermuffin_temp1.order_start_time))) AND ((ordermuffin_temp1.hour)::double precision < date_part('hour'::text, ordermuffin_temp1.order_end_time))) THEN 1
            ELSE 0
        END) AS jobs_booked
   FROM ( SELECT 
				t.professional__c,
				t.ordernumber,
				"left"((t.locale__c)::text, 2) AS locale,
				t.delivery_area__c AS city,
				a.type__c,
				t.order_start__c,
				date(t.order_start__c) AS order_start_date,
				(t.order_start__c)::time without time zone AS order_start_time,
				date_part('hour'::text, t.order_start__c) AS order_start_hour,
				date_part('dow'::text, date(t.order_start__c)) AS order_dow,
				t.order_end__c,
				date(t.order_end__c) AS order_end_date,
				(t.order_end__c)::time without time zone AS order_end_time,
				date_part('hour'::text, t.order_end__c) AS order_end_hour,
				unnest(ARRAY[0, 1, 2, 3, 4, 5, 6]) AS day,
				unnest(ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) AS hour
			FROM salesforce."order" t 
				LEFT JOIN salesforce.account a 
				ON t.professional__c = a.sfid 
			WHERE (((date_part('year'::text, t.order_start__c) || '-'::text) || date_part('week'::text, t.order_start__c)) = ((date_part('year'::text, now()) || '-'::text) || date_part('week'::text, now())))
				AND a.test__c != true
				AND ((a.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
				AND (a.company_name__c LIKE 'BAT%' or a.company_name__c LIKE 'Book%' or a.company_name__c IS NULL)
				ORDER BY t.order_start__c DESC, date_part('dow'::text, date(t.order_start__c))) ordermuffin_temp1
  GROUP BY ordermuffin_temp1.city, ordermuffin_temp1.locale, ordermuffin_temp1.type__c, ordermuffin_temp1.order_start_date, ordermuffin_temp1.hour;