-- Author: Chandrasen
-- Date of creation: 07/03/2018
-- Description: this query returns the gmv of cleaning-b2c split by month, locale and polygon as requested by Claude (to fill the excel file) 
-- which was asked by Investors


SELECT 	date(date_trunc('month', effectivedate)),
	left(polygon, 2) as locale,
	polygon,
	round(sum(gmv__c)::numeric, 2) as gmv
FROM bi.orders
WHERE effectivedate >= '2016-01-01'
	AND (polygon LIKE 'de%' OR polygon LIKE 'ch%')
	AND status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'FULFILLED')
	AND type NOT IN ('2', 'cleaning-b2b', 'training')
GROUP BY date(date_trunc('month', effectivedate)), polygon, left(polygon, 2)
ORDER BY date, left(polygon, 2), polygon