-- Author: Chandrasen
-- Date: 12/03/2018
-- Description: This query creates the view which returns the cleaners actual availability in the current week

DROP VIEW IF EXISTS bi.cleaners_actual_availability;
CREATE VIEW bi.cleaners_actual_availability AS 
 
 SELECT 	jobs_available.*,
 			jobs_booked.jobs_booked,
 			jobs_available.jobs_available - jobs_booked.jobs_booked AS actual_availability,
 			jobs_booked.city as city_booked,	
 			jobs_booked.hour as hour_booked,
 			jobs_booked.locale as locale_booked,
 			jobs_booked.type__c as type__c_booked,
 			jobs_booked.order_start_date
 FROM
 (
 SELECT availabilitymuffin_temp1.city,
    availabilitymuffin_temp1.date,
    availabilitymuffin_temp1.locale,
    availabilitymuffin_temp1.type__c,
    availabilitymuffin_temp1.hour,
    date_part('dow'::text, availabilitymuffin_temp1.date) AS dow_availability,
    sum(
        CASE
            WHEN (((date_part('dow'::text, availabilitymuffin_temp1.date) = (availabilitymuffin_temp1.day)::double precision) AND ((availabilitymuffin_temp1.hour)::double precision >= date_part('hour'::text, availabilitymuffin_temp1.start_availability))) AND ((availabilitymuffin_temp1.hour)::double precision < date_part('hour'::text, availabilitymuffin_temp1.end_availability))) THEN 1
            ELSE 0
        END) AS jobs_available
   FROM ( SELECT availability.sfid,
            availability.city,
            availability.status__c,
            availability.locale,
            availability.type__c,
            availability.date,
            availability.start_availability,
            availability.end_availability,
            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6]) AS day,
            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) AS hour
           FROM ( SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-02'::date AS date,
                    (split_part((account.availability_monday__c)::text, '-'::text, 1))::time without time zone AS start_availability,
                    (split_part((account.availability_monday__c)::text, '-'::text, 2))::time without time zone AS end_availability
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-03'::date AS day,
                    (split_part((account.availability_tuesday__c)::text, '-'::text, 1))::time without time zone AS start_availability,
                    (split_part((account.availability_tuesday__c)::text, '-'::text, 2))::time without time zone AS end_availability
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-04'::date AS day,
                    (split_part((account.availability_wednesday__c)::text, '-'::text, 1))::time without time zone AS start_availability_wed,
                    (split_part((account.availability_wednesday__c)::text, '-'::text, 2))::time without time zone AS end_availability_wed
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-05'::date AS day,
                    (split_part((account.availability_thursday__c)::text, '-'::text, 1))::time without time zone AS start_availability_thu,
                    (split_part((account.availability_thursday__c)::text, '-'::text, 2))::time without time zone AS end_availability_thu
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-06'::date AS day,
                    (split_part((account.availability_friday__c)::text, '-'::text, 1))::time without time zone AS start_availability_fr,
                    (split_part((account.availability_friday__c)::text, '-'::text, 2))::time without time zone AS end_availability_fr
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-07'::date AS day,
                    (split_part((account.availability_saturday__c)::text, '-'::text, 1))::time without time zone AS start_availability_sa,
                    (split_part((account.availability_saturday__c)::text, '-'::text, 2))::time without time zone AS end_availability_sa
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))) availability
					) availabilitymuffin_temp1		
  GROUP BY availabilitymuffin_temp1.city, availabilitymuffin_temp1.locale, availabilitymuffin_temp1.type__c, availabilitymuffin_temp1.date, availabilitymuffin_temp1.hour
) jobs_available 
LEFT JOIN
	LATERAL	(
				 SELECT *
				 FROM
				 (
				 SELECT ordermuffin_temp1.city,
				    ordermuffin_temp1.order_start_date,
				    ordermuffin_temp1.locale,
				    ordermuffin_temp1.type__c,
				    ordermuffin_temp1.hour,
				    date_part('dow'::text, ordermuffin_temp1.order_start_date) AS dow,
				    sum(
				        CASE
				            WHEN (((date_part('dow'::text, ordermuffin_temp1.order_start_date) = (ordermuffin_temp1.day)::double precision) AND ((ordermuffin_temp1.hour)::double precision >= date_part('hour'::text, ordermuffin_temp1.order_start_time))) AND ((ordermuffin_temp1.hour)::double precision < date_part('hour'::text, ordermuffin_temp1.order_end_time))) THEN 1
				            ELSE 0
				        END) AS jobs_booked
				   FROM ( SELECT t.professional__c,
				            t.ordernumber,
				            "left"((t.locale__c)::text, 2) AS locale,
				            t.delivery_area__c AS city,
				            a.type__c,
				            t.order_start__c,
				            date(t.order_start__c) AS order_start_date,
				            (t.order_start__c)::time without time zone AS order_start_time,
				            date_part('hour'::text, t.order_start__c) AS order_start_hour,
				            date_part('dow'::text, date(t.order_start__c)) AS order_dow,
				            t.order_end__c,
				            date(t.order_end__c) AS order_end_date,
				            (t.order_end__c)::time without time zone AS order_end_time,
				            date_part('hour'::text, t.order_end__c) AS order_end_hour,
				            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6]) AS day,
				            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) AS hour
				           FROM (salesforce."order" t
				             LEFT JOIN salesforce.account a ON (((t.professional__c)::text = (a.sfid)::text)))
				          WHERE ((((((date_part('year'::text, t.order_start__c) || '-'::text) || date_part('week'::text, t.order_start__c)) = ((date_part('year'::text, now()) || '-'::text) || date_part('week'::text, now()))) 
							 AND (a.test__c <> true)) 
							 AND ((a.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
							 AND ((((a.company_name__c)::text ~~ 'BAT%'::text) OR ((a.company_name__c)::text ~~ 'Book%'::text)) OR (a.company_name__c IS NULL)))
				          ORDER BY t.order_start__c DESC, date_part('dow'::text, date(t.order_start__c))) ordermuffin_temp1
				  GROUP BY ordermuffin_temp1.city, ordermuffin_temp1.locale, ordermuffin_temp1.type__c, ordermuffin_temp1.order_start_date, ordermuffin_temp1.hour
				) _inn
				WHERE _inn.hour = jobs_available.hour	
					AND _inn.city = jobs_available.city	
					AND _inn.locale = jobs_available.locale	
					AND _inn.dow = jobs_available.dow_availability	
					AND _inn.type__c = jobs_available.type__c				
	) jobs_booked
ON TRUE
