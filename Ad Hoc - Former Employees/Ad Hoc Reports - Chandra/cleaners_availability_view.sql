 -- Author: Chandrasen
-- Date: 08/03/2018
-- Description: This query creates the view which returns the cleaners who are available in the current week
 
 
 DROP VIEW IF EXISTS bi.cleaners_availability;
 CREATE VIEW bi.cleaners_availability AS 
 
 SELECT availabilitymuffin_temp1.city,
    availabilitymuffin_temp1.date,
    availabilitymuffin_temp1.locale,
	 availabilitymuffin_temp1.type__c,
    availabilitymuffin_temp1.hour,
    date_part('dow'::text, availabilitymuffin_temp1.date) AS dow_availability,
    sum(
        CASE
            WHEN (((date_part('dow'::text, availabilitymuffin_temp1.date) = (availabilitymuffin_temp1.day)::double precision) AND ((availabilitymuffin_temp1.hour)::double precision >= date_part('hour'::text, availabilitymuffin_temp1.start_availability))) AND ((availabilitymuffin_temp1.hour)::double precision < date_part('hour'::text, availabilitymuffin_temp1.end_availability))) THEN 1
            ELSE 0
        END) AS jobs_available
   FROM ( SELECT availability.sfid,
            availability.city,
            availability.status__c,
            availability.locale,
            availability.type__c,
            availability.date,
            availability.start_availability,
            availability.end_availability,
            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6]) AS day,
            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) AS hour
           FROM ( SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-02'::date AS date,
                    (split_part((account.availability_monday__c)::text, '-'::text, 1))::time without time zone AS start_availability,
                    (split_part((account.availability_monday__c)::text, '-'::text, 2))::time without time zone AS end_availability
                   FROM salesforce.account
							WHERE account.test__c != true
								and ((account.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
								and (account.company_name__c LIKE 'BAT%' or account.company_name__c LIKE 'Book%' or account.company_name__c IS NULL)
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-03'::date AS day,
                    (split_part((account.availability_tuesday__c)::text, '-'::text, 1))::time without time zone AS start_availability,
                    (split_part((account.availability_tuesday__c)::text, '-'::text, 2))::time without time zone AS end_availability
                   FROM salesforce.account
							WHERE account.test__c != true
								and ((account.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
								and (account.company_name__c LIKE 'BAT%' or account.company_name__c LIKE 'Book%' or account.company_name__c IS NULL)
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-04'::date AS day,
                    (split_part((account.availability_wednesday__c)::text, '-'::text, 1))::time without time zone AS start_availability_wed,
                    (split_part((account.availability_wednesday__c)::text, '-'::text, 2))::time without time zone AS end_availability_wed
                   FROM salesforce.account
							WHERE account.test__c != true
								and ((account.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
								and (account.company_name__c LIKE 'BAT%' or account.company_name__c LIKE 'Book%' or account.company_name__c IS NULL)
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-05'::date AS day,
                    (split_part((account.availability_thursday__c)::text, '-'::text, 1))::time without time zone AS start_availability_thu,
                    (split_part((account.availability_thursday__c)::text, '-'::text, 2))::time without time zone AS end_availability_thu
                   FROM salesforce.account
							WHERE account.test__c != true
								and ((account.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
								and (account.company_name__c LIKE 'BAT%' or account.company_name__c LIKE 'Book%' or account.company_name__c IS NULL)
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-06'::date AS day,
                    (split_part((account.availability_friday__c)::text, '-'::text, 1))::time without time zone AS start_availability_fr,
                    (split_part((account.availability_friday__c)::text, '-'::text, 2))::time without time zone AS end_availability_fr
                   FROM salesforce.account
							WHERE account.test__c != true
								and ((account.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
								and (account.company_name__c LIKE 'BAT%' or account.company_name__c LIKE 'Book%' or account.company_name__c IS NULL)
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-07'::date AS day,
                    (split_part((account.availability_saturday__c)::text, '-'::text, 1))::time without time zone AS start_availability_sa,
                    (split_part((account.availability_saturday__c)::text, '-'::text, 2))::time without time zone AS end_availability_sa
                   FROM salesforce.account
							WHERE account.test__c != true
								and ((account.status__c)::text = ANY ((ARRAY['ACTIVE'::character varying, 'BETA'::character varying])::text[]))
								and (account.company_name__c LIKE 'BAT%' or account.company_name__c LIKE 'Book%' or account.company_name__c IS NULL))availability) availabilitymuffin_temp1
  GROUP BY availabilitymuffin_temp1.city, availabilitymuffin_temp1.locale, availabilitymuffin_temp1.type__c, availabilitymuffin_temp1.date, availabilitymuffin_temp1.hour;