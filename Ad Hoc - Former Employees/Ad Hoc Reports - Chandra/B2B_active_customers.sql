﻿-- Author: Chandrasen
-- Date of creation: 21/02/2018
-- Description: This query returns the active B2B Customers in our system, who have future B2B orders 
--		& haven't cancelled their plan. 

select 	opp.customer__c,
	opp.status__c,
	opp.stagename
from bi.orders o
-- Joining orders table to opportunity table on contact__c and customer__c
inner join
	lateral	(
		select 	customer__c,
			status__c,
			stagename
		from salesforce.opportunity _inn
		where _inn.customer__c = o.contact__c
	) opp
on true
where o.type = 'cleaning-b2b'
	and order_start__c >= current_timestamp
group by o.contact__c,
	opp.customer__c,
	opp.status__c,
	opp.stagename