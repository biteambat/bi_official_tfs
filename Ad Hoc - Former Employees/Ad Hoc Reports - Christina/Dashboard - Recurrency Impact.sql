SELECT
locale__c AS locale, 
polygon AS city,
type,
Contact__c AS CustomerID,
Customer_name__c AS Customer,
effectivedate AS orderdate,
recurrency__c AS recurrency,
status,
professional__c AS professional,
grand_total_eur__c As grandtotalEUR

FROM
	bi.orders
