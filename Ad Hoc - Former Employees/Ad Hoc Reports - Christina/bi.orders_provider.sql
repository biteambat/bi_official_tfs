DROP TABLE IF EXISTS bi.orders_provider;
CREATE TABLE bi.orders_provider AS 


SELECT DISTINCT ON (orders.opportunityid)
orders.sfid AS orderid
, orders.opportunityid
, opp.name as opportunity
, orders.effectivedate
, orders.professional__c
, prof.pro_name
, prof.provider
, prof.company_name

FROM 
salesforce.order orders

	LEFT JOIN 
		salesforce.opportunity opp
		ON ( orders.opportunityid = opp.sfid)
		
	LEFT JOIN 
		(
		SELECT 
		pro.sfid				pro_id
		, pro.name				pro_name
		, pro.company_name__c	pro_company_name
		, company.sfid			company_id
		, company.name			company_name
		, CASE 	WHEN company.sfid = '0012000001TDMgGAAX' THEN 'BAT'
				WHEN company.sfid IS NULL THEN 'unknown'
				ELSE 'Partner' END provider
		-- ,*
		FROM 
		salesforce.account pro

			LEFT JOIN 
				salesforce.account company
				ON (pro.parentid = company.sfid)
		) AS prof
		ON (prof.pro_id = orders.professional__c)

WHERE 
orders.status IN ('INVOICED','FULFILLED')

ORDER BY 
orders.opportunityid, 
orders.effectivedate DESC;




