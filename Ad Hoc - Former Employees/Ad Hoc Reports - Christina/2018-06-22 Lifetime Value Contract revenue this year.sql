-- SELECT
-- month,
-- duration2,
-- status,
-- SUM(Opps),
-- SUM(Revenue)
-- 
-- 
-- FROM
-- (

SELECT
month,
duration2,
SUm(Opps) As Opps,
SUM(SUM_Revenue),
AVG(AVG_Revenue)

FROM(

SELECT
to_char(Opp.closedate::date, 'YYYY-MM') AS month,
-- Contract.duration__c AS duration,
-- Contract.status__c AS status,
COUNT(*) AS Opps,
SUM(Contract.grand_total__c) AS SUM_Revenue,
AVG(Contract.grand_total__c) AS AVG_Revenue,
CASE WHEN  Contract.grand_total__c = '0' THEN 'irregular' ELSE 
(
CASE 
		-- duration: unlimited
		WHEN additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
		WHEN additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
		-- duration: 12 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
		-- duration: 6 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
		-- duration: 3 month
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
		-- duration: 1 month
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
		ELSE (CASE WHEN Contract.duration__c IS NULL THEN '1' ELSE Contract.duration__c END)END) END AS duration2
		, Contract.opportunity__c 


FROM
salesforce.contract__c AS Contract
INNER JOIN 
cv
		(SELECT
		*
		FROM 
		salesforce.opportunity 
		WHERE 
		test__c = false
 		AND closedate::date >= '2018-01-01'
		AND stagename IN ('WON', 'PENDING', 'WRITTEN CONFIRMATION')
		) AS Opp
		ON (Opp.sfid = Contract.Opportunity__c)

WHERE 
Contract.status__c  IN ('SIGNED','ACCEPTED')

GROUP BY
Opp.closedate,
Contract.duration__c,
Contract.status__c,
additional_agreements__c,
Contract.opportunity__c,
Contract.grand_total__c
) AS Revenue

GROUP BY
Revenue.month,
Revenue.duration2

ORDER BY 
month, duration2 ASC

-- 
-- 
-- GROUP BY
-- month,
-- duration2,
-- status
-- 