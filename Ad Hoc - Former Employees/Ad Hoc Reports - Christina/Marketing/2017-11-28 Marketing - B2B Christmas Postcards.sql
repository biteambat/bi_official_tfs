SELECT
t1.contact__c AS sfid,
t3.name AS CompanyName,
t1.customer_name__c AS customer_full_name,
t1.locale__c as locale,
t3.billingaddress__c,
t3.billingaddress_postalcode__c,
t3.billingaddress_city__c,
t3.billingaddress_country__c,
COUNT(DISTINCT t1.id),
t3.grand_total__c

FROM
bi.orders t1
			LEFT JOIN
				(SELECT
				*
				FROM
				bi.orders 
				WHERE
				type LIKE 'cleaning-b2b'
						AND (locale__c LIKE 'de-de'
							OR locale__c LIKE 'de-en'
							OR locale__c LIKE 'nl-nl'
							OR locale__c LIKE 'nl-en')
						AND date_part('year',effectivedate) = date_part('year', CURRENT_DATE) 
						AND status = 'INVOICED') AS  t2
				
				ON (t1.contact__c = t2.contact__c)
				
				LEFT JOIN
				salesforce.opportunity t3
				ON ( t1.opportunityid = t3.sfid)

WHERE
(t1.type LIKE 'cleaning-b2b'
		AND (t1.locale__c LIKE 'de-de'
			OR t1.locale__c LIKE 'de-en'
			OR t1.locale__c LIKE 'nl-nl'
			OR t1.locale__c LIKE 'nl-en')
		AND t1.effectivedate::date > CURRENT_DATE 
		AND t1.status LIKE 'PENDING TO START')


GROUP BY
t1.contact__c,
t3.name,
t1.customer_name__c,
t1.locale__c,
t3.billingaddress__c,
t3.billingaddress_postalcode__c,
t3.billingaddress_city__c,
t3.billingaddress_country__c,
t3.grand_total__c