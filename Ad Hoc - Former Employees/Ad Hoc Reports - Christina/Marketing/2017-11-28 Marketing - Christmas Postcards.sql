SELECT
t1.contact__c AS sfid,
t1.customer_name__c AS customer_full_name,
t1.shippingstreet AS Shipping_Address,
t1.shippingpostalcode AS Shipping_Zip_Code,
t1.shippingcity AS Shipping_City,
t1.shippingcountry AS Shipping_Country,
t1.locale__c as locale,
COUNT(DISTINCT t1.id)

FROM
bi.orders t1
			LEFT JOIN
				(SELECT
				*
				FROM
				bi.orders 
				WHERE
				type LIKE 'cleaning-b2c'
						AND (locale__c LIKE 'de-de'
							OR locale__c LIKE 'de-en'
							OR locale__c LIKE 'ch-de'
							OR locale__c LIKE 'ch-en')
						AND date_part('year',effectivedate) = date_part('year', CURRENT_DATE) 
						AND status = 'INVOICED') AS  t2
				
				ON (t1.contact__c = t2.contact__c)

WHERE
(t1.type LIKE 'cleaning-b2c'
		AND (t1.locale__c LIKE 'de-de'
			OR t1.locale__c LIKE 'de-en'
			OR t1.locale__c LIKE 'ch-de'
			OR t1.locale__c LIKE 'ch-en')
		AND t1.effectivedate::date > CURRENT_DATE 
		AND t1.status LIKE 'PENDING TO START')


GROUP BY
t1.contact__c,
t1.customer_name__c,
t1.shippingstreet,
t1.shippingpostalcode,
t1.shippingcity,
t1.shippingcountry,
t1.locale__c