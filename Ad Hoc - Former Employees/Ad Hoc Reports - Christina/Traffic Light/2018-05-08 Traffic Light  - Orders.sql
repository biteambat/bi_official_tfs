SELECT
Opp
, delivery_area
, SUM(Orders_NSP)			AS Orders_NSP
, SUM(NSP)					AS NSP
, SUM(Orders_CP)			AS Orders_CP
, SUM(CP)					AS CP
, SUM(Orders_CC)			AS Orders_CC
, SUM(CC)					AS CC
, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
, SUM(NSP_CP)				AS NSP_CP

FROM (
SELECT 

opportunityid AS Opp
-- , delivery_area__c 																		AS delivery_area_mass
, CASE 	WHEN (FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c)) IS NULL 
			THEN 'unknown'
			ELSE FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c) END		AS delivery_area

, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP

, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
				WHEN status like '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP

, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
				
, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
				WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
				
, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
				WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
							
FROM
salesforce."order" AS orders

WHERE
effectivedate::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' 
							AND to_date('2018-05-03', 'YYYY-MM-DD')

AND opportunityid IS NOT NULL

GROUP BY
Opp
, delivery_area__c 

) AS orders_final

GROUP BY
Opp
, delivery_area
