SELECT

Inbound_Cases_final.locale
, Opportunity
, Inbound_Cases_final.opportunity_name
, Inbound_Cases_final.status
, MAX(Inbound_Cases_final.opps)						AS opps
, SUM(createdcases)										AS createdcases

FROM
(

SELECT
Opps.*
, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * createdcases / Opps END)
					ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
									THEN 0.00
									ELSE 1.0 * createdcases END) END)::numeric, 2)							AS createdcases

FROM
(
SELECT
locale__c				AS locale
, customer__c			AS Customer
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps

FROM
		Salesforce.opportunity Opp
		
WHERE
test__c = false
AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')

GROUP BY
locale__c
, customer__c
, name		
, sfid	
, status__c ) AS Opps


LEFT JOIN 
LATERAL


(SELECT 
Contactid 																								AS Contact
, opportunity__c 																						AS Opportunity
, COUNT(1) as CreatedCases

FROM
Salesforce.case		
WHERE
CreatedDate::date between to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' and to_date('2018-05-03', 'YYYY-MM-DD')
AND 
(type LIKE '%KA%'
OR
((Origin LIKE '%B2B - Contact%'
OR Origin LIKE '%B2B AT - CLOSET%'
OR Origin LIKE '%B2B CH - CLOSET%'
OR Origin LIKE '%B2B DE - CLOSET%'
OR Origin LIKE '%B2B NL - CLOSET%')
AND type != 'KA'))
AND Isdeleted = false
AND CreatedById = '00520000003IiNCAA0'

GROUP BY 
Contact
, Opportunity

) AS Inbound_Cases

ON (CASE WHEN Inbound_Cases.Opportunity IS NULL 
		THEN Opps.Customer = Inbound_Cases.Contact 
		ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)
		
) AS Inbound_Cases_final

GROUP BY
Opportunity
, Inbound_Cases_final.locale
, Inbound_Cases_final.customer
, Inbound_Cases_final.opportunity_name
, Inbound_Cases_final.status