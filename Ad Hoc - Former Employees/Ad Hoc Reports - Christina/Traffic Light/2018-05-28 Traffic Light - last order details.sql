(SELECT DISTINCT ON (opportunityid)
opportunityid 
, effectivedate::date 
, sfid AS orderid
, delivery_area__c
, professional__c  AS Professional
, operated_by
, operated_by_detail
		 -- add any other column (expression) from the same row
FROM   salesforce."order" AS orders

LEFT JOIN
	(SELECT
		Professional.sfid AS professional,
		CASE WHEN Partner.name NOT LIKE '%B2BS%' THEN 'BAT' ELSE 'Partner' END AS operated_by,
		CASE WHEN Partner.name NOT LIKE '%B2BS%' THEN 'BAT' ELSE Partner.name END AS operated_by_detail

	FROM
		Salesforce.Account Professional

		LEFT JOIN Salesforce.Account Partner
			ON (Professional.parentid = Partner.sfid)
	)	AS ProfessionalDetails

	ON (orders.professional__c = ProfessionalDetails.professional	)

WHERE
opportunityid IS NOT NULL

ORDER  BY 
opportunityid, 
effectivedate::datetime DESC

) AS last_order