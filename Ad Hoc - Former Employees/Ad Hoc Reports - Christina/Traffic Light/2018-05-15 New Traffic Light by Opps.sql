SELECT

intern_final.locale
, CASE 	WHEN orders_final.delivery_area IS NULL 
			THEN 'unknown'
			ELSE orders_final.delivery_area 
			END										AS delivery_area
, intern_final.Opportunity
, intern_final.opportunity_name
, intern_final.status
, MAX(intern_final.opps)						AS opps

-- 
-- orders 
--

, SUM(Orders_NSP)									AS Orders_NSP
, SUM(NSP)											AS NSP
, SUM(Orders_CP)									AS Orders_CP
, SUM(CP)											AS CP
, SUM(Orders_CC)									AS Orders_CC
, SUM(CC)											AS CC
, SUM(Orders_NSP_CP)								AS Orders_NSP_CP
, SUM(NSP_CP)										AS NSP_CP

--
-- inbound cases
--

, SUM(createdcases) 								AS inbound_cases

-- 
-- internal cases 
--

, SUM(retention_all)								AS retention_all
, SUM(Retention_closed)							AS Retention_closed
, SUM(InvoiceCorrection_All)					AS InvoiceCorrection_All
, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
-- , orders_final.*

FROM
(
SELECT
Opps.*
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * retention_all / Opps END)
					ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
									THEN 0.00
									ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * Retention_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
									THEN 0.00
									ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * InvoiceCorrection_All / Opps END)
					ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
									THEN 0.00
									ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * InvoiceCorrection_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
									THEN 0.00
									ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed

, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * ProfessionalImprovement_All / Opps END)
					ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
									THEN 0.00
									ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
									
, ROUND((CASE 	WHEN Intern.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
					ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
									THEN 0.00
									ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed

FROM
(
SELECT
locale__c				AS locale
, customer__c			AS Customer
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps

FROM
		Salesforce.opportunity Opp
		
WHERE
test__c = false
AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')

GROUP BY
locale__c
, customer__c
, name		
, sfid	
, status__c) AS Opps


LEFT JOIN 
LATERAL


-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. Current_Date
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO

(
SELECT 
Contactid 																								AS Contact
, opportunity__c 																						AS Opportunity
-- ,sfid
, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
--
, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
--
, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
--
, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
--
, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
OR subject LIKE '%nvoice %orrection%'
AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
--
, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
				AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed

FROM
Salesforce.case	interncases	

WHERE
	-- Cases created within the last 8 weeks incl. Current_Date
	CreatedDate::date between to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' 
							AND to_date('2018-05-03', 'YYYY-MM-DD')	
	
	AND Isdeleted = false
	
	-- internal cases: 
	-- 	Customer - Retention 		> communication with sales
	-- 	Order - Invoice editing 	> communication with finance 
	-- 	Professional - Improvement > communication with TO
	
	AND ((reason LIKE '%Customer - Retention%' 
				OR reason LIKE '%Order - Invoice editing%' 
				AND type LIKE '%KA%') 
				
			OR (reason LIKE '%Professional - Improvement%' 
				AND type LIKE '%TO%'))


GROUP BY
Contact
, Opportunity

) AS Intern

ON (CASE WHEN Intern.Opportunity IS NULL 
		THEN Opps.Customer = Intern.Contact 
		ELSE Intern.Opportunity = Opps.Opportunity END)
		
)		AS Intern_final


LEFT JOIN 
LATERAL

-- --
-- -- inbound cases
-- -- Cases created within the last 8 weeks incl. Current_Date
-- -- 	type = KA 
-- --	OR origin 
-- -- AND created by API

(
SELECT
Opportunity
, SUM(createdcases)										AS createdcases

FROM
(

SELECT
Opps.*
, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
					THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
									THEN 0.00 
									ELSE 1.0 * createdcases / Opps END)
					ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
									THEN 0.00
									ELSE 1.0 * createdcases END) END)::numeric, 2)							AS createdcases

FROM
(
SELECT
locale__c				AS locale
, customer__c			AS Customer
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps

FROM
		Salesforce.opportunity Opp
		
WHERE
test__c = false
AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')

GROUP BY
locale__c
, customer__c
, name		
, sfid	
, status__c ) AS Opps


LEFT JOIN 
LATERAL


(SELECT 
Contactid 																								AS Contact
, opportunity__c 																						AS Opportunity
, COUNT(1) as CreatedCases

FROM
Salesforce.case		
WHERE
CreatedDate::date between to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' and to_date('2018-05-03', 'YYYY-MM-DD')
AND 
(type LIKE '%KA%'
OR
((Origin LIKE '%B2B - Contact%'
OR Origin LIKE '%B2B AT - CLOSET%'
OR Origin LIKE '%B2B CH - CLOSET%'
OR Origin LIKE '%B2B DE - CLOSET%'
OR Origin LIKE '%B2B NL - CLOSET%')
AND type != 'KA'))
AND Isdeleted = false
AND CreatedById = '00520000003IiNCAA0'

GROUP BY 
Contact
, Opportunity

) AS Inbound_Cases

ON (CASE WHEN Inbound_Cases.Opportunity IS NULL 
		THEN Opps.Customer = Inbound_Cases.Contact 
		ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)
		
) AS Inbound_Cases_f

GROUP BY
Opportunity

) AS Inbound_Cases_final

ON (Inbound_Cases_final.Opportunity = Intern_final.opportunity)




LEFT JOIN
LATERAL

-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) within the last 8 weeks incl. Current_Date


(
SELECT
Opp
, delivery_area
, SUM(Orders_NSP)			AS Orders_NSP
, SUM(NSP)					AS NSP
, SUM(Orders_CP)			AS Orders_CP
, SUM(CP)					AS CP
, SUM(Orders_CC)			AS Orders_CC
, SUM(CC)					AS CC
, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
, SUM(NSP_CP)				AS NSP_CP

FROM (
SELECT 

opportunityid AS Opp
-- , delivery_area__c 																		AS delivery_area_mass
, CASE 	WHEN (FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c)) IS NULL 
			THEN 'unknown'
			ELSE FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c) END		AS delivery_area

, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP

, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'	THEN 1 
				WHEN status like '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CP

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP

, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
				
, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC

, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
				WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
				WHEN status LIKE '%INVOICED%' 					THEN 1 
				WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
				
, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
				WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
							
FROM
salesforce."order" AS orders

WHERE
effectivedate::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' 
							AND to_date('2018-05-03', 'YYYY-MM-DD')

AND opportunityid IS NOT NULL

GROUP BY
Opp
, delivery_area__c 

) AS orders2

GROUP BY
Opp
, delivery_area

) orders_final

ON (orders_final.opp = Intern_final.opportunity)






GROUP BY
intern_final.Opportunity
, intern_final.locale
, intern_final.customer
, intern_final.opportunity_name
, intern_final.status

, orders_final.delivery_area


