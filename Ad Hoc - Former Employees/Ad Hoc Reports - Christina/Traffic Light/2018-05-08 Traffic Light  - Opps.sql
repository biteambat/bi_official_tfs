SELECT
locale__c
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
FROM
		Salesforce.opportunity
WHERE
test__c = false
AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')



LEFT JOIN

-- Calls

(SELECT

CASE 
WHEN calledcountryshort__c = 'AUT' THEN 'at'
WHEN calledcountryshort__c = 'CHE' THEN 'ch'
WHEN calledcountryshort__c = 'NLD' THEN 'nl'
ELSE 'de' END as locale,
SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END)+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' THEN 1 ELSE 0 END) as all_inbound_calls,
SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END) as lost_inbound_calls,
SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' THEN 1 ELSE 0 END) AS all_outbound,
SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' THEN 1 ELSE 0 END) AS b2b_outbound,
RelatedContact__c as Contact

FROM 
Salesforce.natterbox_call_reporting_object__c t4

	LEFT JOIN 
	salesforce.contact t5
	
	ON
	(t4.relatedcontact__c = t5.sfid)
WHERE
-- -- Calls created within the last 8 weeks incl. Current_Date
call_start_date_date__c::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' 
											AND to_date('2018-05-03', 'YYYY-MM-DD')

AND 	(	(E164Callednumber__C = '493030807263'
				OR E164Callednumber__C='493030807264')
			AND calldirection__c = 'Inbound')
		OR (Callerfirstname__c LIKE 'CM%'
			AND calldirection__c = 'Outbound')

GROUP BY
locale,
Contact) as sub4

ON
(sub3.locale = sub4.locale and sub3.Contact = sub4.Contact)





-- LEFT JOIN
-- 
-- (SELECT
-- 
-- Contactid 																				AS Contact
-- -- , OpportunityID 																	AS Opportunity
-- 
-- , COUNT(1) 																				AS CreatedCases
-- , SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 					AS Cases_without_Opp
-- 
-- , CASE 	WHEN 	(Origin LIKE '%B2B - Contact%'
-- OR Origin LIKE '%B2B DE - CLOSET%'
-- OR Origin LIKE '%checkout%'
-- OR Origin LIKE '%CM DE%'
-- OR Origin LIKE '%TM DE - General Email%') THEN 'de'
-- WHEN 	(Origin LIKE '%B2B CH - CLOSET%'
-- OR Origin LIKE '%CM CH%'
-- OR Origin LIKE '%TM CH - General Email%') THEN 'ch'
-- WHEN 	(Origin LIKE '%B2B AT - CLOSET%'
-- OR Origin LIKE '%CM AT%'
-- OR Origin LIKE '%TM AT - General Email%') THEN 'at'
-- WHEN 	(Origin LIKE '%B2B NL - CLOSET%'
-- OR Origin LIKE '%CM NL%'
-- OR Origin LIKE '%TM NL - General Email%') THEN 'nl'
-- ELSE 'Other' END 															AS locale
-- 
-- 
-- FROM
-- Salesforce.case
-- 
-- WHERE
-- -- Cases created within the last 8 weeks incl. Current_Date
-- CreatedDate::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks'
-- AND to_date('2018-05-03', 'YYYY-MM-DD')
-- 
-- -- cases with type = KA
-- -- OR cases without type = KA, but the case origin is listed below
-- 
-- AND (type LIKE '%KA%'
-- OR ((Origin LIKE '%B2B - Contact%'
-- OR Origin LIKE '%B2B AT - CLOSET%'
-- OR Origin LIKE '%B2B CH - CLOSET%'
-- OR Origin LIKE '%B2B DE - CLOSET%'
-- OR Origin LIKE '%B2B NL - CLOSET%')
-- AND type != 'KA'))
-- 
-- AND Isdeleted = false
-- 
-- -- Cases created by API Tiger
-- AND CreatedById = '00520000003IiNCAA0'
-- 
-- GROUP BY
-- locale,
-- Contact,
-- -- Opportunity
-- 
-- ) AS sub5
-- 
-- ON
-- (sub3.locale = sub5.locale and sub3.Opportunity = sub5.Opportunity)
-- 
-- LEFT JOIN
-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. Current_Date
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- 
-- (SELECT
-- Contactid AS Contact
-- , OpportunityID AS Opportunity
-- , CASE 	WHEN (Origin 	LIKE '%B2B - Contact%'
-- OR Origin LIKE '%B2B DE - CLOSET%'
-- OR Origin LIKE '%checkout%'
-- OR Origin LIKE '%CM DE%'
-- OR Origin LIKE '%TM DE - General Email%'
-- OR Origin LIKE '%TO DE - Contact%'
-- OR Origin LIKE '%TO DE - SMS%'
-- OR Origin LIKE '%B2B - Sales')  		THEN 'de'
-- 
-- WHEN (Origin 	LIKE '%B2B CH - CLOSET%'
-- OR Origin LIKE '%CM CH%'
-- OR Origin LIKE '%TM CH - General Email%'
-- OR Origin LIKE '%TO CH - Contact%') THEN 'ch'
-- 
-- WHEN (Origin 	LIKE '%B2B AT - CLOSET%'
-- OR Origin LIKE '%CM AT%'
-- OR Origin LIKE '%TM AT - General Email%'
-- OR Origin LIKE '%TO AT - Contact%') THEN 'at'
-- 
-- WHEN (Origin 	LIKE '%B2B NL - CLOSET%'
-- OR Origin LIKE '%CM NL%'
-- OR Origin LIKE '%TM NL - General Email%'
-- OR Origin LIKE '%TO NL - Contact%') THEN 'nl'
-- 
-- ELSE 'Other' END 															AS locale
-- 
-- , SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
-- , SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
-- AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS Retention_closed
-- 
-- , SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
-- AND subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 				AS InvoiceCorrection_All
-- , SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
-- AND subject LIKE '%nvoice %orrection%'
-- AND isclosed = 'true' THEN 1 ELSE 0 END)										AS InvoiceCorrection_closed
-- 
-- , SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
-- , SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
-- AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
-- 
-- FROM
-- Salesforce.case
-- 
-- WHERE
-- -- Cases created within the last 8 weeks incl. Current_Date
-- CreatedDate::date between to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks'
-- AND to_date('2018-05-03', 'YYYY-MM-DD')
-- 
-- AND Isdeleted = false
-- 
-- -- internal cases:
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO
-- 
-- AND ((reason LIKE '%Customer - Retention%'
-- OR reason LIKE '%Order - Invoice editing%'
-- AND type LIKE '%KA%')
-- 
-- OR (reason LIKE '%Professional - Improvement%'
-- AND type LIKE '%TO%'))
-- 
-- GROUP BY
-- locale,
-- Contact,
-- Opportunity
-- 
-- ) AS sub6
-- 
-- ON (sub3.locale =sub6.locale AND sub3.Opportunity = sub6.Opportunity)