SELECT
Opps.*
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)										AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)										AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
					

FROM
(
SELECT
locale__c				AS locale
, customer__c			AS Customer
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps

FROM
		Salesforce.opportunity Opp
		
WHERE
test__c = false
AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')

GROUP BY
locale__c
, customer__c
, name		
, sfid	
, status__c ) AS Opps


		LEFT JOIN 
		LATERAL
		(		
		SELECT

		RelatedContact__c 													AS Contact
		
	
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END)
			+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						THEN 1 
						ELSE 0 END) 											AS all_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END) 											AS lost_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
						THEN 1 
						ELSE 0 END)												AS all_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS b2b_outbound
		
		FROM 
		Salesforce.natterbox_call_reporting_object__c t4
		
			LEFT JOIN 
			salesforce.contact t5
			
			ON
			(t4.relatedcontact__c = t5.sfid)
		WHERE
		-- -- Calls created within the last 8 weeks incl. Current_Date
		call_start_date_date__c::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks' 
													AND to_date('2018-05-03', 'YYYY-MM-DD')
		
		AND 	(	(calldirection__c = 'Inbound' 	AND E164Callednumber__C IN ('493030807263', '493030807264'))
				OR (calldirection__c = 'Outbound' 	AND Callerfirstname__c LIKE 'CM%'))
				
		AND RelatedContact__c IS NOT NULL
		
		GROUP BY
		Contact) as Call
		
		ON
		(Opps.Customer = Call.Contact)	
