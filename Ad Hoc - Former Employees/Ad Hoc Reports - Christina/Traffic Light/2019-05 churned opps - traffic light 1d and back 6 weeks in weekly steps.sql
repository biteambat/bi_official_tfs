SELECT tl_1.locale								AS opp_locale
		, opps.*
		, tl_1.status							AS opp_status
		, tl_1.owner							AS opp_owner

-- status change date - 1 day 
		, tl_1.date										AS d1_date
		, tl_1.operated_by								AS d1_operated_by
		, tl_1.operated_by_detail 						AS d1_operated_by_detail
		, tl_1.traffic_light_noshow 					AS d1_orders_noshow_orders
		, tl_1.traffic_light_cancelled_pro				AS d1_orders_cancelled_pro
		, tl_1.traffic_light_cancelled_customer			AS d1_orders_cancelled_cust
		, tl_1.traffic_light_inbound_calls				AS d1_inbound_inbound_calls
		, tl_1.traffic_light_lost_calls 				AS d1_inbound_lost_calls
		, tl_1.traffic_light_createdcases				AS d1_inbound_cases_created
		, tl_1.traffic_light_retention					AS d1_intern_retention
		, tl_1.traffic_light_invoicecorrection			AS d1_intern_invoice
		, tl_1.traffic_light_professionalimprovement 	AS d1_intern_pro_impro
		, tl_1.avg_traffic_light						AS d1_avg_traffic_light
-- status change date - 8 day 
		, tl_8.date										AS d8_date
		, tl_8.operated_by								AS d8_operated_by
		, tl_8.operated_by_detail 						AS d8_operated_by_detail
		, tl_8.traffic_light_noshow 					AS d8_orders_noshow_orders
		, tl_8.traffic_light_cancelled_pro				AS d8_orders_cancelled_pro
		, tl_8.traffic_light_cancelled_customer			AS d8_orders_cancelled_cust
		, tl_8.traffic_light_inbound_calls				AS d8_inbound_inbound_calls
		, tl_8.traffic_light_lost_calls 				AS d8_inbound_lost_calls
		, tl_8.traffic_light_createdcases				AS d8_inbound_cases_created
		, tl_8.traffic_light_retention					AS d8_intern_retention
		, tl_8.traffic_light_invoicecorrection			AS d8_intern_invoice
		, tl_8.traffic_light_professionalimprovement 	AS d8_intern_pro_impro
		, tl_8.avg_traffic_light						AS d8_avg_traffic_light
-- status change date - 15 day 
		, tl_15.date									AS d15_date
		, tl_15.operated_by								AS d15_operated_by
		, tl_15.operated_by_detail 						AS d15_operated_by_detail
		, tl_15.traffic_light_noshow 					AS d15_orders_noshow_orders
		, tl_15.traffic_light_cancelled_pro				AS d15_orders_cancelled_pro
		, tl_15.traffic_light_cancelled_customer		AS d15_orders_cancelled_cust
		, tl_15.traffic_light_inbound_calls				AS d15_inbound_inbound_calls
		, tl_15.traffic_light_lost_calls 				AS d15_inbound_lost_calls
		, tl_15.traffic_light_createdcases				AS d15_inbound_cases_created
		, tl_15.traffic_light_retention					AS d15_intern_retention
		, tl_15.traffic_light_invoicecorrection			AS d15_intern_invoice
		, tl_15.traffic_light_professionalimprovement 	AS d15_intern_pro_impro
		, tl_15.avg_traffic_light						AS d15_avg_traffic_light
-- status change date - 22 day 
		, tl_22.date									AS d22_date
		, tl_22.operated_by								AS d22_operated_by
		, tl_22.operated_by_detail 						AS d22_operated_by_detail
		, tl_22.traffic_light_noshow 					AS d22_orders_noshow_orders
		, tl_22.traffic_light_cancelled_pro				AS d22_orders_cancelled_pro
		, tl_22.traffic_light_cancelled_customer		AS d22_orders_cancelled_cust
		, tl_22.traffic_light_inbound_calls				AS d22_inbound_inbound_calls
		, tl_22.traffic_light_lost_calls 				AS d22_inbound_lost_calls
		, tl_22.traffic_light_createdcases				AS d22_inbound_cases_created
		, tl_22.traffic_light_retention					AS d22_intern_retention
		, tl_22.traffic_light_invoicecorrection			AS d22_intern_invoice
		, tl_22.traffic_light_professionalimprovement 	AS d22_intern_pro_impro
		, tl_22.avg_traffic_light						AS d22_avg_traffic_light
-- status change date - 29 day 
		, tl_29.date									AS d29_date
		, tl_29.operated_by								AS d29_operated_by
		, tl_29.operated_by_detail 						AS d29_operated_by_detail
		, tl_29.traffic_light_noshow 					AS d29_orders_noshow_orders
		, tl_29.traffic_light_cancelled_pro				AS d29_orders_cancelled_pro
		, tl_29.traffic_light_cancelled_customer		AS d29_orders_cancelled_cust
		, tl_29.traffic_light_inbound_calls				AS d29_inbound_inbound_calls
		, tl_29.traffic_light_lost_calls 				AS d29_inbound_lost_calls
		, tl_29.traffic_light_createdcases				AS d29_inbound_cases_created
		, tl_29.traffic_light_retention					AS d29_intern_retention
		, tl_29.traffic_light_invoicecorrection			AS d29_intern_invoice
		, tl_29.traffic_light_professionalimprovement 	AS d29_intern_pro_impro
		, tl_29.avg_traffic_light						AS d29_avg_traffic_light
-- status change date - 36 day 
		, tl_36.date									AS d36_date
		, tl_36.operated_by								AS d36_operated_by
		, tl_36.operated_by_detail 						AS d36_operated_by_detail
		, tl_36.traffic_light_noshow 					AS d36_orders_noshow_orders
		, tl_36.traffic_light_cancelled_pro				AS d36_orders_cancelled_pro
		, tl_36.traffic_light_cancelled_customer		AS d36_orders_cancelled_cust
		, tl_36.traffic_light_inbound_calls				AS d36_inbound_inbound_calls
		, tl_36.traffic_light_lost_calls 				AS d36_inbound_lost_calls
		, tl_36.traffic_light_createdcases				AS d36_inbound_cases_created
		, tl_36.traffic_light_retention					AS d36_intern_retention
		, tl_36.traffic_light_invoicecorrection			AS d36_intern_invoice
		, tl_36.traffic_light_professionalimprovement 	AS d36_intern_pro_impro
		, tl_36.avg_traffic_light						AS d36_avg_traffic_light
-- status change date - 43 day 
		, tl_43.date									AS d43_date
		, tl_43.operated_by								AS d43_operated_by
		, tl_43.operated_by_detail 						AS d43_operated_by_detail
		, tl_43.traffic_light_noshow 					AS d43_orders_noshow_orders
		, tl_43.traffic_light_cancelled_pro				AS d43_orders_cancelled_pro
		, tl_43.traffic_light_cancelled_customer		AS d43_orders_cancelled_cust
		, tl_43.traffic_light_inbound_calls				AS d43_inbound_inbound_calls
		, tl_43.traffic_light_lost_calls 				AS d43_inbound_lost_calls
		, tl_43.traffic_light_createdcases				AS d43_inbound_cases_created
		, tl_43.traffic_light_retention					AS d43_intern_retention
		, tl_43.traffic_light_invoicecorrection			AS d43_intern_invoice
		, tl_43.traffic_light_professionalimprovement 	AS d43_intern_pro_impro
		, tl_43.avg_traffic_light						AS d43_avg_traffic_light


FROM 
(
SELECT 	opp.sfid							opp_id
		, opp.name							opp_name
		, opp.ownerid						opp_owner
		, MIN(opphi.createddate::date) 		statuschange_date
		, opp.churn_reason__c				opp_churn_reason
			

FROM salesforce.opportunity 					opp
LEFT JOIN 	salesforce.opportunityfieldhistory 	opphi 	ON opp.sfid 		= opphi.opportunityid
														AND opphi.field  	IN ('status__c')
														AND opphi.newvalue 	IN ('OFFBOARDING','RETENTION')
														
WHERE opp.test__c IS FALSE
		AND opphi.sfid IS NOT NULL

GROUP BY opp.sfid							
		, opp.name							
		, opp.ownerid
		, opp.churn_reason__c				

														
) AS opps

LEFT JOIN bi.opportunity_traffic_light_new 		tl_1 		ON tl_1.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '1 day' 	= tl_1.date
															
LEFT JOIN bi.opportunity_traffic_light_new 		tl_8 		ON tl_8.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '8 day' 	= tl_8.date		
															
LEFT JOIN bi.opportunity_traffic_light_new 		tl_15 		ON tl_15.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '15 day' 	= tl_15.date
															
LEFT JOIN bi.opportunity_traffic_light_new 		tl_22 		ON tl_22.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '22 day' 	= tl_22.date	
															
LEFT JOIN bi.opportunity_traffic_light_new 		tl_29 		ON tl_29.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '29 day' 	= tl_29.date	
															
LEFT JOIN bi.opportunity_traffic_light_new 		tl_36 		ON tl_36.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '36 day' 	= tl_36.date	
															
LEFT JOIN bi.opportunity_traffic_light_new 		tl_43 		ON tl_43.opportunity  							= opps.opp_id
															AND opps.statuschange_date - interval '43 day' 	= tl_43.date																																
															
WHERE 	opps.statuschange_date::date >= '2019-05-01'
		AND opps.statuschange_date::date <= '2019-05-31'


-- tl.opportunity = '0060J00000lcKy8QAE'

--LIMIT 100

;