SELECT
Year,
Month,
SUM(weekday.Monday) AS Monday,
SUM(weekday.Tuesday) AS Tuesday,
SUM(weekday.Wednesday) AS Wednesday,
SUM(weekday.Thursday) AS Thursday,
SUM(weekday.Friday) AS Friday,
SUM(weekday.Saturday) AS Saturday,
SUM(weekday.Sunday) AS Sunday

FROM
		(SELECT
		effectivedate,
		EXTRACT (year FROM effectivedate::date) as Year,
		EXTRACT (month FROM effectivedate::date) as Month,
		COUNT(sfid) AS Orders,
		date_part('dow', effectivedate) AS date_name,
		(CASE WHEN (date_part('dow', effectivedate) = '1') THEN 1 ELSE 0 END) AS Monday,
		(CASE WHEN (date_part('dow', effectivedate) = '2') THEN 1 ELSE 0 END) AS Tuesday,
		(CASE WHEN (date_part('dow', effectivedate) = '3') THEN 1 ELSE 0 END) AS Wednesday,
		(CASE WHEN (date_part('dow', effectivedate) = '4') THEN 1 ELSE 0 END) AS Thursday,
		(CASE WHEN (date_part('dow', effectivedate) = '5') THEN 1 ELSE 0 END) AS Friday,
		(CASE WHEN (date_part('dow', effectivedate) = '6') THEN 1 ELSE 0 END) AS Saturday,
		(CASE WHEN (date_part('dow', effectivedate) = '0') THEN 1 ELSE 0 END) AS Sunday
		
		FROM
		salesforce."order"
		WHERE
		
		EXTRACT (month FROM effectivedate::date) = EXTRACT (month FROM current_date::date)
		AND EXTRACT (year FROM effectivedate::date) = EXTRACT (year FROM current_date::date)
		
		GROUP BY
		effectivedate) AS weekday

GROUP BY 
Year,
Month