SELECT
	 customer_id,
	 name,
	 payment_method__c,
	 finance_outstanding_balance__c,
	 termination_date

	 
FROM
	(SELECT
		customer_id,
		min(date) as termination_date
	FROM
		bi.recurrent_customers_terminated t1
	JOIN
		bi.orders t2
	ON
		(t1.customer_id = t2.contact__c)
	WHERE
		left(t1.locale__c,2) = 'ch'
		and t2.order_type = '1'
	GROUP BY
		customer_id
	HAVING
		min(date) >= '2016-01-01') as t1



		
		LEFT JOIN
			(SELECT
				name,
				sfid,
				payment_method__c,
				finance_outstanding_balance__c
			FROM
				salesforce.contact) as t3
	ON
		(t1.customer_id = t3.sfid)
		
		
		LEFT JOIN
			(SELECT
				contact__c,
				COUNT(1) as Orders
			FROM
				salesforce.order
			WHERE
				status not like '%CANCELLED%'
				AND effectivedate::date > current_date
			GROUP BY
				contact__c
			HAVING
				COUNT(1) < 1) as t4
	ON
		(t1.customer_id = t4.contact__c)

				
		
WHERE
t3.payment_method__c = 'wiretransfer'
		
	