-- rebuild salforce report: https://eu7.salesforce.com/00O0J0000074D80
-- Div 25 cases (sf: 505; query: 480)

SELECT 		TO_CHAR(o.effectivedate, 'YYYY-MM') AS  year_month,
			, ca.sfid
			, ca.casenumber
			, ca.isclosed
			, ca.ownerid
			, u.name
			, ca.origin
			, ca.type
			, ca.reason
			, ca.status
			, ca.contactid
			, co.name
			, co.type__c
			, co.company_name__c
			, ca.order__c
			, o.type
			, ca.accountid
			, a.name
			, a.company_name__c
			, ca.opportunity__c
			, opp.name
			, opp.grand_total__c
			-- , ca.segment					-250 / 250-500 / 500-1000 / 1000+
			-- , *
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE		ca.isclosed				= FALSE																					-- 1										
	AND	(																													-- 2
				u.name				NOT SIMILAR TO 	'%(Accounting|TOShared|Marketing|BAT B2B Admin Queue)%'
			AND u.name				NOT SIMILAR TO 	'%(Nicolai|Bätcher|Kharoo|Haferkorn|Heumer|Ahlers|Ribeiro|Klonaris|Kiekebusch|Steven|André)%'
		)
	AND (	
	
	-- old case setup
			(	(
				ca.origin			SIMILAR TO 		'%(B2B - Contact|B2B de - Customer dashboard|B2B DE|B2B CH|TFS CM)%' 	-- 3
			AND ca.type				NOT SIMILAR TO	'%(CLM HR|CLM|CM B2C|Sales|PM)%' 										-- 11
				)
			OR	(
				ca.origin			SIMILAR TO		'%(CM|Insurance|checkout|partner portal)%'								-- 4
			AND ca.type				SIMILAR TO		'%(KA|B2B)%'															-- 5
				)
			)
			
	-- new case setup
		OR	( 	(
				ca.origin			SIMILAR TO		'%(B2B customer)%'														-- 7 
			AND ca.type				SIMILAR TO		'%(CM B2B|Pool)%'														-- 9
				)
			OR 	(
			--	ca.origin			NOT SIMILAR TO	'%(B2B customer)%'														-- 8
			-- AND	
			ca.type				SIMILAR TO		'%(CM B2B)%'															-- 10
				)
			)							
		)
;

SELECT 		 a.partnername							partner
			, a.partnerid							partnerID
			, a.partnerpph							partnerpph
			, opp.name								ppportunity
			, opp.monthly_partner_costs__c			ppp_monthly_partner_costs
			--, o.effectivedate						order_effectivedate
			--, o.professional__c
			--, o.status								order_status
			--, o.order_duration__c					order_duration
			--,*
			
			-- number of order
			, COUNT(*)								orders
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 			THEN 1 ELSE 0 END )		AS FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 				THEN 1 ELSE 0 END )		AS INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN 1 ELSE 0 END )		AS CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN 1 ELSE 0 END )		AS CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN 1 ELSE 0 END )		AS CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN 1 ELSE 0 END )		AS CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'		THEN 1 ELSE 0 END )		AS NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN 1 ELSE 0 END )		AS NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN 1 ELSE 0 END )		AS PENDING_ALLOCATION						
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'		THEN 1 ELSE 0 END )		AS PENDING_TO_START	
			
			-- hours (order duration)
			, SUM ( CASE WHEN 	o.status 	LIKE 	'FULFILLED' 			THEN o.order_duration__c ELSE 0 END )		AS h_FULFILLED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'INVOICED' 				THEN o.order_duration__c ELSE 0 END )		AS h_INVOICED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_CUSTOMER' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_MISTAKE' 	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_MISTAKE
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_PROFESSIONAL'THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'CANCELLED_TERMINATED'	THEN o.order_duration__c ELSE 0 END )		AS h_CANCELLED_TERMINATED
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_CUSTOMER'		THEN o.order_duration__c ELSE 0 END )		AS h_NOSHOW_CUSTOMER
			, SUM ( CASE WHEN 	o.status 	LIKE 	'NOSHOW_PROFESSIONAL'	THEN o.order_duration__c ELSE 0 END )		AS h_NOSHOW_PROFESSIONAL
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_ALLOCATION'	THEN o.order_duration__c ELSE 0 END )		AS h_PENDING_ALLOCATION	
			, SUM ( CASE WHEN 	o.status 	LIKE 	'PENDING_TO_START'		THEN o.order_duration__c ELSE 0 END )		AS h_PENDING_TO_START						
			
FROM		salesforce.order 		o
			
LEFT JOIN	salesforce.opportunity	opp 	ON o.opportunityid 	= opp.sfid
LEFT JOIN 	(	SELECT 			pro.sfid			proid
								, pro.name			proname
								, pro.parentid		parentid
								, partner.sfid		partnerid
								, partner.name		partnername
								, pro.locale__c		locale
								, partner.pph__c	partnerpph
								
				FROM 			salesforce.account 	pro 
				LEFT JOIN 		salesforce.account 	partner 	ON 	pro.parentid = partner.sfid 
				WHERE 			pro.parentid		IS NOT NULL
			) 						AS a	ON o.professional__c		= a.proid


-- LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid


WHERE 		o.test__c 						= FALSE
			AND o.effectivedate::date 		>= '2018-10-01'
			AND o.effectivedate::date 		<= '2018-10-31'
			AND o.type						IN ('cleaning-b2b','cleaning-window')
			AND a.locale					LIKE ('de-%')
			AND a.parentid 					IS NOT NULL


GROUP BY 	year_month
			, a.partnername
			, a.partnerid
			, a.partnerpph
			, opp.name
			, opp.monthly_partner_costs__c
			--, o.status
;	
