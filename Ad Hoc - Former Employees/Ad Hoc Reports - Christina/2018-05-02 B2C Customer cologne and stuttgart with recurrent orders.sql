-- Author: Christina Janson
-- Date of Creation: 2018-05-02
-- Short Description: This query contains all B2C customers with recurrent orders between 2017-01-01 and 2018-04-30 in de-stuttgart and de-cologne

SELECT
date
, delivery_area__C
, contact__c
, customer_name__c
, SUM(gmv__c) AS gmv
, SUM(grand_total_eur__c) AS grand_total_eur
, COUNT(sfid) As Orders

FROM
			
			(
			SELECT
			to_char(effectivedate::date, 'YYYY-MM') AS date
			, delivery_area__c
			, contact__c
			, customer_name__c
			, gmv__c
			, grand_total_eur__c
			, sfid	
			
			FROM 
			salesforce."order"
			
			WHERE
			test__c = false
			AND effectivedate::date BETWEEN '2017-10-01' AND '2018-04-30'
			AND type = 'cleaning-b2c'
			AND recurrency__c > 0
			AND delivery_area__c IN ('de-stuttgart', 'de-cologne')
			AND status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
			
			) AS t1

GROUP BY
delivery_area__c
, contact__c
, customer_name__c
, date


