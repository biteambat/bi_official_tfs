-- CH B2B Professionals - former ALL TIME
-- 
-- account.status LIKE 'LEFT', 'TERMINATED
-- 

SELECT

id AS id
, name AS name
, billingstreet 
, billingpostalcode
, billingcity
, billingcountry
, hr_social_security_number__c AS social_security_number
, hr_contract_start__c::date AS Acquisition_date

-- LEFT JOIN with order table, 
-- -- (row 57) COUNT sfid  AND order_duration GROUPED BY Professional
-- -- (row 70) WINDOW grouped by Professional to get the AVG per Professional 
, orders.AVG_per_month AS Number_of_orders_per_month
, orders.AVG_hours_per_month AS Number_of_hours_per_month

, rating__c AS rating
, status__c AS status

-- Uli requested the MAX Invoiced / Fullfilled Order as Termination Date --> quite often its empty
, lastorderdate AS Termination_date

-- maybe this should replace the TERMINATION DATE (row 27)
, hr_contract_end__c::date AS Contract_End_Date
, ending_reason__c AS Termination_reason

FROM 
salesforce.account as t1

LEFT JOIN 
LATERAL

-- LEFT JOIN with all INVOICED and FULLFILLED Orders for --> LAST ORDER DATE -> Termination Date

		( 
		SELECT
		professional__c AS professional
		, MAX(effectivedate::date) AS lastorderdate
		
		FROM 
		salesforce."order"
		
		WHERE
		professional__c IS NOT NULL
		AND status IN ('INVOICED', 'FULLFILLED')
		
		GROUP BY 
		professional__c

		) AS lastorder
		ON ( lastorder.professional = t1.sfid)


LEFT JOIN
LATERAL

		(SELECT
		professional
		, MIN( AVG_per_month) AS AVG_per_month
		, MIN( AVG_hours_per_month) AS AVG_hours_per_month
		
		FROM
		(
		SELECT
		professional
		, AVG(orders) OVER (PARTITION BY professional) AS AVG_per_month
		, AVG(hours) OVER (PARTITION BY professional) AS AVG_hours_per_month
		
		FROM(
		SELECT
		professional__c AS Professional
		, DATE_PART ('month', effectivedate) AS month
		, COUNT (sfid) AS orders
		, SUM (order_duration__c)  AS hours

		FROM
		salesforce."order"
		
		WHERE
		-- as they LEFT or TERMINATED far more then 4 month ago, the Order # and hours per month are calculated based on the month bevor TERMINATION
		DATE_PART ('year', effectivedate) = DATE_PART ('year', lastorder.lastorderdate)
		AND DATE_PART ('month', effectivedate) < DATE_PART ('month', lastorder.lastorderdate)
		AND status IN ('INVOICED', 'FULLFILLED')
		AND professional__c IS NOT NULL
		
		GROUP BY
		professional__c
		, month ) AS orders1
		
		GROUP BY 
		professional
		, orders
		, hours 
		 ) AS orders2

GROUP BY 
professional 

) AS orders

ON ( orders.professional = t1.sfid)		


WHERE
locale__c LIKE 'ch%'
AND type__c NOT LIKE 'cleaning-b2b'
AND status__c  IN ('LEFT', 'TERMINATED')

