SELECT
t1.name AS Opportunity,
year_month,
total_order_count,
total_hours,
t2.hours_weekly__c*4.33 AS expacted_monthly_hours

FROM 
	bi.b2borders t1

LEFT JOIN
	salesforce.opportunity t2

	ON	
	t1.opportunity_id = t2.sfid
	
WHERE
t1.name LIKE'%COREtransform GmbH%' 
OR t1.name LIKE 'VPSitex Deutschland GmbH (Suttnerstr. 14-16)'
OR t1.name LIKE 'COMPLEVO GmbH'
OR t1.name LIKE 'Nova Bauen GmbH'
OR t1.name LIKE 'Berndt + Partner GmbH'
OR t1.name LIKE 'Musikfonds e. V.'


