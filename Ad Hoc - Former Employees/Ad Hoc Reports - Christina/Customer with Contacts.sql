SELECT
sub4.week AS Week,
sub4.Year AS Year,
MIN(sub4.startdate_of_week) AS startdate_of_week,
sub4.locale AS locale,
SUM(sub4.CreatedCases) AS CreatedCases,
SUM(sub4.Outbound_Calls) AS Outbound_Calls,
SUM(sub4.Inbound_Calls) AS Inbound_Calls,
SUM(sub4.CreatedCases) + SUM(sub4.Outbound_Calls) + SUM(sub4.Inbound_Calls) AS All_Contacts,
COUNT(DISTINCT sub4.Contact) AS Customers_falsch,
SUM(sub4.All_Customers) AS All_Customers,
SUM(CASE WHEN sub4.All_Contacts > 0 THEN 1 ELSE 0 END) AS Customer_with_contact

FROM
				(SELECT
				EXTRACT(WEEK FROM sub1.date) as week,
				EXTRACT(Year FROM sub1.date) as Year,
				min(sub1.date) as startdate_of_week,
				sub1.locale AS locale,
				sub1.Contact AS Contact,
				sub1.ContactName,
				SUM(sub1.All_Customers) AS All_Customers,

				SUM(sub3.CreatedCases) AS CreatedCases,
				SUM(sub2.all_outbound) AS Outbound_Calls,
				SUM(sub2.all_inbound_calls) AS Inbound_Calls,
				SUM(sub3.CreatedCases) + SUM(sub2.all_outbound) + SUM(sub2.all_inbound_calls) AS All_Contacts,
				SUM(sub1.GMV_net) AS GMV_net -- ,
				
				-- SUM(all_inbound_calls+CreatedCases)/sum(invoiced_orders) as inbound_per_order,
				-- SUM(all_outbound-b2b_outbound) as outbound,
				-- SUM(all_inbound_calls+CreatedCases+(all_outbound-b2b_outbound))/sum(invoiced_orders) as contact_per_order
				
				FROM
				(SELECT
					effectivedate::date as date,
					left(locale__c,2) as locale,
					COUNT(DISTINCT(order_id__c)) as Invoiced_Orders,
					COUNT(DISTINCT contact__c) AS All_Customers,
					contact__c AS Contact,
					Customer_Name__c AS ContactName,
					gmv_eur_net AS GMV_net
				
				
				FROM
					bi.orders t1
				WHERE
					status in ('INVOICED','NOSHOW CUSTOMER')
					AND order_type = '1'
					AND effectivedate::date > '2017-03-01'
				GROUP BY
				date,
				left(locale__c,2),
				Contact,
				ContactName,
				GMV_net)  as sub1
				
				
				-- ------------------------------------------------------------------------------------------------------------------------------------------- CALLS 
				
				LEFT JOIN
					(SELECT
						call_start_date_date__c::date as date,
						t2.type__c,
						t2.sfid AS Contact,
						CASE 
							WHEN calledcountryshort__c = 'AUT' THEN 'at'
							WHEN calledcountryshort__c = 'CHE' THEN 'ch'
							WHEN calledcountryshort__c = 'NLD' THEN 'nl'
							ELSE 'de' END as locale,
						SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END)+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' THEN 1 ELSE 0 END) as all_inbound_calls,
						SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' THEN 1 ELSE 0 END) AS all_outbound,
						SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t2.type__c= 'customer-b2b' THEN 1 ELSE 0 END) AS b2b_outbound
					FROM 
						Salesforce.natterbox_call_reporting_object__c t1
					LEFT JOIN 
						salesforce.contact t2
					ON
						(t1.relatedcontact__c = t2.sfid)
					WHERE
					(Connnected_User_Full_Name__c like 'CM%'
						OR
						(E164Callednumber__C = '31208086378'
						OR E164Callednumber__C='41800002624'
						OR E164Callednumber__C='41800002625'
						OR E164Callednumber__C='43720880499'
						OR E164Callednumber__C='43800722433'
						OR E164Callednumber__C='493030807403'
						OR E164Callednumber__C='498007224330')
					AND calldirection__c = 'Inbound')
					OR
					(Callerfirstname__c like 'CM%'
					AND calldirection__c = 'Outbound'
					)
					GROUP BY
					locale,
					t2.type__c,
					date,
					Contact) as sub2
					ON
					(sub1.date = sub2.date AND sub1.locale = sub2.locale AND sub1.Contact = sub2.Contact)
				
				-- ------------------------------------------------------------------------------------------------------------------------------------------- CASES
				
					LEFT JOIN
						(SELECT 
							CreatedDate::date AS date,
							CASE
								WHEN (Origin LIKE '%CM DE%' OR Origin LIKE '%TM DE - General Email%' OR Origin LIKE '%checkout%')  THEN 'de'
								WHEN (Origin LIKE '%CM CH%' OR Origin LIKE '%TM CH - General Email%') THEN 'ch'
								WHEN (Origin LIKE '%CM AT%' OR Origin LIKE '%TM AT - General Email%') THEN 'at'
								WHEN (Origin LIKE '%CM NL%' OR Origin LIKE '%TM NL - General Email%') THEN 'nl'
								ELSE 'Other' END AS locale,
								contactid AS Contact,
							COUNT(1) as CreatedCases
						
						FROM
							Salesforce.case
						
						WHERE
							CreatedDate::date > '2017-03-01'
							and 
							(Origin LIKE '%CM DE%'
								OR Origin LIKE '%CM CH%'
								OR Origin LIKE '%CM AT%'
								OR Origin LIKE '%CM NL%'
								OR Origin LIKE '%TM AT - General Email%'
								OR Origin LIKE '%TM CH - General Email%'
								OR Origin LIKE '%TM DE - General Email%'
								OR Origin LIKE '%TM NL - General Email%'
								OR Origin LIKE '%checkout%')
							AND 
							(type NOT LIKE '%Accounting%' 
								OR type NOT LIKE '%Damage%')
							AND Isdeleted = false
						
						GROUP BY 
							locale, 
							date,
							Contact)
						AS sub3 
						
							ON
							(sub1.date = sub3.date AND sub1.locale = sub3.locale AND sub1.Contact = sub3.Contact)
							GROUP BY
							year,
							week,
							sub1.locale,
							sub1.Contact,
							sub1.ContactName) AS sub4
GROUP BY
Week,
Year,

locale
