-- CH B2B Professionals - active
-- 
-- account.status NOT LIKE 'LEFT', 'TERMINATED
-- 

SELECT

id AS id
-- , sfid
, name AS name
, billingstreet 
, billingpostalcode
, billingcity
, billingcountry
, hr_social_security_number__c AS social_security_number
, hr_contract_start__c::date AS Acquisition_date

-- LEFT JOIN with order table, 
-- -- (row 45) COUNT sfid  AND order_duration GROUPED BY Professional
-- -- (row 40) WINDOW grouped by Professional to get the AVG per Professional 
, orders.AVG_per_month AS Number_of_orders_per_month
, orders.AVG_hours_per_month AS Number_of_hours_per_month

, rating__c AS rating
, status__c AS status
, ending_reason__c


FROM 
salesforce.account as t1

	LEFT JOIN
	(SELECT
	professional
	, MIN( AVG_per_month) AS AVG_per_month
	, MIN( AVG_hours_per_month) AS AVG_hours_per_month
	
	FROM
	(
		SELECT
		professional
		, AVG(orders) OVER (PARTITION BY professional) AS AVG_per_month
		, AVG(hours) OVER (PARTITION BY professional) AS AVG_hours_per_month
		
		FROM(
		SELECT
		professional__c AS Professional
		, DATE_PART ('month', effectivedate) AS month
		, COUNT (sfid) AS orders
		, SUM (order_duration__c)  AS hours

		FROM
		salesforce."order"
		
		WHERE
		DATE_PART ('year', effectivedate) = '2018'
		AND DATE_PART ('month', effectivedate) < DATE_PART ('month', CURRENT_DATE)
		AND status IN ('INVOICED', 'FULFILLED')
		AND professional__c IS NOT NULL
		
		GROUP BY
		professional__c
		, month ) AS orders1
		
		GROUP BY 
		professional
		, orders
		, hours 
		 ) AS orders2

	GROUP BY 
	professional 
	
	) AS orders
	
	ON ( orders.professional = t1.sfid)		


WHERE
locale__c LIKE 'ch%'
AND type__c NOT LIKE 'cleaning-b2b'
AND status__c NOT IN ('LEFT', 'TERMINATED')
