SELECT
*
FROM
(
SELECT
CURRENT_DATE				AS date
, traffic_lights.*

-- AVG Traffic light:
-- if opp status = RETENTION -> traffic light has te be 1(red)
-- Order related Traffic Lights are trible-weightes. 
-- The rest are equally weighted.

, 

ROUND(

CASE WHEN Status = 'RETENTION' OR traffic_light_Retention = 1 THEN 1 ELSE
(((traffic_light_noshow + traffic_light_cancelled_pro)*3 
+ traffic_light_inbound_calls + traffic_light_lost_calls + traffic_light_CreatedCases
+ traffic_light_InvoiceCorrection + traffic_light_ProfessionalImprovement + 
 (CASE WHEN traffic_light_Retention = 2 THEN 1 ELSE traffic_light_Retention END )
) / 12) END , 1) 							AS AVG_Traffic_Light

FROM
(
SELECT
traffic_light_detail.*,

ROUND((CASE
WHEN nsp_Rate >= 0.05 THEN 1
WHEN nsp_Rate between 0.005 and 0.05 THEN 2
WHEN nsp_Rate <= 0.005 THEN 3 END), 1)  AS traffic_light_noshow,

ROUND((CASE
WHEN cp_Rate >= 0.1 THEN 1
WHEN cp_Rate between 0.005 and 0.1 THEN 2
WHEN cp_Rate <= 0.005 THEN 3 END), 1)  AS traffic_light_cancelled_pro,

ROUND((CASE
WHEN cc_Rate >= 0.2 THEN 1
WHEN cc_Rate between 0.005 and 0.2 THEN 2
WHEN cc_Rate <= 0.005 THEN 3 END), 1) AS traffic_light_cancelled_customer,

ROUND((CASE
WHEN all_inbound_calls >= 3 THEN 1
WHEN all_inbound_calls between 1 AND 3 THEN 2
WHEN all_inbound_calls <= 1 OR (all_inbound_calls IS NULL) = TRUE THEN 3 END), 1) AS traffic_light_inbound_calls,

ROUND((CASE
WHEN lost_inbound_calls >= 3 THEN 1
WHEN lost_inbound_calls between 1 AND 4 THEN 2
WHEN lost_inbound_calls <= 1 OR (lost_inbound_calls IS NULL) = TRUE THEN 3 END), 1) AS traffic_light_lost_calls,

ROUND((CASE
WHEN createdcases >= 3 THEN 1
WHEN createdcases between 1 AND 3 THEN 2
WHEN createdcases <=1 OR (createdcases IS NULL) = TRUE THEN 3 END), 1) AS traffic_light_CreatedCases,

ROUND((CASE
WHEN retention_all > 0 AND retention_closed <> retention_all THEN 1
WHEN retention_all > 0 AND retention_closed = retention_all THEN 2
WHEN retention_all =0 OR (retention_all IS NULL) = TRUE THEN 3 END), 1) AS traffic_light_Retention,

ROUND((CASE
WHEN invoicecorrection_all > 0 AND invoicecorrection_closed <> invoicecorrection_all THEN 1
WHEN invoicecorrection_all > 0 AND invoicecorrection_closed = invoicecorrection_all THEN 1
WHEN invoicecorrection_all =0 OR (invoicecorrection_all IS NULL) = TRUE THEN 3 END), 1) AS traffic_light_InvoiceCorrection,

ROUND((CASE
WHEN professionalimprovement_all > 0 AND professionalimprovement_closed <> professionalimprovement_all THEN 1
WHEN professionalimprovement_all > 0 AND professionalimprovement_closed = professionalimprovement_all THEN 1
WHEN professionalimprovement_all =0 OR (professionalimprovement_all IS NULL) = TRUE THEN 3 END), 1) AS traffic_light_ProfessionalImprovement


FROM
 
(
SELECT
O.*
-- , orders_final.delivery_area AS delivery_area_1
-- , last_order.delivery_area__c AS delivery_area_2
, CASE 	WHEN orders_final.delivery_area IS NULL AND last_order.delivery_area__c IS NOT NULL THEN last_order.delivery_area__c
			WHEN orders_final.delivery_area = 'unknown' AND last_order.delivery_area__c IS NOT NULL THEN last_order.delivery_area__c
			WHEN orders_final.delivery_area IS NOT NULL AND orders_final.delivery_area <> 'unknown' THEN orders_final.delivery_area
			ELSE 'unknown' END AS delivery_area
, last_order.operated_by
, last_order.operated_by_detail

-- orders

, orders_final.orders_nsp
, orders_final.nsp
, (CASE WHEN orders_final.orders_nsp > 0 THEN orders_final.nsp/orders_final.orders_nsp ELSE 0 END) AS nsp_Rate

, orders_final.orders_cp
, orders_final.cp
, (CASE WHEN orders_final.orders_cp > 0 THEN orders_final.cp/orders_final.orders_cp ELSE 0 END) 	AS cp_Rate

, orders_final.orders_cc
, orders_final.cc
, (CASE WHEN orders_final.orders_cc > 0 THEN orders_final.cc/orders_final.orders_cc ELSE 0 END) 	AS cc_Rate

, orders_final.orders_nsp_cp
, orders_final.nsp_cp
, (CASE WHEN orders_final.orders_nsp_cp > 0 THEN orders_final.nsp_cp/orders_final.orders_nsp_cp ELSE 0 END) AS nso_cp_Rate

-- inbound cases

, Inbound_Cases_final.createdcases
, Inbound_Cases_final.opencases

-- internal cases

, Intern_Cases_final.retention_all
, Intern_Cases_final.retention_closed
, Intern_Cases_final.invoicecorrection_all
, Intern_Cases_final.invoicecorrection_closed
, Intern_Cases_final.professionalimprovement_all
, Intern_Cases_final.professionalimprovement_closed

-- calls

, Calls_final.all_inbound_calls
, Calls_final.lost_inbound_calls
, Calls_final.all_outbound
, Calls_final.b2b_outbound

FROM
(SELECT
locale__c																				AS locale
, customer__c																			AS Customer
, opp.name																				AS Opportunity_Name
, opp.sfid																				AS Opportunity
, status__c																				AS Status
, COUNT (opp.sfid) OVER (PARTITION BY customer__c) 						AS Opps
, tuser.name																			AS Owner
, CASE WHEN opp.grand_total__c 	IS NOT NULL THEN opp.grand_total__c
-- WHEN opp.amount 				IS NOT NULL THEN opp.amount
WHEN opp.hours_weekly__c 	IS NOT NULL THEN (opp.hours_weekly__c*opp.plan_pph__c*4.33)+(CASE WHEN pps__c IS NULL THEN 0 ELSE pps__c END)
WHEN opp.hours_weekly__c 	IS NULL 		THEN (2*opp.plan_pph__c*4.33)+(CASE WHEN pps__c IS NULL THEN 0 ELSE pps__c END)
		 ELSE 0 END																		AS Revenue

FROM
		Salesforce.opportunity Opp
		
		LEFT JOIN salesforce.user tuser ON (Opp.ownerid = tuser.sfid)
				
		
WHERE
test__c = false
-- AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')

GROUP BY
locale__c
, customer__c
, opp.name		
, opp.sfid	
, status__c
, owner
, grand_total__c
-- , opp.amount
, opp.hours_weekly__c
, opp.plan_pph__c
, pps__c

) AS O

-- --
-- -- last order details
-- -- served by BAT (TFS) or Partner

LEFT JOIN

		(SELECT DISTINCT ON (opportunityid)
		opportunityid AS Opp
		, effectivedate::date 
		, sfid AS orderid
		, delivery_area__c
		, professional__c  AS Professional
		, operated_by
		, operated_by_detail
				 -- add any other column (expression) from the same row
		FROM   salesforce."order" AS orders
		
		LEFT JOIN
			(SELECT
				Professional.sfid AS professional,
				CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE 'Partner' END AS operated_by,
				CASE WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT' ELSE Partner.name END AS operated_by_detail
		
			FROM
				Salesforce.Account Professional
		
				LEFT JOIN Salesforce.Account Partner
					ON (Professional.parentid = Partner.sfid)
			)	AS ProfessionalDetails
		
			ON (orders.professional__c = ProfessionalDetails.professional	)
		
		WHERE
		opportunityid IS NOT NULL
		
		ORDER  BY 
		opportunityid, 
		effectivedate::date DESC
		
		) AS last_order

ON (last_order.Opp = O.opportunity)

-- --
-- -- orders
-- -- Orders Order Start Date (effectivedate) within the last 8 weeks incl. CURRENT_DATE

LEFT JOIN		
		
		(
		SELECT
		Opp
		, delivery_area
		, SUM(Orders_NSP)			AS Orders_NSP
		, SUM(NSP)					AS NSP
		, SUM(Orders_CP)			AS Orders_CP
		, SUM(CP)					AS CP
		, SUM(Orders_CC)			AS Orders_CC
		, SUM(CC)					AS CC
		, SUM(Orders_NSP_CP)		AS Orders_NSP_CP
		, SUM(NSP_CP)				AS NSP_CP
		
		FROM (
		SELECT 
		
		opportunityid AS Opp
		-- , delivery_area__c 																		AS delivery_area_mass
		, CASE 	WHEN (FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c)) IS NULL 
					THEN 'unknown'
					ELSE FIRST_VALUE (delivery_area__c) OVER (PARTITION BY opportunityid ORDER BY delivery_area__c) END		AS delivery_area
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 				THEN 1 
						WHEN status LIKE '%FULFILLED%' 				THEN 1 ELSE 0 END) 	AS Orders_NSP
		
		, SUM(CASE 	WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%'		THEN 1 
						WHEN status like '%INVOICED%' 				THEN 1 
						WHEN status LIKE '%FULFILLED%' 				THEN 1 ELSE 0 END) 	AS Orders_CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 ELSE 0 END) 	AS CP
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_CC
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED CUSTOMER%' 		THEN 1 ELSE 0 END) 	AS CC
		
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 
						WHEN status LIKE '%INVOICED%' 					THEN 1 
						WHEN status LIKE '%FULFILLED%' 					THEN 1 ELSE 0 END) 	AS Orders_NSP_CP
						
		, SUM(CASE 	WHEN status LIKE '%CANCELLED PROFESSIONAL%' 	THEN 1 
						WHEN status LIKE '%NOSHOW PROFESSIONAL%' 		THEN 1 ELSE 0 END) 	AS NSP_CP
									
		FROM
		salesforce."order" AS orders
		
		WHERE
		effectivedate::date 	BETWEEN CURRENT_DATE - INTERVAL '8 Weeks' 
									AND CURRENT_DATE
-- 
-- effectivedate::date 	BETWEEN to_date('2018-05-03', 'YYYY-MM-DD') - INTERVAL '8 Weeks'
-- 									AND to_date('2018-05-03', 'YYYY-MM-DD')
											
		AND opportunityid IS NOT NULL
		
		GROUP BY
		Opp
		, delivery_area__c 
		
		) AS orders2
		
		GROUP BY
		Opp
		, delivery_area
		
) AS orders_final
		
ON (orders_final.opp = O.opportunity)

		
-- --
-- -- inbound cases
-- -- Cases created within the last 8 weeks incl. CURRENT_DATE
-- -- 	type = KA OR CM B2B 
-- --	OR origin 
-- -- AND created by API
		
LEFT JOIN 
LATERAL
		
		(
		SELECT
		Opportunity
		, SUM(createdcases)										AS createdcases
		, SUM(OpenCases)										AS OpenCases
		
		FROM
		(
		
		SELECT
		Opps.*
		-- , OpenCases
		, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * OpenCases / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * OpenCases / Opps END)
							ELSE (CASE 	WHEN 1.0 * OpenCases IS NULL
											THEN 0.00
											ELSE 1.0 * OpenCases END) END)::numeric, 2)							AS OpenCases
		, ROUND((CASE 	WHEN Inbound_Cases.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * createdcases / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * createdcases / Opps END)
							ELSE (CASE 	WHEN 1.0 * createdcases IS NULL
											THEN 0.00
											ELSE 1.0 * createdcases END) END)::numeric, 2)							AS createdcases
		
		FROM
		(
		SELECT
		locale__c				AS locale
		, customer__c			AS Customer
		, name					AS Opportunity_Name
		, sfid					AS Opportunity
		, status__c				AS Status
		, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps
		
		FROM
				Salesforce.opportunity Opp
				
		WHERE
		test__c = false
-- 		AND stagename IN ('WON', 'PENDING') 
		AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')
		
		GROUP BY
		locale__c
		, customer__c
		, name		
		, sfid	
		, status__c ) AS Opps
		
		
		LEFT JOIN 
		LATERAL
		
		
		(SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity
		, SUM (CASE WHEN isclosed = FALSE THEN 1 ELSE 0 END) 													AS OpenCases
		, SUM(CASE WHEN CreatedDate::date between CURRENT_DATE - INTERVAL '8 Weeks' and CURRENT_DATE THEN 1 ELSE 0 END) AS CreatedCases
		FROM
		Salesforce.case		
		WHERE
		-- CreatedDate::date between CURRENT_DATE - INTERVAL '8 Weeks' and CURRENT_DATE

		--AND 
		(type LIKE '%KA%'
		OR
		((Origin LIKE '%B2B - Contact%'
		OR Origin LIKE '%B2B AT - CLOSET%'
		OR Origin LIKE '%B2B CH - CLOSET%'
		OR Origin LIKE '%B2B DE - CLOSET%'
		OR Origin LIKE '%B2B NL - CLOSET%'
		OR Origin LIKE '%TFS CM%'
		OR Origin LIKE '%B2B de - Customer dashboard%')
		AND type != 'KA')
		OR (Origin LIKE '%B2B customer%' AND (type = 'CM B2B' OR type = 'Pool'))
		OR (Origin NOT LIKE '%B2B customer%' AND type = 'CM B2B')
		)
		AND Isdeleted = false
		AND CreatedById = '00520000003IiNCAA0'
		
		GROUP BY 
		Contact
		, Opportunity
		
		) AS Inbound_Cases
		
		ON (CASE WHEN Inbound_Cases.Opportunity IS NULL 
				THEN Opps.Customer = Inbound_Cases.Contact 
				ELSE Inbound_Cases.Opportunity = Opps.Opportunity END)
				
		) AS Inbound_Cases_f
		
		GROUP BY
		Opportunity
		
) AS Inbound_Cases_final

ON (Inbound_Cases_final.Opportunity = O.Opportunity)




-- --
-- -- internal cases:
-- -- Cases created within the last 8 weeks incl. CURRENT_DATE
-- -- 	Customer - Retention 		> communication with sales
-- -- 	Order - Invoice editing 	> communication with finance
-- -- 	Professional - Improvement > communication with TO

LEFT JOIN

		(SELECT
		Opportunity
		, SUM(retention_all)								AS retention_all
		, SUM(Retention_closed)							AS Retention_closed
		, SUM(InvoiceCorrection_All)					AS InvoiceCorrection_All
		, SUM(InvoiceCorrection_closed)				AS InvoiceCorrection_closed
		, SUM(ProfessionalImprovement_All)			AS ProfessionalImprovement_All
		, SUM(ProfessionalImprovement_closed)		AS ProfessionalImprovement_closed
		
		FROM
		(
		SELECT
		Opps.*
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * retention_all / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * retention_all / Opps END)
							ELSE (CASE 	WHEN 1.0 * retention_all IS NULL
											THEN 0.00
											ELSE 1.0 * retention_all END) END)::numeric, 2)							AS retention_all
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * Retention_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * Retention_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * Retention_closed IS NULL
											THEN 0.00
											ELSE 1.0 * Retention_closed END) END)::numeric, 2)						AS Retention_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_All IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_All END) END)::numeric, 2)				AS InvoiceCorrection_All
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * InvoiceCorrection_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * InvoiceCorrection_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * InvoiceCorrection_closed IS NULL
											THEN 0.00
											ELSE 1.0 * InvoiceCorrection_closed END) END)::numeric, 2)			AS InvoiceCorrection_closed
		
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_All / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_All / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_All IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_All END) END)::numeric, 2)		AS ProfessionalImprovement_All
											
		, ROUND((CASE 	WHEN Intern.opportunity IS NULL
							THEN (CASE 	WHEN 1.0 * ProfessionalImprovement_closed / Opps IS NULL  
											THEN 0.00 
											ELSE 1.0 * ProfessionalImprovement_closed / Opps END)
							ELSE (CASE 	WHEN 1.0 * ProfessionalImprovement_closed IS NULL
											THEN 0.00
											ELSE 1.0 * ProfessionalImprovement_closed END) END)::numeric, 2) 	AS ProfessionalImprovement_closed
		
		FROM
		(
		SELECT
		locale__c				AS locale
		, customer__c			AS Customer
		, name					AS Opportunity_Name
		, sfid					AS Opportunity
		, status__c				AS Status
		, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps
		
		FROM
				Salesforce.opportunity Opp
				
		WHERE
		test__c = false
-- 		AND stagename IN ('WON', 'PENDING') 
		AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')
		
		GROUP BY
		locale__c
		, customer__c
		, name		
		, sfid	
		, status__c ) AS Opps
		
	
		LEFT JOIN 
		LATERAL
		
		(
		SELECT 
		Contactid 																								AS Contact
		, opportunity__c 																						AS Opportunity
		-- ,sfid
		, SUM (CASE WHEN Contactid IS NULL THEN 1 ELSE 0 END ) 									AS Cases_without_Opp
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) 			AS Retention_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Customer - Retention%'
		AND isclosed = 'true' THEN 1 ELSE 0 END) 														AS Retention_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment / invoice%'
		OR subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) 								AS InvoiceCorrection_All
		--
		, SUM(CASE 	WHEN reason LIKE '%Order - Invoice editing%'
		OR reason LIKE '%Order - payment /invoice%'
		OR subject LIKE '%nvoice %orrection%'
		AND isclosed = 'true' THEN 1 ELSE 0 END)														AS InvoiceCorrection_closed
		--
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) 	AS ProfessionalImprovement_All
		, SUM(CASE 	WHEN reason LIKE '%Professional - Improvement%'
						AND isclosed = 'true' THEN 1 ELSE 0 END) 										AS ProfessionalImprovement_closed
		
		FROM
		Salesforce.case	interncases	
		
		WHERE
			-- Cases created within the last 8 weeks incl. CURRENT_DATE
			CreatedDate::date between CURRENT_DATE - INTERVAL '8 Weeks' 
									AND CURRENT_DATE	
			
			AND Isdeleted = false
			
			-- internal cases: 
			-- 	Customer - Retention 		> communication with sales
			-- 	Order - Invoice editing 	> communication with finance 
			-- 	Professional - Improvement 	> communication with TO
			
			AND ((reason LIKE '%Customer - Retention%' 
						OR (reason LIKE '%Order - Invoice editing%' OR reason LIKE '%Order - payment / invoice%') 
						
					OR (reason LIKE '%Professional - Improvement%' 
						AND (type LIKE '%TO%' OR type LIKE '%CLM%'))))
		
		
		GROUP BY
		Contact
		, Opportunity
		
		) AS Intern
		
		ON (CASE WHEN Intern.Opportunity IS NULL 
				THEN Opps.Customer = Intern.Contact 
				ELSE Intern.Opportunity = Opps.Opportunity END)
				
		)		AS Intern_f
		
		GROUP BY
		Opportunity
		
) AS Intern_Cases_final
		
ON (Intern_Cases_final.opportunity = O.Opportunity)


-- --
-- -- calls:


LEFT JOIN

(
SELECT
Opps.opportunity
, ROUND ((CASE WHEN 1.0 * all_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_inbound_calls / opps END) ::numeric, 2)											AS all_inbound_calls
, ROUND ((CASE WHEN 1.0 * lost_inbound_calls / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * lost_inbound_calls / opps END) ::numeric, 2)											AS lost_inbound_calls
, ROUND ((CASE WHEN 1.0 * all_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * all_outbound / opps END) ::numeric, 2)												AS all_outbound
, ROUND ((CASE WHEN 1.0 * b2b_outbound / opps IS NULL  
					THEN 0.00 
					ELSE 1.0 * b2b_outbound / opps END) ::numeric, 2)												AS b2b_outbound
					

FROM
(
SELECT
locale__c				AS locale
, customer__c			AS Customer
, name					AS Opportunity_Name
, sfid					AS Opportunity
, status__c				AS Status
, COUNT (sfid) OVER (PARTITION BY customer__c) 						AS Opps

FROM
		Salesforce.opportunity Opp
		
WHERE
test__c = false
-- AND stagename IN ('WON', 'PENDING') 
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')

GROUP BY
locale__c
, customer__c
, name		
, sfid	
, status__c ) AS Opps


		LEFT JOIN 
		LATERAL
		(		
		SELECT

		RelatedContact__c 														AS Contact
		
	
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END)
			+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' 
						THEN 1 
						ELSE 0 END) 											AS all_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 
						THEN 1 
						ELSE 0 END) 											AS lost_inbound_calls
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' 
						THEN 1 
						ELSE 0 END)												AS all_outbound
		, SUM(CASE 	WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' 
						THEN 1 
						ELSE 0 END) 											AS b2b_outbound
		
		FROM 
		Salesforce.natterbox_call_reporting_object__c t4
		
			LEFT JOIN 
			salesforce.contact t5
			
			ON
			(t4.relatedcontact__c = t5.sfid)
		WHERE
		-- -- Calls created within the last 8 weeks incl. CURRENT_DATE
		call_start_date_date__c::date 	BETWEEN CURRENT_DATE - INTERVAL '8 Weeks' 
													AND CURRENT_DATE
		
		AND 	(	(calldirection__c = 'Inbound' 	AND E164Callednumber__C IN ('493030807263', '493030807264'))
				OR (calldirection__c = 'Outbound' 	AND Callerfirstname__c LIKE 'CM%'))
				
		AND RelatedContact__c IS NOT NULL
		
		GROUP BY
		Contact) as Call
		
		ON
		(Opps.Customer = Call.Contact)	
		
) AS Calls_final

ON (Calls_final.Opportunity = O.Opportunity)


) AS traffic_light_detail

) AS traffic_lights

) AS t_berlin

WHERE

delivery_area IN ('de-berlin')

