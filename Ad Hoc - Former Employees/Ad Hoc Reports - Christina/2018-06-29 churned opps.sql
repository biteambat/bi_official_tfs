-- DROP TABLE IF EXISTS bi.churn_details;
-- 	CREATE TABLE bi.churn_details as 


SELECT  
-- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the last order's date 
-- It's the last day on which they are making money

		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		o.delivery_area__c,
		o.opportunityid,
		oo.name,
		oo.stagename,
		oo.status__c,
		MAX(CASE WHEN ooo.grand_total__c IS NULL THEN ooo.total_amount ELSE ooo.grand_total__c END) as grand_total,
		oo.contract_duration__c,
		oo.hours_weekly__c,
	 	oo.hours_weekly__c*4.33 AS avg_monthly_hours,	
		oo.churn_reason__c,
		oo.nextstep,
		MAX(o.effectivedate) AS date_churn,
		MIN(o.effectivedate) AS first_order,
		MAX(o.effectivedate) - MIN(o.effectivedate) AS days,
		(MAX(o.effectivedate) - MIN(o.effectivedate))/30 AS months,
			CASE WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) < 31 THEN 'M0'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 31 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 61 THEN 'M1'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 61 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 92 THEN 'M2'
			  ELSE '>M3'
			  END as sub_kpi_2
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.b2borders ooo
		
	ON
	
		o.opportunityid = ooo.opportunity_id
				
	WHERE
	
		o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE

	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		o.delivery_area__c,
		-- type_date,
		o.opportunityid,
		oo.name,
		oo.stagename,
		oo.grand_total__c,
		oo.contract_duration__c,
		oo.hours_weekly__c,
		oo.status__c,
		oo.churn_reason__c,
		oo.nextstep