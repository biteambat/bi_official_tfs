
SELECT
N_SFID 																AS SFID
, N_Opportunity_Name 											AS Opportunity_Name
, N_Cleaning_Service_Value 									AS Cleaning_Service_Value

, orders 															AS Orders_done
-- , orders_planned													AS Orders_recurrent

, N_weekly_hours 													AS weekly_hours
, N_AVG_monthly_hours 											AS AVG_monthly_hours
, executed_hours													AS executed_hours
, executed_hours / N_AVG_monthly_hours						AS executed_h_rate

, Plan_normal_time												AS Plan_normal_time

, Orders_Cancelled 												AS BAT_Cancelled_NOT_Rescheduled
, Orders_cancelled_holidays									AS BAT_Cancelled_NOT_Rescheduled_Holidays
, Orders_rescheduled__Cancelled_BAT 						AS BAT_Cancelled_Rescheduled

, Orders_Cancelled_Customer									AS Customer_Cancelled_NOT_Rescheduled
, Customer_Cancelled_Recurrent_Normal_Time				AS Customer_Cancelled_Rescheduled_Normal_Time
, Customer_Cancelled_Recurrent_Evening						AS Customer_Cancelled_Rescheduled_Evening
, Customer_Cancelled_Recurrent_Sunday						AS Customer_Cancelled_Rescheduled_Sunday
, Customer_Cancelled_Recurrent_Holidays_NOT_Sunday 	AS Customer_Cancelled_Recurrent_Holidays_NOT_Sunday 
, Customer_Cancelled_Recurrent_Special_Holidays			AS Customer_Cancelled_Recurrent_Special_Holidays

, Extra_Normal_Time 							AS Extra_Normal_Time
, Extra_Evening								AS Extra_Evening
, Extra_Sunday									AS Extra_Sunday
, Extra_Holidays_NOT_Sunday 				AS Extra_Holidays_NOT_Sunday
, Extra_Special_Holidays 					AS Extra_Special_Holidays

, FirstMonth									AS FirstMonth
, first_order_date							AS First_Order_date
, Review_public_holidays					AS Review_public_holidays
, StageName										AS stage
, status__c										AS status
, churn_reason__c								AS churn_reason

FROM
	(
	SELECT 
	o.sfid 										AS N_SFID
	, o.customer__c
	, o.Name 									AS N_Opportunity_Name 
	, o.StageName								
	, o.status__c
	, o.churn_reason__c
	-- 	, o.supplies__c
	, o.grand_total__c	 					AS N_Cleaning_Service_Value 
	, o.CloseDate
	, ooo.first_order_date 					AS first_order_date
		
	, (CASE	WHEN (EXTRACT (month FROM ooo.first_order_date::date) = 4) 
				AND (EXTRACT (year FROM ooo.first_order_date::date) = 2018) 
			THEN 1 
			ELSE 0 
			END) 									AS FirstMonth
				
	, oo.first_order_date_monthly

-- Orders

	, oo.orders 								AS orders
	
	, (CASE WHEN o9.Orders_rescheduled > 0 
			THEN o9.Orders_rescheduled 
			ELSE 0 
			END) 									AS Orders_rescheduled
		
	, (CASE WHEN o9.rescheduled_Orders_Cancelled_BAT > 0 
			THEN o9.rescheduled_Orders_Cancelled_BAT 
			ELSE 0 
			END) 									AS Orders_rescheduled__Cancelled_BAT
		
	, (CASE WHEN o9.rescheduled_Orders_Cancelled_Customer > 0 
			THEN o9.rescheduled_Orders_Cancelled_Customer 
			ELSE 0 	
			END) 									AS Orders_rescheduled__Cancelled_Customer
	
--  Cancelled Orders	

	, (CASE WHEN oooo.orders_cancelled > 0 
			THEN oooo.orders_cancelled 
			ELSE 0 
			END) 									AS Orders_Cancelled
	
	, (CASE WHEN oooo.Orders_cancelled_holidays > 0 
		THEN oooo.Orders_cancelled_holidays 
		ELSE 0 
		END) 									AS Orders_cancelled_holidays		
	
	, (CASE WHEN ooooo.Orders_cancelled_customer > 0 
			THEN ooooo.Orders_cancelled_customer 
			ELSE 0 
			END) 									AS Orders_Cancelled_Customer
	
	, (CASE WHEN oooo.Orders_Cancelled_Professional > 0 
			THEN oooo.Orders_Cancelled_Professional 
			ELSE 0 
			END ) 								AS Orders_Cancelled_Professional
	
	, (CASE WHEN oooo.Orders_Cancelled_Mistake > 0 
			THEN oooo.Orders_Cancelled_Mistake 
			ELSE 0 
			END ) 								AS Orders_Cancelled_Mistake
	
	, (CASE WHEN oooo.Orders_Cancelled_Terminated > 0 
			THEN oooo.Orders_Cancelled_Terminated 
			ELSE 0 
			END ) 								AS Orders_Cancelled_Terminated	
	
--  hours
	
	, o.hours_weekly__c 						AS N_weekly_hours
	, o.hours_weekly__c*4.33 				AS N_AVG_monthly_hours
	, oo.executed_hours 						AS executed_hours
	, o.hours_weekly__c*5 					AS max_monthly_hours
	, ooooo.cancelled_customer_hours 	AS cancelled_customer_hours
	, ooooooo.holiday_mistake_hours 		AS holiday_mistake_hours
	, (o.grand_total__c/o.hours_weekly__c/4.33) AS PPH


	, (CASE WHEN oo.Recurrent_orders > 0 
			THEN oo.Recurrent_orders 
			ELSE 0 
			END) 									AS orders_planned
	
	, (CASE WHEN oo.Extra_Sunday > 0 
			THEN oo.Extra_Sunday 
			ELSE 0 
			END) 									AS Extra_Sunday
		
	, (CASE WHEN oo.Extra_Normal_Time > 0 
			THEN oo.Extra_Normal_Time 
			ELSE 0 
			END) 									AS Extra_Normal_Time
		
	, (CASE WHEN oo.Extra_Evening > 0 
			THEN oo.Extra_Evening 
			ELSE 0 
			END) 									AS Extra_Evening
	
	, (CASE WHEN oo.Extra_Holidays_NOT_Sunday > 0 
			THEN oo.Extra_Holidays_NOT_Sunday 
			ELSE 0 
			END) 									AS Extra_Holidays_NOT_Sunday
	
	, (CASE WHEN oo.Extra_Special_Holidays > 0 
			THEN oo.Extra_Special_Holidays 
			ELSE 0 
			END) 									AS Extra_Special_Holidays
	
	
	, (CASE WHEN o9.Customer_Cancelled_Recurrent_Sunday > 0 
			THEN o9.Customer_Cancelled_Recurrent_Sunday 
			ELSE 0 
			END) 									AS Customer_Cancelled_Recurrent_Sunday
	
	, (CASE WHEN o9.Customer_Cancelled_Recurrent_Normal_Time > 0 
			THEN o9.Customer_Cancelled_Recurrent_Normal_Time 
			ELSE 0 
			END)									AS Customer_Cancelled_Recurrent_Normal_Time
	
	, (CASE WHEN o9.Customer_Cancelled_Recurrent_Evening > 0 
			THEN o9.Customer_Cancelled_Recurrent_Evening 
			ELSE 0 
			END) 									AS Customer_Cancelled_Recurrent_Evening
	
	, (CASE WHEN o9.Customer_Cancelled_Recurrent_Holidays_NOT_Sunday > 0 
			THEN o9.Customer_Cancelled_Recurrent_Holidays_NOT_Sunday 
			ELSE 0 
			END) 									AS Customer_Cancelled_Recurrent_Holidays_NOT_Sunday
	
	, (CASE WHEN o9.Customer_Cancelled_Recurrent_Special_Holidays > 0 
			THEN o9.Customer_Cancelled_Recurrent_Special_Holidays 
			ELSE 0 
			END) 									AS Customer_Cancelled_Recurrent_Special_Holidays
	
	, (CASE WHEN oo.Plan_normal_time > 0 
				AND (oo.Plan_normal_time + oo.Plan_evening + Plan_sunday) = 1 
			THEN 1 
			ELSE 0 
			END) 									AS Plan_normal_time
	
	, (CASE WHEN (CASE 	WHEN oooooo.orders_holiday > 0 
						THEN oooooo.orders_holiday 
						ELSE 0 END) > 0 
			THEN 1 
			ELSE 0 
			END) 									AS Review_public_holidays	
	 

	FROM 
	salesforce.opportunity o 
	
	INNER JOIN 
	
		(SELECT
		opportunityid 							AS opportunityid
		, COUNT(sfid) 							AS Orders
		, SUM(order_duration__c) 			AS executed_hours
		, MIN(effectivedate::date) 		AS first_order_date_monthly
		
		, (CASE WHEN SUM(Recurrent_Normal_Time) > 0 
				THEN 1 
				ELSE 0 
				END) 								AS Plan_normal_time
		
		, (CASE WHEN SUM(Recurrent_Evening) > 0 
				THEN 1 
				ELSE 0 
				END) 								AS Plan_evening
		
		, (CASE WHEN SUM(Recurrent_Sunday) > 0 
				THEN 1 
				ELSE 0 
				END) 								AS Plan_sunday				
		
		-- RECURRENT BOOKING

		, SUM(Recurrent_orders) 				AS Recurrent_orders		
		, SUM(Recurrent_Sunday) 				AS Recurrent_Sunday
		, SUM(Recurrent_Normal_Time) 			AS Recurrent_Normal_Time
		, SUM(Recurrent_Evening) 				AS Recurrent_Evening
		, SUM(Recurrent_Holidays_NOT_Sunday)AS Recurrent_Holidays_NOT_Sunday
		, SUM(Recurrent_Special_Holidays) 	AS Recurrent_Special_Holidays
		
		-- EXTRA BOOKINGS
		
		, SUM(Extra_Sunday) 						AS Extra_Sunday
		, SUM(Extra_Normal_Time) 				AS Extra_Normal_Time
		, SUM(Extra_Evening) 					AS Extra_Evening
		, SUM(Extra_Holidays_NOT_Sunday) 	AS Extra_Holidays_NOT_Sunday
		, SUM(Extra_Special_Holidays) 		AS Extra_Special_Holidays
		
		FROM
		
			(SELECT
				
			*
			-- EXTRA BOOKINGS
				
			, (CASE WHEN extra_booking 	= 1 
						AND Holiday 	= 0 
						AND sunday 		= 1 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Extra_Sunday
			
			, (CASE WHEN extra_booking 	= 1 
						AND Holiday 	= 0 
						AND sunday 		= 0 
						AND normal_time = 1 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Extra_Normal_Time
					
			, (CASE WHEN extra_booking 	= 1 
						AND Holiday 	= 0 
						AND sunday 		= 0 
						AND normal_time = 0 
					THEN order_duration__c
					ELSE 0 
					END) 					AS Extra_Evening
					
			, (CASE WHEN extra_booking 	= 1 
						AND Holiday 	= 1 
						AND sunday 		= 0 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Extra_Holidays_NOT_Sunday
					
			, (CASE WHEN extra_booking 	= 1 
						AND Holiday 	= 1 
						AND sunday 		= 1 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Extra_Special_Holidays
				
			
			-- RECURRENT BOOKING

			, (CASE WHEN extra_booking 	= 0 
					THEN order_duration__c 
					ELSE 1 
					END) 					AS Recurrent_orders
									
			, (CASE WHEN extra_booking 	= 0 
						AND Holiday 	= 0 
						AND sunday 		= 1 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Recurrent_Sunday
						
			, (CASE WHEN extra_booking 	= 0 
						AND Holiday 	= 0 
						AND sunday 		= 0 
						AND normal_time = 1 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Recurrent_Normal_Time
						
			, (CASE WHEN extra_booking 	= 0 
						AND Holiday 	= 0 
						AND sunday 		= 0 
						AND normal_time = 0 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Recurrent_Evening
						
			, (CASE WHEN extra_booking 	= 0 
						AND Holiday 	= 1 
						AND sunday 		= 0 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Recurrent_Holidays_NOT_Sunday
						
			, (CASE WHEN extra_booking 	= 0 
						AND Holiday 	= 1 
						AND sunday 		= 1 
					THEN order_duration__c 
					ELSE 0 
					END) 					AS Recurrent_Special_Holidays
				
			FROM
				(
				SELECT
				opportunityid
				, sfid
				, effectivedate::date
				, (CASE WHEN recurrency__c = 0 
						THEN 1 
						ELSE 0 
						END) 				AS extra_booking
						
				, LEFT(order_time__c, 2) 	AS order_hour
				
				, (CASE WHEN LEFT(order_time__c, 2) >= '05' 
							AND LEFT(order_time__c, 2) < '22' 
						THEN 1 
						ELSE 0 
						END) 				AS normal_time 
						
				, order_duration__c	
				, acquisition_channel__c
				, (CASE WHEN extract(dow from effectivedate) = 0 
						THEN 1 
						ELSE 0 
						END) 				AS sunday
						
				, (CASE WHEN effectivedate::date = '2018-04-02' 
						THEN 1 
						ELSE 0 
						END) 				AS Holiday
							
				FROM
				salesforce."order" t1
				
				WHERE
				
				status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
				AND EXTRACT (month FROM effectivedate::date) = 4
				AND EXTRACT (year FROM effectivedate::date) = 2018

									
				) AS orders2
		
			) as orders
				
				GROUP BY
				opportunityid
				
				
		) AS oo	
		
		ON o.sfid = oo.opportunityid

	INNER JOIN bi.b2borders ooo
	
		ON o.sfid = ooo.opportunity_id
		

--  ORDERS RESCHEDULED

	LEFT JOIN

		(SELECT
		o8.opportunityid					AS opportunityid
		, SUM(order_duration)			AS Orders_rescheduled
		
		, SUM (CASE WHEN o8.history_status_cancelled LIKE 'CANCELLED PROFESSIONAL' 
-- 						OR o8.history_status_cancelled LIKE 'CANCELLED MISTAKE'
					THEN order_duration 
					ELSE 0 
					END) 					AS rescheduled_Orders_Cancelled_BAT
					
		, SUM (CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' 
					THEN order_duration 
					ELSE 0 
					END) 					AS rescheduled_Orders_Cancelled_Customer
					
	-- CUSTOMER CANCELLED RECURRENT BOOKING
				
		, SUM (CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' 
						AND o8.extra_booking 	= 0 
						AND o8.Holiday 			= 0 
						AND o8.sunday 			= 1 
					THEN order_duration 
					ELSE 0 
					END) 					AS Customer_Cancelled_Recurrent_Sunday
		
		, SUM (CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' 
						AND o8.extra_booking 	= 0 
						AND o8.Holiday 			= 0 
						AND o8.sunday 			= 0 
						AND o8.normal_time 		= 1 
					THEN order_duration 
					ELSE 0 
					END) 					AS Customer_Cancelled_Recurrent_Normal_Time
		
		, SUM (CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' 
						AND o8.extra_booking 	= 0 
						AND o8.Holiday 			= 0 
						AND o8.sunday 			= 0 
						AND o8.normal_time 		= 0 
					THEN order_duration 
					ELSE 0 
					END) 					AS Customer_Cancelled_Recurrent_Evening
		
		, SUM (CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' 
						AND o8.extra_booking 	= 0 
						AND o8.Holiday 			= 1 
						AND o8.sunday 	= 0 
					THEN order_duration 
					ELSE 0 
					END) 			AS Customer_Cancelled_Recurrent_Holidays_NOT_Sunday
		
		, SUM (CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' 
						AND o8.extra_booking 	= 0 
						AND o8.Holiday 			= 1 
						AND o8.sunday 			= 1 
					THEN order_duration 
					ELSE 0 
					END)			AS Customer_Cancelled_Recurrent_Special_Holidays
		
		FROM
			(SELECT
			Orders.effectivedate
			, Orders.sfid
			, Orders.opportunityid 												AS opportunity
			, Orders."status"
			, Orders.order_start__c
			, Orders.order_end__c
			, Orders.order_time__c
			, Orders.order_duration__c											AS order_duration
			, HISTORY.*
			, HISTORY.status_cancelled 										AS history_status_cancelled
			, HISTORY.status_rescheduled 										AS history_status_rescheduled
			
			, HISTORY.order_start_rescheduled::timestamp 
				- HISTORY.order_start_cancelled::timestamp 				AS slot_cancelled_rescheduled
			
			, Orders.order_start__c::timestamp 
				- HISTORY.order_start_rescheduled::timestamp 			AS slot_final
				
			, to_number(HISTORY.order_duration_rescheduled, '999') 
				- to_number(HISTORY.order_duration_cancelled, '999')	AS duration_dif
				
			, (CASE WHEN Orders.recurrency__c = 0 
					THEN 1 
					ELSE 0 
					END) 																AS extra_booking
			
			, LEFT(Orders.order_time__c, 2) 									AS order_hour
			
			, (CASE WHEN LEFT(Orders.order_time__c, 2) >= '05' 
						AND LEFT(order_time__c, 2) < '22' 
					THEN 1 
					ELSE 0 
					END) 																AS normal_time 
				
			, Orders.order_duration__c
			, Orders.acquisition_channel__c
			, (CASE WHEN extract(dow from Orders.effectivedate) = 0 
					THEN 1 
					ELSE 0 
					END) 																AS sunday
					
			, (CASE WHEN Orders.effectivedate::date = '2018-04-02' 
					THEN 1 
					ELSE 0 
					END) 																AS Holiday
				
			FROM
				
			salesforce."order" AS Orders
						
			LEFT JOIN

-- ------------- Order Status History

				(
				SELECT
				
				-- cancelled	
									
				t1.date 						AS date_cancelled
				, t1.sf_orderid 			AS orderid
				, t1.opportunityid
				, t1.status 				AS status_cancelled
				, t1.order_start 			AS order_start_cancelled
				, t1.order_time__c 		AS order_time_cancelled
				, t1.order_duration 		AS order_duration_cancelled
										
				-- rescheduled
										
				, t2.date 					AS date_rescheduled
				, t2.status 				AS status_rescheduled
				, t2.order_start 			AS order_start_rescheduled
				, t2.order_time__c 		AS order_time_rescheduled
				, t2.order_duration 		AS order_duration_rescheduled
				
				
				FROM
										
				bi.b2b_orderstatushistory_Cancelled t1
										
				LEFT JOIN
										
				-- LATERAL
										
					bi.b2b_orderstatushistory_Rescheduled t2
										
					ON ( t1.sf_orderid = t2.sf_orderid AND t1.date < t2.date)
									
				) AS HISTORY
									
				ON (Orders.sfid = HISTORY.orderid)
					
				WHERE
				
					Orders.status IN (	'PENDING TO START',
												'FULFILLED', 
												'NOSHOW CUSTOMER', 
												'INVOICED')
										
					AND EXTRACT (month FROM Orders.effectivedate::date) = 4
					AND EXTRACT (year FROM Orders.effectivedate::date) = 2018
					
					AND HISTORY.status_rescheduled IS NOT NULL
					AND (Orders.order_start__c::timestamp 
							- HISTORY.order_start_rescheduled::timestamp) ='00:00:00'
					
				) AS o8
			
		GROUP BY 
		o8.opportunityid		
	
		)	AS o9 	
		
		ON o.sfid = o9.opportunityid
		
--  CANCELLED ORDERS without CANCELLED CUSTOMER	

	LEFT JOIN
		
		(SELECT 
			opportunityid
			, SUM(order_duration__c) 	AS Orders_cancelled
			
			, SUM (CASE WHEN effectivedate::date = '2018-04-02' THEN order_duration__c ELSE 0 END) AS Orders_cancelled_holidays
			
			, SUM (CASE WHEN Status LIKE 'CANCELLED PROFESSIONAL' 
						THEN order_duration__c 
						ELSE 0 
						END) 					AS Orders_Cancelled_Professional
			
			, SUM (CASE WHEN Status LIKE 'CANCELLED MISTAKE' 
						THEN order_duration__c 
						ELSE 0 
						END) 					AS Orders_Cancelled_Mistake
			
			, SUM (CASE WHEN Status LIKE 'CANCELLED TERMINATED' 
						THEN order_duration__c 
						ELSE 0 	
						END) 					AS Orders_Cancelled_Terminated
		
		FROM
			salesforce."order" t2
		
		WHERE
			status LIKE '%CANCELLED%'
			AND status NOT LIKE 'CANCELLED CUSTOMER'
			AND status NOT LIKE 'CANCELLED MISTAKE'
			AND status NOT LIKE 'CANCELLED TERMINATED'
			AND EXTRACT (month FROM effectivedate::date) = 4
			AND EXTRACT (year FROM effectivedate::date) = 2018
		
		GROUP BY 
			opportunityid) AS oooo
		
		ON o.sfid = oooo.opportunityid

-- CANCELLED CUSTOMER ORDERS
		
	LEFT JOIN
	
		(SELECT 
			opportunityid
			, SUM(order_duration__c) 		AS Orders_cancelled_customer
			, SUM(order_duration__c) 		AS cancelled_customer_hours
			
		FROM
			salesforce."order" t3
			
		WHERE
			status LIKE 'CANCELLED CUSTOMER'
			AND EXTRACT (month FROM effectivedate::date) = 4
			AND EXTRACT (year FROM effectivedate::date) = 2018
		
		GROUP BY 
			opportunityid) AS ooooo
		
		ON o.sfid = ooooo.opportunityid	


-- PUBLIC HOLIDAYS

-- PUBLIC HOLIDAYS	w/o CANCELLED MISTAKE	

		LEFT JOIN
		--
		(SELECT
		opportunityid
		, COUNT(sfid) 							AS orders_holiday
		, SUM(order_duration__c) 			AS holiday_hours
		, MIN(effectivedate::date) 		AS first_order_date_monthly
		--
		FROM
		salesforce."order" t4
		--
		WHERE
		(status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
		OR (status LIKE '%CANCELLED%' AND status NOT LIKE 'CANCELLED MISTAKE'))
		--
		-- Holidays
		--
		AND effectivedate::date = '2018-04-02'
		--
		GROUP BY
		opportunityid) AS oooooo
		--
			ON o.sfid = oooooo.opportunityid			


--  PUBLIC HOLIDAY is CANCELLED MISTAKE	

	LEFT JOIN			
	
		(SELECT
			opportunityid
			, COUNT(sfid) 					AS orders_holiday_mistake
			, SUM(order_duration__c) 		AS holiday_mistake_hours
			, MIN(effectivedate::date) 		AS first_order_date_monthly
			
		FROM
			salesforce."order" t5
			
		WHERE
			status IN ('CANCELLED MISTAKE')

		-- Holidays

			AND effectivedate::date = '2018-04-02'
			
		GROUP BY
			opportunityid) AS ooooooo
				
		ON o.sfid = ooooooo.opportunityid	

	
WHERE
	o.grand_total__c IS NOT NULL
	-- AND o.stagename IN ('WON')

GROUP BY
o.sfid
, o.customer__c
, o.Name
, o.StageName
, o.supplies__c
, o.grand_total__c 
, o.CloseDate
, o.hours_weekly__c
, o.status__c
, o.churn_reason__c 
, oo.first_order_date_monthly
, oo.orders
, oo.executed_hours
, oo.Extra_Sunday
, oo.Extra_Normal_Time 									
, oo.Extra_Evening								
, oo.Extra_Holidays_NOT_Sunday 							
, oo.Extra_Special_Holidays
, oo.Plan_normal_time 
, oo.Plan_evening 
, oo.Plan_sunday
, oo.Recurrent_orders
, ooo.first_order_date
, oooo.orders_cancelled
, oooo.Orders_cancelled_holidays
, oooo.Orders_Cancelled_Professional
, oooo.Orders_Cancelled_Mistake
, oooo.Orders_Cancelled_Terminated
, ooooo.Orders_cancelled_customer
, ooooo.cancelled_customer_hours
, oooooo.orders_holiday
, ooooooo.orders_holiday_mistake
, ooooooo.holiday_mistake_hours
, o9.Orders_rescheduled
, o9.rescheduled_Orders_Cancelled_BAT
, o9.rescheduled_Orders_Cancelled_Customer
, o9.Customer_Cancelled_Recurrent_Sunday 
, o9.Customer_Cancelled_Recurrent_Normal_Time 
, o9.Customer_Cancelled_Recurrent_Evening 
, o9.Customer_Cancelled_Recurrent_Holidays_NOT_Sunday
, o9.Customer_Cancelled_Recurrent_Special_Holidays
) AS old
