SELECT
o8.opportunityid																											AS opportunityid
, COUNT(sfid) 																												AS Orders_rescheduled
, SUM(CASE WHEN o8.history_status_cancelled LIKE 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) 	AS rescheduled_Orders_Cancelled_Professional
, SUM(CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' THEN 1 ELSE 0 END) 		AS rescheduled_Orders_Cancelled_Customer

FROM
		(SELECT
		Orders.effectivedate
		, Orders.sfid
		, Orders.opportunityid AS opportunity
		, Orders."status"
		, Orders.order_start__c
		, Orders.order_end__c
		, Orders.order_time__c
		, Orders.order_duration__c
		, HISTORY.*
		, HISTORY.status_cancelled 	AS history_status_cancelled
		, HISTORY.status_rescheduled 	AS history_status_rescheduled
		, HISTORY.order_start_rescheduled::timestamp - HISTORY.order_start_cancelled::timestamp AS slot_cencelled_rescheduled
		, Orders.order_start__c::timestamp - HISTORY.order_start_rescheduled::timestamp AS slot_final
		, to_number(HISTORY.order_duration_rescheduled, '999') - to_number(HISTORY.order_duration_cancelled, '999') AS duration_dif
		
		FROM
		
			salesforce."order" AS Orders
				
					LEFT JOIN
						-- ------------- Order Status History
						(
							SELECT
							
								-- cancelled						
								 t1.date AS date_cancelled
								, t1.sf_orderid AS orderid
								, t1.opportunityid
								, t1.status AS status_cancelled
								, t1.order_start AS order_start_cancelled
								, t1.order_time__c AS order_time_cancelled
								, t1.order_duration AS order_duration_cancelled
								
								-- rescheduled
								, t2.date AS date_rescheduled
								, t2.status AS status_rescheduled
								, t2.order_start AS order_start_rescheduled
								, t2.order_time__c AS order_time_rescheduled
								, t2.order_duration AS order_duration_rescheduled
		
		
							FROM
								
								bi.b2b_orderstatushistory_Cancelled t1
								
							LEFT JOIN
								-- LATERAL
								
								bi.b2b_orderstatushistory_Rescheduled t2
								
								ON ( t1.sf_orderid = t2.sf_orderid AND t1.date < t2.date)
							
							) AS HISTORY
							
						ON (Orders.sfid = HISTORY.orderid)
			
		WHERE
		
			Orders.status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
			AND EXTRACT (month FROM Orders.effectivedate::date) = EXTRACT (month FROM current_date::date)
			AND EXTRACT (year FROM Orders.effectivedate::date) = EXTRACT (year FROM current_date::date)
			
			AND HISTORY.status_rescheduled IS NOT NULL
			AND (Orders.order_start__c::timestamp - HISTORY.order_start_rescheduled::timestamp) ='00:00:00'
			
			) AS o8
	
GROUP BY 
o8.opportunityid

	