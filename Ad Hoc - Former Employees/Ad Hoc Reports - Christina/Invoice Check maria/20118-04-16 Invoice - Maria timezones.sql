SELECT
opportunityid 									AS OppId
, COUNT(sfid) 									AS Orders
, SUM(order_duration__c) 					AS executed_hours
, MIN(effectivedate::date) 				AS first_order_date_monthly

-- RECURRENT BOOKING

, SUM(Recurrent_Sunday) 					AS Recurrent_Sunday
, SUM(Recurrent_Normal_Time) 				AS Recurrent_Normal_Time
, SUM(Recurrent_Evening) 					AS Recurrent_Evening
, SUM(Recurrent_Holidays_NOT_Sunday) 	AS Recurrent_Holidays_NOT_Sunday
, SUM(Recurrent_Special_Holidays) 		AS Recurrent_Special_Holidays

-- EXTRA BOOKINGS

, SUM(Extra_Sunday) 							AS Extra_Sunday
, SUM(Extra_Normal_Time) 					AS Extra_Normal_Time
, SUM(Extra_Evening) 						AS Extra_Evening
, SUM(Extra_Holidays_NOT_Sunday) 		AS Extra_Holidays_NOT_Sunday
, SUM(Extra_Special_Holidays) 			AS Extra_Special_Holidays

FROM

(SELECT

-- EXTRA BOOKINGS

		(CASE WHEN extra_booking = 1 AND Holiday = 0 AND sunday = 1 THEN 1 ELSE 0 END) AS Extra_Sunday
		, (CASE WHEN extra_booking = 1 AND Holiday = 0 AND sunday = 0 AND normal_time = 1 THEN 1 ELSE 0 END) AS Extra_Normal_Time
		, (CASE WHEN extra_booking = 1 AND Holiday = 0 AND sunday = 0 AND normal_time = 0 THEN 1 ELSE 0 END) AS Extra_Evening
		, (CASE WHEN extra_booking = 1 AND Holiday = 1 AND sunday = 0 THEN 1 ELSE 0 END) AS Extra_Holidays_NOT_Sunday
		, (CASE WHEN extra_booking = 1 AND Holiday = 1 AND sunday = 1 THEN 1 ELSE 0 END) AS Extra_Special_Holidays

-- RECURRENT BOOKING

		, (CASE WHEN extra_booking = 0 AND Holiday = 0 AND sunday = 1 THEN 1 ELSE 0 END) AS Recurrent_Sunday
		, (CASE WHEN extra_booking = 0 AND Holiday = 0 AND sunday = 0 AND normal_time = 1 THEN 1 ELSE 0 END) AS Recurrent_Normal_Time
		, (CASE WHEN extra_booking = 0 AND Holiday = 0 AND sunday = 0 AND normal_time = 0 THEN 1 ELSE 0 END) AS Recurrent_Evening
		, (CASE WHEN extra_booking = 0 AND Holiday = 1 AND sunday = 0 THEN 1 ELSE 0 END) AS Recurrent_Holidays_NOT_Sunday
		, (CASE WHEN extra_booking = 0 AND Holiday = 1 AND sunday = 1 THEN 1 ELSE 0 END) AS Recurrent_Special_Holidays

, *

FROM
			(
			SELECT
			opportunityid
			, sfid
			, effectivedate::date
			, (CASE WHEN recurrency__c = 0 THEN 1 ELSE 0 END) AS extra_booking
			, LEFT(order_time__c, 2) AS order_hour
			, (CASE WHEN LEFT(order_time__c, 2) >= '05' AND LEFT(order_time__c, 2) < '22' THEN 1 ELSE 0 END) AS normal_time 
			, order_duration__c
			, acquisition_channel__c
			, (CASE WHEN extract(dow from effectivedate) = 0 THEN 1 ELSE 0 END) AS sunday
			, (CASE WHEN effectivedate::date = '2018-04-02' THEN 1 ELSE 0 END) AS Holiday
			
					FROM
						salesforce."order" t1
					WHERE
						status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
						AND EXTRACT (month FROM effectivedate::date) = 4
						AND EXTRACT (year FROM effectivedate::date) = 2018
					) AS orders2
					) as orders

GROUP BY
opportunityid
