DROP TABLE IF EXISTS bi.b2b_orderstatushistory_cancelled;

CREATE TABLE bi.b2b_orderstatushistory_cancelled AS

SELECT
		
				created_at::timestamp 					AS date
				, event_name
				, order_Json->>'Id' 						AS SF_OrderId
				, order_Json->>'OpportunityId' 		AS OpportunityId
				, order_Json->>'AccountId'				AS ProfessionalId
				, order_Json->>'Status'					AS Status
--				, order_Json->>'In_Test_Period__c'	AS In_Test_Period
-- 			, order_Json->>'Auto_Matched__c'		AS Auto_Matched
				, order_Json->>'Recurrency__c'		AS Recurrency
												
				, order_Json->>'EffectiveDate'		AS EffectiveDate
				, order_Json->>'Order_Start__c'		AS Order_Start
				, order_Json->>'Order_Time__c'		AS Order_Time__c
				, order_Json->>'Order_End__c'			AS Order_End
				, order_Json->>'Order_Duration__c'	AS Order_Duration
				
				, order_Json->>'Discount__c'			AS Discount
				, order_Json->>'Web_Grand_Total__c'	AS Web_Grand_Total
				, order_Json->>'PPH__c'					AS PPH
				, order_Json->>'Grand_Total_EUR__c'	AS Grand_Total_EUR
				, order_Json->>'GMV__c'					AS GMV
				
		
		FROM
			
				events.sodium
		
		WHERE
		
				event_name LIKE 'Order%'
				AND (
				event_name LIKE  '%CANCELLED-PROFESSIONAL%'
					OR event_name LIKE  '%CANCELLED-MISTAKE%'
					OR event_name LIKE  '%CANCELLED CUSTOMER%'
					)
-- 				AND EXTRACT (MONTH FROM created_at::date) = 4
				AND EXTRACT (YEAR FROM created_at::date) = 2018
				
-- 				AND created_at::date = CURRENT_DATE
				AND (order_Json->>'OpportunityId') IS NOT NULL	
						
;

DROP TABLE IF EXISTS bi.b2b_orderstatushistory_Rescheduled;

CREATE TABLE bi.b2b_orderstatushistory_Rescheduled AS


SELECT
		
				created_at::timestamp 					AS date
				, order_Json->>'Id' 						AS SF_OrderId
				, order_Json->>'OpportunityId' 		AS OpportunityId
				, order_Json->>'AccountId'				AS ProfessionalId
				, order_Json->>'Status'					AS Status
--				, order_Json->>'In_Test_Period__c'	AS In_Test_Period
-- 			, order_Json->>'Auto_Matched__c'		AS Auto_Matched
				, order_Json->>'Recurrency__c'		AS Recurrencyb2b_orderstatushistory_cancelled
												
				, order_Json->>'EffectiveDate'		AS EffectiveDate
				, order_Json->>'Order_Start__c'		AS Order_Start
				, order_Json->>'Order_Time__c'		AS Order_Time__c
				, order_Json->>'Order_End__c'			AS Order_End
				, order_Json->>'Order_Duration__c'	AS Order_Duration
				
				, order_Json->>'Discount__c'			AS Discount
				, order_Json->>'Web_Grand_Total__c'	AS Web_Grand_Total
				, order_Json->>'PPH__c'					AS PPH
				, order_Json->>'Grand_Total_EUR__c'	AS Grand_Total_EUR
				, order_Json->>'GMV__c'					AS GMV

		
		FROM
		
				events.sodium
		
		WHERE
		
				event_name LIKE 'Order%'
				AND 
				(event_name LIKE '%RESCHEDULED%'
						OR event_name LIKE '%PENDING%')
-- 				AND EXTRACT (MONTH FROM created_at::date) = 4
				AND EXTRACT (YEAR FROM created_at::date) = 2018
				AND (order_Json->>'OpportunityId') IS NOT NULL
				
;