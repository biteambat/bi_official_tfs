--
-- check the 'public holidays' section monthly (LINE 125)
--
--
SELECT
N_SFID AS N_SFID
, N_Opportunity_Name AS N_Opportunity_Name
, N_Cleaning_Service_Value AS N_Cleaning_Service_Value
, N_weekly_hours AS N_weekly_hours
, N_AVG_monthly_hours AS N_AVG_monthly_hours
, orders AS Orders
, Orders_Cancelled AS N_BAT_Cancelled_NOT_Rescheduled
, Orders_rescheduled__Cancelled_Professional AS N_BAT_Cancelled_Rescheduled

FROM
(
SELECT 
	o.sfid AS N_SFID,
	o.customer__c, 
	o.Name AS N_Opportunity_Name, 
	o.StageName, 
-- 	o.supplies__c,
	o.grand_total__c AS N_Cleaning_Service_Value, 
	o.CloseDate, 
	ooo.first_order_date,
	(CASE WHEN EXTRACT (month FROM current_date::date) = (EXTRACT (month FROM ooo.first_order_date::date)) 
					AND EXTRACT (year FROM current_date::date) = (EXTRACT (year FROM ooo.first_order_date::date)) THEN 1 ELSE 0 END) AS FirstMonth,	
	oo.first_order_date_monthly,

-- ---------------------------------------------------------------------------------------------------------------------------------------------- orders
	oo.orders AS orders,
	
	(CASE WHEN o9.Orders_rescheduled > 0 THEN o9.Orders_rescheduled ELSE 0 END) AS Orders_rescheduled,
	(CASE WHEN o9.rescheduled_Orders_Cancelled_Professional > 0 THEN o9.rescheduled_Orders_Cancelled_Professional ELSE 0 END) AS Orders_rescheduled__Cancelled_Professional,
	(CASE WHEN o9.rescheduled_Orders_Cancelled_Customer > 0 THEN o9.rescheduled_Orders_Cancelled_Customer ELSE 0 END) AS Orders_rescheduled__Cancelled_Customer,
	
-- ---------------------------------------------------------------------------------------------------------------------------------------------- Cancelled Orders	
	(CASE WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled ELSE 0 END) AS Orders_Cancelled,
	(CASE WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer ELSE 0 END) AS Orders_Cancelled_Customer,
	(CASE WHEN oooo.Orders_Cancelled_Professional > 0 THEN oooo.Orders_Cancelled_Professional ELSE 0 END ) AS Orders_Cancelled_Professional,
	(CASE WHEN oooo.Orders_Cancelled_Mistake > 0 THEN oooo.Orders_Cancelled_Mistake ELSE 0 END ) AS Orders_Cancelled_Mistake,
	(CASE WHEN oooo.Orders_Cancelled_Terminated > 0 THEN oooo.Orders_Cancelled_Terminated ELSE 0 END ) AS Orders_Cancelled_Terminated,	

-- ---------------------------------------------------------------------------------------------------------------------------------------------- hours
	o.hours_weekly__c AS N_weekly_hours,
	o.hours_weekly__c*4.33 AS N_AVG_monthly_hours,
	oo.executed_hours AS executed_hours,
	o.hours_weekly__c*5 AS max_monthly_hours,
	ooooo.cancelled_customer_hours AS cancelled_customer_hours,
	ooooooo.holiday_mistake_hours AS holiday_mistake_hours,
	(o.grand_total__c/o.hours_weekly__c/4.33) AS PPH

-- ,
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE A - executed hours = max hours
-- (CASE WHEN oo.executed_hours = (o.hours_weekly__c*5) THEN 1 ELSE 0 END) AS fine_A,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE B - executed hours + CANCELLED CUSTOMER = max hours
-- (CASE WHEN (oo.executed_hours + ooooo.cancelled_customer_hours) = (o.hours_weekly__c*5) THEN 1 ELSE 0 END) AS fine_B,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE C - first month
-- (CASE WHEN EXTRACT (month FROM current_date::date) = (EXTRACT (month FROM ooo.first_order_date::date)) THEN 1 ELSE 0 END) AS fine_C,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE D - executed hours < max hours AND no cancelled Orders
-- (CASE WHEN ((oo.executed_hours < (o.hours_weekly__c*5))
-- AND ((CASE WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled ELSE 0 END) = 0)) THEN 1 ELSE 0 END) AS fine_D,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE E - executed hours < max hours AND CANCELLED CUSTOMER
-- (CASE WHEN ((oo.executed_hours < (o.hours_weekly__c*5))
-- AND ((CASE WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS fine_E,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE G - executed hours < max hours AND Public Holiday = CANCELLED MISTAKE
-- (CASE WHEN ((oo.executed_hours < (o.hours_weekly__c*5))
-- AND ((CASE WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS fine_G,
-- 
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW first month
-- (CASE WHEN EXTRACT (month FROM current_date::date) = (EXTRACT (month FROM ooo.first_order_date::date)) THEN 1 ELSE 0 END) AS Review_firstmonth,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW h
-- (CASE WHEN o.hours_weekly__c > 0 THEN 0 ELSE 1 END) AS Review_h,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW executed hours > max hours
-- (CASE WHEN oo.executed_hours > (o.hours_weekly__c*5) THEN 1 ELSE 0 END) AS Review_extrabooking,
-- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW public holidays
-- 	(CASE WHEN (CASE WHEN oooooo.orders_holiday > 0 THEN oooooo.orders_holiday ELSE 0 END) > 0 THEN 1 ELSE 0 END) AS Review_public_holidays	
	
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------------------------------- 
	 
FROM 
	salesforce.opportunity o 
	
	INNER JOIN 
		(SELECT
			opportunityid,
			COUNT(sfid) AS orders,
			SUM(order_duration__c) AS executed_hours,
			MIN(effectivedate::date) AS first_order_date_monthly
			
		FROM
			salesforce."order" t1
		WHERE
			status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
			AND EXTRACT (month FROM effectivedate::date) = 4
			AND EXTRACT (year FROM effectivedate::date) = 2018
		GROUP BY
			opportunityid) AS oo	
		ON o.sfid = oo.opportunityid

	INNER JOIN bi.b2borders ooo
		ON o.sfid = ooo.opportunity_id
		

-- ---------------------------------------------------------------------------------------------------------------------------------------------- ORDERS RESCHEDULED

LEFT JOIN


		(
		SELECT
		o8.opportunityid																											AS opportunityid
		, COUNT(sfid) 																												AS Orders_rescheduled
		, SUM(CASE WHEN o8.history_status_cancelled LIKE 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) 	AS rescheduled_Orders_Cancelled_Professional
		, SUM(CASE WHEN o8.history_status_rescheduled LIKE 'CANCELLED CUSTOMER' THEN 1 ELSE 0 END) 		AS rescheduled_Orders_Cancelled_Customer
		
		FROM
				(SELECT
				Orders.effectivedate
				, Orders.sfid
				, Orders.opportunityid AS opportunity
				, Orders."status"
				, Orders.order_start__c
				, Orders.order_end__c
				, Orders.order_time__c
				, Orders.order_duration__c
				, HISTORY.*
				, HISTORY.status_cancelled 	AS history_status_cancelled
				, HISTORY.status_rescheduled 	AS history_status_rescheduled
				, HISTORY.order_start_rescheduled::timestamp - HISTORY.order_start_cancelled::timestamp AS slot_cencelled_rescheduled
				, Orders.order_start__c::timestamp - HISTORY.order_start_rescheduled::timestamp AS slot_final
				, to_number(HISTORY.order_duration_rescheduled, '999') - to_number(HISTORY.order_duration_cancelled, '999') AS duration_dif
				
				FROM
				
					salesforce."order" AS Orders
						
							LEFT JOIN
								-- ------------- Order Status History
								(
									SELECT
									
										-- cancelled						
										 t1.date AS date_cancelled
										, t1.sf_orderid AS orderid
										, t1.opportunityid
										, t1.status AS status_cancelled
										, t1.order_start AS order_start_cancelled
										, t1.order_time__c AS order_time_cancelled
										, t1.order_duration AS order_duration_cancelled
										
										-- rescheduled
										, t2.date AS date_rescheduled
										, t2.status AS status_rescheduled
										, t2.order_start AS order_start_rescheduled
										, t2.order_time__c AS order_time_rescheduled
										, t2.order_duration AS order_duration_rescheduled
				
				
									FROM
										
										bi.b2b_orderstatushistory_Cancelled t1
										
									LEFT JOIN
										-- LATERAL
										
										bi.b2b_orderstatushistory_Rescheduled t2
										
										ON ( t1.sf_orderid = t2.sf_orderid AND t1.date < t2.date)
									
									) AS HISTORY
									
								ON (Orders.sfid = HISTORY.orderid)
					
				WHERE
				
					Orders.status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
					AND EXTRACT (month FROM Orders.effectivedate::date) = 4
					AND EXTRACT (year FROM Orders.effectivedate::date) = 2018
					
					AND HISTORY.status_rescheduled IS NOT NULL
					AND (Orders.order_start__c::timestamp - HISTORY.order_start_rescheduled::timestamp) ='00:00:00'
					
					) AS o8
			
		GROUP BY 
		o8.opportunityid	
		)
		AS o9 	
		ON o.sfid = o9.opportunityid
		
-- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED ORDERS without CANCELLED CUSTOMER	
	LEFT JOIN
		(SELECT 
			opportunityid,
			COUNT(sfid) AS Orders_cancelled,
			SUM(CASE WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) AS Orders_Cancelled_Professional,
			SUM(CASE WHEN Status LIKE 'CANCELLED MISTAKE' THEN 1 ELSE 0 END) AS Orders_Cancelled_Mistake,
			SUM(CASE WHEN Status LIKE 'CANCELLED TERMINATED' THEN 1 ELSE 0 END) AS Orders_Cancelled_Terminated
		FROM
			salesforce."order" t2
		WHERE
			status LIKE '%CANCELLED%'
			AND status NOT LIKE 'CANCELLED CUSTOMER'
			AND EXTRACT (month FROM effectivedate::date) = 4
			AND EXTRACT (year FROM effectivedate::date) = 2018
		GROUP BY 
			opportunityid) AS oooo
		ON o.sfid = oooo.opportunityid

-- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED CUSTOMER ORDERS
		
	LEFT JOIN
		(SELECT 
			opportunityid,
			COUNT(sfid) AS Orders_cancelled_customer,
			SUM(order_duration__c) AS cancelled_customer_hours
		FROM
			salesforce."order" t3
		WHERE
			status LIKE 'CANCELLED CUSTOMER'
			AND EXTRACT (month FROM effectivedate::date) = 4
			AND EXTRACT (year FROM effectivedate::date) = 2018
		GROUP BY 
			opportunityid) AS ooooo
		ON o.sfid = ooooo.opportunityid	


-- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS
-- ---------------------------------------------------------------------------------------------------------------------------------------------- 		
-- ---------------------------------------------------------------------------------------------------------------------------------------------- 
-- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS	w/o CANCELLED MISTAKE	
	LEFT JOIN			
		(SELECT
			opportunityid,
			COUNT(sfid) AS orders_holiday,
			SUM(order_duration__c) AS holiday_hours,
			MIN(effectivedate::date) AS first_order_date_monthly
			
		FROM
			salesforce."order" t4
		WHERE
			(status IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
			OR (status LIKE '%CANCELLED%' AND status NOT LIKE 'CANCELLED MISTAKE'))

			-- Holidays

			AND effectivedate::date = '2018-04-02'

		GROUP BY
			opportunityid) AS oooooo	
		ON o.sfid = oooooo.opportunityid			

-- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAY is CANCELLED MISTAKE	
	LEFT JOIN			
		(SELECT
			opportunityid,
			COUNT(sfid) AS orders_holiday_mistake,
			SUM(order_duration__c) AS holiday_mistake_hours,
			MIN(effectivedate::date) AS first_order_date_monthly
			
		FROM
			salesforce."order" t5
		WHERE
			status IN ('CANCELLED MISTAKE')

			-- Holidays

			AND effectivedate::date = '2018-04-02'
			
		GROUP BY
			opportunityid) AS ooooooo	
		ON o.sfid = ooooooo.opportunityid	



-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------------------------------------	
	
WHERE 
	o.grand_total__c IS NOT NULL
	-- AND o.stagename IN ('WON')

GROUP BY
o.sfid,
o.customer__c,
o.Name, 
o.StageName,
o.supplies__c, 
o.grand_total__c, 
o.CloseDate,
o.hours_weekly__c, 
oo.first_order_date_monthly,
oo.orders,
oo.executed_hours,
ooo.first_order_date,
oooo.orders_cancelled,
oooo.Orders_Cancelled_Professional,
oooo.Orders_Cancelled_Mistake,
oooo.Orders_Cancelled_Terminated,
ooooo.Orders_cancelled_customer,
ooooo.cancelled_customer_hours,
oooooo.orders_holiday,
ooooooo.orders_holiday_mistake,
ooooooo.holiday_mistake_hours,
o9.Orders_rescheduled,
o9.rescheduled_Orders_Cancelled_Professional,
o9.rescheduled_Orders_Cancelled_Customer) AS old