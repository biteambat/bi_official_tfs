SELECT
CANCELLED.*
, RESCHEDULED.*

FROM

(SELECT
		
				created_at::timestamp 				AS date
				, order_Json->>'Id' 					AS SF_OrderId
				, order_Json->>'OpportunityId' 	AS OpportunityId
				, order_Json->>'AccountId'			AS ProfessionalId
				-- , order_Json->>'CreatedById'		AS Created_by_Id
				-- , order_Json->>'LastModifiedId'	AS Last_modified_Id
				-- , order_Json->>'OwnerId' 			AS OwnerId 
				, order_Json->>'Status'				AS Status
				-- , order_Json->>'Order_Id__c'		AS Order_Id
		
		FROM
			
				events.sodium
		
		WHERE
		
				event_name LIKE 'Order%'
				AND (
-- 				event_name LIKE  '%CANCELLED-PROFESSIONAL%'
-- 					OR 
					event_name LIKE  '%CANCELLED CUSTOMER%'
					)
				AND EXTRACT (MONTH FROM created_at::date) = EXTRACT(MONTH FROM CURRENT_DATE)
				AND EXTRACT (YEAR FROM created_at::date) = EXTRACT(YEAR FROM CURRENT_DATE)
				
-- 				AND created_at::date = CURRENT_DATE
				AND (order_Json->>'OpportunityId') IS NOT NULL
-- 				AND (order_Json->>'Id' ) = '8010J000002NKVIQA4'

LIMIT 10

) AS CANCELLED

LEFT JOIN 

	LATERAL
	
	(

		SELECT
		
				created_at::timestamp 				AS date
				, order_Json->>'Id' 					AS SF_OrderId
				, order_Json->>'OpportunityId' 	AS OpportunityId
				, order_Json->>'AccountId'			AS ProfessionalId
				-- , order_Json->>'CreatedById'		AS Created_by_Id
				-- , order_Json->>'LastModifiedId'	AS Last_modified_Id
				-- , order_Json->>'OwnerId' 			AS OwnerId 
				, order_Json->>'Status'				AS Status
				-- , order_Json->>'Order_Id__c'		AS Order_Id
		
		FROM
		
				events.sodium
		
		WHERE
		
				event_name LIKE 'Order%'
				AND 
				(event_name LIKE '%RESCHEDULED%')
-- 			AND EXTRACT (MONTH FROM created_at::date) = EXTRACT(MONTH FROM CURRENT_DATE)
-- 				AND EXTRACT (YEAR FROM created_at::date) = EXTRACT(YEAR FROM CURRENT_DATE)
-- 				AND created_at::date = CURRENT_DATE
				AND (order_Json->>'OpportunityId') IS NOT NULL
-- 				AND (order_Json->>'Id' ) = '8010J000002Mst2QAC'
		
		) AS RESCHEDULED
		
		ON
		
		( CANCELLED.SF_OrderID = RESCHEDULED.SF_OrderId AND CANCELLED.date < RESCHEDULED.date)
		
		