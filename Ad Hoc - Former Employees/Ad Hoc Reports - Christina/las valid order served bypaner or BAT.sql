SELECT *
FROM
salesforce.account
LIMIT 10


SELECT DISTINCT ON (orders.opportunityid)
orders.sfid AS orderid
, orders.opportunityid
, opp.name as oportunity
, orders.effectivedate
, orders.professional__c

FROM 
salesforce.order orders

	LEFT JOIN 
		salesforce.opportunity opp
		ON ( orders.opportunityid = opp.sfid)
		
	LEFT JOIN 
		salesforce.account pro
		ON (orders.professional__c = pro.sfid)

WHERE 
orders.status IN ('INVOICED','FULFILLED')

ORDER BY orders.opportunityid, orders.effectivedate DESC

LIMIT 10
