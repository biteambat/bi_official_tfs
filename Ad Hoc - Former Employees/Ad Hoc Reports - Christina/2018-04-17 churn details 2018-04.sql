SELECT
BOM_final.locale
, BOM_final.city
, BOM_final.customer_id__c AS customer_ID
, BOM_final.opportunityid AS opportunity_ID
, BOM_final.Opportunity
, BOM_final.firstorder AS first_order
, EXTRACT (YEAR FROM BOM_final.age) * 12 + EXTRACT (MONTH FROM BOM_final.age) as months_with_us
, BOM_final.has_grand_total
, BOM_final.revenue
, BOM_final.operated_by
, BOM_final.operated_by_detail
, BOM_final.status
, BOM_final.churn_reason

FROM
(SELECT
BOM.Year_Month
, BOM.locale
, BOM.city
, BOM.customer_id__c
, BOM.opportunityid
, BOM.firstorder
, age (BOM.firstorder) as age
, BOM.revenue
, BOM.has_grand_total
, BOM.operated_by
, BOM.operated_by_detail
, BOM.Opportunity
, BOM.churn_reason
, BOM.status


FROM
(SELECT
	TO_CHAR(t1.Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	LEFT(t1.locale__c,2) as locale,	
	CASE WHEN t1.polygon is null THEN 
			 (CASE WHEN t1.professional__c is null THEN 'Other' ELSE 
			 (CASE WHEN t3.delivery_areas is null THEN 
			 (CASE WHEN t3.delivery_sub is null THEN 
			 'Other' ELSE t3.delivery_sub END ) ELSE t3.delivery_areas END) END)
			  ELSE t1.polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN t2.stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	t1.customer_id__c,
	t1.opportunityid,
	t2.name AS Opportunity,
	COUNT(DISTINCT(t2.sfid)) as sfid,
	t4.first_order_date AS firstorder,	
	CASE WHEN t2.grand_total__c > 0 THEN t2.grand_total__c ELSE 0 END AS revenue,
	CASE WHEN t2.grand_total__c > 0 THEN 1 ELSE 0 END AS has_grand_total,
	t3.operated_by,
	t3.operated_by_detail,
	t2.churn_reason__c AS churn_reason,
	t2.status__c AS status
	
FROM
	bi.orders t1
	
	-- add Opportunity details
		LEFT JOIN 
			Salesforce.Opportunity t2
		ON
			(t1.Opportunityid = t2.sfid AND LOWER (t2.name) NOT LIKE '%test%')
	
	-- get first order date
		LEFT JOIN
			bi.b2borders t4
		ON (t1.Opportunityid = t4.opportunity_id)			
		
	-- add Professional details JOIN with Parent Account (Partner?)
		LEFT JOIN
			(SELECT
			*,
			sub1.sfid as sub1sfid,
			sub1.delivery_areas__c as delivery_areas,
			sub2.delivery_areas__c as delivery_sub,
			CASE WHEN sub2.name NOT LIKE '%B2BS%' THEN 'BAT' ELSE 'Partner' END AS operated_by,
			CASE WHEN sub2.name NOT LIKE '%B2BS%' THEN 'BAT' ELSE sub2.name END AS operated_by_detail
			
			FROM
				Salesforce.Account sub1
			LEFT JOIN Salesforce.Account sub2
				ON (sub1.parentid = sub2.sfid))
			
			AS t3
		ON
			(t1.professional__c = t3.sub1sfid)
		
		
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL','WAITING FOR RESCHEDULE')
	AND t1.order_type = '2'
	AND LEFT(t1.locale__c,2) = 'de'
	AND EXTRACT(year FROM effectivedate) = EXTRACT (year FROM Current_date)
	AND EXTRACT(month FROM effectivedate) = EXTRACT (month FROM (Current_date))
	AND t2.test__c = false
	
	
GROUP BY
t1.Effectivedate
, t2.stagename
, Year_month
, locale
, t1.polygon
, customer_type
, customer_id__c
, opportunityid
, customer_id__c
, t1.professional__c
, t3.delivery_areas
, t3.delivery_sub
, t4.first_order_date
, t2.grand_total__c
, t3.operated_by
, t3.operated_by_detail	
, t2.name
, t2.churn_reason__c
, t2.status__c
) AS BOM

GROUP BY
BOM.Year_Month
, BOM.locale
, BOM.city
, BOM.customer_id__c
, BOM.opportunityid
, BOM.firstorder
, BOM.revenue
, BOM.has_grand_total
, BOM.operated_by
, BOM.operated_by_detail
, BOM.Opportunity
, BOM.churn_reason
, BOM.status
) AS BOM_final


LEFT JOIN

(SELECT
EOM.Year_Month
, EOM.locale
, EOM.city
, EOM.customer_id__c
, EOM.opportunityid
, min(EOM.mindate) as mindate
, COUNT(DISTINCT(EOM.opportunityid)) as value
FROM
(SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,

	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 
			 (CASE WHEN professional__c is null THEN 'Other' ELSE (CASE WHEN t3.delivery_areas__c is null THEN 'Other' ELSE t3.delivery_areas__c END) END)
			  ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	customer_id__c,
	opportunityid,

		min(effectivedate::date) as mindate,
	COUNT(DISTINCT(t2.sfid)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid and LOWER(t2.name) not like '%test%')
LEFT JOIN
	Salesforce.account t3
ON
	(t1.professional__c = t3.sfid)

WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL','WAITING FOR RESCHEDULE')
	AND t1.order_type = '2'
	AND EXTRACT(year FROM effectivedate) = EXTRACT (year FROM Current_date)
	AND EXTRACT(month FROM effectivedate) = EXTRACT (month FROM Current_date + Interval '1 Month')
	AND LEFT(t1.locale__c,2) = 'de'
GROUP BY
Year_month
, locale
, polygon
, customer_type
, customer_id__c
, opportunityid
, customer_id__c
,	professional__c
,	t3.delivery_areas__c) AS EOM

GROUP BY
EOM.Year_Month
, EOM.locale
, EOM.city
, EOM.customer_id__c
, EOM.opportunityid) AS EOM_final

ON (BOM_final.opportunityid = EOM_final.opportunityid)
WHERE
EOM_final.opportunityid IS NULL
