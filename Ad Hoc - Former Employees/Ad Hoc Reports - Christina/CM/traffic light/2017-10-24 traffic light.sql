SELECT
	locale,
	Company,
	CASE 
	WHEN NSPrate >= 0.1 THEN 1
	WHEN NSPrate between 0.005 and 0.1 THEN 2
	WHEN NSPrate <= 0.005 THEN 3 END as traffic_light_noshow,
	CASE 
	WHEN NCRate >= 0.2 THEN 1
	WHEN NCRate between 0.005 and 0.2 THEN 2
	WHEN NCRate <= 0.005 THEN 3 END as traffic_light_cancelled_pro,
	CASE 
	WHEN inbound_calls >= 4 THEN 1
	WHEN inbound_calls between 2 AND 4 THEN 2
	WHEN inbound_calls <= 2 OR (inbound_calls IS NULL) = TRUE THEN 3 END as traffic_light_inbound_calls,
	CASE 
	WHEN lost_inbound_calls >= 4 THEN 1
	WHEN lost_inbound_calls between 2 AND 4 THEN 2
	WHEN lost_inbound_calls <= 2 OR (lost_inbound_calls IS NULL) = TRUE THEN 3 END as traffic_light_lost_calls,
	CASE 
	WHEN CreatedCases >= 4 THEN 1
	WHEN CreatedCases between 2 AND 4 THEN 2
	WHEN CreatedCases <=2 OR (CreatedCases IS NULL) = TRUE THEN 3 END as traffic_light_CreatedCases,
	CASE 
	WHEN Retention_All > 0 AND Retention_closed <> Retention_All THEN 1
	WHEN Retention_All > 0 AND Retention_closed = Retention_All THEN 1
	WHEN Retention_All =0 OR (Retention_All IS NULL) = TRUE THEN 3 END as traffic_light_Retention,
	CASE 
	WHEN InvoiceCorrection_All > 0 AND InvoiceCorrection_closed <> InvoiceCorrection_All THEN 1
	WHEN InvoiceCorrection_All > 0 AND InvoiceCorrection_closed = InvoiceCorrection_All THEN 1
	WHEN InvoiceCorrection_All =0 OR (InvoiceCorrection_All IS NULL) = TRUE THEN 3 END as traffic_light_InvoiceCorrection,
	CASE 
	WHEN ProfessionalImprovement_All > 0 AND ProfessionalImprovement_closed <> ProfessionalImprovement_All THEN 1
	WHEN ProfessionalImprovement_All > 0 AND ProfessionalImprovement_closed = ProfessionalImprovement_All THEN 1
	WHEN ProfessionalImprovement_All =0 OR (ProfessionalImprovement_All IS NULL) = TRUE THEN 3 END as traffic_light_ProfessionalImprovement

	
FROM
		(SELECT
		sub1.locale AS locale,
		sub1.Company AS Company,
		
		-- orders 
		SUM(sub1.Orders_NSP) AS Orders_NSP,
		SUM(sub1.NSP) AS NSP,
		(CASE WHEN SUM(sub1.Orders_NSP) > 0 THEN SUM(sub1.NSP)/SUM(sub1.Orders_NSP) ELSE 0 END) AS NSPRate,
		
		SUM(sub1.Orders_CP) AS Orders_CANCELLED_PROFESSIONAL,
		SUM(sub1.CP) AS CANCELLED_PROFESSIONAL,
		(CASE WHEN SUM(sub1.Orders_CP) > 0 THEN SUM(sub1.CP)/SUM(sub1.Orders_CP) ELSE 0 END) AS NCRate,
		
		SUM(sub1.Orders_NSP_CP) AS Orders_NSP_CP,
		SUM(sub1.NSP_CP) AS NSP_CANCELLED_PROFESSIONAL,
		(CASE WHEN SUM(sub1.Orders_NSP_CP) > 0 THEN SUM(sub1.NSP_CP)/SUM(sub1.Orders_NSP_CP) ELSE 0 END) AS Rate,
		
		-- calls
		sub2.inbound_calls AS inbound_calls,
		sub2.lost_inbound_calls AS lost_inbound_calls,
		
		-- cases
		CreatedCases AS CreatedCases,
		sub2.Retention_All AS Retention_All,
		sub2.Retention_closed AS Retention_closed,
		sub2.InvoiceCorrection_All AS InvoiceCorrection_All,
		sub2.InvoiceCorrection_closed AS InvoiceCorrection_closed,
		sub2.ProfessionalImprovement_All AS ProfessionalImprovement_All,
		sub2.ProfessionalImprovement_closed AS ProfessionalImprovement_closed,		
		
		-- other
		sub2.all_contacts AS all_contacts,	
		sub2.Opp_active AS Opp_active

		FROM
				(SELECT 
					left(t1.locale__c,2) AS locale,
					MIN(t1.effectivedate::date) AS FirstDate,
					t1.contact__c AS Contact,
					
					t2.company_name__c AS Company,
					SUM(CASE WHEN t1.status like'%NOSHOW PROFESSIONAL%' THEN 1 
								WHEN t1.status like'%INVOICED%' THEN 1 
								WHEN t1.status like'%FULFILLED%' THEN 1 ELSE 0 END) AS Orders_NSP,
					SUM(CASE WHEN t1.status like'%NOSHOW PROFESSIONAL%' THEN 1 ELSE 0 END) AS NSP,
					SUM(CASE WHEN t1.status like'%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN t1.status like'%INVOICED%' THEN 1 
								WHEN t1.status like'%FULFILLED%' THEN 1 ELSE 0 END) AS Orders_CP,
					SUM(CASE WHEN t1.status like'%CANCELLED PROFESSIONAL%' THEN 1 ELSE 0 END) AS CP,
					SUM(CASE WHEN t1.status like'%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN t1.status like'%NOSHOW PROFESSIONAL%' THEN 1 
								WHEN t1.status like'%INVOICED%' THEN 1 
								WHEN t1.status like'%FULFILLED%' THEN 1 ELSE 0 END) AS Orders_NSP_CP,
					SUM(CASE WHEN t1.status like'%CANCELLED PROFESSIONAL%' THEN 1 
								WHEN t1.status like'%NOSHOW PROFESSIONAL%' THEN 1 ELSE 0 END) AS NSP_CP			
					FROM
					bi.orders t1
						LEFT JOIN 
							salesforce.contact t2
						ON
							(t1.contact__c = t2.sfid)
					WHERE
							order_type = '2'
							AND effectivedate::date between current_date - INTERVAL '8 Weeks' and current_date
							AND (t2.status__c <> 'LEFT' AND t2.status__c <> 'INACTIVE')
							
					GROUP BY
					locale,
					Contact,
					
					company_name__c) AS sub1
				
				LEFT JOIN
								(SELECT
									min(sub3.date) AS startdate_of_week,
									sub3.locale,
									sub3.Contact AS Contact,
									sub3.ContactName,
									sub3.Company,							
									SUM(CASE WHEN all_inbound_calls IS NULL THEN 0 ELSE all_inbound_calls END) AS inbound_calls,
									SUM(CASE WHEN lost_inbound_calls IS NULL THEN 0 ELSE lost_inbound_calls END) AS lost_inbound_calls,
									SUM(CASE WHEN b2b_outbound IS NULL THEN 0 ELSE b2b_outbound END) AS b2b_outbound_calls,
									SUM(CASE WHEN CreatedCases IS NULL THEN 0 ELSE CreatedCases END) AS CreatedCases,
									
									-- neu
									SUM(CASE WHEN Retention_All IS NULL THEN 0 ELSE Retention_All END) AS Retention_All,
									SUM(CASE WHEN Retention_closed IS NULL THEN 0 ELSE Retention_closed END) AS Retention_closed,
									SUM(CASE WHEN InvoiceCorrection_All IS NULL THEN 0 ELSE InvoiceCorrection_All END) AS InvoiceCorrection_All,
									SUM(CASE WHEN InvoiceCorrection_closed IS NULL THEN 0 ELSE InvoiceCorrection_closed END) AS InvoiceCorrection_closed,
									SUM(CASE WHEN ProfessionalImprovement_All IS NULL THEN 0 ELSE ProfessionalImprovement_All END) AS ProfessionalImprovement_All,
									SUM(CASE WHEN ProfessionalImprovement_closed IS NULL THEN 0 ELSE ProfessionalImprovement_closed END) AS ProfessionalImprovement_closed,
									
									SUM(CASE WHEN all_inbound_calls IS NULL THEN 0 ELSE all_inbound_calls END)
									+ SUM(CASE WHEN b2b_outbound IS NULL THEN 0 ELSE b2b_outbound END)
									+ SUM(CASE WHEN CreatedCases IS NULL THEN 0 ELSE CreatedCases END) AS all_contacts,		
									
									SUM(Invoiced_Orders) AS Invoiced_Orders,
									SUM(Opp_active) AS Opp_active
								FROM
									(SELECT
										min(date) AS date,
										LEFT(t3.locale__c,2) AS locale,
										total_order_count AS invoiced_orders,
										COUNT(DISTINCT(opportunity_id)) AS Opp_active,
										t3.contact__c AS Contact,
										t3.Contact_Name__c AS ContactName,
										t3.name AS Company
										
									FROM
										bi.b2borders t3
						         WHERE 	
						            year_month = TO_CHAR(current_date,'YYYY-MM')
									GROUP BY
										locale,
										contact,
										total_order_count,
										t3.contact_name__c,
										t3.name)  AS sub3
								
									LEFT JOIN
										(SELECT
											CASE 
												WHEN calledcountryshort__c = 'AUT' THEN 'at'
												WHEN calledcountryshort__c = 'CHE' THEN 'ch'
												WHEN calledcountryshort__c = 'NLD' THEN 'nl'
												ELSE 'de' END as locale,
											SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END)+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' THEN 1 ELSE 0 END) as all_inbound_calls,
											SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END) as lost_inbound_calls,
											SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' THEN 1 ELSE 0 END) AS all_outbound,
											SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t5.type__c= 'customer-b2b' THEN 1 ELSE 0 END) AS b2b_outbound,
											RelatedContact__c as Contact
										FROM 
											Salesforce.natterbox_call_reporting_object__c t4
										LEFT JOIN 
											salesforce.contact t5
										ON
											(t4.relatedcontact__c = t5.sfid)
										WHERE
										call_start_date_date__c::date between current_date - INTERVAL '8 Weeks' and current_date
										AND
										((E164Callednumber__C = '493030807263'
											OR E164Callednumber__C='493030807264')
											AND calldirection__c = 'Inbound')
										OR
										(Callerfirstname__c like 'CM%'
											AND calldirection__c = 'Outbound')
										GROUP BY
										locale,
										Contact) as sub4
										ON
										(sub3.locale = sub4.locale and sub3.Contact = sub4.Contact)
								
										LEFT JOIN
											(SELECT 
												CASE
													WHEN (Origin LIKE '%B2B - Contact%' OR Origin LIKE '%B2B DE - CLOSET%' OR Origin LIKE '%checkout%' OR Origin LIKE '%CM DE%' OR Origin LIKE '%TM DE - General Email%')  THEN 'de'
													WHEN (Origin LIKE '%B2B CH - CLOSET%' OR Origin LIKE '%CM CH%' OR Origin LIKE '%TM CH - General Email%') THEN 'ch'
													WHEN (Origin LIKE '%B2B AT - CLOSET%' OR Origin LIKE '%CM AT%' OR Origin LIKE '%TM AT - General Email%') THEN 'at'
													WHEN (Origin LIKE '%B2B NL - CLOSET%' OR Origin LIKE '%CM NL%' OR Origin LIKE '%TM NL - General Email%') THEN 'nl'
													ELSE 'Other' END AS locale,
												COUNT(1) as CreatedCases,
												Contactid as Contact
											FROM
												Salesforce.case		
											WHERE
												CreatedDate::date between current_date - INTERVAL '8 Weeks' and current_date
												AND 
												(type LIKE '%KA%'
													OR
													((Origin LIKE '%B2B - Contact%'
														OR Origin LIKE '%B2B AT - CLOSET%'
														OR Origin LIKE '%B2B CH - CLOSET%'
														OR Origin LIKE '%B2B DE - CLOSET%'
														OR Origin LIKE '%B2B NL - CLOSET%')
														AND type != 'KA'))
												AND Isdeleted = false
												AND CreatedById = '00520000003IiNCAA0'
											
											GROUP BY 
												locale, 
												Contact) AS sub5 
											
												ON
												(sub3.locale = sub5.locale and sub3.Contact = sub5.Contact)
										
										LEFT JOIN
											(SELECT 
												CASE
													WHEN (Origin LIKE '%B2B - Contact%' OR Origin LIKE '%B2B DE - CLOSET%' OR Origin LIKE '%checkout%' OR Origin LIKE '%CM DE%' OR Origin LIKE '%TM DE - General Email%' OR Origin LIKE '%TO DE - Contact%' OR Origin LIKE '%TO DE - SMS%' OR Origin LIKE '%B2B - Sales')  THEN 'de'
													WHEN (Origin LIKE '%B2B CH - CLOSET%' OR Origin LIKE '%CM CH%' OR Origin LIKE '%TM CH - General Email%' OR Origin LIKE '%TO CH - Contact%') THEN 'ch'
													WHEN (Origin LIKE '%B2B AT - CLOSET%' OR Origin LIKE '%CM AT%' OR Origin LIKE '%TM AT - General Email%' OR Origin LIKE '%TO AT - Contact%') THEN 'at'
													WHEN (Origin LIKE '%B2B NL - CLOSET%' OR Origin LIKE '%CM NL%' OR Origin LIKE '%TM NL - General Email%' OR Origin LIKE '%TO NL - Contact%') THEN 'nl'
													ELSE 'Other' END AS locale,
												Contactid as Contact,
												SUM(CASE WHEN reason LIKE '%Customer - Retention%' THEN 1 ELSE 0 END) AS Retention_All,
												SUM(CASE WHEN reason LIKE '%Customer - Retention%' AND isclosed = 'true' THEN 1 ELSE 0 END) AS Retention_closed,
												SUM(CASE WHEN reason LIKE '%Order - Invoice editing%' AND subject LIKE '%nvoice %orrection%' THEN 1 ELSE 0 END) AS InvoiceCorrection_All,
												SUM(CASE WHEN reason LIKE '%Order - Invoice editing%' AND subject LIKE '%nvoice %orrection%' AND isclosed = 'true' THEN 1 ELSE 0 END) AS InvoiceCorrection_closed,
												SUM(CASE WHEN reason LIKE '%Professional - Improvement%' THEN 1 ELSE 0 END) AS ProfessionalImprovement_All,
												SUM(CASE WHEN reason LIKE '%Professional - Improvement%' AND isclosed = 'true' THEN 1 ELSE 0 END) AS ProfessionalImprovement_closed
												
											FROM
												Salesforce.case		
											WHERE
												CreatedDate::date between current_date - INTERVAL '8 Weeks' and current_date
												AND Isdeleted = false
												AND 	((reason LIKE '%Customer - Retention%' OR reason LIKE '%Order - Invoice editing%' AND type LIKE '%KA%') 
														OR (reason LIKE '%Professional - Improvement%' AND type LIKE '%TO%'))
											
											GROUP BY 
												locale, 
												Contact) As sub6
												
											ON (sub3.locale =sub6.locale AND sub3.Contact = sub6.Contact)
										
							
								GROUP BY
								sub3.locale,
								sub3.Contact,
								sub3.ContactName,
								Company) AS sub2
				
								ON
								(sub1.locale = sub2.locale and sub1.Contact = sub2.Contact)		
				
		
GROUP BY
sub1.locale, 
sub1.Company,
sub2.inbound_calls,
sub2.lost_inbound_calls,
CreatedCases,
sub2.Retention_All,
sub2.Retention_closed,
sub2.InvoiceCorrection_All,
sub2.InvoiceCorrection_closed,
sub2.ProfessionalImprovement_All,
sub2.ProfessionalImprovement_closed,
sub2.all_contacts,		
sub2.Opp_active) as a