DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_cases_service_level_basis 	AS 

SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.createddate				date1_opened
			, MIN(cahi.createddate) 		date2_closed
			, CAST('New Case' as varchar) 	AS type
--			, age ( MIN(cahi.createddate), ca.createddate) AGE
--			, extract (epoch FROM age ( MIN(cahi.createddate), ca.createddate)) / 3600 SVL
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN 	(SELECT * 	FROM salesforce.casehistory type 
						WHERE		type.field 					= 			'Type'
							AND 	type.newvalue		= 			'CM B2B'
							AND		type.oldvalue		NOT IN 		('B2B','KA')
--							AND		type.createddate::date <> 		ca.case_createddate::date
							AND 	type.createddate::date >= 		'2019-01-01'
--							AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
--							AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
							) AS typechanged
							
							ON ca.sfid 				= typechanged.caseid 
													
LEFT JOIN 	salesforce.casehistory	cahi	ON ca.sfid				= cahi.caseid 
												AND   	cahi.field IN ('created','Status') 
												AND 	cahi.newvalue IN ('Closed')

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE																						-- 1		
					
	--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	ca.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'				THEN 1
						WHEN 	ca.type 	= 'CLM'					THEN 1
						WHEN 	ca.type		= 'CM B2C'				THEN 1
						WHEN 	ca.type		= 'Sales'				THEN 1
						WHEN 	ca.type		= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	LIKE 'CM%'				THEN 1
						WHEN 	ca.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	ca.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	ca.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'					THEN 1
						WHEN 	ca.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'Pool'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			)
		)	
		
	AND ca.createddate::date > '2019-01-01'	
	AND typechanged.caseid IS NULL
--		AND ca.casenumber = '00535948' 								-- in case you are looking for a case -- its case^2 
		
GROUP BY
		ca.sfid						
			, ca.casenumber					
			, ca.createddate				

UNION ALL


SELECT 		ca.case_ID							case_ID
			, ca.case_number					case_number
			, hi.createddate  					date1_opened
			, MIN(cahi.createddate) 			date2_closed
			, CAST('Reopened Case' as varchar) 	AS type

FROM salesforce.casehistory hi
INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate

			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
					) 						ca 		ON hi.caseid			= ca.case_ID	
							
-- date 2

LEFT JOIN 	salesforce.casehistory	cahi			ON ca.case_ID				= cahi.caseid 
														AND   	cahi.field IN ('created','Status') 
														AND 	cahi.newvalue IN ('Closed')	
 														AND cahi.createddate >= hi.createddate

WHERE		-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2019-01-01'
--			AND ca.case_number	 = '00506591'
			
GROUP BY
ca.case_id
, ca.case_number
, hi.createddate

UNION ALL


SELECT 		ca.case_ID							case_ID
			, ca.case_number					case_number
			, hi.createddate  					date1_opened
			, MIN(cahi.createddate) 			date2_closed
			, CAST('Type Change' as varchar) 	AS type

FROM salesforce.casehistory hi
INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.origin 					case_origin

			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
					) 						ca 		ON hi.caseid			= ca.case_ID	
							
-- date 2

LEFT JOIN 	salesforce.casehistory			cahi			ON ca.case_ID				= cahi.caseid 
														AND   	cahi.field IN ('created','Status') 
														AND 	cahi.newvalue IN ('Closed')	
 														AND cahi.createddate >= hi.createddate

WHERE		-- type change		
			hi.field 					= 			'Type'
			AND 	hi.newvalue			= 			'CM B2B'
			AND		hi.oldvalue			NOT IN 		('B2B','KA')
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2019-01-01'
			AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
			AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
				
GROUP BY
ca.case_id
, ca.case_number
, hi.createddate

ORDER BY 	case_id
			, date1_opened
			, date2_closed

;