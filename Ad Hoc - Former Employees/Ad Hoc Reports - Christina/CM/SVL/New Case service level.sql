SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.createddate				date1_opened
			, MIN(cahi.createddate) 		date2_closed
			, CAST('New Case' as varchar) 	AS type
--			, ca.status						case_status
--			, age ( MIN(cahi.createddate), ca.createddate) AGE
--			, extract (epoch FROM age ( MIN(cahi.createddate), ca.createddate)) / 3600 SVL
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
-- LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
-- LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
-- LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
-- LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-----------------------------------------------------------------------------------------
-- JOIN case history 

LEFT JOIN 	salesforce.casehistory	cahi	ON ca.sfid				= cahi.caseid 
												AND   	cahi.field IN ('created','Status') 
												AND 	cahi.newvalue IN ('Closed')

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE																						-- 1		
					
	--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	ca.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'				THEN 1
						WHEN 	ca.type 	= 'CLM'					THEN 1
						WHEN 	ca.type		= 'CM B2C'				THEN 1
						WHEN 	ca.type		= 'Sales'				THEN 1
						WHEN 	ca.type		= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	LIKE 'CM%'				THEN 1
						WHEN 	ca.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	ca.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	ca.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'					THEN 1
						WHEN 	ca.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'Pool'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			)
		)	
		
	AND ca.createddate::date > '2019-01-01'	
--		AND ca.casenumber = '00535948' 								-- in case you are looking for a case -- its case^2 
		

GROUP BY
		ca.sfid						
			, ca.casenumber					
			, ca.createddate				
			, ca.isclosed					
			, ca.status							
;