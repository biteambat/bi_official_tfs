DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level_basis;
CREATE 	TABLE 				bi.CM_cases_service_level_basis 	AS 

SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.createddate				date1_opened
			, MIN(cahi.createddate) 		date2_closed
			, CAST('New Case' as varchar) 	AS type
--			, age ( MIN(cahi.createddate), ca.createddate) AGE
--			, extract (epoch FROM age ( MIN(cahi.createddate), ca.createddate)) / 3600 SVL
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN 	(SELECT * 	FROM salesforce.casehistory type 
						WHERE		type.field 					= 			'Type'
							AND 	type.newvalue		= 			'CM B2B'
							AND		type.oldvalue		NOT IN 		('B2B','KA')
--							AND		type.createddate::date <> 		ca.case_createddate::date
							AND 	type.createddate::date >= 		'2018-07-01'
--							AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
--							AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
							) AS typechanged
							
							ON ca.sfid 				= typechanged.caseid 
													
LEFT JOIN 	salesforce.casehistory	cahi	ON ca.sfid				= cahi.caseid 
												AND   	cahi.field IN ('created','Status') 
												AND 	cahi.newvalue IN ('Closed')

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE																						-- 1		
					
	--	excluded case owner	
		((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	ca.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'				THEN 1
						WHEN 	ca.type 	= 'CLM'					THEN 1
						WHEN 	ca.type		= 'CM B2C'				THEN 1
						WHEN 	ca.type		= 'Sales'				THEN 1
						WHEN 	ca.type		= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	LIKE 'CM%'				THEN 1
						WHEN 	ca.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	ca.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	ca.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'					THEN 1
						WHEN 	ca.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'Pool'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
			)
		)	
		
	AND ca.createddate::date > '2018-07-01'	
	AND typechanged.caseid IS NULL
--		AND ca.casenumber = '00535948' 								-- in case you are looking for a case -- its case^2 
		
GROUP BY
		ca.sfid						
			, ca.casenumber					
			, ca.createddate				

UNION ALL


SELECT 		ca.case_ID							case_ID
			, ca.case_number					case_number
			, hi.createddate  					date1_opened
			, MIN(cahi.createddate) 			date2_closed
			, CAST('Reopened Case' as varchar) 	AS type

FROM salesforce.casehistory hi
INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate

			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
					) 						ca 		ON hi.caseid			= ca.case_ID	
							
-- date 2

LEFT JOIN 	salesforce.casehistory	cahi			ON ca.case_ID				= cahi.caseid 
														AND   	cahi.field IN ('created','Status') 
														AND 	cahi.newvalue IN ('Closed')	
 														AND cahi.createddate >= hi.createddate

WHERE		-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2018-07-01'
--			AND ca.case_number	 = '00506591'
			
GROUP BY
ca.case_id
, ca.case_number
, hi.createddate

UNION ALL


SELECT 		ca.case_ID							case_ID
			, ca.case_number					case_number
			, hi.createddate  					date1_opened
			, MIN(cahi.createddate) 			date2_closed
			, CAST('Type Change' as varchar) 	AS type

FROM salesforce.casehistory hi
INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.origin 					case_origin

			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
					) 						ca 		ON hi.caseid			= ca.case_ID	
							
-- date 2

LEFT JOIN 	salesforce.casehistory			cahi			ON ca.case_ID				= cahi.caseid 
														AND   	cahi.field IN ('created','Status') 
														AND 	cahi.newvalue IN ('Closed')	
 														AND cahi.createddate >= hi.createddate

WHERE		-- type change		
			hi.field 					= 			'Type'
			AND 	hi.newvalue			= 			'CM B2B'
			AND		hi.oldvalue			NOT IN 		('B2B','KA')
			AND		hi.createddate::date <> 		ca.case_createddate::date
			AND 	hi.createddate::date >= 		'2018-07-01'
			AND 	ca.case_origin		NOT LIKE 	'%B2B customer%' 	
			AND 	ca.case_origin	 	NOT LIKE 	'CM - Team'
				
GROUP BY
ca.case_id
, ca.case_number
, hi.createddate

ORDER BY 	case_id
			, date1_opened
			, date2_closed

;


SELECT *
FROM bi.CM_cases_service_level

-- LIMIT 10
;

DROP 	TABLE IF EXISTS 	bi.CM_cases_service_level;
CREATE 	TABLE 				bi.CM_cases_service_level 	AS 

SELECT 	
--		date_trunc ('day', basis.date1_opened::date)	AS trunc
--		, date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' AS trunc
--		, date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened  AS test
				
--		, CASE WHEN date_part('dow', basis.date1_opened) IN ('6','0')	THEN 0 ELSE  
--				(DATE_PART 	('hour', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened) * 60 
--						+ DATE_PART 	('minute', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened)) END   AS date1_opened_minutes -- check if its weekend ? 
						
--		, (DATE_PART 	('hour', basis.date2_closed) * 60 
--						+ DATE_PART 	('minute', basis.date2_closed))  AS date2_closed_minutes
						
--		, (CASE WHEN
--				((	SELECT 	COUNT(*)
--					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
--					WHERE 	date_part ('dow', dd) NOT IN ('6','0')) -2	) 			> 0 
			
			-- in case the working days are > 0 -> workind days * 24 * 60 minutes = working days in minutes within working h
--			THEN  
--				((SELECT COUNT(*)
--				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
--				WHERE date_part('dow', dd) NOT IN ('6','0')) - CASE WHEN date_part('dow', basis.date1_opened) IN ('6')	THEN 1 ELSE 2 END	) * 24 * 60
--			ELSE 0 END)																		AS Days_Minutes
--		, CASE WHEN date_part('dow', basis.date1_opened) IN ('6')	THEN 1 ELSE 0 END 	AS weekday	
			
		--, basis.date2_closed
		basis.*
-- ---------------------------------------------------------------------------------- difference: case opened / reopened / type change -> case closed		
-- -
-- - working h Mo-Fr 08:00:00 - 17:00:00 
-- - summertime timezone difference of 1 h

		, CASE 	WHEN basis.date1_opened::date 			IS NULL 							THEN 'rule 0.1' -- opened date missing
				WHEN basis.date2_closed::date 			IS NULL 							THEN 'rule 0.2' -- still open case
				WHEN basis.date1_opened::timestamp 	> basis.date2_closed::timestamp			THEN 'rule 1.0' -- open after closed
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN -- is same date
					 (CASE WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 2.1' 	 -- opened befor working day start
																							ELSE 'rule 2.2' END) -- 
				ELSE (CASE WHEN TIME '16:00:00.0' < basis.date1_opened::time 				THEN 'rule 3.1' 
							  WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') 	THEN 'rule 3.2'
				ELSE (CASE WHEN basis.date1_opened::time < TIME '07:00:00.0' 				THEN 'rule 3.3' 
																							ELSE 'rule 3.4'END)
				END) END 																	AS rule_set 
				
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					THEN 
		
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
					(CASE WHEN basis.date1_opened::time < TIME '07:00:00.0'					THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
						+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) END)
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
	-- minutes pased on the owner change date UNTIL END OF WORK
				-- owner changed after working day
						(CASE 	WHEN TIME '16:00:00.0' < basis.date1_opened::time 			THEN 0 
						-- owner change at the weekend
								WHEN date_part('dow', basis.date1_opened::date) IN ('6','0') THEN 0
				ELSE
					-- owner changed before working day
						(CASE 	WHEN basis.date1_opened::time < TIME '07:00:00.0' 			THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60
								ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - basis.date1_opened::time) * 60 
										+ DATE_PART 	('minute', TIME '16:00:00.0' - basis.date1_opened::time) END) END)
					+
					-- minutes pased on the first contact date beginning at the morning working time
						(CASE 	WHEN basis.date2_closed::time < TIME '07:00:00.0' 			THEN 0 
								WHEN basis.date2_closed::time < TIME '16:00:00.0' 			THEN 	DATE_PART 	('hour', basis.date2_closed::time - TIME '07:00:00.0' ) * 60 
																									+	DATE_PART 	('minute', basis.date2_closed::time - TIME '07:00:00.0')
					ELSE 	(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60 END)
					+
					
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) -2	) 			> 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END) END 																AS SVL_Minutes
			
-- ------------------------------------------------------------------------------------------------------------------------------------------------
-- - SVL Customer, dont care about working hours
-- -
				
-- if the case opened was after the case closed date just use the value "-1" -> this will be used as a filter in tableau				
		, CASE 	WHEN basis.date1_opened::timestamp > basis.date2_closed::timestamp 			THEN -1

-- if case opened date and case closed date on the same day, calculate difference between times in minutes
				WHEN basis.date1_opened::date = basis.date2_closed::date 					
				THEN 		DATE_PART 	('hour', basis.date2_closed::timestamp - basis.date1_opened::timestamp) * 60 
							+ DATE_PART 	('minute', basis.date2_closed::timestamp - basis.date1_opened::timestamp) 
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
				ELSE
				
				
				-- date opened to EOD in minutes
				CASE WHEN date_part('dow', basis.date1_opened) IN ('6','0')	THEN 0 ELSE  
				(DATE_PART 	('hour', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened) * 60 
						+ DATE_PART 	('minute', date_trunc ('day', basis.date1_opened::date)	+ interval '1 day' - basis.date1_opened)) END 
						
				-- + minutes on the case closed date		
				+	(DATE_PART 	('hour', basis.date2_closed) * 60  + DATE_PART 	('minute', basis.date2_closed))		
				+
					
	-- create a list of all date incl. case opened date and case closed date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
			(CASE WHEN
				((	SELECT 	COUNT(*)
					FROM 	generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
					WHERE 	date_part('dow', dd) NOT IN ('6','0')) - 2)			> 0 
			
			-- in case the working days are > 0 -> workind days * 24 * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNT(*)
				FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) - CASE WHEN date_part('dow', basis.date1_opened) IN ('6')	THEN 1 ELSE 2 END) * 24 * 60
			ELSE 0 END)  	END
																								AS SVL_Customer
			
			, cas.createddate				case_createddate
			, cas.isclosed					case_isclosed
			, cas.ownerid					case_ownerid
			, u.name						case_owner
			, cas.origin					case_origin
			, cas.type						case_type
			, cas.reason					case_reason
			, cas.status					case_status
			, cas.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, cas.order__c					orderid
			, o.type						order_type
			, cas.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, cas.opportunity__c			opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
		
FROM bi.cm_cases_service_level_basis basis

LEFT JOIN salesforce.case 			cas 	ON basis.case_id		= cas.sfid
LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

-- WHERE basis.case_number = '00304377'

-- LIMIT 100
;