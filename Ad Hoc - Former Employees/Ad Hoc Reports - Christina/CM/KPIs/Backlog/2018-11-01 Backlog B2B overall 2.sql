SELECT 		ca.sfid
			, ca.casenumber
			, ca.isclosed
			, ca.ownerid
			, u.name
			, ca.origin
			, ca.type
			, ca.reason
			, ca.status
			, ca.contactid
			, co.name
			, co.type__c
			, co.company_name__c
			, ca.order__c
			, o.type
			, ca.accountid
			, a.name
			, a.company_name__c
			, ca.opportunity__c
			, opp.name
			, opp.grand_total__c
			-- , ca.segment					-250 / 250-500 / 500-1000 / 1000+
			-- , *
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE		ca.isclosed				= FALSE																					-- 1										
	AND	((CASE	WHEN 	u.name 		= 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		= 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		= 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		= 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		= 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		= 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		= 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		= 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		= 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		= 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		= 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		= 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		= 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		= 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		= 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		= 	'%André%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	= 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	= 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	= 'B2B DE%' 		THEN 1
						WHEN 	ca.origin 	= 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	= 'TFS CM%' 		THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'			THEN 1
						WHEN 	ca.type 	= 'CLM'				THEN 1
						WHEN 	ca.type		= 'CM B2C'			THEN 1
						WHEN 	ca.type		= 'Sales'			THEN 1
						WHEN 	ca.type		= 'PM'				THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	= 'CM%'				THEN 1
						WHEN 	ca.origin 	= 'Insurance' 		THEN 1
						WHEN 	ca.origin	= '%checkout%' 		THEN 1
						WHEN 	ca.origin 	= '%partner portal' THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'				THEN 1
						WHEN 	ca.type 	= 'B2B'				THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	= 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'			THEN 1
						WHEN 	ca.type 	= 'Pool'			THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'			THEN 1 ELSE 0 END) = 1
			)
		)
;

	
-- rebuild salforce report: https://eu7.salesforce.com/00O0J0000074D80
-- Div 25 cases (sf: 505; query: 480)