-- rebuild salforce report: https://eu7.salesforce.com/00O0J0000074D80
-- Div 25 cases (sf: 505; query: 480)

SELECT 		ca.sfid
			, ca.casenumber
			, ca.isclosed
			, ca.ownerid
			, u.name
			, ca.origin
			, ca.type
			, ca.reason
			, ca.status
			, ca.contactid
			, co.name
			, co.type__c
			, co.company_name__c
			, ca.order__c
			, o.type
			, ca.accountid
			, a.name
			, a.company_name__c
			, ca.opportunity__c
			, opp.name
			, opp.grand_total__c
			-- , ca.segment					-250 / 250-500 / 500-1000 / 1000+
			-- , *
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE		ca.isclosed				= FALSE																					-- 1										
	AND	(																													-- 2
				u.name				NOT SIMILAR TO 	'%(Accounting|TOShared|Marketing|BAT B2B Admin Queue)%'
			AND u.name				NOT SIMILAR TO 	'%(Nicolai|Bätcher|Kharoo|Haferkorn|Heumer|Ahlers|Ribeiro|Klonaris|Kiekebusch|Steven|André)%'
		)
	AND (	
	
	-- old case setup
			(	(
				ca.origin			SIMILAR TO 		'%(B2B - Contact|B2B de - Customer dashboard|B2B DE|B2B CH|TFS CM)%' 	-- 3
			AND ca.type				NOT SIMILAR TO	'%(CLM HR|CLM|CM B2C|Sales|PM)%' 										-- 11
				)
			OR	(
				ca.origin			SIMILAR TO		'%(CM|Insurance|checkout|partner portal)%'								-- 4
			AND ca.type				SIMILAR TO		'%(KA|B2B)%'															-- 5
				)
			)
			
	-- new case setup
		OR	( 	(
				ca.origin			SIMILAR TO		'%(B2B customer)%'														-- 7 
			AND ca.type				SIMILAR TO		'%(CM B2B|Pool)%'														-- 9
				)
			OR 	(
				ca.origin			NOT SIMILAR TO	'%(B2B customer)%'														-- 8
			AND	ca.type				SIMILAR TO		'%(CM B2B)%'															-- 10
				)
			)							
		)
