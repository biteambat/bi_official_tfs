SELECT		
			TO_CHAR (CURRENT_DATE,'YYYY-WW') 		AS date_part
			, MIN 	(CURRENT_DATE::date) 			AS date
			, CAST 	('-' 			AS varchar) 	AS locale
			, cases.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)		AS type
			, CAST	('Open Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)		AS sub_kpi_1
			, cases.case_status						AS sub_kpi_2
			, cases.case_reason						AS sub_kpi_3
			, cases.case_reason						AS sub_kpi_4
			, cases.case_reason						AS sub_kpi_5
			
			, COUNT(*)								AS value

FROM


(
SELECT 		ca.sfid							case_ID
			, ca.casenumber					case_number
			, ca.isclosed					case_isclosed
			, ca.ownerid					case_ownerid
			, u.name						case_owner
			, ca.origin						case_origin
			, ca.type						case_type
			, ca.reason						case_reason
			, ca.status						case_status
			, ca.contactid					contactid
			, co.name						contact_name
			, co.type__c					contact_type
			, co.company_name__c			contact_companyname
			, ca.order__c					orderid
			, o.type						order_type
			, ca.accountid					professionalid
			, a.name						professional
			, a.company_name__c				professional_companyname
			, ca.opportunity__c				opportunityid
			, opp.name						opportunity
			, opp.grand_total__c			grand_total
			--, ca.segment					-250 / 250-500 / 500-1000 / 1000+
			--, *
			
FROM 		salesforce.case 		ca
LEFT JOIN	salesforce.user			u 		ON ca.ownerid 			= u.sfid
LEFT JOIN	salesforce.opportunity	opp 	ON ca.opportunity__c 	= opp.sfid
LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid
LEFT JOIN 	salesforce.account		a 		ON ca.accountid			= a.sfid
LEFT JOIN	salesforce.order 		o 		ON ca.order__c			= o.sfid 

-- 1 AND 2 AND (((3 AND 11) OR (4 AND 5)) OR ((7 AND 9) OR (8 AND 10)))

WHERE		ca.isclosed				= FALSE																					-- 1										
	AND((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
				WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
				WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
				WHEN 	u.name 		LIKE 	'%André%' 				THEN 1 ELSE 0 END) = 0 									--2				
		)
	AND (	
	
	-- old case setup (3 AND NOT 11)
			(
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B - Contact%'	THEN 1
						WHEN 	ca.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
						WHEN 	ca.origin	LIKE 'B2B DE%' 			THEN 1
						WHEN 	ca.origin 	LIKE 'B2B CH%'			THEN 1
						WHEN 	ca.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CLM HR'				THEN 1
						WHEN 	ca.type 	= 'CLM'					THEN 1
						WHEN 	ca.type		= 'CM B2C'				THEN 1
						WHEN 	ca.type		= 'Sales'				THEN 1
						WHEN 	ca.type		= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
			)
		OR	(
				(CASE 	WHEN 	ca.origin 	LIKE 'CM%'				THEN 1
						WHEN 	ca.origin 	LIKE 'Insurance' 		THEN 1
						WHEN 	ca.origin	LIKE '%checkout%' 		THEN 1
						WHEN 	ca.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
			AND	(CASE 	WHEN 	ca.type		= 'KA'					THEN 1
						WHEN 	ca.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
			)
			
	-- new case setup
		OR	(	
				(CASE 	WHEN 	ca.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
			AND (CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1
						WHEN 	ca.type 	= 'Pool'				THEN 1 ELSE 0 END) = 1									-- 11
			)
			
		OR 	(
				(CASE 	WHEN 	ca.type		= 'CM B2B'				THEN 1 ELSE 0 END) = 1
			)
--		OR (u.department LIKE 'CM'	AND ca.type <> 'CM B2C')
		)		
--		AND ca.casenumber = '00517627'

) AS cases



GROUP BY 	case_origin
			, cases.case_status	
			, cases.case_reason
;

	
-- rebuild salforce report: https://eu7.salesforce.com/00O0J0000074D80
-- Div 25 cases (sf: 505; query: 480)