SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Reopened Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
		
					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Reopened'
			AND hi.createddate::date >= '2018-11-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Closed Cases' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
		
					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Status'
			AND hi.newvalue LIKE 'Closed'
			AND hi.createddate::date >= '2018-11-01'

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;

SELECT		
			TO_CHAR ((reopened.date::date),'YYYY-WW') 	AS date_part
			, MIN 	(reopened.date::date)				AS date
			, CAST 	('-' 			AS varchar) 		AS locale
			, reopened.case_origin						AS origin
			, CAST 	('B2B'			AS varchar)			AS type
			, CAST	('Cases Type change' 	AS varchar)		AS kpi
			, CAST 	('Count'		AS varchar)			AS sub_kpi_1
			, reopened.case_status						AS sub_kpi_2
			, reopened.case_reason						AS sub_kpi_3			
			, CASE 	WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NULL 		THEN 'unknown'
					WHEN reopened.grand_total IS NULL	AND reopened.opportunityid IS NOT NULL 	THEN 'PPH'
					WHEN reopened.grand_total < 250 											THEN '<250€'
		  			WHEN reopened.grand_total >= 250 	AND reopened.grand_total < 500 			THEN '250€-500€'
		 			WHEN reopened.grand_total >= 500 	AND reopened.grand_total < 1000 		THEN '500€-1000€'
		  																						ELSE '>1000€'		END AS sub_kpi_4
			, reopened.opportunity						AS sub_kpi_5
			, reopened.case_owner						AS sub_kpi_6
			, CAST 	('-' 			AS varchar)			AS sub_kpi_7
			, reopened.opportunityid					AS sub_kpi_8
			, CAST 	('-' 			AS varchar)			AS sub_kpi_9
			, CAST 	('-' 			AS varchar)			AS sub_kpi_10			
			, COUNT(*)									AS value


FROM 		(


SELECT 		hi.createddate::date 			AS date
			, *
			-- , field
FROM salesforce.casehistory hi

INNER JOIN 	(SELECT 	cas.sfid						case_ID
						, cas.casenumber				case_number
						, cas.createddate				case_createddate
						, cas.isclosed					case_isclosed
						, cas.ownerid					case_ownerid
						, u.name						case_owner
						, cas.origin					case_origin
						, cas.type						case_type
						, cas.reason					case_reason
						, cas.status					case_status
						, cas.contactid					contactid
						, co.name						contact_name
						, co.type__c					contact_type
						, co.company_name__c			contact_companyname
						, cas.order__c					orderid
						, o.type						order_type
						, cas.accountid					professionalid
						, a.name						professional
						, a.company_name__c				professional_companyname
						, cas.opportunity__c			opportunityid
						, opp.name						opportunity
						, opp.grand_total__c			grand_total
			FROM salesforce.case 				cas
			LEFT JOIN	salesforce.user			u 		ON cas.ownerid 			= u.sfid
			LEFT JOIN	salesforce.opportunity	opp 	ON cas.opportunity__c 	= opp.sfid
			LEFT JOIN 	salesforce.contact		co 		ON cas.contactid 		= co.sfid
			LEFT JOIN 	salesforce.account		a 		ON cas.accountid		= a.sfid
			LEFT JOIN	salesforce.order 		o 		ON cas.order__c			= o.sfid 

			WHERE
		-- just CM B2B
		--	excluded case owner	
				((CASE	WHEN 	u.name 		LIKE 	'%Accounting%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%TOShared%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Marketing%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%BAT B2B Admin Queue%' THEN 1 
						WHEN 	u.name 		LIKE 	'%Nicolai%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Bätcher%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kharoo%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Haferkorn%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Heumer%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ahlers%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%Ribeiro%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Klonaris%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Kiekebusch%' 			THEN 1 
						WHEN 	u.name 		LIKE 	'%Steven%' 				THEN 1 
						WHEN 	u.name 		LIKE 	'%André_Wagner%' 				THEN 1 ELSE 0 END) = 0 									--2				
				)
			AND (	
			
			-- old case setup (3 AND NOT 11)
					(
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B - Contact%'	THEN 1
								WHEN 	cas.origin 	LIKE 'B2B de - Customer dashboard%' THEN 1
								WHEN 	cas.origin	LIKE 'B2B DE%' 			THEN 1
								WHEN 	cas.origin 	LIKE 'B2B CH%'			THEN 1
								WHEN 	cas.origin 	LIKE 'TFS CM%' 			THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CLM HR'				THEN 1
								WHEN 	cas.type 	= 'CLM'					THEN 1
								WHEN 	cas.type	= 'CM B2C'				THEN 1
								WHEN 	cas.type	= 'Sales'				THEN 1
								WHEN 	cas.type	= 'PM'					THEN 1 ELSE 0 END) = 0									-- 11
					)
				OR	(
						(CASE 	WHEN 	cas.origin 	LIKE 'CM%'				THEN 1
								WHEN 	cas.origin 	LIKE 'Insurance' 		THEN 1
								WHEN 	cas.origin	LIKE '%checkout%' 		THEN 1
								WHEN 	cas.origin 	LIKE '%partner portal' 	THEN 1 ELSE 0 END) = 1									-- 4
					AND	(CASE 	WHEN 	cas.type	= 'KA'					THEN 1
								WHEN 	cas.type 	= 'B2B'					THEN 1 ELSE 0 END) = 1									-- 5
					)
					
			-- new case setup
				OR	(	
						(CASE 	WHEN 	cas.origin 	LIKE 'B2B customer%'	THEN 1 ELSE 0 END) = 1									-- 3
					AND (CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'Pool'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1									-- 11
					)
					
				OR 	(
						(CASE 	WHEN 	cas.type	= 'CM B2B'				THEN 1
								WHEN 	cas.type 	= 'TFS - CM'			THEN 1 ELSE 0 END) = 1
					)
				)	
		
					) 						ca 		ON hi.caseid			= ca.case_ID			

WHERE		
	-- reopened cases
			hi.field = 'Type'
			AND hi.newvalue LIKE 'CM B2B'
			AND (hi.oldvalue NOT LIKE 'B2B' OR hi.oldvalue NOT LIKE 'KA')
			AND hi.createddate::date >= '2018-11-22'
			AND (ca.case_origin NOT LIKE 'B2B customer%' OR ca.case_origin NOT LIKE 'CM - Team')

			
) AS reopened

GROUP BY 	reopened.date
			, reopened.case_origin
			, reopened.case_status	
			, reopened.case_reason
			, reopened.grand_total
			, reopened.opportunityid
			, reopened.opportunity	
			, reopened.case_owner			
;
