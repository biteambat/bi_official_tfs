SELECT 		TO_CHAR (n.call_start_date_time__c::date,'YYYY-WW') 		AS date_part
			, MIN 	(n.call_start_date_time__c::date) 					AS date
			, CAST 	('-' 			AS varchar) 						AS locale
			, n.e164callednumber__c										AS origin -- NEW
			, CAST 	('B2B'			AS varchar)							AS type
			, CAST	('Inbound Calls'AS varchar)							AS kpi
			, CAST 	('Count'		AS varchar)							AS sub_kpi_1
			, n.callconnectedcheckbox__c 								AS sub_kpi_2
			, n.wrapup_string_1__c										AS sub_kpi_3
			, n.number_not_in_salesforce__c								AS sub_kpi_4
			, CASE 	WHEN n.relatedcontact__c	IS NOT NULL THEN co.name 
			ELSE a.name END	
			
			
			
			AS sub_kpi_5
			, u.name													AS sub_kpi_6
			, CAST 	('-' 			AS varchar)		AS sub_kpi_7
			, CASE 	WHEN n.relatedcontact__c IS NOT NULL THEN co.sfid
					WHEN n.account__c		 IS NOT NULL THEN a.sfid
					WHEN n.lead__c			 IS NOT NULL THEN l.sfid
					ELSE 'unknown' END						AS sub_kpi_8
			, CASE WHEN co.name	IS NULL THEN a.type__c ELSE co.type__c END	AS sub_kpi_9
			, CAST 	('-' 			AS varchar)							AS sub_kpi_10			
			
			, COUNT (*)
--			, *
			
FROM 		salesforce.natterbox_call_reporting_object__c 	n
LEFT JOIN	salesforce.user									u 		ON n.ownerid 			= u.sfid
LEFT JOIN 	salesforce.contact								co 		ON n.relatedcontact__c 	= co.sfid
LEFT JOIN 	salesforce.account								a 		ON n.account__c			= a.sfid
LEFT JOIN 	salesforce.lead									l 		ON n.lead__c			= l.sfid

WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c IN ('493030807264', '41435084849')
GROUP BY 	n.call_start_date_time__c::date
			, n.e164callednumber__c
			, n.callconnectedcheckbox__c
			, n.wrapup_string_1__c
			, n.hangupcause__c
			, co.name
			, u.name
			, n.number_not_in_salesforce__c
			, co.sfid
			, a.sfid
			, co.type__c
			, a.name
			, a.type__c
			, n.relatedcontact__c
			, n.account__c
			, n.lead__c
			, l.sfid
;


SELECT 		n.sfid
	--		, n.owner__c
	--		, n.ownerid
	--		, n.connnected_user_full_name__c
			, n.relatedcontact__c
			, n.callconnectedcheckbox__c
	--		, n.connectedgroup__c -- missing 
			, n.account__c
			, n.relatedcontact__c
			, n.lead__c
			, n.number_not_in_salesforce__c
	--		, n.name -- sfid 15 dig
			
			, n.*
			
FROM 		salesforce.natterbox_call_reporting_object__c n
WHERE 		calldirection__c = 'Inbound'
			AND e164callednumber__c = '493030807264'
			AND account__c IS NULL
			AND relatedcontact__c IS NULL
			AND number_not_in_salesforce__c = 'FALSE'


;

SELECT		*
FROM 		bi.cm_cases
LIMIT		50
;