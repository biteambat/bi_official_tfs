SELECT

	t3.year,
	t3.week,
	min(t3.mindate) as min_start_date,
	t3.acquisition_channel,
	CASE WHEN t3.Day_of_week in ('0','6') or (t3.Day_of_week in ('0','6') and t3.Hour_of_day between 9 and 17) THEN 'Out of Working Hours' ELSE 'Working Hours' ENd as Creation_Type,
	AVG(t4.time_to_firstcall) as average_time_to_firstcall,
	Min(t4.time_to_firstcall) as min_time_to_firstcall,
	max(t4.time_to_firstcall) as max_time_to_firstcall,
	AVG(t3.time_to_connection) as average_time_to_connection,
	AVG(t5.time_to_firstcall_after_assignment) as average_time_to_firstcall_after_assignment,
	Min(t5.time_to_firstcall_after_assignment) as min_time_to_firstcall_after_assignment,
	max(t5.time_to_firstcall_after_assignment) as max_time_to_firstcall_after_assignment
	
FROM

	(SELECT
	
	EXTRACT(YEAR FROM t2.createddate) as year,
	EXTRACT(WEEK FROM t2.createddate) as week,
	EXTRACT(DOW FROM t2.createddate::date) as Day_Of_week,
	EXTRACT(HOUR FROm t2.createddate::timestamp + INTERVAL '2 Hours') as Hour_of_day,
	min(t2.createddate::date) as mindate,
	t2.sfid,
	-- COUNT(DISTINCT t2.sfid) as leads_created,
		(CASE WHEN t2.createddate::date > '2017-03-26' THEN t2.createddate::timestamp + INTERVAL '2 Hours' ELSE t2.createddate::timestamp + INTERVAL '1 Hour' END) as lead_Creation_Date,
		min(t1.call_start_date_time__c::timestamp) as First_Call_To_Likeli,
	CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'inbound' ELSE 'outbound' END as acquisition_channel,
	MIN(t1.call_start_date_time__c::timestamp - (CASE WHEN t2.createddate::date > '2017-03-26' THEN t2.createddate::timestamp + INTERVAL '2 Hours' ELSE t2.createddate::timestamp + INTERVAL '1 Hour' END)) as time_to_connection
	
	FROM
	
	bi.b2b_likelies t2
	
	
	LEFT JOIN
	
	salesforce.natterbox_call_reporting_object__c t1
	
	ON 
	
	RIGHT(t1.other_id__c, 18) = t2.sfid
	
	WHERE
	
	t1.calldirection__c LIKE '%Outbound%'
	AND t1.callconnected__c LIKE '%Yes%'
	AND t2.sfid IS NOT NULL
	and t2.createddate::date >= '2017-01-01'
	AND t2.acquisition_channel__c IN ('web', 'inbound') 
	GROUP BY
	
	year,
	week,
	(CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'inbound' ELSE 'outbound' END),
	t2.sfid,
	lead_creation_date,
	hour_of_day,
	day_of_week
	HAVING
			(MIN(t1.call_start_date_time__c::timestamp - (CASE WHEN t2.createddate::date > '2017-03-26' THEN t2.createddate::timestamp + INTERVAL '2 Hours' ELSE t2.createddate::timestamp + INTERVAL '1 Hour' END))) > '0'
	
	ORDER BY
	
	year desc,
	week desc) t3
	
LEFT JOIN


	(SELECT

		EXTRACT(YEAR FROM t2.createddate) as year,
		EXTRACT(WEEK FROM t2.createddate) as week,
		EXTRACT(DOW FROM t2.createddate::date) as Day_of_week,
		EXTRACT(HOUR FROm t2.createddate::timestamp + INTERVAL '2 Hours') as Hour_of_day,
		min(t2.createddate::date) as mindate,
		t2.sfid,
		t2.createddate::timestamp + INTERVAL '2 Hours' as lead_Creation_Date,
		min(t1.call_start_date_time__c::timestamp) as First_Call_To_Likeli,
		-- COUNT(DISTINCT t2.sfid) as leads_created,
		CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'inbound' ELSE 'outbound' END as acquisition_channel,
		MIN(t1.call_start_date_time__c::timestamp - (t2.createddate::timestamp + INTERVAL '2 Hours' )) as time_to_firstcall
		
		FROM
		
		bi.b2b_likelies t2
		
		
		LEFT JOIN
		
		salesforce.natterbox_call_reporting_object__c t1
		
		ON 
		
		RIGHT(t1.other_id__c, 18) = t2.sfid
		
		
		WHERE
		
		t1.calldirection__c LIKE '%Outbound%'
		AND t2.sfid IS NOT NULL
		and t2.createddate::date >= '2017-01-01'
		AND t2.acquisition_channel__c IN ('web', 'inbound') 
		GROUP BY
		
		year,
		week,
		t2.createddate::timestamp,
		day_of_week,
		(CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'inbound' ELSE 'outbound' END),
		t2.sfid
		HAVING
		(MIN(t1.call_start_date_time__c::timestamp - (t2.createddate::timestamp + INTERVAL '2 Hours' ))) > '0'
		
		
		ORDER BY
		
		year desc,
		week desc) t4
	
ON

	t3.sfid = t4.sfid	
	
-- ---------------------------------------------------------------- added likeli history for the first assigned date from API Tiger or MC Tiger to any other user
	
LEFT JOIN

		(SELECT
		MIN(t1.createddate) AS first_assigned_date,
		t1.parentid as likelieid,
		MIN(t2.call_start_date_time__c::timestamp - (t1.createddate::timestamp + INTERVAL '2 Hours' )) AS time_to_firstcall_after_assignment
		
		FROM 
			salesforce.likeli__history t1
			
		LEFT JOIN
		
		salesforce.natterbox_call_reporting_object__c t2
		
		ON 
		
		RIGHT(t2.other_id__c, 18) = t1.parentid
		
		
		WHERE 
			EXTRACT(YEAR FROM t1.createddate) = '2018'
			AND t1.field = 'Owner'
			AND (t1.oldvalue = '00520000003IiNCAA0' OR t1.oldvalue = '00520000004n1OxAAI')
		
		GROUP BY
		likelieid
		
		HAVING
		(MIN(t2.call_start_date_time__c::timestamp - (t1.createddate::timestamp + INTERVAL '2 Hours' ))) > '0'
		
		) AS t5
		
		ON t3.sfid = t5.likelieid	
		

-- ------------------------------------------------------------------------------------------------------------	
	
GROUP BY

	t3.year,
	t3.week,
	creation_type,
	t3.acquisition_channel
	
ORDER BY

	year desc,
	week desc