DROP TABLE IF EXISTS bi.b2b_additional_booking;
CREATE TABLE bi.b2b_additional_booking 
(opportunity 			TEXT
, closed_date 			DATE 
, closedby 				TEXT
, type 					CHAR(50)
, acquisition_type		CHAR(50)
, acquisition_channel 	CHAR(50) 
, package 				CHAR(50)
, revenue 				NUMERIC
, hours 				NUMERIC); 

SELECT*
FROM bi.b2b_additional_booking


INSERT INTO bi.b2b_additional_booking
(opportunity
, closed_date
, closedby
, type
, acquisition_type
, acquisition_channel
, package
, revenue
--, hours
)
VALUES
('0060J00000oPI3ZQAW','2017-06-12','0050J0000074nTEQAY','one-off','additional','web','upgrade','144.33')