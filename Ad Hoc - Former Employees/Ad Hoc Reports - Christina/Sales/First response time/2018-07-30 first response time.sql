-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Sales First Response Time

DROP TABLE IF EXISTS bi.sales_firstresponsetime;
CREATE TABLE bi.sales_firstresponsetime as 

SELECT
t1.*,

-- ---------------------------------------------------------------------------------- difference: owner change -> first contact		

-- DATE_PART 	('day', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 24 +
-- DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 60 +
-- DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) 										AS First_Response_Time_Minutes_old,

-- -
-- - working h Mo-Fr 09:00:00 - 18:00:00 
-- - summertime timezone difference of 2 h

CASE 
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau
	WHEN t1.first_contact::timestamp < t1.ownerchange_date::timestamp
	THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
	WHEN t1.first_contact::date = t1.ownerchange_date::date
	THEN
		DATE_PART 	('day', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 24 +
		DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 60 +
		DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp)
	
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
	ELSE
	
	-- minutes pased on the owner change date until end of work
		(CASE  WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 0 
		WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 0
		ELSE	
		DATE_PART 	('hour', TIME '16:00:00.0' - t1.ownerchange_date::time) * 60 +
		DATE_PART 	('minute', TIME '16:00:00.0' - t1.ownerchange_date::time) END)
		+
	
	-- minutes pased on the first contact date beginning at the morning working time
		DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' ) * 60 +
		DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
		+
	
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
		(CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END)
			
END AS First_Response_Time_Minutes,

-- ------------
-- FRT details

-- weekday
date_part('dow', t1.ownerchange_date::date) AS weekday,

-- minutes dame day
CASE 	WHEN t1.first_contact::date = t1.ownerchange_date::date
		THEN
		DATE_PART 	('day', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 24 +
		DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 60 +
		DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp)
		ELSE NULL END AS FRT_sameday,

-- minutes until workday end

CASE  WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 0 
		WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 0
		ELSE
		DATE_PART 	('hour', TIME '16:00:00.0' - t1.ownerchange_date::time) * 60 +
		DATE_PART 	('minute', TIME '16:00:00.0' - t1.ownerchange_date::time) END AS FRT_afternoon,
		
-- minutes pased on the first contact date beginning at the morning working time

		DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' ) * 60 +
		DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0') AS FRT_morning,		

-- day difference - weekend
CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END AS FRT_daydif,


-- ---------------------------------------------------------------------------------- difference: created date -> owner change		
-- -
-- - working h Mo-Fr 09:00:00 - 18:00:00 
-- - summertime timezone difference of 2 h

CASE 

-- if likeli wasn't assigned to an agent use -1 to filter in tableau
	WHEN t1.ownerchange_date::timestamp IS NULL THEN -1

-- if the created date was after the owner change date just use the value "-1" -> this will be used as a filter in tableau
-- WHEN t1.ownerchange_date::timestamp < t1.created_date::timestamp
-- THEN -1

-- if created date and owner change date on the same day, calculate difference between times in minutes
	WHEN t1.ownerchange_date::date = t1.created_date::date
	THEN
		DATE_PART 	('day', t1.ownerchange_date::timestamp - t1.created_date::timestamp) * 24 +
		DATE_PART 	('hour', t1.ownerchange_date::timestamp - t1.created_date::timestamp) * 60 +
		DATE_PART 	('minute', t1.ownerchange_date::timestamp - t1.created_date::timestamp)
	
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
	ELSE
	
	-- minutes pased on the created date until end of work day
	-- in case likeli was created outside business hours use 0 as value
		(CASE WHEN 
		(TIME '16:00:00.0' < t1.created_date::time )
		THEN 0
		ELSE (DATE_PART 	('hour', TIME '16:00:00.0' - t1.created_date::time) * 60 +
		DATE_PART 	('minute', TIME '16:00:00.0' - t1.created_date::time)) END)
		+
	
	-- minutes pased on the owner change date beginning at the morning working time
		DATE_PART 	('hour', t1.ownerchange_date::time - TIME '07:00:00.0' ) * 60 +
		DATE_PART 	('minute', t1.ownerchange_date::time - TIME '07:00:00.0')
		+
	
	-- create a list of all date incl. created date and owner change date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: created date, owner change date -> minutes are calculated separatly
		(CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.created_date::date, t1.ownerchange_date::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.created_date::date, t1.ownerchange_date::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END)
				
END AS Assign_to_Agent_Minutes,

-- Likeli created outside of working hours
CASE WHEN TIME '16:00:00.0' < t1.created_date::time THEN 1 ELSE 0 END AS created_outside_working_h 


FROM
(SELECT
sub1.parentid AS LikelieID,
likeli.contact_name__c AS Name,
likeli.locale__c AS locale,
likeli.billingaddress_city__c AS city,
likeli.acquisition_channel__c AS aquisition_channel,
likeli.acquisition_tracking_id__c AS aquisition_trackingId,
likeli.stage__c AS stage,
likeli.lost_reason__c AS lost_reason,

CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
		WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
		ELSE 'Agent' END AS createdby,
sub1.createddate AS created_date,
-- CASE WHEN sub1.

CASE 	WHEN t2.ownerchange_date IS NOT NULL THEN t2.ownerchange_date
		WHEN t2.ownerchange_date IS NULL 
			AND (CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
							WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
							ELSE 'Agent' END) <> 'API Tiger' THEN sub1.createddate
		ELSE NULL END AS ownerchange_date,
		
-- t2.ownerchange_date AS ownerchange_date_old,
		
CASE 	WHEN t2.Agent IS NOT NULL THEN t2.Agent
		WHEN t2.Agent IS NULL 
			AND (CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
							WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
							ELSE 'Agent' END) <> 'API Tiger' THEN likeli.ownername
		ELSE NULL END AS Agent,
		
-- t2.Agent AS Agent_old,

t3.stage_not_reached_date AS stage_not_reached_date,
t4.stage_ended_date AS stage_ended_date,
t5.stage_reached_date AS stage_reached_date,
t6.stage_dm_reached_date AS stage_dm_reached_date,
t7.stage_qualified_date AS stage_qualified_date,


-- ---------------------------------------------------------------------------------- first contacts

LEAST 
		(t3.stage_not_reached_date,
		t4.stage_ended_date,
		t5.stage_reached_date,
		t6.stage_dm_reached_date,
		t7.stage_qualified_date) AS first_contact
		
		


FROM 
salesforce.likeli__history sub1

LEFT JOIN
			(
			SELECT
			sublikeli.sfid AS userid,
			subuser.name AS ownername,
			*
			FROM
			salesforce.likeli__c AS sublikeli
			
			LEFT JOIN	
				salesforce.user AS subuser
				ON (sublikeli.ownerid = subuser.sfid)
			
			) AS likeli
				
				
			ON
				(sub1.parentid = likeli.userid)
			

-- ---------------------------------------------------------------------------------- likelihistory - owner-change

LEFT JOIN
		(SELECT
		sub2.sfid,
		sub2.parentid,
		sub2.createddate AS ownerchange_date,
		CASE WHEN sub3.name IS NULL THEN sub2.newvalue ELSE sub3.name END AS Agent
		
		FROM 
		salesforce.likeli__history sub2
		
			LEFT JOIN
			salesforce.user sub3
			ON
				(sub2.newvalue = sub3.sfid)
		
		WHERE
		sub2.field = 'Owner'
		) AS t2
		
		ON (sub1.parentid = t2.parentid) 
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - NOT REACHED

LEFT JOIN
		(SELECT
		sub4.sfid,
		sub4.parentid,
		sub4.oldvalue,
		sub4.createddate AS stage_not_reached_date
		
		FROM 
		salesforce.likeli__history sub4
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'NOT REACHED'
		) AS t3
		
		ON (sub1.parentid = t3.parentid)
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - ENDED		

LEFT JOIN		
		(SELECT
		sub5.sfid,
		sub5.parentid,
		sub5.createddate AS stage_ended_date
		
		FROM 
		salesforce.likeli__history sub5
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'ENDED'
		) AS t4
		
		ON (sub1.parentid = t4.parentid)		
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - REACHED		

LEFT JOIN
		(SELECT
		sub6.sfid,
		sub6.parentid,
		sub6.createddate AS stage_reached_date
		
		FROM 
		salesforce.likeli__history sub6
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'REACHED'
		) AS t5

		ON ( sub1.parentid = t5.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - DM REACHED		

LEFT JOIN
		(SELECT
		sub7.sfid,
		sub7.parentid,
		sub7.createddate AS stage_dm_reached_date
		
		FROM 
		salesforce.likeli__history sub7
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'DM REACHED'
		) AS t6

		ON ( sub1.parentid = t6.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - QUALIFIED		

LEFT JOIN
		(SELECT
		sub8.sfid,
		sub8.parentid,
		sub8.createddate AS stage_qualified_date
		
		FROM 
		salesforce.likeli__history sub8
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'QUALIFIED'
		) AS t7

		ON ( sub1.parentid = t7.parentid)
		
				
WHERE
sub1.field = 'created'
AND sub1.createddate::date > '2017-06-01'
AND likeli.acquisition_channel__c <> 'outbound'
AND likeli.type__c <> 'B2C'
AND likeli.test__c = 'false'
AND likeli.ownerid NOT LIKE '00520000004n1OxAAI'



GROUP BY
sub1.parentid,
likeli.contact_name__c,
likeli.locale__c,
likeli.billingaddress_city__c,
likeli.acquisition_channel__c,
likeli.acquisition_tracking_id__c,
likeli.stage__c,
likeli.lost_reason__c,
likeli.ownername,

likeli.createdbyid,
sub1.createddate,

t2.ownerchange_date,
t2.Agent,

t3.stage_not_reached_date,
t4.stage_ended_date,
t5.stage_reached_date,
t6.stage_dm_reached_date,
t7.stage_qualified_date
) AS t1

ORDER BY 
	created_date desc
;