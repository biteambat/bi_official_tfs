SELECT
sub1.parentid AS LikelieID,
likeli.contact_name__c AS Name,
likeli.locale__c AS locale,
likeli.billingaddress_city__c AS city,
likeli.acquisition_channel__c AS aquisition_channel,
likeli.acquisition_tracking_id__c AS aquisition_trackingId,
likeli.stage__c AS stage,
likeli.lost_reason__c AS lost_reason,

sub1.createddate AS created_date,

t2.ownerchange_date,
t2.Agent,

t3.stage_not_reached_date,
t4.stage_ended_date,
t5.stage_reached_date,
t6.stage_dm_reached_date,
t7.stage_qualified_date

FROM 
salesforce.likeli__history sub1

LEFT JOIN
			salesforce.likeli__c AS likeli
			ON
				(sub1.parentid = likeli.sfid)

-- ---------------------------------------------------------------------------------- likelihistory - owner-change

LEFT JOIN
		(SELECT
		sub2.sfid,
		sub2.parentid,
		sub2.createddate AS ownerchange_date,
		CASE WHEN sub3.name IS NULL THEN sub2.newvalue ELSE sub3.name END AS Agent
		
		FROM 
		salesforce.likeli__history sub2
		
			LEFT JOIN
			salesforce.user sub3
			ON
				(sub2.newvalue = sub3.sfid)
		
		WHERE
		sub2.field = 'Owner'
		AND sub2.createddate::date > '2017-11-01'
		) AS t2
		
		ON (sub1.parentid = t2.parentid) 
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - NOT REACHED

LEFT JOIN
		(SELECT
		sub4.sfid,
		sub4.parentid,
		sub4.createddate AS stage_not_reached_date
		
		FROM 
		salesforce.likeli__history sub4
		
		WHERE
		field = 'stage__c'
		AND newvalue = 'NOT REACHED'
		AND sub4.createddate::date > '2017-11-01'
		) AS t3
		
		ON (sub1.parentid = t3.parentid)
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - ENDED		

LEFT JOIN		
		(SELECT
		sub5.sfid,
		sub5.parentid,
		sub5.createddate AS stage_ended_date
		
		FROM 
		salesforce.likeli__history sub5
		
		WHERE
		field = 'stage__c'
		AND newvalue = 'ENDED'
		AND sub5.createddate::date > '2017-11-01'
		) AS t4
		
		ON (sub1.parentid = t4.parentid)		
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - REACHED		

LEFT JOIN
		(SELECT
		sub6.sfid,
		sub6.parentid,
		sub6.createddate AS stage_reached_date
		
		FROM 
		salesforce.likeli__history sub6
		
		WHERE
		field = 'stage__c'
		AND newvalue = 'REACHED'
		AND sub6.createddate::date > '2017-11-01'
		) AS t5

		ON ( sub1.parentid = t5.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - DM REACHED		

LEFT JOIN
		(SELECT
		sub7.sfid,
		sub7.parentid,
		sub7.createddate AS stage_dm_reached_date
		
		FROM 
		salesforce.likeli__history sub7
		
		WHERE
		field = 'stage__c'
		AND newvalue = 'DM REACHED'
		AND sub7.createddate::date > '2017-11-01'
		) AS t6

		ON ( sub1.parentid = t6.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - QUALIFIED		

LEFT JOIN
		(SELECT
		sub8.sfid,
		sub8.parentid,
		sub8.createddate AS stage_qualified_date
		
		FROM 
		salesforce.likeli__history sub8
		
		WHERE
		field = 'stage__c'
		AND newvalue = 'QUALIFIED'
		AND sub8.createddate::date > '2017-11-01'
		) AS t7

		ON ( sub1.parentid = t7.parentid)
		
				
WHERE
sub1.field = 'created'
AND sub1.createddate::date > '2017-11-01'
AND likeli.acquisition_channel__c <> 'outbound'
AND likeli.type__c <> 'B2C'
AND likeli.test__c = 'false'