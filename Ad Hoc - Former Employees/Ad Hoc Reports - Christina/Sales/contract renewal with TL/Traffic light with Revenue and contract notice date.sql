SELECT
*
FROM
bi.opportunity_traffic_light_new traffic

LEFT JOIN
(
SELECT
sfid
,CASE	WHEN opp.grand_total__c 	IS NOT NULL THEN opp.grand_total__c
-- WHEN opp.amount 				IS NOT NULL THEN opp.amount
WHEN opp.hours_weekly__c 	IS NOT NULL THEN (opp.hours_weekly__c*opp.plan_pph__c*4.33)+(CASE WHEN pps__c IS NULL THEN 0 ELSE pps__c END)
WHEN opp.hours_weekly__c 	IS NULL 		THEN (2*opp.plan_pph__c*4.33)+(CASE WHEN pps__c IS NULL THEN 0 ELSE pps__c END)
ELSE 0 END AS Revenue
-- ,*
-- , contract.opportunity__c
, contract.notice_date__c
, contract.additional_agreements__c
, contract.promotion_terms__c
-- , locale__c
--
FROM
Salesforce.opportunity Opp
--
LEFT JOIN
(
SELECT
opportunity__c
, notice_date__c
, additional_agreements__c
, promotion_terms__c
-- , *
FROM
salesforce.contract__c
WHERE
status__c IN ('ACCEPTED', 'SIGNED')
AND test__c IS FALSE
) AS contract
ON (Opp.sfid = contract.opportunity__c)
--
WHERE
test__c = false
-- AND stagename IN ('WON', 'PENDING')
AND status__c IN ('ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'RETENTION')
--
) AS sales
ON (traffic.opportunity = sales.sfid)
	
	
WHERE date = CURRENT_DATE