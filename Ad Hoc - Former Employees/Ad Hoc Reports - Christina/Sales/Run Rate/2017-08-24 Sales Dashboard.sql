DROP TABLE IF EXISTS bi.salesreport_daily;
CREATE TABLE bi.salesreport_daily as 

-- Signed Deals - Actual
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Deals' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	ROUND(COUNT(1),2) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
-- Total Signed Deals - Actual

INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Deals' as Varchar) as kpi,
	CAST('Total' as varchar) as customer_type,
	CAST('' as varchar) as channel,
	CAST('' as varchar) as acquisition_type,
	ROUND(COUNT(1),2) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month;
	
-- Signed Revenue - Actual	
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Revenue' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	SUM(grand_total__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
-- Signed Hours - Actual		
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Hours' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	
	CASE WHEN LEFT(locale__c,2) ='de' THEN SUM(grand_total__c)/25.9 
	WHEN LEFT(locale__c,2) ='nl' THEN SUM(grand_total__c)/27.9 
	ELSE 0 END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;	
	
-- FTE Numbers - Actual

INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Actual','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Actual','FTE','KA','Sales','Inbound','1');

-- KPI Targets

-- Signed Deals - Target

INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Sales','Inbound','41');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Sales','Outbound','20');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','SME','Funnel','Inbound','22');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Signed Deals','KA','Sales','Outbound','4');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','KA','Sales','Inbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Signed Deals','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','SME','Sales','Inbound','5');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Signed Deals','KA','Sales','Outbound','1');

-- FTE - Target

INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','SME','Sales','Outbound','7');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','SME','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','SME','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','FTE','KA','Sales','Inbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','SME','Sales','Inbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','SME','Sales','Outbound','2');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','KA','Sales','Outbound','1');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','FTE','KA','Sales','Inbound','1');

-- Average Revenue - Target

INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Sales','Inbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Sales','Outbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','SME','Funnel','Inbound','370');
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average Revenue','KA','Sales','Outbound','1100');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','SME','Sales','Inbound','500');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','SME','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average Revenue','KA','Sales','Outbound','0');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','SME','Sales','Inbound','379');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','SME','Sales','Outbound','379');
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average Revenue','KA','Sales','Outbound','1100');

-- Average PPH - Target

INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','SME','Funnel','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('de','2017-08','Target','Average PPH','KA','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','SME','Sales','Inbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','SME','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('ch','2017-08','Target','Average PPH','KA','Sales','Outbound',38.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','SME','Sales','Inbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','SME','Sales','Outbound',25.90);
INSERT INTO bi.salesreport_daily VALUES ('nl','2017-08','Target','Average PPH','KA','Sales','Outbound',25.90);

-- Signed Revenue - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
sub1.value*sub2.value AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Deals')
	AND sub2.kpi in ('Average Revenue')
		AND sub1.sub_kpi in ('Target');	
	
-- Signed Hours - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Hours' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
sub1.value/sub2.value AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Average PPH')
	AND sub1.sub_kpi in ('Target');
	
-- Average PPH - Actual

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Average Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Signed Deals')
	AND sub1.sub_kpi in ('Actual');
	
-- Total Signed Deals - Target

INSERT INTO  bi.salesreport_daily	
SELECT
locale,
year_month, 
sub_kpi,
CAST('Signed Deals' as Varchar) as kpi,
CAST('Total' as varchar) as customer_type,
CAST('' as varchar) as channel,
CAST('' as varchar) as acquisition_type,
SUM(value)
FROM
	bi.salesreport_daily
WHERE
	kpi in ('Signed Deals')
	AND sub_kpi in ('Target')
GROUP BY
locale,
year_month, 
sub_kpi
;