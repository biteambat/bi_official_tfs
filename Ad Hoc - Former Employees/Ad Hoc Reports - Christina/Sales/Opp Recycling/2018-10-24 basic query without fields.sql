SELECT		opp.customer__c
			, opp.sfid
			, opp.acquisition_channel__c
			, opp.name
			, opp.email__c
			, opp.phone__c
			, lost.lost_date
			, lost.lost_reason
			, opp.ownerid
			, u.name 									AS opp_owner
			, opp.closedate
			, opp.locale__c
			, opp.office_size__c
			, opp.grand_total__c
			, opp.supplies__c
			, opp.office_rooms__c
			, opp.office_bathrooms__c 
			, opp.office_kitchens__c
			, opp.office_employees__c
			, opp.recurrency__c
			, opp.times_per_week__c
			, opp.hours_weekly__c
			, opp.shippingaddress_city__c
			, contract.createdbyid
			, contract.createdby				
			, contract.sent_date__c
			, contract.link_opened__c
			, contract.start__c
			, contract.duration__C
			, contract.promotion_id__c
			-- , *
FROM 		salesforce.opportunity 					opp

-- only opps related to customers which never had a WON opp
INNER JOIN 	(
			WITH customers AS (
				SELECT 		co.sfid 
							, STRING_AGG (op.stagename, ',' ORDER BY op.closedate::date) 	AS stages
				FROM 		salesforce.opportunity 	op
				LEFT JOIN	salesforce.contact 		co	ON op.customer__c = co.sfid
				GROUP BY	co.sfid
				)
			SELECT 			sfid 						AS Id
			FROM 			customers
			WHERE 			stages  					LIKE '%LOST%'
			AND 			stages 						NOT LIKE '%WON%'		
			) AS contacts								ON contacts.id = opp.customer__C

LEFT JOIN 	salesforce.user 						u 	ON u.sfid = opp.ownerid	

-- opportunity history for the lost_date and lost_reason
LEFT JOIN 	(
			SELECT DISTINCT ON (stage.opportunityid)
							stage.opportunityid
							, stage.createddate::date AS lost_date
							, stage.newvalue 		AS lost_reason
			FROM 			salesforce.opportunityfieldhistory stage
			WHERE			field 						IN ('lost_reason__c')
			GROUP BY 		stage.opportunityid
							, stage.newvalue
							, stage.createddate
			ORDER BY 		stage.opportunityid, 
							stage.createddate 		DESC
			) AS lost 									ON lost.opportunityid = opp.sfid

-- contract infortmation if contract link was opened
LEFT JOIN 	(
			SELECT DISTINCT ON (contracts.opportunity__c)
							contracts.opportunity__c
							, contracts.createdbyid
							, u.name					AS createdby -- created by USER
							, contracts.sent_date__c
							, contracts.link_opened__c
							, contracts.start__c
							, contracts.duration__C
							, contracts.promotion_id__c
							-- , *
			FROM			salesforce.contract__c 	contracts
			LEFT JOIN 		salesforce.user 		u	ON u.sfid = contracts.createdbyid -- created by 
			WHERE 			link_opened__c 				IS NOT NULL
			ORDER BY 		contracts.opportunity__c, 
							contracts.sent_date__c 	DESC
			) AS contract 								ON contract.opportunity__c = opp.sfid