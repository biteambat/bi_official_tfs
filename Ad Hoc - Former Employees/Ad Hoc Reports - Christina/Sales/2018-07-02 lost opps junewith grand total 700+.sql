SELECT
Opp.sfid,
Opp.name AS opportunity_name,
owner.name AS opportunity_owner,
Opp.stagename AS stage,
Opp.lost_reason__c AS lost_reason,
Opp.grand_total__c AS grand_total,
Stage.versions AS offers_sent
		
FROM 

	salesforce.opportunity Opp

INNER JOIN	
		
		(SELECT
		opportunityid,
		COUNT( opportunityid) AS versions
		
		FROM
		
			salesforce.opportunityfieldhistory
			
		WHERE
			
			field IN ('StageName')
			AND newvalue IN ('CONTRACT SEND OUT', 'OFFER SENT')
			AND EXTRACT(YEAR FROM createddate::date) = 2018
			AND EXTRACT(MONTH FROM createddate::date) = 6
			
		GROUP BY
		
		opportunityid
			
		) AS Stage
		
		ON Stage.opportunityid = Opp.sfid
		
LEFT JOIN
	
		salesforce.user AS owner
		
	ON 
	
		Opp.ownerid = owner.sfid

			
WHERE 

	test__c = false
	AND stagename IN ('LOST')
	AND grand_total__c >= 700
	
	
ORDER BY grand_total__c DESC