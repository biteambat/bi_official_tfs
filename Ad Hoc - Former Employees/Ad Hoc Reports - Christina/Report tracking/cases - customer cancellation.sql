SELECT
CURRENT_DATE AS date
, CAST('Customer Cancellation' as varchar) AS report
, cancellation.agentname
, cancellation.status
, COUNT(cancellation.sfid)
FROM
(
SELECT
opp.name
, cases.casenumber
, cases.subject
, cases.status AS status
, cases.createddate
, opp.grand_total__c
, cases.reason
, cases.type
, agent.name AS agentname
, cases.sfid AS sfid
-- ,*

FROM
salesforce.case cases

	LEFT JOIN 
		salesforce.user agent
	ON 
	cases.ownerid = agent.sfid
	
	LEFT JOIN 
		salesforce.opportunity opp
	ON 
	cases.opportunity__c = opp.sfid

WHERE
cases.isclosed = FALSE
AND cases.type NOT IN ('CLM HR','CLM','CM B2C','Sales','PM')
AND cases.reason IN (	'Customer Cancellation', 
				'Customer - Retention',
				'Customer - Retention budget',
				'Customer - Retention change in personnel',
				'Customer - Retention failure/neglect',
				'Customer - Retention not enough hours booked',
				'Customer - Retention other',
				'Customer - Retention own solution',
				'Customer - Retention professional no show',
				'Customer - Retention quality')
) AS cancellation

GROUP BY
cancellation.agentname
, cancellation.status

-- LIMIT 10