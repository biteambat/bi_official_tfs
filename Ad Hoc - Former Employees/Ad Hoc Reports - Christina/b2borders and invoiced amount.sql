SELECT
t1.locale__c AS locale,
t1.year_month AS Month,
t1.opportunity_id AS Opportunity,
t1.name AS Name,
TO_CHAR(t1.first_order_date,'YYYY-MM') AS First_Order_Date,
SUM(t2.Invoiced_Amount) AS Invoiced_Amount

FROM 
bi.b2borders  AS t1

LEFT JOIN
	(SELECT
		amount__c AS Invoiced_Amount,
		opportunity__c AS Opportunity,
		TO_CHAR(issued__c,'YYYY-MM') AS Month
	FROM
	salesforce.invoice__c) AS t2
	
	ON (t1.opportunity_id = t2.Opportunity AND t1.year_month = t2.Month)
	
GROUP BY
t1.locale__c,
t1.year_month, 
t1.opportunity_id,
t1.name,
first_order_date