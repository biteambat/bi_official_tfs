SELECT
	t1.month,
	t1.ordermth,
	t1.country,
	t4.CAC as Cost_per_Acquisition,
	t3.GPM as GrossProfitMargin,
	t1.GMV as GrossMarketValue,
	(t4.CAC  - t1.GMV * t3.GPM) / t2.unique_cleaners as ROI
	
FROM
(
SELECT
	to_char(contract_start_date, 'YYYY-MM') as Month,
	country,
	to_char(order_date, 'YYYY-MM') as OrderMth,
	sum(gmv_eur_net) as GMV

FROM
	bi.cohort_cleaners
	
GROUP BY
	Month,
	country,
	OrderMth) as t1
JOIN
	(SELECT
	to_char(contract_start_date, 'YYYY-MM') as Month,
	country as Country,
	count(distinct(sfid)) as unique_cleaners
	
FROM
	bi.cohort_cleaners t1
GROUP BY
	Month,
	Country) as t2 ON (t1.Month = t2.Month and t1.country = t2.country)
LEFT JOIN
	(SELECT
		locale as Country,
		to_char(mindate, 'YYYY-MM') as Month,
		value as GPM
	FROM
		bi.city_overview_dashboard
	WHERE
		geographic_level = 'Country level'
		AND time_frame = 'Monthly level'
 		AND kpi = '% GPM'
 	GROUP BY
 		GPM,
		Country,
		Month) as t3 ON (t1.OrderMth = t3.Month and t1.country = t3.Country)
LEFT JOIN
	(SELECT
		locale as Country,
		to_char(date, 'YYYY-MM') as Month,
		(Case when onboardings > 0 THEN (Classifieds + flyers + newspaper + promotion + other_costs + facebook + sem) / onboardings ELSE 0 END) as CAC
	FROM
		bi.cleaners_costsperonboarding
	GROUP BY
		CAC,
		Country,
		Month,
		CAC) as t4 on (t1.Month = t4.Month and t1.country = t4.Country)

GROUP BY
	t1.month,
	t1.ordermth,
	t1.country,
	Cost_per_Acquisition,
	GrossProfitMargin,
	GrossMarketValue,
	ROI