SELECT 
    Voucher__c,
    COUNT(1) as Orders,
    SUM(CASE WHEN Status like '%CANCELLED%' THEN 1 ELSE 0 END) as cancelled_orders,
    CAST(SUM(CASE WHEN Status like '%CANCELLED%' THEN 1 ELSE 0 END) as decimal)/COUNT(1) as Cancellation_Rate
FROM
    bi.orders_w_marketing o
WHERE
    (Voucher__c in ('PUTZEN10') or Voucher__c like 'SPRINGRT%')
	and o.test__c = '0'
GROUP BY
    Voucher__c