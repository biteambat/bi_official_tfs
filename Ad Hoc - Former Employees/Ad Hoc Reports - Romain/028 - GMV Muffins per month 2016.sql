SELECT
	to_char (effectivedate::date, 'YYYY-MM') as monthd,
	replace(o.city,'+','') as city,
	sum(o.gmv_eur) as Revenue,
	sum(o.order_duration__C) as Timed,
	sum(o.gmv_eur) / sum(o.order_duration__C) as pph

FROM bi.orders_w_marketing o

WHERE
	o.status = ('INVOICED')
	and o.Effectivedate::date between '2016-01-01' and '2016-04-30'
	and o.city in ('DE-Munich', 'DE-Frankfurt am Main', 'DE-Hamburg')
	and o.type = '60'
	
GROUP BY
	city,
	monthd