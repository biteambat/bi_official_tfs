SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	o.customer_id__c as custoid,
	o.customer_name__c as Fullname,
	o.customer_email__c as Email,
	SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-12' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) as Open_Order,
	count(distinct(o.Order_Origin__c)) as Multiorder
	
FROM bi.orders_w_marketing o

WHERE
   o.effectivedate > '2016-05-12'

GROUP BY
  	custoid,
   Fullname,
   Email,
   city,
   country

HAVING SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-12' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) > 1