SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.ordernumber as orderid,
	o.pph__c as price,
	o.customer_id__C as custoid,
	o.customer_name__c as Fullname,
	o.customer_email__c as Email,
	o.sfid as SFID
	
FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.city in ('DE-Essen', 'DE-Dusseldorf', 'DE-Duisburg', 'DE-Dortmund')
	and o.status not in ('CANCELLED TERMINATED')
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-04-30')
	and o.test__c = '0'
	and o.type = '60'
	 
GROUP BY 
	city,
	country,
	orderid,
	price,
	recurrency,
	custoid,
	Fullname,
	Email,
	SFID