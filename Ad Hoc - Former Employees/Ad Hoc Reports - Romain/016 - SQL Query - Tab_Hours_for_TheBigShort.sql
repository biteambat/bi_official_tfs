SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	CASE WHEN Acquisition_Customer_creation__c::date between '2016-01-01' and '2016-01-31' THEN 'New' ELSE 'Existing' END as customertype,
	Sum(gmv_eur) as gmv,
	Sum(order_duration__c) as Hours


FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.status in ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
	and (o.effectivedate >= '2016-03-01' and o.effectivedate <= '2016-03-31')
	and o.test__c = '0'

	 
GROUP BY
	city,
	customertype,
	country

ORDER BY
	city asc