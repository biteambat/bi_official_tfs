SELECT
	o.pph__c as price,
	o.status as Orderstatus,
	o.ordernumber as OrderIDDB,
	o.Order_Origin__c as Multiorder,
	o.customer_id__c as custoid,
	o.customer_name__c as Fullname,
	o.customer_email__c as Email,
	right(locale__c ,2) as comm_language
	
FROM bi.orders_w_marketing o

WHERE
    o.effectivedate >= '2016-05-18'
    and o.status not in ('%CANCELLED%', 'INVOICED')
    and o.customer_id__c in 


GROUP BY
  	price,
  	OrderIDDB,
   Orderstatus,
   Multiorder,
   custoid,
   Fullname,
   Email,
   comm_language