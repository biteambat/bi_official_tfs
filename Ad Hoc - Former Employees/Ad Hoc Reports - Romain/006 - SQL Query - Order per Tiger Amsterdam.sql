SELECT
	o.city as city,
	o.effectivedate::timestamp::date as Cleaningdate,
	o.professional__c as cleaner,
	o."orderID" as odnum
	
FROM bi.owm_w_muffins_names o

WHERE
	left(o.city, 2) = 'NL'

ORDER BY
	city asc,
	Cleaningdate asc