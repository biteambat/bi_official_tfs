DROP TABLE IF EXISTS bi.temp_churnlist;
 CREATE TABLE bi.temp_churnlist as 
 SELECT

  created_at::date as date,
  created_at as time,
  (order_Json->>'Order_Start__c') as orderdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id
  FROM
   events.sodium
  WHERE
   event_name = 'Order Event:CANCELLED TERMINATED'

 ;

 DROP TABLE IF EXISTS bi.temp_churncount_weekly;
 CREATE TABLE bi.temp_churncount_weekly AS

  SELECT
   EXTRACT(year from t1.date) as yearnum,
   EXTRACT(week from t1.date) as weeknum,
   MIN(t1.date) as mindate,
   LEFT(t1.locale__c,2) as locale,
   t2.polygon as city,
   COUNT(DISTINCT t1.customer_id) as churned_customers

  FROM bi.temp_churnlist t1

  JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

  WHERE t2.test__c = '0' and order_type = '1'
  
  GROUP BY EXTRACT(year from t1.date), EXTRACT(week from t1.date), LEFT(t1.locale__c,2) ,t2.polygon

  ORDER BY yearnum desc, weeknum desc, locale asc, city asc

 ;

 DROP TABLE IF EXISTS bi.temp_activecount_weekly;
 CREATE TABLE bi.temp_activecount_weekly AS

  SELECT
   EXTRACT(year from t1.effectivedate) as yearnum,
   EXTRACT(week from t1.effectivedate) as weeknum,
   MIN(t1.effectivedate) as mindate,
   LEFT(t1.locale__c,2) as locale,
   t1.polygon as city,
   COUNT(DISTINCT t1.customer_id__c) as active_customers

  FROM bi.orders t1

  WHERE t1.test__c = '0'
   AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
   and order_type = '1'

  GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(week from t1.effectivedate), LEFT(t1.locale__c,2), t1.polygon

  ORDER BY yearnum desc, weeknum desc, locale asc, city asc
 ;

  SELECT
   t1.yearnum as yearnum,

   NULL::int as monthnum,

   t1.weeknum as weeknum,
   MIN(t1.mindate) as mindate,
   t1.locale as locale,
   t1.city as city,

   'City level'::text as geographic_level,

   'Weekly level'::text as time_frame,

   '# churned customers'::text as kpi,
   (t1.churned_customers::numeric) as value

  FROM bi.temp_churncount_weekly t1

  GROUP BY t1.yearnum, t1.weeknum, t1.locale, t1.city, kpi, t1.churned_customers

  ORDER BY yearnum desc, weeknum desc, locale asc, city asc
 ;