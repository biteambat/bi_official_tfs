SELECT
	distinct(o.shippingpostalcode) as zipcode,
	left(o.city, 2) as country,
	o.city as city

FROM bi.orders_w_marketing o

WHERE
	o.type = '60'
