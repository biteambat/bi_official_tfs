DELIMITER //
CREATE OR REPLACE FUNCTION bi.matching_stats(crunchdate date) RETURNS VOID AS 
$BODY$
DECLARE 

BEGIN


DROP TABLE IF EXISTS bi.temp_events_matching;

CREATE TABLE bi.temp_events_matching AS

SELECT
	order_Json->>'Order_Id__c' as OrderID,
	event_name

FROM
	events.sodium
	
WHERE
	created_at::date >= '2016-08-01'
	and (event_name = 'Order Event:PENDING TO START' or event_name = 'Order Event:RESCHEDULED' or event_name = 'Order Event:CANCELLED-NO-MANPOWER'	or event_name = 'Order Event:CANCELLED SKIPPED'); 

DROP TABLE IF EXISTS bi.matching_temp_table;

CREATE TABLE bi.matching_temp_table AS

SELECT
	t2.polygon as Polygon,
	t2.acquisition_new_customer__c as New_Old,
	SUM(CASE WHEN event_name = 'Order Event:PENDING TO START' THEN 1 ELSE 0 END) as NbPTS,
	SUM(CASE WHEN event_name = 'Order Event:RESCHEDULED' THEN 1 ELSE 0 END) as NbRS,
	SUM(CASE WHEN event_name = 'Order Event:CANCELLED-NO-MANPOWER' THEN 1 ELSE 0 END) as NbNMP,
	SUM(CASE WHEN event_name = 'Order Event:CANCELLED SKIPPED' THEN 1 ELSE 0 END) as NbCS

FROM
	bi.temp_events_matching t1 LEFT JOIN bi.orders t2 on (OrderID = t2.order_id__c)
	
WHERE
	t2.effectivedate >= '2016-08-18'
	
GROUP BY
	Polygon,
	New_Old;
	
END;

$BODY$ LANGUAGE 'plpgsql'