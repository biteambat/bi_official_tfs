SELECT
	o.city as city,
	o.cw as calendarweek,
	o.cleanerid as Cleaner,
	CASE WHEN SUM(worked_hours) > SUM(contract_hours) then SUM(contract_hours) ELSE SUM(worked_hours) END as Worked_Hours,
	sum(o.contract_hours) as Total_contract_Hours
		
FROM 
	bi.utalization_per_cleaner_weekly o

WHERE
	o.first_day_of_cw >= '2016-02-29' and o.first_day_of_cw <= '2016-03-20'
	and contract_end > first_day_of_cw
	and start_date <= first_day_of_cw

GROUP BY
	calendarweek,
	city,
	Cleaner

ORDER BY
	city asc,
	calendarweek asc
