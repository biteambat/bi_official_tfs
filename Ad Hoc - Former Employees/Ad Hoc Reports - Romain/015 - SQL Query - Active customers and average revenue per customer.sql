SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	CASE WHEN Acquisition_Customer_creation__c::date between '2016-01-01' and '2016-01-31' THEN 'New' ELSE 'Existing' END as customertype,
	COUNT(DISTINCT(Customer_id__c)) as distinct_Customer,
	Sum(gmv_eur) as gmv,
	Sum(gmv_eur)/COUNT(DISTINCT(Customer_id__c)) as gmvpercusto,
	sum(order_duration__c)/COUNT(DISTINCT(Customer_id__c)) as AVGhrs

FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.status = ('INVOICED')
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-01-31')
	and o.test__c = '0'

	 
GROUP BY
	city,
	customertype,
	country

ORDER BY
	city asc