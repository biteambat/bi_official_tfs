SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.pph__c as price,
	o.order_creation__c::timestamp::date as Ordercreation,
	o.order_start__c as Orderstart,
	o.order_end__c as OrderEnd,
	o.effectivedate::timestamp::date as Cleaningdate,
	o.order_duration__c as hrs,
	o.acquisition_channel__c as Acqchannel,
	case when o.type = '60' then 'Muffin' else 'Freelance' end as Programtype

FROM bi.orders_w_marketing o

WHERE
	o.status = ('CANCELLED NO MANPOWER')
	and LEFT(locale__c,2) = 'de'
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-05-09')
	and o.test__c = '0'

GROUP BY
	city,
	price,
	hrs,
	country,
	Cleaningdate,
	Programtype,
	recurrency,
	Ordercreation,
	Orderstart,
	Acqchannel,
	OrderEnd