SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.pph__c as price,
	o.order_creation__c::timestamp::date as orderd,
	sum(o.order_duration__c) as hours,
	case when o.type = '60' then 'Muffin' else 'Freelance' end as Programtype,
	o."type" as Ordertype,
	o.ordernumber as o_number,
	o.Id as ID,
	o.shippingpostalcode as zipcode,

	case
		when o.type = '1' then 'Economy'
		when o.type = '2' then 'Economy Plus'
		when o.type = '3' then 'Premium'
		when o.type = '4' then 'Premium Pro'
		when o.type is NULL and pph__c = 15 then 'Economy (No type but PPH = 15)'
	End as Quality_level

FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.city in ('DE-Essen', 'DE-Essen+')
	and o.order_creation__c >= '2016-03-03'
	and acquisition_channel__c = 'web'
	and o.test__c = '0'

GROUP BY
	city,
	price,
	country,
	orderd,
	Quality_level,
	Ordertype,
	o_number,
	ID,
	Programtype,
	recurrency

ORDER BY
	city asc,
	orderd asc