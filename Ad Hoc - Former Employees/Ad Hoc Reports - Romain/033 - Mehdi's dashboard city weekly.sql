DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$development_dashboard_kpis(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := ' bi.daily$development_dashboard_kpis';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

--# Acquisitions ====================================================================> CHECKED 

	DROP TABLE IF EXISTS bi.temp_city_overview_dashboard;
	CREATE TABLE bi.temp_city_overview_dashboard AS

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(week from o.effectivedate)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;


--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NTY'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NOT THERE YET') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale, o.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	--take hours
	--take polygons except for cleaner

--Gross Profit Margin

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--Utilization Rate

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churnlist;
	CREATE TABLE bi.temp_churnlist as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		WHERE
		 event_name = 'Order Event:CANCELLED TERMINATED'

	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly;
	CREATE TABLE bi.temp_churncount_weekly AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(week from t1.date) as weeknum,
			LEFT(t1.locale__c,2) as locale,
			t2.polygon as city,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist t1

		JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(week from t1.date), LEFT(t1.locale__c,2) ,t2.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly;
	CREATE TABLE bi.temp_activecount_weekly AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(week from t1.effectivedate) as weeknum,
			LEFT(t1.locale__c,2) as locale,
			t1.polygon as city,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(week from t1.effectivedate), LEFT(t1.locale__c,2), t1.polygon

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc
	;

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,
			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly t1

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, t1.city, kpi, t1.churned_customers

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc
	;


	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,
			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly t1

		JOIN bi.temp_churncount_weekly t2 
			ON t1.weeknum = t2.weeknum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale
				AND t1.city = t2.city

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, t1.city, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, weeknum desc, locale asc, city asc
	;




-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,			

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(week from o.effectivedate)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc

	;


--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			NULL::int as monthnum,

			EXTRACT(week from o.effectivedate)::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% NTY'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NOT THERE YET') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'
		GROUP BY yearnum, weeknum, locale

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

	--take hours
	--take polygons except for cleaner

--Gross Profit Margin

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

--Utilization Rate

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			NULL::int as monthnum,

			m.year_week::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, m.year_week::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;
	CREATE TABLE bi.temp_churnlist_countrylevel as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		WHERE
		 event_name = 'Order Event:CANCELLED TERMINATED'

	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
	CREATE TABLE bi.temp_churncount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(week from t1.date) as weeknum,
			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist_countrylevel t1

		JOIN bi.orders t2 ON t1.customer_id = t2.customer_id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(week from t1.date), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;
	CREATE TABLE bi.temp_activecount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(week from t1.effectivedate) as weeknum,
			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(week from t1.effectivedate), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, weeknum desc, locale asc
	;

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,
			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly_countrylevel t1

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, kpi, t1.churned_customers

		ORDER BY yearnum desc, weeknum desc, locale asc
	;


	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			NULL::int as monthnum,

			t1.weeknum as weeknum,
			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Weekly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly_countrylevel t1

		JOIN bi.temp_churncount_weekly_countrylevel t2 
			ON t1.weeknum = t2.weeknum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale

		GROUP BY t1.yearnum, t1.weeknum, t1.locale, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, weeknum desc, locale asc
	;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			LEFT(o.locale__c,2)::text as locale,

			o.polygon::text as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'

		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(month from o.effectivedate)::int, LEFT(o.locale__c,2), o.polygon::text

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;


--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,
			o.polygon as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NTY'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NOT THERE YET') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale, o.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--Gross Profit Margin

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--Utilization Rate

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale
			m.delivery_area as city,--city

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), m.delivery_area, kpi

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churnlist;
	CREATE TABLE bi.temp_churnlist as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		WHERE
		 event_name = 'Order Event:CANCELLED TERMINATED'

	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly;
	CREATE TABLE bi.temp_churncount_weekly AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(month from t1.date) as monthnum,
			LEFT(t1.locale__c,2) as locale,
			t2.polygon as city,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist t1

		JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(month from t1.date), LEFT(t1.locale__c,2) ,t2.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly;
	CREATE TABLE bi.temp_activecount_weekly AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(month from t1.effectivedate) as monthnum,
			LEFT(t1.locale__c,2) as locale,
			t1.polygon as city,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(month from t1.effectivedate), LEFT(t1.locale__c,2), t1.polygon

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc
	;

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,
			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly t1

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, t1.city, kpi, t1.churned_customers

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc
	;


	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,
			t1.locale as locale,
			t1.city as city,

			'City level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly t1

		JOIN bi.temp_churncount_weekly t2 
			ON t1.monthnum = t2.monthnum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale
				AND t1.city = t2.city

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, t1.city, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, monthnum desc, locale asc, city asc
	;




-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,

			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,			

			'# Invoiced acquisitions'::text as kpi,

			COUNT(DISTINCT o.sfid)::numeric as value

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.acquisition_new_customer__c = '1' 
			AND (o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER'))
			and order_type = '1'
		GROUP BY EXTRACT(year from o.effectivedate)::int, EXTRACT(month from o.effectivedate)::int, LEFT(o.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc

	;


--GMV invoiced =====================================================================>  CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced GMV'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.gmv_eur ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

--Hours Invoiced ================================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'Invoiced Hours'::text as kpi,
			ROUND(SUM(CASE WHEN o.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric,2) as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

--% NMP (out of INVOICED + NMP) =================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NMP'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NO MANPOWER') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'

		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

--% NTY (out of INVOICED + NTY) ================================================> CHECKED

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from o.effectivedate)::int as yearnum,

			EXTRACT(month from o.effectivedate)::int as monthnum,

			NULL::int as weeknum,
			LEFT(o.locale__c,2) as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% NTY'::text as kpi,
			CASE WHEN (SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END) > 0) THEN 
				ROUND(SUM(CASE WHEN o.status in ('CANCELLED NOT THERE YET') THEN o.order_duration__c ELSE 0 END)::numeric / SUM(CASE WHEN o.status in ('INVOICED','CANCELLED NOT THERE YET','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.order_duration__c ELSE 0 END)::numeric,2) 
			ELSE 0 END
			as value

		FROM bi.orders o

		WHERE o.test__c = '0'
			and order_type = '1'
		GROUP BY yearnum, monthnum, locale

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

	--take hours
	--take polygons except for cleaner

--Gross Profit Margin

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% GPM'::text as kpi,--kpi
			CASE WHEN SUM(m.revenue) > 0 THEN (SUM(m.revenue)-SUM(m.salary_payed))/SUM(m.revenue) ELSE NULL END as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

--Utilization Rate

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			EXTRACT(year from m.mindate)::int as yearnum,--yearnum

			EXTRACT(month from m.mindate)::int as monthnum,

			NULL::int as weeknum,--weeknum
			left(m.delivery_area,2) as locale,--locale

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'UR %'::text as kpi,--kpi
			SUM(CASE WHEN (m.worked_hours) > (m.weekly_hours) THEN (m.weekly_hours) ELSE (m.worked_hours) END)/SUM(m.weekly_hours) as value--value

		FROM bi.gpm_weekly_cleaner m

		GROUP BY EXTRACT(year from m.mindate)::int, EXTRACT(month from m.mindate)::int, left(m.delivery_area,2), kpi

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

--% churns (out of active customers)

-- # churns per year, week, city, 

	DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;
	CREATE TABLE bi.temp_churnlist_countrylevel as 
	SELECT

		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		 events.sodium
		WHERE
		 event_name = 'Order Event:CANCELLED TERMINATED'

	;

	DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
	CREATE TABLE bi.temp_churncount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.date) as yearnum,
			EXTRACT(month from t1.date) as monthnum,
			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id) as churned_customers

		FROM bi.temp_churnlist_countrylevel t1

		JOIN bi.orders t2 ON t1.customer_id = t2.customer_id__c

		WHERE t2.test__c = '0' and order_type = '1'
		
		GROUP BY EXTRACT(year from t1.date), EXTRACT(month from t1.date), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc

	;

	DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;
	CREATE TABLE bi.temp_activecount_weekly_countrylevel AS

		SELECT
			EXTRACT(year from t1.effectivedate) as yearnum,
			EXTRACT(month from t1.effectivedate) as monthnum,
			LEFT(t1.locale__c,2) as locale,
			COUNT(DISTINCT t1.customer_id__c) as active_customers

		FROM bi.orders t1

		WHERE t1.test__c = '0'
			AND t1.status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
			and order_type = '1'

		GROUP BY EXTRACT(year from t1.effectivedate), EXTRACT(month from t1.effectivedate), LEFT(t1.locale__c,2)

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,
			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'# churned customers'::text as kpi,
			(t1.churned_customers::numeric) as value

		FROM bi.temp_churncount_weekly_countrylevel t1

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, kpi, t1.churned_customers

		ORDER BY yearnum desc, monthnum desc, locale asc
	;


	INSERT INTO bi.temp_city_overview_dashboard

		SELECT
			t1.yearnum as yearnum,

			t1.monthnum as monthnum,

			NULL::int as weeknum,
			t1.locale as locale,

			''::text as city,

			'Country level'::text as geographic_level,

			'Monthly level'::text as time_frame,

			'% churned customers'::text as kpi,
			(t2.churned_customers::numeric/t1.active_customers::numeric) as value

		FROM bi.temp_activecount_weekly_countrylevel t1

		JOIN bi.temp_churncount_weekly_countrylevel t2 
			ON t1.monthnum = t2.monthnum 
				AND t1.yearnum = t2.yearnum
				AND t1.locale = t2.locale

		GROUP BY t1.yearnum, t1.monthnum, t1.locale, kpi, t2.churned_customers, t1.active_customers

		ORDER BY yearnum desc, monthnum desc, locale asc
	;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	DROP TABLE IF EXISTS bi.temp_activecount_weekly_countrylevel;
	DROP TABLE IF EXISTS bi.temp_churncount_weekly_countrylevel;
	DROP TABLE IF EXISTS bi.temp_churnlist_countrylevel;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'