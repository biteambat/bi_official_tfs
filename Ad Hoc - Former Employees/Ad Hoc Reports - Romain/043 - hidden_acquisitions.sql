SELECT
 EXTRACT(WEEK FROM effectivedate::date) as Week,
 MIN(Effectivedate::date) as Date,
 CASE WHEN acquisition_new_customer__C = '1' THEN 'Acquisition' ELSE 'Existing Customer' END as customertype,
 COUNT(DISTINCT(customer_id__c))
FROM
 bi.orders
WHERE
 status = 'INVOICED'
 and effectivedate::date between '2016-01-01' and '2016-09-12'
 and polygon = 'de-berlin'
 and acquisition_channel__c = 'web'
GROUP BY
 week,
 customertype