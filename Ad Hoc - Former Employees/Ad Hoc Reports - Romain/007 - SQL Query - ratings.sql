SELECT
	replace(o.city,'+','') as city,
	AVG(o.rating_service__c)

FROM bi.orders_w_marketing o

WHERE
	o.status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	and left(o.city, 2) = 'DE'
	and (o.effectivedate >= '2015-07-01' and o.effectivedate <= '2016-03-31')
	and o.test__c = '0'

GROUP BY
	city