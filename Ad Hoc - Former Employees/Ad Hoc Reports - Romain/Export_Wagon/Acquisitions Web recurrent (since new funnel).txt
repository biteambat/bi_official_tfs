SELECT
  o.order_creation__c::date as date,
  replace(o.city,'+','') as city,
  o.marketing_channel as channel,
  voucher__c as VoucherCode,
  count(id),
  polygon as Polygon,
  Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency

FROM bi.orders_w_marketing o

WHERE o.test__c = '0'
 AND o.status not like ('%CANCELLED%')
 AND o.order_creation__c >= '2015-10-01'
 AND acquisition_new_customer__c = '1'
 AND acquisition_channel__c = 'web'

GROUP BY o.order_creation__c::date, o.city, channel ,  VoucherCode, Polygon, recurrency
 
ORDER BY date desc, city asc, marketing_channel desc