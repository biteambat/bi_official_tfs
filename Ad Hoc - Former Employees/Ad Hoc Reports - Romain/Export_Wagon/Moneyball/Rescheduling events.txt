SELECT
	created_at::date as date,
	created_at as time,
	order_Json->>'Order_Start__c' as orderdate,
	order_Json->>'Order_Id__c' as Order_id,
	order_Json->>'Contact__c' as customer_id,
	o.acquisition_customer_creation__c::date as Joined_date,
	Max(case when o.status = 'INVOICED' then o.effectivedate else '2000-01-01' end) as LastInvoicedDate,
	event_name as NameofEvent
	
FROM
	events.sodium JOIN bi.orders_w_marketing o ON o.customer_id__c = order_Json->>'Contact__c'


WHERE
	event_name LIKE '%RESCHEDUL%'
	and o.recurrency__c > 6
	and left(o.locale__c, 2) = 'de'
	
GROUP BY
	time,
	date,
	orderdate,
	Order_id,
	customer_id,
	Joined_date,
	NameofEvent