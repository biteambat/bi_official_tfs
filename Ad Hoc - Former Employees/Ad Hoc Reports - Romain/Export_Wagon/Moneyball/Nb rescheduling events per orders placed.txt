SELECT
 	o.order_id__c,
 	SUM(case when event_name = 'Order Event:WAITING-FOR-RESCHEDULE' then 1 else 0 end) as NbReschedules
 
FROM
  	bi.orders_w_marketing o LEFT JOIN events.sodium ON (o.order_id__c = CAST(order_Json->>'Order_Id__c' as varchar) and created_at::date >= '2016-05-01' and event_name = 'Order Event:WAITING-FOR-RESCHEDULE' )


WHERE
	o.effectivedate >= '2016-06-01'  and o.effectivedate < '2016-07-01'
 	and o.recurrency__c > 6
 	and left(o.locale__c, 2) = 'de'
 	and o.test__c = '0'
 
GROUP BY
 	o.order_id__c