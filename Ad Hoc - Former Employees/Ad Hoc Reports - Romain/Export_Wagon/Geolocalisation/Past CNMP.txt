SELECT
	replace(city,'+','') as city,
	LEFT(Locale__c,2) as country,
	effectivedate as Cleaningdate,
	order_duration__c as hrs,
	sfid as SFID,
	order_start__c::time + interval '2 hours' as	Starttime,
	Case
		when order_start__c::time + interval '2 hours' between '00:00:00' and '07:59:00' then 'Early AM'
		when order_start__c::time + interval '2 hours' between '08:00:00' and '09:59:00' then 'Mid AM'
		when order_start__c::time + interval '2 hours' between '10:00:00' and '11:59:00' then 'Late AM'
		when order_start__c::time + interval '2 hours' between '12:00:00' and '14:59:00' then 'Early PM'
		when order_start__c::time + interval '2 hours' between '15:00:00' and '17:59:00' then 'Mid PM'
		when order_start__c::time + interval '2 hours' between '18:00:00' and '23:59:00' then 'Late PM'
	End as TimeofDay,
	shippinglongitude as longitude,
	shippinglatitude as latitude

FROM bi.orders_w_marketing o

WHERE
	status = ('CANCELLED NO MANPOWER')
	and (effectivedate >= '2016-09-19' and effectivedate <= '2016-10-09')
	and test__c = '0'

GROUP BY
	city,
	country,
	SFID,
	Cleaningdate,
	Hrs, 
	TimeofDay,
	Starttime,
	latitude,
	longitude