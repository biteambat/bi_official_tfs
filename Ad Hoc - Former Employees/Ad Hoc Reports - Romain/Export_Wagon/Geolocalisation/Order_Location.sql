SELECT
   pph__c as pricepoint,
   customer_id__c as CustoID,
   sum(order_duration__c) as Hours,
   sum(gmv_eur) as GMV,
   status as Status,
	recurrency__c as RecType,
   shippinglongitude as Longitude,
   shippinglatitude as Latitude
    
FROM bi.orders_w_marketing

WHERE
	effectivedate > '2016-08-24'
	and test__c = '0'
	
GROUP BY
	RecType,
	pricepoint,
   CustoID,
   Status,
   Longitude,
   Latitude