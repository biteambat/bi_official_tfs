SELECT
	working_city__c as City,
	latitude as latitude,
	longitude as longitude,
	sfid as SFID,
	has_vehicle__c as Vehicle,
	status as Status

FROM salesforce.lead

WHERE
	createddate >= '2016-04-01'

GROUP BY
	City,
	latitude,
	longitude,
	SFID,
	Vehicle,
	Status