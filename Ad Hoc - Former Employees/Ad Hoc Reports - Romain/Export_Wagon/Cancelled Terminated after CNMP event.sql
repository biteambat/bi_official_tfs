SELECT
 nmp_events,
 COUNT(DISTINCT(customer_id)) as all_customers,
 SUM(churn_flag) as Churn_Flag
FROM(
SELECT
 Customer_id,
 COUNT(DISTINCT(order_id)) as NMP_Events,
 MAX(CASE WHEN date_of_churn is not null THEn 1 ELSE 0 END) as Churn_Flag
FROM
(
SELECT
 t1.customer_id,
 t1.order_id,
 t1.orderdate as date_of_nmp,
 min(t2.date) as date_of_churn
FROM

 (SELECT
  created_at::date as date,
  event_name,
  (order_Json->>'Order_Start__c') as orderdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id,
  order_Json->>'Recurrency__c' as recurrency
 
 FROM
  events.sodium
 
 WHERE
  event_name in ('Order Event:CANCELLED-NO-MANPOWER')
  and created_at >= '2016-05-01'
  and order_Json->>'Recurrency__c' > '0'
  
  ) as t1
 
 LEFT JOIN
  
(SELECT
  created_at::date as date,
  event_name,
  (order_Json->>'Order_Start__c') as orderdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id
 
 FROM
  events.sodium
 
 WHERE
  event_name in ( 'Order Event:CANCELLED TERMINATED')
  and created_at >= '2016-05-01') as t2
ON
 (t1.customer_id = t2.customer_id and t2.date between t1.date and t1.date + 14)
GROUP BY
 t1.customer_id,
 t1.order_id,
 t1.orderdate) as t3
GROUP BY
 Customer_Id) as t4
GROUP BY
 nmp_events