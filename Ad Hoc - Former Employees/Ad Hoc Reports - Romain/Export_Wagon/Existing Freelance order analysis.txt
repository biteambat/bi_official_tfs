SELECT
	replace(city,'+','') as city,
	delivery_area as dev_area,
	Case when recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
   pph__c as pricepoint,
   customer_id__c as CustoID,
   sum(order_duration__c) as Hours,
   sum(gmv_eur) as GMV,
   status as Status,
   shippinglongitude as Longitude,
   shippinglatitude as Latitude
    
FROM bi.orders_w_marketing o

WHERE
	effectivedate > '2016-06-21'
	and test__c = '0'
	and left(locale__C, 2) = 'de'
	and type <> '60'

GROUP BY
	city,
	dev_area,
	recurrency,
   pricepoint,
   CustoID,
   Status,
   Longitude,
   Latitude