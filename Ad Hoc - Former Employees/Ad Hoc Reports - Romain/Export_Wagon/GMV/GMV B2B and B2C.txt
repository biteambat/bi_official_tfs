SELECT
    left(locale__c, 2) as country,
    polygon as polygon,
    sum(gmv_eur) as GMV,
    Case when acquisition_new_customer__c = 'TRUE' then 'New' else 'Old' end as New_Old,
    effectivedate as cleaningdate,
	 order_type as OrderType
  
FROM bi.orders o

WHERE
	effectivedate <= '2016-08-21' and effectivedate >= '2016-01-01'
	and test__c = '0'
	and status in ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')


GROUP BY
	country,
	cleaningdate,
	polygon,
	New_Old,
	OrderType