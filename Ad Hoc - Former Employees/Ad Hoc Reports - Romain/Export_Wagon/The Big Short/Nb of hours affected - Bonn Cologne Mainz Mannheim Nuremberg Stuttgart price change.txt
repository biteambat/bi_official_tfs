SELECT
    o.pph__c as price,
    o.customer_id__c,
    o.customer_name__C as Namecustomer,
    o.city as city,
    SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-25' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) as Open_Orders,
    SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-01' and o.Effectivedate::date <= '2016-05-31' THEN o.order_duration__c ELSE 0 END) as HoursMay,
    SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-04-01' and o.Effectivedate::date <= '2016-04-30' THEN o.order_duration__c ELSE 0 END) as HoursApril,
    SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-25' and acquisition_channel__c = 'recurrent' THEN o.order_duration__c ELSE 0 END) as Open_Hours
 
FROM bi.orders_w_marketing o

WHERE
    o.city in ('DE-Bonn', 'DE-Cologne', 'DE-Mainz', 'DE-Mannheim', 'DE-Nuremberg', 'DE-Stuttgart')
    and o.test__c = '0'
    and o.type = '60'
    and o.pph__c < 16.9
 
GROUP BY 
    price,
    Customer_id__c,
    city,
    Namecustomer

HAVING SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-25' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) > 0

ORDER BY
    price asc