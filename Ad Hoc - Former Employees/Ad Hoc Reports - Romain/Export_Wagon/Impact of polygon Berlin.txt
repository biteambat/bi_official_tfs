SELECT
    delivery_area as DA,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	Case when acquisition_new_customer__c = 'True' then 'New' else 'Old' end as OldorNew,
    order_creation__c::date as Cleaningdate,
    status as orderstatus,
    sum(o.gmv_eur) as GMV,
    Case when o.type = '60' THEN 'Muffin' else 'Freelance' end as Program_Type
  
FROM bi.orders_w_marketing o

WHERE
	delivery_area in ('de-berlin')
	and o.order_creation__c >= '2016-01-01'
	and o.test__c = '0'
	and acquisition_channel__c = 'web'

GROUP BY
	DA,
	recurrency,
	OldorNew,
    Cleaningdate,
    status,
    Program_Type