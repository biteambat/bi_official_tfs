SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	o.professional__C as Cleaner,
	o.sfid as SFID,
	o.effectivedate as DateofService,
	o.order_duration__c as orderduration

FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = 'at'
	and o.status = 'INVOICED'
	and o.effectivedate >= '2016-05-16'
 	and o.effectivedate < '2016-05-22'
 	
GROUP BY
	Cleaner,
	sfid,
	city,
	country,
	DateofService,
	orderduration
	