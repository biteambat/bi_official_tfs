SELECT
	t1.country,
	t1.Cohort_month,
	t1.OrderMth,
	round((cast(t1.active_cleaners as decimal) / t2.CohortNBMth0),2) as RR

FROM
(SELECT
	to_char(contract_start_date, 'YYYY-MM') as Cohort_month,
	country,
	to_char(order_date, 'YYYY-MM') as OrderMth,
	count(distinct(sfid)) as active_cleaners

FROM
	bi.cohort_cleaners

GROUP BY
	Cohort_month,
	country,
	OrderMth) as t1 JOIN

(SELECT
	country,
	to_char(contract_start_date, 'YYYY-MM') as Cohort_month,
	count(distinct(sfid)) as CohortNBMth0

FROM
	bi.cohort_cleaners

GROUP BY
	Cohort_month,
	country) as t2 on (t1.Cohort_month = t2.Cohort_month and t1.country = t2.country)
		
GROUP BY
	t1.country,
	t1.Cohort_month,
	t1.OrderMth,
	t1.active_cleaners,
	RR