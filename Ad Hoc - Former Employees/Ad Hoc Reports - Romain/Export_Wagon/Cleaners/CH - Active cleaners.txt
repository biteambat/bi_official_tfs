SELECT
	o.name as Namecleaner,
	o.sfid as SFID,
	a.order_duration__c as Hours,
	a.effectivedate as cleaningdate,
	a.order_id__c as OrderID

FROM salesforce.account o INNER JOIN bi.orders a on (a.professional__c = o.sfid)

WHERE
	o.test__c = '0'
	and left(a.locale__c, 2) = 'ch'
	and a.effectivedate >= '2016-01-01'
	and (o.hr_contract_end__c::date >= '2016-01-01' OR o.hr_contract_end__c IS NULL)

GROUP BY
	Namecleaner,
	o.SFID,
	Hours,
	cleaningdate,
	OrderID