SELECT
	replace(o.city,'+','') as city,
	left(o.locale__c , 2) as country,
	o.effectivedate,
	count(distinct o.customer_id__c)

FROM bi.orders_w_marketing o

WHERE
	o.status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	and (o.effectivedate >= '2015-01-01' and o.effectivedate <= '2016-02-28')
	and o.test__c = '0'

GROUP BY
	country,
	city,
	o.effectivedate

ORDER BY
	city asc
	
-- This is a test