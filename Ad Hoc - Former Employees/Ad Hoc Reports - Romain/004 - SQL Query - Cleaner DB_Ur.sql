SELECT
	o.city as city,
	o.cleanerid as cleaner,
	o.cw as calendarweek,
	o.worked_hours / o.contract_hours as Ur,
	o.worked_hours,
	o.contract_hours,
	o.contract_end,
	first_day_of_cw,
	start_date
	
FROM bi.utalization_per_cleaner_weekly o

WHERE
	o.first_day_of_cw >= '2016-01-04' and o.first_day_of_cw <= '2016-03-20'
	and contract_end > first_day_of_cw
	and start_date <= first_day_of_cw 
