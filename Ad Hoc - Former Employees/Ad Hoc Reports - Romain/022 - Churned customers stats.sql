SELECT
	replace(o.city,'+','') as city,
	o.pph__c as price,
	o.customer_id__C as custoid,
	o.ordernumber as OrderNum,
	o.customer_name__c as Fullname,
	o.gmv_eur as GMV,
	o.order_duration__c as Hours
	
FROM bi.orders_w_marketing o

WHERE
	o.customer_id__c in ('0032000001bjjYeAAI',
'0032000001hfCmvAAE',
'0032000001YFcvIAAT',
'0032000001YmOziAAF',
'0032000001WXZSyAAP',
'0032000001LtXecAAF',
'0032000001fv3GFAAY',
'0032000001XydPmAAJ',
'0032000001hfWfQAAU',
'0032000001en78lAAA',
'0032000001N0ShAAAV',
'0032000001QZya7AAD',
'0032000001ZJsMHAA1',
'0032000001afHHNAA2',
'0032000001f1T2fAAE',
'0032000001fuwnmAAA',
'0032000001glXsmAAE',
'0032000001gm9WxAAI',
'0032000001iggcXAAQ',
'0032000001PkMyHAAV',
'0032000001Xvc8RAAR',
'0032000001ZKTsCAAX',
'0032000001fBwwbAAC')
	and o.status in ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
	and o.effectivedate >= '2016-04-01' and o.effectivedate < '2016-05-01'
    and o.test__c = '0'
	
GROUP BY 
	city,
	OrderNum,
	price,
	custoid,
	Fullname,
	GMV,
	Hours