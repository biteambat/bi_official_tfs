SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	sum(o.order_duration__c) as hrs,
	o.customer_id__c as customerid
	
FROM bi.orders_w_marketing o

WHERE
	o.status = ('INVOICED')
	and LEFT(locale__c,2) = 'de'
	and (o.effectivedate >= '2016-04-01' and o.effectivedate < '2016-05-01')
	and o.test__c = '0'
    and o.type = '60'
    and o.recurrency__c > 6


GROUP BY
	city,
	country,
	customerid
