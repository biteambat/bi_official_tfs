SELECT
	o.city as city,
	LEFT(Locale__c,2) as country,
	CASE WHEN Acquisition_Customer_creation__c::date between '2016-04-01' and '2016-04-30' THEN 'New' ELSE 'Existing' END as customertype,
	o.gmv_eur as gmv,
	o.ordernumber as orderid


FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.status = ('INVOICED')
	and (o.effectivedate >= '2016-04-01' and o.effectivedate <= '2016-04-30')
	and o.test__c = '0'
	and o.type = '60'
	 
GROUP BY
	city,
	customertype,
	country,
	orderid,
	gmv

ORDER BY
	city asc