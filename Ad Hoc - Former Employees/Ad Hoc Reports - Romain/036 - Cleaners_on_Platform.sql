SELECT
    t1.Month,
    SUM(CASE WHEN TO_CHAR(startdate::date,'YYYY-MM') <= TO_CHAR(mindate,'YYYY-MM') and (TO_CHAR(enddate::date,'YYYY-MM') > TO_CHAR(mindate,'YYYY-MM') or enddate is null) THEN 1 ELSE 0 END) as cop

FROM

(SELECT
    TO_CHAR(date,'YYYY-MM') as MOnth,
    min(date) as mindate,
    max(date) as maxdate

FROM
    bi.orderdate

WHERE
    TO_CHAR(date,'YYYY-MM') >= '2016-01'

GROUP BY
 Month) as t1,

(SELECT
    name as Namecleaner,
    sfid as SFID,
    hr_contract_start__c::date as StartDate,
    hr_contract_end__c::date as EndDate
 
FROM salesforce.account

WHERE
    test__c = '0'
    and left(locale__c, 2) = 'ch'
    and (hr_contract_end__c::date >= '2016-01-01' OR hr_contract_end__c IS NULL)
 
GROUP BY
    Namecleaner,
    SFID,
    StartDate,
    EndDate ) as t2

GROUP BY
    t1.month