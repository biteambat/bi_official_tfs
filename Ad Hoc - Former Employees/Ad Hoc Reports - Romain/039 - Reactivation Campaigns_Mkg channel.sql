SELECT
	t4.AcqChannel,
	Nborders as OrderCount,
	count(customer) as NbCusto
	
	
FROM
(SELECT
	t3.marketing_channel as AcqChannel,
	CustoID as Customer,
	ReactivationDate as WelcomeBack,
	count(distinct(case when t2.voucher__c > '0' THEN t2.Order_ID__c ELSE NULL END)) as Vouchercount,
	count(t2.order_id__c) as Nborders


FROM
(SELECT
	customer_id__c as CustoID,
	min(order_start__c::date) as ReactivationDate
	
FROM
	bi.orders

WHERE
	voucher__c in ('YELLOW15', 'YELLOW35', 'MAY15', 'MAY1H', 'SPRINGRT15', 'SPRINGRT1H')
	and status in ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')

GROUP BY
	CustoID) as t1 LEFT JOIN bi.orders as t2 on (t1.CustoID = t2.customer_id__c and t2.order_start__c >= t1.ReactivationDate and t2.status in ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL'))
	
LEFT JOIN bi.orders t3 on (t1.CustoID = t3.customer_id__c and t3.acquisition_new_customer__c = '1') 

GROUP BY
	Customer,
	WelcomeBack,
	AcqChannel) as t4
	
GROUP BY
	Ordercount,
	AcqChannel
	
	