SELECT
	o.customer_id__C as Customer,
	/*date_part(dd, MAX(o.effectivedate::timestamp::date) - MIN(o.effectivedate::timestamp::date))/(count(*)-1) as Average_Days_Between_Orders,*/
	(MAX(o.effectivedate::timestamp::date) - MIN(o.effectivedate::timestamp::date)) as days_between_orders,
	(count(*)-1) as nb_orders,
	replace(o.city,'+','') as City,
	left(o.city, 2) as Country
	
FROM bi.orders_w_marketing o

WHERE
	left(o.city, 2) in ('DE', 'AT')

GROUP BY
	Customer, City, Country
	
HAVING Count(*) > 1
	
