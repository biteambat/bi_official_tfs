SELECT
   to_char(CleaningDate, 'YYYY-MM') as Mth,
   t2.polygon as polygon,
	COUNT(DISTINCT(CSFID)) as Active_Cleaners,
	Sum(Hours) / COUNT(DISTINCT(CSFID)) as avg_hours_per_cleaner
	
FROM

(SELECT
   a.polygon as Polygon,
   o.sfid as CSFID,
   o.hr_contract_start__c::date as StartDate,
   o.hr_contract_end__c::date as EndDate,
   a.effectivedate as CleaningDate,
   a.status as Status,
   sum(a.order_duration__c) as Hours
 
FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

WHERE
   o.test__c = '0'
   and a.test__c = '0'
   and effectivedate >= '2016-01-01'
	and a.type = 'cleaning-b2c'
	and status = 'INVOICED'
    
GROUP BY
   CSFID,
   StartDate,
   Polygon,
   Cleaningdate,
   Status,
   EndDate) as t2

GROUP BY
   Mth,
	polygon