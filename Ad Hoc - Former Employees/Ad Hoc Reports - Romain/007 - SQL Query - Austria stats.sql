SELECT
	replace(o.city,'+','') as city,
	o.marketing_channel as Mkgchan,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.gmv_eur as GMV

FROM bi.orders_w_marketing o

WHERE
	o.status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	and left(o.city, 2) = 'AT'
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-03-31')
	and o.test__c = '0'