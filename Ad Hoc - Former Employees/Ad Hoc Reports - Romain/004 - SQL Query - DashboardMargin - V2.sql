SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.pph__c as price,
	o.effectivedate::timestamp::date as Cleaningdate,
	sum(case when o.status in ('INVOICED') then o.order_duration__c else NULL end) as invoiced_hours

FROM bi.orders_w_marketing o

WHERE
	o.status = ('INVOICED')
	and o.type = '60'
	and LEFT(locale__c,2) = 'de'
	and (o.effectivedate >= '2016-02-29' and o.effectivedate <= '2016-03-20')
	and o.test__c = '0'

GROUP BY
	city,
	price,
	country,
	Cleaningdate,
	recurrency

ORDER BY
	city asc,
	Cleaningdate asc