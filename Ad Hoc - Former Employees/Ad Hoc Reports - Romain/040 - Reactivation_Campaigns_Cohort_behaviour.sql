SELECT
	ReturningMth as CohortMth,
	Count(distinct(Customer)) as CustoCount,
	Sum(Nborders) / Count(distinct(Customer)) as ActivityRate

FROM
(SELECT
	t1.CustoID as Customer,
	extract(month from ReactivationDate) as WelcomeBackMth,
	extract(month from order_start__c) as OrderMth,
	extract(month from order_start__c) - extract(month from ReactivationDate) as ReturningMth,
	count(distinct(case when voucher__c > '0' THEN Order_ID__c ELSE NULL END)) as Vouchercount,
	count(order_id__c) as Nborders


FROM
(SELECT
	customer_id__c as CustoID,
	min(order_start__c::date) as ReactivationDate
	

FROM
	bi.orders

WHERE
	voucher__c in ('YELLOW15', 'YELLOW35', 'MAY15', 'MAY1H', 'SPRINGRT15', 'SPRINGRT1H')
	and status in ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')

GROUP BY
	CustoID) as t1 LEFT JOIN bi.orders as t2 on (t1.CustoID = t2.customer_id__c and t2.order_start__c >= t1.ReactivationDate and t2.status in ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL'))

GROUP BY
	Customer,
	WelcomeBackMth,
	OrderMth,
	ReturningMth) as t3
	
GROUP BY
	CohortMth
	