SELECT
	left(o.city, 2) as country,
	replace(o.city,'+','') as city,
	o.acquisition_new_customer__c as customer_type,
	sum(o.gmv_eur) as Revenue,
	sum(o.order_duration__c) as Duration,
	COUNT(DISTINCT(Customer_id__c)) as distinct_Customer

FROM bi.orders_w_marketing o

WHERE
	o.status not in ('CANCELLED FAKE','CANCELLED MISTAKE')
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-03-31')
	and left(o.city, 2) in ('DE', 'AT', 'CH', 'NL')
	and o.test__c = '0'

GROUP BY
	country,
	city,
	Cleaningdate,
	customer_type
	
ORDER BY city desc

