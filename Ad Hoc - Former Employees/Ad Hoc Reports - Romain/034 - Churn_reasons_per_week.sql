SELECT
 Country,
 Poly,
 Week,
 COUNT(DISTINCT(customer_id)) as Total_number_of_churns,
 SUM(cnmp_flag) as Number_of_churns_due_to_NMP,
 SUM(bad_rating_Flag) as Number_of_churns_due_to_bad_rating,
 COUNT(DISTINCT(customer_id)) - SUM(cnmp_flag) - SUM(bad_rating_Flag) as Number_of_churns_due_to_unknown_reason

FROM(

SELECT
 Customer_id,
 Poly,
 Country,
 EXTRACT(week from date_of_churn::date) as Week,
 COUNT(DISTINCT(order_id)) as CT_Events,
 MAX(CASE WHEN date_of_cnmp is not null THEN 1 ELSE 0 END) as cnmp_Flag,
 MAX(CASE WHEN date_of_bad_rating is not null THEN 1 ELSE 0 END) as bad_rating_Flag
 
FROM
(
SELECT
 t1.customer_id,
 t1.order_id,
 t15.polygon as Poly,
 left(t1.Locale__c,2) as Country,
 t1.orderdate as date_of_churn,
 max(t2.date) as date_of_cnmp,
 max(t10.date) as date_of_bad_rating
 
FROM

 (SELECT
  created_at::date as date,
  event_name,
  (order_Json->>'Order_Start__c') as orderdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id,
  order_Json->>'Recurrency__c' as recurrency
 
 FROM
  events.sodium
 
 WHERE
  event_name in ('Order Event:CANCELLED TERMINATED')
  and created_at >= '2016-05-01'
  and order_Json->>'Recurrency__c' > '0'
  
  ) as t1
  
 LEFT JOIN
 
(SELECT
  polygon as polygon,
  order_id__c as Order_id
  
 FROM
  bi.orders
  
 WHERE
  status = 'CANCELLED TERMINATED' ) as T15

 ON
  t1.Order_id = t15.Order_id
  
 LEFT JOIN
  
(SELECT
  created_at::date as date,
  event_name,
  (order_Json->>'Order_Start__c') as orderdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id
 
 FROM
  events.sodium
 
 WHERE
  event_name in ( 'Order Event:CANCELLED-NO-MANPOWER')
  and created_at >= '2016-05-01') as t2
ON
 (t1.customer_id = t2.customer_id and t2.date between t1.date - 28 and t1.date )
 
 LEFT JOIN

(SELECT
  order_id__c as order_id,
  effectivedate::date as orderdate,
  order_start__c::date as date,
  customer_id__c as customer_id
   
 FROM
  bi.orders
 
 WHERE
  rating_service__c in ('1', '2', '3')
  and effectivedate::date >= '2016-05-01'
  and recurrency__c > '0'
  
  ) as t10

ON
(t1.customer_id = t10.customer_id and t10.date between t1.date - 28 and t1.date) 
 
GROUP BY
 t1.customer_id,
 t1.order_id,
 t1.orderdate,
 left(t1.locale__c,2),
 t15.polygon
) as t3
GROUP BY
 Customer_Id,
 Week,
 Country,
 Poly
 ) as t4
 
 GROUP BY
 Week,
 Country,
 Poly