SELECT 
	count(distinct(t1.customer_id)) as Nbchurns,
	count(1) as check_safe,
	EXTRACT(week from t1.date) as weeknum,
	t2.polygon as polygon,
   Case when t2.acquisition_customer_creation__c::date > '2016-05-19' then 'Old' else 'New' end as Acq_Date

FROM 

(SELECT
		created_at::date as date,
		created_at as time,
		(order_Json->>'Order_Start__c') as orderdate,
		order_Json->>'Locale__c' as Locale__c,
		order_Json->>'Order_Id__c' as Order_id,
		order_Json->>'Contact__c' as customer_id
		FROM
		events.sodium
		WHERE
		event_name = 'Order Event:CANCELLED TERMINATED')

as t1 JOIN bi.orders as t2 on t1.Order_id = t2.order_id__c

WHERE
	extract(year from t1.date) = '2016'
	and t2.test__c = '0'
		 
GROUP BY
	weeknum,
	polygon,
	Acq_Date