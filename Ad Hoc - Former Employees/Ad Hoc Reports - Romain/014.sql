SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.pph__c as price,
	o.effectivedate::timestamp::date as Cleaningdate,
	sum(case when o.status in ('INVOICED') then o.order_duration__c else NULL end) as invoiced_hours,
	o.gmv_eur as GMV,
	o.acquisition_new_customer__c as customer_type,
	COUNT(DISTINCT(Customer_id__c)) as distinct_Customer,
	Case 
		when o.type = '1' then 'Economy'
		when o.type = '2' then 'Economy Plus'
		when o.type = '3' then 'Premium'
		when o.type = '4' then 'Premium Pro'
		when o.type is NULL and pph__c = 15 then 'Economy (No type but PPH = 15)'
		when o.type = '60' then 'Muffins'
	End as Program

FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.status = ('INVOICED')
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-04-26')
	and o.test__c = '0'

GROUP BY
	city,
	price,
	country,
	Cleaningdate,
	recurrency,
	GMV,
	customer_type,
	Program

ORDER BY
	city asc,
	Cleaningdate asc