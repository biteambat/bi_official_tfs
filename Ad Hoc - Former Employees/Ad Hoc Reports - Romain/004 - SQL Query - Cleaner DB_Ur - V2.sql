SELECT
	o.city as city,
	o.cw as calendarweek,
	o.cleanerid as Cleaner,
	SUM(CASE WHEN (o.worked_hours <= o.contract_hours) THEN (o.contract_hours - o.worked_hours) ELSE 0 END) as Unused_Hours,
	sum(o.contract_hours) as Total_contract_Hours
		
FROM 
	bi.utalization_per_cleaner_weekly o

WHERE
	o.first_day_of_cw >= '2016-02-29' and o.first_day_of_cw <= '2016-03-20'
	and contract_end > first_day_of_cw
	and start_date <= first_day_of_cw

GROUP BY
	calendarweek,
	city,
	Cleaner

ORDER BY
	city asc,
	calendarweek asc
