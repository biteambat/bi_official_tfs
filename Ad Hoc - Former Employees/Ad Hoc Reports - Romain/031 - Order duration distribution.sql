SELECT
    o.order_duration__c as booking_duration,
    o.city as City,
    o.ordernumber as Orderid,
    o.effectivedate::timestamp::date as Cleaningdate,
    o.status as orderstatus
    
FROM bi.orders_w_marketing o
    
WHERE
    o.effectivedate >= '2015-09-01' and o.effectivedate < '2016-06-01'
    and o.city in ('DE-Berlin', 'DE-Frankfurt am Main', 'DE-Stuttgart', 'DE-Munich', 'DE-Cologne')
    and o.acquisition_channel__c = 'web'

GROUP BY
    booking_duration,
    City,
    Orderid,
    Cleaningdate,
    orderstatus