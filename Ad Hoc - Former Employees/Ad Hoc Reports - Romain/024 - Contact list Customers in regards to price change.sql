SELECT
 o.pph__c as price,
 o.customer_id__c,
 o.city as city,
 COUNT(1) as Orders,
     SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-10' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) as Open_Order
 
FROM bi.orders_w_marketing o

WHERE
 o.city in ('DE-Bonn')
 and o.test__c = '0'
 and o.type = '60'
 and PPH__c < 19.9

GROUP BY 
 price,
 Customer_id__c,
 city

HAVING SUM(CASE WHEN o.Status not like '%CANCELLED%' and o.Effectivedate::date >= '2016-05-10' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) > 0

ORDER BY
 price asc
 
 