SELECT
	o.ordernumber as orderid,
	left(o.city, 2) as country,
	o.marketing_channel as MKGchannel,
	o.gmv_eur as Revenue,
	o.order_duration__c as Duration,
	o.acquisition_new_customer__c as AcquisitionOrRebookinG,
	o.voucher__c as vouchertype

FROM bi.orders_w_marketing o

WHERE
	o.status not in ('CANCELLED FAKED', 'CANCELLED MISTAKE')
	and left(o.city, 2) in ('DE', 'AT')
	and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-03-31')
	and o.test__c = '0'

ORDER BY
	country
 