SELECT
	replace(o.city,'+','') as city,
	LEFT(Locale__c,2) as country,
	Case when o.recurrency__c > 6 then 'Recurrent' else 'One-off' end as recurrency,
	o.pph__c as price,
	o.order_creation__c::timestamp::date as orderd,
	sum(case when o.status in ('INVOICED') then o.order_duration__c else NULL end) as invoiced_hours,
	case when o.type = '60' then 'Muffin' else 'Freelance' end as Programtype

FROM bi.orders_w_marketing o

WHERE
	LEFT(Locale__c,2) = ('de')
	and o.order_creation__c >= '2016-03-03'
	and acquisition_channel__c = 'web'
	and o.test__c = '0'

GROUP BY
	city,
	price,
	country,
	orderd,
	Programtype,
	recurrency

ORDER BY
	city asc,
	orderd asc