SELECT
    count(o.ordernumber) as Nborders
    
FROM bi.orders_w_marketing o

WHERE
    left(o.city, 2) = 'DE'
    and (o.effectivedate >= '2016-01-01' and o.effectivedate <= '2016-01-31')
    and o.status not like '%CANCELLED%'
    and o.type = '60'
    