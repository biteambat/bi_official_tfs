date/time          : 2016-07-27, 13:51:29, 38ms
computer name      : BIPC01
user name          : Asus
registered owner   : Asus
operating system   : Windows 8 Tablet PC x64 build 9200
system language    : German
system up time     : 5 days 22 hours
program up time    : 19 minutes 51 seconds
processors         : 4x Intel(R) Core(TM) i7-4500U CPU @ 1.80GHz
physical memory    : 1168/8075 MB (free/total)
free disk space    : (C:) 458,87 MB
display mode       : 1920x1080, 32 bit
process id         : $2dec
allocated memory   : 88,10 MB
largest free block : 131025,99 GB
executable         : heidisql.exe
exec. date/time    : 2014-11-11 21:38
version            : 9.1.0.4867
compiled with      : Delphi XE5
madExcept version  : 4.0.8.1
callstack crc      : $96fa3b98, $dd38b1b3, $dd38b1b3
exception number   : 1
exception class    : EDatabaseError
exception message  : ERROR: must be superuser or have the same role to cancel queries running in other server processes.

main thread ($2a18):
00a71f2b heidisql.exe dbconnection    2399 +61 TPgConnection.Query
00c365b3 heidisql.exe Main           10891 +15 TMainForm.actCancelOperationExecute
005d90e0 heidisql.exe System.Classes           TBasicAction.Execute
00664463 heidisql.exe Vcl.ActnList             TCustomAction.Execute
005d8e14 heidisql.exe System.Classes           TBasicActionLink.Execute
00693cf7 heidisql.exe Vcl.Controls             TControl.Click
00733594 heidisql.exe Vcl.ComCtrls             TToolButton.Click
00694446 heidisql.exe Vcl.Controls             TControl.WMLButtonUp
0040cb1e heidisql.exe System                   TObject.Dispatch
00693453 heidisql.exe Vcl.Controls             TControl.WndProc
00692f30 heidisql.exe Vcl.Controls             TControl.Perform
0069a0e4 heidisql.exe Vcl.Controls             TWinControl.IsControlMouseMsg
0069a917 heidisql.exe Vcl.Controls             TWinControl.WndProc
0073ab4c heidisql.exe Vcl.ComCtrls             TToolBar.WndProc
00699d5a heidisql.exe Vcl.Controls             TWinControl.MainWndProc
005da693 heidisql.exe System.Classes           StdWndProc
7ffadbf6 user32.dll                            DispatchMessageW
00812f6f heidisql.exe Vcl.Forms                TApplication.ProcessMessage
00812fe3 heidisql.exe Vcl.Forms                TApplication.HandleMessage
008134cf heidisql.exe Vcl.Forms                TApplication.Run
00c57d87 heidisql.exe heidisql          77 +24 initialization
7ffadc81 KERNEL32.DLL                          BaseThreadInitThunk
7ffadc96 ntdll.dll                             RtlUserThreadStart

