SELECT
			CASE 	WHEN AllContacts >= '10' THEN 'A - 10 or more contacts'
		WHEN AllContacts >= '5' AND AllContacts < '10' THEN 'B - 5-9 contacts'
		WHEN AllContacts >= '1' AND AllContacts < '5' THEN 'C - 1-4 contacts'
		WHEN AllContacts = '0' THEN 'no contacts'
		ELSE 'D - Other' END as cluster,
		COUNT(DISTINCT(Contact)) as opportunities
FROM(SELECT
	Contact,
	sum(all_contacts) as allcontacts,
	sum(inbound_calls) as icalls,
	sum(b2b_outbound_calls) as ocalls,
	sum(CreatedCases) as cases
	
		FROM(
		SELECT
			sub1.year,
			sub1.month,
			min(sub1.date) as startdate_of_week,
			sub1.locale,
			sub1.Contact,
			sub1.ContactName,
			sub1.Company,
			sub1.sign_date,
			sub1.first_order_date,
			min(monthly_net_revenue) as monthly_net_revenue,
			
			SUM(CASE WHEN all_inbound_calls IS NULL THEN 0 ELSE all_inbound_calls END) as inbound_calls,
			SUM(CASE WHEN b2b_outbound IS NULL THEN 0 ELSE b2b_outbound END) as b2b_outbound_calls,
			SUM(CASE WHEN CreatedCases IS NULL THEN 0 ELSE CreatedCases END) as CreatedCases,
			SUM(CASE WHEN all_inbound_calls IS NULL THEN 0 ELSE all_inbound_calls END)
			+ SUM(CASE WHEN b2b_outbound IS NULL THEN 0 ELSE b2b_outbound END)
			+ SUM(CASE WHEN CreatedCases IS NULL THEN 0 ELSE CreatedCases END) as all_contacts,		
			SUM(Invoiced_Orders) as Invoiced_Orders,
			SUM(Opp_active) as Opp_active
		FROM
			(SELECT
				EXTRACT(YEAR FROM date) as Year,
				EXTRACT(MONTH FROM date) as Month,
				min(date) as date,
				LEFT(t2.locale__c,2) as locale,
				total_order_count as invoiced_orders,
				COUNT(DISTINCT(opportunity_id)) as Opp_active,
				t2.contact__c as Contact,
				t2.Contact_Name__c as ContactName,
				t2.name as Company,
				t2.sign_date as sign_date,
				t2.first_order_date as first_order_date,
				CASE WHEN t2.grand_total_calc is null THEN total_amount ELSE t2.grand_total_calc END as monthly_net_revenue
				
			FROM
				bi.b2borders t2
			GROUP BY
				Year,
				Month,
				locale,
				contact,
				total_order_count,
				t2.contact_name__c,
				t2.name,
				t2.sign_date,
				t2.first_order_date,
				monthly_net_revenue
		
	)  as sub1
		
			LEFT JOIN
				(SELECT
					EXTRACT(Month FROM call_start_date_date__c::date) as 
Month,
					EXTRACT(YEAR FROM call_start_date_date__c::date) as 
Year,
					CASE 
						WHEN calledcountryshort__c = 'AUT' THEN 'at'
						WHEN calledcountryshort__c = 'CHE' THEN 'ch'
						WHEN calledcountryshort__c = 'NLD' THEN 'nl'
						ELSE 'de' END as locale,
					SUM(CASE WHEN callconnected__c ='No' AND 
calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END)+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' THEN 1 ELSE 0 END) as all_inbound_calls,
					SUM(CASE WHEN callconnected__c ='Yes' AND 
calldirection__c = 'Outbound' THEN 1 ELSE 0 END) AS all_outbound,
					SUM(CASE WHEN callconnected__c ='Yes' AND 
calldirection__c = 'Outbound' AND t2.type__c= 'customer-b2b' THEN 1 ELSE 0 END) AS b2b_outbound,
					RelatedContact__c as Contact
				FROM 
					Salesforce.natterbox_call_reporting_object__c t1
				LEFT JOIN 
					salesforce.contact t2
				ON
					(t1.relatedcontact__c = t2.sfid)
				WHERE
				call_start_date_date__c::date between current_date - INTERVAL 
'60 Days' and current_date
				AND
				((E164Callednumber__C = '493030807263'
					OR E164Callednumber__C='493030807264')
					AND calldirection__c = 'Inbound')
				OR
				(Callerfirstname__c like 'CM%'
					AND calldirection__c = 'Outbound')
				GROUP BY
				locale,
				Month,
				Year,
				Contact) as sub2
				ON
				(sub1.month = sub2.month AND sub1.locale = sub2.locale and 
sub1.year = sub2.year and sub1.Contact = sub2.Contact)
		
				LEFT JOIN
					(SELECT 
						EXTRACT(Year from CreatedDate::date) AS Year,
						EXTRACT(Month from CreatedDate::date) as Month,
						CASE
							WHEN (Origin LIKE '%B2B - Contact%' OR 
Origin LIKE '%B2B DE - CLOSET%' OR Origin LIKE '%checkout%' OR Origin LIKE '%CM DE%' OR Origin LIKE '%TM DE - General Email%')  THEN 'de'
							WHEN (Origin LIKE '%B2B CH - 
CLOSET%' OR Origin LIKE '%CM CH%' OR Origin LIKE '%TM CH - General Email%') THEN 'ch'
							WHEN (Origin LIKE '%B2B AT - CLOSET%' 
OR Origin LIKE '%CM AT%' OR Origin LIKE '%TM AT - General Email%') THEN 'at'
							WHEN (Origin LIKE '%B2B NL - CLOSET%' 
OR Origin LIKE '%CM NL%' OR Origin LIKE '%TM NL - General Email%') THEN 'nl'
							ELSE 'Other' END AS locale,
						COUNT(1) as CreatedCases,
						Contactid as Contact
					
					FROM
						Salesforce.case		
					WHERE
						CreatedDate::date between current_date - 
INTERVAL '60 Days' and current_date
						AND 
						(type LIKE '%KA%'
							OR
							((Origin LIKE '%B2B - Contact%'
								OR Origin LIKE '%B2B AT - CLOSET%'
								OR Origin LIKE '%B2B CH - CLOSET%'
								OR Origin LIKE '%B2B DE - CLOSET%'
								OR Origin LIKE '%B2B NL - CLOSET%')
								AND type != 'KA'))
						AND Isdeleted = false
						AND CreatedById = '00520000003IiNCAA0'
					GROUP BY 
						locale, 
						Year,
						Month,
						Contact
						)
					AS sub3 
					
						ON
						(sub1.month = sub3.month and sub1.year = 
sub3.year and sub1.locale = sub3.locale and sub1.Contact = sub3.Contact)
						GROUP BY
						sub1.year,
						sub1.month,
						sub1.locale,
						sub1.Contact,
						sub1.ContactName,
						Company,
						sign_date,
						first_order_date) as a			
	WHERE
		startdate_of_week between current_date - INTERVAL '60 Days' and current_date
	GROUP BY
		contact) as overall
GROUP BY
	cluster
			