

SELECT
	marketing_channel,
		CASE
	WHEN referrer_url like '%indeed%' or referrer_url like '%apply.bookatiger.com%' or referrer_url like '%careerjet%' or referrer_url like '%aktuelle-jobs%' or referrer_url like '%adzuna%' THEN 'Job Ads'
	WHEN referrer_url like '%clid=goog%' THEN 'SEM'
	WHEN referrer_url like '%clid=goob%' THEN 'SEM Brand'
	WHEN referrer_url like '%google%' and referrer_url not like '%clid=goob%' and referrer_url not like '%clid=goog%' THEN 'SEO'
	WHEN referrer_url like '%gdtiger20%' or referrer_url like '%GDTIGER20%' THEN 'Display Remarketing'
	WHEN referrer_url like '%fbtiger20%' or referrer_url like '%FBTIGER20%' or referrer_url like '%FBTIGER50%' or referrer_url like '%batfb%' then 'Facebook'
	WHEN referrer_url like '%facebook%' or referrer_url like '%instagram%' Then 'Facebook Organic'
	WHEN referrer_url like '%freenet%' or referrer_url like '%outlook%' or referrer_url like '%gmx%' or referrer_url like '%t-online%' or referrer_url like '%bluewin%' THEN 'Newsletter'
	WHEN referrer_url not like '%bookatiger.com%' THEN 'Referral'
	
	ELSE Marketing_channel end as marketing_channel_new,
	utm_source,
	utm_medium,
	utm_campaign,
	utm_content,
	utm_term,
	clid,
	referrer_url
FROM
	bi.etl_piwik_visits
WHERE
	server_date::date > '2017-01-01'
	and Marketing_Channel = 'DTI'
	and referrer_url is not null