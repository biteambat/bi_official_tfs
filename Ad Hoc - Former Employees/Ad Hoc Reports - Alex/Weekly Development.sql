SELECT
	EXTRACT(WEEK FROM effectivedate::date) as week,
	polygon,
	CASE 
	WHEN recurrency__c between 1 and 7 THEN '7'
	WHEN recurrency__c between 8 and 14 THEN '14'
	WHEN recurrency__c between 15 and 28 THEN '28'
	ELSE '0' END as recurrent,
	min(effectivedate::date) as mindate,
	SUM(GMV_eur_net) as GMV_eur_net,
	COUNT(DISTINCT(contact__c)) as unique_customers,
	COUNT(DISTINCT(case when acquisition_new_customer__C = '1' THEN contact__c ELSE NULL END)) as acquisitions,
	COUNT(DISTINCT(case when acquisition_new_customer__c = '0' and acquisition_channel__c = 'web' THEN contact__c ELSE NULL END)) as reactivated_customers,
	COUNT(DISTINCT(case when acquisition_new_customer__c = '0' and acquisition_channel__c != 'web' THEN contact__c else null end)) as recurrent_customers

FROM
	bi.orders
WHERE
	status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
	and effectivedate::date between '2016-11-13' and '2017-02-05'
	and order_type = '1'
GROUP BY
	week,
	polygon,
	recurrent
ORDER BY
	mindate