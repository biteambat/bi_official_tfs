SELECT
	sfid,
	plan_pph__c,
	name,
	email,
	locale__c
FROM(
SELECT
	DISTINCT(contact__c)
FROM(
SELECT
 contact__c,
 sfid as sfid,
 pph__c as price,
 effectivedate,
 row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as order_sequence
  
FROM bi.orders

WHERE
 (polygon != 'de-berlin' or polygon is null)
 and left(locale__c,2) = 'de'
 and test__c = '0'
 and pph__c < 17.9
 and order_type = '1'

GROUP BY 
 price,
 contact__c,
 sfid,
 effectivedate

HAVING SUM(CASE WHEN status not like '%CANCELLED%' and effectivedate::date >= '2017-01-18' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) > 0

ORDER BY
 contact__c,
 order_sequence) as a ) as t1
LEFT JOIN
	salesforce.contact t2
ON
	(t1.contact__c =  t2.sfid)


SELECT
 contact__c,
 sfid as sfid,
 pph__c as price,
 effectivedate,
 row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as order_sequence
  
FROM bi.orders

WHERE
 (polygon != 'de-berlin' or polygon is null)
 and left(locale__c,2) = 'de'
 and test__c = '0'
 and pph__c < 17.9
 and order_type = '1'

GROUP BY 
 price,
 contact__c,
 sfid,
 effectivedate

HAVING SUM(CASE WHEN status not like '%CANCELLED%' and effectivedate::date >= '2017-01-18' and acquisition_channel__c = 'recurrent' THEN 1 ELSE 0 END) > 0

ORDER BY
 contact__c,
 order_sequence