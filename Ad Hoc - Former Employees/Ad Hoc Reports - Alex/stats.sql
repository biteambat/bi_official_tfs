DROP TABLE IF EXISTS bi.nmp_per_cleaner;
CREATE TABLE bi.nmp_per_cleaner as
SELECT
	CASE WHEN t1.professional__c is null THEN t3.professional__c ELSE t1.professional__c END,
	t1.date,
	t1.order_Start__c,
	creation_date,
	t1.recurrency__c,
	Order_Id__c
FROM
(SELECT
	effectivedate::date as date,
	Order_Start__c,
	Order_Duration__c,
	sfid,
	contact__c,
	recurrency__c,
	Order_Creation__c::date as creation_date,
	professional__c,
	LEFT(locale__c,2) as locale
FROM
		bi.orders
WHERE
	Order_type = '1'
	and effectivedate::date between  '2016-07-11' and '2016-07-23'
	and status = 'CANCELLED NO MANPOWER'
	and test__c = '0'
	and acquisition_channel__c = 'recurrent' and recurrency__c > '0'
	and LEFT(locale__c,2) = 'de') as t1
LEFT JOIN
	(SELECT
	 CONTACT__c,
	 MAX(Effectivedate::date) as Last_Invoiced_Order
FROM
		bi.orders
WHERE
	Order_type = '1'
	and status = 'INVOICED'
	and test__c = '0'
	and LEFT(locale__c,2) = 'de'
GROUP BY
	contact__c) as t2
ON
	(t1.contact__c = t2.contact__c)
LEFT JOIn
	bi.orders t3
ON
	(t2.contact__c = t3.contact__c and t2.last_invoiced_Order = t3.effectivedate::date)
WHERE
	t1.date > t2.last_invoiced_order
