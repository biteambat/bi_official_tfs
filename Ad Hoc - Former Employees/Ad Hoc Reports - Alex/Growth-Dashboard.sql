DROP TABLE bi.city_gmv;
CREATE TABLE bi.city_gmv as 
select
	CASE WHEN t2.city is null THEN 'Other' ELSE t2.city END as new_city,
	MIN(t1.Effectivedate::date) as date,
	EXTRACT(WEEK FROM Effectivedate::date) as week,
	SUM(Order_Duration__c) as Hours
from
    bi.orders_w_marketing t1
left join
    bi.city_radius_v3 t2
on
	(t1.Order_Id__c = t2.Order_Id__c and OrderNo = '1')
WHERE
	Effectivedate::date > '2016-01-15'
	and left(locale__c,2) = 'de'
	and status in ('INVOICED')
GROUP BY
	new_city,
	week;

DROP TABLE IF EXISTS bi.growth_table;
CREATE TABLE bi.growth_table as 
SELECT
	t1.new_city,
	t1.mindate as date,
	t2.mindate,
	t1.GMV as Hours_Current,
	t2.GMV as Hours_4weeksago
FROM
(SELECT
	t1.new_city,
	week,
	min(date) as mindate,
	SUM(Hours) as GMV
FROM
	bi.city_gmv t1
GROUP BY
	new_city,
	week) as t1
LEFT JOIN
(SELECT
	t1.new_city,
	min(date) as mindate,
	week,
	SUM(Hours) as GMV
FROM
	bi.city_gmv t1
GROUP BY
	new_city,
	week) as t2
ON
	(t1.week = (t2.week+4) and t1.new_city = t2.new_city );	
