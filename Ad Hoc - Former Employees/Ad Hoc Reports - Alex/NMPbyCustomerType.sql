
SELECT
	polygon,
	CASE 
	WHEN acquisition_channel__c = 'web' and recurrency__c > '0' THEN 'New Recurrent Customer'
	WHEN acquisition_channel__c = 'recurrent' and recurrency__c > '0' THEN 'Existing Recurrent Customer'
	WHEN acquisition_channel__c = 'web' and recurrency__c = '0' THEN 'Trial Customer'
	ELSE 'Others' END as customer_type,
	SUM(Order_Duration__c) as Hours
FROM
	bi.orders
WHERE
	Order_type = '1'
	and effectivedate::date between  '2016-07-11' and '2016-07-23'
	and status = 'CANCELLED NO MANPOWER'
	and test__c = '0'
GROUP BY
	polygon,
	customer_type	

		
	