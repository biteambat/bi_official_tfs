DROP VIEW IF EXISTS bi.customer_segmentation;

CREATE VIEW bi.customer_segmentation as 

SELECT
 contact__c,
 CASE
 WHEN (country in ('de','at','nl') and GMV_Last30D > 120) or (country = 'ch' and GMV_Last30D > 195) THEN CAST('A' as varchar)
 WHEN (country in ('de','at','nl') and GMV_Last30D between 80 and 120) or (country = 'ch' and GMV_Last30D between 130 and 195) THEN CAST('B' as varchar)
 WHEN (country in ('de','at','nl') and GMV_Last30D < 80) or (country = 'ch' and GMV_Last30D < 130) THEN CAST('C' as varchar)
 ELSE 'Other'::varchar END as segmentation

FROM(

SELECT
 contact__c,
 left(locale__c,2) as country,
 SUM(GMV_eur) as GMV_Last30D
 
FROM
 bi.orders

WHERE
 order_type = '1'
 and status = 'INVOICED'
 and effectivedate::date between current_date - 32 and current_date - 2
 and left(locale__c,2) in ('de','at','nl')

GROUP BY
 contact__c,
 country) 
 
 as t1;