
SELECT
	t1.contact__c,
	t1.professional__c,
	t1.order_start__c,
	t1.date,
	t2.Last_Invoiced_order,
	t3.professional__c,
	t4.holiday_flag as holiday_order_cleaner,
	t4.sickness_flag as sickness_order_cleaner,
	t5.holiday_flag as holiday_v2_order_cleaner,
	t5.sickness_flag as sickness_v2_order_cleaner
FROM
(SELECT
	effectivedate::date as date,
	Order_Start__c,
	Order_Duration__c,
	sfid,
	contact__c,
	professional__c,
	LEFT(locale__c,2) as locale
FROM
		bi.orders
WHERE
	Order_type = '1'
	and effectivedate::date between  '2016-07-11' and '2016-07-23'
	and status = 'CANCELLED NO MANPOWER'
	and test__c = '0'
	and acquisition_channel__c = 'recurrent' and recurrency__c > '0'
	and LEFT(locale__c,2) = 'de') as t1
LEFT JOIN
	(SELECT
	 CONTACT__c,
	 MAX(Effectivedate::date) as Last_Invoiced_Order
FROM
		bi.orders
WHERE
	Order_type = '1'
	and status = 'INVOICED'
	and test__c = '0'
	and LEFT(locale__c,2) = 'de'
GROUP BY
	contact__c) as t2
ON
	(t1.contact__c = t2.contact__c)
LEFT JOIn
	bi.orders t3
ON
	(t2.contact__c = t3.contact__c and t2.last_invoiced_Order = t3.effectivedate::date)
LEFT JOIN
	bi.holidays_per_cleaner t4
ON
	(t3.professional__c = t4.account__c and t1.date = t4.date)
LEFT JOIN
	bi.holidays_per_cleaner t5
ON
	(t1.professional__c = t5.account__c and t1.date = t5.date)
WHERE
	t1.date > t2.last_invoiced_order
