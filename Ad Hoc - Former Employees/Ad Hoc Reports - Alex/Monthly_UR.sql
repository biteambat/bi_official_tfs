DELIMITER //
CREATE OR REPLACE FUNCTION bi.daily$URMonthly(crunchdate date) RETURNS void AS 
$BODY$
BEGIN


DROP TABLE IF EXISTS bi.util_month_1;
CREATE TABLE bi.util_month_1 as 	
SELECT
	t2.sfid,
	t2.working_city__c,
	t1.Effectivedate::Date as date,
	SUM(Order_Duration__c) as Worked_Hours
FROM
	Salesforce.Account t2
LEFT JOIN 
	Salesforce.Order t1
ON
	(t1.professional__c = t2.sfid and t1.test__c = '0' and t1.status in ('PENDING TO START','INVOICED'))
WHERE
	t2.type__c = '60'
GROUP BY
	t2.sfid,
	t2.working_city__c,
	date;

DROP TABLE IF EXISTS bi.util_month_2;
CREATE TABLE bi.util_month_2 as 	
SELECT
	sfid,
	date,
	t2.working_city__c,
	hr_contract_end__c::date as end_date,
	hr_contract_start__c::Date as onboarding_date,
	hr_contract_weekly_hours_min__c as weekly_hours,
	hr_contract_weekly_hours_min__c*4.3 as monthly_hours
FROM
	Salesforce.Account t2,
	bi.orderdate t1
WHERE
	type__c = '60'
	and t1.date > '2015-08-01'
GROUP BY
	sfid,
	date,
	t2.working_city__c,
	onboarding_date,
	end_date,
	weekly_hours,
	monthly_hours;

DROP TABLE IF EXISTS bi.utalization_temp3;
CREATE TABLE  bi.utalization_temp3 as 	
SELECT
	t1.date,
	t1.working_city__c,
	onboarding_date,
	end_date::date as end_date,
	t1.sfid,
	weekly_hours,
	monthly_hours,
	worked_hours
FROM
	bi.util_month_2 t1
LEFT JOIN
	bi.util_month_1 t2
ON
	(t1.sfid = t2.sfid and t1.date = t2.date)
WHERE
	t1.date::date between '2015-10-26' and '2016-06-30';


DROP TABLE IF EXISTS bi.utalization_per_cleaner;
CREATE TABLE bi.utalization_per_cleaner as 
SELECT
	EXTRACT(MONTH FROM date) as Month,
	CAST(min(date) as date) as min_date,
	end_date,
	sfid,
	onboarding_date,
	working_city__c,
	COUNT(DISTINCT(EXTRACT(WEEK FROM DATE))) as Weeks,
	SUM(Worked_Hours) as worked_hours,
	weekly_hours 
FROM
	bi.utalization_temp3
WHERE
	date > onboarding_date
GROUP BY
	Month,
	working_city__c,
	onboarding_date,
	end_date,
	sfid,
	weekly_hours;

DROP TABLE IF EXISTS bi.utalization_per_cleaner_temp1 ;
CREATE TABLE bi.utalization_per_cleaner_temp1 as 
SELECT
	Month,
	min_date,
	end_date,
	sfid,
	onboarding_date,
	working_city__c,
	weeks,
	worked_hours,
	weekly_hours,
	SUM(CASE WHEN weeks = 5 THEN 4.3*weekly_hours ELSE weeks*weekly_hours END) as monthly_capacity,
	CASE WHEN worked_hours > SUM(CASE WHEN weeks = 5 THEN 4.3*weekly_hours ELSE weeks*weekly_hours END) THEN SUM(CASE WHEN weeks = 5 THEN 4.3*weekly_hours ELSE weeks*weekly_hours END) ELSE worked_hours END as worked_hours_cap
FROM
	bi.utalization_per_cleaner 
GROUP BY
		Month,
	min_date,
	end_date,
	sfid,
	onboarding_date,
	working_city__c,
	weeks,
	worked_hours,
	weekly_hours;

DROP TABLE IF EXISTS bi.utalization_per_cleaner_monthly;
CREATE TABLE bi.utalization_per_cleaner_monthly as
SELECT
	month,
	min(min_date) as date,
	working_city__c as city,
	SUM(worked_hours_cap) as worked_hours_w_cap,
	SUM(monthly_capacity) as monthly_capacity,
	SUM(worked_hours_cap)/SUM(monthly_capacity) as Utilization
FROM
	bi.utalization_per_cleaner_temp1
WHERE
	end_date > min_date
	and extract(month from min_date) != extract(month from end_date)
	and month in (11,12,1,2,3,4,5,6)
GROUP BY
	Month,
	City;

INSERT INTO bi.utalization_per_cleaner_monthly
SELECT
	month,
	min(min_date) as date,
	'DE-Total' as city,
	SUM(worked_hours_cap) as worked_hours_w_cap,
	SUM(monthly_capacity) as monthly_capacity,
	SUM(worked_hours_cap)/SUM(monthly_capacity) as Utilization
FROM
	bi.utalization_per_cleaner_temp1
WHERE
	end_date > min_date
	and extract(month from min_date) != extract(month from end_date)
	and month in (11,12,1,2,3,4,5,6)
GROUP BY
	Month;

-- Rolling Utilization Rate

DROP TABLE IF EXISTS bi.utalization_per_cleaner_rolling;
CREATE TABLE bi.utalization_per_cleaner_rolling as 
SELECT
	EXTRACT(WEEK FROM (cast(current_date as date))) - 1 as week,
	min(date) as mindate,
	end_date,
	sfid,
	onboarding_date,
	working_city__c as city,
	EXTRACT(WEEK FROM (cast('2016-04-18' as date) - INTERVAL '28 days')) as start_week,
	EXTRACT(WEEK FROM (cast('2016-04-18' as date)))-1 as end_week,
	COUNT(DISTINCT(EXTRACT(WEEK FROM DATE))) as Weeks,
	COUNT(DISTINCT(DATE)) as dates,
	SUM(Worked_Hours) as worked_hours,
	weekly_hours 
FROM
	bi.utalization_temp3
WHERE
	date > onboarding_date
	and EXTRACT(WEEK FROM DATE) between EXTRACT(WEEK FROM (cast(current_date as date) - INTERVAL '28 days')) and EXTRACT(WEEK FROM (cast(current_date as date)))-1
GROUP BY
	week,
	working_city__c,
	onboarding_date,
	end_date,
	sfid,
	weekly_hours;

DROP TABLE IF EXISTS bi.utalization_per_cleaner_temp1_rolling;
CREATE TABLE bi.utalization_per_cleaner_temp1_rolling as 
SELECT
	week,
	mindate as min_date,
	city,
	end_date,
	sfid,
	onboarding_date,
	weeks,
	worked_hours,
	weekly_hours,
	SUM(CASE WHEN weeks = 5 THEN 4.3*weekly_hours ELSE weeks*weekly_hours END) as monthly_capacity,
	CASE WHEN worked_hours > SUM(CASE WHEN weeks = 5 THEN 4.3*weekly_hours ELSE weeks*weekly_hours END) THEN SUM(CASE WHEN weeks = 5 THEN 4.3*weekly_hours ELSE weeks*weekly_hours END) ELSE worked_hours END as worked_hours_cap
FROM
	bi.utalization_per_cleaner_rolling 
GROUP BY
	week,
	mindate,
	end_date,
	sfid,
	city,
	onboarding_date,
	city,
	weeks,
	worked_hours,
	weekly_hours;


DELETE FROM bi.utalization_per_cleaner_monthly_rolling WHERE EXTRACT(WEEK FROM (cast(current_date as date)))-1 = WEEK;

INSERT INTO bi.utalization_per_cleaner_monthly_rolling
SELECT
	week,
	city,
	cast(current_date as date)-1 as date,
	SUM(worked_hours_cap) as worked_hours_w_cap,
	SUM(monthly_capacity) as monthly_capacity,
	SUM(worked_hours_cap)/SUM(monthly_capacity) as Utilization
FROM
	bi.utalization_per_cleaner_temp1_rolling
WHERE
	end_date > min_date
	and extract(month from min_date) != extract(month from end_date)
GROUP BY
	week,
	City;

INSERT INTO bi.utalization_per_cleaner_monthly_rolling
SELECT
	week,
	'DE-Total' as city,
	cast(current_date as date)-1 as date,
	SUM(worked_hours_cap) as worked_hours_w_cap,
	SUM(monthly_capacity) as monthly_capacity,
	SUM(worked_hours_cap)/SUM(monthly_capacity) as Utilization
FROM
	bi.utalization_per_cleaner_temp1_rolling
WHERE
	end_date > min_date
	and extract(month from min_date) != extract(month from end_date)
GROUP BY
	week;

DROP TABLE IF EXISTS bi.util_month_1;
DROP TABLE IF EXISTS bi.util_month_2;
DROP TABLE IF EXISTS bi.utalization_temp3;
DROP TABLE IF EXISTS bi.utalization_per_cleaner_temp1 ;
DROP TABLE IF EXISTS bi.utalization_per_cleaner_temp1_rolling;


 END;

$BODY$ LANGUAGE 'plpgsql'