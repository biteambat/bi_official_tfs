SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Month,
	COUNT(DISTINCT(t1.sfid)) as Signings,
	COUNT(DISTINCT(t2.contact__c)) as customer
FROM
	Salesforce.Opportunity t1
LEFT JOIN
	bi.orders t2
ON
	(t1.sfid = t2.opportunityid and t2.status not like '%CANCELLED%')
WHERE
	t1.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED')
GROUP BY
	Month