DROP TABLE IF EXISTS bi.overlapping_orders;
CREATE TABLE bi.overlapping_orders as 
SELECT
	t2.professional__c,
	t1.date as orderdate,
	order_Start,
	polygon,
	acquisition_customer_creation__c as acquisition_date,
	status,
	order_id__c,
	contact__c
FROM
(SELECT
	professional__c,
	effectivedate::date as date,
	COUNT(1)
FROM
	bi.orders
WHERE
		Effectivedate::date between '2016-08-15' and '2016-08-29'
	and status not like '%CANCELLED%'
	and order_type = '1'
GROUP BY
	professional__c,
	effectivedate::date
HAVING
	count(1) > 0) as t1
LEFT JOIN(
SELECT
	professional__c,
	effectivedate::Date as date,
	Order_start__c + Interval '2 Hours' as order_start,
	Status,
	Order_id__c,
	polygon,
	acquisition_customer_creation__c,
	contact__c
FROM
	bi.orders
WHERE
	Effectivedate::date between '2016-08-15' and '2016-08-29'
	and status not like '%CANCELLED%' and status not like '%ERROR%'
	and order_type = '1') as t2
ON
	(t1.professional__c = t2.professional__c and t1.date = t2.date)

SELECT
	polygon,
	COUNT(DISTINCT(Order_Id__c)) as Unique_Orders
FROM
	bi.overlapping_orders 
GROUP BY
	Polygon



SELECT
	polygon,
	SUM(CASE WHEN acquisition_date >= '2016-07-01' THEN 1 ELSE 0 END) as acquisition_july_august,
	SUM(gmv_eur) as GMV,
	COUNT(DISTINCT(order_Id__c)) as orders
FROM(
SELECT
	a1.*,
	MAX(CASE WHEN a2.orderid is not null THEN 1 ELSE 0 END) as overlapping_order
FROM
(SELECT
	professional__c,
	acquisition_customer_creation__c as acquisition_date,
	effectivedate::Date as date,
	Order_start__c + Interval '2 Hours' as order_start,
	Status,
	polygon,
	gmv_eur,
	Order_id__c,
	contact__c
FROM
(SELECT
	DISTINCT(professional) as Distinct_Cleaner
FROM(	
SELECT
	t1.order_Id__c as OrderId,
	t2.order_Id__c,
	t1.polygon,
	t1.acquisition_date,
	t1.professional__c as professional,
	t1.order_start as order_start_time,
	t2.order_start,
	t1.order_start - t2.order_start as timedifference_to_next_order
FROM
	bi.overlapping_orders t1
LEFT JOIN
	bi.overlapping_orders t2
ON
	(t1.professional__c = t2.professional__c and t1.orderdate = t2.orderdate and t1.order_Id__c != t2.order_Id__c)
WHERE
	t1.order_Id__c is not null) as a
WHERE
	timedifference_to_next_order between '-03:30:00' and '03:30:00') as a
LEFT JOIN
	bi.orders b
oN
	(a.distinct_cleaner = b.professional__c)
WHERE
	Effectivedate::date between '2016-08-15' and '2016-08-29'
	and status not like '%CANCELLED%' and status not like '%ERROR%'
	and order_type = '1') as a1
LEFT JOIN
	(SELECT
	t1.order_Id__c as OrderId,
	t2.order_Id__c,
	t1.professional__c as professional,
	
	t1.order_start as order_start_time,
	t2.order_start,
	t1.order_start - t2.order_start as timedifference_to_next_order
FROM
	bi.overlapping_orders t1
LEFT JOIN
	bi.overlapping_orders t2
ON
	(t1.professional__c = t2.professional__c and t1.orderdate = t2.orderdate and t1.order_Id__c != t2.order_Id__c)
WHERE
	t1.order_Id__c is not null
) as a2
ON
	(a2.orderid = a1.order_id__c and a2.timedifference_to_next_order between '-03:30:00' and '03:30:00')

GROUP BY
		a1.professional__c,
	a1.date,
	 a1.order_start,
a1.	Status,
a1.	Order_id__c,
	a1.contact__c,
	a1.gmv_eur,
	a1.acquisition_date,
	a1.polygon,
	a1.order_start
HAVING
		MAX(CASE WHEN a2.orderid is not null THEN 1 ELSE 0 END) = '1'
ORDER BY
	professional__c,
	a1.date,
	a1.order_start) as a123
GROUP BY
	polygon