SELECT
	opportunityid,
	t2.name,
	MAX(effectivedate::date) as Last_Executed_Order,
	
	SUM(CASE WHEN effectivedate::date < current_date::date THEN 1 ELSE 0 END) as Orders_Past,
	SUM(CASE WHEN effectivedate::date >= current_date::date THEN 1 ELSE 0 END) as Orders_Future
FROM
	bi.orders t1
LEFT JOIn
	salesforce.Opportunity t2
ON
	(t1.opportunityid = t2.sfid)
WHERE
	order_type = '2'
	and status not like '%CANCELLED%'
	and left(t1.locale__c,2) = 'de'
GROUP BY
	opportunityid,
	t2.name
HAVING
		SUM(CASE WHEN effectivedate::date >= current_date::date THEN 1 ELSE 0 END) = 0