SELECT
	EXTRACT(WEEK from effectivedate::date) as CW,
	polygon,
	MIN(effectivedate::date) as mindate,
	SUM(CASE WHEN status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN GMV_eur_net ELSE 0 END) as GMV_eur_net,
	COUNT(DISTINCT(CASE WHEN status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') and acquisition_new_Customer__c = '1' THEN Contact__c ELSE NULL END)) as all_customers,
	COUNT(DISTINCT(CASE WHEN status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN Contact__c ELSE NULL END)) as all_customers,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '0' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN contact__c ELSE null END)) as Trial_Customer,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '28' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN contact__c ELSE null END)) as recurrent28d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '14' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN contact__c ELSE null END)) as recurrent14d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '7' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')THEN contact__c ELSE null END)) as recurrent7d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '0' and acquisition_new_customer__c = '1' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN contact__c ELSE null END)) as acq_Trial_Customer,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '28' and acquisition_new_customer__c = '1' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN contact__c ELSE null END)) as acq_recurrent28d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '14' and acquisition_new_customer__c = '1' and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER') THEN contact__c ELSE null END)) as acq_recurrent14d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '7' and acquisition_new_customer__c = '1'  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')THEN contact__c ELSE null END)) as acq_recurrent7d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '0' and status = 'CANCELLED CUSTOMER' THEN contact__c ELSE null END)) as Trial_Customer,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '28' and status = 'CANCELLED CUSTOMER' THEN contact__c ELSE null END)) as recurrent28d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '14' and status = 'CANCELLED CUSTOMER' THEN contact__c ELSE null END)) as recurrent14d,
	COUNT(DISTINCT(CASE WHEN recurrency__c = '7' and status = 'CANCELLED CUSTOMER' THEN contact__c ELSE null END)) as recurrent7d
FROM
	bi.orders
WHERE
	order_type = '1'
	and effectivedate::date between '2017-01-09' and '2017-02-05'
GROUP BY
	CW,
	polygon
ORDER by
	polygon,
	cw