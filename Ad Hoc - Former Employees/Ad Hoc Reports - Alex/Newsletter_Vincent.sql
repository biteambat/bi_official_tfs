SELECT
	a.customer_name__c,
	left(a.locale__c,2) as locale,
	right(a.locale__c,2) as language,
	a.customer_email__c,
	polygon
FROM(

SELECT
	Contact__c,
	Effectivedate::date,
	status, 
	locale__c,
	customer_name__c,
	customer_email__c,
	shippingpostalcode,
	shippingcity,
	type,
	polygon,
	rank() OVER (PARTITION BY contact__c ORDER BY effectivedate asc) as ranking
FROM
	bi.orders
WHERE
	Status not like '%CANCELLED%' and Status not in ('INVOICED','ERROR GEO','ERROR INTERNAL','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','FULFILLED')
	and Effectivedate::date > current_date 
	and LEFT(locale__c,2) = 'de'
	and type not like '%222%' and type not like '%60%'
	and recurrency__c >= '6'
	and polygon is null) as a
LEFT JOIN
	Salesforce.contact c
ON
	(a.contact__c = c.sfid)
WHERE
	ranking = '1'
