DROP TABLE IF EXISTS bi.terminated_customer_20170206;
CREATE TABLE bi.terminated_customer_20170206 as 
SELECT
	order_Json->>'Order_Id__c' as order_id,
	created_at::date as date
FROM
	events.sodium
WHERE
	created_at::date >= '2016-10-01'
	and event_name in ('Order Event:CANCELLED TERMINATED');


SELECT
	EXTRACT(WEEK FROM date::date) as Week,
	polygon,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.terminated_customer_20170206 t1
JOIN
	bi.orders t2
ON
	(t1.order_id = t2.order_id__c)
WHERE
	polygon = 'de-berlin'
GROUP BY
	Week,
	polygon;

SELECT
	*
FROM(
SELECT
	EXTRACT(WEEK FROM min_start_date) as Week,
	min(min_start_date) as mindate,
	polygon,
	recurrency__c,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM(SELECT
	contact__c,
	recurrency__c,
	polygon,
	min(order_date) as min_start_date
FROM
	(
SELECT
	t1.*,
	contact__c,
	recurrency__c,
	polygon,
	t2.effectivedate::date as order_date,
	status
FROM
	bi.terminated_customer_20170206 t1
JOIN
	bi.orders t2
ON
	(t1.order_id = t2.order_id__c)
ORDER BY
	contact__c,
	order_date) as a
GROUP BY
	contact__c,
	recurrency__c,
	polygon) as b
WHERE
	polygon in ('de-berlin','de-munich','de-hamburg')
	and min_start_date::date < '2017-02-06'
GROUP BY
	Week,
	Polygon
ORDER By
	polygon,
	recurrency__c,
	mindate) as a
LEFT JOIN
(SELECT
	
	EXTRACT(WEEK FROM effectivedate::date) as week,
	polygon,
	recurrency__c,
	MIN(effectivedate::date) as mindate,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.orders
WHERE
	status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
	and effectivedate::Date between '2016-10-01' and '2017-02-05'
	and test__c = '0'	
	and acquisition_channel__c = 'web'
	and	polygon in ('de-berlin','de-munich','de-hamburg')
	and recurrency__c = '0'
GROUP BY
	week,
	recurrency__c,
	polygon
ORDER BY
	mindate,
	polygon) as b
ON
	(
	
