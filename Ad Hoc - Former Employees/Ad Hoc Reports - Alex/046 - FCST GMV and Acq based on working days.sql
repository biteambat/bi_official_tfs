

SELECT
			LEFT(o.locale__c,2) as locale,

			o.polygon as city_polygon,

			(SELECT count(*)-1 as wds_gone_currMonth
			FROM generate_series(0, (current_date::date - (current_date - interval '15 days')::date::date)) i
			WHERE date_part('dow', (current_date - interval '15 days')::date::date + i) NOT IN (0,6))
			as workingdays_in_L15D,

			((SELECT count(*) 
			FROM generate_series(0, (((date_trunc('MONTH', current_date::date) + INTERVAL '1 MONTH - 1 day')::date) - current_date::date::date)) i
			WHERE date_part('dow', current_date::date::date + i) NOT IN (0,6)))		
			as workingdays_left_currMonth,

			SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and current_date::date - interval '1 day') THEN o.gmv_eur ELSE 0 END) 
			as invoiced_gmv_L14D,

			SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END) 
			as invoiced_gmv_currmonth,

			SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and current_date::date - interval '1 day') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE 0 END) 
			as invoiced_acq_L14D,

			SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END) 
			as invoiced_acq_currmonth,





			 (SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur_net ELSE 0 END))

			 + 

			 (
			 	(((SELECT count(*) 
			FROM generate_series(0, (((date_trunc('MONTH', current_date::date) + INTERVAL '1 MONTH - 1 day')::date) - current_date::date::date)) i
			WHERE date_part('dow', current_date::date::date + i) NOT IN (0,6)))	)

			 	*

			 	((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and current_date::date - interval '1 day')  THEN o.gmv_eur ELSE 0 END))/((SELECT count(*)-1 as wds_gone_currMonth
			FROM generate_series(0, (current_date::date - (current_date - interval '15 days')::date::date)) i
			WHERE date_part('dow', (current_date - interval '15 days')::date::date + i) NOT IN (0,6))))
			 )

			as forecast_invoiced_gmv,






			 (SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END))

			 + 

			 (
			 	(	((SELECT count(*) 
			FROM generate_series(0, (((date_trunc('MONTH', current_date::date) + INTERVAL '1 MONTH - 1 day')::date) - current_date::date::date)) i
			WHERE date_part('dow', current_date::date::date + i) NOT IN (0,6))))

			 	*

			 	(
			 	
			 	(SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate between current_date::date - interval '15 days' and 				current_date::date - interval '1 day') AND o.acquisition_new_customer__c = '1' and date_part('dow',Effectivedate::date) not in ('0','6') THEN 1 ELSE 0 END))
			 	
			 	
			 	/((SELECT count(*)-1 as wds_gone_currMonth
			FROM generate_series(0, (current_date::date - (current_date - interval '15 days')::date::date)) i
			WHERE date_part('dow', (current_date - interval '15 days')::date::date + i) NOT IN (0,6))))
			 )

			as forecast_invoiced_acquisitions 




		FROM bi.orders o

		WHERE o.test__c = '0' and order_type = '1' and (LEFT(o.locale__c,2) = LEFT(o.polygon,2) OR o.polygon IS NULL)

		GROUP BY LEFT(o.locale__c,2), o.polygon

		ORDER BY locale asc, city_polygon asc