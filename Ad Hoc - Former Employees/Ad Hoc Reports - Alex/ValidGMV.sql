
DROP TABLE IF EXISTS bi.order_events_20161020;
CREATE TABLE bi.order_events_20161020 as 
SELECt
	order_json->>'Order_Id__c' as Order_Id,
	created_at as date,
	event_name
FROM
	events.sodium
WHERE
	created_at::date >= '2016-06-01'
	and event_name like '%Order Event%';


DROP TABLE IF EXISTS bi.order_events_20161020_v2;
CREATE TABLE bi.order_events_20161020_v2 as
SELECT
	Order_Id,
	MAX(date) as last_date
FROM
	 bi.order_events_20161020 
WHERE
	date::date < '2016-10-01'
GROUP BY
	Order_Id


DROP TABLE IF EXISTS bi.order_events_20161020_v3;
CREATE TABLE bi.order_events_20161020_v3 as	
SELECT
	t1.Order_Id,
	CASE WHEN event_name like '%CANCELLED%' THEN 'CANCELLED' ELSE 'VALID' END as Status
FROM
	bi.order_events_20161020_v2 t1
JOIn
	bi.order_events_20161020 t2
ON
	(t1.Order_Id = t2.Order_Id and t1.last_date = t2.date);
	
SELECT
	*
FROM
	bi.orders t1
LEFT
	bi.order_events_20161020_v3 t2
ON
	(t1.order_id__c = t2.order_id)
	