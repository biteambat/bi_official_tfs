

DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
CREATE TABLE bi.AllOrders_tableaucohorts as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 contact__c,
 Order_Id__c,
 CAST(Effectivedate as Date) as Start_date,
 LEFT(locale__c,2) as locale,
 Recurrency__c,
 status,
 order_duration__c,
 marketing_channel,
 type as ordertype,
 payment_method__c as payment_method,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
	and order_type = '2'
	and contact__c is not null
ORDER BY
  contact__c,
  CAST(Effectivedate as Date);
 
CREATE INDEX IDX10 ON bi.AllOrders_tableaucohorts(contact__c);
   
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
CREATE TABLE bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date varchar(20) DEFAULT NULL,  city varchar(55) DEFAULT null, OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,  Start_Date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, order_duration__c integer default null, acquisition_channel varchar(40) NOT NULL, ordertype varchar NULL, locale varchar NULL, payment_method varchar(50) Default NULL, funnel varchar(50) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date,city,OrderNo,contact__c,Order_Id__c,Start_Date,Recurrency__c,status,order_duration__c,acquisition_channel,ordertype,locale,payment_method,funnel)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Effectivedate as Date) as Start_Date,
  Recurrency__c,
  status,
  order_duration__c,
  marketing_channel,
  type,
  LEFT(locale__c,2),
  payment_method__c as payment_method,
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and order_type = '2'
  and Recurrency__c > 6
  and contact__c is not null
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
 Order BY
  contact__c,
  Cast(Order_Creation__c as Date);

DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucoho0rts;
CREATE TABLE bi.RecurringCustomer_tableaucohorts as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  acquisition_channel,
  ordertype,
  locale,
  payment_method,
  funnel as acquisition_funnel
FROM
  bi.RecurringCohortsCustomers_tableaucohorts
WHERE
  OrderNo = 1;
  
CREATE INDEX IDX145 ON bi.RecurringCustomer_tableaucohorts(contact__c);

DROP TABLE IF EXISTS bi.b2bcohorts;
CREATE TABLE bi.b2bcohorts as 
SELECT
  t1.Contact__c,
  city,
  first_order_date,
  start_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(Start_date as date),'YYYY-MM') as Order_Month,
  status,
  order_duration__c,
  acquisition_channel,
  t1.ordertype,
  t1.locale,
  t1.payment_method,
  acquisition_funnel
FROM
  bi.RecurringCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.Contact__c = t2.Contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(Start_Date as date),'YYYY-MM'));


DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCohorts_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;
