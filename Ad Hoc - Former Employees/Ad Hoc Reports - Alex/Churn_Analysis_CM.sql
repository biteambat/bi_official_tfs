SELECT
	 customer_id,
	 termination_date,
	 all_inbound_calls,
	 connected_calls,
	 all_outbound,
	 b2b_outbound,
	 cases,
	 orders
FROM
(SELECT
	customer_id,
	min(date) as termination_date
FROM
	bi.recurrent_customers_terminated t1
JOIN
	bi.orders t2
ON
	(t1.customer_id = t2.contact__c)
WHERE
	left(t1.locale__c,2) = 'ch'
	and t2.order_type = '1'
GROUP BY
	customer_id
HAVING
	min(date) > '2017-05-31') as t1
LEFt JOIN
	(SELECT
			CASE 
				WHEN calledcountryshort__c = 'AUT' THEN 'at'
				WHEN calledcountryshort__c = 'CHE' THEN 'ch'
				WHEN calledcountryshort__c = 'NLD' THEN 'nl'
				ELSE 'de' END as locale,
			SUM(CASE WHEN callconnected__c ='No' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END)+SUM(CASE WHEN callconnected__c = 'Yes' AND calldirection__c = 'Inbound' THEN 1 ELSE 0 END) as all_inbound_calls,
			SUM(CASE WHEN callconnected__c ='Yess' AND calldirection__c = 'Inbound' AND callringseconds__c > 30 THEN 1 ELSE 0 END) as connected_calls,
			SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' THEN 1 ELSE 0 END) AS all_outbound,
			SUM(CASE WHEN callconnected__c ='Yes' AND calldirection__c = 'Outbound' AND t2.type__c= 'customer-b2b' THEN 1 ELSE 0 END) AS b2b_outbound,
			RelatedContact__c as Contact
		FROM 
			Salesforce.natterbox_call_reporting_object__c t1
		LEFT JOIN 
			salesforce.contact t2
		ON
			(t1.relatedcontact__c = t2.sfid)
		WHERE
		((E164Callednumber__C = '493030807263'
			OR E164Callednumber__C='493030807264')
		AND calldirection__c = 'Inbound')
		OR
		(Callerfirstname__c like 'CM%'
		AND calldirection__c = 'Outbound'
		)
		and t1.createddate::date between '2017-03-01' and '2017-06-30' 
		GROUP BY
		locale,
		Contact) as t2
ON
	(t1.customer_id = t2.contact)
LEFT JOIN
	(SELECT
	contactid,
	COUNT(1) as Cases
FROM
	salesforce.case
WHERE
	origin like '%CM CH%' and origin not like 'CM CH - Feedback'
	and createddate::date between '2017-03-01' and '2017-06-30'
	and createdbyid = '00520000003IiNCAA0'
GROUP BY
	contactid) as t3
ON
	(t1.customer_id = t3.contactid)
LEFT JOIn
	(SELECT
		contact__c,
		COUNT(1) as Orders
	FROM
		salesforce.order
	WHERE
		status not like '%CANCELLED%'
	GROUP BY
		contact__c) as t4
ON
	(t1.customer_id = t4.contact__c)
		
	