DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	professional__c,
	delivery_areas__c,
	Effectivedate::date as date,
	hr_contract_start__c::date as contract_start,
	hr_contract_end__c::date as contract_end,
	hr_contract_weekly_hours_min__c as weekly_hours,
	SUM(GMV__c) as GMV,
	SUM(Order_Duration__c) as Hours
FROM
	Salesforce.Account t1
JOIN
	Salesforce.Order t2
ON
	(t2.professional__c = t1.sfid and t2.status in ('INVOICED','NOSHOW CUSTOMER'))
WHERE
	type__c = '60'
	and t2.type = '60'
	and t1.test__c = '0'
GROUP BY
	professional__c,
	delivery_areas__c,
	hr_contract_start__c,
	hr_contract_end__c,
	date,
	hr_contract_weekly_hours_min__c;
	

DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a,
	EXTRACT(DOW FROM date::date) as weekday
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and status__c in ('waiting','approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	account__c,
	date,
	SUM(CASE WHEN type__c = 'holidays' THEN a ELSE 0 END)/4 as holidays,
	SUM(CASE WHEN type__c = 'sickness' THEN a ELSE 0 END)/4 as sickness
FROM
	bi.holidays_cleaner_2
GROUP BY
		account__c,
	date;

DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 	
SELECT
	*
FROM
	bi.holidays_cleaner_3 
WHERE
	holidays > 0 or sickness > 0;

SELECT
	*
FROM
bi.holidays_cleaner_4
	
	
SELECT	
	EXTRACT(YEAR FROM Date) as Year,
	EXTRACT(MONTH FROM DATE) as Month,
	CASE 
	WHEN EXTRACT(MONTH FROM contract_end) =  EXTRACT(MONTH FROM date) and EXTRACT(YEAR FROM contract_end) =  EXTRACT(YEAR FROM date) THEN 0 
	WHEN EXTRACT(MONTH FROM contract_start) =  EXTRACT(MONTH FROM date) and EXTRACT(YEAR FROM contract_start) =  EXTRACT(YEAR FROM date)  THEN DATE_PART('day', '2016-04-30'::date) - DATE_PART('day', contract_start)
	WHEN EXTRACT(MONTH FROM contract_end) > EXTRACT(MONTH FROM date) and EXTRACT(YEAR FROM contract_end) =  EXTRACT(YEAR FROM date) THEN 0 
	ELSE DATE_PART('day', contract_end) - DATE_PART('day', contract_start) END days_working,
	professional__c,
	contract_start,
	contract_end,
	SUM(GMV),
	MAX(weekly_hours)*4.3 as monthly_hours
FROM
	bi.gmp_per_cleaner t1
WHERE
	date > '2016-01-01'
GROUP BY
	year,
	month,
	professional__c,
	contract_start,
	contract_end;