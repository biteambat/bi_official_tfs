SELECT
	extract(week from date) as week,
	min(date) as mindate,
	payment,
	COUNT(1) as Orders
FROM(SELECT
	t2.order_id,
	CASE WHEN t2.method__c is null THEN payment_method__c else method__c END as payment,
	t1.sfid,
	t1.status,
	effectivedate::date as date
FROM
	bi.orders t1
LEFT JOIN
	bi.payment_method t2
ON
	(t1.sfid = t2.order_id)
WHERE
	t1.status = 'INVOICED'
	and effectivedate::date > '2017-02-01' and effectivedate::date < '2017-08-11'
	and left(locale__c,2) = 'ch'
	) as a
GROUP BY
	week,
	payment