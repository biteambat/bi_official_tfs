DROP TABLE IF EXISTS  bi.cltv_per_city_raw;
CREATE TABLE bi.cltv_per_city_raw as 
SELECT
	'01-01-1991'::date as date,
	CAST('Churn Rate' as varchar) as kpi,
	returning_month,
	CAST((SUM(previous_customer)-SUM(returning_customer)) as decimal)/SUM(previous_customer) as churn
FROM
(
SELECT
	sub1.first_order_month,
	sub1.order_month,
	sub1.returning_month,
	sub1.unique_customer as returning_customer,
	sub2.unique_customer as total_cohort,
	CAST(sub1.unique_customer as decimal)/sub2.unique_customer as return_rate,
	sub1.hours as hours,
	sub3.unique_customer as previous_customer,
	CAST((sub3.unique_customer-sub1.unique_customer) as decimal)/sub3.unique_customer as churn_rate
FROM
(SELECT
	t1.first_order_month,
	t1.order_month,
	min(start_date) as order_date,
		(date_part('year', age(min(t1.start_date::date), min(t1.first_order_date::date)))*12)+
date_part('month', age(min(t1.start_date::date), min(t1.first_order_date::date))) as returning_month,
	COUNT(DISTINCT(t1.contact__c)) as unique_customer,
	SUM(Order_Duration__c) as Hours
FROM
	bi.RecurringCustomerCohort t1
WHERE
	t1.first_order_month >= '2015-06'
	and t1.order_month < '2017-01'
GROUP BY
	t1.first_order_Month,
	t1.order_month)	as sub1
LEFT JOIN
	(SELECT
		first_order_month,
		COUNT(DISTINCT(contact__c)) as unique_customer
	FROM
		bi.RecurringCustomerCohort
	GROUP BY
		first_order_month) sub2
ON
	(sub1.first_order_month = sub2.first_order_month)
LEFT JOIn
	(SELECT
	t1.first_order_month,
	t1.order_month,
	min(start_date) as order_date,
		(date_part('year', age(min(t1.start_date::date), min(t1.first_order_date::date)))*12)+
date_part('month', age(min(t1.start_date::date), min(t1.first_order_date::date))) as returning_month,
	COUNT(DISTINCT(t1.contact__c)) as unique_customer,
	SUM(Order_Duration__c) as Hours
FROM
	bi.RecurringCustomerCohort t1
WHERE
	t1.first_order_month >= '2015-06'
	and t1.order_month < '2017-01'
GROUP BY
	t1.first_order_Month,
	t1.order_month)	as sub3
ON
	(to_char(cast(sub1.order_date as date),'YYYY-MM') = to_char(cast(sub3.order_date + Interval '1 Month' as date),'YYYY-MM') and sub1.first_order_month = sub3.first_order_month)) as a
WHERE
	order_month in ('2016-11')
	and returning_month between 0 and 6
GROUP BY
	returning_month;






	
SELECT
	SUM(churn)/COUNT(DISTINCT(returning_month)) as average_churn
FROM
(SELECT
	returning_month,
	CAST((SUM(previous_customer)-SUM(returning_customer)) as decimal)/SUM(previous_customer) as churn
FROM
(
SELECT
	sub1.first_order_month,
	sub1.order_month,
	sub1.returning_month,
	sub1.unique_customer as returning_customer,
	sub2.unique_customer as total_cohort,
	CAST(sub1.unique_customer as decimal)/sub2.unique_customer as return_rate,
	sub1.hours as hours,
	sub3.unique_customer as previous_customer,
	CAST((sub3.unique_customer-sub1.unique_customer) as decimal)/sub3.unique_customer as churn_rate
FROM
(SELECT
	t1.first_order_month,
	t1.order_month,
	min(start_date) as order_date,
		(date_part('year', age(min(t1.start_date::date), min(t1.first_order_date::date)))*12)+
date_part('month', age(min(t1.start_date::date), min(t1.first_order_date::date))) as returning_month,
	COUNT(DISTINCT(t1.contact__c)) as unique_customer,
	SUM(Order_Duration__c) as Hours
FROM
	bi.RecurringCustomerCohort t1
WHERE
	t1.first_order_month >= '2015-06'
	and t1.order_month < '2017-01'
GROUP BY
	t1.first_order_Month,
	t1.order_month)	as sub1
LEFT JOIN
	(SELECT
		first_order_month,
		COUNT(DISTINCT(contact__c)) as unique_customer
	FROM
		bi.RecurringCustomerCohort
	GROUP BY
		first_order_month) sub2
ON
	(sub1.first_order_month = sub2.first_order_month)
LEFT JOIn
	(SELECT
	t1.first_order_month,
	t1.order_month,
	min(start_date) as order_date,
		(date_part('year', age(min(t1.start_date::date), min(t1.first_order_date::date)))*12)+
date_part('month', age(min(t1.start_date::date), min(t1.first_order_date::date))) as returning_month,
	COUNT(DISTINCT(t1.contact__c)) as unique_customer,
	SUM(Order_Duration__c) as Hours
FROM
	bi.RecurringCustomerCohort t1
WHERE
	t1.first_order_month >= '2015-06'
	and t1.order_month < '2017-01'
GROUP BY
	t1.first_order_Month,
	t1.order_month)	as sub3
ON
	(to_char(cast(sub1.order_date as date),'YYYY-MM') = to_char(cast(sub3.order_date + Interval '1 Month' as date),'YYYY-MM') and sub1.first_order_month = sub3.first_order_month)) as a
WHERE
	order_month in ('2016-11')
	and returning_month between 4 and 8
GROUP BY
	returning_month) as a


DROP TABLE IF EXISTS  bi.cltv_per_city_raw;
CREATE TABLE bi.cltv_per_city_raw as 
SELECT
	date,
	CAST('B2C' as varchar) as business,
	CAST('GPM%' as varchar) as kpi,
	CAST('-' as varchar) as breakdown,
	delivery_area as city,
	value
FROM(
SELECT
	EXTRACT(MONTH FROM date::date) as Month,
	EXTRACT(YEAR FROM date::date) as Year,
	min(date::date) as date,
	delivery_area,
	CAST('GPM %' as text) as kpi,
	CAST('B2C' as text) as breakdo9wn,
	CASE WHEN sum(b2c_revenue) >0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
	bi.gpm_city_over_time
WHERE
		LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
	year,
	month,
	delivery_area) as a;

INSERT INTO bi.cltv_per_city_raw 
SELECT
	date,
	CAST('B2C' as varchar) as business,
	CAST('Costs' as varchar) as kpi,
	CAST('-' as varchar) as breakdown,
	polygon,
	sum(costs) as value
FROM(
SELECT
		TO_CHAR(date,'YYYY-MM') as Month_Year,
	min(date) as date,	
	polygon,
	locale,
	SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+youtube_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+newsletter_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
	SUM(all_acq) as all_acquisitions
FROM
	bi.cpacalcpolygon
GROUP BY
	Month_Year,
	locale,
	polygon) as a
GROUP BY
	date,
	polygon;
	
INSERT INTO bi.cltv_per_city_raw 
SELECT
	date,
	CAST('B2C' as varchar) as business,
	CAST('Acquisitions' as varchar) as kpi,
	CAST('-' as varchar) as breakdown,
	polygon,
	sum(all_acquisitions) as value
FROM(
SELECT
		TO_CHAR(date,'YYYY-MM') as Month_Year,
	min(date) as date,	
	polygon,
	locale,
	SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+youtube_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+newsletter_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
	SUM(all_acq) as all_acquisitions
FROM
	bi.cpacalcpolygon
GROUP BY
	Month_Year,
	locale,
	polygon) as a
GROUP BY
	date,
	polygon;

