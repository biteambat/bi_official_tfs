SELECT
	COUNT(1) as Orders,
	SUM(CASE WHEN t1.professional__c is not null then 1 else 0 end) as orders_W_professionals,
	SUM(CASE WHEN t1.professional__c is not null and t2.professional__c is not null THEN 1 ELSE 0 END) as orders_blocked
FROM
(SELECT
	effectivedate::date as date,
	Order_Start__c,
	Order_Duration__c,
	sfid,
	professional__c,
	LEFT(locale__c,2) 
FROM
		bi.orders
WHERE
	Order_type = '1'
	and effectivedate::date between  '2016-07-11' and '2016-07-23'
	and status = 'CANCELLED NO MANPOWER'
	and test__c = '0'
	and acquisition_channel__c = 'recurrent' and recurrency__c > '0'
	and LEFT(locale__c,2) = 'de') as t1
LEFT JOIN 
	(SELECT
	effectivedate::date as date,
	Order_Start__c,
	Order_Duration__c,
	
	sfid,
	professional__c,
	LEFT(locale__c,2) 
FROM
		bi.orders
WHERE
	Order_type = '1'
	and effectivedate::date between  '2016-07-11' and '2016-07-23'
	and status = 'INVOICED'
	and test__c = '0') as t2
ON
	(t1.professional__c = t2.professional__c and t1.date = t2.date)