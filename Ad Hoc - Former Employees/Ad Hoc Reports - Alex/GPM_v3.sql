DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.sfid as professional__c,
	delivery_areas__c as delivery_areas,
	hr_contract_start__c::date as contract_start,
	hr_contract_end__c::date as contract_end,
	hr_contract_weekly_hours_min__c as weekly_hours
FROM
	Salesforce.Account t1
WHERE
	t1.type__c like '%60%'
	and  t1.test__c = '0'
GROUP BY
	t1.sfid,
	delivery_areas__c,
	hr_contract_start__c,
	hr_contract_end__c,
	hr_contract_weekly_hours_min__c;

DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
	SUM(GMV__c) as GMV,
	SUM(Order_Duration__c) as Hours
FROM
	Salesforce.Order t1
WHERE
	t1.status in ('INVOICED','NOSHOW CUSTOMER') 
	and t1.type = '60'
GROUP BY
	t1.professional__c,
	date;

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	date,
	gmv,
	hours
FROM
	bi.cleaner_stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);

DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';
	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and status__c in ('approved');

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
FROM
	bi.holidays_cleaner_3
GROUP BY
	Year_Month,
	account__c;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
SELECT	
   to_char(date,'YYYY-MM') as Year_Month,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	CASE 
	WHEN to_char(contract_start, 'YYYY-MM') < to_char(date, 'YYYY-MM') and to_char(contract_end, 'YYYY-MM') > to_char(date, 'YYYY-MM') THEN  DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) 
	WHEN to_char(contract_end, 'YYYY-MM') = to_char(date,'YYYY-MM') and EXTRaCT(DAY FROM contract_end) = DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))  and to_char(contract_start, 'YYYY-MM') = to_char(date,'YYYY-MM') THEN DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))  - DATE_PART('day', contract_start)
	WHEN to_char(contract_end, 'YYYY-MM') = to_char(date,'YYYY-MM') and EXTRaCT(DAY FROM contract_end) = DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))  and to_char(contract_start, 'YYYY-MM') < to_char(date,'YYYY-MM') THEN DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) 
	WHEN to_char(contract_end, 'YYYY-MM') = to_char(date,'YYYY-MM') THEN 0
	WHEN to_char(contract_start, 'YYYY-MM') = to_char(date,'YYYY-MM') THEN DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))  - DATE_PART('day', contract_start) +1
	ELSE DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))  - DATE_PART('day', contract_start) END as days_worked,
	DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
	contract_start,
	contract_end,
	SUM(hours) as worked_hours,
	SUM(GMV) as GMV,
	MAX(Weekly_hours) as weekly_hours,
	MAX(weekly_hours)*4.3 as monthly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Month,
	days_worked,
	professional__c,
	contract_start,
	days_of_month,
	contract_end,
	delivery_area;

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month);


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3;
CREATE TABLE bi.gpm_per_cleaner_v3 as 
select
    year_month,
    professional__c,
    delivery_area,
    worked_hours,
    contract_end,
    monthly_hours,
    days_worked,
    sickness,
    mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as holidays_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
	SUM(GMV) as GMV,
	SUM(GMV/1.19) as Revenue
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    year_month,
    professional__c,
    weekly_hours,
    contract_end,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    mindate,
    holiday,
    holidays_hours,
    days_worked,
    worked_hours;

DROP TABLE IF EXISTS bi.gpm_city;
CREATE TABLE bi.gpm_city as   
select
    year_month,
    delivery_area,
    mindate,
    professional__c,
    CASE WHEN total_hours > working_hours THEN total_hours*12.25 ELSE working_hours*12.25 END as salary_payed,
    SUM(Revenue) as revenue
from 
    bi.gpm_per_cleaner_v3 t1
LEFT JOIn
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid)
GROUP BY
	year_month,
	mindate,
	delivery_area,
	mindate,
	professional__c,
	salary_payed;


DROP TABLE IF EXISTS bi.gpm_city_over_time;
CREATE TABLE bi.gpm_city_over_time as 	
SELECT
	year_month,
	delivery_area,
	min(mindate) as date,
	SUM(salary_payed) as salary,
	sum(revenue) as revenue,
	sum(revenue)-sum(salary_payed) as gp
FROM
	bi.gpm_city
GROUP BY
	year_month,
	delivery_area;

