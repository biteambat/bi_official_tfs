SELECT
	TO_CHAR(effectivedate::date,'YYYY-MM') as Month,
	min(effectivedate::date) as mindate,
	SUM(CASE WHEN Order_type = '1' THEN Order_Duration__c ELSE 0 END)*25 as B2C_QM,
	SUM(CASE WHEN Order_type = '1' THEN Order_Duration__c ELSE 0 END) as hours,
	SUM(CASE WHEN Order_type = '2' THEN Order_Duration__c ELSE 0 END)*200 as B2B_QM
FROM
	bi.orders
WHERE
	status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
	and effectivedate::date between '2014-06-01' and '2016-10-31'
GROUP BY
	Month