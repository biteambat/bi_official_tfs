SELECT
	week,
	CASE WHEN COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) > 0  THEN SUM(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN b2c_hours+b2b_hours ELSE 0 END)/COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) ELSE 0 END as b2b_b2c_hours_per_cleaner,
		CASE WHEN COUNT(DISTINCT(CASE WHEN b2b_hours = 0 and b2c_hours >0 THEN professional__c ELSE null END)) > 0  THEN SUM(CASE WHEN b2b_hours = 0 and b2c_hours >0 THEN b2c_hours+b2b_hours ELSE 0 END)/COUNT(DISTINCT(CASE WHEN b2b_hours = 0 and b2c_hours >0 THEN professional__c ELSE null END)) ELSE 0 END as b2c_cleaner_hours_per_cleaner,
		CASE WHEN COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours = 0 THEN professional__c ELSE null END)) > 0  THEN SUM(CASE WHEN b2b_hours > 0 and b2c_hours = 0 THEN b2c_hours+b2b_hours ELSE 0 END)/COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours = 0 THEN professional__c ELSE null END)) ELSE 0 END as b2b9_cleaner_hours_per_cleaner,
	COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) as b2b_b2c_cleaner,
	COUNT(DISTINCT(CASE WHEN b2b_hours = 0 and b2c_hours >0 THEN professional__c ELSE null END)) as b2c_cleaner,
	COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours =0 THEN professional__c ELSE null END)) as b2b_cleaner,
	CASE WHEN COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) > 0  THEN SUM(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN b2b_hours ELSE 0 END)/COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) ELSE 0 END as b2b_hours_per_b2cb2bcleaner,
		CASE WHEN COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) > 0  THEN SUM(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN b2c_hours ELSE 0 END)/COUNT(DISTINCT(CASE WHEN b2b_hours > 0 and b2c_hours >0 THEN professional__c ELSE null END)) ELSE 0 END as b2c_hours_per_b2bb2ccleaner
FROM(
SELECT
	EXTRACT(WEEK FROM Effectivedate::date) as week,
	min(effectivedate::date) as date,
	professional__c,
	SUM(CASE WHEN order_type = '1' THEN Order_Duration__c ELSE 0 END) as B2C_Hours,
	SUM(CASE WHEN order_type = '2' THEN Order_Duration__c ELSE 0 END) as B2B_Hours
FROm
	bi.orders
WHERE
	polygon = 'de-berlin'
	and (status = 'INVOICED' or Status not like '%CANCELLED%')
	and effectivedate::date between '2016-05-01' and '2016-09-11'
	and type in ('cleaning-b2c','cleaning-b2b')
GROUP BY
	WEEK,
	professional__c) as a
GROUP BY
	week
	
SELECT
	*
FROM
	Salesforce.Account
WHERE
	