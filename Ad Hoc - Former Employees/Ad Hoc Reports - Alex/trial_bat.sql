
DROP TABLE IF EXISTS  bi.trial_kpis;
CREATE TABLE bi.trial_kpis as 
SELECT
	t2.date as date,
	LEFT(locale,2) as locale,
	'trial customer' as kpi,
	count(distinct(t1.customer_Id__c)) as unique_customer
FROM
(SELECT
	customer_id__c,
	LEFT(locale__c,2) as locale,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
	bi.orders_w_marketing
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
GROUP BY
	customer_id__c,
	locale
HAVING
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
	(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders_w_marketing
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and acquisition_channel__c = 'web' and recurrency__c = '0' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t2
ON
	(t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
		(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders_w_marketing
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t3
ON
	(t1.customer_id__c = t3.customer_id__c) 
GROUP BY
	t1.locale,
	t2.date;
	
INSERT INTO bi.trial_kpis
SELECT
	t3.date as date,
	t1.locale,
	'trial customer' as kpi,
	count(distinct(t1.customer_Id__c)) as unique_customer
FROM
(SELECT
	customer_id__c,
	LEFT(t1.locale__c,2) as locale,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
	bi.orders_w_marketing t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
GROUP BY
	customer_id__c,
	locale
HAVING
	SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
	(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders_w_marketing
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and acquisition_channel__c = 'web' and recurrency__c = '0' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t2
ON
	(t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
		(SELECT
	    customer_id__c,
	    min(order_Creation__c::date) as date
FROM
	bi.orders_w_marketing
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
	and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
	customer_id__c) as t3
ON
	(t1.customer_id__c = t3.customer_id__c) 
WHERE
	t3.date is not null
GROUP BY
	t1.locale,
	t3.date;

INSERT INTO bi.trial_kpis
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	'All Website Orders' as All_Orders,
	SUM(CASE WHEN acquisition_channel__c = 'web' THEN 1 ELSE 0 END) as all_website_orders
FROM
	bi.orders_w_marketing t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
GROUP BY
	date,
	locale;

INSERT INTO bi.trial_kpis	
SELECT
	order_Creation__c::date as date,
	LEFT(t1.locale__c,2) as locale,
	'All Website Trial Orders' as All_Orders,
	SUM(CASE WHEN acquisition_channel__c = 'web' and (recurrency__c = '0' or recurrency__c is null) THEN 1 ELSE 0 END) as all_website_orders
FROM
	bi.orders_w_marketing t1
WHERE
	ORder_Creation__c::date >= '2016-07-08'
	and test__c = '0'	
GROUP BY
	date,
	locale;