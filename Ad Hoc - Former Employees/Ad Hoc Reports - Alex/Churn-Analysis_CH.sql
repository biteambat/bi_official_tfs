CREATE TABLE bi.recurrent_customers_terminated as 

    SELECT
      created_at::date as date,
      event_name,
      (order_Json->>'Order_Start__c') as orderdate,
      order_Json->>'Locale__c' as Locale__c,
      order_Json->>'Order_Id__c' as Order_id,
      order_Json->>'Contact__c' as customer_id,
      order_Json->>'Recurrency__c' as recurrency,
      order_Json->>'Type' as ordertype

    FROM
      events.sodium

    WHERE
      (event_name in ('Order Event:CANCELLED TERMINATED'))
      and created_at >= '2017-05-01'
      and order_Json->>'Recurrency__c' > '0'
  ;
  

SELECT
	customer_id,
	termination_date,
	first_order_date,
	orders,
	cleaners,
	same_cleaner_ratio,
	professional_ratings,
	service_ratings,
	count_1_professional_rating,
	count_1_service_rating,
	future_orders.count as orders_pending,
	canc_profess,
	canc_nmp
FROM(  
SELECT
	customer_id,
	min(date) as termination_date
FROM
	bi.recurrent_customers_terminated t1
JOIN
	bi.orders t2
ON
	(t1.customer_id = t2.contact__c)
WHERE
	left(t1.locale__c,2) = 'ch'
	and t2.order_type = '1'
GROUP BY
	customer_id) as churn
LEFT JOIN
	(SELECT
		contact__c,
		min(effectivedate::date) as first_order_date,
		COUNT(DISTINCT(order_id__c)) as orders,
		COUNT(DISTINCT(professional__c)) as cleaners,
		CASE WHEN COUNT(DISTINCT(professional__c)) > 0 THEN CAST(COUNT(DISTINCT(order_id__c)) as decimal)/COUNT(DISTINCT(professional__c)) ELSE 0 END as same_cleaner_ratio,
		AVG(rating_professional__c) as professional_ratings,
		AVG(rating_service__c) as service_ratings,
		SUM(CASE WHEN rating_professional__c = '1' THEN 1 ELSE 0 END) as count_1_professional_rating,
		SUM(CASE WHEN rating_service__c = '1' THEN 1 ELSE 0 END) as count_1_service_rating
	FROM
		bi.orders
	WHERE
		(status not like '%CANCELLED%')
		and effectivedate::date between '2017-03-15' and '2017-06-15'
	GROUP BY
		contact__c) as orders
ON
	(churn.customer_id = orders.contact__c)
LEFT JOIN
	(SELECT
		contact__c,
		COUNT(1)
	 FROM
	 	bi.orders
	 WHERE
	 	effectivedate::date > current_date::date
	 	and status not like '%CANCELLED%'
	 GROUP BY
	 	contact__c) as future_orders
ON
	(churn.customer_id = future_orders.contact__c)
LEFT JOIn
	(SELECT
		contact__c,
		SUM(CASE WHEN status = 'CANCELLED PROFESSIONAL' THEN 1 ELSE 0 END) as canc_profess,
		SUM(CASE WHEN status = 'CANCELLED NO MANPOWER' THEN 1 ELSE 0 END) as canc_nmp
	FROM
		bi.orders
	WHERE
		effectivedate::date between '2017-03-15' and '2017-06-15'
	GROUP BY
		contact__c) as nmp_orders
ON
	(orders.contact__c = nmp_orders.contact__c)
	

	