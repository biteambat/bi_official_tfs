
Per Polygon
- Number of Cleaners (splitted by new & existing)
- % Active Cleaners (Cleaners with 1 Job / Cleaners on Platform)
- Number of Customers (splitted by new & existing)
- Revenue per Cleaner (New & Existing)
- Revenue per Customer (New & Existing)
- Number of Matchings 
- Number of Invoiced Orders
- 

SELECT
	polygon,
	CASE WHEN Count_Existing_Cleaners > 0 THEN hours_existing_cleaner/Count_Existing_Cleaners ELSE 0 END as Hours_per_Existing_Cleaner,
	CASE WHEN Count_New_Cleaners > 0 THEN hours_new_cleaner/Count_New_Cleaners ELSE 0 END as Hours_per_New_CLeaners,
	round(CAST((count_New_cleaners+count_existing_cleaners) as decimal)/cleaner_on_platform,2) as percentage_active_cleaners
FROM(
SELECT
	polygon,
	COUNT(DISTINCT(professional__c)) as total_cleaners,
	COUNT(DISTINCT(CASE WHEN to_char(effectivedate::date,'YYYY-MM') = to_char(hr_contract_start__c::date,'YYYY-MM') THEN t1.professional__c ELSE null END)) as Count_New_Cleaners,
	COUNT(DISTINCT(CASE WHEN to_char(effectivedate::date,'YYYY-MM') != to_char(hr_contract_start__c::date,'YYYY-MM') THEN t1.professional__c ELSE null END)) as Count_Existing_Cleaners,
	SUM(CASE WHEN to_char(effectivedate::date,'YYYY-MM') = to_char(hr_contract_start__c::date,'YYYY-MM') THEN Order_Duration__c ELSE 0 END) as Hours_New_Cleaner,
	SUM(CASE WHEN to_char(effectivedate::date,'YYYY-MM') != to_char(hr_contract_start__c::date,'YYYY-MM') THEN Order_Duration__c ELSE 0 END) as Hours_Existing_Cleaner	
FROM
	bi.orders t1
LEFT JOIN
	Salesforce.Account t2
ON
	(t1.professional__c = t2.sfid)
WHERE
	Status = 'INVOICED'
	and to_char(current_date-30,'YYYY-MM') = to_char(effectivedate::date,'YYYY-MM')
	and LEFT(t1.locale__c,2) = 'de'
	and t1.type = 'cleaning-b2c'
GROUP BY
	polygon) as sub1
LEFT JOIN
	(SELECT
		delivery_areas__c,
		SUM(CASE WHEN to_char(hr_contract_start__c::date,'YYYY-MM') <= to_char(current_date-30,'YYYY-MM') and to_char(hr_contract_end__c::date,'YYYY-MM') >= to_char(current_Date-30,'YYYY-MM') THEN 1 ELSE 0 END) as cleaner_on_platform
	FROM
		Salesforce.Account
	WHERE
		LEFT(locale__c,2) = 'de'
	GROUP BY
		delivery_areas__c) as cop
ON
	(sub1.polygon = cop.delivery_areas__c)
		
	