SELECT
	distinct_cleaners,
	SUM(canc_nmp) as canc_nmp,
	sum(invoiced) as invoiced,
	SUM(canc_nmp)/SUM(invoiced) as NMP_Rate,
	COUNT(DISTINCT(contact__c)) as unique_customers,
	SUM(canc_nmp_gmv) as GMV_NMP_Rev
FROM(
SELECT
	contact__c,
	min(effectivedate::date) as mindate,
	SUM(CASE WHEN status = 'CANCELLED NO MANPOWER' THEN 1 ELSE 0 END) as Canc_NMP,
	SUM(CASE WHEN status = 'CANCELLED NO MANPOWER' THEN gmv_eur_net ELSE 0 END) as Canc_NMP_gmv,
	SUM(CASE WHEN status = 'INVOICED' THEN 1 ELSE 0 END) as Invoiced,
	COUNT(DISTINCT(CASE WHEN status = 'INVOICED' THEN professional__c ELSE null end)) as distinct_cleaners
FROM
	bi.orders
WHERE
	order_type = '1'
	and left(locale__c,2) = 'ch'
GROUP BY
	contact__c
HAVING
		SUM(CASE WHEN status = 'INVOICED' THEN 1 ELSE 0 END) between 3 and 15
		and 	min(effectivedate::date) > '2017-01-01'::date) AS A
GROUP BY
	distinct_cleaners