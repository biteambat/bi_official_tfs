
SELECT
	EXTRACT(WEEK from effectivedate::date) as WEEK,
	min(effectivedate::date) as week_start_date,
	SUM(CASE WHEN Company = 'BAT' THEN ORder_Duration__c ELSE 0 END) as BAT_Cleaner_Hours,
	SUM(CASE WHEN Company = 'SUBCONTRACTOR' THEN ORder_Duration__c ELSE 0 END) as SUB_Cleaner_Hours
FROM
	bi.orders a
LEFT JOIN
(
SELECT
	a.sfid,
	CASE WHEN (t2.name like '%BAT%' or t2.name like '%BOOK%') THEN 'BAT' ELSE 'SUBCONTRACTOR' END as Company,
	t2.name
    FROM
        Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE 
    	a.test__c = '0' and a.name not like '%test%') as b
ON
	(a.professional__c = b.sfid)
WHERE
	order_type = '2'
	and status not like '%CANCELLED%'
	and effectivedate::date between '2016-07-01' and '2016-10-23'
GROUP BY
	Week