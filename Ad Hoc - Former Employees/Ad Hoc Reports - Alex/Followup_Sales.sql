SELECT
	t1.opportunityid,
	t1.customer__c,
	t1.name,
	first_of_date,
	last_of_date,
	CASE WHEN first_other_date is null THEN 'Still Offer Sent' ELSE 'Lost or Won' END as current_stage,
	SUM(CASE WHEN (first_of_date < t2.date or first_of_date = t2.date) and (last_of_date > t2.date or last_of_date = t2.date )  THEN calls ELSE 0 END) as calls_offer_sent
FROM(SELECT
	opportunityid,
	customer__c,
	t2.name,
	min(case when t1.stagename = 'OFFER SENT' THEN t1.createddate::date ELSE NULL END) as first_of_date,
	max(case when t1.stagename = 'OFFER SENT' THEN t1.createddate::date ELSE NULL END) as last_of_date,
	min(case when t1.stagename in ('DECLINED','IRREGULAR','LOST','RUNNING','SIGNED','TERMINATED') THEN t1.createddate::date ELSE NULL END) as first_other_date
FROM
	Salesforce.OpportunityHistory t1
JOIN
	salesforce.opportunity t2
ON
	(t1.opportunityid = t2.sfid)
GROUP BY
	Opportunityid,
	t2.name,
	customer__c
HAVING
		min(case when t1.stagename = 'OFFER SENT' THEN t1.createddate::date ELSE NULL END) is not null
		and min(case when t1.stagename = 'OFFER SENT' THEN t1.createddate::date ELSE NULL END) > '2017-02-01') as t1
LEFT JOIN
	
	(SELECT
					t1.createddate::date as date,
					relatedcontact__c,
					COUNT(1) as Calls
				FROM 
					Salesforce.natterbox_call_reporting_object__c t1
				JOIN 
					salesforce.contact t2
				ON
					(t1.relatedcontact__c = t2.sfid)
				WHERE
					calldirection__c = 'Outbound'
					and caller_user_full_name__c like 'BZ%'
					and t1.createddate::date between '2017-02-01' and '2017-07-06'
				GROUP BY
					date,
					relatedcontact__c) as t2
ON
	(t1.customer__c = t2.relatedcontact__c)
GROUP BY
		t1.opportunityid,
	t1.customer__c,
	first_of_date,
	t1.name,
	current_stage,
	last_of_date
	
