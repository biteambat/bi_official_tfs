SELECT
	t1.public_id__c,
	polygon,
	acquisition_channel,
	orders,
	createdcases
FROM
	Salesforce.Contact t1
LEFT JOIn
	(SELECT
		a1.contact__c,
		a1.polygon,
		a2.marketing_channel as acquisition_channel,
		COUNT(1) as Orders
	FROM
		bi.orders a1
	LEFT JOIN
		(SELECt
			contact__c,
			marketing_channel
		FROM
			bi.orders 
	WHERE
		acquisition_new_customer__c = '1'
	GROUP BY
		contact__c,
		marketing_channel) a2
	ON 
		a1.contact__c = a2.contact__c
	WHERE
		a1.polygon is not null
	GROUP BY
		a1.contact__c,
		a1.polygon,
		a2.marketing_channel) as t2
ON
	(t1.sfid = t2.contact__c)
LEFT JOIN
	(SELECT 
				
				contactid,
				COUNT(1) as CreatedCases
			
			FROM
				Salesforce.case
			
			WHERE
				CreatedDate::date > '2017-03-01'
				and 
				(Origin LIKE '%CM DE%'
					OR Origin LIKE '%CM CH%'
					OR Origin LIKE '%CM AT%'
					OR Origin LIKE '%CM NL%'
					OR Origin LIKE '%TM AT - General Email%'
					OR Origin LIKE '%TM CH - General Email%'
					OR Origin LIKE '%TM DE - General Email%'
					OR Origin LIKE '%TM NL - General Email%'
					OR Origin LIKE '%checkout%')
				AND 
				(type NOT LIKE '%Accounting%' 
					OR type NOT LIKE '%Damage%')
				AND Isdeleted = false
			
			GROUP BY 
				contactid) as t3
			ON
				(t1.sfid = t3.contactid)
WHERE
	t1.public_id__c in ('MGNNNP',
'D57W6J',
'T9RR3T',
'5QXFL8',
'M5MBTK',
'KSVQG3',
'LTR493',
'DBP3T3',
'7NMX64',
'NKXSGW',
'WM9BJF',
'622PNB',
'R9LJD4',
'GJD4QJ',
'VB3TJ5',
'7PWTXN',
'S3V979',
'3Q5C9S',
'L8Q6Q6',
'VSTQJ4',
'5MSRT7',
'KRRBKQ',
'DRHV2T',
'D6PZKG',
'HSBWBM',
'GWKX75',
'R38635',
'89L82M',
'FTMRMQ',
'XC4SNX',
'5367R8',
'SKT6N6')