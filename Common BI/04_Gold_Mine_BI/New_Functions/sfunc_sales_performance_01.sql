
DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_sales_performance_01(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_sales_performance_01';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Performance of Sales Managers and Account Management in Sales

------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.sales_performance_01;
CREATE TABLE bi.sales_performance_01 AS


-- Inbound Likelies
-- # of leads assigned(received) 
-- # of leads qualified
-- # of leads not reached
-- # of invalid leads
-- % of invalid leads: SUM(Invalid)/Leads Received
-- # of suitable leads(valid)
-- # of leads won(leads to opps)
-- % Lead-to-opp (CVR1): SUM(leads_converted_opps) / SUM(leads_suitable)
-- % Lead-to-customer(CVR2): SUM(leads_won) / SUM(leads_suitable)


WITH leads AS(

SELECT
    TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS leads_created_date,
    u.name AS owner_name,
    COUNT(li.sfid) AS leads_assigned,
    SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' AND li.lost_reason__c NOT LIKE 'not suitable%' OR li.lost_reason__c IS NULL THEN 1 ELSE 0 END) AS leads_suitable,
    SUM(CASE WHEN li.lost_reason__c LIKE 'invalid%' OR li.lost_reason__c LIKE 'not suitable%' THEN 1 ELSE 0 END) AS leads_invalid,
    SUM(CASE WHEN li.stage__c = 'QUALIFIED' THEN 1 ELSE 0 END) AS leads_qualified,
--    SUM(CASE WHEN li.direct_relation__c IS FALSE THEN 1 ELSE NULL END) AS leads_qualified,
    SUM(CASE WHEN li.stage__c = 'NOT REACHED' THEN 1 ELSE 0 END) AS leads_not_reached,
    SUM(CASE WHEN li.opportunity__c IS NOT NULL THEN 1 ELSE 0 END) AS leads_converted_opps,
    COALESCE(SUM(t1.leads_won), 0) AS leads_won,
    COALESCE(SUM(t1.leads_lost), 0) AS leads_lost

    
FROM
    salesforce.likeli__c li
LEFT JOIN (
    SELECT
        opps.sfid,
        SUM(CASE WHEN opps.stagename IN ('WON', 'PENDING') THEN 1 ELSE 0 END) AS leads_won,
        SUM(CASE WHEN opps.stagename IN ('LOST', 'CONTRACT FAILED') THEN 1 ELSE 0 END) AS leads_lost
    FROM
        salesforce.opportunity opps
    WHERE
        opps.test__c IS FALSE
    GROUP BY
        opps.sfid) t1 ON
    li.opportunity__c = t1.sfid
    
LEFT JOIN salesforce.user u ON
    li.ownerid = u.sfid

WHERE
    li.type__c = 'B2B'
    AND li.test__c IS FALSE
    AND li.acquisition_channel__c IN ('inbound', 'web')
    AND (li.acquisition_tracking_id__c NOT LIKE '%raffle%' OR li.acquisition_tracking_id__c NOT LIKE '%news%' OR li.acquisition_tracking_id__c IS NULL)
-- AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
    AND (li.company_name__c NOT LIKE '%test%' OR li.company_name__c IS NULL OR li.company_name__c NOT LIKE '%bookatiger%')
    AND (li.email__c NOT LIKE '%bookatiger%' OR li.email__c IS NULL)
    
GROUP BY
    TO_CHAR(li.createddate, 'YYYY-MM'),
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
    u.name
ORDER BY
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE DESC),

-- CALL STATS by call directions    
-- total # of calls
-- # of connected_calls
-- sum of call duration(min)
-- avg of call duration(min)
-- avg ring duration(min)
calls AS(

SELECT 
    TO_CHAR(t2.calldate::DATE, 'YYYY-MM') AS year_month,
    t2.calldate,
    t2.owner_name,
    t2.calldirection,
    MAX(total_calls) AS total_calls,
    MAX(connected_calls) AS connected_calls,
    t2.sum_call_duration_sec,
    t2.sum_call_duration,
    ROUND(t2.avg_call_duration_sec::NUMERIC, 2) AS avg_call_duration_sec,
    t2.avg_call_duration,
    t2.avg_ring_duration,
    ROUND(t2.avg_ring_duration_sec::NUMERIC, 2) AS avg_ring_duration_sec


FROM salesforce.likeli__c li

LEFT JOIN (
SELECT
    TO_CHAR(t1.calldate::DATE, 'YYYY-MM') AS year_month,
    t1.calldate,
    t1.name AS owner_name,
    t1.calldirection__c AS calldirection,
    SUM(t1.total_calls) AS total_calls,
    SUM(t1.connected_calls) AS connected_calls,
    SUM(t1.lost_calls) AS lost_calls,
    t1.sum_call_duration_sec,
    TO_CHAR((t1.sum_call_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS sum_call_duration,
    t1.avg_call_duration_sec,
    TO_CHAR((t1.avg_call_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS avg_call_duration,
    TO_CHAR((t1.avg_ring_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS avg_ring_duration,
    t1.avg_ring_duration_sec
FROM
    (
    SELECT
        u.name,
        nc.call_start_date_time__c::DATE AS calldate,
        nc.calldirection__c,
        COUNT(*) AS total_calls,
        SUM(CASE WHEN nc.callconnected__c = 'Yes' THEN 1 ELSE 0 END) AS connected_calls,
        SUM(CASE WHEN nc.callconnected__c = 'No' AND nc.callconnectedcheckbox__c IS FALSE THEN 1 ELSE 0 END) AS lost_calls,
        SUM(nc.calltalkseconds__c) AS sum_call_duration_sec,
        AVG(nc.calltalkseconds__c) AS avg_call_duration_sec,
        AVG(nc.callringseconds__c) AS avg_ring_duration_sec
    FROM
        salesforce.natterbox_call_reporting_object__c nc
    JOIN salesforce.user u ON
        nc.ownerid = u.sfid
    WHERE
        nc.call_start_date_time__c::DATE >= '2018-01-01'
        AND nc.wrapup_string_1__c LIKE '%B2B%'
    --    AND nc.calldirection__c = 'Outbound'
    GROUP BY
        u.name,
        calldate,
        nc.calldirection__c 
   ) AS t1
        
GROUP BY
    owner_name,
    year_month,
    calldate,
    t1.calldirection__c,
    sum_call_duration,
    avg_call_duration,
    avg_ring_duration,
    t1.sum_call_duration_sec,
    t1.avg_call_duration_sec,
    t1.avg_ring_duration_sec
    
ORDER BY
    calldate DESC ) t2 ON
    
TO_CHAR(li.createddate::DATE, 'YYYY-MM-DD') = TO_CHAR(t2.calldate::DATE, 'YYYY-MM-DD')

WHERE
    li.type__c = 'B2B'
    AND li.test__c IS FALSE
    AND li.acquisition_channel__c IN ('inbound', 'web')
    AND (li.acquisition_tracking_id__c NOT LIKE '%raffle%' OR li.acquisition_tracking_id__c NOT LIKE '%news%' OR li.acquisition_tracking_id__c IS NULL)
-- AND ((li.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR li.lost_reason__c IS NULL)
    AND (li.company_name__c NOT LIKE '%test%' OR li.company_name__c IS NULL OR li.company_name__c NOT LIKE '%bookatiger%')
    AND (li.email__c NOT LIKE '%bookatiger%' OR li.email__c IS NULL)
    AND t2.calldate IS NOT NULL
    
GROUP BY
    year_month,
    t2.calldirection,
    t2.owner_name,
    t2.calldate,
    t2.sum_call_duration,
    t2.sum_call_duration_sec,
    t2.avg_call_duration,
    t2.avg_call_duration_sec,
    t2.avg_ring_duration,
    t2.avg_ring_duration_sec
ORDER BY
    t2.calldate::DATE DESC
),


-- total # of deals (contracts ACCEPTED or SIGNED)
-- net revenue of all deals
-- # of recurring deals (6m, 12m, 24m, 36m, 48m and unlimited contracts)
-- # of one-off deals (fixed-term contracts, no duration)
-- total revenue of recurring deals and one-off deals

deals AS (

SELECT 
    t5.year_month,
    t5.opps_signed_date::DATE,
    t5.owner_name,
--    t5.service_type__c AS service_type,
    SUM(t5.grand_total__c)::DECIMAL AS total_net_revenue_deals,
    COUNT(t5.recurring_deals) + COUNT(t5.one_off_deals) AS total_deals,
    COUNT(t5.recurring_deals) AS total_recurring_deals,
    SUM(CASE WHEN t5.recurring_deals IS NOT NULL THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS revenue_recurring_deals,
    COUNT(t5.one_off_deals) AS total_one_off_deals,
    SUM(CASE WHEN one_off_deals IS NOT NULL THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS revenue_one_off_deals,
    SUM(CASE WHEN t5.recurring_deals = '6-month-contract' THEN 1 ELSE 0 END) AS "6-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '6-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-6-month-contract",
    SUM(CASE WHEN t5.recurring_deals= '12-month-contract' THEN 1 ELSE 0 END) AS "12-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '12-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-12-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '24-month-contract' THEN 1 ELSE 0 END) AS "24-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '24-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-24-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '36-month-contract' THEN 1 ELSE 0 END) AS "36-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '36-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-36-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '48-month-contract' THEN 1 ELSE 0 END) AS "48-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '48-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-48-month-contract",
    SUM(CASE WHEN t5.recurring_deals = 'unlimited-contract' THEN 1 ELSE 0 END) AS "unlimited-contract",
    SUM(CASE WHEN t5.recurring_deals = 'unlimited-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-unlimited-contract"
FROM 
(
SELECT
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM') AS year_month,
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD') AS opps_signed_date,
    u.name AS owner_name,
    c.opportunity__c,
    c.name AS contract_name,
    opps.name AS opp_name,
--    c.service_type__c,
--    c.when_accepted__c,
    c.grand_total__c,
    c.start__c,
    c.end__c,
    c.duration__c::NUMERIC,
    CASE WHEN c.duration__c::NUMERIC = 6 THEN '6-month-contract'
        WHEN c.duration__c::NUMERIC = 12 THEN '12-month-contract'
        WHEN c.duration__c::NUMERIC = 24 THEN '24-month-contract'
        WHEN c.duration__c::NUMERIC = 36 THEN '36-month-contract'
        WHEN c.duration__c::NUMERIC = 48 THEN '48-month-contract'
        WHEN c.start__c IS NOT NULL AND c.end__c IS NULL AND c.duration__c IS NULL THEN 'unlimited-contract'
        END AS recurring_deals,
    CASE WHEN c.start__c IS NOT NULL AND c.end__c IS NOT NULL AND c.duration__c IS NULL THEN 'fixed-term-contract' END AS one_off_deals

FROM
    salesforce.contract__c c
LEFT JOIN salesforce.opportunity opps ON
    c.opportunity__c = opps.sfid
LEFT JOIN salesforce.user u ON
    c.ownerid = u.sfid
WHERE
    c.test__c IS FALSE
    AND c.status__c IN ('ACCEPTED', 'SIGNED')
    AND opps.closedate <= CURRENT_DATE
GROUP BY
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD'),
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM'),
    c.opportunity__c,
    u.name,
--    c.service_type__c,
    c.start__c,
    c.end__c,
    c.duration__c,
    c.name,
    c.grand_total__c,
    opps.name
    
ORDER BY 
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD') DESC ) AS t5
    
GROUP BY 
    t5.year_month,
    t5.opps_signed_date,
    t5.owner_name
--    t5.service_type__c
ORDER BY
     t5.opps_signed_date DESC),
     

-- Inquiries(cases) for Account Management
-- New cases, cases in progress, closed cases, reopened cases
-- Total number of cases    
cases AS (

SELECT 
    t6.year_month,
    t6.casedate,
    t6.owner_name,
    SUM(CASE WHEN t6.status = 'New' THEN 1 ELSE 0 END) AS new_cases,
    SUM(CASE WHEN t6.status = 'In Progress' THEN 1 ELSE 0 END) AS cases_in_progress,
    SUM(CASE WHEN t6.status = 'Closed' THEN 1 ELSE 0 END) AS closed_cases,
    SUM(CASE WHEN t6.status = 'Reopened' THEN 1 ELSE 0 END) AS reopened_cases

FROM
(SELECT 
    ca.createddate::DATE AS casedate,
    TO_CHAR(ca.createddate::DATE, 'YYYY-MM') AS year_month,
    ca.sfid,
    ca.casenumber,
    ca.status,
    u.name AS owner_name
FROM salesforce.CASE ca     
LEFT JOIN salesforce.user u ON ca.ownerid = u.sfid

WHERE
    ca.test__c IS FALSE
    AND ca.createddate::date >= '2018-01-01'
    
GROUP BY 
    ca.createddate::DATE,
    TO_CHAR(ca.createddate::DATE, 'YYYY-MM'),
    ca.sfid,
    ca.casenumber,
    ca.status,
    u.name
ORDER BY 
    TO_CHAR(ca.createddate::DATE, 'YYYY-MM') ) AS t6
    
GROUP BY 
    t6.casedate,
    t6.year_month,
    t6.owner_name
ORDER BY 
    t6.casedate DESC),


-- productive days (number of days worked)
-- % productivity (productive days / working days in the month): WILL BE ADDED in Tableau via two date filter (working days and non-working days)

working_days AS (

SELECT
    t3.year_month,
    t3.leads_created_date,
    t3.owner_name,
    t3.working_days_in_month,
    t3.day_type,
    t3.non_working_day_number,
--    CASE WHEN t3.leads_created_date >= DATE_TRUNC('month', NOW()) THEN MAX(t3.working_day_number) ELSE 0 END AS working_days_till_today,
    t3.working_day
FROM
    (
    SELECT
        u.name AS owner_name,
        TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS leads_created_date,
        wd.working_day_number AS working_day,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month AS working_days_in_month
    FROM
        salesforce.likeli__c li
    LEFT JOIN bi.working_days_monthly wd ON
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE = wd.date
    LEFT JOIN salesforce.user u ON
        li.ownerid = u.sfid
    WHERE
        li.type__c = 'B2B'
        AND li.test__c IS FALSE
        AND li.company_name__c NOT LIKE '%test%'
        AND li.name NOT LIKE '%test%'

        GROUP BY u.name,
        TO_CHAR(li.createddate, 'YYYY-MM'),
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
        wd.working_day_number,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month
    ORDER BY
        TO_CHAR(li.createddate, 'YYYY-MM') DESC) t3

GROUP BY
    t3.year_month,
    t3.leads_created_date,
    t3.owner_name,
    t3.day_type,
    t3.non_working_day_number,
    t3.working_day,
    t3.working_days_in_month
ORDER BY
    t3.leads_created_date DESC ),
    
    
dates AS(
SELECT
    d::DATE AS DATE,
    TO_CHAR(d, 'YYYY-MM') AS year_month
FROM
    GENERATE_SERIES('2018-01-01', CURRENT_DATE, '1 day'::INTERVAL) d ),

users AS(
        SELECT 
            u.name AS owner_name
        FROM salesforce.USER u
        WHERE u.name IN ('Malte Jetter', 'Marc Heumer', 'Steven Ellison', 'Tommy Haferkorn', 'André Wagner', 'Anshuman Kharoo')
        )


SELECT
    dates.date,
    dates.year_month,
    users.owner_name,
    working_days.working_days_in_month,
    working_days.working_day,
    CASE WHEN calls.total_calls IS NOT NULL THEN COUNT(DISTINCT(dates.date)) ELSE 0 END AS productive_days,
--    ROUND(MAX(CASE WHEN calls.total_calls IS NOT NULL THEN working_days.working_days_till_today ELSE 0 END) / working_days.number_working_days_in_month::NUMERIC, 2) AS productivity,
    leads.leads_assigned,
    leads.leads_suitable,
    leads.leads_invalid,
    leads.leads_qualified,
    leads.leads_not_reached,
    leads.leads_converted_opps,
    ROUND(SUM(leads.leads_converted_opps) / NULLIF(SUM(leads.leads_suitable), 0), 2) AS cvr1,
    ROUND(SUM(leads.leads_won) / NULLIF(SUM(leads.leads_suitable), 0), 2) AS cvr2,
    leads.leads_won,
    leads.leads_lost,
    calls.calldirection,
    calls.total_calls,
    calls.connected_calls,
    calls.sum_call_duration,
    calls.sum_call_duration_sec,
    calls.avg_call_duration,
    calls.avg_call_duration_sec,
    calls.avg_ring_duration,
    calls.avg_ring_duration_sec,
    calls.total_calls / NULLIF((deals.total_deals), 0) AS calls_to_deals,
--    deals.service_type,
    COALESCE(deals.total_deals, 0) AS total_deals,
    COALESCE(deals.total_recurring_deals, 0) AS total_recurring_deals,
    COALESCE(deals.total_one_off_deals, 0) AS total_one_off_deals,
    COALESCE(deals.total_net_revenue_deals, 0) AS total_net_revenue_deals,
    COALESCE(deals.revenue_recurring_deals, 0) AS revenue_recurring_deals,
    COALESCE(deals.revenue_one_off_deals, 0) AS revenue_one_off_deals,
    COALESCE(deals."6-month-contract", 0) AS "6-month-contract",
    COALESCE(deals."revenue-6-month-contract", 0) AS "revenue-6-month-contract",
    COALESCE(deals."12-month-contract", 0) AS "12-month-contract",
    COALESCE(deals."revenue-12-month-contract", 0) AS "revenue-12-month-contract",
    COALESCE(deals."24-month-contract", 0) AS "24-month-contract",
    COALESCE(deals."revenue-24-month-contract", 0) AS "revenue-24-month-contract",
    COALESCE(deals."36-month-contract", 0) AS "36-month-contract",
    COALESCE(deals."revenue-36-month-contract", 0) AS "revenue-36-month-contract",
    COALESCE(deals."48-month-contract", 0) AS "48-month-contract",
    COALESCE(deals."revenue-48-month-contract", 0) AS "revenue-48-month-contract",
    COALESCE(deals."unlimited-contract", 0) AS "unlimited-contract",
    COALESCE(deals."revenue-unlimited-contract", 0) AS "revenue-unlimited-contract",
    COALESCE(cases.new_cases, 0) AS new_cases,
    COALESCE(cases.cases_in_progress, 0) AS cases_in_progress,
    COALESCE(cases.closed_cases, 0) AS closed_cases,
    COALESCE(cases.reopened_cases, 0) AS reopened_cases,
    COALESCE((cases.new_cases + cases.cases_in_progress + cases.closed_cases + cases.reopened_cases), 0) AS total_cases
    

FROM dates
CROSS JOIN users

LEFT JOIN calls ON 
calls.calldate = dates.date
AND calls.owner_name = users.owner_name

LEFT JOIN leads ON
leads.leads_created_date = dates.date
AND leads.owner_name = users.owner_name

LEFT JOIN cases ON 
calls.calldate = cases.casedate
AND cases.owner_name = users.owner_name

LEFT JOIN deals ON 
deals.opps_signed_date = dates.date
AND deals.owner_name = users.owner_name

LEFT JOIN working_days ON 
dates.date = working_days.leads_created_date

GROUP BY
    leads.leads_assigned,
    leads.leads_created_date,
    leads.leads_suitable,
    leads.leads_invalid,
    leads.leads_qualified,
    leads.leads_not_reached,
    leads.leads_converted_opps,
    leads.leads_won,
    leads.leads_lost,
    leads.owner_name,
    dates.year_month,
    dates.date,
    leads.leads_created_date,
    users.owner_name,
    calls.calldirection,
    calls.total_calls,
    calls.connected_calls,
    calls.sum_call_duration,
    calls.sum_call_duration_sec,
    calls.avg_call_duration,
    calls.avg_call_duration_sec,
    calls.avg_ring_duration,
    calls.avg_ring_duration_sec,
    working_days.working_days_in_month,
    working_days.working_day,
--    deals.service_type,
    deals.total_deals,
    deals.total_recurring_deals,
    deals.total_one_off_deals,
    deals.total_net_revenue_deals,
    deals.revenue_recurring_deals,
    deals.revenue_one_off_deals,
    deals."6-month-contract",
    deals."revenue-6-month-contract",
    deals."12-month-contract",
    deals."revenue-12-month-contract",
    deals."24-month-contract",
    deals."revenue-24-month-contract",
    deals."36-month-contract",
    deals."revenue-36-month-contract",
    deals."48-month-contract",
    deals."revenue-48-month-contract",
    deals."unlimited-contract",
    deals."revenue-unlimited-contract",
    cases.new_cases,
    cases.cases_in_progress,
    cases.closed_cases,
    cases.reopened_cases


ORDER BY
    dates.date DESC;

   
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

    INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'







