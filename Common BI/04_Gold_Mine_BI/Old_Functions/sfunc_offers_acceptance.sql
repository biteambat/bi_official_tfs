

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_offers_acceptance(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_offers_acceptance';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- Short Description: table containing the number of offers received and accepted for each partner

DROP TABLE IF EXISTS bi.offers_acceptance;
CREATE TABLE bi.offers_acceptance as

SELECT

    TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
    MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
    a.sfid,
    a.name,
    a.company_name__c,
    a.delivery_areas__c,
    a.type__c,
    a.status__c,
    a.role__c,
    COUNT(o.suggested_partner__c) as offers_received,
    SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted


FROM

    salesforce.account a


LEFT JOIN

    salesforce.partner_offer_partner__c o
        
ON

    a.sfid = o.suggested_partner__c 
    
WHERE

    TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
    AND a.test__c IS FALSE
    
GROUP BY

    TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
    a.sfid,
    a.name,
    a.company_name__c,
    a.delivery_areas__c,
    a.type__c,
    a.status__c,
    a.role__c

ORDER BY

    TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
    a.name asc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'