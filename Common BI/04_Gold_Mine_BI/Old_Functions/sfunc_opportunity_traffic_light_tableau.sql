

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_opportunity_traffic_light_tableau(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_opportunity_traffic_light_tableau';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- Short Description: 	Opportunity Traffic Light TODAY for tableau
-- -------------------	(History and current AVG result in one table for tableau)

DROP TABLE IF EXISTS bi.opportunity_traffic_light_tableau;


CREATE TABLE bi.opportunity_traffic_light_tableau AS
SELECT history.date AS history_date ,
       history.locale AS history_locale ,
       history.customer AS history_customer --, history.opportunity_name							AS history_opportunity_name
 ,
       history.opportunity AS history_opportunity ,
       history.opps AS history_opps ,
       history.traffic_light_noshow AS history_traffic_light_noshow ,
       history.traffic_light_cancelled_pro AS history_traffic_light_cancelled_pro ,
       history.traffic_light_cancelled_customer AS history_traffic_light_cancelled_customer ,
       history.traffic_light_inbound_calls AS history_traffic_light_inbound_calls ,
       history.traffic_light_lost_calls AS history_traffic_light_lost_calls ,
       history.traffic_light_createdcases AS history_traffic_light_createdcases ,
       history.traffic_light_retention AS history_traffic_light_retention ,
       history.traffic_light_invoicecorrection AS history_traffic_light_invoicecorrection ,
       history.traffic_light_professionalimprovement AS history_traffic_light_professionalimprovement ,
       history.avg_traffic_light AS history_avg_traffic_light ,
       current.date AS current__date ,
       current.opportunity AS current_opportunity ,
       current.opportunity_name AS current_opportunity_name ,
       current.delivery_area AS current_delivery_area ,
       current.status AS current_status ,
       current.owner AS current_owner ,
       current.operated_by AS operated_by ,
       current.operated_by_detail AS operated_by_detail ,
       current.traffic_light_noshow AS current_traffic_light_noshow ,
       current.traffic_light_cancelled_pro AS current_traffic_light_cancelled_pro ,
       current.traffic_light_cancelled_customer AS current_traffic_light_cancelled_customer ,
       current.traffic_light_inbound_calls AS current_traffic_light_inbound_calls ,
       current.traffic_light_lost_calls AS current_traffic_light_lost_calls ,
       current.traffic_light_createdcases AS current_traffic_light_createdcases ,
       current.traffic_light_retention AS current_traffic_light_retention ,
       current.traffic_light_invoicecorrection AS current_traffic_light_invoicecorrection ,
       current.traffic_light_professionalimprovement AS current_traffic_light_professionalimprovement ,
       current.avg_traffic_light AS current_avg_traffic_light
FROM bi.opportunity_traffic_light_new AS history
LEFT JOIN
    ( SELECT *
     FROM bi.opportunity_traffic_light_new
     WHERE date = (CURRENT_DATE - INTERVAL '0 DAY') ) AS CURRENT ON (history.opportunity = current.opportunity)
WHERE history.date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY') ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'