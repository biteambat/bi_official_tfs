

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_cvr_leads(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_cvr_leads';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.cvr_leads;
CREATE TABLE bi.cvr_leads AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as date,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.createddate,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN o.opportunity__c IS NULL THEN 'No' ELSE 'Yes' END as converted_in_opportunity,
	ooo.stagename as stage_opp,
	o.lost_reason__c,
	o.locale__C,
	LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
	o.contact_name__c,
	o.acquisition_channel__c,
	o.acquisition_tracking_id__c,
	lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as keyword,
	t1.spending,
	o.customer__c,
	o.stage__c,
	o.type__c,
	o.company_name__c,
	o.opportunity__c,
	o.ownerid,
	u.name as owner_name,
	o.total_calls__c,
	o.delivery_area__c,
	o.grand_total__c
	
FROM

	salesforce.likeli__c o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.opportunity__c = ooo.sfid

LEFT JOIN

	salesforce.user u
	
ON

	o.ownerid = u.sfid
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.start_date, 'YYYY-MM') as year_month,
		o.sub_kpi_3 as keyword,
		o.city,
		SUM(o.value) as spending
		
	FROM
	
		public.marketing_spending o
		
	GROUP BY
	
		year_month,
		city,
		keyword
		
	ORDER BY
	
		year_month desc,
		keyword) as t1

ON

	TO_CHAR(o.createddate, 'YYYY-MM') = t1.year_month
	AND lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) = t1.keyword
	AND LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) = t1.city
	
WHERE

	o.type__c = 'B2B'
	AND o.acquisition_channel__c IN ('inbound', 'web')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.test__c IS FALSE
	AND (o.acquisition_tracking_id__c NOT LIKE '%raffle%' OR o.acquisition_tracking_id__c NOT LIKE '%news%' OR o.acquisition_tracking_id__c IS NULL)
	-- AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
	AND (o.acquisition_channel__c NOT LIKE 'outbound' OR o.acquisition_channel__c IS NULL)
	AND (o.company_name__c NOT LIKE '%test%' OR o.company_name__c IS NULL OR o.company_name__c NOT LIKE '%bookatiger%')
	AND (o.email__c NOT LIKE '%bookatiger%' OR o.email__c IS NULL)
	
ORDER BY

	date desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'
