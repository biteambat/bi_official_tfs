

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_CM_cases(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_CM_cases';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- Short Description: CM Backlog History (count)

DELETE
FROM bi.CM_cases
WHERE date = CURRENT_DATE
    AND kpi = 'Open Cases';


INSERT INTO bi.CM_cases
SELECT TO_CHAR (CURRENT_DATE,'YYYY-IW') AS date_part,
       MIN (CURRENT_DATE::date) AS date,
           CAST ('-' AS varchar) AS locale,
                cases.case_origin AS origin,
                CAST ('B2B' AS varchar) AS TYPE,
                     CAST ('Open Cases' AS varchar) AS kpi,
                          CAST ('Count' AS varchar) AS sub_kpi_1,
                               cases.case_status AS sub_kpi_2,
                               cases.case_reason AS sub_kpi_3,
                               CASE
                                   WHEN cases.grand_total IS NULL
                                        AND cases.opportunityid IS NULL THEN 'unknown'
                                   WHEN cases.grand_total IS NULL
                                        AND cases.opportunityid IS NOT NULL THEN 'PPH'
                                   WHEN cases.grand_total < 250 THEN '<250€'
                                   WHEN cases.grand_total >= 250
                                        AND cases.grand_total < 500 THEN '250€-500€'
                                   WHEN cases.grand_total >= 500
                                        AND cases.grand_total < 1000 THEN '500€-1000€'
                                   ELSE '>1000€'
                               END AS sub_kpi_4,
                               cases.opportunity AS sub_kpi_5,
                               cases.case_owner AS sub_kpi_6,
                               MIN (cases.case_createddate::date) AS sub_kpi_7,
                                   cases.Opportunityid AS sub_kpi_8,
                                   CAST ('-' AS varchar) AS sub_kpi_9,
                                        CAST ('-' AS varchar) AS sub_kpi_10,
                                             COUNT(*) AS value
FROM bi.cm_cases_today cases
GROUP BY case_origin,
         cases.case_status,
         cases.case_reason,
         cases.grand_total,
         cases.opportunityid,
         cases.opportunity,
         cases.case_owner ;

-- Short Description: CM NEW cases History (count)
-- Created on: 07/11/2018

DELETE
FROM bi.CM_cases
WHERE date = 'YESTERDAY'
    AND kpi = 'Created Cases';


INSERT INTO bi.CM_cases
SELECT TO_CHAR ((cases.case_createddate::date),'YYYY-IW') AS date_part ,
       MIN (cases.case_createddate::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                cases.case_origin AS origin ,
                CAST ('B2B' AS varchar) AS TYPE ,
                     CAST ('Created Cases' AS varchar) AS kpi ,
                          CAST ('Count' AS varchar) AS sub_kpi_1 ,
                               cases.case_status AS sub_kpi_2 ,
                               cases.case_reason AS sub_kpi_3,
                               CASE
                                   WHEN cases.grand_total IS NULL
                                        AND cases.opportunityid IS NULL THEN 'unknown'
                                   WHEN cases.grand_total IS NULL
                                        AND cases.opportunityid IS NOT NULL THEN 'PPH'
                                   WHEN cases.grand_total < 250 THEN '<250€'
                                   WHEN cases.grand_total >= 250
                                        AND cases.grand_total < 500 THEN '250€-500€'
                                   WHEN cases.grand_total >= 500
                                        AND cases.grand_total < 1000 THEN '500€-1000€'
                                   ELSE '>1000€'
                               END AS sub_kpi_4 ,
                               cases.opportunity AS sub_kpi_5 ,
                               cases.case_owner AS sub_kpi_6 ,
                               CAST ('-' AS varchar) AS sub_kpi_7 ,
                                    cases.opportunityid AS sub_kpi_8 ,
                                    CAST ('-' AS varchar) AS sub_kpi_9 ,
                                         CAST ('-' AS varchar) AS sub_kpi_10,
                                              COUNT(*) AS value
FROM bi.cm_cases_created_yesterday cases
GROUP BY cases.case_createddate ,
         cases.case_origin ,
         cases.case_status,
         cases.case_reason ,
         cases.grand_total ,
         cases.opportunityid ,
         cases.opportunity,
         cases.case_owner ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Reopened Cases (count)
-- Created on: 23/11/2018

DELETE
FROM bi.CM_cases
WHERE kpi = 'Reopened Cases';


INSERT INTO bi.CM_cases
SELECT TO_CHAR ((reopened.date::date),'YYYY-IW') AS date_part ,
       MIN (reopened.date::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                reopened.case_origin AS origin ,
                CAST ('B2B' AS varchar) AS TYPE ,
                     CAST ('Reopened Cases' AS varchar) AS kpi ,
                          CAST ('Count' AS varchar) AS sub_kpi_1 ,
                               reopened.case_status AS sub_kpi_2 ,
                               reopened.case_reason AS sub_kpi_3,
                               CASE
                                   WHEN reopened.grand_total IS NULL
                                        AND reopened.opportunityid IS NULL THEN 'unknown'
                                   WHEN reopened.grand_total IS NULL
                                        AND reopened.opportunityid IS NOT NULL THEN 'PPH'
                                   WHEN reopened.grand_total < 250 THEN '<250€'
                                   WHEN reopened.grand_total >= 250
                                        AND reopened.grand_total < 500 THEN '250€-500€'
                                   WHEN reopened.grand_total >= 500
                                        AND reopened.grand_total < 1000 THEN '500€-1000€'
                                   ELSE '>1000€'
                               END AS sub_kpi_4 ,
                               reopened.opportunity AS sub_kpi_5 ,
                               reopened.case_owner AS sub_kpi_6 ,
                               CAST ('-' AS varchar) AS sub_kpi_7 ,
                                    reopened.opportunityid AS sub_kpi_8 ,
                                    CAST ('-' AS varchar) AS sub_kpi_9 ,
                                         CAST ('-' AS varchar) AS sub_kpi_10,
                                              COUNT(*) AS value
FROM
    ( SELECT hi.createddate::date AS date ,
             *
     FROM salesforce.casehistory hi
     INNER JOIN bi.cm_cases_basis ca ON hi.caseid = ca.case_ID
     WHERE -- reopened cases
 hi.field = 'Status'
         AND hi.newvalue LIKE 'Reopened'
         AND hi.createddate::date >= '2018-11-01' ) AS reopened
GROUP BY reopened.date ,
         reopened.case_origin ,
         reopened.case_status,
         reopened.case_reason ,
         reopened.grand_total ,
         reopened.opportunityid ,
         reopened.opportunity,
         reopened.case_owner ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Closed Cases (count)
-- Created on: 23/11/2018

DELETE
FROM bi.CM_cases
WHERE kpi = 'Closed Cases';


INSERT INTO bi.CM_cases
SELECT TO_CHAR ((closed.date::date),'YYYY-IW') AS date_part ,
       MIN (closed.date::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                closed.case_origin AS origin ,
                CAST ('B2B' AS varchar) AS TYPE ,
                     CAST ('Closed Cases' AS varchar) AS kpi ,
                          CAST ('Count' AS varchar) AS sub_kpi_1 ,
                               closed.case_status AS sub_kpi_2 ,
                               closed.case_reason AS sub_kpi_3,
                               CASE
                                   WHEN closed.grand_total IS NULL
                                        AND closed.opportunityid IS NULL THEN 'unknown'
                                   WHEN closed.grand_total IS NULL
                                        AND closed.opportunityid IS NOT NULL THEN 'PPH'
                                   WHEN closed.grand_total < 250 THEN '<250€'
                                   WHEN closed.grand_total >= 250
                                        AND closed.grand_total < 500 THEN '250€-500€'
                                   WHEN closed.grand_total >= 500
                                        AND closed.grand_total < 1000 THEN '500€-1000€'
                                   ELSE '>1000€'
                               END AS sub_kpi_4 ,
                               closed.opportunity AS sub_kpi_5 ,
                               closed.case_owner AS sub_kpi_6 ,
                               CAST ('-' AS varchar) AS sub_kpi_7 ,
                                    closed.opportunityid AS sub_kpi_8 ,
                                    CAST ('-' AS varchar) AS sub_kpi_9 ,
                                         CAST ('-' AS varchar) AS sub_kpi_10,
                                              COUNT(*) AS value
FROM
    ( SELECT hi.createddate::date AS date ,
             *
     FROM salesforce.casehistory hi
     INNER JOIN bi.cm_cases_basis ca ON hi.caseid = ca.case_ID
     WHERE -- closed cases
 hi.field = 'Status'
         AND hi.newvalue LIKE 'Closed'
         AND hi.createddate::date >= '2018-11-01' ) AS closed
GROUP BY closed.date ,
         closed.case_origin ,
         closed.case_status,
         closed.case_reason ,
         closed.grand_total ,
         closed.opportunityid ,
         closed.opportunity,
         closed.case_owner ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Case Type changes (count)
-- Created on: 23/11/2018

DELETE
FROM bi.CM_cases
WHERE kpi = 'Cases Type changed';


INSERT INTO bi.CM_cases
SELECT TO_CHAR ((type_change.date::date),'YYYY-IW') AS date_part ,
       MIN (type_change.date::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                type_change.case_origin AS origin ,
                CAST ('B2B' AS varchar) AS TYPE ,
                     CAST ('Cases Type changed' AS varchar) AS kpi ,
                          CAST ('Count' AS varchar) AS sub_kpi_1 ,
                               type_change.case_status AS sub_kpi_2 ,
                               type_change.case_reason AS sub_kpi_3,
                               CASE
                                   WHEN type_change.grand_total IS NULL
                                        AND type_change.opportunityid IS NULL THEN 'unknown'
                                   WHEN type_change.grand_total IS NULL
                                        AND type_change.opportunityid IS NOT NULL THEN 'PPH'
                                   WHEN type_change.grand_total < 250 THEN '<250€'
                                   WHEN type_change.grand_total >= 250
                                        AND type_change.grand_total < 500 THEN '250€-500€'
                                   WHEN type_change.grand_total >= 500
                                        AND type_change.grand_total < 1000 THEN '500€-1000€'
                                   ELSE '>1000€'
                               END AS sub_kpi_4 ,
                               type_change.opportunity AS sub_kpi_5 ,
                               type_change.case_owner AS sub_kpi_6 ,
                               CAST ('-' AS varchar) AS sub_kpi_7 ,
                                    type_change.opportunityid AS sub_kpi_8 ,
                                    CAST ('-' AS varchar) AS sub_kpi_9 ,
                                         CAST ('-' AS varchar) AS sub_kpi_10,
                                              COUNT(*) AS value
FROM
    ( SELECT hi.createddate::date AS date ,
             *
     FROM salesforce.casehistory hi
     LEFT JOIN bi.cm_cases_basis ca ON hi.caseid = ca.case_ID -- reopened cases

     WHERE hi.field = 'Type'
         AND hi.newvalue = 'CM B2B'
         AND hi.oldvalue NOT IN ('B2B',
                                 'KA')
         AND hi.createddate::date <> ca.case_createddate::date
         AND hi.createddate::date >= '2018-11-01'
         AND ca.case_origin NOT LIKE '%B2B customer%'
         AND ca.case_origin NOT LIKE 'CM - Team' ) AS type_change
GROUP BY type_change.date ,
         type_change.case_origin ,
         type_change.case_status,
         type_change.case_reason ,
         type_change.grand_total ,
         type_change.opportunityid ,
         type_change.opportunity,
         type_change.case_owner ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Inbound Calls (count)
-- Created on: 07/11/2018

DELETE
FROM bi.CM_cases
WHERE date = 'YESTERDAY'
    AND kpi = 'Inbound Calls';


INSERT INTO bi.CM_cases
SELECT TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') AS date_part ,
       MIN (n.call_start_date_time__c::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                n.e164callednumber__c AS origin -- NEW
 ,
                CAST ('B2B' AS varchar) AS TYPE ,
                     CAST ('Inbound Calls'AS varchar) AS kpi ,
                          CAST ('Count' AS varchar) AS sub_kpi_1 ,
                               n.callconnectedcheckbox__c AS sub_kpi_2 ,
                               n.wrapup_string_1__c AS sub_kpi_3 ,
                               CASE
                                   WHEN n.relatedcontact__c IS NOT NULL THEN co.type__c
                                   WHEN n.account__c IS NOT NULL THEN a.type__c
                                   WHEN n.lead__c IS NOT NULL THEN 'lead'
                                   ELSE 'unknown'
                               END AS sub_kpi_4 ,
                               CASE
                                   WHEN n.relatedcontact__c IS NOT NULL THEN co.name
                                   WHEN n.account__c IS NOT NULL THEN a.name
                                   WHEN n.lead__c IS NOT NULL THEN l.name
                                   ELSE 'unknown'
                               END AS sub_kpi_5 ,
                               u.name AS sub_kpi_6 ,
                               n.callringseconds__c AS sub_kpi_7,
                               CASE
                                   WHEN n.relatedcontact__c IS NOT NULL THEN n.relatedcontact__c
                                   WHEN n.account__c IS NOT NULL THEN n.account__c
                                   WHEN n.lead__c IS NOT NULL THEN n.lead__c
                                   ELSE 'unknown'
                               END AS sub_kpi_8 ,
                               n.number_not_in_salesforce__c AS sub_kpi_9 ,
                               CAST ('-' AS varchar) AS sub_kpi_10 -- not used
 ,
                                    COUNT (*)--			, *

FROM salesforce.natterbox_call_reporting_object__c n
LEFT JOIN salesforce.user u ON n.ownerid = u.sfid
LEFT JOIN salesforce.contact co ON n.relatedcontact__c = co.sfid
LEFT JOIN salesforce.account a ON n.account__c = a.sfid
LEFT JOIN salesforce.lead l ON n.lead__c = l.sfid
WHERE calldirection__c = 'Inbound'
    AND e164callednumber__c IN ('493030807264',
                                '41435084849')
    AND n.call_start_date_time__c::date = 'YESTERDAY'
    AND ((n.callringseconds__c < 5
          AND n.callconnectedcheckbox__c IS FALSE) IS FALSE)
GROUP BY n.call_start_date_time__c::date ,
         n.e164callednumber__c ,
         n.callconnectedcheckbox__c ,
         n.wrapup_string_1__c ,
         n.relatedcontact__c ,
         n.account__c ,
         n.callringseconds__c ,
         n.lead__c ,
         co.type__c ,
         a.type__c ,
         co.name ,
         a.name ,
         l.name ,
         u.name ,
         n.number_not_in_salesforce__c ;

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM B2B Outbound Calls (count)
-- Created on: 07/11/2018

DELETE
FROM bi.CM_cases
WHERE date = 'YESTERDAY'
    AND kpi = 'Outbound Calls';


INSERT INTO bi.CM_cases
SELECT TO_CHAR (n.call_start_date_time__c::date,'YYYY-IW') AS date_part ,
       MIN (n.call_start_date_time__c::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                CAST ('BAT CM' AS varchar) AS origin,
                     CAST ('B2B' AS varchar) AS TYPE ,
                          CAST ('Outbound Calls' AS varchar) AS kpi ,
                               CAST ('Count' AS varchar) AS sub_kpi_1 ,
                                    n.callconnectedcheckbox__c AS sub_kpi_2 ,
                                    n.wrapup_string_1__c AS sub_kpi_3 ,
                                    CASE
                                        WHEN n.relatedcontact__c IS NOT NULL THEN co.type__c
                                        WHEN n.account__c IS NOT NULL THEN a.type__c
                                        WHEN n.lead__c IS NOT NULL THEN 'lead'
                                        ELSE 'unknown'
                                    END AS sub_kpi_4 ,
                                    CASE
                                        WHEN n.relatedcontact__c IS NOT NULL THEN co.name
                                        WHEN n.account__c IS NOT NULL THEN a.name
                                        WHEN n.lead__c IS NOT NULL THEN l.name
                                        ELSE 'unknown'
                                    END AS sub_kpi_5 ,
                                    u.name AS sub_kpi_6 ,
                                    CAST ('-' AS varchar) AS sub_kpi_7 -- not used
 ,
                                         CASE
                                             WHEN n.relatedcontact__c IS NOT NULL THEN n.relatedcontact__c
                                             WHEN n.account__c IS NOT NULL THEN n.account__c
                                             WHEN n.lead__c IS NOT NULL THEN n.lead__c
                                             ELSE 'unknown'
                                         END AS sub_kpi_8 ,
                                         n.number_not_in_salesforce__c AS sub_kpi_9 ,
                                         CAST ('-' AS varchar) AS sub_kpi_10 -- not used
 ,
                                              COUNT (*)--			, *

FROM salesforce.natterbox_call_reporting_object__c n
LEFT JOIN salesforce.user u ON n.ownerid = u.sfid
LEFT JOIN salesforce.contact co ON n.relatedcontact__c = co.sfid
LEFT JOIN salesforce.account a ON n.account__c = a.sfid
LEFT JOIN salesforce.lead l ON n.lead__c = l.sfid
WHERE calldirection__c = 'Outbound'
    AND n.department__c LIKE 'CM'
    AND co.type__c NOT LIKE 'customer-b2c'
    AND n.call_start_date_time__c::date = 'YESTERDAY'
GROUP BY n.call_start_date_time__c::date ,
         n.e164callednumber__c ,
         n.callconnectedcheckbox__c ,
         n.wrapup_string_1__c ,
         n.relatedcontact__c ,
         n.account__c ,
         n.lead__c ,
         co.type__c ,
         a.type__c ,
         co.name ,
         a.name ,
         l.name ,
         u.name ,
         n.number_not_in_salesforce__c ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: CM Kpis Traffic Lights (count)
-- Created on: 05/12/2018

DELETE
FROM bi.CM_cases
WHERE kpi = 'Traffic Light';


INSERT INTO bi.CM_cases
SELECT TO_CHAR (traffic_lights.date,'YYYY-IW') AS date_part ,
       MIN (traffic_lights.date::date) AS date ,
           CAST ('-' AS varchar) AS locale ,
                CAST ('-' AS varchar) AS origin ,
                     CAST ('B2B' AS varchar) AS TYPE ,
                          CAST ('Traffic Light' AS varchar) AS kpi ,
                               CAST ('Count' AS varchar) AS sub_kpi_1 ,
                                    traffic_lights.avg_traffic_light AS sub_kpi_2 ,
                                    CASE
                                        WHEN traffic_lights.avg_traffic_light IS NULL THEN 'red'
                                        WHEN traffic_lights.avg_traffic_light <= 2.4 THEN 'red'
                                        WHEN traffic_lights.avg_traffic_light > 2.4
                                             AND traffic_lights.avg_traffic_light <= 2.9 THEN 'yellow'
                                        WHEN traffic_lights.avg_traffic_light >2.9 THEN 'green'
                                        ELSE '-'
                                    END AS sub_kpi_3,
                                    CASE
                                        WHEN traffic_lights.avg_traffic_light IS NULL THEN 1
                                        WHEN traffic_lights.avg_traffic_light <= 2.4 THEN 1
                                        WHEN traffic_lights.avg_traffic_light > 2.4
                                             AND traffic_lights.avg_traffic_light <= 2.9 THEN 2
                                        WHEN traffic_lights.avg_traffic_light >2.9 THEN 3
                                        ELSE 0
                                    END AS sub_kpi_4,
                                    CAST ('-' AS varchar) AS sub_kpi_5 ,
                                         CAST ('-' AS varchar) AS sub_kpi_6 ,
                                              CAST ('-' AS varchar) AS sub_kpi_7 ,
                                                   CAST ('-' AS varchar) AS sub_kpi_8 ,
                                                        CAST ('-' AS varchar) AS sub_kpi_9 ,
                                                             CAST ('-' AS varchar) AS sub_kpi_10,
                                                                  COUNT(*) AS value
FROM bi.opportunity_traffic_light_new traffic_lights
GROUP BY traffic_lights.date ,
         traffic_lights.avg_traffic_light ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'