

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_weekday_revenue(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_weekday_revenue';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.weekday_revenue;
CREATE TABLE bi.weekday_revenue as 


SELECT

  t8.year,
  CONCAT('WK',t8.week::text) as week,
  t8.day_of_week,
  t8.polygon,
  CASE WHEN t8.order_type = 2 THEN 'cleaning-b2b' ELSE 'cleaning-b2c' END as type,
  ROUND(AVG(revenue)) as revenue
  
FROM

  (SELECT
    
    EXTRACT(YEAR FROM o.effectivedate) as year,
    EXTRACT(WEEK FROM o.effectivedate) as week,     
    TO_CHAR(o.effectivedate, 'day') as day_of_week,
    o.polygon,
    o.order_type,
    SUM(o.gmv_eur_net) as revenue
          
  FROM
          
    bi.orders o
                
  WHERE
          
    o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START','FULFILLED')
    -- AND o.effectivedate > (current_date - 60)
    -- AND o.effectivedate < current_date
    AND o.polygon IS NOT NULL
    AND o.effectivedate NOT IN ('2017-04-14', '2017-04-16', '2017-05-01', '2017-06-05', '2017-10-03', '2017-10-03', '2017-12-25', '2017-12-26')
    AND o.effectivedate >= '2018-01-01'
            
  GROUP BY
    
    week,   
    day_of_week,
    o.effectivedate,
    o.polygon,
    o.order_type
            
  ORDER BY
    
    week,     
    day_of_week,
    o.polygon
    -- o.type
    ) as t8
  
WHERE

  t8.order_type IS NOT NULL
  
GROUP BY

  t8.year,
  t8.week,
  t8.day_of_week,
  t8.polygon,
  t8.order_type
  
ORDER BY

  year,
  week,
  t8.polygon;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'