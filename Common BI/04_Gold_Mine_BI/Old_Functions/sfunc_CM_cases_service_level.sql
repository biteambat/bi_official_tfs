

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_CM_cases_service_level(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_CM_cases_service_level';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- CM B2B Case Service Level

DROP TABLE IF EXISTS bi.CM_cases_service_level;


CREATE TABLE bi.CM_cases_service_level AS
SELECT basis.* -- ---------------------------------------------------------------------------------- difference: case opened / reopened / type change -> case closed
-- -
-- - working h Mo-Fr 08:00:00 - 17:00:00 bi
-- - summertime timezone difference of 1 h
 ,
       CASE
           WHEN basis.date1_opened::date IS NULL THEN 'rule 0.1' -- opened date missing

           WHEN basis.date2_closed::date IS NULL THEN 'rule 0.2' -- still open case

           WHEN basis.date1_opened::TIMESTAMP > basis.date2_closed::TIMESTAMP THEN 'rule 1.0' -- open after closed

           WHEN basis.date1_opened::date = basis.date2_closed::date THEN -- is same date
 (CASE
      WHEN basis.date1_opened::TIME < TIME '07:00:00.0' THEN 'rule 2.1' -- opened befor working day start

      ELSE 'rule 2.2'
  END)--

           ELSE (CASE
                     WHEN TIME '16:00:00.0' < basis.date1_opened::TIME THEN 'rule 3.1' -- opened after working day start

                     WHEN date_part('dow', basis.date1_opened::date) IN ('6',
                                                                         '0') THEN 'rule 3.2' -- opened at the weekend

                     WHEN basis.date1_opened::TIME < TIME '07:00:00.0' THEN 'rule 3.3' -- opened before working day start

                     ELSE 'rule 3.4' -- opened at a weekday within working hours

                 END)
       END AS rule_set -- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau
 ,
       CASE
           WHEN basis.date1_opened::TIMESTAMP > basis.date2_closed::TIMESTAMP THEN -1 -- if owner change date and first contact date on the same day, calculate difference between times in minutes

           WHEN basis.date1_opened::date = basis.date2_closed::date THEN -- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
 (CASE
      WHEN basis.date1_opened::TIME < TIME '07:00:00.0' THEN DATE_PART ('hour', basis.date2_closed::TIME - TIME '07:00:00.0') * 60 + DATE_PART ('minute', basis.date2_closed::TIME - TIME '07:00:00.0')
      ELSE DATE_PART ('hour', basis.date2_closed::TIMESTAMP - basis.date1_opened::TIMESTAMP) * 60 + DATE_PART ('minute', basis.date2_closed::TIMESTAMP - basis.date1_opened::TIMESTAMP)
  END) -- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes

           ELSE -- minutes pased on the owner change date UNTIL END OF WORK
 -- owner changed after working day
 (CASE
      WHEN TIME '16:00:00.0' < basis.date1_opened::TIME THEN 0 -- owner change at the weekend

      WHEN date_part('dow', basis.date1_opened::date) IN ('6',
                                                          '0') THEN 0
      ELSE -- owner changed before working day
 (CASE
      WHEN basis.date1_opened::TIME < TIME '07:00:00.0' THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60
      ELSE DATE_PART ('hour', TIME '16:00:00.0' - basis.date1_opened::TIME) * 60 + DATE_PART ('minute', TIME '16:00:00.0' - basis.date1_opened::TIME)
  END)
  END) + -- minutes pased on the first contact date beginning at the morning working time
 (CASE
      WHEN basis.date2_closed::TIME < TIME '07:00:00.0' THEN 0
      WHEN basis.date2_closed::TIME < TIME '16:00:00.0' THEN DATE_PART ('hour', basis.date2_closed::TIME - TIME '07:00:00.0') * 60 + DATE_PART ('minute', basis.date2_closed::TIME - TIME '07:00:00.0')
      ELSE (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0')) * 60
  END) + -- create a list of all date incl. owner change date and first contact date
 -- COUNT the working days and SUBTRACT 2 days
 -- 2 days: owner change date, first contact date -> minutes are calculated separatly
 (CASE
      WHEN (
                (SELECT COUNT(*)
                 FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
                 WHERE date_part('dow', dd) NOT IN ('6',
                                                    '0')) -2) > 0 -- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
 THEN (
           (SELECT COUNt(*)
            FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
            WHERE date_part('dow', dd) NOT IN ('6',
                                               '0')) -2) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
      ELSE 0
  END)
       END AS SVL_Minutes -- ------------------------------------------------------------------------------------------------------------------------------------------------
-- - SVL Customer, dont care about working hours
-- -
 -- if the case opened was after the case closed date just use the value "-1" -> this will be used as a filter in tableau
 ,
       CASE
           WHEN basis.date1_opened::TIMESTAMP > basis.date2_closed::TIMESTAMP THEN -1 -- if case opened date and case closed date on the same day, calculate difference between times in minutes

           WHEN basis.date1_opened::date = basis.date2_closed::date THEN DATE_PART ('hour', basis.date2_closed::TIMESTAMP - basis.date1_opened::TIMESTAMP) * 60 + DATE_PART ('minute', basis.date2_closed::TIMESTAMP - basis.date1_opened::TIMESTAMP) -- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes

           ELSE -- date opened to EOD in minutes
 CASE
     WHEN date_part('dow', basis.date1_opened) IN ('6',
                                                   '0') THEN 0
     ELSE (DATE_PART ('hour', date_trunc ('day', basis.date1_opened::date) + interval '1 day' - basis.date1_opened) * 60 + DATE_PART ('minute', date_trunc ('day', basis.date1_opened::date) + interval '1 day' - basis.date1_opened))
 END -- + minutes on the case closed date
 + (DATE_PART ('hour', basis.date2_closed) * 60 + DATE_PART ('minute', basis.date2_closed)) + -- create a list of all date incl. case opened date and case closed date
 -- COUNT the working days
 -- and SUBTRACT 2 days
 -- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
 (CASE
      WHEN (
                (SELECT COUNT(*)
                 FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
                 WHERE date_part('dow', dd) NOT IN ('6',
                                                    '0')) - 2) > 0 -- in case the working days are > 0 -> workind days * 24 * 60 minutes = working days in minutes within working h
 THEN (
           (SELECT COUNT(*)
            FROM generate_series (basis.date1_opened::date, basis.date2_closed::date, '1 day'::interval) dd
            WHERE date_part('dow', dd) NOT IN ('6',
                                               '0')) - CASE
                                                           WHEN date_part('dow', basis.date1_opened) IN ('6') THEN 1
                                                           ELSE 2
                                                       END) * 24 * 60
      ELSE 0
  END)
       END AS SVL_Customer ,
       cas.createddate case_createddate ,
       cas.isclosed case_isclosed ,
       cas.ownerid case_ownerid ,
       u.name case_owner ,
       cas.origin case_origin ,
       cas.type case_type ,
       cas.reason case_reason ,
       cas.status case_status ,
       cas.contactid contactid ,
       co.name contact_name ,
       co.type__c contact_type ,
       co.company_name__c contact_companyname ,
       cas.order__c orderid ,
       o.type order_type ,
       cas.accountid professionalid ,
       a.name professional ,
       a.company_name__c professional_companyname ,
       cas.opportunity__c opportunityid ,
       opp.name opportunity ,
       opp.grand_total__c grand_total ,
       u2.name event_user
FROM bi.cm_cases_service_level_basis basis
LEFT JOIN salesforce.CASE cas ON basis.case_id = cas.sfid
LEFT JOIN salesforce.user u ON cas.ownerid = u.sfid
LEFT JOIN salesforce.opportunity opp ON cas.opportunity__c = opp.sfid
LEFT JOIN salesforce.contact co ON cas.contactid = co.sfid
LEFT JOIN salesforce.account a ON cas.accountid = a.sfid
LEFT JOIN salesforce.order o ON cas.order__c = o.sfid
LEFT JOIN salesforce.user u2 ON basis.event_by = u2.sfid -- WHERE basis.case_id = '5000J00001Q5PmtQAF'
-- LIMIT 100
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'