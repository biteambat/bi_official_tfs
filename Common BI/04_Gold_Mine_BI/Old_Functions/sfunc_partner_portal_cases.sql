

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_partner_portal_cases(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_partner_portal_cases';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- Short Description: all the cases created by the partners via the portal

DROP TABLE IF EXISTS bi.partner_portal_cases;
CREATE TABLE bi.partner_portal_cases AS 

SELECT

    TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
    o.createddate,
    o.accountid,
    b.delivery_areas__c,
    a.company_name__c,
    CASE WHEN a.type__c = 'partner' THEN 'None' ELSE a.name END as cleaner,
    a.type__c,
    o.sfid as sfid_case,
    o.reason,
    o.origin,
    o.subject,
    o.description,  
    COUNT(DISTINCT o.sfid) as cases
    
FROM

    salesforce.case o
    
LEFT JOIN

    salesforce.account a
    
ON

    o.accountid = a.sfid

LEFT JOIN

    salesforce.account b
    
ON 

    a.company_name__c = b.name
    
WHERE

    (o.origin = 'Partner portal' OR o.origin LIKE 'Partner - portal%')
    AND LOWER(o.description) NOT LIKE '%test%'
    AND LOWER(o.description) NOT LIKE '%test%'
    AND LOWER(a.name) NOT LIKE '%test%'
    AND a.test__c IS FALSE
    AND b.test__c IS FALSE
    AND a.company_name__c NOT IN ('##')
    
GROUP BY

    TO_CHAR(o.createddate, 'YYYY-MM'),
    o.createddate,
    o.accountid,
    b.delivery_areas__c,
    a.name,
    a.company_name__c,
    a.type__c,
    o.sfid,
    o.reason,
    o.origin,
    o.description,
    o.subject
    
ORDER BY

    TO_CHAR(o.createddate, 'YYYY-MM') desc;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'