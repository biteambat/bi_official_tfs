

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_timelapses_recruitment_process(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_timelapses_recruitment_process';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.timelapses_recruitment_process;
CREATE TABLE bi.timelapses_recruitment_process as 


(SELECT

	EXTRACT(year from l.createddate) as created_year,
	EXTRACT(month from l.createddate) as created_month,
	l.countrycode,
	l.delivery_area__c as polygon,
	t1.*
	
FROM 
	
	(SELECT
	
		leadid,
		min(CASE WHEN field = 'created' THEN createddate ELSE NULL END) as created_date,
		min(CASE WHEN field = 'Status' THEN
												 CASE WHEN oldvalue IN ('New lead', 'New Leads')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_touched,
		CASE WHEN (EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('New lead', 'New Leads') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int) = 0 THEN 1 ELSE (EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('New lead', 'New Leads') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int) END as time_to_touched,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents before contract')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_documents_before_contract,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents before contract') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_documents_before_contract,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview to be scheduled')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_interview_to_be_scheduled,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview to be scheduled') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_interview_to_be_scheduled,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview scheduled')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_interview_scheduled,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview scheduled') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_interview_scheduled,										 
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for contract')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_waiting_for_contract,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for contract') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_waiting_for_contract,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for greenlight')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_waiting_for_greenlight,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for greenlight') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_waiting_for_greenlight,
	    min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents after interview', 'Contract ready')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_after_interview,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents after interview', 'Contract ready') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_after_interview,
		min(CASE WHEN field = 'leadConverted' THEN createddate ELSE NULL END) as conversion_date,												 
		EXTRACT(epoch FROM (min(CASE WHEN field = 'leadConverted' THEN createddate ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_conversion								 
			
			
		
	FROM
	
		salesforce.leadhistory
		
		
	GROUP BY 
	
		leadid
	
		
	ORDER BY
	
		leadid) as t1
		
		
LEFT JOIN

	salesforce.lead l
	
	
ON

	l.sfid = t1.leadid	
	
	
WHERE 

	l.delivery_area__c IS NOT NULL
	AND l.ownerid NOT IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC'))
;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'