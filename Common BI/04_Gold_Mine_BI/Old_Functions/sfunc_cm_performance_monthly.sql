

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_cm_performance_monthly(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_cm_performance_monthly';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Short Description: monthly CM Team and Agent Performance (B2B)

DROP TABLE IF EXISTS bi.cm_performance_monthly;


CREATE TABLE bi.cm_performance_monthly AS
SELECT * ,
       CASE
           WHEN performance.inbound_calls > 0 THEN performance.inbound_calls >= (performance.target_inbound_calls * performance.FTE)
       END AS performance_inbound_calls ,
       CASE
           WHEN performance.cases_closed > 0 THEN performance.cases_closed >= (performance.target_cases_closed * performance.FTE)
       END AS performance_cases_closed ,
       CASE
           WHEN performance.svl_share_within_24h > 0 THEN performance.svl_share_within_24h >= (performance.targert_svl * performance.FTE)
       END AS performance_svl
FROM
    (SELECT MIN(cases.date) AS date ,
            cases.sub_kpi_6 AS agent -- FTE of every CM agent to calculate the share of the targets
 ,
            CASE
                WHEN cases.sub_kpi_6 = 'Katharina Kühner' THEN 1
                WHEN cases.sub_kpi_6 = 'Katharina Kuehner' THEN 1
                WHEN cases.sub_kpi_6 = 'Sercan Tas' THEN 1
                WHEN cases.sub_kpi_6 = 'André Bauß' THEN 1
                WHEN cases.sub_kpi_6 = 'Carmen Haas' THEN 1
                WHEN cases.sub_kpi_6 = 'Julian Schäfer' THEN 1
                WHEN cases.sub_kpi_6 = 'Susanne Marino' THEN 1
                WHEN cases.sub_kpi_6 = 'Nathalie Tostmann' THEN 1
                WHEN cases.sub_kpi_6 = 'Danny Taszarek' THEN 1
                WHEN cases.sub_kpi_6 = 'Felix Liedtke' THEN 1
                WHEN cases.sub_kpi_6 = 'Vivien Greve' THEN 1
                WHEN cases.sub_kpi_6 = 'Daniela Kaim' THEN 1
                WHEN cases.sub_kpi_6 = 'Salim Abdoulaye' THEN 1
                WHEN cases.sub_kpi_6 = 'Lennart Bär' THEN 0.5
                WHEN cases.sub_kpi_6 = 'Karla Sorgato' THEN 0.5
                WHEN cases.sub_kpi_6 = 'CM Support2' THEN 0.5
                WHEN cases.sub_kpi_6 = 'Marleen Dreyer' THEN 0.5
                ELSE 0
            END AS FTE ,
            SUM(CASE
                    WHEN cases.kpi = 'Inbound Calls'
                         AND cases.sub_kpi_2 = 'true' THEN cases.value
                    ELSE 0
                END) AS inbound_calls ,
            CAST ('20' AS numeric) /5 * 21 AS target_inbound_calls ,
                 SUM(CASE
                         WHEN cases.kpi = 'Closed Cases' THEN cases.value
                         ELSE 0
                     END) AS cases_closed ,
                 CAST ('90' AS numeric) /5 * 21 AS target_cases_closed ,
                      svl_final.svl_share_within_24h AS svl_share_within_24h ,
                      svl_final.cases AS svl_casesclosed ,
                      svl_final.svl_cases_within_24h AS svl_cases_within_24h ,
                      CAST ('0.95' AS numeric) AS targert_svl
     FROM bi.cm_cases cases
     LEFT JOIN
         ( SELECT svl_details.month AS MONTH ,
                  MIN(svl_details.opened_date) AS MinDate ,
                  agent ,
                  COUNT(*) AS cases ,
                  SUM(svl_details.svl_within_24h) AS svl_cases_within_24h ,
                  ROUND(SUM(svl_details.svl_within_24h)/ COUNT(*)::numeric,4) AS svl_share_within_24h
          FROM
              ( SELECT --		*
 TO_CHAR(svl.date1_opened, 'YYYY-MM') AS MONTH ,
 svl.date1_opened::date AS opened_date ,
 svl.event_user AS agent ,
 svl.case_id AS case_id ,
 svl.case_number AS case_number ,
 svl.type AS svl_type ,
 svl.closed AS svl_has_closeddate ,
 svl.case_isclosed AS case_stil_closed ,
 svl.case_type AS case_type ,
 svl.case_origin AS case_origin ,
 svl.case_reason AS case_reason ,
 svl.case_status AS case_status,
 svl.svl_customer AS svl_customer -- IMPROVEMENT: maybe the following should be included in the SVL function!
 ,
 CASE
     WHEN svl.svl_customer/60 <= 24 THEN 1
     ELSE 0
 END AS svl_within_24h ,
 CASE
     WHEN svl.svl_customer = 0
          AND svl.type = 'Reopened Case' THEN 1
     ELSE 0
 END AS exclude_reopened_closed_due_mail -- exclude when 1
 ,
 CASE
     WHEN svl.type = 'New Case'
          AND svl.case_origin = 'direct email outbound' THEN 1
     ELSE 0
 END AS exclude_direct_email_outbound -- exclude when 1
 ,
 CASE
     WHEN svl.type = 'Damage'
          OR svl.case_reason = '%Damage%' THEN 1
     ELSE 0
 END AS exclude_damage_cases -- exclude when 1
 ,
 CASE
     WHEN svl.case_reason = 'Opportunity - Onboarding' THEN 1
     ELSE 0
 END AS exclude_onboarding_cases -- exclude when 1
 ,
 CASE
     WHEN svl.case_status = 'Closed'
          AND svl.case_isclosed IS FALSE THEN 1
     ELSE 0
 END AS exclude_created_within_clean_up -- exclude when 1
 ,
 CASE
     WHEN svl.case_reason LIKE 'Customer - Retention%' THEN 1
     ELSE 0
 END AS exclude_retention -- exclude when 1

               FROM bi.cm_cases_service_level svl
               WHERE NOT svl.type = '# Reopened'
                   AND svl.date1_opened::date >= '2019-01-01' ) AS svl_details
          WHERE svl_details.exclude_reopened_closed_due_mail = 0
              AND svl_details.exclude_direct_email_outbound = 0
              AND svl_details.exclude_damage_cases = 0
              AND svl_details.exclude_onboarding_cases = 0
              AND svl_details.exclude_created_within_clean_up = 0
              AND svl_details.exclude_retention = 0
          GROUP BY MONTH ,
                   agent ) AS svl_final ON (svl_final.month = TO_CHAR (cases.date,'YYYY-MM')
                                            AND svl_final.agent = cases.sub_kpi_6)
     WHERE cases.date >= '2019-01-01'
     GROUP BY cases.sub_kpi_6,
              svl_final.svl_share_within_24h ,
              svl_final.cases,
              svl_final.svl_cases_within_24h) AS performance ;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'