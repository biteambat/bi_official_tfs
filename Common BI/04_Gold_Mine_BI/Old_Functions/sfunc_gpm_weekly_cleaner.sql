

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_gpm_weekly_cleaner(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_gpm_weekly_cleaner';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.gpm_weekly_cleaner;
CREATE TABLE bi.gpm_weekly_cleaner as 
SELECT
  year_week,
  mindate,
  professional__c,
  name,
  delivery_area,
  contract_start,
  contract_end,
  weekly_hours,
  absence_hours,
  total_hours,
  SUM(worked_hours) as worked_hours,
  CASE WHEN SUM(worked_hours) > 0 THEN SUM(gmv)/SUM(worked_hours) ELSE 0 END as pph,
  revenue,
  salary_payed,
  hr_contract_weekly_hours_max__c as max_weekly_hours,
  revenue-salary_payed as gp,
  CASE WHEN revenue > 0 THEN (revenue-salary_payed)/revenue ELSE 0 END  as gpm
FROM
  bi.gpm_per_cleaner_per_week t1
LEFT JOIN
  salesforce.account t2
ON
  (t1.professional__c = t2.sfid)
WHERE
  (diff_week_contractend > 6 OR diff_week_contractend IS NULL)
GROUP BY
  year_week,
  mindate,
  contract_start,
  total_hours,
  contract_end,
  absence_hours,
  professional__c,
  name,
  weekly_hours,
  hr_contract_weekly_hours_max__c,
  delivery_area,
  salary_payed,
  revenue;


INSERT INTO bi.gpm_weekly_cleaner
SELECT
  year_week,
  mindate,
  professional__c,
  name,
  UPPER(LEFT(delivery_area, 2)),
  contract_start,
  contract_end,
  weekly_hours,
  absence_hours,
  total_hours,
  SUM(worked_hours) as worked_hours,
  CASE WHEN SUM(worked_hours) > 0 THEN SUM(gmv)/SUM(worked_hours) ELSE 0 END as pph,
  revenue,
  salary_payed,
  hr_contract_weekly_hours_max__c as max_weekly_hours,
  revenue-salary_payed as gp,
  CASE WHEN revenue > 0 THEN (revenue-salary_payed)/revenue ELSE 0 END  as gpm
FROM
  bi.gpm_per_cleaner_per_week t1
LEFT JOIN
  salesforce.account t2
ON
  (t1.professional__c = t2.sfid)
WHERE
  (diff_week_contractend > 6 OR diff_week_contractend IS NULL)
GROUP BY
  year_week,
  mindate,
  contract_start,
  total_hours,
  contract_end,
  absence_hours,
  professional__c,
  name,
  weekly_hours,
  hr_contract_weekly_hours_max__c,
  delivery_area,
  salary_payed,
  revenue;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'