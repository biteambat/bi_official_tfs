

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_closed_opps(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_closed_opps';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- Short Description: this table contains the data concerning the opportunities closed on every working day

DROP TABLE IF EXISTS bi.closed_opps;
CREATE TABLE bi.closed_opps AS 

SELECT

	TO_CHAR(o.closedate, 'YYYY-MM') as year_month,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END as converted_in_opportunity,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c as closed_by_id,
	u.name as owner_name,
	o.delivery_area__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.closedate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.sfid = ooo.sfid
	
LEFT JOIN

	salesforce.likeli__C oooo
	
ON

	o.sfid = oooo.opportunity__c
	
LEFT JOIN

	salesforce.user u
	
ON

	o.closed_by__c = u.sfid
	
WHERE

	o.test__c IS FALSE
	AND o.acquisition_channel__c IN ('web', 'inbound')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.closedate <= current_date
	AND o.stagename NOT LIKE '%LOST%'
	AND o.stagename IN ('WRITTEN CONFIRMATION', 'WON','PENDING')
	AND o.closed_by__c IS NOT NULL
	
GROUP BY

	year_month,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	o.acquisition_channel__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c,
	u.name,
	o.delivery_area__c
	
ORDER BY

	TO_CHAR(o.closedate, 'YYYY-MM-DD') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'