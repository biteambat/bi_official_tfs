

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_monitoring_subcontractor(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_monitoring_subcontractor';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.monitoring_subcontractor;
CREATE TABLE bi.monitoring_subcontractor as

SELECT

	big_table.year_month,
	big_table.mindate,
	big_table.polygon,
	big_table.name_partner,
	big_table.sfid_partner,
	big_table.company_name__c,
	big_table.cost_sub_per_hour,
	big_table.number_employees_tot,
	small_table.cleaners_platform as number_cleaners_platform,
	-- CASE WHEN big_table.number_employees_tot > 0 THEN ROUND(small_table.cleaners_platform/big_table.number_employees_tot::numeric, 2) ELSE 0 END as share_partner,
	MAX(big_table.number_cleaners_active) as number_cleaners_active,
	-- CASE WHEN big_table.number_cleaners_platform > 0 THEN ROUND(big_table.number_cleaners_active/small_table.cleaners_platform::numeric, 2) ELSE 0 END as share_active_platform,
	MAX(big_table.number_customers) as number_customers,
	big_table.hours_executed,
	big_table.avg_hours_cleaner,
	big_table.avg_hours_customer,
	big_table.revenue,
	big_table.cost_sub,
	big_table.gp,
	big_table.gpm


FROM

(SELECT

	finaltable1.year_month,
	finaltable1.mindate,
	finaltable1.polygon,
	finaltable1.name_partner,
	finaltable1.sfid_partner,
	finaltable1.company_name__c,
	finaltable1.cost_sub_per_hour,
	finaltable1.number_employees_tot,
	finaltable1.number_cleaners_platform,
	-- CASE WHEN finaltable1.number_employees_tot > 0 THEN ROUND(finaltable1.number_cleaners_platform/finaltable1.number_employees_tot::numeric, 2) ELSE 0 END as share_partner,
	finaltable2.number_cleaners_platform as number_cleaners_active,
	-- CASE WHEN finaltable1.number_cleaners_platform > 0 THEN ROUND(finaltable2.number_cleaners_platform/finaltable1.number_cleaners_platform::numeric, 2) ELSE 0 END as share_active_platform,
	finaltable1.number_customers,
	finaltable1.hours_executed,
	finaltable1.avg_hours_cleaner,
	finaltable1.avg_hours_customer,
	finaltable1.revenue,
	finaltable1.cost_sub,
	finaltable1.gp,
	finaltable1.gpm

FROM

	(SELECT
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
	
	FROM
	
		(SELECT
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			cost_sub_per_hour,
			number_employees_tot,
			COUNT(DISTINCT name_cleaner) as number_cleaners_platform,
			-- CASE WHEN SUM(order_duration) > 0 THEN 1 ELSE 0 END as number_active_cleaners,
			COUNT(DISTINCT contact_customer) as number_customers,
			SUM(order_duration) as hours_executed,
			CASE WHEN COUNT(DISTINCT sfid_professional) > 0 THEN SUM(order_duration)/COUNT(DISTINCT sfid_professional) ELSE 0 END as avg_hours_cleaner,
			CASE WHEN COUNT(DISTINCT contact_customer) > 0 THEN SUM(order_duration)/COUNT(DISTINCT contact_customer) ELSE 0 END as avg_hours_customer,
			SUM(revenue) as revenue,
			SUM(cost_sub) as cost_sub,
			SUM(gp) as gp,
			CASE WHEN SUM(revenue) > 0 THEN SUM(gp)/SUM(revenue) ELSE 0 END as gpm
		
		FROM	
			
			(SELECT
			
				TO_CHAR(o.effectivedate::date,'YYYY-MM') as year_month,
				-- EXTRACT(YEAR FROM o.effectivedate) as year,
				-- EXTRACT(MONTH FROM o.effectivedate) as month,
				o.effectivedate::date as mindate,
				t1.name_partner,
			   t1.status__c,
				t1.sfid_partner,
				t1.parentid_company,
				t1.company_name__c,
				t1.number_employees_tot,
			
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as hourly_cost_sub,
			
				-- t1.hourly_cost_sub,
				t1.parentid_cleaner,
				t1.sfid_cleaner,
				t1.name_cleaner,
				o.professional__c as sfid_professional,
				o.contact__c as contact_customer,
				o.sfid as order_reference,
				t1.polygon,
				o.pph__c as price_order,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.order_duration__c END as order_duration,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END as revenue,
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as cost_sub_per_hour,
				CASE WHEN o.status LIKE '%NOSHOW PROFESSIONAL%' THEN 0 ELSE (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as cost_sub,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as gp,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE (CASE WHEN (o.eff_pph*o.order_duration__c) > 0 THEN (o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c)/(o.eff_pph*o.order_duration__c) ELSE 0 END) END as gpm
			
			FROM
			
				(SELECT
	
					a.name as name_partner,
					a.status__c,
					a.type__c,
					a.hr_contract_start__c as contract_start,
					a.hr_contract_end__c as contract_end,
					a.sfid as sfid_partner,
					a.parentid as parentid_company,
					a.company_name__c,
					b.delivery_areas__c as polygon,
					a.numberofemployees as number_employees_tot,
					a.pph__c as hourly_cost_sub,
					b.parentid as parentid_cleaner,
					b.sfid as sfid_cleaner,
					b.name as name_cleaner
					
				FROM
				
					salesforce.account a
					
				LEFT JOIN
				
					salesforce.account b
					
				ON 
				
					a.sfid = b.parentid
					
				WHERE
				
					-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
					a.type__c = 'partner'
					AND a.type__c NOT LIKE 'master'
					AND a.type__c NOT LIKE 'master'
					AND a.company_name__c NOT LIKE '%BAT%'
					AND a.company_name__c IS NOT NULL
					AND a.company_name__c NOT LIKE ''
					AND LOWER(a.name) not like '%test%'
					AND LOWER(a.company_name__c) not like '%test%'
					AND a.test__c IS FALSE
					) as t1
					
			LEFT JOIN
			
				bi.orders o
				
			ON 
			
				t1.sfid_cleaner = o.professional__c
				
			--- WHERE 
			
				-- o.status NOT LIKE '%CANCELLED%'
				
			ORDER BY
			
				year_month) as t3
				
		GROUP BY
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			number_employees_tot,
			cost_sub_per_hour
			
		ORDER BY
		
			year_month,
			name_partner) as table4
			
	GROUP BY
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
		
	ORDER BY
	
		year_month,
		name_partner) as finaltable1
		
LEFT JOIN

	(SELECT
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
	
	FROM
	
		(SELECT
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			cost_sub_per_hour,
			number_employees_tot,
			COUNT(DISTINCT sfid_professional) as number_cleaners_platform,
			-- CASE WHEN SUM(order_duration) > 0 THEN 1 ELSE 0 END as number_active_cleaners,
			COUNT(DISTINCT contact_customer) as number_customers,
			SUM(order_duration) as hours_executed,
			CASE WHEN COUNT(DISTINCT sfid_professional) > 0 THEN SUM(order_duration)/COUNT(DISTINCT sfid_professional) ELSE 0 END as avg_hours_cleaner,
			CASE WHEN COUNT(DISTINCT contact_customer) > 0 THEN SUM(order_duration)/COUNT(DISTINCT contact_customer) ELSE 0 END as avg_hours_customer,
			SUM(revenue) as revenue,
			SUM(cost_sub) as cost_sub,
			SUM(gp) as gp,
			CASE WHEN SUM(revenue) > 0 THEN SUM(gp)/SUM(revenue) ELSE 0 END as gpm
		
		FROM	
			
			(SELECT
			
				TO_CHAR(o.effectivedate::date,'YYYY-MM') as year_month,
				-- EXTRACT(YEAR FROM o.effectivedate) as year,
				-- EXTRACT(MONTH FROM o.effectivedate) as month,
				o.effectivedate::date as mindate,
				t1.name_partner,
			   t1.status__c,
				t1.sfid_partner,
				t1.parentid_company,
				t1.company_name__c,
				t1.number_employees_tot,
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as hourly_cost_sub,
				t1.parentid_cleaner,
				t1.sfid_cleaner,
				t1.name_cleaner,
				o.professional__c as sfid_professional,
				o.contact__c as contact_customer,
				o.sfid as order_reference,
				o.polygon,
				o.pph__c as price_order,
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.order_duration__c END as order_duration,
				CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.eff_pph*o.order_duration__c END as revenue,
				
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as cost_sub_per_hour,
				
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as cost_sub,
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as gp,
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE (CASE WHEN (o.eff_pph*o.order_duration__c) > 0 THEN (o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c)/(o.eff_pph*o.order_duration__c) ELSE 0 END) END as gpm
			
			FROM
			
				(SELECT
	
					a.name as name_partner,
					a.status__c,
					a.type__c,
					a.hr_contract_start__c as contract_start,
					a.hr_contract_end__c as contract_end,
					a.sfid as sfid_partner,
					a.parentid as parentid_company,
					a.company_name__c,
					b.delivery_areas__c as polygon,
					a.numberofemployees as number_employees_tot,
					a.pph__c as hourly_cost_sub,
					b.parentid as parentid_cleaner,
					b.sfid as sfid_cleaner,
					b.name as name_cleaner
					
				FROM
				
					salesforce.account a
					
				LEFT JOIN
				
					salesforce.account b
					
				ON 
				
					a.sfid = b.parentid
					
				WHERE
				
					-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
					a.type__c = 'partner'
					AND a.type__c NOT LIKE 'master'
					AND a.company_name__c NOT LIKE '%BAT%'
					AND a.company_name__c IS NOT NULL
					AND a.company_name__c NOT LIKE ''
					AND LOWER(a.name) not like '%test%'
					AND LOWER(a.company_name__c) not like '%test%'
					AND a.test__c IS FALSE
					) as t1
					
			LEFT JOIN
			
				bi.orders o
				
			ON 
			
				t1.sfid_cleaner = o.professional__c
				
			WHERE 
			
				o.status NOT LIKE '%CANCELLED%'
				
			ORDER BY
			
				year_month) as t3
				
		GROUP BY
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			number_employees_tot,
			cost_sub_per_hour
			
		ORDER BY
		
			year_month,
			name_partner) as table4
			
	GROUP BY
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
		
	ORDER BY
	
		year_month,
		name_partner) as finaltable2
		
ON 

	finaltable1.sfid_partner = finaltable2.sfid_partner
	AND finaltable1.year_month = finaltable2.year_month
	
-- WHERE 

	-- finaltable1.polygon IS NOT NULL
	) as big_table
	
	
LEFT JOIN


	(SELECT

	name_partner,
	COUNT(DISTINCT sfid_cleaner) as cleaners_platform
	
	FROM

	(SELECT
	
		a.name as name_partner,
		a.status__c,
		a.type__c,
		a.hr_contract_start__c as contract_start,
		a.hr_contract_end__c as contract_end,
		a.sfid as sfid_partner,
		a.parentid as parentid_company,
		a.company_name__c,
		b.delivery_areas__c as polygon,
		a.numberofemployees as number_employees_tot,
		a.pph__c as hourly_cost_sub,
		b.parentid as parentid_cleaner,
		b.sfid as sfid_cleaner,
		b.name as name_cleaner
		
	FROM
	
		salesforce.account a
		
	LEFT JOIN
	
		salesforce.account b
		
	ON 
	
		a.sfid = b.parentid
		
	WHERE
	
		-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
		a.type__c = 'partner'
		AND a.type__c NOT LIKE 'master'
		AND a.company_name__c NOT LIKE '%BAT%'
		AND a.company_name__c IS NOT NULL
		AND a.company_name__c NOT LIKE ''
		AND LOWER(a.name) not like '%test%'
		AND LOWER(a.company_name__c) not like '%test%'
		AND a.test__c IS FALSE
					) as table_plus
					
	GROUP BY

		name_partner)	as small_table
		

ON

	big_table.name_partner = small_table.name_partner

GROUP BY

	big_table.year_month,
	big_table.mindate,
	big_table.polygon,
	big_table.name_partner,
	big_table.sfid_partner,
	big_table.company_name__c,
	big_table.cost_sub_per_hour,
	big_table.number_employees_tot,
	number_cleaners_platform,
	-- number_cleaners_active,
	-- big_table.number_cleaners_active,
	big_table.hours_executed,
	big_table.avg_hours_cleaner,
	big_table.avg_hours_customer,
	big_table.revenue,
	big_table.cost_sub,
	big_table.gp,
	big_table.gpm,
	small_table.cleaners_platform
	
ORDER BY

	year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'