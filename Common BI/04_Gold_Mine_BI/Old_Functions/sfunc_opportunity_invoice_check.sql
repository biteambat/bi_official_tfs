

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_opportunity_invoice_check(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_opportunity_invoice_check';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- Short Description: Opportunity invoice forecast details for the invoice check done by sales

DELETE
FROM bi.opportunity_invoice_check
WHERE TO_CHAR(((DATE - INTERVAL '1day')::DATE), 'YYYY-MM') = TO_CHAR(((CURRENT_DATE - INTERVAL '1day')::DATE), 'YYYY-MM');


INSERT INTO bi.opportunity_invoice_check
SELECT CURRENT_DATE AS DATE , TO_CHAR(((CURRENT_DATE - INTERVAL '1day')::DATE), 'YYYY-MM') AS year_month,
                              o.sfid AS OpportunityId,
                              o.customer__c AS customerId,
                              o.Name,
                              o.StageName,
                              o.status__c AS status,
                              o.supplies__c AS supplies,
                              o.grand_total__c,
                              o.CloseDate,
                              ooo.first_order_date,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM ooo.first_order_date::DATE) = EXTRACT (MONTH
                                                                                             FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
                                        AND (EXTRACT (YEAR
                                                      FROM ooo.first_order_date::DATE) = EXTRACT (YEAR
                                                                                                  FROM (CURRENT_DATE - INTERVAL '1day')::DATE)) THEN 1
                                   ELSE 0
                               END) AS FirstMonth,
                              oo.first_order_date_monthly -- ---------------------------------------------------------------------------------------------------------------------------------------------- orders
,
                              oo.orders AS orders,
                              (CASE
                                   WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled
                                   ELSE 0
                               END) AS Orders_Cancelled,
                              (CASE
                                   WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer
                                   ELSE 0
                               END) AS Orders_Cancelled_Customer,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Professional > 0 THEN oooo.Orders_Cancelled_Professional
                                   ELSE 0
                               END) AS Orders_Cancelled_Professional,
                              (CASE
                                   WHEN oooooooo.Orders_Noshow_Professional > 0 THEN oooooooo.Orders_Noshow_Professional
                                   ELSE 0
                               END) AS Orders_Noshow_Professional,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Mistake > 0 THEN oooo.Orders_Cancelled_Mistake
                                   ELSE 0
                               END) AS Orders_Cancelled_Mistake,
                              (CASE
                                   WHEN ooooooooo.orders_bridging_day > 0 THEN ooooooooo.orders_bridging_day
                                   ELSE 0
                               END) AS Orders_Bridging_Day,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Terminated > 0 THEN oooo.Orders_Cancelled_Terminated
                                   ELSE 0
                               END) AS Orders_Cancelled_Terminated -- ---------------------------------------------------------------------------------------------------------------------------------------------- hours
,
                              o.hours_weekly__c AS weekly_hours,
                              oo.executed_hours AS executed_hours,
                              o.hours_weekly__c*oo.CW AS max_monthly_hours,
                              oooo.cancelled_Professional_hours,
                              oooo.cancelled_Mistake_hours,
                              oooo.cancelled_Terminated_hours,
                              ooooo.cancelled_customer_hours AS cancelled_customer_hours,
                              ooooooo.holiday_mistake_hours AS holiday_mistake_hours,
                              oooooooo.Noshow_Professional_hours,
                              (CASE
                                   WHEN o.hours_weekly__c > 0 THEN (o.grand_total__c / o.hours_weekly__c / 4.33)
                                   ELSE 0
                               END) AS PPH -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE A - executed hours = max hours
,
                              (CASE
                                   WHEN oo.executed_hours = (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS fine_A -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE B - executed hours + CANCELLED CUSTOMER = max hours
,
                              (CASE
                                   WHEN (oo.executed_hours + ooooo.cancelled_customer_hours) = (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS fine_B -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE C - first month
,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (MONTH
                                                                                                         FROM ooo.first_order_date::DATE)
                                        AND EXTRACT (YEAR
                                                     FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (YEAR
                                                                                                             FROM ooo.first_order_date::DATE) THEN 1
                                   ELSE 0
                               END) AS fine_C -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE D - executed hours < max hours AND no cancelled Orders
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled
                                                     ELSE 0
                                                 END) = 0)) THEN 1
                                   ELSE 0
                               END) AS fine_D -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE E - executed hours < max hours AND CANCELLED CUSTOMER
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS fine_E -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE G - executed hours = max hours AND Public Holiday = CANCELLED MISTAKE
,
                              (CASE
                                   WHEN ((oo.executed_hours = (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS fine_G -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE H - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE
,
                              (CASE
                                   WHEN (o.hours_weekly__c*oo.CW) = oo.executed_hours + (CASE
                                                                                             WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                             ELSE 0
                                                                                         END)
                                        AND ( ( CASE
                                                    WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                    ELSE 0
                                                END) > 0)THEN 1
                                   ELSE 0
                               END) AS fine_H -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW first month
,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (MONTH
                                                                                                         FROM ooo.first_order_date::DATE)
                                        AND EXTRACT (YEAR
                                                     FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (YEAR
                                                                                                             FROM ooo.first_order_date::DATE) THEN 1
                                   ELSE 0
                               END) AS Review_firstmonth -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW h
,
                              (CASE
                                   WHEN o.hours_weekly__c > 0 THEN 0
                                   ELSE 1
                               END) AS Review_h -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW executed hours > max hours
,
                              (CASE
                                   WHEN oo.executed_hours > (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS Review_extrabooking -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW public holidays
,
                              (CASE
                                   WHEN (CASE
                                             WHEN oooooo.orders_holiday > 0 THEN oooooo.orders_holiday
                                             ELSE 0
                                         END) > 0 THEN 1
                                   ELSE 0
                               END) AS Review_public_holidays -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW max hours > executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((o.hours_weekly__c*oo.CW) > (oo.executed_hours + (CASE
                                                                                               WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                               ELSE 0
                                                                                           END) + (CASE
                                                                                                       WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                                                                       ELSE 0
                                                                                                   END))
                                         AND (CASE
                                                  WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                  ELSE 0
                                              END) > 0) THEN 1
                                   ELSE 0
                               END) AS Review_Bridging_days_h -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - executed hours < max hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                     ELSE 0
                                                 END) > 0)
                                         AND ( ( CASE
                                                     WHEN ooooooooo.orders_bridging_day > 0 THEN ooooooooo.orders_bridging_day
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS REVIEW_Bridging_day -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((o.hours_weekly__c*oo.CW) = (oo.executed_hours + (CASE
                                                                                               WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                               ELSE 0
                                                                                           END) + (CASE
                                                                                                       WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                                                                       ELSE 0
                                                                                                   END))
                                         AND (CASE
                                                  WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                  ELSE 0
                                              END) > 0) THEN 1
                                   ELSE 0
                               END) AS Review_incl_Bridging_day_h -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------

FROM salesforce.opportunity o
INNER JOIN
    ( SELECT basic.opportunityid AS opportunityid,
             basic.first_order_date_monthly AS first_order_date_monthly,
             basic.orders AS orders,
             basic.executed_hours AS executed_hours,
             SUM (CASE
                      WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday THEN 1
                      ELSE 0
                  END) AS CW
     FROM
         ( SELECT t1.opportunityid AS opportunityid,
                  COUNT (sfid) AS orders,
                        SUM (order_duration__c) AS executed_hours,
                            MIN (effectivedate::DATE) AS first_order_date_monthly
          FROM salesforce."order" t1
          WHERE status IN ('PENDING TO START',
                           'FULFILLED',
                           'NOSHOW CUSTOMER',
                           'INVOICED')
              AND EXTRACT (MONTH
                           FROM effectivedate::DATE) = EXTRACT (MONTH
                                                                FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
              AND EXTRACT (YEAR
                           FROM effectivedate::DATE) = EXTRACT (YEAR
                                                                FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
          GROUP BY t1.opportunityid) AS basic
     LEFT JOIN bi.working_days_monthly days ON (TO_CHAR(basic.first_order_date_monthly, 'YYYY-MM') = days.year_month)
     GROUP BY basic.opportunityid,
              basic.first_order_date_monthly,
              basic.orders,
              basic.executed_hours) AS oo ON o.sfid = oo.opportunityid
INNER JOIN bi.b2borders ooo ON o.sfid = ooo.opportunity_id -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED ORDERS without CANCELLED CUSTOMER
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS Orders_cancelled,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Professional,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED MISTAKE' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Mistake,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED TERMINATED' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Terminated,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Professional_hours,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED MISTAKE' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Mistake_hours,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED TERMINATED' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Terminated_hours
     FROM salesforce."order" t2
     WHERE status LIKE '%CANCELLED%'
         AND status NOT LIKE 'CANCELLED CUSTOMER'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS oooo ON o.sfid = oooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- NOSHOW PROFESSIONAL
LEFT JOIN
    ( SELECT opportunityid,
             SUM(CASE
                     WHEN Status LIKE 'NOSHOW PROFESSIONAL' THEN 1
                     ELSE 0
                 END) AS Orders_Noshow_Professional,
             SUM(CASE
                     WHEN Status LIKE 'NOSHOW PROFESSIONAL' THEN order_duration__c
                     ELSE 0
                 END) AS Noshow_Professional_hours
     FROM salesforce."order" t22
     WHERE status LIKE 'NOSHOW PROFESSIONAL'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS oooooooo ON o.sfid = oooooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED CUSTOMER ORDERS
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS Orders_cancelled_customer,
                   SUM (order_duration__c) AS cancelled_customer_hours
     FROM salesforce."order" t3
     WHERE status LIKE 'CANCELLED CUSTOMER'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS ooooo ON o.sfid = ooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS  w/o CANCELLED MISTAKE
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_holiday,
                   SUM (order_duration__c) AS holiday_hours,
                       MIN (effectivedate::DATE) AS first_order_date_monthly
     FROM salesforce."order" t4
     WHERE (status IN ('PENDING TO START',
                       'FULFILLED',
                       'NOSHOW CUSTOMER',
                       'INVOICED')
            OR (status LIKE '%CANCELLED%'
                AND status NOT LIKE 'CANCELLED MISTAKE'))
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-01-01',
                                     '2018-05-01',
                                     '2018-05-10',
                                     '2018-05-21',
                                     '2018-10-03') -- should be replaced by an JOIN with the working day table

     GROUP BY opportunityid) AS oooooo ON o.sfid = oooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAY is CANCELLED MISTAKE
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_holiday_mistake,
                   SUM (order_duration__c) AS holiday_mistake_hours,
                       MIN (effectivedate::DATE) AS first_order_date_monthly
     FROM salesforce."order" t5
     WHERE status IN ('CANCELLED MISTAKE')
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-01-01',
                                     '2018-05-01',
                                     '2018-05-10',
                                     '2018-05-21',
                                     '2018-10-03') -- should be replaced by an JOIN with the working day table

     GROUP BY opportunityid) AS ooooooo ON o.sfid = ooooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- BRIDGING DAY is CANCELLED CUSTOMER
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_bridging_day,
                   SUM (order_duration__c) AS holiday_bridging_day
     FROM salesforce."order" t6
     WHERE status IN ('CANCELLED CUSTOMER')
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-05-11')
     GROUP BY opportunityid) AS ooooooooo ON o.sfid = ooooooooo.opportunityid -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------

WHERE o.grand_total__c IS NOT NULL
    AND o.test__c IS FALSE -- AND o.stagename IN ('WON')

GROUP BY o.sfid,
         o.customer__c,
         o.Name,
         o.StageName,
         o.status__c,
         o.supplies__c,
         o.grand_total__c,
         o.CloseDate,
         o.hours_weekly__c,
         oo.first_order_date_monthly,
         oo.orders,
         oo.executed_hours,
         oo.CW,
         ooo.first_order_date,
         oooo.cancelled_Professional_hours,
         oooo.cancelled_Mistake_hours,
         oooo.cancelled_Terminated_hours,
         oooo.orders_cancelled,
         oooo.Orders_Cancelled_Professional,
         oooo.Orders_Cancelled_Mistake,
         oooo.Orders_Cancelled_Terminated,
         ooooo.Orders_cancelled_customer,
         ooooo.cancelled_customer_hours,
         oooooo.orders_holiday,
         ooooooo.orders_holiday_mistake,
         ooooooo.holiday_mistake_hours,
         oooooooo.Orders_Noshow_Professional,
         oooooooo.Noshow_Professional_hours,
         ooooooooo.orders_bridging_day,
         ooooooooo.holiday_bridging_day ;

-- Short Description: Opportunity invoice forecast details for the invoice check done by sales
 -- Created on: 28/11/2018

DELETE
FROM bi.opportunity_invoice_check
WHERE TO_CHAR(((DATE - INTERVAL '1day')::DATE), 'YYYY-MM') = TO_CHAR(((CURRENT_DATE - INTERVAL '1day')::DATE), 'YYYY-MM');


INSERT INTO bi.opportunity_invoice_check
SELECT CURRENT_DATE AS DATE , TO_CHAR(((CURRENT_DATE - INTERVAL '1day')::DATE), 'YYYY-MM') AS year_month,
                              o.sfid AS OpportunityId,
                              o.customer__c AS customerId,
                              o.Name,
                              o.StageName,
                              o.status__c AS status,
                              o.supplies__c AS supplies,
                              o.grand_total__c,
                              o.CloseDate,
                              ooo.first_order_date,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM ooo.first_order_date::DATE) = EXTRACT (MONTH
                                                                                             FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
                                        AND (EXTRACT (YEAR
                                                      FROM ooo.first_order_date::DATE) = EXTRACT (YEAR
                                                                                                  FROM (CURRENT_DATE - INTERVAL '1day')::DATE)) THEN 1
                                   ELSE 0
                               END) AS FirstMonth,
                              oo.first_order_date_monthly -- ---------------------------------------------------------------------------------------------------------------------------------------------- orders
,
                              oo.orders AS orders,
                              (CASE
                                   WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled
                                   ELSE 0
                               END) AS Orders_Cancelled,
                              (CASE
                                   WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer
                                   ELSE 0
                               END) AS Orders_Cancelled_Customer,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Professional > 0 THEN oooo.Orders_Cancelled_Professional
                                   ELSE 0
                               END) AS Orders_Cancelled_Professional,
                              (CASE
                                   WHEN oooooooo.Orders_Noshow_Professional > 0 THEN oooooooo.Orders_Noshow_Professional
                                   ELSE 0
                               END) AS Orders_Noshow_Professional,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Mistake > 0 THEN oooo.Orders_Cancelled_Mistake
                                   ELSE 0
                               END) AS Orders_Cancelled_Mistake,
                              (CASE
                                   WHEN ooooooooo.orders_bridging_day > 0 THEN ooooooooo.orders_bridging_day
                                   ELSE 0
                               END) AS Orders_Bridging_Day,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Terminated > 0 THEN oooo.Orders_Cancelled_Terminated
                                   ELSE 0
                               END) AS Orders_Cancelled_Terminated -- ---------------------------------------------------------------------------------------------------------------------------------------------- hours
,
                              o.hours_weekly__c AS weekly_hours,
                              oo.executed_hours AS executed_hours,
                              o.hours_weekly__c*oo.CW AS max_monthly_hours,
                              oooo.cancelled_Professional_hours,
                              oooo.cancelled_Mistake_hours,
                              oooo.cancelled_Terminated_hours,
                              ooooo.cancelled_customer_hours AS cancelled_customer_hours,
                              ooooooo.holiday_mistake_hours AS holiday_mistake_hours,
                              oooooooo.Noshow_Professional_hours,
                              (CASE
                                   WHEN o.hours_weekly__c > 0 THEN (o.grand_total__c / o.hours_weekly__c / 4.33)
                                   ELSE 0
                               END) AS PPH -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE A - executed hours = max hours
,
                              (CASE
                                   WHEN oo.executed_hours = (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS fine_A -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE B - executed hours + CANCELLED CUSTOMER = max hours
,
                              (CASE
                                   WHEN (oo.executed_hours + ooooo.cancelled_customer_hours) = (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS fine_B -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE C - first month
,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (MONTH
                                                                                                         FROM ooo.first_order_date::DATE)
                                        AND EXTRACT (YEAR
                                                     FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (YEAR
                                                                                                             FROM ooo.first_order_date::DATE) THEN 1
                                   ELSE 0
                               END) AS fine_C -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE D - executed hours < max hours AND no cancelled Orders
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled
                                                     ELSE 0
                                                 END) = 0)) THEN 1
                                   ELSE 0
                               END) AS fine_D -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE E - executed hours < max hours AND CANCELLED CUSTOMER
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS fine_E -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE G - executed hours = max hours AND Public Holiday = CANCELLED MISTAKE
,
                              (CASE
                                   WHEN ((oo.executed_hours = (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS fine_G -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE H - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE
,
                              (CASE
                                   WHEN (o.hours_weekly__c*oo.CW) = oo.executed_hours + (CASE
                                                                                             WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                             ELSE 0
                                                                                         END)
                                        AND ( ( CASE
                                                    WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                    ELSE 0
                                                END) > 0)THEN 1
                                   ELSE 0
                               END) AS fine_H -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW first month
,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (MONTH
                                                                                                         FROM ooo.first_order_date::DATE)
                                        AND EXTRACT (YEAR
                                                     FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (YEAR
                                                                                                             FROM ooo.first_order_date::DATE) THEN 1
                                   ELSE 0
                               END) AS Review_firstmonth -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW h
,
                              (CASE
                                   WHEN o.hours_weekly__c > 0 THEN 0
                                   ELSE 1
                               END) AS Review_h -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW executed hours > max hours
,
                              (CASE
                                   WHEN oo.executed_hours > (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS Review_extrabooking -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW public holidays
,
                              (CASE
                                   WHEN (CASE
                                             WHEN oooooo.orders_holiday > 0 THEN oooooo.orders_holiday
                                             ELSE 0
                                         END) > 0 THEN 1
                                   ELSE 0
                               END) AS Review_public_holidays -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW max hours > executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((o.hours_weekly__c*oo.CW) > (oo.executed_hours + (CASE
                                                                                               WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                               ELSE 0
                                                                                           END) + (CASE
                                                                                                       WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                                                                       ELSE 0
                                                                                                   END))
                                         AND (CASE
                                                  WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                  ELSE 0
                                              END) > 0) THEN 1
                                   ELSE 0
                               END) AS Review_Bridging_days_h -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - executed hours < max hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                     ELSE 0
                                                 END) > 0)
                                         AND ( ( CASE
                                                     WHEN ooooooooo.orders_bridging_day > 0 THEN ooooooooo.orders_bridging_day
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS REVIEW_Bridging_day -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((o.hours_weekly__c*oo.CW) = (oo.executed_hours + (CASE
                                                                                               WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                               ELSE 0
                                                                                           END) + (CASE
                                                                                                       WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                                                                       ELSE 0
                                                                                                   END))
                                         AND (CASE
                                                  WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                  ELSE 0
                                              END) > 0) THEN 1
                                   ELSE 0
                               END) AS Review_incl_Bridging_day_h -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------

FROM salesforce.opportunity o
INNER JOIN
    ( SELECT basic.opportunityid AS opportunityid,
             basic.first_order_date_monthly AS first_order_date_monthly,
             basic.orders AS orders,
             basic.executed_hours AS executed_hours,
             SUM (CASE
                      WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday THEN 1
                      ELSE 0
                  END) AS CW
     FROM
         ( SELECT t1.opportunityid AS opportunityid,
                  COUNT (sfid) AS orders,
                        SUM (order_duration__c) AS executed_hours,
                            MIN (effectivedate::DATE) AS first_order_date_monthly
          FROM salesforce."order" t1
          WHERE status IN ('PENDING TO START',
                           'FULFILLED',
                           'NOSHOW CUSTOMER',
                           'INVOICED')
              AND EXTRACT (MONTH
                           FROM effectivedate::DATE) = EXTRACT (MONTH
                                                                FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
              AND EXTRACT (YEAR
                           FROM effectivedate::DATE) = EXTRACT (YEAR
                                                                FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
          GROUP BY t1.opportunityid) AS basic
     LEFT JOIN bi.working_days_monthly days ON (TO_CHAR(basic.first_order_date_monthly, 'YYYY-MM') = days.year_month)
     GROUP BY basic.opportunityid,
              basic.first_order_date_monthly,
              basic.orders,
              basic.executed_hours) AS oo ON o.sfid = oo.opportunityid
INNER JOIN bi.b2borders ooo ON o.sfid = ooo.opportunity_id -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED ORDERS without CANCELLED CUSTOMER
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS Orders_cancelled,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Professional,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED MISTAKE' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Mistake,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED TERMINATED' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Terminated,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Professional_hours,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED MISTAKE' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Mistake_hours,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED TERMINATED' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Terminated_hours
     FROM salesforce."order" t2
     WHERE status LIKE '%CANCELLED%'
         AND status NOT LIKE 'CANCELLED CUSTOMER'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS oooo ON o.sfid = oooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- NOSHOW PROFESSIONAL
LEFT JOIN
    ( SELECT opportunityid,
             SUM(CASE
                     WHEN Status LIKE 'NOSHOW PROFESSIONAL' THEN 1
                     ELSE 0
                 END) AS Orders_Noshow_Professional,
             SUM(CASE
                     WHEN Status LIKE 'NOSHOW PROFESSIONAL' THEN order_duration__c
                     ELSE 0
                 END) AS Noshow_Professional_hours
     FROM salesforce."order" t22
     WHERE status LIKE 'NOSHOW PROFESSIONAL'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS oooooooo ON o.sfid = oooooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED CUSTOMER ORDERS
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS Orders_cancelled_customer,
                   SUM (order_duration__c) AS cancelled_customer_hours
     FROM salesforce."order" t3
     WHERE status LIKE 'CANCELLED CUSTOMER'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS ooooo ON o.sfid = ooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS  w/o CANCELLED MISTAKE
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_holiday,
                   SUM (order_duration__c) AS holiday_hours,
                       MIN (effectivedate::DATE) AS first_order_date_monthly
     FROM salesforce."order" t4
     WHERE (status IN ('PENDING TO START',
                       'FULFILLED',
                       'NOSHOW CUSTOMER',
                       'INVOICED')
            OR (status LIKE '%CANCELLED%'
                AND status NOT LIKE 'CANCELLED MISTAKE'))
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-01-01',
                                     '2018-05-01',
                                     '2018-05-10',
                                     '2018-05-21',
                                     '2018-10-03') -- should be replaced by an JOIN with the working day table

     GROUP BY opportunityid) AS oooooo ON o.sfid = oooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAY is CANCELLED MISTAKE
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_holiday_mistake,
                   SUM (order_duration__c) AS holiday_mistake_hours,
                       MIN (effectivedate::DATE) AS first_order_date_monthly
     FROM salesforce."order" t5
     WHERE status IN ('CANCELLED MISTAKE')
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-01-01',
                                     '2018-05-01',
                                     '2018-05-10',
                                     '2018-05-21',
                                     '2018-10-03') -- should be replaced by an JOIN with the working day table

     GROUP BY opportunityid) AS ooooooo ON o.sfid = ooooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- BRIDGING DAY is CANCELLED CUSTOMER
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_bridging_day,
                   SUM (order_duration__c) AS holiday_bridging_day
     FROM salesforce."order" t6
     WHERE status IN ('CANCELLED CUSTOMER')
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-05-11')
     GROUP BY opportunityid) AS ooooooooo ON o.sfid = ooooooooo.opportunityid -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------

WHERE o.grand_total__c IS NOT NULL
    AND o.test__c IS FALSE -- AND o.stagename IN ('WON')

GROUP BY o.sfid,
         o.customer__c,
         o.Name,
         o.StageName,
         o.status__c,
         o.supplies__c,
         o.grand_total__c,
         o.CloseDate,
         o.hours_weekly__c,
         oo.first_order_date_monthly,
         oo.orders,
         oo.executed_hours,
         oo.CW,
         ooo.first_order_date,
         oooo.cancelled_Professional_hours,
         oooo.cancelled_Mistake_hours,
         oooo.cancelled_Terminated_hours,
         oooo.orders_cancelled,
         oooo.Orders_Cancelled_Professional,
         oooo.Orders_Cancelled_Mistake,
         oooo.Orders_Cancelled_Terminated,
         ooooo.Orders_cancelled_customer,
         ooooo.cancelled_customer_hours,
         oooooo.orders_holiday,
         ooooooo.orders_holiday_mistake,
         ooooooo.holiday_mistake_hours,
         oooooooo.Orders_Noshow_Professional,
         oooooooo.Noshow_Professional_hours,
         ooooooooo.orders_bridging_day,
         ooooooooo.holiday_bridging_day ;

-- Short Description: Opportunity invoice forecast details for the invoice check done by sales
 -- Created on: 28/11/2018

DELETE
FROM bi.opportunity_invoice_check
WHERE TO_CHAR(((DATE - INTERVAL '1day')::DATE), 'YYYY-MM') = TO_CHAR(((CURRENT_DATE - INTERVAL '1day')::DATE), 'YYYY-MM');


INSERT INTO bi.opportunity_invoice_check
SELECT CURRENT_DATE AS DATE , TO_CHAR(((CURRENT_DATE - INTERVAL '1day')::DATE), 'YYYY-MM') AS year_month,
                              o.sfid AS OpportunityId,
                              o.customer__c AS customerId,
                              o.Name,
                              o.StageName,
                              o.status__c AS status,
                              o.supplies__c AS supplies,
                              o.grand_total__c,
                              o.CloseDate,
                              ooo.first_order_date,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM ooo.first_order_date::DATE) = EXTRACT (MONTH
                                                                                             FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
                                        AND (EXTRACT (YEAR
                                                      FROM ooo.first_order_date::DATE) = EXTRACT (YEAR
                                                                                                  FROM (CURRENT_DATE - INTERVAL '1day')::DATE)) THEN 1
                                   ELSE 0
                               END) AS FirstMonth,
                              oo.first_order_date_monthly -- ---------------------------------------------------------------------------------------------------------------------------------------------- orders
,
                              oo.orders AS orders,
                              (CASE
                                   WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled
                                   ELSE 0
                               END) AS Orders_Cancelled,
                              (CASE
                                   WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer
                                   ELSE 0
                               END) AS Orders_Cancelled_Customer,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Professional > 0 THEN oooo.Orders_Cancelled_Professional
                                   ELSE 0
                               END) AS Orders_Cancelled_Professional,
                              (CASE
                                   WHEN oooooooo.Orders_Noshow_Professional > 0 THEN oooooooo.Orders_Noshow_Professional
                                   ELSE 0
                               END) AS Orders_Noshow_Professional,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Mistake > 0 THEN oooo.Orders_Cancelled_Mistake
                                   ELSE 0
                               END) AS Orders_Cancelled_Mistake,
                              (CASE
                                   WHEN ooooooooo.orders_bridging_day > 0 THEN ooooooooo.orders_bridging_day
                                   ELSE 0
                               END) AS Orders_Bridging_Day,
                              (CASE
                                   WHEN oooo.Orders_Cancelled_Terminated > 0 THEN oooo.Orders_Cancelled_Terminated
                                   ELSE 0
                               END) AS Orders_Cancelled_Terminated -- ---------------------------------------------------------------------------------------------------------------------------------------------- hours
,
                              o.hours_weekly__c AS weekly_hours,
                              oo.executed_hours AS executed_hours,
                              o.hours_weekly__c*oo.CW AS max_monthly_hours,
                              oooo.cancelled_Professional_hours,
                              oooo.cancelled_Mistake_hours,
                              oooo.cancelled_Terminated_hours,
                              ooooo.cancelled_customer_hours AS cancelled_customer_hours,
                              ooooooo.holiday_mistake_hours AS holiday_mistake_hours,
                              oooooooo.Noshow_Professional_hours,
                              (CASE
                                   WHEN o.hours_weekly__c > 0 THEN (o.grand_total__c / o.hours_weekly__c / 4.33)
                                   ELSE 0
                               END) AS PPH -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE A - executed hours = max hours
,
                              (CASE
                                   WHEN oo.executed_hours = (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS fine_A -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE B - executed hours + CANCELLED CUSTOMER = max hours
,
                              (CASE
                                   WHEN (oo.executed_hours + ooooo.cancelled_customer_hours) = (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS fine_B -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE C - first month
,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (MONTH
                                                                                                         FROM ooo.first_order_date::DATE)
                                        AND EXTRACT (YEAR
                                                     FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (YEAR
                                                                                                             FROM ooo.first_order_date::DATE) THEN 1
                                   ELSE 0
                               END) AS fine_C -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE D - executed hours < max hours AND no cancelled Orders
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN oooo.orders_cancelled > 0 THEN oooo.orders_cancelled
                                                     ELSE 0
                                                 END) = 0)) THEN 1
                                   ELSE 0
                               END) AS fine_D -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE E - executed hours < max hours AND CANCELLED CUSTOMER
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS fine_E -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE G - executed hours = max hours AND Public Holiday = CANCELLED MISTAKE
,
                              (CASE
                                   WHEN ((oo.executed_hours = (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS fine_G -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE H - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE
,
                              (CASE
                                   WHEN (o.hours_weekly__c*oo.CW) = oo.executed_hours + (CASE
                                                                                             WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                             ELSE 0
                                                                                         END)
                                        AND ( ( CASE
                                                    WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                    ELSE 0
                                                END) > 0)THEN 1
                                   ELSE 0
                               END) AS fine_H -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW first month
,
                              (CASE
                                   WHEN EXTRACT (MONTH
                                                 FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (MONTH
                                                                                                         FROM ooo.first_order_date::DATE)
                                        AND EXTRACT (YEAR
                                                     FROM (CURRENT_DATE - INTERVAL '1day')::DATE) = EXTRACT (YEAR
                                                                                                             FROM ooo.first_order_date::DATE) THEN 1
                                   ELSE 0
                               END) AS Review_firstmonth -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW h
,
                              (CASE
                                   WHEN o.hours_weekly__c > 0 THEN 0
                                   ELSE 1
                               END) AS Review_h -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW executed hours > max hours
,
                              (CASE
                                   WHEN oo.executed_hours > (o.hours_weekly__c*oo.CW) THEN 1
                                   ELSE 0
                               END) AS Review_extrabooking -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW public holidays
,
                              (CASE
                                   WHEN (CASE
                                             WHEN oooooo.orders_holiday > 0 THEN oooooo.orders_holiday
                                             ELSE 0
                                         END) > 0 THEN 1
                                   ELSE 0
                               END) AS Review_public_holidays -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW max hours > executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((o.hours_weekly__c*oo.CW) > (oo.executed_hours + (CASE
                                                                                               WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                               ELSE 0
                                                                                           END) + (CASE
                                                                                                       WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                                                                       ELSE 0
                                                                                                   END))
                                         AND (CASE
                                                  WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                  ELSE 0
                                              END) > 0) THEN 1
                                   ELSE 0
                               END) AS Review_Bridging_days_h -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - executed hours < max hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((oo.executed_hours < (o.hours_weekly__c*oo.CW))
                                         AND ( ( CASE
                                                     WHEN ooooooo.orders_holiday_mistake > 0 THEN ooooooo.orders_holiday_mistake
                                                     ELSE 0
                                                 END) > 0)
                                         AND ( ( CASE
                                                     WHEN ooooooooo.orders_bridging_day > 0 THEN ooooooooo.orders_bridging_day
                                                     ELSE 0
                                                 END) > 0)) THEN 1
                                   ELSE 0
                               END) AS REVIEW_Bridging_day -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day
,
                              (CASE
                                   WHEN ((o.hours_weekly__c*oo.CW) = (oo.executed_hours + (CASE
                                                                                               WHEN ooooooo.holiday_mistake_hours > 0 THEN ooooooo.holiday_mistake_hours
                                                                                               ELSE 0
                                                                                           END) + (CASE
                                                                                                       WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                                                                       ELSE 0
                                                                                                   END))
                                         AND (CASE
                                                  WHEN ooooooooo.holiday_bridging_day > 0 THEN ooooooooo.holiday_bridging_day
                                                  ELSE 0
                                              END) > 0) THEN 1
                                   ELSE 0
                               END) AS Review_incl_Bridging_day_h -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------

FROM salesforce.opportunity o
INNER JOIN
    ( SELECT basic.opportunityid AS opportunityid,
             basic.first_order_date_monthly AS first_order_date_monthly,
             basic.orders AS orders,
             basic.executed_hours AS executed_hours,
             SUM (CASE
                      WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday THEN 1
                      ELSE 0
                  END) AS CW
     FROM
         ( SELECT t1.opportunityid AS opportunityid,
                  COUNT (sfid) AS orders,
                        SUM (order_duration__c) AS executed_hours,
                            MIN (effectivedate::DATE) AS first_order_date_monthly
          FROM salesforce."order" t1
          WHERE status IN ('PENDING TO START',
                           'FULFILLED',
                           'NOSHOW CUSTOMER',
                           'INVOICED')
              AND EXTRACT (MONTH
                           FROM effectivedate::DATE) = EXTRACT (MONTH
                                                                FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
              AND EXTRACT (YEAR
                           FROM effectivedate::DATE) = EXTRACT (YEAR
                                                                FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
          GROUP BY t1.opportunityid) AS basic
     LEFT JOIN bi.working_days_monthly days ON (TO_CHAR(basic.first_order_date_monthly, 'YYYY-MM') = days.year_month)
     GROUP BY basic.opportunityid,
              basic.first_order_date_monthly,
              basic.orders,
              basic.executed_hours) AS oo ON o.sfid = oo.opportunityid
INNER JOIN bi.b2borders ooo ON o.sfid = ooo.opportunity_id -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED ORDERS without CANCELLED CUSTOMER
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS Orders_cancelled,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Professional,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED MISTAKE' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Mistake,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED TERMINATED' THEN 1
                           ELSE 0
                       END) AS Orders_Cancelled_Terminated,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED PROFESSIONAL' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Professional_hours,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED MISTAKE' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Mistake_hours,
                   SUM(CASE
                           WHEN Status LIKE 'CANCELLED TERMINATED' THEN order_duration__c
                           ELSE 0
                       END) AS Cancelled_Terminated_hours
     FROM salesforce."order" t2
     WHERE status LIKE '%CANCELLED%'
         AND status NOT LIKE 'CANCELLED CUSTOMER'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS oooo ON o.sfid = oooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- NOSHOW PROFESSIONAL
LEFT JOIN
    ( SELECT opportunityid,
             SUM(CASE
                     WHEN Status LIKE 'NOSHOW PROFESSIONAL' THEN 1
                     ELSE 0
                 END) AS Orders_Noshow_Professional,
             SUM(CASE
                     WHEN Status LIKE 'NOSHOW PROFESSIONAL' THEN order_duration__c
                     ELSE 0
                 END) AS Noshow_Professional_hours
     FROM salesforce."order" t22
     WHERE status LIKE 'NOSHOW PROFESSIONAL'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS oooooooo ON o.sfid = oooooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED CUSTOMER ORDERS
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS Orders_cancelled_customer,
                   SUM (order_duration__c) AS cancelled_customer_hours
     FROM salesforce."order" t3
     WHERE status LIKE 'CANCELLED CUSTOMER'
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
     GROUP BY opportunityid) AS ooooo ON o.sfid = ooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS  w/o CANCELLED MISTAKE
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_holiday,
                   SUM (order_duration__c) AS holiday_hours,
                       MIN (effectivedate::DATE) AS first_order_date_monthly
     FROM salesforce."order" t4
     WHERE (status IN ('PENDING TO START',
                       'FULFILLED',
                       'NOSHOW CUSTOMER',
                       'INVOICED')
            OR (status LIKE '%CANCELLED%'
                AND status NOT LIKE 'CANCELLED MISTAKE'))
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-01-01',
                                     '2018-05-01',
                                     '2018-05-10',
                                     '2018-05-21',
                                     '2018-10-03') -- should be replaced by an JOIN with the working day table

     GROUP BY opportunityid) AS oooooo ON o.sfid = oooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAY is CANCELLED MISTAKE
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_holiday_mistake,
                   SUM (order_duration__c) AS holiday_mistake_hours,
                       MIN (effectivedate::DATE) AS first_order_date_monthly
     FROM salesforce."order" t5
     WHERE status IN ('CANCELLED MISTAKE')
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (MONTH
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-01-01',
                                     '2018-05-01',
                                     '2018-05-10',
                                     '2018-05-21',
                                     '2018-10-03') -- should be replaced by an JOIN with the working day table

     GROUP BY opportunityid) AS ooooooo ON o.sfid = ooooooo.opportunityid -- ---------------------------------------------------------------------------------------------------------------------------------------------- BRIDGING DAY is CANCELLED CUSTOMER
LEFT JOIN
    ( SELECT opportunityid,
             COUNT (sfid) AS orders_bridging_day,
                   SUM (order_duration__c) AS holiday_bridging_day
     FROM salesforce."order" t6
     WHERE status IN ('CANCELLED CUSTOMER')
         AND EXTRACT (MONTH
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND EXTRACT (YEAR
                      FROM effectivedate::DATE) = EXTRACT (YEAR
                                                           FROM (CURRENT_DATE - INTERVAL '1day')::DATE)
         AND effectivedate::DATE IN ('2018-05-11')
     GROUP BY opportunityid) AS ooooooooo ON o.sfid = ooooooooo.opportunityid -- ----------------------------------------------------------------------------------------------------------------------------------------------
 -- ----------------------------------------------------------------------------------------------------------------------------------------------

WHERE o.grand_total__c IS NOT NULL
    AND o.test__c IS FALSE -- AND o.stagename IN ('WON')

GROUP BY o.sfid,
         o.customer__c,
         o.Name,
         o.StageName,
         o.status__c,
         o.supplies__c,
         o.grand_total__c,
         o.CloseDate,
         o.hours_weekly__c,
         oo.first_order_date_monthly,
         oo.orders,
         oo.executed_hours,
         oo.CW,
         ooo.first_order_date,
         oooo.cancelled_Professional_hours,
         oooo.cancelled_Mistake_hours,
         oooo.cancelled_Terminated_hours,
         oooo.orders_cancelled,
         oooo.Orders_Cancelled_Professional,
         oooo.Orders_Cancelled_Mistake,
         oooo.Orders_Cancelled_Terminated,
         ooooo.Orders_cancelled_customer,
         ooooo.cancelled_customer_hours,
         oooooo.orders_holiday,
         ooooooo.orders_holiday_mistake,
         ooooooo.holiday_mistake_hours,
         oooooooo.Orders_Noshow_Professional,
         oooooooo.Noshow_Professional_hours,
         ooooooooo.orders_bridging_day,
         ooooooooo.holiday_bridging_day ;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'