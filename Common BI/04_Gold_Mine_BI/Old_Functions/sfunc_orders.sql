

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_orders(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_orders';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP VIEW IF EXISTS bi.orders;
CREATE VIEW bi.orders as 
SELECT 
        t.order_start__c::date as effectivedate,
        t.origin_date__c::date as origin_date__c,
        t.id,
        t.ip__c,
        t.customer_email__c,
        -- t.customer_phone__c,
        t.web_grand_total__c,
        --t.billingcountry, -- XXXX
        t.grand_total_eur__c,
        t.acquisition_new_customer__c,
        --t.createddate, -- XXXX
        t.order_creation__c,
        t.type,
        --t.in_test_period__c, -- XXXX
        t.shippingcity,
        t.acquisition_channel__c,
        --t.description, -- XXXX
        t.referred_by__c,
        t.recurrency__c,
        t.voucher__c,
        CASE WHEN LEFT(t.locale__c,2) = 'ch' 
            THEN 
                (t.discount__c * 0.92) 
            ELSE
                (CASE WHEN t.voucher__c in ('FBBOX5','FBMLBX5','GDMLBX5') THEN 20 ELSE t.discount__c END)
            END 
        as discount__c,
        t.professional__c,
        --t.accountid, -- XXXX
        t.order_start__c,
        t.order_end__c,
        t.order_time__c,
        --t.ip__c, -- XXXX
        t.shippingpostalcode,
        t.acquisition_tracking_id__c,
        t.shippingstreet,
        t.rating_service__c,
        t.shippinglongitude,
        t.shippingcountry,
        t.lastmodifieddate,
        t.acquisition_customer_creation__c,
        t.customer_name__c,
        t.shippinglatitude,
        t.test__c,
        t.billingcity,
        t.gmv__c,
        CASE WHEN t.status = 'FULFILLED' AND t.invoiced__c IS NOT NULL THEN 'INVOICED'::text ELSE t.status::text END as status,
        t.acquisition_channel_params__c,
        --t.createdbyid, -- XXXX
        t.rating_professional__c,
        t.cancellation_reason__c,
        t.acquisition_channel_ref__c,
        t.order_duration__c,
        t.sfid,
        --t.user_agent__c, -- XXXX
        --t.billingpostalcode, -- XXXX
        t.pph__c,
        t.contact__c,
        t.order_id__c,
        t.locale__c,
        t.allocation_attempts__c,
        t.allocation_attempts_last__c,
        t.commuting_time__c,
        t.contact__c as customer_id__c,
        t.opportunityid,
        t.distance__c,
    CASE
        WHEN type::text is NULL or type != 'cleaning-b2b'
        THEN 1
        ELSE 2
        END AS order_type,



    CASE WHEN (t.voucher__c <> '' AND t.voucher__c <> '0' AND t.voucher__c IS NOT NULL AND t.voucher__c IN ('NEWTIGER20','NEWTIGER50','TIGERLOVE','TIGERHELP5','TIGERLOVE10','TIGERHELP10','GUT20','GUT50')) THEN

        (CASE WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
            THEN 'SEM Brand'::text
            

            WHEN (t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
            THEN 'SEM'::text
            

            WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
                AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
            THEN 'Display'::text
                        

            WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
            THEN 'Youtube Paid'::text
            

            WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
                AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
            THEN 'SEO'::text
            

            WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
                AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
                THEN 'SEO Brand'::text
                

            WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
                    OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
            THEN 'Facebook'::text


            WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
                    OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text 
            THEN 'Newsletter'::text
            

            WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
                    AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
                    AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
                    AND t.acquisition_channel_params__c::text <> ''::text)
            THEN 'DTI'::text

            /*WHEN t.acquisition_channel_params__c::text ~~ '%coop%'
                OR t.acquisition_channel_params__c::text ~~ '%afnt%'
                OR t.acquisition_channel_params__c::text ~~ '%putzchecker%'
            THEN 'Affiliate/Coops'*/
            
            ELSE 'Newsletter'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

            END)

    ELSE
            
        (CASE
            WHEN t.voucher__c <> ''
                AND t.voucher__c <> '0'
                AND t.voucher__c IS NOT NULL
                AND t.voucher__c::text in ('FRUEHLING20','FRUEHJAHR20','SAUBER20','GLANZ20','PUTZ20','STEUER20','HAUS20','TECH20','RECHT20','ELTERN20','HAPPY20','BER20','BOOTE20','CT20','BOGLAR20','BOGLFZ20','SAUBER15','PROPER20','FDFLY1J','FDFLY2J','FDFLY3J','FDFLY4J','FDFLY5J','FDFLY6J','FDFLY7J','FDFLY8J','FDFLY9J','FDFLYAJ','FDFLYBJ','FDFLYCJ','FDFLYDJ','FDFLYEJ','FDFLYFJ','FDFLYGJ','FDFLYHJ','FDFLYKJ','FDFLYLJ','PRC1X','PRC2X','PRC3X','PRC4X','PRC5X','PRC6X','PRC7X','PRC8X','PRC9X','PRC1Y','PRC2Y','PRC3Y','PRC4Y','PRC5Y','PRC6Y','PRC7Y','PRC8Y','PRC9Y','PRC1Z','PRC2Z','PRC3Z','PRC4Z','PRC5Z','PRC6Z','PRC7Z','PRC8Z','PRC9Z','SEE35','MAG35','LUZERN35','ZÜRICHSAUBER','STGALLEN35','BERN35','BASEL35','GENEVA35','LAUSANNE35','TRAM35','BATFR75','TIGER75','ZÜRICH35','20MIN50','FUW50','FEM50','TUTTI50','PUTZ35','TAG50','TRIBUNE50','20MIN70','RAB50','PUTZ40','BLATT50','WINTI35','TUTTI35','FRIDAY50','ENCORE50','BILAN50','BILAN70','ENCORE70','TRIBUNE70','FRIDAY70','TAG70','BLATT70','FEM70','ANIBIS50','EBAY50','SCHOON1H','PEPER20','WEWORK20','LZYCMP2AH','VRIEND20','OOSTER20','MODELLISTID20','HUIS20','HE20','PLLEK20','YACHT20','HAYS20','APPLIFIED20','RBI20','AUTO20','UVA20','PAPABUBBLE20','SPARK20','WORKON20','TONY20','TONYTIGER20','SYMPHONY20','FRIS20','AMS15','AMS20','MOVE20','JESSICA1H','DENHAAG1H','DENHAAG15','LAUNCH20','FDFLY1M','FDFLY2M','FDFLY3M','FDFLY4M','FDFLY5M','FDFLY6M','FDFLY7M','FDFLY8M','FDFLY9M','PAKETSEM','PAKETFB','PAKETBR','PRCNL1A','PRCNL1B','PRCNL1C','PRCNL1D','PRCNL1E','PRCNL1F','PRCNL1G','PRCNL1H','PRCNL1J','PRCNL1K','FDFLY7J','FDFLY8J','FDFLY9J','OPTFLY20','GESCHENK20','POSTFLY15','PACK10','TRYNGO50','IMMO50','TIGER30','DH20','KIDS20','LEMATIN50','KLM20','LADIES50','BERLIN20','DEFLY20','DELDRIV11','DELDRIV12','DELDRIV13','DELDRIV14','DELDRIV15','DELDRIV16','DELDRIV17','DELDRIV18','DELDRIV19','DELDRIV21','DELDRIV22','DELDRIV23','DELDRIV24','DELDRIV25','DELDRIV26','DELDRIV27','DELDRIV28','CLHAN20','DRHAN20','DRHAG20','PSTCRD15','PRCFLY20','RAD20','BERJUL20','HAMJUL20','STUJUL20','DUSJUL20','DAMJUL20','HANJUL20','LEIJUL20','BERAUG20','HAMAUG20','STUAUG20','DUSAUG20','DAMAUG20','HANAUG20','LEIAUG20','PSTFLY15','SEPT50','BIEL35','USTER35','ZUG35','SCHMINK15','PSTFLY10','SAUBER10','MEN40','MEN100X','CAR20','BINGO20','GETATIGER20','PRIJS3H','PRCFLY21','WINGS20','OPTIO10JRX','OPTIO10TDB','OPTIO10HKX','OPTIO10VTD','OPTIO10WXW','OPTIO10DRV','OPTIO10QJM','OPTIO10SWJ','OPTIO10RCW','OPTIO10BNH','OPTIO10RGZ','OPTIO10KJX','OPTIO10KDF','OPTIO10ZKL','OPTIO10DPJ','OPTIO10SMZ','OPTIO10HNT','OPTIO10DHV','OPTIO10QBS','OPTIO10PDQ','OPTIO10VPW','OPTIO10MVW','OPTIO10DHB','OPTIO10ZXN','OPTIO10SZV','OPTIO10TVM','OPTIO10TMR','OPTIO10LJX','OPTIO10RXJ','OPTIO10NMB','OPTIO10HXH','OPTIO10RTT','OPTIO10TWL','OPTIO10WSS','OPTIO10ZBH','OPTIO10DJF','OPTIO10HPF','OPTIO10VZM','OPTIO10BLK','OPTIO10LPS','OPTIO10QWS','OPTIO10GXM','OPTIO10GRG','OPTIO10KZZ','OPTIO10SXK','OPTIO10GLQ','OPTIO10GZS','OPTIO10SDG','OPTIO10PWH','OPTIO10GQH','OPTIO10LTG','OPTIO10ZSP','OPTIO10CGJ','OPTIO10SKB','OPTIO10JQL','OPTIO10MHV','OPTIO10NNK','OPTIO10SZR','OPTIO10KBC','OPTIO10ZNV','OPTIO10PJK','OPTIO10HLT','OPTIO10ZQR','OPTIO10LJP','OPTIO10MTV','OPTIO10BZW','OPTIO10XCV','OPTIO10ZWC','OPTIO10PNC','OPTIO10WNP','OPTIO10KPB','OPTIO10LHG','OPTIO10DVQ','OPTIO10VCJ','OPTIO10GJR','OPTIO10JKD','OPTIO10BMV','OPTIO10JTS','OPTIO10SMQ','OPTIO10ZWS','OPTIO10HWL','OPTIO10WBF','OPTIO10GQS','OPTIO10MVL','OPTIO10HHF','OPTIO10VQQ','OPTIO10GRS','OPTIO10GHM','OPTIO10JKH','OPTIO10ZHR','OPTIO10CCJ','OPTIO10HVB','OPTIO10LSH','OPTIO10KCC','OPTIO10JDQ','OPTIO10TRP','OPTIO10HQP','OPTIO10QJW','OPTIO10TVR','OPTIO10SNK','OPTIO20FJN','OPTIO20VJL','OPTIO20MTG','OPTIO20WLG','OPTIO20JPN','OPTIO20XHD','OPTIO20DNL','OPTIO20PMW','OPTIO20RFT','OPTIO20LFD','OPTIO20GFL','OPTIO20ZXC','OPTIO20MXF','OPTIO20XWJ','OPTIO20CDL','OPTIO20PKX','OPTIO20JSZ','OPTIO20TDJ','OPTIO20RWK','OPTIO20DGT','OPTIO20ZXP','OPTIO20GJV','OPTIO20DLH','OPTIO20KRQ','OPTIO20PKW','OPTIO20VFB','OPTIO20MCQ','OPTIO20JVF','OPTIO20HFH','OPTIO20KCF','OPTIO20FGJ','OPTIO20JFN','OPTIO20PLJ','OPTIO20PSL','OPTIO20DRJ','OPTIO20JBR','OPTIO20LLH','OPTIO20TDX','OPTIO20ZMR','OPTIO20DMG','OPTIO20LTF','OPTIO20BSC','OPTIO20BPK','OPTIO20PPB','OPTIO20WNG','OPTIO20RQK','OPTIO20VFN','OPTIO20FLQ','OPTIO20RNP','OPTIO20WVC','OPTIO20ZGX','OPTIO20QBC','OPTIO20SKF','OPTIO20WDC','OPTIO20NVJ','OPTIO20DXM','OPTIO20QFV','OPTIO20CVC','OPTIO20ZRJ','OPTIO20QQJ','OPTIO20HQX','OPTIO20LCP','OPTIO20MMX','OPTIO20WCL','OPTIO20TJX','OPTIO20RZW','OPTIO20RMS','OPTIO20WGT','OPTIO20GWT','OPTIO20VCK','OPTIO20SNS','OPTIO20ZRX','OPTIO20TCG','OPTIO20JGP','OPTIO20HLS','OPTIO20GTS','OPTIO20FGT','OPTIO20VWS','OPTIO20LWK','OPTIO20SWZ','OPTIO20THT','OPTIO20HFV','OPTIO20RTM','OPTIO20QMJ','OPTIO20BMH','OPTIO20SJN','OPTIO20XSR','OPTIO20LHN','OPTIO20MRC','OPTIO20ZBM','OPTIO20MNK','OPTIO20PQC','OPTIO20SSW','OPTIO20FWQ','OPTIO20BKN','OPTIO20HGF','OPTIO20QJS','OPTIO20CFQ','OPTIO20CJF','OPTIO20LKS','OPTIO40MGF','OPTIO40FSZ','OPTIO40XSH','OPTIO40BZS','OPTIO40JRX','OPTIO40WJW','OPTIO40MBK','OPTIO40HNF','OPTIO40NVM','OPTIO40SJN','OPTIO40DQZ','OPTIO40BMR','OPTIO40SCN','OPTIO40JZG','OPTIO40CRJ','OPTIO40KXG','OPTIO40CBQ','OPTIO40NWG','OPTIO40VVR','OPTIO40TSX','OPTIO40DMB','OPTIO40LTL','OPTIO40SPN','OPTIO40NTV','OPTIO40JJS','OPTIO40QMK','OPTIO40MJJ','OPTIO40LKW','OPTIO40ZHV','OPTIO40CNG','OPTIO40LTP','OPTIO40GSG','OPTIO40ZRD','OPTIO40MNR','OPTIO40KLV','OPTIO40HCD','OPTIO40LZB','OPTIO40HKJ','OPTIO40ZPW','OPTIO40MXS','OPTIO40SXT','OPTIO40BLV','OPTIO40CSM','OPTIO40TDX','OPTIO40MMC','OPTIO40QRJ','OPTIO40XHT','OPTIO40BZF','OPTIO40TXL','OPTIO40DLD','OPTIO40ZVL','OPTIO40GFK','OPTIO40GGJ','OPTIO40HXX','OPTIO40DNF','OPTIO40XCH','OPTIO40QWV','OPTIO40QCN','OPTIO40CCB','OPTIO40TBR','OPTIO40WPF','OPTIO40LFM','OPTIO40FPG','OPTIO40BZK','OPTIO40JJG','OPTIO40BVQ','OPTIO40FDB','OPTIO40DXX','OPTIO40KTD','OPTIO40ZSW','OPTIO40XJL','OPTIO40SMH','OPTIO40TNZ','OPTIO40NNJ','OPTIO40DGL','OPTIO40DWD','OPTIO40NJQ','OPTIO40FTL','OPTIO40BLZ','OPTIO40WKD','OPTIO40NDR','OPTIO40HRR','OPTIO40CJS','OPTIO40GXS','OPTIO40CZS','OPTIO40HWN','OPTIO40CNT','OPTIO40GCL','OPTIO40KCV','OPTIO40HSL','OPTIO40RVS','OPTIO40HVM','OPTIO40XLG','OPTIO40GLN','OPTIO40XDT','OPTIO40RHL','OPTIO40ZXC','OPTIO40PBG','OPTIO40PNQ','OPTIO40MDV','OPTIO64GTZ','OPTIO64VSH','OPTIO64KJC','OPTIO64LDN','OPTIO64WNT','OPTIO64PLW','OPTIO64LVL','OPTIO64DSP','OPTIO64XQH','OPTIO64HKR','OPTIO64FKG','OPTIO64DGL','OPTIO64ZJQ','OPTIO64DJH','OPTIO64VCK','OPTIO64FRL','OPTIO64FMG','OPTIO64JTW','OPTIO64WFG','OPTIO64LSF','OPTIO64XFD','OPTIO64MWB','OPTIO64QVG','OPTIO64KCK','OPTIO64FJZ','OPTIO64VPP','OPTIO64JHZ','OPTIO64HXH','OPTIO64CWJ','OPTIO64NKX','OPTIO64CLQ','OPTIO64QKC','OPTIO64SVX','OPTIO64HVX','OPTIO64FPF','OPTIO64TSJ','OPTIO64TNV','OPTIO64QNS','OPTIO64MJB','OPTIO64PWJ','OPTIO64BHX','OPTIO64JGX','OPTIO64QLW','OPTIO64HPQ','OPTIO64ZXW','OPTIO64ZHJ','OPTIO64JSQ','OPTIO64CBN','OPTIO64QLG','OPTIO64PCD','OPTIO64HRK','OPTIO64LMZ','OPTIO64KRX','OPTIO64LWF','OPTIO64SHJ','OPTIO64VRN','OPTIO64XVH','OPTIO64VNH','OPTIO64NNP','OPTIO64FTW','OPTIO64BPW','OPTIO64RHG','OPTIO64RCK','OPTIO64NWH','OPTIO64NSC','OPTIO64XRD','OPTIO64SCW','OPTIO64SQC','OPTIO64KPZ','OPTIO64KRZ','OPTIO64HDN','OPTIO64QWS','OPTIO64VDC','OPTIO64SCK','OPTIO64PRP','OPTIO64CQP','OPTIO64NZD','OPTIO64JLH','OPTIO64XPC','OPTIO64GPF','OPTIO64NDC','OPTIO64ZSW','OPTIO64JPW','OPTIO64HHK','OPTIO64KTB','OPTIO64MLX','OPTIO64VNQ','OPTIO64MVV','OPTIO64DMP','OPTIO64LJJ','OPTIO64GPJ','OPTIO64DBD','OPTIO64GBS','OPTIO64MMD','OPTIO64CPC','OPTIO64MSF','OPTIO64DKP','OPTIO64BJW','OPTIO64CZX','OPTIO64RZQ','MEN100XX','MEN100DM','MEN100SZ','MEN100KP','MEN100WB','CPLATTE40','CPLATTEXXX','BMPUTZ50','CPQUALI50','BMBINGO3H','BMWEL20','BMVKH120','BMVKH220','BMVKH320','CPCAKE50','CPCAKE35','CPPLACE50','BMBFW20','BMBFN20','CPBINGO3H','CPMNXX','DEINZER20','BMPST10','BLICK50','BMPST25','HUIS20','CPHG50','CPHGXZG','CPHGKSD','CPHGLDZ','CPHGCLK','CPHGMCL','CPHGJJM','CPHGTQS','CPHGQFM','CPHGSPD','CPHGRBS')
            THEN 'Brand Marketing Offline'::text
            

            WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%b'::text) 
                AND 
                    (t.voucher__c IS NULL 
                        OR t.voucher__c::text = ''::text 
                        OR t.voucher__c::text in ('WISCH1SEB','WISCH1SEM','TIGER1SEB','WISCHMOP3A','WISCHMOP3C','WISCHMOPS15','WISCHMOP1S','WISCHMOP4B'))
                AND 
                    t.acquisition_channel_params__c::text !~~ '%disp%'::text
                OR t.voucher__c::text in ('WISCH1SEB','SEBTIGER20','SEBTIGER40','TIGER1SEB','SEBTIG15','SEBTIG25','SBTG3HA','SBTG3HB','SBTG3NZD','SBTG3BZJ','SBTG3FFS','SBTG3NNX','SBTG3SJH','SBTG3BDG','SBTG3PMJ','SBTG3KLN','SBTG3DNG','SBTG3ZLD','SBTG3XNX','SBTG3NKJ','SBTG3DBW','SBTG3HWJ','SBTG3NHQ','SBTG3GNM','SBTG3HNS','SBTG3XGF','SBTG3VDF','SBTG3RZV','SBTG3ZTN','SBTG3DDC','SBTG3ZZF','SBTG3VDZ','SBTG3LBH','SBTG3FQT','SBTG3QCP','SBTG3DDZ','SBTG3HST','SBTG3RBF','SBTG3BWH','SBTG3KDD','SBTG3DQM','SBTG3TNM','SBTG3ZTH','SBTG3ZQH','SBTG3FJR','SBTG3JRC','SBTG3DZX','SBTG3LSK','SBTG3WJJ','SBTG3KCF','SBTG3DDH','SBTG3JZC','SBTG3FDX','SBTG3DJB','SBTG3MSQ','SBTG3JGT','SBTG3BCH','SBTG3XWD','SBTG3WZZ','SBTG3SRD','SBTG3DNX','SBTG3JTG','SBTG3HKV','SBTG3LMG','SBTG3XZH','SBTG3KVQ','SBTG3VGK','SBTG3FZF','SBTG3KCZ','SBTG3NJG','SBTG3ZZT','SBTG3VTZ','SBTG3QBG','SBTG3MCR','SBTG3GPN','SBTG3HTR','SBTG3BMW','SBTG3QTD','SBTG3KXD','SBTG3NRH','SBTG3CJC','SBTG3CXG','SBTG3BLR','SBTG3KJC','SBTG3MLB','SBTG3KMH','SBTG3TSW','SBTG3HTC','SBTG3VBP','SBTG3WZP','SBTG3BZT','SBTG3WMD','SBTG3DVD','SBTG3GSJ','SBTG3VNK','SBTG3BBD','SBTG3BTZ','SBTG3XSM','SBTG3WXQ','SBTG3WJM','SBTG3JGF','SBTG3NQS','SBTG3HBR','SBTG3SVN','SBTG3NSK','SBTG3BHP','SBTG3MPJ','SBTG3DFX','SBTG3LWZ','SBTG3GWF','SBTG3DTG','SBTG3JDV','SBTG3XZL','SBTG3CLQ','SBTG3FPL','SBTG3QGT','SBTG3RWM','SBTG3DMZ','SBTG3CBN','SBTG3WRF','SBTG3GFR','SBTG3CNX','SBTG3VLD','SBTG3LPW','SBTG3HFN','SBTG3QZT','SBTG3XJL','SBTG3DRB','SBTG3XLM','SBTG3ZBZ','SBTG3VMZ','SBTG3VKL','SBTG3QXH','SBTG3GGH','SBTG3CSD','SBTG3KGP','SBTG3WGB','SBTG3GLG','SBTG3RHL','SBTG3WPJ','SBTG3CLF','SBTG3BNH','SBTG3BDL','SBTG3HMW','SBTG3HLB','SBTG3XPV','SBTG3STZ','SBTG3LCJ','SBTG3KXM','SBTG3DFG','SBTG3DKN','SBTG3FKR','SBTG3BHZ','SBTG3CMG','SBTG3PCM','SBTG3GVW','SBTG3FMF','SBTG3XRT','SBTG3LLL','SBTG3PST','SBTG3PBS','SBTG3MCX','SBTG3JXS','SBTG3VVP','SBTG3CDF','SBTG3JHC','SBTG3FQB','SBTG3QWN','SBTG3TXW','SBTG3ZRW','SBTG3THK','SBTG3XCV','SBTG3ZJS','SBTG3QFV','SBTG3SGK','SBTG3FFW','SBTG3CHK','SBTG3MSJ','SBTG3NPG','SBTG3LWQ','SBTG3THV','SBTG3RSR','SBTG3CRR','SBTG3LFQ','SBTG3PSL','SBTG3XMP','SBTG3PFM','SBTG3ZSG','SBTG3ZDN','SBTG3MNC','SBTG3TWS','SBTG3VPF','SBTG3ZQQ','SBTG3DWS','SBTG3WKM','SBTG3LGP','SBTG3BNS','SBTG3GDC','SBTG3RQW','SBTG3WMT','SBTG3CWJ','SBTG3CFT','SBTG3DKJ','SBTG3LCH','SBTG3SFZ','SBTG3SZP','SBTG3HHV','SBTG3XWZ')
            THEN 'SEM Brand'::text
            

            WHEN (t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text 
                AND (t.voucher__c IS NULL 
                    OR t.voucher__c ='' 
                    OR t.voucher__c in ('WISCH1SEM','WISCHMOP3C','WISCHMOP3A','TIGER1SEM'))
                OR t.voucher__c in ('WISCH1SEM','TIGER1SEM','SEMTIG15','SEMTIG25','SMTG3HA','SMTG3HB','SMTG3CGV','SMTG3KVQ','SMTG3SFF','SMTG3KFC','SMTG3FBR','SMTG3PPW','SMTG3TTN','SMTG3RDG','SMTG3VHD','SMTG3LFQ','SMTG3MQD','SMTG3PNX','SMTG3CBM','SMTG3MGN','SMTG3PCN','SMTG3XVN','SMTG3VWD','SMTG3WKH','SMTG3WVR','SMTG3GXZ','SMTG3XMW','SMTG3HBS','SMTG3BWT','SMTG3WSL','SMTG3LQL','SMTG3DBL','SMTG3CQB','SMTG3BCT','SMTG3THD','SMTG3XCN','SMTG3TPV','SMTG3KZJ','SMTG3FVP','SMTG3LLC','SMTG3CVK','SMTG3VVB','SMTG3FCC','SMTG3HLJ','SMTG3VLH','SMTG3RLN','SMTG3RMW','SMTG3PFC','SMTG3SGC','SMTG3WZN','SMTG3CPB','SMTG3XHP','SMTG3KNH','SMTG3SPW','SMTG3PDD','SMTG3WBZ','SMTG3ZJK','SMTG3BCV','SMTG3VTC','SMTG3FNP','SMTG3LZV','SMTG3VBW','SMTG3WCH','SMTG3ZKQ','SMTG3SKK','SMTG3QTL','SMTG3MPP','SMTG3FCP','SMTG3TKX','SMTG3PJT','SMTG3VMF','SMTG3QRC','SMTG3NZQ','SMTG3ZWT','SMTG3VQQ','SMTG3MCT','SMTG3NZJ','SMTG3SMT','SMTG3XPB','SMTG3NVB','SMTG3MPM','SMTG3LGV','SMTG3JRV','SMTG3ZSZ','SMTG3KMQ','SMTG3PXX','SMTG3WRZ','SMTG3ZVK','SMTG3SVD','SMTG3LZN','SMTG3SFG','SMTG3NPT','SMTG3MBC','SMTG3PBQ','SMTG3CHZ','SMTG3ZLV','SMTG3HPZ','SMTG3RJR','SMTG3MLJ','SMTG3TMR','SMTG3MPZ','SMTG3TGJ','SMTG3QCM','SMTG3QWX','SMTG3HFS','SMTG3VWZ','SMTG3WRL','SMTG3BFP','SMTG3XNB','SMTG3JWT','SMTG3QHW','SMTG3KCC','SMTG3LCR','SMTG3TLJ','SMTG3KPF','SMTG3HKN','SMTG3MFJ','SMTG3ZGK','SMTG3VBT','SMTG3PLF','SMTG3BZP','SMTG3VGG','SMTG3MSF','SMTG3BKN','SMTG3KVZ','SMTG3LMT','SMTG3HWH','SMTG3HLK','SMTG3DHS','SMTG3BNN','SMTG3HMD','SMTG3FSQ','SMTG3VNZ','SMTG3FDN','SMTG3LJT','SMTG3BZB','SMTG3CCJ','SMTG3VXH','SMTG3TXW','SMTG3LWH','SMTG3TJF','SMTG3JPW','SMTG3PVZ','SMTG3STZ','SMTG3JND','SMTG3TMQ','SMTG3SNL','SMTG3VVH','SMTG3FNK','SMTG3ZCN','SMTG3GJM','SMTG3NTF','SMTG3WDB','SMTG3XSX','SMTG3GDN','SMTG3GNP','SMTG3LCN','SMTG3HKV','SMTG3DTG','SMTG3PMC','SMTG3MBW','SMTG3KXW','SMTG3RZQ','SMTG3NZK','SMTG3ZJS','SMTG3LPW','SMTG3BBW','SMTG3PWD','SMTG3ZWR','SMTG3GMV','SMTG3TWS','SMTG3FFS','SMTG3LLD','SMTG3XDC','SMTG3HNS','SMTG3HHS','SMTG3XLV','SMTG3TCP','SMTG3DDF','SMTG3JPM','SMTG3SRC','SMTG3ZXJ','SMTG3GHM','SMTG3KRV','SMTG3HPC','SMTG3ZNG','SMTG3NRV','SMTG3KMM','SMTG3DVK','SMTG3DTR','SMTG3PRW','SMTG3CLN','SMTG3THT','SMTG3BSG','SMTG3BFX','SMTG3WCW','SMTG3XGW','SMTG3JBM','SMTG3ZJC','SMTG3SXJ','SMTG3VZM','SMTG3QTW','SMTG3HKP','SMTG3MND','SMTG3JDS','SMTG3SFT')
            THEN 'SEM'::text
            

            WHEN t.acquisition_channel_params__c::text ~~ '%disp%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%remarketing%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%Facebook%'::text 
                AND (t.voucher__c IS NULL 
                    OR t.voucher__c = '' 
                    OR t.voucher__c in ('WISCH1GDN','WISCH1SOC','WISCH1GSP'))
                OR t.voucher__c in ('WISCH1GDN','WISCH1SOC','WISCH1GSP','WISCH1CR','DSPUTZ20','TIGER1GDN','GDTIGER20','GDTIG70','SPRINGDS20','SPRINGDS50','SPRINGDS3P','AMTIGER20','AMTIGER1H','XNGTIGER20','RAB20','TIGER1CR','TIGER1XNG','TIGER1LNK','TIGER1SOC','GDML15','GDML20','GDML25','GDML30','GDML50','GDML70','LNKML15','LNKML20','LNKML25','LNKML30','LNKML50','LNKML70','XNGML15','XNGML20','XNGML25','XNGML30','XNGML50','XNGML70','XNGTIGER15','XNGTIGER50','XNGTIGER70','LNKTIGER15','LNKTIGER20','LNKTIGER25','LNKTIGER30','LNKTIGER50','LNKTIGER70','GDTIGER15','GDTIGER25','GDTIGER30','GDTIGER40','GDTIGER50','GDRBK10','GDRBK5','SCTIGER20','SCTIGER50','SCML20','SCML50','CRTIGER20','CRTIGER50','CRML20','CRML50','GD1ML','GDML40','GDTG3HA')
            THEN 'Display'::text
            

            WHEN t.acquisition_channel_params__c::text ~~ '%disp%'::text 
                AND t.acquisition_channel_params__c::text ~~ '%remarketing%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%Facebook%'::text 
            THEN 'Display Remarketing'::text
            

            WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text 
                AND (t.voucher__c IS NULL 
                    OR t.voucher__c = '' 
                    OR t.voucher__c in ('WISCH1YOU'))
                OR t.voucher__c in ('WISCH1YOU')
            THEN 'Youtube Paid'::text
            

            WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
                AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text 
                AND (t.voucher__c IS NULL 
                    OR t.voucher__c::text = ''::text 
                    OR t.voucher__c::text in ('SPRINGS20','ANNA'))
            THEN 'SEO'::text
            

            WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
                AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text 
                AND (t.voucher__c IS NULL 
                    OR t.voucher__c::text = ''::text 
                    OR t.voucher__c in ('WISCH1ABC','TIGER1ABC','TIGERABC20','TIGERABC40'))
                OR t.voucher__c in ('WISCH1ABC','TIGER1ABC','TIGERABC20','TIGERABC40','SEBOURSQJ','SEBOURFWR','SEBOURJHS')
                THEN 'SEO Brand'::text
                

            WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%batfb%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
                
                AND (t.acquisition_channel_params__c::text ~~ '%utm%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%utm_campaign%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%lead-gen%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%fblg%'::text) 
                
                AND (t.voucher__c IS NULL 
                    OR t.voucher__c::text = ''::text 
                    OR t.voucher__c::text in ('WISCHMOP3C','WISCH1FB','RABATT20','FBPRIMA20','FBPUTZCH40','FBPUTZ20','FBPUTZ15','FBTIGER50','STAR50','FBTIGER20','FBTIGER30','FBTIGER40','FBTIGER15','FBTIG70','SPRINGFB20','SPRINGFB50','SPRINGFB3P'))
            
                OR t.voucher__c in ('WISCH1FB','RABATT20','FBPRIMA20','FBPUTZCH40','FBPUTZ20','TIGER1FB','FBTIGER15','FBPUTZ15','FBTIGER50','FBTIGER20','STAR50','FBTIGER30','FBTIGER40','FBTIG70','FBTIGER25','FBML15','FBML20','FBML25','FBML30','FBML50','FBML70','FBRC70','FBRC50','FBKIT10','FBBOX5','FBKIT','VADER10','APPIE15','FBKIT','FB1ML','FBML40','FBRBK10','FBRBK15','FBRBK20','SSQUAD40','FBTG3HA','FBTG3HB','FBTG3CBD','FBTG3MMX','FBTG3JLW','FBTG3RSC','FBTG3MVJ','FBTG3FLX','FBTG3NCH','FBTG3BPX','FBTG3MWG','FBTG3TNX','FBTG3JRM','FBTG3KVD','FBTG3FWS','FBTG3VQR','FBTG3WCJ','FBTG3NCC','FBTG3MBW','FBTG3FDQ','FBTG3DGN','FBTG3NKQ','FBTG3JTC','FBTG3KCT','FBTG3MHJ','FBTG3MSM','FBTG3KLK','FBTG3TSM','FBTG3KQZ','FBTG3MKJ','FBTG3KZW','FBTG3ZKT','FBTG3BDX','FBTG3RMP','FBTG3DNK','FBTG3DXF','FBTG3ZSZ','FBTG3LZC','FBTG3MWB','FBTG3FHX','FBTG3KZZ','FBTG3KPH','FBTG3WCX','FBTG3JZS','FBTG3CXD','FBTG3WHP','FBTG3WZR','FBTG3HGB','FBTG3PFS','FBTG3PLK','FBTG3SFX','FBTG3LWW','FBTG3WZV','FBTG3TCX','FBTG3XKD','FBTG3SPM','FBTG3VKC','FBTG3ZRZ','FBTG3XTJ','FBTG3TDB','FBTG3RSX','FBTG3VHG','FBTG3QGR','FBTG3MXV','FBTG3JCF','FBTG3ZMC','FBTG3CQK','FBTG3KHX','FBTG3HPV','FBTG3MZD','FBTG3RZF','FBTG3WBN','FBTG3LFP','FBTG3GKN','FBTG3GQD','FBTG3DXM','FBTG3BZG','FBTG3ZHL','FBTG3SJX','FBTG3DSR','FBTG3NNX','FBTG3GML','FBTG3MCW','FBTG3GSZ','FBTG3DSP','FBTG3GVF','FBTG3GCR','FBTG3DHN','FBTG3NDQ','FBTG3JPF','FBTG3GKH','FBTG3VBZ','FBTG3PXB','FBTG3MDV','FBTG3JVD','FBTG3WBW','FBTG3MVR','FBTG3XHJ','FBTG3VQX','FBTG3LXW','FBTG3MLP','FBTG3VZP','FBTG3PFW','FBTG3SCM','FBTG3QSD','FBTG3JCD','FBTG3JLM','FBTG3HRK','FBTG3ZLT','FBTG3WGM','FBTG3FZB','FBTG3RRG','FBTG3DKN','FBTG3CSK','FBTG3PCQ','FBTG3SBQ','FBTG3KCH','FBTG3KNW','FBTG3PQM','FBTG3TMZ','FBTG3NTM','FBTG3GPL','FBTG3VHN','FBTG3SDF','FBTG3TCR','FBTG3KMS','FBTG3QSR','FBTG3MLK','FBTG3BWB','FBTG3XVB','FBTG3WCL','FBTG3WJR','FBTG3TSC','FBTG3TKH','FBTG3VCZ','FBTG3ZQF','FBTG3HBZ','FBTG3PLH','FBTG3LRJ','FBTG3RQH','FBTG3QQQ','FBTG3BCL','FBTG3NHD','FBTG3BKJ','FBTG3LXX','FBTG3MSN','FBTG3KRB','FBTG3QRQ','FBTG3NTF','FBTG3PXN','FBTG3RXL','FBTG3VNK','FBTG3VSR','FBTG3NSZ','FBTG3XJB','FBTG3FLH','FBTG3HHN','FBTG3CLC','FBTG3GHP','FBTG3LNZ','FBTG3VCQ','FBTG3QRS','FBTG3RGV','FBTG3NRJ','FBTG3XNR','FBTG3VSW','FBTG3BQW','FBTG3KMQ','FBTG3CVT','FBTG3QKL','FBTG3XRJ','FBTG3XHK','FBTG3CCC','FBTG3QZD','FBTG3SQG','FBTG3NDL','FBTG3JPL','FBTG3LCM','FBTG3BSM','FBTG3QQR','FBTG3CFM','FBTG3CLQ','FBTG3FBK','FBTG3GVX','FBTG3DPD','FBTG3XCK','FBTG3WJB','FBTG3RVH','FBTG3KVV','FBTG3NHG','FBTG3ZVL','FBTG3HWF','FBTG3QFQ','FBTG3XZV','FBTG3DXD','FBTG3QZN','FBTG3PKG','FBTG3NHJ','FBTG3WMH','FBTG3VKX','FBTG3PCL','FBTG3KNH','TIJGER25','FBMLBX5','GDMLBX5','FBDCT20','FBDCT25','FBDCT50','FBNC25','FBNC30')
            THEN 'Facebook'::text
            

            WHEN t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
                AND t.acquisition_channel_ref__c::text !~~ '%batfb%'::text 
                AND (t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
                    OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
                AND t.voucher__c IS NULL 
            THEN 'Facebook Organic'::text
            

            WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
                    OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
                    OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text
                    OR t.voucher__c IN ('GESCHENK15','GESCHENK1H','GIFT15','GIFT1H','TBOX5','SUMMER10','SUMMER15','SUMMER1H','NEWTIGER20','NEWTIGER50','TIGERLOVE','MOVE24','INCENT1','INCENT2','TIGERHELP5','TIGERLOVE10','TIGERHELP10','YELLOW15','YELLOW1H','YELLOW35','TIGERKIT1','TIGERKIT5','GESCHENK35','GIFT35','CLEAN5','CLEAN10','CLEAN20','YELLOW10','TIGERKIT10','SUN15','SUN35','EMSURVEY','EMSEP15','EMSEP35','EMSEP25','EMNC25','EMAC25','EMNC30','EMAC30','EMACHP')
            THEN 'Newsletter'::text
            

            WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%disp%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
                    AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
                    AND t.acquisition_channel_params__c::text <> ''::text)
            THEN 'DTI'::text

            
            WHEN t.acquisition_channel_params__c::text !~~ '%goog%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
                AND t.voucher__c::text > '0'::text
                AND t.voucher__c::text <> ''::text
                AND t.voucher__c::text IS NOT NULL
                AND t.voucher__c not in ('20MIN50')
            THEN 'Voucher Campaigns'::text

            ELSE 'Unattributed'::text

        END) 

        END AS marketing_channel,

        CASE
            WHEN "left"(t.shippingpostalcode::text, 2) = '52'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Aachen'::text
            WHEN (("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['10'::text, '11'::text, '12'::text, '13'::text, '14'::text, '15'::text])) OR ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['140'::text, '141'::text, '144'::text, '145'::text, '153'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Berlin'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = '320'::text OR "left"(t.shippingpostalcode::text, 2) = '33'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Bielefeld'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['530'::text, '531'::text, '532'::text, '533'::text, '537'::text, '538'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Bonn'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '28'::text OR "left"(t.shippingpostalcode::text, 3) = '277'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Bremen'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['502'::text, '503'::text, '504'::text, '505'::text, '506'::text, '507'::text, '508'::text, '509'::text, '510'::text, '511'::text, '513'::text, '514'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Cologne'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['642'::text, '643'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Darmstadt'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '44'::text OR (t.shippingpostalcode::text = ANY (ARRAY['441'::character varying::text, '442'::character varying::text, '443'::character varying::text, '445'::character varying::text, '457'::character varying::text, '580'::character varying::text, '582'::character varying::text, '583'::character varying::text, '584'::character varying::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Dortmund'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '01'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Dresden'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['454'::text, '461'::text, '470'::text, '471'::text, '472'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Duisburg'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '47'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Duisburg+'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '40'::text OR ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['414'::text, '415'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Dusseldorf'::text
            WHEN (("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['44866'::text, '44867'::text, '46047'::text, '46236'::text, '46238'::text, '46240'::text])) OR "left"(t.shippingpostalcode::text, 2) = '45'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Essen'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['446'::text, '448'::text, '462'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Essen+'::text
            WHEN (("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['61118'::text, '61440'::text, '61449'::text, '63065'::text, '63067'::text, '63069'::text, '63071'::text, '63073'::text, '63075'::text, '63150'::text, '63165'::text, '63263'::text, '63303'::text, '63477'::text, '65451'::text, '65760'::text, '65824'::text, '65843'::text, '65929'::text, '65931'::text, '65933''65934'::text, '65936'::text])) OR "left"(t.shippingpostalcode::text, 2) = '60'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Frankfurt am Main'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['611'::text, '612'::text, '613'::text, '614'::text, '630'::text, '631'::text, '632'::text, '633'::text, '634'::text, '654'::text, '657'::text, '658'::text, '659'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Frankfurt am Main+'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '79'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Freiburg'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '59'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Halle'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '58'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hagen'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['20'::text, '22'::text])) OR ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['21031'::text, '21075'::text, '21077'::text, '21079''21107'::text, '21109'::text, '21129'::text])) THEN 'DE-Hamburg'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '21'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hamburg+'::text
            WHEN "left"(t.shippingpostalcode::text, 3) = '483'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hamm'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '30'::text OR "left"(t.shippingpostalcode::text, 3) = '311'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Hanover'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '24'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Kiel'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '76'::text OR ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['751'::text, '750'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Kalsruhe'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['041'::text, '042'::text, '043'::text, '044'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Leipzig'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '23'::text OR "left"(t.shippingpostalcode::text, 3) = '192'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Lübeck'::text
            WHEN ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['55116'::text, '55118'::text, '55120'::text, '55122'::text, '55124'::text, '55126'::text, '55127'::text, '55128'::text, '55129'::text, '55130'::text, '55131'::text, '55246'::text, '55252'::text, '55257'::text, '55294'::text, '65201'::text, '65203'::text, '65239'::text, '65462'::text, '65474'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mainz'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['550'::text, '551'::text, '552'::text, '650'::text, '651'::text, '652'::text, '653'::text, '655'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mainz+'::text
            WHEN ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['67059'::text, '67061'::text, '67063'::text, '67065'::text, '67067'::text, '67069'::text, '67071'::text, '67122'::text, '67141'::text, '68159'::text, '68161'::text, '68163'::text, '68165'::text, '68167'::text, '68169'::text, '68199'::text, '68219'::text, '68229'::text, '68239'::text, '68259'::text, '68305'::text, '68307'::text, '68309'::text, '68549'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mannheim'::text
            WHEN (("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['670'::text, '671'::text, '672'::text, '673'::text, '690'::text, '691'::text, '692'::text, '694'::text, '699'::text])) OR "left"(t.shippingpostalcode::text, 2) = '68'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mannheim+'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '39'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Magdeburg'::text
            WHEN ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['41061'::text, '41063'::text, '41065'::text, '41066'::text, '41068'::text, '41069'::text, '41169'::text, '41179'::text, '41189'::text, '41199'::text, '41236'::text, '41238'::text, '41239'::text, '41352'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mönchengladbach'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '41'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Mönchengladbach+'::text
            WHEN "left"(t.locale__c::text, 2) = 'de'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['80'::text, '81'::text])) OR ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['82008'::text, '82024'::text, '82031'::text, '82041'::text, '82049'::text, '82061'::text, '82166'::text, '85521'::text, '85579'::text, '85609'::text, '85737'::text, '85774'::text])) THEN 'DE-Munich'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['480'::text, '481'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Munster'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '90'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Nuremberg'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['910'::text, '911'::text, '912'::text, '913'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Nuremberg+'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['930'::text, '931'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Regensburg'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['180'::text, '181'::text, '182'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Rostock'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['660'::text, '661'::text, '662'::text, '663'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Saarbrücken'::text
            WHEN ("left"(t.shippingpostalcode::text, 2) = '70'::text OR ("left"(t.shippingpostalcode::text, 5) = ANY (ARRAY['71254'::text, '73728'::text, '73732'::text, '73733'::text, '73734'::text, '73760'::text]))) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Stuttgart'::text
            WHEN (("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['710'::text, '712'::text, '716'::text, '737'::text, '720'::text, '721'::text, '725'::text, '726'::text, '727'::text, '730'::text, '732'::text, '736'::text, '743'::text, '752'::text, '753'::text, '754'::text])) OR "left"(t.shippingpostalcode::text, 2) = '71'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Stuttgart+'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = '312'::text OR "left"(t.shippingpostalcode::text, 2) = '38'::text) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-WBurgBschweig'::text
            WHEN "left"(t.shippingpostalcode::text, 2) = '42'::text AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Wuppertal'::text
            WHEN ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['970'::text, '971'::text, '972'::text])) AND "left"(t.locale__c::text, 2) = 'de'::text THEN 'DE-Würzburg'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 1) = '1'::text THEN 'AT-Vienna'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '50'::text THEN 'AT-Salzburg'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '60'::text THEN 'AT-Insbruck'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '40'::text THEN 'AT-Linz'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '80'::text THEN 'AT-Graz'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND "left"(t.shippingpostalcode::text, 2) = '90'::text THEN 'AT-Klagenfurt'::text
            WHEN "left"(t.locale__c::text, 2) = 'at'::text AND ("left"(t.shippingpostalcode::text, 1) = ANY (ARRAY['2'::text, '3'::text, '4'::text, '5'::text, '6'::text, '7'::text, '8'::text, '9'::text])) THEN 'AT-Other'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['420'::text, '422'::text, '424'::text, '440'::text, '441'::text, '442'::text])) THEN 'CH-Basel'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['341'::text, '350'::text, '351'::text])) THEN 'CH-Bern'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['111'::text, '112'::text, '150'::text, '151'::text, '160'::text, '161'::text, '167'::text, '169'::text, '180'::text])) THEN 'CH-Lausanne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['564'::text, '610'::text, '620'::text, '627'::text, '628'::text, '630'::text, '631'::text, '634'::text, '635'::text, '636'::text, '637'::text, '638'::text, '640'::text, '641'::text])) THEN 'CH-Lucerne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['520'::text, '910'::text, '911'::text, '920'::text, '921'::text, '922'::text, '923'::text, '924'::text, '930'::text, '931'::text, '932'::text, '940'::text, '941'::text, '942'::text, '943'::text, '945'::text])) THEN 'CH-St.Gallen'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 3) = ANY (ARRAY['542'::text, '543'::text, '545'::text, '562'::text, '824'::text, '830'::text, '831'::text, '832'::text, '833'::text, '835'::text, '840'::text, '841'::text, '842'::text, '844'::text, '845'::text, '847'::text, '848'::text, '849'::text, '860'::text, '861'::text, '870'::text, '880'::text, '881'::text, '890'::text, '891'::text, '893'::text, '894'::text, '895'::text, '896'::text, '954'::text])) THEN 'CH-Zurich'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['40'::text, '41'::text])) THEN 'CH-Basel'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['30'::text, '31'::text, '32'::text, '33'::text, '34'::text])) THEN 'CH-Bern'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '25'::text THEN 'CH-Biel'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '12'::text THEN 'CH-Geneva'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['10'::text, '13'::text])) THEN 'CH-Lausanne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '60'::text THEN 'CH-Lucerne'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND "left"(t.shippingpostalcode::text, 2) = '90'::text THEN 'CH-St.Gallen'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['80'::text, '81'::text, '85'::text])) THEN 'CH-Zurich'::text
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND ("left"(t.shippingpostalcode::text, 1) = ANY (ARRAY['1'::text, '2'::text, '3'::text, '4'::text, '5'::text, '6'::text, '7'::text, '8'::text, '9'::text])) THEN 'CH-Other'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '18'::text THEN 'NL-Alkmaar'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '13'::text THEN 'NL-Almere'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '38'::text THEN 'NL-Amersfoort'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['10'::text, '11'::text, '14'::text, '15'::text, '21'::text])) THEN 'NL-Amsterdam'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['48'::text, '50'::text])) THEN 'NL-Breda-Tillburg'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['54'::text, '55'::text, '56'::text, '57'::text])) THEN 'NL-Eindhoven'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['27'::text, '28'::text])) THEN 'NL-Gouda'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['19'::text, '20'::text])) THEN 'NL-Haarlem'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '52'::text THEN 'NL-Hertogenbosch'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '12'::text THEN 'NL-Hilversum'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '23'::text THEN 'NL-Leiden'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND "left"(t.shippingpostalcode::text, 2) = '62'::text THEN 'NL-Maastricht'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['30'::text, '31'::text])) THEN 'NL-Rotterdam'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['22'::text, '24'::text, '25'::text, '26'::text])) THEN 'NL-The Hague'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['34'::text, '35'::text, '36'::text, '37'::text, '39'::text])) THEN 'NL-Utrecht'::text
            WHEN "left"(t.locale__c::text, 2) = 'nl'::text AND ("left"(t.shippingpostalcode::text, 2) = ANY (ARRAY['3'::text, '4'::text, '5'::text, '6'::text, '7'::text, '8'::text, '9'::text])) THEN 'NL-Other'::text
            ELSE 'DE-Other'::text
        END AS city, -- XXXX
        CASE
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date < '2015-10-01'::date THEN t.gmv__c * 0.96::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2015-10-01'::date AND t.order_creation__c::date  < '2017-12-01') THEN t.gmv__c * 0.92::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2017-12-01'::date AND t.order_creation__c::date  < '2018-01-01') THEN t.gmv__c * 0.85696::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-01-01'::date AND t.order_creation__c::date  < '2018-02-01') THEN t.gmv__c * 0.85497::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-02-01'::date AND t.order_creation__c::date  < '2018-03-01') THEN t.gmv__c * 0.86243::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-03-01'::date AND t.order_creation__c::date  < '2018-04-01') THEN t.gmv__c * 0.86721::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-04-01'::date AND t.order_creation__c::date  < '2018-05-01') THEN t.gmv__c * 0.85067::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-05-01'::date AND t.order_creation__c::date  < '2018-06-01') THEN t.gmv__c * 0.83410::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-06-01'::date AND t.order_creation__c::date  < '2018-07-01') THEN t.gmv__c * 0.86971::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-07-01'::date AND t.order_creation__c::date  < '2018-08-01') THEN t.gmv__c * 0.86421::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-08-01'::date AND t.order_creation__c::date  < '2018-09-01') THEN t.gmv__c * 0.86292::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-09-01'::date AND t.order_creation__c::date  < '2018-10-01') THEN t.gmv__c * 0.88224::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-10-01'::date AND t.order_creation__c::date  < '2018-11-01') THEN t.gmv__c * 0.87716::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-11-01'::date AND t.order_creation__c::date  < '2018-12-01') THEN t.gmv__c * 0.87718::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2018-12-01'::date AND t.order_creation__c::date  < '2019-01-01') THEN t.gmv__c * 0.88333::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2019-01-01'::date AND t.order_creation__c::date  < '2019-02-01') THEN t.gmv__c * 0.88740::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2019-02-01'::date AND t.order_creation__c::date  < '2019-03-01') THEN t.gmv__c * 0.87732::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND (t.order_creation__c::date >= '2019-03-01'::date AND t.order_creation__c::date  < '2019-04-01') THEN t.gmv__c * 0.87894::double precision
            
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2019-04-01'::date THEN t.gmv__c * 0.89543::double precision 
            ELSE t.gmv__c
        END AS gmv_eur,


        CASE
        WHEN type::text is NULL or type != 'cleaning-b2b'
                        THEN (CASE WHEN LEFT(t.locale__c::text, 2) = 'at' THEN (t.gmv__c*0.2)/1.2
                             WHEN LEFT(t.locale__c::text, 2) = 'de' and type = 'cleaning-b2c' THEN (t.gmv__c)/1.19
                                WHEN    LEFT(t.locale__c::text, 2) = 'de' and (type != 'cleaning-b2c' or type != 'cleaning-b2b') THEN (t.gmv__c*0.2)/1.19
                             WHEN LEFT(t.locale__c::text, 2) = 'ch' THEN (CASE
                                                                         WHEN t.order_creation__c::date < '2015-10-01'::date THEN t.gmv__c * 0.96::double precision
                                                                         WHEN (t.order_creation__c::date >= '2015-10-01'::date AND t.order_creation__c::date  < '2017-12-01') THEN t.gmv__c * 0.92::double precision
                                                                         WHEN (t.order_creation__c::date >= '2017-12-01'::date AND t.order_creation__c::date  < '2018-01-01') THEN t.gmv__c * 0.85696::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-01-01'::date AND t.order_creation__c::date  < '2018-02-01') THEN t.gmv__c * 0.85497::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-02-01'::date AND t.order_creation__c::date  < '2018-03-01') THEN t.gmv__c * 0.86243::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-03-01'::date AND t.order_creation__c::date  < '2018-04-01') THEN t.gmv__c * 0.86721::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-04-01'::date AND t.order_creation__c::date  < '2018-05-01') THEN t.gmv__c * 0.85067::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-05-01'::date AND t.order_creation__c::date  < '2018-06-01') THEN t.gmv__c * 0.83410::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-06-01'::date AND t.order_creation__c::date  < '2018-07-01') THEN t.gmv__c * 0.86971::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-07-01'::date AND t.order_creation__c::date  < '2018-08-01') THEN t.gmv__c * 0.86421::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-08-01'::date AND t.order_creation__c::date  < '2018-09-01') THEN t.gmv__c * 0.86292::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-09-01'::date AND t.order_creation__c::date  < '2018-10-01') THEN t.gmv__c * 0.88224::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-10-01'::date AND t.order_creation__c::date  < '2018-11-01') THEN t.gmv__c * 0.87716::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-11-01'::date AND t.order_creation__c::date  < '2018-12-01') THEN t.gmv__c * 0.87718::double precision
                                                                         WHEN (t.order_creation__c::date >= '2018-12-01'::date AND t.order_creation__c::date  < '2019-01-01') THEN t.gmv__c * 0.88333::double precision
                                                                         WHEN (t.order_creation__c::date >= '2019-01-01'::date AND t.order_creation__c::date  < '2019-02-01') THEN t.gmv__c * 0.88740::double precision
                                                                         WHEN (t.order_creation__c::date >= '2019-02-01'::date AND t.order_creation__c::date  < '2019-03-01') THEN t.gmv__c * 0.87732::double precision
                                                                         WHEN (t.order_creation__c::date >= '2019-03-01'::date AND t.order_creation__c::date  < '2019-04-01') THEN t.gmv__c * 0.87894::double precision
                                                                         
                                                                         
                                                                         WHEN t.order_creation__c::date >= '2019-04-01'::date THEN t.gmv__c * 0.89543::double precision
                                                                         END)/1.08
                             WHEN LEFT(t.locale__c::text, 2) = 'nl' THEN (t.gmv__c)/1.06 END) 
            
        ELSE    (CASE
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date < '2015-10-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.96::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2015-10-01'::date AND t.order_creation__c::date < '2017-12-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.92::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2017-12-01'::date AND t.order_creation__c::date < '2018-01-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.85696::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-01-01'::date AND t.order_creation__c::date < '2018-02-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.85497::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-02-01'::date AND t.order_creation__c::date < '2018-03-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.86243::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-03-01'::date AND t.order_creation__c::date < '2018-04-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.86721::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-04-01'::date AND t.order_creation__c::date < '2018-05-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.85067::double precision
                        
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-05-01'::date AND t.order_creation__c::date < '2018-06-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.83410::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-06-01'::date AND t.order_creation__c::date < '2018-07-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.86971::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-07-01'::date AND t.order_creation__c::date < '2018-08-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.86421::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-08-01'::date AND t.order_creation__c::date < '2018-09-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.86292::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-09-01'::date AND t.order_creation__c::date < '2018-10-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.88224::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-10-01'::date AND t.order_creation__c::date < '2018-11-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.87716::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-11-01'::date AND t.order_creation__c::date < '2018-12-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.87718::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-12-01'::date AND t.order_creation__c::date < '2019-01-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.88333::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2019-01-01'::date AND t.order_creation__c::date < '2019-02-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.88740::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2019-02-01'::date AND t.order_creation__c::date < '2019-03-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.87732::double precision
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2019-03-01'::date AND t.order_creation__c::date < '2019-04-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.87894::double precision
                        
                        WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2019-04-01'::date THEN ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c) * 0.89543::double precision
                        
                        ELSE ((CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END)*Order_Duration__c)
                    END)
                    
              
                    
        END

        as gmv_eur_net,

    p.delivery_area::varchar(50) AS delivery_area,
    p.efficiency_area AS efficiency_area,
    p.polygon,
    t.error_note__c as error_note__c,
    t2.createddate as customer_creation_date,
    CASE WHEN pph__c = '0' THEN Eff_PPH else pph__c END as eff_pph
   FROM salesforce."order" t
   LEFT JOIN Salesforce.contact t2 ON t.contact__c = t2.sfid
     LEFT JOIN bi.orders_location p ON t.order_id__c::text = p.order_id__c::text
     LEFT JOIN bi.b2b_pph_eff t4 ON (TO_CHAR(t.effectivedate::date,'YYYY-MM') = t4.year_month and t.opportunityid = t4.opportunity_id );
  
  
ALTER TABLE bi.orders
  OWNER TO u1250badtnvu5v;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'