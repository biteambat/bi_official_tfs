

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_margin_per_cleaner(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_margin_per_cleaner';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.margin_per_cleaner;
CREATE TABLE bi.margin_per_cleaner as
SELECT
  t2.name,
  t2.rating__c as rating,
  t5.Score_cleaners,
  CASE WHEN t2.hr_contract_start__c >= (current_date - 10) THEN 'New' ELSE 'Old' END as flag_new,
  t5.category,
  t5.existence,
  t2.type__c,
  t1.*,
  CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
  CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END as contract_morning_hours,
  t3.Working_Hours_Morning,
  availability_Share,
  CASE WHEN availability_share is null or availability_share = 0 THEN 0 ELSE cast(t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END) END as morning_ur,
  CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END as contract_hours_afternoon,
  t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
  CASE WHEN (1-availability_Share) is null or (1-availability_Share) = 0 THEN 0 ELSE cast(t3.total_hours-t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*(1-availability_Share) END) END as afternoon_ur,
  CASE WHEN availability_Share is null THEN 0 ELSE CASE WHEN t3.Working_Hours_Morning > working_hours*availability_Share THEN working_hours*availability_Share ELSE t3.Working_Hours_Morning END END capped_hours_morning,
  CASE WHEN (1-availability_Share) = 0 THEN 0 ELSE CASE WHEN t3.total_hours-t3.working_hours_morning  > working_hours*  CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END THEN working_hours*(  CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END) ELSE t3.total_hours-t3.working_hours_morning END END capped_hours_afternoon
FROM
  bi.gpm_per_cleaner_v3 t1
LEFT JOIN
    Salesforce.Account t2
ON
  (t1.professional__c = t2.sfid)
LEFT JOIN
  bi.order_distribution t3
ON
  (t1.professional__c = t3.professional__c and t1.year_month = t3.month)
LEFT JOIn
  bi.cleaner_ur_temp t4
ON
  (t1.professional__c = t4.sfid)
LEFT JOIN
  bi.Performance_cleaners t5
ON
  (t1.professional__c = t5.sfid);

-- DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3;
-- DROP TABLE IF EXISTS bi.order_distribution;
-- DROP TABLE IF EXISTS bi.cleaner_ur_temp;

----------------------------------------------------------------------------------------------------------------
-- INSERTING HERE THE GPM DATA OF SWITZERLAND INTO THE MARGIN_PER_CLEANER TABLE


INSERT INTO bi.margin_per_cleaner
SELECT

  t1.*

FROM

  bi.gpm_ch t1;



----------------------------------------------------------------------------------------------------------------
-- INSERTING HERE THE GPM DATA OF AUSTRIA INTO THE MARGIN_PER_CLEANER TABLE


INSERT INTO bi.margin_per_cleaner
SELECT

  t1.*

FROM

  bi.gpm_at t1;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'