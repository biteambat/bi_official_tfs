

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_churn_forecast(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_churn_forecast';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.churn_forecast;
CREATE TABLE bi.churn_forecast as
    
SELECT

    TO_CHAR(t1.confirmed_end__c,'YYYY-MM') as year_month,
    MIN(t1.confirmed_end__c) as confirmed_end,
    CASE WHEN t1.status__c IN ('RESIGNED', 'CANCELLED') THEN 'CHURNED' ELSE t1.status__C END as status,
    t1.sfid,
    t1.name,
    -- 'Offboarding' as status,
    COUNT(DISTINCT t1.sfid) as number_opps,
    SUM(t1.avg_last3_months) as amount_lost
    -- SUM(CASE WHEN t1.grand_total__c > t1.avg_last3_months THEN t1.grand_total__c ELSE t1.avg_last3_months END) as amount_lost
    
FROM    
    
    (SELECT
    
        o.sfid,
        o.name,
        o.status__c,
        o.stagename,
        o.delivery_area__c,
        o.grand_total__c,
        oo.potential as avg_last3_months,
        ooo.confirmed_end__c
        
    FROM
    
        salesforce.opportunity o
        
    LEFT JOIN
    
        salesforce.contract__c ooo
        
    ON
    
        o.sfid = ooo.opportunity__c
        
    LEFT JOIN
    
        bi.potential_revenue_per_opp oo
        
    ON
    
        o.sfid = oo.opportunityid
        
    WHERE
    
        o.status__c IN ('OFFBOARDING', 'RETENTION', 'RESIGNED', 'CANCELLED')
        AND o.locale__c LIKE 'de%'
        AND ooo.confirmed_end__c IS NOT NULL
        AND o.test__c IS FALSE
        AND ooo.service_type__c LIKE 'maintenance cleaning'
        -- AND ooo.active__c IS TRUE
        -- We consider that the last valid contract can be RESIGNED, CANCELLED, SIGNED or ACCEPTED
        AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')) as t1
        
WHERE

    CASE WHEN t1.grand_total__c > t1.avg_last3_months THEN t1.grand_total__c ELSE t1.avg_last3_months END IS NOT NULL
    
    
GROUP BY 

    year_month,
    t1.sfid,
    t1.name,
    CASE WHEN t1.status__c IN ('RESIGNED', 'CANCELLED') THEN 'CHURNED' ELSE t1.status__C END;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'