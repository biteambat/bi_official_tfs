

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_customer_cohorts_b2b(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_customer_cohorts_b2b';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.customer_cohorts_b2b;
CREATE TABLE bi.customer_cohorts_b2b as   
SELECT
  a.first_order_month as cohort,
  a.start_date::date as start_date,
  b.order_month,
  b.start_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.locale,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer
FROM
(SELECT
  first_order_month,
  order_month,
  order_date,
  start_date,
  locale,
  distinct_customer
FROM
   bi.customer_cohorts_overall_b2b
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  locale,
  distinct_customer
FROM
  bi.customer_cohorts_overall_b2b
) as b
ON
  (a.first_order_month = b.first_order_month and a.locale = b.locale);

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'
