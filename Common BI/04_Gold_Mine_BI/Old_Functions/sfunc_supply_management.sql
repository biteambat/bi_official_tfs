

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_supply_management(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_supply_management';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.supply_management;
CREATE TABLE bi.supply_management as

SELECT

  UPPER(LEFT(p.locale__c, 2)) as country,
  p.servicedate__c as service_date,
  p.status__c as status_order,
  p.name as type_product,
  p.quantity__c as quantity,
  p.total_amount__c as total_amount
  


FROM

  salesforce.productlineitem__c p
  
ORDER BY

  p.servicedate__c;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'