

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_b2bcohorts(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_b2bcohorts';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.b2bcohorts;
CREATE TABLE bi.b2bcohorts as 
SELECT
  t1.Contact__c,
  city,
  first_order_date,
  start_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(Start_date as date),'YYYY-MM') as Order_Month,
  status,
  order_duration__c,
  gmv_eur_net,
  acquisition_channel,
  t1.sub_kpi,
  t1.ordertype,
  t1.locale,
  acquisition_funnel,
  amount
FROM
  bi.RecurringCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.Contact__c = t2.Contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(Start_Date as date),'YYYY-MM'));

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'