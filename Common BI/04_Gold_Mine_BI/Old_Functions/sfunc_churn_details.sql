

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_churn_details(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_churn_details';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Short Description: list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED
-- churn date = last order's date - It's the last day on which they are making money

DROP TABLE IF EXISTS bi.churn_details;
CREATE TABLE bi.churn_details as 


SELECT  

		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		o.delivery_area__c,
		o.opportunityid,
		oo.name,
		oo.stagename,
		oo.status__c,
		ooo.potential as grand_total,
		oo.contract_duration__c,
		oo.hours_weekly__c,
	 	oo.hours_weekly__c*4.33 AS avg_monthly_hours,	
		oo.churn_reason__c,
		oo.nextstep,
		MAX(o.effectivedate) AS date_churn,
		MIN(o.effectivedate) AS first_order,
		MAX(o.effectivedate) - MIN(o.effectivedate) AS Age_days,
			CASE WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) < 31 THEN 'M0'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 31 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 61 THEN 'M1'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 61 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 92 THEN 'M2'
			  ELSE '>M3'
			  END as Age_Segment
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN

		bi.potential_revenue_per_opp ooo

	ON

		o.opportunityid = ooo.opportunityid
				
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		AND o.createddate >= '2018-01-01'

	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		o.delivery_area__c,
		-- type_date,
		o.opportunityid,
		oo.name,
		oo.stagename,
		ooo.potential,
		oo.contract_duration__c,
		oo.hours_weekly__c,
		oo.status__c,
		oo.churn_reason__c,
		oo.nextstep;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'