

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_rejection_reasons_per_polygon(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_rejection_reasons_per_polygon';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.rejection_reasons_per_polygon;
CREATE TABLE bi.rejection_reasons_per_polygon as 


SELECT

	Table1.countrycode as Country,
	Table1.polygon,
	CAST(Table1.year_created as integer),
	CAST(Table1.month_created as integer),
	date_created,
	CAST(Table1.year_touched as integer),
	CAST(Table1.month_touched as integer),
	date_touched1,

	CAST(Table1.Rejected as numeric),
	Table1.Availability as Availability_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Availability as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Availability,
	Table1.Distance as Distance_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Distance as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Distance,
	Table1.Personal_qualities as Personal_qualities_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Personal_qualities as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Personal_qualities,
	Table1.Professional_skills as Professional_skills_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Professional_skills as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Professional_skills,
	Table1.Contractual_reasons as Contractual_reasons_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Contractual_reasons as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Contractual_reasons,
	Table1.Student as Student_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Student as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Student,
	Table1.Other as Other_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Other as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Other


FROM

	(SELECT

		a.countrycode,
		a.delivery_area__c as polygon,
		EXTRACT(year from a.createddate) as year_created,
		EXTRACT(month from a.createddate) as month_created,
		MIN(a.createddate::date) date_created,
		EXTRACT(year from leadtouched.date_touched) as year_touched,
		EXTRACT(month from leadtouched.date_touched) as month_touched,
		MIN(leadtouched.date_touched::date) date_touched1,

		
		SUM(CASE WHEN (a.rejection_reason__c IS NULL) THEN 0 ELSE 1 END) as Rejected,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Availability') THEN 1 ELSE 0 END) as Availability,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Distance') THEN 1 ELSE 0 END) as Distance,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('No Work Permit', 'Soft Skills', 'Failed background check', 'Punctuality / reliability', 'No show', 'Appearance', 'Not reached anymore') THEN 1 ELSE 0 END) as Personal_qualities, 

		SUM(CASE WHEN a.rejection_reason__c IN ('Language skills', 'Cleaning skills') THEN 1 ELSE 0 END) as Professional_skills,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Earnings too low', 'Found other Full-time job', 'Wants full-time', 'Minijob') THEN 1 ELSE 0 END) as Contractual_Reasons,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Student') THEN 1 ELSE 0 END) as Student,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Other (add note)') THEN 1 ELSE 0 END) as Other


	FROM

		salesforce.lead a

	LEFT JOIN

			bi.timelapses_recruitment_process leadtouched

		ON

			a.sfid = leadtouched.leadid
		
	WHERE

		a.createddate > '2016-06-01'
		AND a.delivery_area__c IS NOT NULL
		AND a.countrycode IN ('DE', 'AT', 'NL', 'CH')
		AND a.delivery_area__c IN ('at-vienna', 'ch-basel', 'ch-bern', 'ch-geneva', 'ch-lausanne', 'ch-lucerne', 'ch-stgallen', 'ch-zurich', 'de-berlin', 'de-bonn', 'de-cologne', 'de-dusseldorf', 'de-essen', 'de-frankfurt', 'de-hamburg', 'de-mainz', 'de-manheim', 'de-munich', 'de-nuremberg', 'de-stuttgart', 'nl-amsterdam', 'nl-hague')
		AND a.note__c NOT LIKE '%SUBS%'
		AND a.note__c NOT LIKE '%Subs%'
		AND a.note__c NOT LIKE '%subs%'
		
		
	GROUP BY

		a.countrycode,
		a.delivery_area__c,
		year_created,
		month_created,
		year_touched,
		month_touched

	ORDER BY 

		a.countrycode asc,
		a.delivery_area__c asc,
		year_created asc,
		month_created asc,
		year_touched asc,
		month_touched asc,
		Rejected asc,
		Availability asc,
		Distance asc,
		Personal_qualities asc,
		Professional_skills asc,
		Contractual_reasons asc,
		Student asc,
		Other asc

		) as Table1

GROUP BY

	Country,
	Table1.polygon,
	Table1.year_created,
	Table1.month_created,
	Table1.date_created,
	Table1.year_touched,
	Table1.month_touched,
	date_touched1,
	Table1.Rejected,
	Table1.Availability,
	Table1.Distance,
	Table1.Personal_qualities,
	Table1.Professional_skills,
	Table1.Contractual_reasons,
	Table1.Student,
	Table1.Other
	
ORDER BY

	Country, polygon asc, date_touched1 asc, Rejected asc, Availability asc, Distance asc, Personal_qualities asc, Professional_skills asc, Contractual_reasons asc, Student asc, Other asc;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'