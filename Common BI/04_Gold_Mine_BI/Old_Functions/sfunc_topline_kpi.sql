

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_topline_kpi(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_topline_kpi';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.topline_kpi;
CREATE TABLE bi.topline_kpi as 
SELECT
  EXTRACT(Month FROM Order_Creation__C::Date) as Month,
  EXTRACT(Year FROM Order_Creation__C::Date) as Year,
  MIN(Order_Creation__C::Date) as Mindate,
  replace(city, '+', '') as city,
  CAST('Booked GMV' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
    and order_type = '1'
    AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  city;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(Month FROM Order_Creation__C::Date) as Month,
  EXTRACT(Year FROM Order_Creation__C::Date) as Year,
  MIN(Order_Creation__C::Date) as Mindate,
  UPPER(LEFT(locale__c,2)) as locale,
  CAST('Booked GMV' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  locale;

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  city,
  CAST('M1 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  city;
  

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  LEFT(city,2) as locale,
  CAST('M1 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  locale;
  

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  city,
  CAST('M3 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  city;
  

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  LEFT(city,2) as locale,
  CAST('M3 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  locale;
  
INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  city,
  CAST('M6 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '6'
GROUP BY
  month,
  year,
  city;
  

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  LEFT(city,2) as locale,
  CAST('M6 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '6'
GROUP BY
  month,
  year,
  locale;
  

INSERT INTO bi.topline_kpi    
SELECT
  EXTRACT(MONTH FROM Date) as Month,
  EXTRACT(YEAR FROM Date) as Year,
  MIN(date::date) as mindate,
  city,
  CAST('M1 RR' as text) as kpi,
  CASE WHEN acquisition_Channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
  CASE WHEN SUM(Acquisitions) > 0 THEN SUM(Acquisitions*cohort_Return_rate)/SUM(Acquisitions) ELSE 0 END as value
FROM
    bi.retention_city_marketing
GROUP BY
  Month,
  year,
  city,
  kpi,
  channel;



INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Total Customers' as kpi,
  CAST('B2C' as text) as breakdown,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(locale__c,2)) as citya,
  'Total Customers' as kpi,
  CAST('B2C' as text) as breakdown,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Total Customers' as kpi,
  CAST('B2B' as text) as breakdown,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status not like '%CANCELLED%' and status not like '%ERROR%'
  and effectivedate::date < current_date::date
  and order_type = '2'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(locale__c,2)) as citya,
  'Total Customers' as kpi,
  CAST('B2B' as text) as breakdown,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status not like '%CANCELLED%' and status not like '%ERROR%'
  and effectivedate::date < current_date::date
  and order_type = '2'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM Order_Creation__c::Date) as Month,
  EXTRACT(YEAR FROM Order_Creation__c::date) as year,
  min(Order_Creation__c::date) as mindate,
  city,
  'Acquisitions' as kpi,
  marketing_channel as breakdown,
  COUNT(1) as value
FROM
  bi.orders
WHERE
  Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  and test__c = '0'
  and Acquisition_New_Customer__c = '1'
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  year,
  city,
  breakdown;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM max_date::date) as Month,
  EXTRACT(YEAR FROM max_date::date) as Year,
  MIN(max_date::date) as mindate,
    case
  when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
  WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
  WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
  WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
  WHEN delivery_areas__c = 'ch-lucerne' then 'CH-Lucerne'
  when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
  when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
  when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
  when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('COP Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as cop
FROM
(SELECT
  TO_CHAR(date,'YYYY-MM') as Month,
  MAX(Date) as max_date
FROM
  (select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
  Month) as a,
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
WHERE
  left(b.locale__c,2) in ('de','nl')
 GROUP BY
  Year,
  Month,
  max_date::date,
  delivery_areas__c;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM max_date::date) as Month,
  EXTRACT(YEAR FROM max_date::date) as Year,
  MIN(max_date::date) as mindate,
  UPPER(left(delivery_areas__c,2)) as locale,
  CAST('COP Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as cop
FROM
(SELECT
  TO_CHAR(date,'YYYY-MM') as Month,
  MAX(Date) as max_date
FROM
  (select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
  Month) as a,
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
WHERE
  left(b.locale__c,2) in ('de','nl')
 GROUP BY
  Year,
  Month,
  max_date::date,
  locale;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM max_date::date) as Month,
  EXTRACT(YEAR FROM max_date::date) as Year,
  MIN(max_date::date) as mindate,
    case
  when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
  WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
  WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
  WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
  WHEN delivery_areas__c = 'ch-lucerne' then 'CH-Lucerne'
  when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
  when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
  when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
  when delivery_areas__c is null and left(locale__c,2) = 'ch' THEN 'CH-Other'
  when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('COP Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and (max_date < new_contract_end or hr_contract_end__c is null) THEN sfid ELSE null END)) as a
FROM
(SELECT
  TO_CHAR(date,'YYYY-MM') as Month,
  MAX(Date) as max_date
FROM
  (select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
  Month) as a,
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  ) as b
WHERE
  LEFT(b.delivery_areas__c,2) = 'ch'
 GROUP BY
  Year,
  Month,
  max_date::date,
  delivery_areas__c,
  left(locale__c,2);

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM max_date::date) as Month,
  EXTRACT(YEAR FROM max_date::date) as Year,
  MIN(max_date::date) as mindate,
  UPPER(LEFT(delivery_areas__c,2)) as locale,
  CAST('COP Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and (max_date < new_contract_end or hr_contract_end__c is null) THEN sfid ELSE null END)) as a
FROM
(SELECT
  TO_CHAR(date,'YYYY-MM') as Month,
  MAX(Date) as max_date
FROM
  (select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
  Month) as a,
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  ) as b
WHERE
  LEFT(b.delivery_areas__c,2) = 'ch'
 GROUP BY
  Year,
  Month,
  max_date::date,
  locale;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM hr_contract_start__c::date) as Month,
  EXTRACT(YEAR FROM hr_contract_start__c::date) as Year,
  MIN(hr_contract_start__c::date) as mindate,
    case
  when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
  WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
  WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
  WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
  WHEN delivery_areas__c = 'ch-lucerne' then 'CH-Lucerne'
  when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
  when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
  when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
  when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_areas__c = 'de-duisburg' THEN 'de-otherDuisburg'
  WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as City,
  CAST('Onboardings Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
(SELECT
    a.*
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
  hr_contract_start__c <= current_date
  and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
  Month,
  Year,
  City;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM hr_contract_start__c::date) as Month,
  EXTRACT(YEAR FROM hr_contract_start__c::date) as Year,
  MIN(hr_contract_start__c::date) as mindate,
  UPPER(left(delivery_areas__c,2)) as locales,
  CAST('Onboardings Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
(SELECT
    a.*
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
  hr_contract_start__c <= current_date
  and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
  Month,
  Year,
  locales;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM hr_contract_end__c::date) as Month,
  EXTRACT(YEAR FROM hr_contract_end__c::date) as Year,
  MIN(hr_contract_end__c::date) as mindate,
  upper(left(delivery_areas__c,2)) as localea,
  CAST('Churn Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
(SELECT
    a.*
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE  (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
  hr_contract_end__c < current_date
  and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
  Month,
  Year,
  localea;
  


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM hr_contract_end__c::date) as Month,
  EXTRACT(YEAR FROM hr_contract_end__c::date) as Year,
  MIN(hr_contract_end__c::date) as mindate,
        case
  when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
  WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
  WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
  WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
  WHEN delivery_areas__c = 'ch-lucerne' then 'CH-Lucerne'
  when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
  when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
  when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
  when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city,
  CAST('Churn Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
(SELECT
    a.*
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
  hr_contract_end__c < current_date
  and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
  Month,
  Year,
  city;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM createddate::date) as Month,
  EXTRACT(YEAR FROM createddate::date) as Year,
  MIN(createddate::date) as mindate,
      case
  when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
  WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
  WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
  WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
  WHEN delivery_areas__c = 'ch-lucerne' then 'CH-Lucerne'
  when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
  when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
  when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
  when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city,
  CAST('Onboardings Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
  Salesforce.Account
WHERE
  createddate::date < current_date
  and LEFT(Locale__c,2) in ('at')
GROUP BY
  Month,
  Year,
  City;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM hr_contract_start__c::date) as Month,
  EXTRACT(YEAR FROM hr_contract_start__c::date) as Year,
  MIN(hr_contract_start__c::date) as mindate,
      case
  when delivery_areas__c = 'at-vienna' THEN 'AT-Vienna'
  WHEN delivery_areas__c = 'ch-zurich' THEN 'CH-Zurich'
  WHEN delivery_areas__c = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_areas__c = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_areas__c = 'ch-basel' THEN 'CH-Basel'
  WHEN delivery_areas__c = 'ch-lausanne' then 'CH-Lausanne'
  WHEN delivery_areas__c = 'ch-lucerne' then 'CH-Lucerne'
  when delivery_areas__c = 'ch-geneve' then 'CH-Geneva'
  when delivery_areas__c = 'de-berlin' THEN 'DE-Berlin'
  when delivery_areas__c = 'ch-stgallen' THEN 'CH-St.Gallen'
  when delivery_areas__c = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_areas__c = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_areas__c = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_areas__c = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_areas__c = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_areas__c = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_areas__c = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_areas__c = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_areas__c = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_areas__c = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_areas__c = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_areas__c = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_areas__c = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_areas__c = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_areas__c = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as city,
  CAST('Onboardings Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
  Salesforce.Account
WHERE
  hr_contract_start__c::date < current_date
  and LEFT(Locale__c,2) in ('ch')
GROUP BY
  Month,
  Year,
  City;


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as Month,
  EXTRACT(Year FROM mindate) as Year,
  mindate,
  city,
  kpi,
  CAST('-' as text) as breakdown,
  value
FROM
  bi.recurrent_kpis_monthly 
WHERE
  kpi in ('Existing Recurrent','New Recurrent','Recurrent Cancellations');


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  MIN(t2.date) as mindate,
  t1.delivery_areas__c as city,
  CAST('Churn Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  COUNT(DISTINCT(t1.sfid)) as value
FROM
  Salesforce.Account t1
JOIn
  bi.left_date t2
ON
  (t1.sfid = t2.sfid)
WHERE
  LEFT(locale__c,2) in ('at','ch')
GROUP BY
  Month,
  Year,
  city;

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  MIN(t2.date) as mindate,
  UPPER(LEFT(t1.delivery_areas__c,2)) as citya,
  CAST('Churn Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  COUNT(DISTINCT(t1.sfid)) as value
FROM
  Salesforce.Account t1
JOIn
  bi.left_date t2
ON
  (t1.sfid = t2.sfid)
WHERE
  LEFT(locale__c,2) in ('at','ch')
GROUP BY
  Month,
  Year,
  citya;

-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.topline_kpi
SELECT

  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  t2.city as city,
  CAST('Total Hours' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(t2.Order_Duration__c) as value
  
FROM

  bi.orders t2
  
WHERE

  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and t2.effectivedate < current_date
  and LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  AND effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year,
  City
  
ORDER BY
  
  year desc,
  month desc,
  city desc;

----------------------------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT

  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  UPPER(LEFT(t2.locale__c,2)) as citya,
  CAST('Total Hours' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(t2.Order_Duration__c) as value
  
FROM

  bi.orders t2
  
WHERE

  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and t2.effectivedate < current_date
  and LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  AND effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year,
  UPPER(LEFT(t2.locale__c,2))
  
ORDER BY
  
  year desc,
  month desc,
  UPPER(LEFT(t2.locale__c,2)) desc;

-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  t2.city as city,
  CAST('Hours per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and t2.effectivedate < current_date
  and (t1.type__c like  '%cleaning-b2c%' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like '%cleaning-b2b%')
  and LEFT(t2.locale__C,2) in ('de','nl')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  City;
  

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  UPPER(LEFT(t2.locale__c,2)) as citya,
  CAST('Hours per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and t2.effectivedate < current_date
  and (t1.type__c like  '%cleaning-b2c%' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like '%cleaning-b2b%')
  and LEFT(t2.locale__C,2) in ('de','nl')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  t2.city as city,
  CAST('Hours per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and t2.effectivedate < current_date
  and (t1.type__c like  '%cleaning-b2c%' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like '%cleaning-b2b%')
  and LEFT(t2.locale__C,2) in ('ch','at')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  City;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  UPPER(LEFT(t2.locale__c,2)) as citya,
  CAST('Hours per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and t2.effectivedate < current_date
  and (t1.type__c like  '%cleaning-b2c%' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like '%cleaning-b2b%')
  and LEFT(t2.locale__C,2) in ('ch','at')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

---------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  t2.city as city,
  CAST('Hours per Cleaner BAT' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  (SELECT
        t1.*,
        t2.name as subcon
      FROM
       Salesforce.Account t1
        JOIN
            Salesforce.Account t2
        ON
            (t2.sfid = t1.parentid)
    
        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name LIKE '%BAT Business Services GmbH%') t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  and t2.effectivedate < current_date
  AND t2.effectivedate >= '2018-01-01'

GROUP BY
  Month,
  Year,
  City;
  
---------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  t2.city as city,
  CAST('Hours per Cleaner Partners' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  (SELECT
        t1.*,
        t2.name as subcon
      FROM
       Salesforce.Account t1
        JOIN
            Salesforce.Account t2
        ON
            (t2.sfid = t1.parentid)
    
        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name NOT LIKE '%BAT Business Services GmbH%') t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  and t2.effectivedate < current_date
  AND t2.effectivedate >= '2018-01-01'

GROUP BY
  Month,
  Year,
  City;
 
-----------------------------------------------------------------------------------
---------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  UPPER(LEFT(t2.city, 2)) as city,
  CAST('Hours per Cleaner BAT' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  (SELECT
        t1.*,
        t2.name as subcon
      FROM
       Salesforce.Account t1
        JOIN
            Salesforce.Account t2
        ON
            (t2.sfid = t1.parentid)
    
        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name LIKE '%BAT Business Services GmbH%') t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  and t2.effectivedate < current_date
  AND t2.effectivedate >= '2018-01-01'

GROUP BY
  Month,
  Year,
  UPPER(LEFT(t2.city, 2));
  
---------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  MIN(effectivedate::date) as mindate,
  UPPER(LEFT(t2.city, 2)) as city,
  CAST('Hours per Cleaner Partners' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  (SELECT
        t1.*,
        t2.name as subcon
      FROM
       Salesforce.Account t1
        JOIN
            Salesforce.Account t2
        ON
            (t2.sfid = t1.parentid)
    
        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name NOT LIKE '%BAT Business Services GmbH%') t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  and LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  and t2.effectivedate < current_date
  AND t2.effectivedate >= '2018-01-01'

GROUP BY
  Month,
  Year,
  UPPER(LEFT(t2.city, 2));

 
 -----------------------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM current_date::date) as Month,
  EXTRACT(YEAR FROM current_date::date) as Year,
  MIN(current_date::date) as mindate,
  t2.city as city,
  CAST('GMV per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(GMV_eur)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status = 'INVOICED'
  and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
  and (t1.type__c like  '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
  and LEFT(t2.locale__C,2) in ('de','nl')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  City;
    
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM current_date::date) as Month,
  EXTRACT(YEAR FROM current_date::date) as Year,
  MIN(current_date::date) as mindate,
  t2.city as city,
  CAST('Hours per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status = 'INVOICED'
  and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
  and LEFT(t2.locale__C,2) in ('at','ch')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  City;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Invoiced Revenue' as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
        THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
       ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
      END
        
        
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
GROUP BY
  Month,
  Year,
  citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  upper(LEFT(locale__c,2)) as locale,
  'Invoiced Revenue' as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
      THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                  WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                  WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                  WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                  WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                  WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
     ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                  WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                  WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                  WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                  WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                  WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
    END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  locale;
    
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Invoiced Revenue' as kpi,
  CAST('Recurrent' as text) as breakdown,
   CASE WHEN min(effectivedate::date) < '2018-01-01'
        THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
       ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
      END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and recurrency__c > '6'
    and order_type = '1'
    AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  upper(LEFT(locale__c,2)) as locale,
  'Invoiced Revenue' as kpi,
  CAST('Recurrent' as text) as breakdown,
   CASE WHEN min(effectivedate::date) < '2018-01-01'
        THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
       ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
      END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and recurrency__c > '6'
  and  order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  locale;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Invoiced Revenue' as kpi,
  CAST('One-Off' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
        THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
       ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
      END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and (recurrency__c is null or recurrency__c = '0')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  upper(LEFT(locale__c,2)) as locale,
  'Invoiced Revenue' as kpi,
  CAST('One-Off' as text) as breakdown,
   CASE WHEN min(effectivedate::date) < '2018-01-01'
        THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
       ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
      END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and (recurrency__c is null or recurrency__c = '0')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  locale;


-- INSERT INTO bi.topline_kpi
-- SELECT
-- EXTRACT(MONTH FROM date::date) as Month,
-- EXTRACT(YEAR FROM date::date) as Year,
-- MIN(date::date) as date,
-- CASE
-- when polygon = 'at-vienna' THEN 'AT-Vienna'
-- WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
-- WHEN polygon = 'ch-bern' THEN 'CH-Bern'
-- WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
-- WHEN polygon = 'ch-basel' THEN 'CH-Basel'
-- WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
-- WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
-- when polygon = 'ch-geneve' then 'CH-Geneva'
-- when polygon = 'de-berlin' THEN 'DE-Berlin'
-- when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
-- when polygon = 'de-bonn' THEN 'DE-Bonn'
-- WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
-- WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
-- WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
-- WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
-- WHEN polygon = 'de-essen' THEN 'DE-Essen'
-- WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
-- WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
-- WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
-- WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
-- WHEN polygon = 'de-munich' THEN 'DE-Munich'
-- WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
-- WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
-- WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
-- WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
-- ELSE 'Other'
-- END as citya,
-- 'Invoiced Revenue' as kpi,
-- 'B2B' as breakdown,
-- SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END)
-- 
-- FROM
-- 
-- bi.b2borders
-- 
-- WHERE
-- 
-- date::date < current_date
-- 
-- GROUP BY
-- 
-- Month,
-- Year,
--   Citya;

-- ch1.
-- Date: 08/03/2018
-- Name: Chandra, Sylvain
-- Description: The below insert returns 'Net invoiced B2B revenue' group by city from orders table. It is same as 
--  the above insert but we released that in the above insert the way we calculate the net revenue is wrong and 
--    hence we changed the query to the below one. 

INSERT INTO bi.topline_kpi 
SELECT
  EXTRACT(MONTH FROM o.effectivedate) as month,
  EXTRACT(YEAR FROM o.effectivedate) as year,
  MIN(o.effectivedate::date) as date,
  CASE
    when polygon = 'at-vienna' THEN 'AT-Vienna'
    WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
    WHEN polygon = 'ch-bern' THEN 'CH-Bern'
    WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
    WHEN polygon = 'ch-basel' THEN 'CH-Basel'
    WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
    WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
    when polygon = 'ch-geneve' then 'CH-Geneva'
    when polygon = 'de-berlin' THEN 'DE-Berlin'
    when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
    when polygon = 'de-bonn' THEN 'DE-Bonn'
    WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
    WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
    WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
    WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
    WHEN polygon = 'de-essen' THEN 'DE-Essen'
    WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
    WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
    WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
    WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
    WHEN polygon = 'de-munich' THEN 'DE-Munich'
    WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
    WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
    WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
    WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
    ELSE 'Other'
  END as citya,
  'Invoiced Revenue' as kpi,
  'B2B' as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2b')
  AND effectivedate >= '2018-01-01'
  
GROUP BY
  month,
  year,
  citya;


-- INSERT INTO bi.topline_kpi
-- SELECT
-- 
-- EXTRACT(MONTH FROM date::date) as Month,
-- EXTRACT(YEAR FROM date::date) as Year,
-- MIN(date::date) as date,
-- UPPER(LEFT(locale__c,2)) as locale,
-- 'Invoiced Revenue' as kpi,
-- CAST('B2B' as text) as breakdown,
-- SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END)
-- 
-- FROM
-- 
-- bi.b2borders
-- 
-- WHERE
-- 
-- date::date < current_date
-- 
-- GROUP BY
-- 
-- Month,
-- Year,
-- locale
--   ;
  
  
-- ch2. 
-- Date: 08/03/2018
-- Name: Chandra, Sylvain
-- Description: The below insert returns 'Net invoiced B2B revenue' group by locale from orders table. It is same as 
--  the above insert but we released that in the above insert the way we calculate the net revenue is wrong and 
--    hence we changed the query to the below one. 
INSERT INTO bi.topline_kpi 
SELECT

  EXTRACT(MONTH FROM o.effectivedate) as month,
  EXTRACT(YEAR FROM o.effectivedate) as year,
  MIN(o.effectivedate::date) as date,
  UPPER(LEFT(locale__c,2)) as locale,
  'Invoiced Revenue' as kpi,
  CAST('B2B' as text) as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2b')
  AND effectivedate >= '2018-01-01'
  
GROUP BY

  month,
  year,
  locale;


 
-- INSERT INTO bi.topline_kpi
-- SELECT
-- 
-- EXTRACT(MONTH FROM date::date) as Month,
-- EXTRACT(YEAR FROM date::date) as Year,
-- MIN(date::date) as date,
-- 'GROUP' as locale,
-- 'Invoiced Revenue' as kpi,
-- CAST('B2B' as text) as breakdown,
-- SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END)
-- 
-- FROM
-- 
-- bi.b2borders
-- 
-- WHERE
-- 
-- date::date < current_date
-- 
-- GROUP BY
-- 
-- Month,
--   Year;
  
  
-- ch3.
-- Date: 08/03/2018
-- Name: Chandra, Sylvain
-- Description: The below insert returns 'Net invoiced B2B revenue' group by group from orders table. It is same as 
--  the above insert but we released that in the above insert the way we calculate the net revenue is wrong and 
--    hence we changed the query to the below one.
 
INSERT INTO bi.topline_kpi
SELECT

  EXTRACT(MONTH FROM o.effectivedate) as month,
  EXTRACT(YEAR FROM o.effectivedate) as year,
  MIN(o.effectivedate::date) as date,
  'GROUP' as locale,
  'Invoiced Revenue' as kpi,
  CAST('B2B' as text) as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2b')
  AND effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year;


-- INSERT INTO bi.topline_kpi
-- SELECT
-- EXTRACT(MONTH FROM effectivedate::date) as Month,
-- EXTRACT(YEAR FROM effectivedate::date) as Year,
-- min(effectivedate::date) as date,
-- case
-- when polygon = 'at-vienna' THEN 'AT-Vienna'
-- WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
-- WHEN polygon = 'ch-bern' THEN 'CH-Bern'
-- WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
-- WHEN polygon = 'ch-basel' THEN 'CH-Basel'
-- WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
-- WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
-- when polygon = 'ch-geneve' then 'CH-Geneva'
-- when polygon = 'de-berlin' THEN 'DE-Berlin'
-- when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
-- when polygon = 'de-bonn' THEN 'DE-Bonn'
-- WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
-- WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
-- WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
-- WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
-- WHEN polygon = 'de-essen' THEN 'DE-Essen'
-- WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
-- WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
-- WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
-- WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
-- WHEN polygon = 'de-munich' THEN 'DE-Munich'
-- WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
-- WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
-- WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
-- WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
-- ELSE 'Other'
-- END as citya,
-- 'Invoiced Revenue' as kpi,
-- CAST('B2C' as text) as breakdown,
-- CASE WHEN min(effectivedate::date) < '2018-01-01'
-- THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
-- WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06
-- WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
-- WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
-- WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
-- ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
-- WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06
-- WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
-- WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
-- WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
-- END
-- FROM
-- bi.orders
-- WHERE
-- test__c = '0'
-- and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
-- and (effectivedate::date < current_date)
-- and type != 'cleaning-b2b'
-- GROUP BY
-- Month,
-- Year,
--   Citya;
  
  
-- ch4.
-- Date: 08/03/2018
-- Name: Chandra, Sylvain
-- Description: The below insert returns 'Net invoiced B2C revenue' group by city from orders table. It is same as 
--  the above insert but we released that in the above insert the way we calculate the net revenue is wrong and 
--    hence we changed the query to the below one. 

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM o.effectivedate) as month,
  EXTRACT(YEAR FROM o.effectivedate) as year,
  MIN(o.effectivedate::date) as date,
  CASE
    when polygon = 'at-vienna' THEN 'AT-Vienna'
    WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
    WHEN polygon = 'ch-bern' THEN 'CH-Bern'
    WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
    WHEN polygon = 'ch-basel' THEN 'CH-Basel'
    WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
    WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
    when polygon = 'ch-geneve' then 'CH-Geneva'
    when polygon = 'de-berlin' THEN 'DE-Berlin'
    when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
    when polygon = 'de-bonn' THEN 'DE-Bonn'
    WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
    WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
    WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
    WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
    WHEN polygon = 'de-essen' THEN 'DE-Essen'
    WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
    WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
    WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
    WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
    WHEN polygon = 'de-munich' THEN 'DE-Munich'
    WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
    WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
    WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
    WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
    ELSE 'Other'
  END as citya,
  'Invoiced Revenue' as kpi,
  'B2C' as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2c')
  AND effectivedate >= '2018-01-01'
  
GROUP BY
  month,
  year,
  citya;



-- INSERT INTO bi.topline_kpi
-- SELECT
-- EXTRACT(MONTH FROM effectivedate::date) as Month,
-- EXTRACT(YEAR FROM effectivedate::date) as Year,
-- min(effectivedate::date) as date,
-- upper(LEFT(locale__c,2)) as locale,
-- 'Invoiced Revenue' as kpi,
-- CAST('B2C' as text) as breakdown,
-- CASE WHEN min(effectivedate::date) < '2018-01-01'
-- THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
-- WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06
-- WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
-- WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
-- WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
-- ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
-- WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06
-- WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
-- WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
-- WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
-- END
-- 
-- FROM
-- bi.orders
-- WHERE
-- test__c = '0'
-- and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
-- and (effectivedate::date < current_date)
-- and type != 'cleaning-b2b'
-- GROUP BY
-- Month,
-- Year,
--   locale;
  
  
-- ch5.
-- Date: 08/03/2018
-- Name: Chandra, Sylvain
-- Description: The below insert returns 'Net invoiced B2C revenue' group by locale from orders table. It is same as 
--  the above insert but we released that in the above insert the way we calculate the net revenue is wrong and 
--    hence we changed the query to the below one. 
INSERT INTO bi.topline_kpi
SELECT

  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  upper(LEFT(locale__c,2)) as locale,
  'Invoiced Revenue' as kpi,
  CAST('B2C' as text) as breakdown,
  SUM(o.gmv_eur_net) as revenue_net
  
FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2c')
  AND effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year,
  locale;



INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
  WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/  (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) != 'ch'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

  
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;
  
INSERT INTO bi.topline_kpi
SELECT

  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  MIN(date::date) as date,
  CASE
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'PPH' as kpi,
  CAST('B2B' as text) as breakdown,
  SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END)/SUM(t2.total_invoiced_hours) as PPH
  
FROM

  bi.b2borders t2
  
GROUP BY

  Month,
  Year,
  Citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(replace(city, '+', ''),2)) as citya,
  'PPH' as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;
  
INSERT INTO bi.topline_kpi
SELECT

  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  MIN(date::date) as date,
  UPPER(LEFT(polygon,2)) as citya,
  'PPH' as kpi,
  CAST('B2B' as text) as breakdown,
  SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END)/SUM(t2.total_invoiced_hours) as PPH
  
FROM

  bi.b2borders t2
  
GROUP BY

  Month,
  Year,
  Citya;


  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(Locale__c,2)) as citya,
  'PPH' as kpi,
  CAST('-' as text) as breakdown,
    CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
  WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/  (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value

FROM
  bi.orders t2
WHERE
  test__c = '0'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  LEFT(Locale__c,2);
  
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('Recurrent' as text) as breakdown,
  SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and acquisition_channel__c = 'recurrent'
  and t2.order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(Locale__c,2)) as citya,
  'PPH' as kpi,
  CAST('Recurrent' as text) as breakdown,
  SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and acquisition_channel__c = 'recurrent'
  and t2.order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  LEFT(Locale__c,2);  
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('One-Off' as text) as breakdown,
  SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM 
  bi.orders t2
WHERE 
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and acquisition_channel__c = 'web'
  and t2.order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(Locale__c,2)) as citya,
  'PPH' as kpi,
  CAST('One-Off' as text) as breakdown,
  SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and acquisition_channel__c = 'web'
  and t2.order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  LEFT(Locale__c,2);


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM current_date::date) as Month,
  EXTRACT(YEAR FROM current_date::date) as Year,
  MIN(current_date::date) as mindate,
  t2.city as city,
  CAST('GMV per Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(GMV_eur)/COUNT(DISTINCT(Professional__c)) as value
FROM
  Salesforce.Account t1
JOIN 
  bi.orders t2
ON
  (t1.sfid = t2.professional__c)
WHERE
  status = 'INVOICED'
  and effectivedate::date between current_date::date -  interval '31 days' and current_date::date -  interval '1 days'
  and LEFT(t2.locale__C,2) in ('at','ch')
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  City;
  
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN sum(revenue) >0 THEN SUM(gp)/sum(revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
    LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
  year,
  month,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  LEFT(year_month, 4)::numeric as Month,
  RIGHT(year_month,2)::numeric as Year,
  concat(LEFT(year_month, 4)::text,'-',RIGHT(year_month,2)::text,'-01')::date as date,
  case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
  WHEN delivery_Area = 'ch-zurich' THEN 'CH-Zurich'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2B Sub' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value
FROM
  bi.gpm_subcontractor
  
WHERE

  LEFT(year_month, 4) IS NOT NULL
  
GROUP BY
  year_month,
  year,
  month,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  LEFT(year_month, 4)::numeric as Month,
  RIGHT(year_month,2)::numeric as Year,
  concat(LEFT(year_month, 4)::text,'-',RIGHT(year_month,2)::text,'-01')::date as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('B2B Sub' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value
  
FROM

  bi.gpm_subcontractor
  
WHERE

  LEFT(year_month, 4) IS NOT NULL
  
GROUP BY

  year_month,
  year,
  month,
  localea;

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'ch-basel' THEN 'CH-Basel'
  when delivery_area = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_area = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_area = 'ch-lausanne' THEN 'CH-Lausanne'
  WHEN delivery_Area = 'ch-lucerne' THEN 'CH-Lucerne'
  WHEN delivery_Area = 'ch-stgallen' THEN 'CH-Stgallen'
  WHEN delivery_area = 'ch-zurich' THEN 'CH-Zurich'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(b2c_revenue) > 0 THEN sum(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
  
WHERE
  mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  citya;
  
 INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'ch-basel' THEN 'CH-Basel'
  when delivery_area = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_area = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_area = 'ch-lausanne' THEN 'CH-Lausanne'
  WHEN delivery_Area = 'ch-lucerne' THEN 'CH-Lucerne'
  WHEN delivery_Area = 'ch-stgallen' THEN 'CH-Stgallen'
  WHEN delivery_area = 'ch-zurich' THEN 'CH-Zurich'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2B' as text) as breakdown,
  CASE WHEN sum(b2b_revenue) > 0 THEN sum(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
  
WHERE
  mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(b2c_revenue) > 0 THEN sum(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
WHERE
  mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  localea;
  
 INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'ch-basel' THEN 'CH-Basel'
  when delivery_area = 'ch-bern' THEN 'CH-Bern'
  WHEN delivery_area = 'ch-geneva' THEN 'CH-Geneva'
  WHEN delivery_area = 'ch-lausanne' THEN 'CH-Lausanne'
  WHEN delivery_Area = 'ch-lucerne' THEN 'CH-Lucerne'
  WHEN delivery_Area = 'ch-stgallen' THEN 'CH-Stgallen'
  WHEN delivery_area = 'ch-zurich' THEN 'CH-Zurich'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
  
WHERE
  mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  citya;
 
  
  
 INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
WHERE
  mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  localea;
  
  INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('B2B' as text) as breakdown,
  CASE WHEN sum(b2b_revenue) > 0 THEN sum(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value

FROM

  bi.gpm_ch
WHERE
  mindate::Date >= '2017-01-01'

GROUP BY
  year,
  month,
  localea;
    
 

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  case
  when delivery_area = 'at-vienna' THEN 'AT-Vienna'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('AT' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_at

GROUP BY
  year,
  month,
  citya;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate::date) as Month,
  EXTRACT(YEAR FROM mindate::date) as Year,
  min(mindate::date) as date,
  UPPER(LEFT(delivery_area,2)) as localea,
  CAST('GPM %' as text) as kpi,
  CAST('AT' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN sum(gp)/sum(revenue) ELSE 0 END as value

FROM

  bi.gpm_at

GROUP BY
  year,
  month,
  localea;

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
    EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('B2B Revenue' as text) as kpi,
  CASE WHEN (t2.Subcon like '%BAT%' or t2.Subcon like '%BOOK%') THEN 'BAT Cleaner' ELSE 'Sub Cleaner' END as Typeaa,
  SUM(GMV_eur) as value
FROM
  bi.orders t1
LEFT JOIN
(SELECT
    a.*,
    t2.name as subcon
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as t2
ON
  (t1.professional__c = t2.sfid)
WHERE
  status not like '%CANCELLED%' and status not in ('INTERNAL ERROR')
  and order_type = '2'
  AND t1.effectivedate >= '2018-01-01'
GROUP BY
  year,
  month,
  citya,
  typeaa;

INSERT INTO bi.topline_kpi
SELECT
    EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(delivery_area,2)) as citya,
  CAST('B2B Revenue' as text) as kpi,
  CASE WHEN (t2.Subcon like '%BAT%' or t2.Subcon like '%BOOK%') THEN 'BAT Cleaner' ELSE 'Sub Cleaner' END as Typeaa,
  SUM(GMV_eur) as value
FROM
  bi.orders t1
LEFT JOIN
(SELECT
    a.*,
    t2.name as subcon
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as t2
ON
  (t1.professional__c = t2.sfid)
WHERE
  status not like '%CANCELLED%' and status not in ('INTERNAL ERROR')
  and order_type = '2'
  AND t1.effectivedate >= '2018-01-01'
GROUP BY
  year,
  month,
  citya,
  typeaa;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2B' as text) as breakdown,
  CASE WHEN sum(b2b_revenue) >0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
    LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
  year,
  month,
  citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_Area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(b2c_revenue) >0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
    LEFT(delivery_area,2) = 'nl' or LEFT(delivery_area,2) = 'de'
GROUP BY
  year,
  month,
  citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'DE' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  (CASE WHEN sum(revenue) > 0 THEN SUM(gp)/sum(revenue) ELSE 0 END) as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%de%'
GROUP BY
  year,
  month;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'DE' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%de%'
GROUP BY
  year,
  month;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'DE' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2B' as text) as breakdown,
  CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%de%'
GROUP BY
  year,
  month;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'NL' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN sum(revenue) > 0 THEN SUM(gp)/sum(revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%nl%'
GROUP BY
  year,
  month;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'NL' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2B' as text) as breakdown,
  CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%nl%'
GROUP BY
  year,
  month;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'NL' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%nl%'
GROUP BY
  year,
  month;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'AT-Vienna' as city,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  0.2 as value
FROM
  bi.gpm_city_over_time
GROUP BY
  year,
  month;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'AT-Vienna' as city,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  0.2 as value
FROM
  bi.gpm_city_over_time
GROUP BY
  year,
  month;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'AT' as city,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  0.2 as value
FROM
  bi.gpm_city_over_time
GROUP BY
  year,
  month;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'AT' as city,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  0.2 as value
FROM
  bi.gpm_city_over_time
GROUP BY
  year,
  month;


INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'CH' as city,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  
  CASE WHEN min(effectivedate::date) < '2018-01-01'
  THEN
    (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) 
  ELSE
      (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077) 
  END as value

FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) = 'ch'
  and effectivedate::date < '2017-01-01'
GROUP BY
  Month,
  Year
HAVING
  ((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN  Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;


INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'CH' as city,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
  THEN
    (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) 
  ELSE
      (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077) 
  END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) = 'ch'
  and order_type = '1'
  and effectivedate::date < '2017-01-01'
GROUP BY
  Month,
  Year
HAVING
  ((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN  Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;  

  
  
INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
  THEN
    (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) 
  ELSE
      (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077) 
  END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) = 'ch'
  and polygon in ('ch-geneva','ch-lausanne')
  and effectivedate::date < '2017-01-01'
GROUP BY
  Month,
  Year,
  citya
HAVING
  ((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN  Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;
  
  
INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
  THEN
    (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) 
  ELSE
      (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077)-25.03)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077) 
  END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) = 'ch'
  and polygon in ('ch-geneva','ch-lausanne')
  and order_type = '1'
  and effectivedate::date < '2017-01-01'
GROUP BY
  Month,
  Year,
  citya
HAVING
  ((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN  Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;  
  
INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
  THEN
    (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) 
  ELSE
      (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077) 
  END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) = 'ch'
  and polygon not in ('ch-geneva','ch-lausanne')
  and effectivedate::date < '2017-01-01'
GROUP BY
  Month,
  Year,
  citya
HAVING
  ((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN  Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;    

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
  THEN
    (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) 
  ELSE
      (((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077)-24.45)/((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
      SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b')THEN GMV_eur
    ELSE 0 END)/  (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.077) 
  END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) = 'ch'
  and polygon not in ('ch-geneva','ch-lausanne')
  and order_type = '1'
  and effectivedate::date < '2017-01-01'
GROUP BY
  Month,
  Year,
  citya
HAVING
  ((CASE WHEN (SUM(CASE WHEN type != 'cleaning-b2b' and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN GMV_eur
ELSE 0 END)/(SUM(CASE WHEN  Status in ('INVOICED','NOSHOW CUSTOMER') and (type != 'cleaning-b2b') THEN Order_Duration__c ELSE 0 END)) ELSE 0 END)/1.08) > 0;  

  -------------------

  INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  city,
  'M1 RR Recurrent' as kpi,
  CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  city,
  kpi,
  channel;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
      case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M1 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  city,
  kpi;


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
        case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M3 RR Recurrent' as kpi,
  CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  city,
  kpi,
  channel;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
        case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M3 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  city;

-----------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  UPPER(left(city,2)) as locale,
  'M1 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  locale;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  UPPER(left(city,2)) as locale,
  'M3 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  locale,
  kpi;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  UPPER(left(city,2)) as locale,
  'M6 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '6'
GROUP BY
  month,
  year,
  locale,
  kpi;
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
        case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M6 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '6'
GROUP BY
  month,
  year,
  city,
  kpi;

---------------------------------------------------------------------

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
      case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Utilization GPM Afternoon' as kpi,
  '-' as channel,
  CASE WHEN SUM(contract_hours_afternoon) > 0 THEN SUM(CASE WHEN capped_hours_afternoon > contract_hours_afternoon THEN contract_hours_afternoon ELSE capped_hours_afternoon END)/SUM(contract_hours_afternoon) ELSE 0 END as value
FROM
  bi.margin_per_cleaner
GROUP BY
  Month,
  year,
  delivery_area;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
      case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Utilization GPM Morning' as kpi,
  '-' as channel,
  CASE WHEN SUM(contract_morning_hours) > 0 THEN SUM(CASE WHEN capped_hours_morning > contract_morning_hours THEN contract_morning_hours ELSE capped_hours_morning END)/SUM(contract_morning_hours) ELSE 0 END as value
FROM
  bi.margin_per_cleaner
WHERE
  delivery_area like 'de%' or delivery_area like '%nl%'
GROUP BY
  Month,
  year,
  delivery_area;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
    case
  when delivery_area = 'de-berlin' THEN 'DE-Berlin'
  when delivery_area = 'de-bonn' THEN 'DE-Bonn'
  WHEN delivery_area = 'de-cologne' THEN 'DE-Cologne'
  WHEN delivery_area = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN delivery_Area = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN delivery_Area = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN delivery_area = 'de-essen' THEN 'DE-Essen'
  WHEN delivery_area = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN delivery_area = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN delivery_area = 'de-mainz' THEN 'DE-Mainz'
  WHEN delivery_area = 'de-manheim' THEN 'DE-Mannheim'
  WHEN delivery_area = 'de-munich' THEN 'DE-Munich'
  WHEN delivery_area = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN delivery_area = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN delivery_area = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN delivery_area = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Utilization GPM' as kpi,
  '-' as channel,
  CASE WHEN SUM(working_hours) > 0 THEN  SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
  bi.margin_per_cleaner
GROUP BY
  Month,
  year,
  delivery_area;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'DE-Total' as City,
  'Utilization GPM' as kpi,
  '-' as channel,
  CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
  bi.margin_per_cleaner
WHERE
  delivery_area like 'de%'
GROUP BY
  Month,
  year;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'DE' as citya,
  'Utilization GPM' as kpi,
  '-' as channel,
  CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
  bi.margin_per_cleaner
WHERE
  delivery_area like 'de%'
GROUP BY
  Month,
  year;


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'CH' as citya,
  'Utilization GPM' as kpi,
  '-' as channel,
  1 as Utilization
FROM
  bi.margin_per_cleaner
WHERE
  delivery_area like 'de%'
GROUP BY
  Month,
  year;
  

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM Effectivedate::date) as month,
  EXTRACT(YEAR FROM Effectivedate::date) as year,
  min(Effectivedate::date),
    case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Utilization GPM' as kpi,
  '-' as channel,
  1 as Utilization
FROM
  bi.orders
WHERE
  LEFT(polygon,2) = 'ch' 
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  year,
  polygon;
  
  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'NL' as citya,
  'Utilization GPM' as kpi,
  '-' as channel,
  CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
  bi.margin_per_cleaner
WHERE
  delivery_area like 'nl%'
GROUP BY
  Month,
  year;



INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'AT-Vienna'
  'Utilization GPM' as kpi,
  '-' as channel,
  1 as Utilization
FROM
  bi.margin_per_cleaner
GROUP BY
  Month,
  year,
  delivery_area;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'AT'
  'Utilization GPM' as kpi,
  '-' as channel,
  1 as Utilization
FROM
  bi.margin_per_cleaner

GROUP BY
  Month,
  year;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'CH'
  'Utilization GPM' as kpi,
  '-' as channel,
  1 as Utilization
FROM
  bi.margin_per_cleaner
GROUP BY
  Month,
  year;

-- GROUP KPI's


  
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  CAST('GROUP' as text) as locale,
  'PPH' as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
  WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/  (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year;

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'GROUP' as locale,
  CAST('M1 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '1'
GROUP BY
  month,
  year;
  
  
INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'GROUP' as locale,
  CAST('M3 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '3'
GROUP BY
  month,
  year;

INSERT INTO bi.topline_kpi    
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'GROUP' as locale,
  CAST('M6 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '6'
GROUP BY
  month,
  year;
  

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'GROUP',
  'M1 RR Recurrent' as kpi,
  '-' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '1'
GROUP BY
  month,
  year;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'GROUP',
  'M3 RR Recurrent' as kpi,
  '-' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'GROUP',
  'M6 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year;

INSERT INTO bi.topline_kpi  
SELECT
  EXTRACT(Month FROM Order_Creation__C::Date) as Month,
  EXTRACT(Year FROM Order_Creation__C::Date) as Year,
  MIN(Order_Creation__C::Date) as Mindate,
  'GROUP' as value,
  CAST('Booked GMV' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'GROUP' as text,
  'Invoiced Revenue' as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
       THEN
        SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
            WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
            WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
            WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
     ELSE
       SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
            WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
            WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
            WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
     END
        
        
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year;
  
-- INSERT INTO bi.topline_kpi
-- SELECT
-- EXTRACT(MONTH FROM effectivedate::date) as Month,
-- EXTRACT(YEAR FROM effectivedate::date) as Year,
-- min(effectivedate::date) as date,
-- 'GROUP' as text,
-- 'Invoiced Revenue' as kpi,
-- CAST('B2C' as text) as breakdown,
-- CASE WHEN min(effectivedate::date) < '2018-01-01'
-- THEN
-- SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06
-- WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
-- WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
-- WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
-- ELSE
-- SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
-- WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06
-- WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
-- WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
-- WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
-- END
-- FROM
-- bi.orders
-- WHERE
-- test__c = '0'
-- and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM')
-- and (effectivedate::date < current_date)
-- and order_type = '1'
-- GROUP BY
-- Month,
--   Year;
  
  
-- ch6.
-- Date: 08/03/2018
-- Name: Chandra, Sylvain
-- Description: The below insert returns 'Net invoiced B2C revenue' group by group from orders table. It is same as 
--  the above insert but we released that in the above insert the way we calculate the net revenue is wrong and 
--    hence we changed the query to the below one. 
INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'GROUP' as text,
  'Invoiced Revenue' as kpi,
  CAST('B2C' as text) as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2c')
  AND o.effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year;



INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'GROUP',
  'Invoiced Revenue' as kpi,
  CAST('One-Off' as text) as breakdown,
  CASE WHEN min(effectivedate::date) < '2018-01-01'
       THEN
        SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
            WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
            WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
            WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
     ELSE
       SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
            WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
            WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
            WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
            WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
     END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER')
  and (recurrency__c is null or recurrency__c = '0')
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year;

-------------------------------------------------------------------------

INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
       case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a LEFT JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      -- and a.type = 'cleaning-b2c'
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
        
    GROUP BY
       CSFID,
       StartDate,
       citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
     citya,
     kpi,
     breakdown

  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners Partners'::text as kpi,
    '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
       case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours,
       o.name
     
    FROM bi.orders a RIGHT JOIN 
   
   (SELECT

     t1.*,
     t2.name as subcon
     
    FROM
    
      Salesforce.Account t1
    
    JOIN
    
       Salesforce.Account t2
    ON
       
      (t2.sfid = t1.parentid)
    
    
    WHERE 
    
      t1.status__c not in ('SUSPENDED') 
      and t1.test__c = '0' 
      and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name NOT LIKE '%BAT Business Services GmbH%'
      ) o 
   
   on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and a.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
        
    GROUP BY
       CSFID,
       StartDate,
       citya,
       Cleaningdate,
       Status,
       EndDate,
     o.name) as t2

  GROUP BY
     month,
     year,
     citya,
     kpi,
     breakdown

  ORDER BY 
    year desc, month desc, mindate desc, citya asc;


--------------------------------------------------------------------------


INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners BAT'::text as kpi,
    '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
       case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours,
       o.name
     
    FROM bi.orders a RIGHT JOIN 
   
   (SELECT

     t1.*,
     t2.name as subcon
     
    FROM
    
      Salesforce.Account t1
    
    JOIN
    
       Salesforce.Account t2
    ON
       
      (t2.sfid = t1.parentid)
    
    
    WHERE 
    
      t1.status__c not in ('SUSPENDED') 
      and t1.test__c = '0' 
      and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name LIKE '%BAT Business Services GmbH%'
      ) o 
   
   on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and a.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
        
    GROUP BY
       CSFID,
       StartDate,
       citya,
       Cleaningdate,
       Status,
       EndDate,
     o.name) as t2

  GROUP BY
     month,
     year,
     citya,
     kpi,
     breakdown

  ORDER BY 
    year desc, month desc, mindate desc, citya asc;

---------------------------------------------------------------------------
---------------------------------------------------------------------------


INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    UPPER(LEFT(t2.citya, 2))::text as city,
    'Active cleaners Partners'::text as kpi,
    '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
       case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours,
       o.name
     
    FROM bi.orders a RIGHT JOIN 
   
   (SELECT

     t1.*,
     t2.name as subcon
     
    FROM
    
      Salesforce.Account t1
    
    JOIN
    
       Salesforce.Account t2
    ON
       
      (t2.sfid = t1.parentid)
    
    
    WHERE 
    
      t1.status__c not in ('SUSPENDED') 
      and t1.test__c = '0' 
      and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name NOT LIKE '%BAT Business Services GmbH%'
      ) o 
   
   on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and a.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
        
    GROUP BY
       CSFID,
       StartDate,
       citya,
       Cleaningdate,
       Status,
       EndDate,
     o.name) as t2

  GROUP BY
     month,
     year,
     UPPER(LEFT(t2.citya, 2)),
     kpi,
     breakdown

  ORDER BY 
    year desc, month desc, mindate desc;


--------------------------------------------------------------------------


INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
  MIN(Cleaningdate)::date as mindate,
  UPPER(LEFT(t2.citya, 2))::text as city,
  'Active cleaners BAT'::text as kpi,
  '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
       case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours,
       o.name
     
    FROM bi.orders a RIGHT JOIN 
   
   (SELECT

     t1.*,
     t2.name as subcon
     
    FROM
    
      Salesforce.Account t1
    
    JOIN
    
       Salesforce.Account t2
    ON
       
      (t2.sfid = t1.parentid)
    
    
    WHERE 
    
      t1.status__c not in ('SUSPENDED') 
      and t1.test__c = '0' 
      and t1.name not like '%test%'
      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
      and t2.name LIKE '%BAT Business Services GmbH%'
      ) o 
   
   on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and a.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
        
    GROUP BY
       CSFID,
       StartDate,
       citya,
       Cleaningdate,
       Status,
       EndDate,
     o.name) as t2

  GROUP BY
     month,
     year,
     UPPER(LEFT(t2.citya, 2)),
     kpi,
     breakdown

  ORDER BY 
    year desc, month desc, mindate desc;



---------------------------------------------------------------------------
---------------------------------------------------------------------------



INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
     'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
  SUM(HOURS)/COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
       case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
      and a.type = 'cleaning-b2c'
        
    GROUP BY
       CSFID,
       StartDate,
       citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
     citya,
     kpi,
     breakdown

  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
  (Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
  
  FROM

    (SELECT
     'DE' as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
      and a.type in ('cleaning-b2c','cleaning-b2b')
      and o.type__c like 'cleaning-b2c%'
      and left(a.locale__c,2) = 'de'
        
    GROUP BY
       CSFID,
       StartDate,
        citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
     citya
  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    UPPER(t2.citya::text) as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
    LEFT(a.locale__c,2) citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a LEFT JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and a.type in ('cleaning-b2c','cleaning-b2b')
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
      and (o.type__c like '%cleaning-b2c%' or o.type__c like '%cleaning-b2b%')
        and LEFT(a.locale__c,2) in ('de','nl')
    GROUP BY
       CSFID,
       StartDate,
        citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
    citya
  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;

INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Active cleaners'::text as kpi,
    '-'::text as breakdown,
  COUNT(DISTINCT(CSFID)) as value
  
  FROM

    (SELECT
    'CH' citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a LEFT JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
      and o.type__c != 'cleaning-b2c%'
        and left(a.locale__c,2) = 'ch'
    GROUP BY
       CSFID,
       StartDate,
        citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
    citya
  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;





INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
  (Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
  
  FROM

    (SELECT
     'NL' as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
       and effectivedate < current_date
      and a.type in ('cleaning-b2c','cleaning-b2b')
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
      and o.type__c like 'cleaning-b2c%'
      and left(a.locale__c,2) = 'nl'
        
    GROUP BY
       CSFID,
       StartDate,
        citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
     citya
  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi

SELECT
  EXTRACT(month from Cleaningdate)::int as month,
  EXTRACT(year from Cleaningdate)::int as year,
    MIN(Cleaningdate)::date as mindate,
    t2.citya::text as city,
    'Avg Hours p. cleaner'::text as kpi,
    '-'::text as breakdown,
  (Sum(Hours) / COUNT(DISTINCT(CSFID))) as value
  
  FROM

    (SELECT
     'CH' as citya,
       o.sfid as CSFID,
       o.hr_contract_start__c::date as StartDate,
       o.hr_contract_end__c::date as EndDate,
       a.effectivedate as CleaningDate,
       a.status as Status,
       sum(a.order_duration__c) as Hours
     
    FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)

    WHERE
       o.test__c = '0'
       and a.test__c = '0'
       and effectivedate >= '2018-01-01'
      and effectivedate < current_date
      and status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
      and o.type__c != 'cleaning-b2c%'
        and left(a.locale__c,2) = 'ch'
        
    GROUP BY
       CSFID,
       StartDate,
        citya,
       Cleaningdate,
       Status,
       EndDate) as t2

  GROUP BY
     month,
     year,
     citya
  ORDER BY 
    year desc, month desc, mindate desc, citya asc

;



INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date) as Month,
  EXTRACT(YEAR FROM Date) as year,
  min(date) as mindate,
      case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Marketing Costs'::text as kpi,
    '-'::text as breakdown,
    SUM(costs) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
  date >= '2018-01-01'
GROUP BY
    polygon,
    date) as a
GROUP By
  month,
  year,
    citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date) as Month,
  EXTRACT(YEAR FROM Date) as year,
  min(date) as mindate,
      case
  when polygon = 'at-vienna' THEN 'AT-Vienna'
  WHEN polygon = 'ch-zurich' THEN 'CH-Zurich'
  WHEN polygon = 'ch-bern' THEN 'CH-Bern'
  WHEN polygon = 'ch-geneva' THEN 'CH-Geneva'
  WHEN polygon = 'ch-basel' THEN 'CH-Basel'
  WHEN polygon = 'ch-lausanne' then 'CH-Lausanne'
  WHEN polygon = 'ch-lucerne' then 'CH-Lucerne'
  when polygon = 'ch-geneve' then 'CH-Geneva'
  when polygon = 'de-berlin' THEN 'DE-Berlin'
  when polygon = 'ch-stgallen' THEN 'CH-St.Gallen'
  when polygon = 'de-bonn' THEN 'DE-Bonn'
  WHEN polygon = 'de-cologne' THEN 'DE-Cologne'
  WHEN polygon = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN polygon = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN polygon = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN polygon = 'de-essen' THEN 'DE-Essen'
  WHEN polygon = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN polygon = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN polygon = 'de-mainz' THEN 'DE-Mainz'
  WHEN polygon = 'de-manheim' THEN 'DE-Mannheim'
  WHEN polygon = 'de-munich' THEN 'DE-Munich'
  WHEN polygon = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN polygon = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN polygon = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN polygon = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'Marketing Acquisitions'::text as kpi,
    '-'::text as breakdown,
    SUM(all_acq) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    sum(all_acq) as all_acq,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
  date >= '2019-01-01'
GROUP BY
    polygon,
    date) as a
GROUP By
  month,
  year,
    citya;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date) as Month,
  EXTRACT(YEAR FROM Date) as year,
  min(date) as mindate,
  UPPER(LEFT(polygon,2)) as locale,
  'Marketing Costs'::text as kpi,
    '-'::text as breakdown,
    SUM(costs) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
  date >= '2018-01-01'
GROUP BY
    polygon,
    date) as a
GROUP By
  month,
  year,
    locale;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date) as Month,
  EXTRACT(YEAR FROM Date) as year,
  min(date) as mindate,
  UPPER(LEFT(polygon,2)) as locale,
  'Marketing Acquisitions'::text as kpi,
    '-'::text as breakdown,
    SUM(all_acq) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    sum(all_acq) as all_acq,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
  date >= '2018-01-01'
GROUP BY
    polygon,
    date) as a
GROUP By
  month,
  year,
    locale;
    
   INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date) as Month,
  EXTRACT(YEAR FROM Date) as year,
  min(date) as mindate,
  'GROUP' as locale,
  'Marketing Costs'::text as kpi,
    '-'::text as breakdown,
    SUM(costs) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
  date >= '2018-01-01'
GROUP BY
    polygon,
    date) as a
GROUP By
  month,
  year;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM date) as Month,
  EXTRACT(YEAR FROM Date) as year,
  min(date) as mindate,
  'GROUP' as locale,
  'Marketing Acquisitions'::text as kpi,
    '-'::text as breakdown,
    SUM(all_acq) as value
FROm(
SELECT
    polygon,
    date,
    SUM(sem_cost+sem_discount) as sem_costs,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(facebook_cost+facebook_discount) as facebook_cost,
    sum(all_acq) as all_acq,
    SUM(sem_acq+sem_brand_acq+display_acq+facebook_acq+facebook_organic_acq+offline_acq+offline_acq+vouchers_acq+dti_acq+newsletter_acq+seo_acq+seo_brand_acq+youtube_acq+other_acq) as acquisitions
FROM
    bi.cpacalcpolygon
WHERE
  date >= '2018-01-01'
GROUP BY
    polygon,
    date) as a
GROUP By
  month,
  year;


INSERT INTO bi.topline_kpi
SELECT

  month::double precision,
  year::double precision,
  mindate::date,
  CASE 

  when CITY = 'at-vienna' THEN 'AT-Vienna'
    WHEN CITY = 'ch-zurich' THEN 'CH-Zurich'
    WHEN CITY = 'ch-bern' THEN 'CH-Bern'
    WHEN CITY = 'ch-geneva' THEN 'CH-Geneva'
    WHEN CITY = 'ch-basel' THEN 'CH-Basel'
    WHEN CITY = 'ch-lausanne' then 'CH-Lausanne'
    WHEN CITY = 'ch-lucerne' then 'CH-Lucerne'
    when CITY = 'ch-geneve' then 'CH-Geneva'
    when CITY = 'de-berlin' THEN 'DE-Berlin'
    when CITY = 'ch-stgallen' THEN 'CH-St.Gallen'
    when CITY = 'de-bonn' THEN 'DE-Bonn'
    WHEN CITY = 'de-cologne' THEN 'DE-Cologne'
    WHEN CITY = 'de-dortmund' THEN 'DE-Dortmund'
    WHEN CITY = 'de-duisburg' THEN 'DE-Duisburg'
    WHEN CITY = 'de-dusseldorf' THEN 'DE-Dusseldorf'
    WHEN CITY = 'de-essen' THEN 'DE-Essen'
    WHEN CITY = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
    WHEN CITY = 'de-hamburg' THEN 'DE-Hamburg'
    WHEN CITY = 'de-mainz' THEN 'DE-Mainz'
    WHEN CITY = 'de-manheim' THEN 'DE-Mannheim'
    WHEN CITY = 'de-munich' THEN 'DE-Munich'
    WHEN CITY = 'de-nuremberg' THEN 'DE-Nuremberg'
    WHEN CITY = 'de-stuttgart' THEN 'DE-Stuttgart'
    WHEN CITY = 'nl-amsterdam' THEN 'NL-Amsterdam'
    WHEN CITY = 'nl-hague' THEN 'NL-The Hague'
    WHEN CITY IN ('DE', 'NL', 'CH', 'AT') THEN CITY
    ELSE 'Other'
    END as city,
   kpi::text,
    breakdown::text,
    value::double precision


FROM

  bi.SummaryKPIs
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'
