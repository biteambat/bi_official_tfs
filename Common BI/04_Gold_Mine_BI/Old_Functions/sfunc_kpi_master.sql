

DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_kpi_master(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_kpi_master';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.kpi_master;
CREATE TABLE bi.kpi_master AS


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 04/04/2018
-- Short Description: Calculation of the revenue netto B2B

SELECT

	t1.date_part,
	MIN(t1.date) as date,
	t1.locale,
	t1.languages as languages,
	t1.city as city,
	t1.type,
	t1.kpi,
	t1.sub_kpi_1,
	t1.sub_kpi_2,
	t1.sub_kpi_3,
	t1.sub_kpi_4,
	t1.sub_kpi_5,
	SUM(t1.invoiced + t1.new_invoice + t1.gutschrift + t1.storno) as value
		
FROM
	
	(SELECT
	
		TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
		MIN(o.issued__c::date) as date,
		LEFT(o.locale__c,2) as locale,
		o.locale__c as languages,
		oooo.delivery_area__c as city,
		CAST('B2B' as varchar) as type,
		CAST('Revenue' as varchar) as kpi,
		CAST('Netto' as varchar) as sub_kpi_1,
		CAST('Monthly' as varchar) as sub_kpi_2,
		CAST('-' as varchar) as sub_kpi_3,
		CAST('-' as varchar) as sub_kpi_4,
		CAST('-' as varchar) as sub_kpi_5,
		o.sfid,
		o.name,
		MIN(CASE WHEN o.name LIKE '%G' THEN o.amount__c ELSE 0 END)/1.19 as gutschrift,
		MIN(CASE WHEN o.name LIKE '%S' THEN o.amount__c ELSE 0 END)/1.19 as storno,
		MIN(CASE WHEN o.name LIKE '%N' THEN o.amount__c ELSE 0 END)/1.19 as new_invoice,
		MIN(CASE WHEN (o.name NOT LIKE '%S' AND o.name NOT LIKE '%G' AND o.name NOT LIKE '%N') THEN o.amount__c ELSE 0 END)/1.19 as invoiced,
		MIN(o.amount__c)/1.19 as value
	
	FROM
	
		salesforce.invoice__c o
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON 
	
		o.opportunity__c = oo.opportunityid

	LEFT JOIN
				
		salesforce.opportunity oooo
		
	ON 
	
		o.opportunity__c = oooo.sfid
		
	WHERE
	
		o.opportunity__c IS NOT NULL
		AND o.professional__c IS NULL
		-- AND o.status__c = 'CLOSED'
		AND o.test__c IS FALSE
		AND o.issued__c IS NOT NULL
		AND o.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		date_part,
		locale,
		o.locale__c,
		oooo.delivery_area__c,
		-- o.opportunity__c,
		o.sfid,
		o.name
			
	ORDER BY
	
		date_part desc) as t1
		
GROUP BY

	t1.date_part,
	t1.locale,
	t1.languages,
	t1.type,
	t1.city,
	t1.kpi,
	t1.sub_kpi_1,
	t1.sub_kpi_2,
	t1.sub_kpi_3,
	t1.sub_kpi_4,
	t1.sub_kpi_5
	
ORDER BY

	t1.date_part desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 29/04/2019
-- Short Description: Calculation of the revenue netto B2B adjusted with the corrections made to the invoices afterwards

SELECT
		
	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	CASE WHEN t2.sub_kpi_3 IS NULL THEN 'Partner' ELSE t2.sub_kpi_3 END as sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5,
	-- t2.first_invoice,
	-- (t2.amount_first_invoice)/1.19 as amount_first_invoice,
	-- t2.original_invoice__c,
	-- t2.number_invoices,
	-- SUM(t2.final_amount_invoiced)/1.19 - (t2.amount_first_invoice)/1.19 as correction,
	SUM(t2.final_amount_invoiced)/1.19 as value

FROM
	
	(SELECT	
		
		TO_CHAR(t1.date,'YYYY-MM') as date_part,
		MIN(t1.date) as date,
		t1.locale,
		t1.languages as languages,
		t1.city as city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		
		t1.name_first_invoice,
		t1.opportunity__c,
	
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c,
		-- t1.name_second_invoice,
		-- MAX(t1.date_second_invoice) as date_last_invoice,
		SUM(t1.first_amount)/COUNT(t1.first_invoice) as amount_first_invoice,
		SUM(t1.first_amount) as sum_first_amount,
		SUM(t1.second_amount) as sum_second_amount,
		COUNT(t1.first_invoice) as number_invoices,
		CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
		     ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
			  END as final_amount_invoiced
		
	FROM
	
		(SELECT

			-- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
			MIN(o.issued__c::date) as date,
			LEFT(o.locale__c,2) as locale,
			o.locale__c as languages,
			oooo.delivery_area__c as city,
			CAST('B2B' as varchar) as type,
			CAST('Revenue Adjusted' as varchar) as kpi,
			CAST('Netto' as varchar) as sub_kpi_1,
			CAST('Monthly' as varchar) as sub_kpi_2,
			op.provider as sub_kpi_3,
			o.service_type__c as sub_kpi_4,
			CAST('-' as varchar) as sub_kpi_5,
			o.sfid as first_invoice,
			o.name as name_first_invoice,			
			o.opportunity__c,
			o.createddate as date_first_invoice,
			o.amount__c as first_amount,
			oo.original_invoice__c,
			oo.name as name_second_invoice,
			oo.createddate as date_second_invoice,
			oo.sfid as second_invoice,
			oo.amount__c as second_amount	
			
		FROM
			
			salesforce.invoice__c o
			
		LEFT JOIN
			
			salesforce.invoice__c oo
			
		ON
			
			o.sfid = oo.original_invoice__c
			
		LEFT JOIN
			
			salesforce.order ooo
			
		ON 
			
			o.opportunity__c = ooo.opportunityid

		LEFT JOIN
				
			salesforce.opportunity oooo
			
		ON 
		
			o.opportunity__c = oooo.sfid
			
		LEFT JOIN

			bi.order_provider op
			
		ON
		
			oooo.sfid = op.opportunityid
			
		WHERE
			
			o.opportunity__c IS NOT NULL
			AND o.test__c IS FALSE
			AND o.issued__c IS NOT NULL
			AND o.original_invoice__c IS NULL

			-- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
			
		GROUP BY
			
			-- TO_CHAR(o.issued__c,'YYYY-MM'),
			LEFT(o.locale__c,2),
			o.locale__c,
			oooo.delivery_area__c,
			o.name,
			o.opportunity__c,
			o.issued__c,
			o.createddate,
			o.sfid,
			o.amount__c,
			oo.original_invoice__c,
			oo.name,
			oo.createddate,
			oo.sfid,
			oo.amount__c,
			op.provider,
			o.service_type__c
			
		ORDER BY
		
			date desc) as t1
			
			
	GROUP BY
	
		TO_CHAR(t1.date,'YYYY-MM'),
		t1.locale,
		t1.languages,
		t1.city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		t1.name_first_invoice,
		t1.opportunity__c,
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c) as t2
		
		
GROUP BY

	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	t2.sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5
	-- t2.first_invoice,
	-- t2.amount_first_invoice,
	-- t2.number_invoices,
	-- t2.original_invoice__c
	
ORDER BY

	date_part desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 22/05/2019
-- Short Description: Calculation of the first invoicing round

SELECT
		
	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	t2.sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5,
	-- t2.first_invoice,
	-- (t2.amount_first_invoice)/1.19 as amount_first_invoice,
	-- t2.original_invoice__c,
	-- t2.number_invoices,
	SUM(t2.amount_first_invoice)/1.19 as value
	-- SUM(t2.final_amount_invoiced)/1.19 as value

FROM
	
	(SELECT	
		
		TO_CHAR(t1.date,'YYYY-MM') as date_part,
		MIN(t1.date) as date,
		t1.locale,
		t1.languages as languages,
		t1.city as city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		
		t1.name_first_invoice,
		t1.opportunity__c,
	
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c,
		-- t1.name_second_invoice,
		-- MAX(t1.date_second_invoice) as date_last_invoice,
		SUM(t1.first_amount)/COUNT(t1.first_invoice) as amount_first_invoice,
		SUM(t1.first_amount) as sum_first_amount,
		SUM(t1.second_amount) as sum_second_amount,
		COUNT(t1.first_invoice) as number_invoices,
		CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
		     ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
			  END as final_amount_invoiced
		
	FROM
	
		(SELECT

			-- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
			MIN(o.issued__c::date) as date,
			LEFT(o.locale__c,2) as locale,
			o.locale__c as languages,
			oooo.delivery_area__c as city,
			CAST('B2B' as varchar) as type,
			CAST('First Invoicing Round' as varchar) as kpi,
			CAST('Netto' as varchar) as sub_kpi_1,
			CAST('Monthly' as varchar) as sub_kpi_2,
			CAST('-' as varchar) as sub_kpi_3,
			CAST('-' as varchar) as sub_kpi_4,
			CAST('-' as varchar) as sub_kpi_5,
			o.sfid as first_invoice,
			o.name as name_first_invoice,
			o.opportunity__c,
			o.createddate as date_first_invoice,
			o.amount__c as first_amount,
			oo.original_invoice__c,
			oo.name as name_second_invoice,
			oo.createddate as date_second_invoice,
			oo.sfid as second_invoice,
			oo.amount__c as second_amount	
			
		FROM
			
			salesforce.invoice__c o
			
		LEFT JOIN
			
			salesforce.invoice__c oo
			
		ON
			
			o.sfid = oo.original_invoice__c
			
		LEFT JOIN
			
			salesforce.order ooo
			
		ON 
			
			o.opportunity__c = ooo.opportunityid

		LEFT JOIN
				
			salesforce.opportunity oooo
			
		ON 
		
			o.opportunity__c = oooo.sfid
			
		WHERE
			
			o.opportunity__c IS NOT NULL
			AND o.test__c IS FALSE
			AND o.issued__c IS NOT NULL
			AND o.original_invoice__c IS NULL
			-- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
			
		GROUP BY
			
			-- TO_CHAR(o.issued__c,'YYYY-MM'),
			LEFT(o.locale__c,2),
			o.locale__c,
			oooo.delivery_area__c,
			o.name,
			o.opportunity__c,
			o.issued__c,
			o.createddate,
			o.sfid,
			o.amount__c,
			oo.original_invoice__c,
			oo.name,
			oo.createddate,
			oo.sfid,
			oo.amount__c
			
		ORDER BY
		
			date desc) as t1
			
			
	GROUP BY
	
		TO_CHAR(t1.date,'YYYY-MM'),
		t1.locale,
		t1.languages,
		t1.city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		t1.name_first_invoice,
		t1.opportunity__c,
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c) as t2
		
		
GROUP BY

	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	t2.sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5
	-- t2.first_invoice,
	-- t2.amount_first_invoice,
	-- t2.number_invoices,
	-- t2.original_invoice__c
	
ORDER BY

	date_part desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 29/04/2019
-- Short Description: Calculation of the corrections made to the invoices afterwards

SELECT
		
	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	t2.sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5,
	-- t2.first_invoice,
	-- (t2.amount_first_invoice)/1.19 as amount_first_invoice,
	-- t2.original_invoice__c,
	-- t2.number_invoices,
	SUM(t2.final_amount_invoiced)/1.19 - SUM(t2.amount_first_invoice)/1.19 as value
	-- SUM(t2.final_amount_invoiced)/1.19 as value

FROM
	
	(SELECT	
		
		TO_CHAR(t1.date,'YYYY-MM') as date_part,
		MIN(t1.date) as date,
		t1.locale,
		t1.languages as languages,
		t1.city as city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		
		t1.name_first_invoice,
		t1.opportunity__c,
	
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c,
		-- t1.name_second_invoice,
		-- MAX(t1.date_second_invoice) as date_last_invoice,
		SUM(t1.first_amount)/COUNT(t1.first_invoice) as amount_first_invoice,
		SUM(t1.first_amount) as sum_first_amount,
		SUM(t1.second_amount) as sum_second_amount,
		COUNT(t1.first_invoice) as number_invoices,
		CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
		     ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
			  END as final_amount_invoiced
		
	FROM
	
		(SELECT

			-- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
			MIN(o.issued__c::date) as date,
			LEFT(o.locale__c,2) as locale,
			o.locale__c as languages,
			oooo.delivery_area__c as city,
			CAST('B2B' as varchar) as type,
			CAST('Correction Invoices' as varchar) as kpi,
			CAST('Netto' as varchar) as sub_kpi_1,
			CAST('Monthly' as varchar) as sub_kpi_2,
			CAST('-' as varchar) as sub_kpi_3,
			CAST('-' as varchar) as sub_kpi_4,
			CAST('-' as varchar) as sub_kpi_5,
			o.sfid as first_invoice,
			o.name as name_first_invoice,
			o.opportunity__c,
			o.createddate as date_first_invoice,
			o.amount__c as first_amount,
			oo.original_invoice__c,
			oo.name as name_second_invoice,
			oo.createddate as date_second_invoice,
			oo.sfid as second_invoice,
			oo.amount__c as second_amount	
			
		FROM
			
			salesforce.invoice__c o
			
		LEFT JOIN
			
			salesforce.invoice__c oo
			
		ON
			
			o.sfid = oo.original_invoice__c
			
		LEFT JOIN
			
			salesforce.order ooo
			
		ON 
			
			o.opportunity__c = ooo.opportunityid

		LEFT JOIN
				
			salesforce.opportunity oooo
			
		ON 
		
			o.opportunity__c = oooo.sfid
			
		WHERE
			
			o.opportunity__c IS NOT NULL
			AND o.test__c IS FALSE
			AND o.issued__c IS NOT NULL
			AND o.original_invoice__c IS NULL
			-- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
			
		GROUP BY
			
			-- TO_CHAR(o.issued__c,'YYYY-MM'),
			LEFT(o.locale__c,2),
			o.locale__c,
			oooo.delivery_area__c,
			o.name,
			o.opportunity__c,
			o.issued__c,
			o.createddate,
			o.sfid,
			o.amount__c,
			oo.original_invoice__c,
			oo.name,
			oo.createddate,
			oo.sfid,
			oo.amount__c
			
		ORDER BY
		
			date desc) as t1
			
			
	GROUP BY
	
		TO_CHAR(t1.date,'YYYY-MM'),
		t1.locale,
		t1.languages,
		t1.city,
		t1.type,
		t1.kpi,
		t1.sub_kpi_1,
		t1.sub_kpi_2,
		t1.sub_kpi_3,
		t1.sub_kpi_4,
		t1.sub_kpi_5,
		t1.name_first_invoice,
		t1.opportunity__c,
		t1.date_first_invoice,
		t1.first_invoice,
		t1.first_amount,
		t1.original_invoice__c) as t2
		
		
GROUP BY

	t2.date_part,
	t2.date,
	t2.locale,
	t2.languages,
	t2.city,
	t2.type,
	t2.kpi,
	t2.sub_kpi_1,
	t2.sub_kpi_2,
	t2.sub_kpi_3,
	t2.sub_kpi_4,
	t2.sub_kpi_5
	-- t2.first_invoice,
	-- t2.amount_first_invoice,
	-- t2.number_invoices,
	-- t2.original_invoice__c
	
ORDER BY

	date_part desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 04/04/2018
-- Short Description: Calculation of the revenue netto B2C

SELECT

	TO_CHAR(o.effectivedate,'YYYY-MM') as date_part,
	MIN(o.effectivedate::date) as date,
	LEFT(o.locale__c,2) as locale,
	o.locale__c as languages,
	oo.delivery_area__c as city,
	CAST('B2C' as varchar) as type,
	CAST('Revenue' as varchar) as kpi,
	CAST('Netto' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,

	SUM(o.gmv_eur_net) as value

FROM

	bi.orders o

LEFT JOIN
				
	salesforce.opportunity oo
	
ON 

	o.opportunityid = oo.sfid
	
WHERE

	o.effectivedate < current_date
	AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND o.type IN ('cleaning-b2c')
	
GROUP BY

	date_part,
	o.polygon,
	locale,
	languages,
	oo.delivery_area__c,
	type,
	kpi,
	sub_kpi_1,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
	
ORDER BY

	date_part desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of the opportunities churned
	
	
SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_week,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	-- CAST('-' as varchar) as city,
	table1.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Count opps' as varchar) as sub_kpi_1,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
			  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
			  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
			  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
			  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
			  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
			  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
			  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
			  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
			  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
			  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
			  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
			  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
			  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
			  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
			  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
			  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
			  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
			  ELSE '>M18'
			  END as sub_kpi_2,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
		  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
		  ELSE 'Unlimited'
		  END as sub_kpi_3,
		  
	CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
	CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
	COUNT(DISTINCT table1.opportunityid) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		oo.delivery_area__c,
		o.opportunityid,
		ooo.potential as grand_total,
		'last_order' as type_date,
		MIN(o.effectivedate) as date_start,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunityid = ooo.opportunityid

	LEFT JOIN

		salesforce.contract__c cont
	
	ON 

		oo.sfid = cont.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		AND o.test__c IS FALSE
		AND o.professional__c IS NOT NULL
		AND cont.service_type__c = 'maintenance cleaning'
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		oo.delivery_area__c,
		type_date,
		grand_total,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	table1.country,
	table1.locale__c,
	table1.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;
	

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of the opportunities revenue churned		
		
SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	-- CAST('-' as varchar) as city,
	table1.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
			  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
			  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
			  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
			  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
			  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
			  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
			  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
			  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
			  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
			  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
			  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
			  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
			  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
			  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
			  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
			  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
			  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
			  ELSE '>M18'
			  END as sub_kpi_2,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
		  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
		  ELSE 'Unlimited'
		  END as sub_kpi_3,
		  
	CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  WHEN table1.grand_total IS NULL THEN 'Unknown'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
	CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  WHEN table1.grand_total IS NULL THEN 'Unknown'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
	-- table1.opportunityid,
	SUM(table1.grand_total) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		oo.delivery_area__c,
		o.opportunityid,
		ooo.potential as grand_total,
		'last_order' as type_date,
		MIN(o.effectivedate) as date_start,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunityid = ooo.opportunityid

	LEFT JOIN

		salesforce.contract__c cont
	
	ON 

		oo.sfid = cont.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.test__c IS FALSE
		AND o.professional__c IS NOT NULL
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
		AND cont.service_type__c = 'maintenance cleaning'
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		oo.delivery_area__c,
		ooo.potential,
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	-- CASE WHEN oooo.grand_total__c IS NULL THEN oooo.pph__c*4.3*oooo.hours_weekly__c ELSE oooo.grand_total__c END,
	table1.country,
	table1.locale__c,
	table1.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	-- table1.opportunityid
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of the running opportunities		
			
		
SELECT

	year_month,
	date,
	-- max_date,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,	
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5,
	SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value

FROM	
	
	(SELECT	
	
		table1.year_month as year_month,
		MIN(table1.ymd) as date,
		table1.ymd_max as max_date,
		
		table1.country as locale,
		table1.locale__c as languages,
		table1.polygon as city,
		CAST('B2B' as varchar) as type,
		CAST('Running Opps' as varchar) as kpi,
		CAST('Count' as varchar) as sub_kpi_1,	
		CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
			  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
			  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
			  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
			  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
			  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
			  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
			  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
			  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
			  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
			  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
			  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
			  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
			  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
			  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
			  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
			  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
			  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
			  ELSE '>M18'
			  END as sub_kpi_2,
		CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
		  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
		  ELSE 'Unlimited'
		  END as sub_kpi_3,
		CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
		CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
		table1.opportunityid,
		table1.category
		-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
		
	FROM
		
		(SELECT
		
			t2.*,
			o.status__c as status_now,
			ooo.potential as grand_total
		
		FROM
			
			(SELECT
				
					time_table.*,
					table_dates.*,
					CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
				
				FROM	
					
					(SELECT
						
						'1'::integer as key,	
						TO_CHAR(i, 'YYYY-MM') as year_month,
						MIN(i) as ymd,
						MAX(i) as ymd_max
						-- i::date as date 
						
					FROM
					
						generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
						
					GROUP BY
					
						key,
						year_month
						-- date
						
					ORDER BY 
					
						year_month desc) as time_table
						
				LEFT JOIN
				
					(SELECT
					
						'1'::integer as key_link,
						t1.country,
						t1.locale__c,
						t1.delivery_area__c as polygon,
						t1.opportunityid,
						t1.date_start,
						TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
						CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
						CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
						
					
					FROM
						
						((SELECT
		
							LEFT(o.locale__c, 2) as country,
							o.locale__c,
							o.opportunityid,
							oo.delivery_area__c,
							MIN(o.effectivedate) as date_start
						
						FROM
						
							salesforce.order o

						LEFT JOIN
				
							salesforce.opportunity oo
							
						ON 
						
							o.opportunityid = oo.sfid
							
						WHERE
						
							o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
							AND o.type = 'cleaning-b2b'
							AND o.professional__c IS NOT NULL
							AND o.test__c IS FALSE
							
						GROUP BY
						
							o.opportunityid,
							o.locale__c,
							oo.delivery_area__c,
							LEFT(o.locale__c, 2))) as t1
							
					LEFT JOIN
					
						(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
						         -- It's the last day on which they are making money
							LEFT(o.locale__c, 2) as country,
							o.opportunityid,
							'last_order' as type_date,
							MAX(o.effectivedate) as date_churn
							
						FROM
						
							salesforce.order o
							
						LEFT JOIN
						
							salesforce.opportunity oo
							
						ON 
						
							o.opportunityid = oo.sfid

						LEFT JOIN

							salesforce.contract__c cont
						
						ON 

							oo.sfid = cont.opportunity__c
							
						WHERE
						
							o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
							AND oo.status__c IN ('RESIGNED', 'CANCELLED')
							AND o.type = 'cleaning-b2b'
							AND o.professional__c IS NOT NULL
							AND o.test__c IS FALSE
							AND oo.test__c IS FALSE
							AND cont.service_type__c = 'maintenance cleaning'
						
						GROUP BY
						
							LEFT(o.locale__c, 2),
							type_date,
							o.opportunityid) as t2
							
					ON
					
						t1.opportunityid = t2.opportunityid) as table_dates
						
				ON 
				
					time_table.key = table_dates.key_link
					
			) as t2
				
		LEFT JOIN
		
			salesforce.opportunity o
			
		ON
		
			t2.opportunityid = o.sfid

		LEFT JOIN

			bi.potential_revenue_per_opp ooo

		ON

			t2.opportunityid = ooo.opportunityid

		LEFT JOIN

			salesforce.contract__c cont
		
		ON 

			o.sfid = cont.opportunity__c

		WHERE 

			o.test__c IS FALSE
			AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
		
				ooo.potential,
				o.status__c,
				t2.key,	
				t2.year_month,
				t2.ymd,
				t2.ymd_max,
				t2.key_link,
				t2.country,
				t2.locale__c,
				t2.polygon,
				t2.opportunityid,
				t2.date_start,
				t2.year_month_start,
				t2.date_churn,
				t2.year_month_churn,
				t2.category
			) as table1

	WHERE

		table1.opportunityid IS NOT NULL
			
	GROUP BY
	
		table1.year_month,
		table1.ymd_max,
		table1.country,
		LEFT(table1.locale__c, 2),
		table1.locale__c,
		table1.polygon,
		CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
			  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
			  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
			  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
			  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
			  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
			  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
			  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
			  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
			  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
			  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
			  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
			  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
			  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
			  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
			  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
			  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
			  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
			  ELSE '>M18'
			  END,
			  sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table1.category,
		table1.opportunityid
		
	ORDER BY
	
		table1.year_month desc)	 as table2
		
GROUP BY

	year_month,
	date,
	-- max_date,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,	
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
ORDER BY 

	year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 04/06/2018
-- Short Description: Calculation of the running opportunities revenue					
			
	
SELECT

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	-- table3.opportunityid,
	SUM(CASE WHEN table3.value > 0 THEN table3.grand_total ELSE 0 END) as value

FROM
	
	(SELECT
	
		year_month,
		date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid,
		SUM(table2.grand_total) as grand_total,
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps' as varchar) as kpi,
			CAST('Revenue' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.category,
			table1.grand_total
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				ooo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								oo.delivery_area__c,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o

							LEFT JOIN
				
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid

							LEFT JOIN

								salesforce.contract__c cont
							
							ON 

								oo.sfid = cont.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND cont.service_type__c = 'maintenance cleaning'
								
							GROUP BY
							
								o.opportunityid,
								o.locale__c,
								oo.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid

							LEFT JOIN

								salesforce.contract__c cont
							
							ON 

								oo.sfid = cont.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND oo.test__c IS FALSE
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND cont.service_type__c = 'maintenance cleaning'
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid
				
			LEFT JOIN
			
				salesforce.contract__c oo
				
			ON
			
				t2.opportunityid = oo.opportunity__c

			LEFT JOIN

				bi.potential_revenue_per_opp ooo

			ON
				
				t2.opportunityid = ooo.opportunityid

			LEFT JOIN

				salesforce.contract__c cont
			
			ON 

				oo.sfid = cont.opportunity__c
	
			WHERE
	
				o.test__c IS FALSE
				AND cont.service_type__c = 'maintenance cleaning'
				-- AND oo.status__c IN ('ACCEPTED', 'SIGNED')
				
			GROUP BY
			
					ooo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category
				) as table1
				
		GROUP BY
		
			table1.year_month,
			table1.ymd_max,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid,
			table1.grand_total
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	WHERE
	
		-- table2.opportunityid = '0060J00000qCkNQQA0'
	
		table2.category = 'RUNNING'
			
	GROUP BY
	
		year_month,
		date,
		max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid
		
	ORDER BY 
	
		year_month desc) as table3
		
GROUP BY

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5
	-- table3.opportunityid
	
ORDER BY

	table3.year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 28/11/2018
-- Short Description: Calculation of the offboarding/retention opportunities revenue			

SELECT

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	-- table3.opportunityid,
	SUM(CASE WHEN table3.value > 0 THEN table3.grand_total ELSE 0 END) as value

FROM
	
	(SELECT
	
		year_month,
		date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid,
		SUM(table2.grand_total) as grand_total,
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Offboarding/Retention' as varchar) as kpi,
			CAST('Revenue' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			table1.status_now as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.category,
			table1.grand_total
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				ooo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								oo.delivery_area__c,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o

							LEFT JOIN
				
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid

							LEFT JOIN

								salesforce.contract__c cont
							
							ON 

								oo.sfid = cont.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND cont.service_type__c = 'maintenance cleaning'
								
							GROUP BY
							
								o.opportunityid,
								o.locale__c,
								oo.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid

							LEFT JOIN

								salesforce.contract__c cont
							
							ON 

								oo.sfid = cont.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND oo.test__c IS FALSE
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND cont.service_type__c = 'maintenance cleaning'
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid
				
			LEFT JOIN
			
				salesforce.contract__c oo
				
			ON
			
				t2.opportunityid = oo.opportunity__c

			LEFT JOIN

				bi.potential_revenue_per_opp ooo

			ON
				
				t2.opportunityid = ooo.opportunityid
	
			WHERE
	
				o.test__c IS FALSE
				AND o.status__c IN ('OFFBOARDING', 'RETENTION')
				AND oo.service_type__c = 'maintenance cleaning'
				-- AND oo.active__c IS TRUE 
				-- AND oo.status__c IN ('ACCEPTED', 'SIGNED')
				
			GROUP BY
			
					ooo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category
				) as table1
				
		GROUP BY
		
			table1.year_month,
			table1.ymd_max,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid,
			table1.grand_total
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	WHERE
	
		-- table2.opportunityid = '0060J00000qCkNQQA0'
	
		table2.category = 'RUNNING'
			
	GROUP BY
	
		year_month,
		date,
		max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid
		
	ORDER BY 
	
		year_month desc) as table3
		
GROUP BY

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5
	-- table3.opportunityid
	
ORDER BY

	table3.year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of young churn. We consider an opportunity as young if its active period is below 42 days (6 weeks of orders planned)


SELECT

	t2.year_month_churn,
	MIN(t2.mindate) as mindate,
	t2.country as locale,
	t2.locale__c as languages,
	t2.city as city,
	CAST('B2B' as varchar) as type,
	CAST('Young churn' as varchar) as kpi,
	CAST('Opps' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.young_churn) as value

FROM

	(SELECT
	
		t1.year_month_churn,
		MIN(t1.date_churn) as mindate,
		t1.country,
		t1.locale__c,
		t1.city,
		-- t1.year_month_churn,
		-- t1.date_churn,
		COUNT(DISTINCT t1.new_opps) as new_opps,	
		SUM(CASE WHEN t1.churn_opps IS NOT NULL THEN 1 ELSE 0 END) as opps_churned,
		SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN 1 ELSE 0 END) as young_churn	
	
	FROM	
		
		(SELECT
		
			t1.*
		
		FROM
		
			(SELECT
			
				list_start.year_month as year_month_start,
				list_start.date_start,
				list_start.country,
				list_start.locale__c,
				list_start.city,
				list_start.opportunityid as new_opps,
				list_churn.opportunityid as churn_opps,
				list_churn.year_month as year_month_churn,
				list_churn.date_churn
			
			FROM
				
				(SELECT
		
					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					oo.delivery_area__c as city,
					MIN(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					o.opportunityid,
					MIN(o.effectivedate)::timestamp as date_start
				
				FROM
				
					salesforce.order o

				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid

				LEFT JOIN

					salesforce.contract__c cont
				
				ON 

					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
					
				GROUP BY
				
					o.opportunityid,
					o.locale__c,
					oo.delivery_area__c,
					LEFT(o.locale__c, 2)) as list_start
					
			LEFT JOIN
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
					LEFT(oo.locale__c, 2) as country,
					o.opportunityid,
					MAX(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					MAX(o.effectivedate)::timestamp as date_churn
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid

				LEFT JOIN

					salesforce.contract__c cont
				
				ON 

					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND oo.test__c IS FALSE
					AND o.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
				
				GROUP BY
				
					LEFT(oo.locale__c, 2),
					o.opportunityid) as list_churn
					
			ON
			
				list_start.opportunityid = list_churn.opportunityid) as t1
				
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			t1.new_opps = oo.sfid
			) as t1
			
	GROUP BY
	
		-- year_month_start,
		year_month_churn,
		t1.country,
		t1.locale__c,
		t1.city
		
	ORDER BY
	
		year_month_churn desc) as t2
		
WHERE

	t2.year_month_churn IS NOT NULL
		
GROUP BY

	t2.year_month_churn,
	t2.country,
	t2.locale__c,
	t2.city
	
ORDER BY

	t2.year_month_churn desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 15/05/2018 - Updated on 28/05/2019
-- Short Description: Calculation of churn before start	

SELECT

	TO_CHAR(list_churn.confirmed_end__c,'YYYY-WW') as date_part,
	MIN(list_churn.confirmed_end__c::date) as date,
	LEFT(list_churn.locale__c, 2) as locale,
	list_churn.locale__c as languages,
	list_churn.delivery_area__c as city,
	'B2B' as type,	
	'Churn' as kpi,
	'Before start'  as sub_kpi_1,
	'Weekly' as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT list_churn.sfid) as value

FROM


	(SELECT
	
		o.sfid,
		o.name,
		o.locale__c,
		o.delivery_area__c,
		o.grand_total__c,
		-- ooo.amount__c,
		oo.start__c,
		oo.duration__c,
		oo.end__c,
		oo.resignation_date__c,
		oo.confirmed_end__c
	
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.sfid = oo.opportunity__c
		
	LEFT JOIN
	
		salesforce.invoice__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
	
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND oo.service_type__c = 'maintenance cleaning'
		AND ooo.service_type__c = 'maintenance cleaning'
		-- AND ooo.issued__c::text IS NULL
		-- AND ooo.issued__c::text = '2019-04-30'
		-- AND LEFT(oo.confirmed_end__c::text, 7) = '2019-05'
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.grand_total__c,
		o.locale__c,
		o.delivery_area__c,
		-- ooo.amount__c,
		oo.start__c,
		oo.duration__c,
		oo.end__c,
		oo.resignation_date__c,
		oo.confirmed_end__c) as list_churn
		
LEFT JOIN

	(SELECT

		o.opportunityid,
		MIN(o.effectivedate) as first_order
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.test__c IS FALSE
		AND o."type" = 'cleaning-b2b'
		AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'CANCELLED CUSTOMER')
		AND o.opportunityid IS NOT NULL
		
	GROUP BY
	
		o.opportunityid) as dates
		
ON

	list_churn.sfid = dates.opportunityid
	
WHERE

	dates.first_order IS NULL
	
GROUP BY

	TO_CHAR(list_churn.confirmed_end__c,'YYYY-WW'),
	LEFT(list_churn.locale__c, 2),
	list_churn.locale__c,
	list_churn.delivery_area__c;
	
	

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master		

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of young churn revenue. We consider an opportunity as young if its active period is below 42 days (6 weeks of orders planned)


SELECT

	t2.year_month_churn,
	MIN(t2.mindate) as mindate,
	t2.country as locale,
	t2.locale__c as languages,
	t2.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Young churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.young_revenue_churn) as value

FROM

	
	(SELECT
	
		t1.year_month_churn,
		MIN(t1.date_churn) as mindate,
		t1.country,
		t1.locale__c,
		t1.delivery_area__c,
		-- t1.year_month_churn,
		-- t1.date_churn,
		COUNT(DISTINCT t1.new_opps) as new_opps,	
		SUM(CASE WHEN t1.churn_opps IS NOT NULL THEN 1 ELSE 0 END) as opps_churned,
		SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN 1 ELSE 0 END) as young_churn,	
		SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN t1.churned_revenue ELSE 0 END) as young_revenue_churn
	
	FROM	
		
		(SELECT
		
			t1.*
		
		FROM
		
			(SELECT
			
				list_start.year_month as year_month_start,
				list_start.date_start,
				list_start.country,
				list_start.locale__c,
				list_start.delivery_area__c,
				list_start.opportunityid as new_opps,
				list_churn.opportunityid as churn_opps,
				list_churn.revenue as churned_revenue,
				list_churn.year_month as year_month_churn,
				list_churn.date_churn
			
			FROM
				
				(SELECT
		
					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					ooo.delivery_area__c,
					MIN(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					o.opportunityid,
					MIN(o.effectivedate)::timestamp as date_start
				
				FROM
				
					salesforce.order o

				LEFT JOIN

					salesforce.opportunity ooo

				ON
					o.opportunityid = ooo.sfid

				LEFT JOIN

					salesforce.contract__c cont
				
				ON 

					ooo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
					
				GROUP BY
				
					o.opportunityid,
					o.locale__c,
					ooo.delivery_area__c,
					LEFT(o.locale__c, 2)) as list_start
					
			LEFT JOIN
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
					LEFT(oo.locale__c, 2) as country,
					o.opportunityid,
					ooo.potential as revenue,
					MAX(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					MAX(o.effectivedate)::timestamp as date_churn
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid

				LEFT JOIN	

					bi.potential_revenue_per_opp ooo

				ON

					o.opportunityid = ooo.opportunityid

				LEFT JOIN

					salesforce.contract__c cont
				
				ON 

					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND oo.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
				
				GROUP BY
				
					LEFT(oo.locale__c, 2),
					ooo.potential,
					o.opportunityid) as list_churn
					
			ON
			
				list_start.opportunityid = list_churn.opportunityid) as t1
				
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			t1.new_opps = oo.sfid
			) as t1
			
	GROUP BY
	
		-- year_month_start,
		year_month_churn,
		t1.country,
		t1.delivery_area__c,
		t1.locale__c
		
	ORDER BY
	
		year_month_churn desc) as t2
		
WHERE

	t2.year_month_churn IS NOT NULL
		
GROUP BY

	t2.year_month_churn,
	t2.country,
	t2.delivery_area__c,
	t2.locale__c
	
ORDER BY

	t2.year_month_churn desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of new opps signed in Sales, based on their closedate and taking into account the stages ('WRITTEN CONFIRMATION', 'WON','PENDING')


SELECT

	TO_CHAR(t2.closedate, 'YYYY-MM') as year_month_signed,
	MIN(t2.closedate) as mindate,
	LEFT(t2.locale__c, 2) as locale,
	t2.locale__c as languages,
	t2.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Count' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Signed Sales' as varchar) as sub_kpi_3,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT t2.sfid) as value

FROM

	salesforce.opportunity t2
	
LEFT JOIN

	salesforce.order o
	
ON

	t2.sfid = o.opportunityid

LEFT JOIN

	salesforce.contract__c cont

ON 

	t2.sfid = cont.opportunity__c
	
WHERE

	t2.test__c IS FALSE
	-- AND t2.stagename IN ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
	AND t2.stagename IN ('WRITTEN CONFIRMATION', 'WON','PENDING')
	AND t2.closed_by__c IS NOT NULL
	AND cont.service_type__c = 'maintenance cleaning'
	
GROUP BY

	year_month_signed,
	locale,
	languages,
	city,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END
	
ORDER BY

	year_month_signed desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of opps revenue signed in Sales, based on their closedate and taking into account the stages ('WRITTEN CONFIRMATION', 'WON','PENDING')
	
SELECT

	t3.year_month_signed,
	MIN(t3.mindate) as mindate,
	t3.locale,
	t3.languages,
	t3.city,
	t3.type,
	t3.kpi,
	t3.sub_kpi_1,
	t3.sub_kpi_2,
	t3.sub_kpi_3,
	t3.sub_kpi_4,
	t3.sub_kpi_5,
	SUM(t3.revenue) as value


FROM	
	
	(SELECT
	
		TO_CHAR(t2.closedate, 'YYYY-MM') as year_month_signed,
		MIN(t2.closedate) as mindate,
		LEFT(t2.locale__c, 2) as locale,
		t2.locale__c as languages,
		t2.delivery_area__c as city,
		CAST('B2B' as varchar) as type,
		CAST('New' as varchar) as kpi,
		CAST('Revenue' as varchar) as sub_kpi_1,
		CAST('Monthly' as varchar) as sub_kpi_2,
		CAST('Opps Signed Sales' as varchar) as sub_kpi_3,
		CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
		CAST('-' as varchar) as sub_kpi_5,
		t2.sfid,
		CASE WHEN t2.grand_total__c IS NULL THEN t2.amount ELSE t2.grand_total__c END as revenue
	
	FROM
	
		salesforce.opportunity t2
		
	LEFT JOIN
	
		salesforce.order o
		
	ON
	
		t2.sfid = o.opportunityid
		
	LEFT JOIN
	
		salesforce.contract__c cont
		
	ON
	
		t2.sfid = cont.opportunity__c

	WHERE
	
		t2.test__c IS FALSE
		-- AND t2.stagename IN ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
		AND t2.stagename IN ('WRITTEN CONFIRMATION', 'WON','PENDING')
		AND cont.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		year_month_signed,
		locale,
		languages,
		city,
		CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END,
		t2.sfid,
		CASE WHEN t2.grand_total__c IS NULL THEN t2.amount ELSE t2.grand_total__c END
		
	ORDER BY
	
		year_month_signed desc) as t3
		
GROUP BY
	
	t3.year_month_signed,
	t3.locale,
	t3.languages,
	t3.city,
	t3.type,
	t3.kpi,
	t3.sub_kpi_1,
	t3.sub_kpi_2,
	t3.sub_kpi_3,
	t3.sub_kpi_4,
	t3.sub_kpi_5
	
ORDER BY

	t3.year_month_signed desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of new opps, based on their first valid order

SELECT

	TO_CHAR(t1.first_order, 'YYYY-MM') as year_month,
	MIN(t1.first_order) as mindate,
	LEFT(t1.locale__c, 2) as locale,
	t1.locale__c as languages,
	t1.delivery_area__c,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Count' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Started' as varchar) as sub_kpi_3,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT t1.sfid) as value


FROM

	
	(SELECT -- This first query is building a table showing the first order date for every opportunity
				
		o.sfid,
		o.name,
		o.locale__c,
		o.email__c,
		o.delivery_area__c,
		o.acquisition_channel__c,
		MIN(oo.effectivedate) as first_order
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	LEFT JOIN
	
		salesforce.contract__c cont
		
	ON
	
		o.sfid = cont.opportunity__c
		
	WHERE
	
		-- o.status__c = 'RUNNING'
		o.test__c IS FALSE
		AND oo.test__c IS FALSE
		AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.professional__C IS NOT NULL
		AND cont.service_type__c = 'maintenance cleaning'
		
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.locale__c,
		o.delivery_area__c,
		o.acquisition_channel__c,
		o.email__c) as t1
	
GROUP BY

	TO_CHAR(t1.first_order, 'YYYY-MM'),
	LEFT(t1.locale__c, 2),
	t1.locale__c,
	t1.delivery_area__c,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of new opps revenue, based on their first valid order

SELECT

	TO_CHAR(t1.first_order, 'YYYY-MM') as year_month,
	MIN(t1.first_order) as mindate,
	LEFT(t1.locale__c, 2) as locale,
	t1.locale__c as languages,
	t1.delivery_area__c,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Started' as varchar) as sub_kpi_3,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t1.potential) as value


FROM

	
	(SELECT -- This first query is building a table showing the first order date for every opportunity
				
		o.sfid,
		o.name,
		o.locale__c,
		o.email__c,
		o.delivery_area__c,
		o.acquisition_channel__c,
		MIN(oo.effectivedate) as first_order,
		ooo.potential
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	LEFT JOIN

		bi.potential_revenue_per_opp ooo

	ON

		o.sfid = ooo.opportunityid
		
	LEFT JOIN
	
		salesforce.contract__c cont
		
	ON
	
		o.sfid = cont.opportunity__c
		
	WHERE
	
		-- o.status__c = 'RUNNING'
		o.test__c IS FALSE
		AND oo.test__c IS FALSE
		AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.professional__C IS NOT NULL
		AND cont.service_type__c = 'maintenance cleaning'
		
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.locale__c,
		o.delivery_area__c,
		o.acquisition_channel__c,
		o.email__c,
		ooo.potential) as t1
	
GROUP BY

	TO_CHAR(t1.first_order, 'YYYY-MM'),
	LEFT(t1.locale__c, 2),
	t1.locale__c,
	t1.delivery_area__c,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END
	
ORDER BY

	TO_CHAR(t1.first_order, 'YYYY-MM') desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of new opps signed before, month closedate < month first valid order

SELECT

	t2.year_month,
	MIN(t2.mindate) as mindate,
	LEFT(t2.locale__c, 2) as locale,
	t2.locale__c as languages,
	t2.delivery_area__c,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Count' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Signed Before' as varchar) as sub_kpi_3,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.signed_before) as value	
		
FROM
		
	(SELECT
	
		TO_CHAR(t1.first_order, 'YYYY-MM') as year_month,
		MIN(t1.first_order) as mindate,
		t1.sfid,
		t1.name,
		t1.locale__c,
		t1.email__c,
		t1.delivery_area__c,
		t1.acquisition_channel__c,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') < TO_CHAR(t1.first_order,'YYYY-MM') THEN 1 ELSE 0 END) as signed_before,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') = TO_CHAR(t1.first_order,'YYYY-MM') THEN 1 ELSE 0 END) as signed_same_month
		
		
	FROM
		
		(SELECT -- This first query is building a table showing the first order date for every opportunity
						
			o.sfid,
			o.name,
			o.locale__c,
			o.email__c,
			o.delivery_area__c,
			o.acquisition_channel__c,
			o.closedate,
			MIN(oo.effectivedate) as first_order,
			ooo.potential
		
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp ooo
		
		ON
		
			o.sfid = ooo.opportunityid
			
		LEFT JOIN
	
			salesforce.contract__c cont
		
		ON
	
			o.sfid = cont.opportunity__c
			
		WHERE
		
			-- o.status__c = 'RUNNING'
			o.test__c IS FALSE
			AND oo.test__c IS FALSE
			AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.professional__C IS NOT NULL
			AND cont.service_type__c = 'maintenance cleaning'
			
			
		GROUP BY
		
			o.sfid,
			o.name,
			o.locale__c,
			o.delivery_area__c,
			o.acquisition_channel__c,
			o.closedate,
			o.email__c,
			ooo.potential) as t1
			
	GROUP BY
	
		TO_CHAR(t1.first_order, 'YYYY-MM'),
		t1.sfid,
		t1.name,
		t1.locale__c,
		t1.email__c,
		t1.delivery_area__c,
		t1.acquisition_channel__c) as t2
		
GROUP BY

	t2.year_month,
	LEFT(t2.locale__c, 2),
	t2.locale__c,
	t2.delivery_area__c,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END
	
ORDER BY

	t2.year_month desc,
	LEFT(t2.locale__c, 2),
	t2.locale__c,
	t2.delivery_area__c,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of new revenue signed before, month closedate < month first valid order	
		
SELECT

	t2.year_month,
	MIN(t2.mindate) as mindate,
	LEFT(t2.locale__c, 2) as locale,
	t2.locale__c as languages,
	t2.delivery_area__c,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Signed Before' as varchar) as sub_kpi_3,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.signed_before) as value	
		
FROM
		
	(SELECT
	
		TO_CHAR(t1.first_order, 'YYYY-MM') as year_month,
		MIN(t1.first_order) as mindate,
		t1.sfid,
		t1.name,
		t1.locale__c,
		t1.email__c,
		t1.delivery_area__c,
		t1.acquisition_channel__c,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') < TO_CHAR(t1.first_order,'YYYY-MM') THEN t1.potential ELSE 0 END) as signed_before,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') = TO_CHAR(t1.first_order,'YYYY-MM') THEN t1.potential ELSE 0 END) as signed_same_month
		
		
	FROM
		
		(SELECT -- This first query is building a table showing the first order date for every opportunity
						
			o.sfid,
			o.name,
			o.locale__c,
			o.email__c,
			o.delivery_area__c,
			o.acquisition_channel__c,
			o.closedate,
			MIN(oo.effectivedate) as first_order,
			ooo.potential
		
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp ooo
		
		ON
		
			o.sfid = ooo.opportunityid
			
		LEFT JOIN
	
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c
			
		WHERE
		
			-- o.status__c = 'RUNNING'
			o.test__c IS FALSE
			AND oo.test__c IS FALSE
			AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.professional__C IS NOT NULL
			AND cont.service_type__c = 'maintenance cleaning'
			
			
		GROUP BY
		
			o.sfid,
			o.name,
			o.locale__c,
			o.delivery_area__c,
			o.acquisition_channel__c,
			o.closedate,
			o.email__c,
			ooo.potential) as t1
			
	GROUP BY
	
		TO_CHAR(t1.first_order, 'YYYY-MM'),
		t1.sfid,
		t1.name,
		t1.locale__c,
		t1.email__c,
		t1.delivery_area__c,
		t1.acquisition_channel__c) as t2
		
GROUP BY

	t2.year_month,
	LEFT(t2.locale__c, 2),
	t2.locale__c,
	t2.delivery_area__c,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END
	
ORDER BY

	t2.year_month desc,
	LEFT(t2.locale__c, 2),
	t2.locale__c,
	t2.delivery_area__c,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/05/2018
-- Short Description: Calculation of new opps signed same month, month closedate = month first valid order

SELECT

	t2.year_month_start,
	MIN(t2.mindate) as mindate,
	t2.country as locale,
	t2.locale__c as languages,
	t2.city as city,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Count' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('Opps Signed Same Month' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.signed_same_month) as value

FROM

	(SELECT
	
		t1.year_month_start,
		MIN(t1.date_start) as mindate,
		t1.country,
		t1.locale__c,
		t1.city,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') < TO_CHAR(t1.date_start,'YYYY-MM') THEN 1 ELSE 0 END) as signed_before,
		SUM(CASE WHEN TO_CHAR(t1.closedate,'YYYY-MM') = TO_CHAR(t1.date_start,'YYYY-MM') THEN 1 ELSE 0 END) as signed_same_month
	
	FROM	
		
		(SELECT
		
			t1.*
		
		FROM
		
			(SELECT
			
				list_start.year_month as year_month_start,
				list_start.date_start,
				list_start.closedate,
				list_start.country,
				list_start.locale__c,
				list_start.city,
				list_start.opportunityid as new_opps,
				list_churn.opportunityid as churn_opps,
				list_churn.year_month as year_month_churn,
				list_churn.date_churn			
			
			FROM
				
				(SELECT
		
					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					oo.delivery_area__c as city,
					MIN(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					o.opportunityid,
					oo.closedate,
					MIN(o.effectivedate)::timestamp as date_start
				
				FROM
				
					salesforce.order o
				
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				LEFT JOIN
	
					salesforce.contract__c cont
					
				ON
				
					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND oo.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
					
				GROUP BY
				
					o.opportunityid,
					oo.closedate,
					o.locale__c,
					oo.delivery_area__c,
					LEFT(o.locale__c, 2)) as list_start
					
			LEFT JOIN
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION', 'CANCELLED CUSTOMER') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
					LEFT(oo.locale__c, 2) as country,
					o.opportunityid,
					oo.closedate,
					MAX(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					MAX(o.effectivedate)::timestamp as date_churn
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
				
				LEFT JOIN
	
					salesforce.contract__c cont
					
				ON
				
					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND oo.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
				
				GROUP BY
				
					LEFT(oo.locale__c, 2),
					o.opportunityid,
					oo.closedate) as list_churn
					
			ON
			
				list_start.opportunityid = list_churn.opportunityid) as t1
				
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			t1.new_opps = oo.sfid
			) as t1
			
	GROUP BY
	
		year_month_start,
		-- year_month_churn,
		t1.country,
		t1.locale__c,
		t1.city
		
	ORDER BY
	
		year_month_start desc) as t2
		
WHERE

	t2.year_month_start IS NOT NULL
		
GROUP BY

	t2.year_month_start,
	t2.country,
	t2.locale__c,
	t2.city
	
ORDER BY

	t2.year_month_start desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 29/05/2018
-- Short Description: Calculation of new opps revenue, based on their first valid order

SELECT

	t2.year_month_start,
	MIN(t2.mindate) as mindate,
	t2.country as locale,
	t2.locale__c as languages,
	t2.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.new_revenue) as value

FROM

	
	(SELECT
	
		t1.year_month_start,
		MIN(t1.date_start) as mindate,
		t1.country,
		t1.locale__c,
		t1.delivery_area__c,
		COUNT(DISTINCT t1.new_opps) as new_opps,	
		SUM(t1.new_revenue) as new_revenue
	
	FROM	
		
		(SELECT
		
			t1.*
		
		FROM
		
			(SELECT
			
				list_start.year_month as year_month_start,
				list_start.date_start,
				list_start.country,
				list_start.locale__c,
				list_start.delivery_area__c,
				list_start.opportunityid as new_opps,
				list_start.revenue as new_revenue,
				list_churn.opportunityid as churn_opps,
				list_churn.revenue as churned_revenue,
				list_churn.year_month as year_month_churn,
				list_churn.date_churn
			
			FROM
				
				(SELECT
		
					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					oo.delivery_area__c,
					MIN(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					o.opportunityid,
					ooo.potential as revenue,
					MIN(o.effectivedate)::timestamp as date_start
				
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid

				LEFT JOIN

					bi.potential_revenue_per_opp ooo

				ON

					o.opportunityid = ooo.opportunityid
					
				LEFT JOIN
	
					salesforce.contract__c cont
					
				ON
				
					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND oo.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
					
				GROUP BY
				
					o.opportunityid,
					ooo.potential,
					o.locale__c,
					oo.delivery_area__c,
					LEFT(o.locale__c, 2)) as list_start
					
			LEFT JOIN
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
					LEFT(oo.locale__c, 2) as country,
					o.opportunityid,
					MAX(CASE WHEN oo.amount IS NULL then oo.grand_total__c ELSE oo.amount END) as revenue,
					MAX(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					MAX(o.effectivedate)::timestamp as date_churn
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				LEFT JOIN
	
					salesforce.contract__c cont
					
				ON
				
					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND oo.test__c IS FALSE
					AND o.test__c is FALSE
					AND cont.service_type__c = 'maintenance cleaning'
				
				GROUP BY
				
					LEFT(oo.locale__c, 2),
					o.opportunityid) as list_churn
					
			ON
			
				list_start.opportunityid = list_churn.opportunityid) as t1
				
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			t1.new_opps = oo.sfid
			
		LEFT JOIN
	
			salesforce.contract__c cont
			
		ON
		
			oo.sfid = cont.opportunity__c
			
		WHERE
		
			cont.service_type__c = 'maintenance cleaning'
			) as t1
			
	GROUP BY
	
		year_month_start,
		t1.country,
		t1.delivery_area__c,
		t1.locale__c
		
	ORDER BY
	
		year_month_start desc) as t2
		
WHERE

	t2.year_month_start IS NOT NULL
		
GROUP BY

	t2.year_month_start,
	t2.country,
	t2.delivery_area__c,
	t2.locale__c
	
ORDER BY

	t2.year_month_start desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 31/05/2018
-- Short Description: Calculation of B2B leads generated based on their created date

SELECT

	TO_CHAR(t1.createddate,'YYYY-MM') as date_part,
	MIN(t1.createddate::date) as date,
	LEFT(t1.locale__c,2) as locale,
	t1.locale__c as languages,
	CAST('-' as varchar) as city,
	CAST('B2B' as varchar) as type,
	CAST('Lead' as varchar) as kpi,
	CAST('Count' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,

	COUNT(DISTINCT t1.sfid) as value

FROM

	salesforce.likeli__c t1
	
WHERE

	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	AND t1.test__c IS FALSE
	AND t1.company_name__c NOT LIKE '%bookatiger%'
	AND t1.company_name__c NOT LIKE '%test%'

GROUP BY

	date_part,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
	
ORDER BY

	date_part desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 25/06/2018
-- Short Description: Calculation of the delay between closedate and first order

SELECT

	t2.date_part,
	MIN(t2.date) as date,
	t2.locale,
	t2.languages,
	t2.city,
	CAST('B2B' as varchar) as type,
	CAST('Delay Start' as varchar) as kpi,
	t2.delay as sub_kpi_1,
	CAST('' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT t2.sfid) as value

FROM
	
	(SELECT
	
		TO_CHAR(t1.closedate, 'YYYY-MM') as date_part,
		MIN(t1.closedate) as date,
		t1.locale,
		t1.languages,
		t1.city,
		t1.starting_date,
		t1.sfid,
		EXTRACT(MONTH FROM t1.starting_date) as month_start,
		EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed as age,
		CASE WHEN t1.closedate <= t1.starting_date THEN  
						CASE WHEN (EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = 0 THEN 'M+0'
			  			WHEN ((EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = 1 OR (EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = -11) THEN 'M+1'
			  			WHEN ((EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = 2 OR (EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = -10) THEN 'M+2'
			  			WHEN ((EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = 3 OR (EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed) = -9) THEN 'M+3'
			  			ELSE 'Later'
			  			END
			  ELSE 
			  		'Corner Cases'
			  END
			  as delay	
	FROM	
	
		(SELECT
		
			MIN(o.closedate) as closedate,
			MIN(oo.effectivedate) as starting_date,	
			EXTRACT(MONTH FROM o.closedate) as month_closed,
			o.sfid,
			LEFT(oo.locale__c, 2) as locale,
			oo.locale__c as languages,
			o.delivery_area__c as city
			
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN	
		
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c 
			
		WHERE
		
			oo.opportunityid IS NOT NULL
			AND o.stagename IN ('WRITTEN CONFIRMATION', 'PENDING', 'WON')
			AND oo.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.test__c IS FALSE
			AND o.test__c IS FALSE
			AND oo.recurrency__c > 0
			AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
		
			month_closed,
			o.sfid,
			locale,
			languages,
			o.delivery_area__c) as t1
			
	GROUP BY
	
		TO_CHAR(t1.closedate, 'YYYY-MM'),
		t1.closedate,
		t1.starting_date,
		EXTRACT(MONTH FROM t1.starting_date),
	   EXTRACT(MONTH FROM t1.starting_date) - t1.month_closed,
		t1.locale,
		t1.languages,
		t1.city,
		t1.sfid) as t2
		
GROUP BY

	t2.date_part,
	t2.locale,
	t2.languages,
	t2.city,
	t2.delay
	
ORDER BY	
	
	t2.date_part desc,
	t2.locale,
	t2.languages,
	t2.city,
	t2.delay;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/07/2018
-- Short Description: Insertion of current pph partners

SELECT

	'2099-01' as year_month,
	'2099-01-01'::date as mindate,
	LEFT(a.locale__c, 2) as locale,
	a.locale__c as languages,
	a.delivery_areas__c as city,
	'B2B' as type,
	'PPH' as kpi,
	'Partner' as sub_kpi_1,
	a.company_name__c as sub_kpi_2,
	'-' as sub_kpi_3,
	'-' as sub_kpi_4,
	'-' as sub_kpi_5,

	a.pph__c as value
	
FROM

	salesforce.account a
	
LEFT JOIN

	salesforce.account b
	
ON 

	a.sfid = b.parentid
	
WHERE

	-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
	a.type__c = 'partner'
	AND a.type__c NOT LIKE 'master'
	AND LOWER(a.company_name__c) NOT LIKE '%test%'
	AND LOWER(a.name) NOT LIKE '%test%'
	AND a.test__c IS FALSE
	AND b.test__c IS FALSE
	AND a.company_name__c NOT LIKE '%BAT Business Services GmbH'
	
GROUP BY	

	year_month,
	mindate,
	LEFT(a.locale__c, 2),
	languages,
	city,
	a.type,
	kpi,
	sub_kpi_1,
	a.company_name__c,
	a.pph__c;
			
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 30/07/2018
-- Short Description: Insertion of number employees partners

SELECT

	'2099-01' as year_month,
	'2099-01-01'::date as mindate,
	LEFT(a.locale__c, 2) as locale,
	a.locale__c as languages,
	a.delivery_areas__c as city,
	'B2B' as type,
	'Employees' as kpi,
	'Partner' as sub_kpi_1,
	a.company_name__c as sub_kpi_2,
	'-' as sub_kpi_3,
	'-' as sub_kpi_4,
	'-' as sub_kpi_5,

	a.numberofemployees as value
	
FROM

	salesforce.account a
	
LEFT JOIN

	salesforce.account b
	
ON 

	a.sfid = b.parentid
	
WHERE

	-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
	a.type__c = 'partner'
	AND a.type__c NOT LIKE 'master'
	AND LOWER(a.company_name__c) NOT LIKE '%test%'
	AND LOWER(a.name) NOT LIKE '%test%'
	AND a.test__c IS FALSE
	AND b.test__c IS FALSE
	AND a.company_name__c NOT LIKE '%BAT Business Services GmbH'
	
GROUP BY	

	year_month,
	mindate,
	LEFT(a.locale__c, 2),
	languages,
	city,
	a.type,
	kpi,
	sub_kpi_1,
	a.company_name__c,
	a.numberofemployees;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 01/08/2018
-- Short Description: Insertion of revenue brought by partners

SELECT

	TO_CHAR(o.issued__c, 'YYYY-MM') as year_month,
	MIN(o.issued__c) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	ooo.delivery_area__c as city,
	'B2B' as type,
	'Revenue' as kpi,
	'Partner' as sub_kpi_1,
	oo.company_name as sub_kpi_2,
	oo.opportunity as sub_kpi_3,
	o.service_type__c as sub_kpi_4,
	'-' as sub_kpi_5,
	MAX(o.amount__c)/1.19 as value
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	bi.order_provider oo
	
ON

	o.opportunity__c = oo.opportunityid

LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.opportunity__c = ooo.sfid
	
WHERE

	oo.provider = 'Partner'
	AND o.test__C IS FALSE
	-- AND o.service_type__c = 'maintenance cleaning'
	AND o.name NOT LIKE '%3359487-112%' -- big mistake in invoicing, it fucks up the results
	AND oo.company_name NOT LIKE '%BAT Business Services GmbH' 
	
GROUP BY

	year_month,
	LEFT(o.locale__c, 2),
	o.locale__c,
	ooo.delivery_area__c,
	oo.company_name,
	oo.opportunity,
	o.service_type__c
	
ORDER BY

	year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 17/06/2019
-- Short Description: Insertion of revenue brought by BAT

SELECT

	TO_CHAR(o.issued__c, 'YYYY-MM') as year_month,
	MIN(o.issued__c) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	ooo.delivery_area__c as city,
	'B2B' as type,
	'Revenue' as kpi,
	'BAT' as sub_kpi_1,
	oo.company_name as sub_kpi_2,
	oo.opportunity as sub_kpi_3,
	'-' as sub_kpi_4,
	'-' as sub_kpi_5,
	MAX(o.amount__c)/1.19 as value
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	bi.order_provider oo
	
ON

	o.opportunity__c = oo.opportunityid

LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.opportunity__c = ooo.sfid
	
WHERE

	oo.provider = 'BAT'
	AND o.test__C IS FALSE
	AND o.opportunity__c IS NOT NULL

GROUP BY

	year_month,
	LEFT(o.locale__c, 2),
	o.locale__c,
	ooo.delivery_area__c,
	oo.company_name,
	oo.opportunity
	
ORDER BY

	year_month desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 01/08/2018
-- Short Description: Insertion of hours executed by partners

SELECT

	TO_CHAR(oo.effectivedate, 'YYYY-MM') as year_month,
	MIN(oo.effectivedate) as mindate,
	LEFT(oo.locale__c, 2) as locale,
	oo.locale__c as languages,
	ooo.delivery_area__c as city,
	'B2B' as type,
	'Hours Executed' as kpi,
	'Partner' as sub_kpi_1,
	o.company_name__c as sub_kpi_2,
	ooo.name as sub_kpi_3,
	cont.service_type__c as sub_kpi_4,
	'-' as sub_kpi_5,

	SUM(oo.order_duration__c) as value
	

FROM

	bi.list_company_cleaners o
	
LEFT JOIN

	salesforce.order oo
	
ON

	o.sfid = oo.professional__c
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	oo.opportunityid = ooo.sfid
	
LEFT JOIN

	salesforce.contract__c cont
	
ON

	ooo.sfid = cont.opportunity__c
	
WHERE

	o.category = 'Partner'
	AND TO_CHAR(oo.effectivedate, 'YYYY-MM') IS NOT NULL
	-- AND oo.effectivedate <= current_date
	AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
	AND oo."type" = 'cleaning-b2b'
	AND LOWER(o.name) NOT LIKE '%test%'
	AND LOWER(o.company_name__c) NOT LIKE '%test%'
	AND oo.test__c IS FALSE
	AND ooo.test__c IS FALSE
	AND o.company_name__c NOT LIKE '%BAT Business Services GmbH'
	-- AND cont.service_type__c = 'maintenance cleaning'
	
GROUP BY

	year_month,
	locale,
	languages,
	oo.opportunityid,
	ooo.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4
	
ORDER BY

	year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 01/08/2018
-- Short Description: Insertion of active customers partners

SELECT

	t1.year_month,
	t1.mindate,
	t1.locale,
	t1.languages,
	t1.city,
	t1.type,
	t1.kpi,
	t1.sub_kpi_1,
	t1.sub_kpi_2,
	'-' as sub_kpi_3,
	t1.sub_kpi_4,
	t1.sub_kpi_5,
	COUNT(DISTINCT t1.sub_kpi_3) as value
	
FROM
	
	(SELECT
	
		TO_CHAR(o.effectivedate, 'YYYY-MM') as year_month,
		MIN(o.effectivedate) as mindate,
		LEFT(o.locale__c, 2) as locale,
		o.locale__c as languages,
		ooo.delivery_area__c as city,
		'B2B' as type,
		'Active Customer' as kpi,
		'Partner' as sub_kpi_1,
		oo.company_name as sub_kpi_2,
		o.opportunityid as sub_kpi_3,
		cont.service_type__c as sub_kpi_4,
		'-' as sub_kpi_5
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		bi.order_provider oo
		
	ON
	
		o.opportunityid = oo.opportunityid

	LEFT JOIN

		salesforce.opportunity ooo

	ON

		o.opportunityid = ooo.sfid
	
	LEFT JOIN

		salesforce.contract__c cont
		
	ON
	
		ooo.sfid = cont.opportunity__c
		
	WHERE
	
		oo.provider = 'Partner'
		AND o.type = 'cleaning-b2b'
		AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		-- AND cont.service_type__c = 'maintenance cleaning'
		AND oo.company_name NOT LIKE '%BAT Business Services GmbH' 
		
	GROUP BY
	
		year_month,
		LEFT(o.locale__c, 2),
		o.locale__c,
		ooo.delivery_area__c,
		oo.company_name,
		o.opportunityid,
		cont.service_type__c
		
	ORDER BY
	
		year_month desc) as t1
		
GROUP BY

	t1.year_month,
	t1.mindate,
	t1.locale,
	t1.languages,
	t1.city,
	t1.type,
	t1.kpi,
	t1.sub_kpi_1,
	t1.sub_kpi_2,
	t1.sub_kpi_4,
	t1.sub_kpi_5
	
ORDER BY

	year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 01/08/2018
-- Short Description: Insertion of active professionals partners

SELECT

	TO_CHAR(oo.effectivedate, 'YYYY-MM') as year_month,
	MIN(oo.effectivedate) as mindate,
	LEFT(oo.locale__c, 2) as locale,
	oo.locale__c as languages,
	ooo.delivery_area__c as city,
	'B2B' as type,
	'Active Professionals' as kpi,
	'Partner' as sub_kpi_1,
	o.company_name__c as sub_kpi_2,
	ooo.name as sub_kpi_3,
	cont.service_type__c as sub_kpi_4,
	'-' as sub_kpi_5,

	COUNT(DISTINCT oo.professional__c) as value
	
FROM

	bi.list_company_cleaners o
	
LEFT JOIN

	salesforce.order oo
	
ON

	o.sfid = oo.professional__c

LEFT JOIN

	salesforce.opportunity ooo
	
ON

	oo.opportunityid = ooo.sfid
	
LEFT JOIN

	salesforce.contract__c cont
	
ON

	ooo.sfid = cont.opportunity__c
	
WHERE

	o.category = 'Partner'
	AND TO_CHAR(oo.effectivedate, 'YYYY-MM') IS NOT NULL
	AND oo.effectivedate <= current_date
	AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
	AND oo."type" = 'cleaning-b2b'
	AND LOWER(o.name) NOT LIKE '%test%'
	AND LOWER(o.company_name__c) NOT LIKE '%test%'
	AND o.company_name__c NOT LIKE '%BAT Business Services GmbH' 
	-- AND cont.service_type__c = 'maintenance cleaning'
	
GROUP BY

	year_month,
	locale,
	languages,

	ooo.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	cont.service_type__c
	
ORDER BY

	year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 01/08/2018
-- Short Description: Insertion of costs of supply for (opps) partners

SELECT

	t1.year_month,
	t1.mindate,
	t1.locale,
	t1.languages,
	t1.city,
	t1.type,
	t1.kpi,
	t1.sub_kpi_1,
	t1.sub_kpi_2,
	t1.sub_kpi_3,
	t1.sub_kpi_4,
	t1.sub_kpi_5,
	
	t1.value * t2.hourly_cost_supply as value	
	
FROM	
	
	(SELECT
	
		TO_CHAR(oo.effectivedate, 'YYYY-MM') as year_month,
		MIN(oo.effectivedate) as mindate,
		LEFT(oo.locale__c, 2) as locale,
		oo.locale__c as languages,
		ooo.delivery_area__c as city,
		'B2B' as type,
		'Cost Supply' as kpi,
		'Partner' as sub_kpi_1,
		o.company_name__c as sub_kpi_2,
		ooo.name as sub_kpi_3,
		oo.opportunityid as sub_kpi_4,
		cont.service_type__c as sub_kpi_5,
	
		SUM(oo.order_duration__c) as value
		
	
	FROM
	
		bi.list_company_cleaners o
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		o.sfid = oo.professional__c
		
	LEFT JOIN

		salesforce.opportunity ooo
	
	ON

		oo.opportunityid = ooo.sfid
		
	LEFT JOIN

		salesforce.contract__c cont
		
	ON
	
		ooo.sfid = cont.opportunity__c
		
	WHERE
	
		o.category = 'Partner'
		AND TO_CHAR(oo.effectivedate, 'YYYY-MM') IS NOT NULL
		AND oo.effectivedate <= current_date
		AND oo."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo."type" = 'cleaning-b2b'
		AND LOWER(o.name) NOT LIKE '%test%'
		AND LOWER(o.company_name__c) NOT LIKE '%test%'
		AND o.company_name__c NOT LIKE '%BAT Business Services GmbH' 
		-- AND cont.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		year_month,
		locale,
		languages,
		oo.opportunityid,
		ooo.delivery_area__c,
		sub_kpi_2,
		sub_kpi_3,
		cont.service_type__c
		
	ORDER BY
	
		year_month desc) as t1
		
LEFT JOIN

	bi.hourly_cost_supply t2
	
ON

	t1.sub_kpi_4 = t2.opportunity__c
	AND t1.year_month = t2."year_month";

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

SELECT

	t3.year_month,
	t3.mindate,
	t3.locale,
	t3.languages,
	t3.city,
	t3.type,
	t3.kpi,
	t3.sub_kpi_1,
	t3.sub_kpi_2,
	t3.sub_kpi_3,
	t3.sub_kpi_4,
	t3.sub_kpi_5,
	CASE WHEN t3.monthly_partner_costs__c IS NULL THEN t3.value ELSE t3.monthly_partner_costs__c END as value

FROM	
	
	(SELECT
	
	
		t1.*,
			
		t2.monthly_partner_costs__c
	
	FROM
		
		(SELECT
		
			TO_CHAR(t0.effectivedate, 'YYYY-MM') as year_month,
			MIN(t0.effectivedate) as mindate,
			LEFT(t0.locale__c, 2) as locale,
			t0.locale__c as languages,
			ooo.delivery_area__c as city,
			'B2B' as type,
			'Operational Costs' as kpi,
			'Partner' as sub_kpi_1,
			o.company_name__c as sub_kpi_2,
			ooo.name as sub_kpi_3,
			t0.opportunityid as sub_kpi_4,
			'Scorecard' as sub_kpi_5,
			SUM(t0.order_duration__c) as hours,
			o.pph_partner,
			CASE WHEN o.company_name__c LIKE 'BAT Business Services GmbH' THEN SUM(t0.order_duration__c*12.9)
			     ELSE SUM(t0.order_duration__c*o.pph_partner)
				  END as value
				
		FROM
		
			bi.list_company_cleaners o
			
		LEFT JOIN
		
			salesforce.order t0
			
		ON
		
			o.sfid = t0.professional__c
		
		LEFT JOIN

			salesforce.opportunity ooo
	
		ON

			t0.opportunityid = ooo.sfid
			
		LEFT JOIN

			salesforce.contract__c cont
			
		ON
		
			ooo.sfid = cont.opportunity__c
			
		WHERE
		
			o.category = 'Partner'
			AND TO_CHAR(t0.effectivedate, 'YYYY-MM') IS NOT NULL
			AND t0.effectivedate <= (current_date + 30)
			AND t0."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND t0."type" = 'cleaning-b2b'
			AND LOWER(o.name) NOT LIKE '%test%'
			AND LOWER(o.company_name__c) NOT LIKE '%test%'
			AND o.company_name__c NOT LIKE '%BAT Business Services GmbH' 
			-- AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
		
			year_month,
			locale,
			languages,
		
			ooo.delivery_area__c,
			sub_kpi_2,
			sub_kpi_3,
			sub_kpi_4,
			o.pph_partner
			-- cont.service_type__c
			
		ORDER BY
		
			year_month desc) as t1
			
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON
	
		t1.sub_kpi_4 = t2.sfid) as t3;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 01/08/2018
-- Short Description: Insertion of costs of partners (not the supply but the operational cost, which can be based on hourly cost or monthly cost and 12.9€ per hour for the BAT cleaners)
	
	
SELECT

	t3.year_month,
	t3.mindate,
	t3.locale,
	t3.languages,
	t3.city,
	t3.type,
	t3.kpi,
	t3.sub_kpi_1,
	t3.sub_kpi_2,
	t3.sub_kpi_3,
	t3.sub_kpi_4,
	t3.sub_kpi_5,
	CASE WHEN t3.monthly_partner_costs__c IS NULL THEN t3.value ELSE t3.monthly_partner_costs__c END as value

FROM	
	
	(SELECT
	
	
		t1.*,
			
		t2.monthly_partner_costs__c
	
	FROM
		
		(SELECT
		
			TO_CHAR(t0.effectivedate, 'YYYY-MM') as year_month,
			MIN(t0.effectivedate) as mindate,
			LEFT(t0.locale__c, 2) as locale,
			t0.locale__c as languages,
			ooo.delivery_area__c as city,
			'B2B' as type,
			'Operational Costs' as kpi,
			'Partner' as sub_kpi_1,
			o.company_name__c as sub_kpi_2,
			ooo.name as sub_kpi_3,
			t0.opportunityid as sub_kpi_4,
			cont.service_type__c as sub_kpi_5,
			SUM(t0.order_duration__c) as hours,
			o.pph_partner,
			CASE WHEN o.company_name__c LIKE 'BAT Business Services GmbH' THEN SUM(t0.order_duration__c*12.9)
			     ELSE SUM(t0.order_duration__c*o.pph_partner)
				  END as value
				
		FROM
		
			bi.list_company_cleaners o
			
		LEFT JOIN
		
			salesforce.order t0
			
		ON
		
			o.sfid = t0.professional__c
		
		LEFT JOIN

			salesforce.opportunity ooo
	
		ON

			t0.opportunityid = ooo.sfid
			
		LEFT JOIN

			salesforce.contract__c cont
			
		ON
		
			ooo.sfid = cont.opportunity__c
			
		WHERE
		
			o.category = 'Partner'
			AND TO_CHAR(t0.effectivedate, 'YYYY-MM') IS NOT NULL
			AND t0.effectivedate <= (current_date + 30)
			AND t0."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND t0."type" = 'cleaning-b2b'
			AND LOWER(o.name) NOT LIKE '%test%'
			AND LOWER(o.company_name__c) NOT LIKE '%test%'
			AND o.company_name__c NOT LIKE '%BAT Business Services GmbH' 
			-- AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
		
			year_month,
			locale,
			languages,
		
			ooo.delivery_area__c,
			sub_kpi_2,
			sub_kpi_3,
			sub_kpi_4,
			o.pph_partner,
			cont.service_type__c
			
		ORDER BY
		
			year_month desc) as t1
			
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON
	
		t1.sub_kpi_4 = t2.sfid) as t3
	;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 16/05/2018
-- Short Description: Insertion of costs sales team DE
INSERT INTO  bi.kpi_master VALUES ('2019-12','2019-12-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','33766');
INSERT INTO  bi.kpi_master VALUES ('2019-11','2019-11-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','33766');
INSERT INTO  bi.kpi_master VALUES ('2019-10','2019-10-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','33766');
INSERT INTO  bi.kpi_master VALUES ('2019-09','2019-09-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-08','2019-08-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-07','2019-07-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-06','2019-06-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-05','2019-05-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-04','2019-04-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-03','2019-03-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-02','2019-02-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2019-01','2019-01-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2018-12','2018-12-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37281');
INSERT INTO  bi.kpi_master VALUES ('2018-11','2018-11-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','30757');
INSERT INTO  bi.kpi_master VALUES ('2018-10','2018-10-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','31834');
INSERT INTO  bi.kpi_master VALUES ('2018-09','2018-09-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','29447');
INSERT INTO  bi.kpi_master VALUES ('2018-08','2018-08-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','23042');
INSERT INTO  bi.kpi_master VALUES ('2018-07','2018-07-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','19978');
INSERT INTO  bi.kpi_master VALUES ('2018-06','2018-06-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','19066');
INSERT INTO  bi.kpi_master VALUES ('2018-05','2018-05-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','20673');
INSERT INTO  bi.kpi_master VALUES ('2018-04','2018-04-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','22005');
INSERT INTO  bi.kpi_master VALUES ('2018-03','2018-03-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','20196');
INSERT INTO  bi.kpi_master VALUES ('2018-02','2018-02-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','23972');
INSERT INTO  bi.kpi_master VALUES ('2018-01','2018-01-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','68565');
INSERT INTO  bi.kpi_master VALUES ('2017-12','2017-12-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','36135');
INSERT INTO  bi.kpi_master VALUES ('2017-11','2017-11-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37442');
INSERT INTO  bi.kpi_master VALUES ('2017-10','2017-10-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','40504');
INSERT INTO  bi.kpi_master VALUES ('2017-09','2017-09-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','41118');
INSERT INTO  bi.kpi_master VALUES ('2017-08','2017-08-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','46110');
INSERT INTO  bi.kpi_master VALUES ('2017-07','2017-07-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','44930');
INSERT INTO  bi.kpi_master VALUES ('2017-06','2017-06-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','61215');
INSERT INTO  bi.kpi_master VALUES ('2017-05','2017-05-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','73985');
INSERT INTO  bi.kpi_master VALUES ('2017-04','2017-04-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','64608');
INSERT INTO  bi.kpi_master VALUES ('2017-03','2017-03-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','55148');
INSERT INTO  bi.kpi_master VALUES ('2017-02','2017-02-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','54984');
INSERT INTO  bi.kpi_master VALUES ('2017-01','2017-01-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','46993');
INSERT INTO  bi.kpi_master VALUES ('2016-12','2016-12-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','37387');
INSERT INTO  bi.kpi_master VALUES ('2016-11','2016-11-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','38446');
INSERT INTO  bi.kpi_master VALUES ('2016-10','2016-10-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','34393');
INSERT INTO  bi.kpi_master VALUES ('2016-09','2016-09-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','36101');
INSERT INTO  bi.kpi_master VALUES ('2016-08','2016-08-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','22989');
INSERT INTO  bi.kpi_master VALUES ('2016-07','2016-07-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','19008');
INSERT INTO  bi.kpi_master VALUES ('2016-06','2016-06-01','de','de-de','-','Marketing Stats','Cost Team','Actual','-','-','-','-','24363');


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 16/05/2018 - Last Update 2018/11/12
-- Short Description: Insertion of Leadgen Costs

INSERT INTO bi.kpi_master 

SELECT

	LEFT(o.start_date::text, 7) as date_part,
	MIN(o.start_date) as mindate,
	o.locale,
	o.languages,
	o.city,
	'B2B' as type,
	'Marketing Stats' as kpi,
	'SEM Costs' as sub_kpi_1,
	'Actual' as sub_kpi_2,
	'-' as sub_kpi_3,
	'-' as sub_kpi_4,
	'-' as sub_kpi_5,
	TRUNC(SUM(o.value), 2) as value


FROM

	public.marketing_spending o
	
WHERE

	o.kpi = 'Marketing Costs'
	
GROUP BY 

	LEFT(o.start_date::text, 7),
	o.locale,
	o.languages,
	o.city
	
ORDER BY

	LEFT(o.start_date::text, 7) desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 16/05/2018 - Last Update 2018/11/13
-- Short Description: Insertion of Likelies created

INSERT INTO bi.kpi_master 

SELECT

	LEFT(o.createddate::text, 7) as date_part,
	MIN(o.createddate) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	'-' as city,
	'B2B' as type,
	'Marketing Stats' as kpi,
	'Likelies' as sub_kpi_1,
	'Actual' as sub_kpi_2,
	CASE WHEN o.stage__c = 'QUALIFIED' THEN 'Qualified'
	     WHEN o.stage__c = 'ENDED' THEN 'Ended'
	     ELSE 'Pipeline'
	     END
	as sub_kpi_3,
	'-' as sub_kpi_4,
	'-' as sub_kpi_5,
	COUNT(DISTINCT o.sfid) as likelies

FROM

	salesforce.likeli__c o
	
WHERE

	o.type__c = 'B2B'
	AND o.acquisition_channel__c IN ('inbound', 'web')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.test__c IS FALSE
	AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
	AND acquisition_channel__c NOT LIKE 'outbound'
	AND company_name__c NOT LIKE '%test%'
	AND company_name__c NOT LIKE '%bookatiger%'
	AND email__c NOT LIKE '%bookatiger%'
	AND o.name NOT LIKE '%test%'
	
GROUP BY

	LEFT(o.createddate::text, 7),
	LEFT(o.locale__c, 2),
	o.locale__c,
	sub_kpi_3
	
ORDER BY

	LEFT(o.createddate::text, 7) desc;

-- INSERT INTO bi.kpi_master VALUES ('2017-07','2017-07-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','4504'); 
-- INSERT INTO bi.kpi_master VALUES ('2017-08','2017-08-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','7790');
-- INSERT INTO bi.kpi_master VALUES ('2017-09','2017-09-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','6645');
-- INSERT INTO bi.kpi_master VALUES ('2017-10','2017-10-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','10981');
-- INSERT INTO bi.kpi_master VALUES ('2017-11','2017-11-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','11957');
-- INSERT INTO bi.kpi_master VALUES ('2017-12','2017-12-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','8726');
-- INSERT INTO bi.kpi_master VALUES ('2018-01','2018-01-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','43664');
-- INSERT INTO bi.kpi_master VALUES ('2018-02','2018-02-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','39754');
-- INSERT INTO bi.kpi_master VALUES ('2018-03','2018-03-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','25415');
-- INSERT INTO bi.kpi_master VALUES ('2018-04','2018-04-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','26934');
-- INSERT INTO bi.kpi_master VALUES ('2018-05','2018-05-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','31032');
-- INSERT INTO bi.kpi_master VALUES ('2018-06','2018-06-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','31882');
-- INSERT INTO bi.kpi_master VALUES ('2018-07','2018-07-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','33986');
-- INSERT INTO bi.kpi_master VALUES ('2018-08','2018-08-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','37134');
-- INSERT INTO bi.kpi_master VALUES ('2018-09','2018-09-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','42380');
-- INSERT INTO bi.kpi_master VALUES ('2018-10','2018-10-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','52139');
-- INSERT INTO bi.kpi_master VALUES ('2018-11','2018-11-01','de','de-de','-','Marketing Stats','Adwords Cost','Actual','-','-','-','-','12166');

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 28/01/2019
-- Short Description: New Running Opps Beta, using the confirmed end date

INSERT INTO bi.kpi_master 

SELECT
	
	year_month,
	date,
	-- max_date,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,	
	sub_kpi_2,
	-- table2.closed_by,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5,
	SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value

FROM	
	
	(SELECT	
	
		table1.year_month as year_month,
		MIN(table1.ymd) as date,
		table1.ymd_max as max_date,
		
		table1.country as locale,
		table1.locale__c as languages,
		table1.polygon as city,
		CAST('B2B' as varchar) as type,
		CAST('Running Opps Beta' as varchar) as kpi,
		CAST('Count' as varchar) as sub_kpi_1,	
		CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
			  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
			  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
			  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
			  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
			  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
			  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
			  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
			  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
			  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
			  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
			  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
			  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
			  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
			  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
			  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
			  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
			  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
			  ELSE '>M18'
			  END as sub_kpi_2,
		CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
		table1.closed_by,
		CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
		CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
		table1.opportunityid,
		table1.category
		-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
		
	FROM
		
		(SELECT
		
			t2.*,
			o.status__c as status_now,
			ct.name as name_closer,
			o.closed_by__c as closed_by,
			o.ownerid,
			usr.name as name_owner,
			ooo.potential as grand_total
		
		FROM
			
			(SELECT
				
					time_table.*,
					table_dates.*,
					CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
				
				FROM	
					
					(SELECT
						
						'1'::integer as key,	
						TO_CHAR(i, 'YYYY-MM') as year_month,
						MIN(i) as ymd,
						MAX(i) as ymd_max
						-- i::date as date 
						
					FROM
					
						generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
						
					GROUP BY
					
						key,
						year_month
						-- date
						
					ORDER BY 
					
						year_month desc) as time_table
						
				LEFT JOIN
				
					(SELECT
					
						'1'::integer as key_link,
						t1.country,
						t1.locale__c,
						t1.delivery_area__c as polygon,
						t1.opportunityid,
						t1.date_start,
						TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
						CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
						CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
						
					
					FROM
						
						((SELECT
		
							LEFT(o.locale__c, 2) as country,
							o.locale__c,
							o.opportunityid,
							oo.delivery_area__c,
							MIN(o.effectivedate) as date_start
						
						FROM
						
							salesforce.order o

						LEFT JOIN
				
							salesforce.opportunity oo
							
						ON 
						
							o.opportunityid = oo.sfid
							
						LEFT JOIN

							salesforce.contract__c cont
							
						ON
						
							oo.sfid = cont.opportunity__c
							
						WHERE
						
							o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
							AND o.type = 'cleaning-b2b'
							AND o.professional__c IS NOT NULL
							AND o.test__c IS FALSE
							AND cont.service_type__c = 'maintenance cleaning'
							
						GROUP BY
						
							o.opportunityid,
							o.locale__c,
							oo.delivery_area__c,
							LEFT(o.locale__c, 2))) as t1
							
					LEFT JOIN
					
						(SELECT
						
							t3.*,
							t3.end_contract as date_churn,
							CASE WHEN t3.date_last_order > t3.end_contract THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
						
						FROM	
							
							(SELECT
							
								t1.country,
								t1.opportunityid,
								t1.date_last_order,
								t2.date_churn as end_contract
							
							FROM
							
								(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
								         -- It's the last day on which they are making money
									LEFT(o.locale__c, 2) as country,
									o.opportunityid,
									'last_order' as type_date,
									MAX(o.effectivedate) as date_last_order
									
								FROM
								
									salesforce.order o
									
								LEFT JOIN
								
									salesforce.opportunity oo
									
								ON 
								
									o.opportunityid = oo.sfid
									
								LEFT JOIN

									salesforce.contract__c cont
									
								ON
								
									oo.sfid = cont.opportunity__c
									
								WHERE
								
									o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
									AND oo.status__c IN ('RESIGNED', 'CANCELLED')
									AND o.type = 'cleaning-b2b'
									AND o.professional__c IS NOT NULL
									AND o.test__c IS FALSE
									AND oo.test__c IS FALSE
									AND cont.service_type__c = 'maintenance cleaning'
								
								GROUP BY
								
									LEFT(o.locale__c, 2),
									type_date,
									o.opportunityid) as t1
									
							LEFT JOIN
							
								(SELECT
								
									t3.*,
									t3.contract_end as date_churn,
									CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
								
								FROM	
									
									(SELECT
									
										t1.country,
										t1.opportunityid,
										t1.date_last_order,
										t2.date_churn as contract_end
									
									FROM
									
										(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
										         -- It's the last day on which they are making money
											LEFT(o.locale__c, 2) as country,
											o.opportunityid,
											'last_order' as type_date,
											MAX(o.effectivedate) as date_last_order
											
										FROM
										
											salesforce.order o
											
										LEFT JOIN
										
											salesforce.opportunity oo
											
										ON 
										
											o.opportunityid = oo.sfid
											
										LEFT JOIN

											salesforce.contract__c cont
											
										ON
										
											oo.sfid = cont.opportunity__c
											
										WHERE
										
											o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
											AND oo.status__c IN ('RESIGNED', 'CANCELLED')
											AND o.type = 'cleaning-b2b'
											AND o.professional__c IS NOT NULL
											AND o.test__c IS FALSE
											AND oo.test__c IS FALSE
											AND cont.service_type__c = 'maintenance cleaning'
										
										GROUP BY
										
											LEFT(o.locale__c, 2),
											type_date,
											o.opportunityid) as t1
											
									LEFT JOIN
									
										(SELECT -- Here we make the list of opps with their confirmed end or end date when available
																
											o.opportunity__c as opportunityid,

											MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
										
										FROM
										
											salesforce.contract__c o
										
										WHERE
										
											o.test__c IS FALSE
											AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
											AND o.service_type__c LIKE 'maintenance cleaning'
											-- AND o.active__c IS TRUE 
											-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
											
										GROUP BY
										
											o.opportunity__c) as t2 
									
									ON
									
										t1.opportunityid = t2.opportunityid) as t3) as t2 
							
							ON
							
								t1.opportunityid = t2.opportunityid) as t3) as t2
							
					ON
					
						t1.opportunityid = t2.opportunityid) as table_dates
						
				ON 
				
					time_table.key = table_dates.key_link
					
			) as t2
				
		LEFT JOIN
		
			salesforce.opportunity o
			
		ON
		
			t2.opportunityid = o.sfid
			
		LEFT JOIN
		
			salesforce.user ct
			
		ON
		
			o.closed_by__c = ct.sfid
			
		LEFT JOIN
			
			salesforce.user usr
			
		ON
		
			o.ownerid = usr.sfid

		LEFT JOIN

			bi.potential_revenue_per_opp ooo

		ON

			t2.opportunityid = ooo.opportunityid
			
		LEFT JOIN
		
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c

		WHERE 

			o.test__c IS FALSE
			AND cont.test__c IS FALSE
			AND cont.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND cont.service_type__c LIKE 'maintenance cleaning'
			
		GROUP BY
		
				ooo.potential,
				o.status__c,
				o.closed_by__c,
				o.ownerid,
				ct.name,
				usr.name,
				t2.key,	
				t2.year_month,
				t2.ymd,
				t2.ymd_max,
				t2.key_link,
				t2.country,
				t2.locale__c,
				t2.polygon,
				t2.opportunityid,
				t2.date_start,
				t2.year_month_start,
				t2.date_churn,
				t2.year_month_churn,
				t2.category
			) as table1

	WHERE

		table1.opportunityid IS NOT NULL
			
	GROUP BY
	
		table1.year_month,
		table1.ymd_max,
		table1.country,
		LEFT(table1.locale__c, 2),
		table1.locale__c,
		table1.polygon,
		CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
			  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
			  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
			  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
			  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
			  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
			  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
			  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
			  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
			  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
			  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
			  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
			  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
			  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
			  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
			  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
			  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
			  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
			  ELSE '>M18'
			  END,
			  sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table1.category,
		table1.opportunityid,
		table1.closed_by
		
	ORDER BY
	
		table1.year_month desc)	 as table2
	
GROUP BY

	year_month,
	date,
	-- max_date,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,	
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	-- table2.closed_by
	
ORDER BY 

	year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 28/01/2019
-- Short Description: New Running Revenue Beta, using the confirmed end date

INSERT INTO bi.kpi_master 

SELECT

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	-- table3.opportunityid,
	SUM(CASE WHEN table3.value > 0 THEN table3.grand_total ELSE 0 END) as value

FROM
	
	(SELECT
	
		year_month,
		date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid,
		SUM(table2.grand_total) as grand_total,
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps Beta' as varchar) as kpi,
			CAST('Revenue' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
			table1.closed_by,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.category,
			table1.grand_total
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				ct.name as name_closer,
				o.closed_by__c as closed_by,
				o.ownerid,
				usr.name as name_owner,
				ooo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								oo.delivery_area__c,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o

							LEFT JOIN
				
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							LEFT JOIN

								salesforce.contract__c cont
								
							ON
							
								oo.sfid = cont.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND cont.service_type__c = 'maintenance cleaning'
								
							GROUP BY
							
								o.opportunityid,
								o.locale__c,
								oo.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT
							
								t3.*,
								t3.contract_end as date_churn,
								CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
							
							FROM	
								
								(SELECT
								
									t1.country,
									t1.opportunityid,
									t1.date_last_order,
									t2.date_churn as contract_end
								
								FROM
								
									(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
									         -- It's the last day on which they are making money
										LEFT(o.locale__c, 2) as country,
										o.opportunityid,
										'last_order' as type_date,
										MAX(o.effectivedate) as date_last_order
										
									FROM
									
										salesforce.order o
										
									LEFT JOIN
									
										salesforce.opportunity oo
										
									ON 
									
										o.opportunityid = oo.sfid
										
									LEFT JOIN

										salesforce.contract__c cont
										
									ON
									
										oo.sfid = cont.opportunity__c
										
									WHERE
									
										o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
										AND oo.status__c IN ('RESIGNED', 'CANCELLED')
										AND o.type = 'cleaning-b2b'
										AND o.professional__c IS NOT NULL
										AND o.test__c IS FALSE
										AND oo.test__c IS FALSE
										AND cont.service_type__c = 'maintenance cleaning'
									
									GROUP BY
									
										LEFT(o.locale__c, 2),
										type_date,
										o.opportunityid) as t1
										
								LEFT JOIN
								
									(SELECT -- Here we make the list of opps with their confirmed end or end date when available
															
										o.opportunity__c as opportunityid,
										MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
									
									FROM
									
										salesforce.contract__c o
									
									WHERE
									
										o.test__c IS FALSE
										AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
										AND o.service_type__c LIKE 'maintenance cleaning'
										-- AND o.active__c IS TRUE 
										-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
										
									GROUP BY
									
										o.opportunity__c) as t2 
								
								ON
								
									t1.opportunityid = t2.opportunityid) as t3) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid
			
			LEFT JOIN
		
				salesforce.user ct
				
			ON
			
				o.closed_by__c = ct.sfid
				
			LEFT JOIN
				
				salesforce.user usr
				
			ON
			
				o.ownerid = usr.sfid
				
			LEFT JOIN
			
				salesforce.contract__c oo
				
			ON
			
				t2.opportunityid = oo.opportunity__c

			LEFT JOIN

				bi.potential_revenue_per_opp ooo

			ON
				
				t2.opportunityid = ooo.opportunityid
	
			LEFT JOIN
		
				salesforce.contract__c cont
			
			ON
		
				o.sfid = cont.opportunity__c

			WHERE 

				o.test__c IS FALSE
				AND cont.test__c IS FALSE
				AND cont.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
				AND cont.service_type__c LIKE 'maintenance cleaning'
				
			GROUP BY
			
					ooo.potential,
					o.status__c,
					t2.key,	
					ct.name,
					o.closed_by__c,
					o.ownerid,
					usr.name,
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category
				) as table1
				
		GROUP BY
		
			table1.year_month,
			table1.ymd_max,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid,
			table1.grand_total,
			table1.closed_by
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	WHERE
	
		-- table2.opportunityid = '0060J00000qCkNQQA0'
	
		table2.category = 'RUNNING'
			
	GROUP BY
	
		year_month,
		date,
		max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid
		
	ORDER BY 
	
		year_month desc) as table3
		
GROUP BY

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5
	-- table3.opportunityid
	
ORDER BY

	table3.year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 28/01/2019
-- Short Description: New Churned opps Beta, using the confirmed end date

INSERT INTO bi.kpi_master 

SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	-- CAST('-' as varchar) as city,
	table1.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn Beta' as varchar) as kpi,
	CAST('Count opps' as varchar) as sub_kpi_1,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
			  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
			  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
			  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
			  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
			  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
			  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
			  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
			  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
			  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
			  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
			  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
			  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
			  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
			  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
			  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
			  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
			  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
			  ELSE '>M18'
			  END as sub_kpi_2,
	
	CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
		  
	CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
	CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
	COUNT(DISTINCT table1.opportunityid) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		oo.delivery_area__c,
		o.opportunityid,
		ct.name as name_closer,
		oo.closed_by__c as closed_by,
		oo.ownerid,
		usr.name as name_owner,
		ooo.potential as grand_total,
		'last_order' as type_date,
		MIN(o.effectivedate) as date_start,
		MAX(t4.date_churn) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
		
		salesforce.user ct
		
	ON
	
		oo.closed_by__c = ct.sfid
		
	LEFT JOIN
		
		salesforce.user usr
		
	ON
	
		oo.ownerid = usr.sfid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunityid = ooo.opportunityid
		
	LEFT JOIN
	
		(SELECT
		
			t3.*,
			CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
			CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
		
		FROM	
			
			(SELECT
			
				t1.country,
				t1.opportunityid,
				t1.date_last_order,
				t2.date_churn as contract_end
			
			FROM
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
				         -- It's the last day on which they are making money
					LEFT(o.locale__c, 2) as country,
					o.opportunityid,
					'last_order' as type_date,
					MAX(o.effectivedate) as date_last_order
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				LEFT JOIN

					salesforce.contract__c cont
					
				ON
				
					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND oo.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
				
				GROUP BY
				
					LEFT(o.locale__c, 2),
					type_date,
					o.opportunityid) as t1
					
			LEFT JOIN
			
				(SELECT -- Here we make the list of opps with their confirmed end or end date when available
										
					o.opportunity__c as opportunityid,
					MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
				
				FROM
				
					salesforce.contract__c o
				
				WHERE
				
					o.test__c IS FALSE
					AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
					AND o.service_type__c LIKE 'maintenance cleaning'
					-- AND o.active__c IS TRUE 
					-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
					
				GROUP BY
				
					o.opportunity__c) as t2 
			
			ON
			
				t1.opportunityid = t2.opportunityid) as t3) as t4
				
	ON
	
		o.opportunityid = t4.opportunityid
		
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		AND o.test__c IS FALSE
		AND o.professional__c IS NOT NULL
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		oo.delivery_area__c,
		ct.name,
		oo.closed_by__c,
		oo.ownerid,
		usr.name,
		type_date,
		grand_total,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	table1.country,
	table1.locale__c,
	table1.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 28/01/2019
-- Short Description: New Churned revenue Beta, using the confirmed end date

INSERT INTO bi.kpi_master 

SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	-- CAST('-' as varchar) as city,
	table1.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn Beta' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	
	CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
			  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
			  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
			  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
			  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
			  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
			  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
			  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
			  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
			  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
			  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
			  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
			  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
			  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
			  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
			  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
			  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
			  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
			  ELSE '>M18'
			  END as sub_kpi_2,
	
	CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
		  
	CASE WHEN table1.grand_total < 250 THEN '<250€'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
		  WHEN table1.grand_total IS NULL THEN 'Unknown'
		  ELSE '>1000€'
		  END as sub_kpi_4,
		  	  
	CASE WHEN table1.grand_total < 250 THEN 'Very Small'
		  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
		  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
		  WHEN table1.grand_total IS NULL THEN 'Unknown'
		  ELSE 'Key Account'
		  END as sub_kpi_5,
	-- table1.opportunityid,
	SUM(table1.grand_total) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		oo.delivery_area__c,
		o.opportunityid,
		ct.name as name_closer,
		oo.closed_by__c as closed_by,
		oo.ownerid,
		usr.name as name_owner,
		ooo.potential as grand_total,
		'last_order' as type_date,
		MIN(o.effectivedate) as date_start,
		MAX(t4.date_churn) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
		
		salesforce.user ct
		
	ON
	
		oo.closed_by__c = ct.sfid
		
	LEFT JOIN
		
		salesforce.user usr
		
	ON
	
		oo.ownerid = usr.sfid
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp ooo
		
	ON
	
		o.opportunityid = ooo.opportunityid
		
	LEFT JOIN
	
		(SELECT
		
			t3.*,
			CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
			CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
		
		FROM	
			
			(SELECT
			
				t1.country,
				t1.opportunityid,
				t1.date_last_order,
				t2.date_churn as contract_end
			
			FROM
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
				         -- It's the last day on which they are making money
					LEFT(o.locale__c, 2) as country,
					o.opportunityid,
					'last_order' as type_date,
					MAX(o.effectivedate) as date_last_order
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				LEFT JOIN

					salesforce.contract__c cont
					
				ON
				
					oo.sfid = cont.opportunity__c
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					AND oo.test__c IS FALSE
					AND cont.service_type__c = 'maintenance cleaning'
				
				GROUP BY
				
					LEFT(o.locale__c, 2),
					type_date,
					o.opportunityid) as t1
					
			LEFT JOIN
			
				(SELECT -- Here we make the list of opps with their confirmed end or end date when available
										
					o.opportunity__c as opportunityid,
					MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
				
				FROM
				
					salesforce.contract__c o
				
				WHERE
				
					o.test__c IS FALSE
					AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
					AND o.service_type__c LIKE 'maintenance cleaning'
					-- AND o.active__c IS TRUE 
					-- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
					
				GROUP BY
				
					o.opportunity__c) as t2 
			
			ON
			
				t1.opportunityid = t2.opportunityid) as t3) as t4
				
	ON
	
		o.opportunityid = t4.opportunityid
		
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.test__c IS FALSE
		AND o.professional__c IS NOT NULL
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		oo.delivery_area__c,
		ooo.potential,
		ct.name,
		oo.closed_by__c,
		oo.ownerid,
		usr.name,
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	-- CASE WHEN oooo.grand_total__c IS NULL THEN oooo.pph__c*4.3*oooo.hours_weekly__c ELSE oooo.grand_total__c END,
	table1.country,
	table1.locale__c,
	table1.delivery_area__c,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	-- table1.opportunityid
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'