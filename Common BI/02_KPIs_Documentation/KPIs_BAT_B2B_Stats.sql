
-- KPIs of B2B Stats

-- Tableau dashboard: https://dub01.online.tableau.com/#/site/battigercave/views/BATBusiness/B2BStats3_0
-- Table: bi.kpi_master
-- SQL Function: bi.mfunc_kpi_master(), bi.sfunc_kpi_master()


-- New Opps Started
-- Number of new opportunities started based on their first order date
SELECT 
  TO_CHAR(t1.first_order, 'YYYY-MM') AS year_month, 
  MIN(t1.first_order) AS mindate, 
  LEFT(t1.locale__c, 2) AS locale, 
  t1.locale__c AS languages, 
  t1.delivery_area__c, 
  CAST('B2B' AS varchar) AS TYPE, 
  CAST('New' AS varchar) AS kpi, 
  CAST('Count' AS varchar) AS sub_kpi_1, 
  CAST('Monthly' AS varchar) AS sub_kpi_2, 
  CAST('Opps Started' AS varchar) AS sub_kpi_3, 
  CASE WHEN t1.acquisition_channel__c IN ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END AS sub_kpi_4, 
  CAST('-' AS varchar) AS sub_kpi_5, 
  COUNT(DISTINCT t1.sfid) AS value 
FROM 
  (
    SELECT 
      -- This first query is building a table showing the first order date for every opportunity
      o.sfid, 
      o.name, 
      o.locale__c, 
      o.email__c, 
      o.delivery_area__c, 
      o.acquisition_channel__c, 
      MIN(oo.effectivedate) AS first_order 
    FROM 
      salesforce.opportunity o 
      LEFT JOIN salesforce.order oo ON o.sfid = oo.opportunityid 
      LEFT JOIN salesforce.contract__c cont ON o.sfid = cont.opportunity__c 
    WHERE 
      -- o.status__c = 'RUNNING'
      o.test__c IS FALSE 
      AND oo.test__c IS FALSE 
      AND oo."status" IN (
        'INVOICED', 'PENDING TO START', 'FULFILLED', 
        'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
      ) 
      AND oo.professional__C IS NOT NULL 
      AND cont.service_type__c = 'maintenance cleaning' 
    GROUP BY 
      o.sfid, 
      o.name, 
      o.locale__c, 
      o.delivery_area__c, 
      o.acquisition_channel__c, 
      o.email__c
  ) AS t1 
GROUP BY 
  TO_CHAR(t1.first_order, 'YYYY-MM'), 
  LEFT(t1.locale__c, 2), 
  t1.locale__c, 
  t1.delivery_area__c, 
  CASE WHEN t1.acquisition_channel__c IN ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- New Signed Same Month
-- Desc: Number of new opportunities signed in the same month, month of closedate = month of the first order

SELECT 
  t2.year_month_start, 
  MIN(t2.mindate) as mindate, 
  t2.country as locale, 
  t2.locale__c as languages, 
  t2.city as city, 
  CAST('B2B' as varchar) as type, 
  CAST('New' as varchar) as kpi, 
  CAST('Count' as varchar) as sub_kpi_1, 
  CAST('Monthly' as varchar) as sub_kpi_2, 
  CAST(
    'Opps Signed Same Month' as varchar
  ) as sub_kpi_3, 
  CAST('-' as varchar) as sub_kpi_4, 
  CAST('-' as varchar) as sub_kpi_5, 
  SUM(t2.signed_same_month) as value 
FROM 
  (
    SELECT 
      t1.year_month_start, 
      MIN(t1.date_start) as mindate, 
      t1.country, 
      t1.locale__c, 
      t1.city, 
      SUM(
        CASE WHEN TO_CHAR(t1.closedate, 'YYYY-MM') < TO_CHAR(t1.date_start, 'YYYY-MM') THEN 1 ELSE 0 END
      ) as signed_before, 
      SUM(
        CASE WHEN TO_CHAR(t1.closedate, 'YYYY-MM') = TO_CHAR(t1.date_start, 'YYYY-MM') THEN 1 ELSE 0 END
      ) as signed_same_month 
    FROM 
      (
        SELECT 
          t1.* 
        FROM 
          (
            SELECT 
              list_start.year_month as year_month_start, 
              list_start.date_start, 
              list_start.closedate, 
              list_start.country, 
              list_start.locale__c, 
              list_start.city, 
              list_start.opportunityid as new_opps, 
              list_churn.opportunityid as churn_opps, 
              list_churn.year_month as year_month_churn, 
              list_churn.date_churn 
            FROM 
              (
                SELECT 
                  LEFT(o.locale__c, 2) as country, 
                  o.locale__c, 
                  oo.delivery_area__c as city, 
                  MIN(
                    TO_CHAR(o.effectivedate, 'YYYY-MM')
                  ) as year_month, 
                  o.opportunityid, 
                  oo.closedate, 
                  MIN(o.effectivedate):: timestamp as date_start 
                FROM 
                  salesforce.order o 
                  LEFT JOIN salesforce.opportunity oo ON o.opportunityid = oo.sfid 
                  LEFT JOIN salesforce.contract__c cont ON oo.sfid = cont.opportunity__c 
                WHERE 
                  o.status IN (
                    'INVOICED', 'PENDING TO START', 'FULFILLED', 
                    'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
                  ) 
                  AND o.type = 'cleaning-b2b' 
                  AND o.professional__c IS NOT NULL 
                  AND o.test__c IS FALSE 
                  AND oo.test__c IS FALSE 
                  AND cont.service_type__c = 'maintenance cleaning' 
                GROUP BY 
                  o.opportunityid, 
                  oo.closedate, 
                  o.locale__c, 
                  oo.delivery_area__c, 
                  LEFT(o.locale__c, 2)
              ) as list_start 
              LEFT JOIN (
                SELECT 
                  -- Here we make a list containing all the opportunities having a ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION', 'CANCELLED CUSTOMER') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                  -- It's the last day on which they are making money
                  LEFT(oo.locale__c, 2) as country, 
                  o.opportunityid, 
                  oo.closedate, 
                  MAX(
                    TO_CHAR(o.effectivedate, 'YYYY-MM')
                  ) as year_month, 
                  MAX(o.effectivedate):: timestamp as date_churn 
                FROM 
                  salesforce.order o 
                  LEFT JOIN salesforce.opportunity oo ON o.opportunityid = oo.sfid 
                  LEFT JOIN salesforce.contract__c cont ON oo.sfid = cont.opportunity__c 
                WHERE 
                  o.status IN (
                    'INVOICED', 'PENDING TO START', 'FULFILLED', 
                    'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
                  ) 
                  AND oo.status__c IN ('RESIGNED', 'CANCELLED') 
                  AND oo.test__c IS FALSE 
                  AND cont.service_type__c = 'maintenance cleaning' 
                GROUP BY 
                  LEFT(oo.locale__c, 2), 
                  o.opportunityid, 
                  oo.closedate
              ) as list_churn ON list_start.opportunityid = list_churn.opportunityid
          ) as t1 
          LEFT JOIN salesforce.opportunity oo ON t1.new_opps = oo.sfid
      ) as t1 
    GROUP BY 
      year_month_start, 
      -- year_month_churn,
      t1.country, 
      t1.locale__c, 
      t1.city 
    ORDER BY 
      year_month_start desc
  ) as t2 
WHERE 
  t2.year_month_start IS NOT NULL 
GROUP BY 
  t2.year_month_start, 
  t2.country, 
  t2.locale__c, 
  t2.city 
ORDER BY 
  t2.year_month_start desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- New Signed Before
-- Desc: Number of new opportunities signed when month of closedate(signed date) < month of the first valid order

SELECT 
  t2.year_month, 
  MIN(t2.mindate) as mindate, 
  LEFT(t2.locale__c, 2) as locale, 
  t2.locale__c as languages, 
  t2.delivery_area__c, 
  CAST('B2B' as varchar) as type, 
  CAST('New' as varchar) as kpi, 
  CAST('Count' as varchar) as sub_kpi_1, 
  CAST('Monthly' as varchar) as sub_kpi_2, 
  CAST('Opps Signed Before' as varchar) as sub_kpi_3, 
  CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4, 
  CAST('-' as varchar) as sub_kpi_5, 
  SUM(t2.signed_before) as value 
FROM 
  (
    SELECT 
      TO_CHAR(t1.first_order, 'YYYY-MM') as year_month, 
      MIN(t1.first_order) as mindate, 
      t1.sfid, 
      t1.name, 
      t1.locale__c, 
      t1.email__c, 
      t1.delivery_area__c, 
      t1.acquisition_channel__c, 
      SUM(
        CASE WHEN TO_CHAR(t1.closedate, 'YYYY-MM') < TO_CHAR(t1.first_order, 'YYYY-MM') THEN 1 ELSE 0 END
      ) as signed_before, 
      SUM(
        CASE WHEN TO_CHAR(t1.closedate, 'YYYY-MM') = TO_CHAR(t1.first_order, 'YYYY-MM') THEN 1 ELSE 0 END
      ) as signed_same_month 
    FROM 
      (
        SELECT 
          -- This first query is building a table showing the first order date for every opportunity
          o.sfid, 
          o.name, 
          o.locale__c, 
          o.email__c, 
          o.delivery_area__c, 
          o.acquisition_channel__c, 
          o.closedate, 
          MIN(oo.effectivedate) as first_order, 
          ooo.potential 
        FROM 
          salesforce.opportunity o 
          LEFT JOIN salesforce.order oo ON o.sfid = oo.opportunityid 
          LEFT JOIN bi.potential_revenue_per_opp ooo ON o.sfid = ooo.opportunityid 
          LEFT JOIN salesforce.contract__c cont ON o.sfid = cont.opportunity__c 
        WHERE 
          -- o.status__c = 'RUNNING'
          o.test__c IS FALSE 
          AND oo.test__c IS FALSE 
          AND oo."status" IN (
            'INVOICED', 'PENDING TO START', 'FULFILLED', 
            'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
          ) 
          AND oo.professional__C IS NOT NULL 
          AND cont.service_type__c = 'maintenance cleaning' 
        GROUP BY 
          o.sfid, 
          o.name, 
          o.locale__c, 
          o.delivery_area__c, 
          o.acquisition_channel__c, 
          o.closedate, 
          o.email__c, 
          ooo.potential
      ) as t1 
    GROUP BY 
      TO_CHAR(t1.first_order, 'YYYY-MM'), 
      t1.sfid, 
      t1.name, 
      t1.locale__c, 
      t1.email__c, 
      t1.delivery_area__c, 
      t1.acquisition_channel__c
  ) as t2 
GROUP BY 
  t2.year_month, 
  LEFT(t2.locale__c, 2), 
  t2.locale__c, 
  t2.delivery_area__c, 
  CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END 
ORDER BY 
  t2.year_month desc, 
  LEFT(t2.locale__c, 2), 
  t2.locale__c, 
  t2.delivery_area__c, 
  CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Running Opps (Contract Based)
-- Desc: Number of running opportunities

SELECT
    
    year_month,
    date,
    -- max_date,
    locale,
    languages,
    city,
    type,
    kpi,
    sub_kpi_1,  
    sub_kpi_2,
    -- table2.closed_by,
    sub_kpi_3,
    sub_kpi_4,
    sub_kpi_5,
    SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value

FROM    
    
    (SELECT 
    
        table1.year_month as year_month,
        MIN(table1.ymd) as date,
        table1.ymd_max as max_date,
        
        table1.country as locale,
        table1.locale__c as languages,
        table1.polygon as city,
        CAST('B2B' as varchar) as type,
        CAST('Running Opps Beta' as varchar) as kpi,
        CAST('Count' as varchar) as sub_kpi_1,  
        CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
              WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
              WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
              WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
              WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
              WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
              WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
              WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
              WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
              WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
              WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
              WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
              WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
              WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
              WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
              WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
              WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
              WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
              ELSE '>M18'
              END as sub_kpi_2,
        CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
        table1.closed_by,
        CASE WHEN table1.grand_total < 250 THEN '<250€'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
          ELSE '>1000€'
          END as sub_kpi_4,
              
        CASE WHEN table1.grand_total < 250 THEN 'Very Small'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
          ELSE 'Key Account'
          END as sub_kpi_5,
        table1.opportunityid,
        table1.category
        -- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
        
    FROM
        
        (SELECT
        
            t2.*,
            o.status__c as status_now,
            ct.name as name_closer,
            o.closed_by__c as closed_by,
            o.ownerid,
            usr.name as name_owner,
            ooo.potential as grand_total
        
        FROM
            
            (SELECT
                
                    time_table.*,
                    table_dates.*,
                    CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
                
                FROM    
                    
                    (SELECT
                        
                        '1'::integer as key,    
                        TO_CHAR(i, 'YYYY-MM') as year_month,
                        MIN(i) as ymd,
                        MAX(i) as ymd_max
                        -- i::date as date 
                        
                    FROM
                    
                        generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
                        
                    GROUP BY
                    
                        key,
                        year_month
                        -- date
                        
                    ORDER BY 
                    
                        year_month desc) as time_table
                        
                LEFT JOIN
                
                    (SELECT
                    
                        '1'::integer as key_link,
                        t1.country,
                        t1.locale__c,
                        t1.delivery_area__c as polygon,
                        t1.opportunityid,
                        t1.date_start,
                        TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
                        CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
                        CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
                        
                    
                    FROM
                        
                        ((SELECT
        
                            LEFT(o.locale__c, 2) as country,
                            o.locale__c,
                            o.opportunityid,
                            oo.delivery_area__c,
                            MIN(o.effectivedate) as date_start
                        
                        FROM
                        
                            salesforce.order o

                        LEFT JOIN
                
                            salesforce.opportunity oo
                            
                        ON 
                        
                            o.opportunityid = oo.sfid
                            
                        LEFT JOIN

                            salesforce.contract__c cont
                            
                        ON
                        
                            oo.sfid = cont.opportunity__c
                            
                        WHERE
                        
                            o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                            AND o.type = 'cleaning-b2b'
                            AND o.professional__c IS NOT NULL
                            AND o.test__c IS FALSE
                            AND cont.service_type__c = 'maintenance cleaning'
                            
                        GROUP BY
                        
                            o.opportunityid,
                            o.locale__c,
                            oo.delivery_area__c,
                            LEFT(o.locale__c, 2))) as t1
                            
                    LEFT JOIN
                    
                        (SELECT
                        
                            t3.*,
                            t3.end_contract as date_churn,
                            CASE WHEN t3.date_last_order > t3.end_contract THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
                        
                        FROM    
                            
                            (SELECT
                            
                                t1.country,
                                t1.opportunityid,
                                t1.date_last_order,
                                t2.date_churn as end_contract
                            
                            FROM
                            
                                (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                                         -- It's the last day on which they are making money
                                    LEFT(o.locale__c, 2) as country,
                                    o.opportunityid,
                                    'last_order' as type_date,
                                    MAX(o.effectivedate) as date_last_order
                                    
                                FROM
                                
                                    salesforce.order o
                                    
                                LEFT JOIN
                                
                                    salesforce.opportunity oo
                                    
                                ON 
                                
                                    o.opportunityid = oo.sfid
                                    
                                LEFT JOIN

                                    salesforce.contract__c cont
                                    
                                ON
                                
                                    oo.sfid = cont.opportunity__c
                                    
                                WHERE
                                
                                    o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                                    AND oo.status__c IN ('RESIGNED', 'CANCELLED')
                                    AND o.type = 'cleaning-b2b'
                                    AND o.professional__c IS NOT NULL
                                    AND o.test__c IS FALSE
                                    AND oo.test__c IS FALSE
                                    AND cont.service_type__c = 'maintenance cleaning'
                                
                                GROUP BY
                                
                                    LEFT(o.locale__c, 2),
                                    type_date,
                                    o.opportunityid) as t1
                                    
                            LEFT JOIN
                            
                                (SELECT
                                
                                    t3.*,
                                    t3.contract_end as date_churn,
                                    CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
                                
                                FROM    
                                    
                                    (SELECT
                                    
                                        t1.country,
                                        t1.opportunityid,
                                        t1.date_last_order,
                                        t2.date_churn as contract_end
                                    
                                    FROM
                                    
                                        (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                                                 -- It's the last day on which they are making money
                                            LEFT(o.locale__c, 2) as country,
                                            o.opportunityid,
                                            'last_order' as type_date,
                                            MAX(o.effectivedate) as date_last_order
                                            
                                        FROM
                                        
                                            salesforce.order o
                                            
                                        LEFT JOIN
                                        
                                            salesforce.opportunity oo
                                            
                                        ON 
                                        
                                            o.opportunityid = oo.sfid
                                            
                                        LEFT JOIN

                                            salesforce.contract__c cont
                                            
                                        ON
                                        
                                            oo.sfid = cont.opportunity__c
                                            
                                        WHERE
                                        
                                            o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                                            AND oo.status__c IN ('RESIGNED', 'CANCELLED')
                                            AND o.type = 'cleaning-b2b'
                                            AND o.professional__c IS NOT NULL
                                            AND o.test__c IS FALSE
                                            AND oo.test__c IS FALSE
                                            AND cont.service_type__c = 'maintenance cleaning'
                                        
                                        GROUP BY
                                        
                                            LEFT(o.locale__c, 2),
                                            type_date,
                                            o.opportunityid) as t1
                                            
                                    LEFT JOIN
                                    
                                        (SELECT -- Here we make the list of opps with their confirmed end or end date when available
                                                                
                                            o.opportunity__c as opportunityid,

                                            MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
                                        
                                        FROM
                                        
                                            salesforce.contract__c o
                                        
                                        WHERE
                                        
                                            o.test__c IS FALSE
                                            AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
                                            AND o.service_type__c LIKE 'maintenance cleaning'
                                            -- AND o.active__c IS TRUE 
                                            -- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
                                            
                                        GROUP BY
                                        
                                            o.opportunity__c) as t2 
                                    
                                    ON
                                    
                                        t1.opportunityid = t2.opportunityid) as t3) as t2 
                            
                            ON
                            
                                t1.opportunityid = t2.opportunityid) as t3) as t2
                            
                    ON
                    
                        t1.opportunityid = t2.opportunityid) as table_dates
                        
                ON 
                
                    time_table.key = table_dates.key_link
                    
            ) as t2
                
        LEFT JOIN
        
            salesforce.opportunity o
            
        ON
        
            t2.opportunityid = o.sfid
            
        LEFT JOIN
        
            salesforce.user ct
            
        ON
        
            o.closed_by__c = ct.sfid
            
        LEFT JOIN
            
            salesforce.user usr
            
        ON
        
            o.ownerid = usr.sfid

        LEFT JOIN

            bi.potential_revenue_per_opp ooo

        ON

            t2.opportunityid = ooo.opportunityid
            
        LEFT JOIN
        
            salesforce.contract__c cont
            
        ON
        
            o.sfid = cont.opportunity__c

        WHERE 

            o.test__c IS FALSE
            AND cont.test__c IS FALSE
            AND cont.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
            AND cont.service_type__c LIKE 'maintenance cleaning'
            
        GROUP BY
        
                ooo.potential,
                o.status__c,
                o.closed_by__c,
                o.ownerid,
                ct.name,
                usr.name,
                t2.key, 
                t2.year_month,
                t2.ymd,
                t2.ymd_max,
                t2.key_link,
                t2.country,
                t2.locale__c,
                t2.polygon,
                t2.opportunityid,
                t2.date_start,
                t2.year_month_start,
                t2.date_churn,
                t2.year_month_churn,
                t2.category
            ) as table1

    WHERE

        table1.opportunityid IS NOT NULL
            
    GROUP BY
    
        table1.year_month,
        table1.ymd_max,
        table1.country,
        LEFT(table1.locale__c, 2),
        table1.locale__c,
        table1.polygon,
        CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
              WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
              WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
              WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
              WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
              WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
              WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
              WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
              WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
              WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
              WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
              WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
              WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
              WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
              WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
              WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
              WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
              WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
              ELSE '>M18'
              END,
              sub_kpi_3,
        sub_kpi_4,
        sub_kpi_5,
        table1.category,
        table1.opportunityid,
        table1.closed_by
        
    ORDER BY
    
        table1.year_month desc)  as table2
    
GROUP BY

    year_month,
    date,
    -- max_date,
    locale,
    languages,
    city,
    type,
    kpi,
    sub_kpi_1,  
    sub_kpi_2,
    sub_kpi_3,
    sub_kpi_4,
    sub_kpi_5
    -- table2.closed_by
    
ORDER BY 

    year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Running Revenue (Contract Based)
-- Desc: Grand total amount on the contract. Average invoiced revenue of last 3 months based on contract

SELECT

    table3.year_month,
    table3.date,
    table3.locale,
    table3.languages,
    table3.city,
    table3.type,
    table3.kpi,
    table3.sub_kpi_1,   
    table3.sub_kpi_2,
    table3.sub_kpi_3,
    table3.sub_kpi_4,
    table3.sub_kpi_5,
    -- table3.opportunityid,
    SUM(CASE WHEN table3.value > 0 THEN table3.grand_total ELSE 0 END) as value

FROM
    
    (SELECT
    
        year_month,
        date,
        locale,
        languages,
        city,
        type,
        kpi,
        sub_kpi_1,  
        sub_kpi_2,
        sub_kpi_3,
        sub_kpi_4,
        sub_kpi_5,
        table2.opportunityid,
        SUM(table2.grand_total) as grand_total,
        SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
    
    FROM    
        
        (SELECT 
        
            table1.year_month as year_month,
            MIN(table1.ymd) as date,
            table1.ymd_max as max_date,
            table1.country as locale,
            table1.locale__c as languages,
            table1.polygon as city,
            CAST('B2B' as varchar) as type,
            CAST('Running Opps Beta' as varchar) as kpi,
            CAST('Revenue' as varchar) as sub_kpi_1,    
            CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
                  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
                  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
                  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
                  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
                  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
                  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
                  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
                  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
                  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
                  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
                  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
                  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
                  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
                  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
                  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
                  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
                  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
                  ELSE '>M18'
                  END as sub_kpi_2,
            CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
            table1.closed_by,
            CASE WHEN table1.grand_total < 250 THEN '<250€'
              WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
              WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
              ELSE '>1000€'
              END as sub_kpi_4,
                  
            CASE WHEN table1.grand_total < 250 THEN 'Very Small'
              WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
              WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
              ELSE 'Key Account'
              END as sub_kpi_5,
            table1.opportunityid,
            table1.category,
            table1.grand_total
            -- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
            
        FROM
            
            (SELECT
            
                t2.*,
                o.status__c as status_now,
                ct.name as name_closer,
                o.closed_by__c as closed_by,
                o.ownerid,
                usr.name as name_owner,
                ooo.potential as grand_total
            
            FROM
                
                (SELECT
                    
                        time_table.*,
                        table_dates.*,
                        CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
                    
                    FROM    
                        
                        (SELECT
                            
                            '1'::integer as key,    
                            TO_CHAR(i, 'YYYY-MM') as year_month,
                            MIN(i) as ymd,
                            MAX(i) as ymd_max
                            -- i::date as date 
                            
                        FROM
                        
                            generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
                            
                        GROUP BY
                        
                            key,
                            year_month
                            -- date
                            
                        ORDER BY 
                        
                            year_month desc) as time_table
                            
                    LEFT JOIN
                    
                        (SELECT
                        
                            '1'::integer as key_link,
                            t1.country,
                            t1.locale__c,
                            t1.delivery_area__c as polygon,
                            t1.opportunityid,
                            t1.date_start,
                            TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
                            CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
                            CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
                            
                        
                        FROM
                            
                            ((SELECT
            
                                LEFT(o.locale__c, 2) as country,
                                o.locale__c,
                                o.opportunityid,
                                oo.delivery_area__c,
                                MIN(o.effectivedate) as date_start
                            
                            FROM
                            
                                salesforce.order o

                            LEFT JOIN
                
                                salesforce.opportunity oo
                                
                            ON 
                            
                                o.opportunityid = oo.sfid
                                
                            LEFT JOIN

                                salesforce.contract__c cont
                                
                            ON
                            
                                oo.sfid = cont.opportunity__c
                                
                            WHERE
                            
                                o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                                AND o.type = 'cleaning-b2b'
                                AND o.professional__c IS NOT NULL
                                AND o.test__c IS FALSE
                                AND cont.service_type__c = 'maintenance cleaning'
                                
                            GROUP BY
                            
                                o.opportunityid,
                                o.locale__c,
                                oo.delivery_area__c,
                                LEFT(o.locale__c, 2))) as t1
                                
                        LEFT JOIN
                        
                            (SELECT
                            
                                t3.*,
                                t3.contract_end as date_churn,
                                CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
                            
                            FROM    
                                
                                (SELECT
                                
                                    t1.country,
                                    t1.opportunityid,
                                    t1.date_last_order,
                                    t2.date_churn as contract_end
                                
                                FROM
                                
                                    (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                                             -- It's the last day on which they are making money
                                        LEFT(o.locale__c, 2) as country,
                                        o.opportunityid,
                                        'last_order' as type_date,
                                        MAX(o.effectivedate) as date_last_order
                                        
                                    FROM
                                    
                                        salesforce.order o
                                        
                                    LEFT JOIN
                                    
                                        salesforce.opportunity oo
                                        
                                    ON 
                                    
                                        o.opportunityid = oo.sfid
                                        
                                    LEFT JOIN

                                        salesforce.contract__c cont
                                        
                                    ON
                                    
                                        oo.sfid = cont.opportunity__c
                                        
                                    WHERE
                                    
                                        o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                                        AND oo.status__c IN ('RESIGNED', 'CANCELLED')
                                        AND o.type = 'cleaning-b2b'
                                        AND o.professional__c IS NOT NULL
                                        AND o.test__c IS FALSE
                                        AND oo.test__c IS FALSE
                                        AND cont.service_type__c = 'maintenance cleaning'
                                    
                                    GROUP BY
                                    
                                        LEFT(o.locale__c, 2),
                                        type_date,
                                        o.opportunityid) as t1
                                        
                                LEFT JOIN
                                
                                    (SELECT -- Here we make the list of opps with their confirmed end or end date when available
                                                            
                                        o.opportunity__c as opportunityid,
                                        MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
                                    
                                    FROM
                                    
                                        salesforce.contract__c o
                                    
                                    WHERE
                                    
                                        o.test__c IS FALSE
                                        AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
                                        AND o.service_type__c LIKE 'maintenance cleaning'
                                        -- AND o.active__c IS TRUE 
                                        -- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
                                        
                                    GROUP BY
                                    
                                        o.opportunity__c) as t2 
                                
                                ON
                                
                                    t1.opportunityid = t2.opportunityid) as t3) as t2
                                
                        ON
                        
                            t1.opportunityid = t2.opportunityid) as table_dates
                            
                    ON 
                    
                        time_table.key = table_dates.key_link
                        
                ) as t2
                    
            LEFT JOIN
            
                salesforce.opportunity o
                
            ON
            
                t2.opportunityid = o.sfid
            
            LEFT JOIN
        
                salesforce.user ct
                
            ON
            
                o.closed_by__c = ct.sfid
                
            LEFT JOIN
                
                salesforce.user usr
                
            ON
            
                o.ownerid = usr.sfid
                
            LEFT JOIN
            
                salesforce.contract__c oo
                
            ON
            
                t2.opportunityid = oo.opportunity__c

            LEFT JOIN

                bi.potential_revenue_per_opp ooo

            ON
                
                t2.opportunityid = ooo.opportunityid
    
            LEFT JOIN
        
                salesforce.contract__c cont
            
            ON
        
                o.sfid = cont.opportunity__c

            WHERE 

                o.test__c IS FALSE
                AND cont.test__c IS FALSE
                AND cont.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
                AND cont.service_type__c LIKE 'maintenance cleaning'
                
            GROUP BY
            
                    ooo.potential,
                    o.status__c,
                    t2.key, 
                    ct.name,
                    o.closed_by__c,
                    o.ownerid,
                    usr.name,
                    t2.year_month,
                    t2.ymd,
                    t2.ymd_max,
                    t2.key_link,
                    t2.country,
                    t2.locale__c,
                    t2.polygon,
                    t2.opportunityid,
                    t2.date_start,
                    t2.year_month_start,
                    t2.date_churn,
                    t2.year_month_churn,
                    t2.category
                ) as table1
                
        GROUP BY
        
            table1.year_month,
            table1.ymd_max,
            table1.country,
            LEFT(table1.locale__c, 2),
            table1.locale__c,
            table1.polygon,
            CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
                  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
                  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
                  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
                  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
                  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
                  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
                  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
                  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
                  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
                  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
                  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
                  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
                  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
                  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
                  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
                  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
                  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
                  ELSE '>M18'
                  END,
                  sub_kpi_3,
            sub_kpi_4,
            sub_kpi_5,
            table1.category,
            table1.opportunityid,
            table1.grand_total,
            table1.closed_by
            
        ORDER BY
        
            table1.year_month desc)  as table2
            
    WHERE
    
        -- table2.opportunityid = '0060J00000qCkNQQA0'
    
        table2.category = 'RUNNING'
            
    GROUP BY
    
        year_month,
        date,
        max_date,
        locale,
        languages,
        city,
        type,
        kpi,
        sub_kpi_1,  
        sub_kpi_2,
        sub_kpi_3,
        sub_kpi_4,
        sub_kpi_5,
        table2.opportunityid
        
    ORDER BY 
    
        year_month desc) as table3
        
GROUP BY

    table3.year_month,
    table3.date,
    table3.locale,
    table3.languages,
    table3.city,
    table3.type,
    table3.kpi,
    table3.sub_kpi_1,   
    table3.sub_kpi_2,
    table3.sub_kpi_3,
    table3.sub_kpi_4,
    table3.sub_kpi_5
    -- table3.opportunityid
    
ORDER BY

    table3.year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Revenue Partners
-- Desc: Revenue generated by partners (order provider is partners)
 
SELECT 
  TO_CHAR(o.issued__c, 'YYYY-MM') as year_month, 
  MIN(o.issued__c) as mindate, 
  LEFT(o.locale__c, 2) as locale, 
  o.locale__c as languages, 
  ooo.delivery_area__c as city, 
  'B2B' as type, 
  'Revenue' as kpi, 
  'Partner' as sub_kpi_1, 
  oo.company_name as sub_kpi_2, 
  oo.opportunity as sub_kpi_3, 
  o.service_type__c as sub_kpi_4, 
  '-' as sub_kpi_5, 
  MAX(o.amount__c)/ 1.19 as value 
FROM 
  salesforce.invoice__c o 
  LEFT JOIN bi.order_provider oo ON o.opportunity__c = oo.opportunityid 
  LEFT JOIN salesforce.opportunity ooo ON o.opportunity__c = ooo.sfid 
WHERE 
  oo.provider = 'Partner' 
  AND o.test__C IS FALSE -- AND o.service_type__c = 'maintenance cleaning'
  AND o.name NOT LIKE '%3359487-112%' -- big mistake in invoicing, it fucks up the results
  AND oo.company_name NOT LIKE '%BAT Business Services GmbH' 
GROUP BY 
  year_month, 
  LEFT(o.locale__c, 2), 
  o.locale__c, 
  ooo.delivery_area__c, 
  oo.company_name, 
  oo.opportunity, 
  o.service_type__c 
ORDER BY 
  year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Revenue Netto (Finance Adjusted)
-- Desc: Net revenue of B2B adjusted with the corrections made to the invoices afterwards for maintenance cleaning

SELECT
        
    t2.date_part,
    t2.date,
    t2.locale,
    t2.languages,
    t2.city,
    t2.type,
    t2.kpi,
    t2.sub_kpi_1,
    t2.sub_kpi_2,
    CASE WHEN t2.sub_kpi_3 IS NULL THEN 'Partner' ELSE t2.sub_kpi_3 END as sub_kpi_3,
    t2.sub_kpi_4,
    t2.sub_kpi_5,
    -- t2.first_invoice,
    -- (t2.amount_first_invoice)/1.19 as amount_first_invoice,
    -- t2.original_invoice__c,
    -- t2.number_invoices,
    -- SUM(t2.final_amount_invoiced)/1.19 - (t2.amount_first_invoice)/1.19 as correction,
    SUM(t2.final_amount_invoiced)/1.19 as value

FROM
    
    (SELECT 
        
        TO_CHAR(t1.date,'YYYY-MM') as date_part,
        MIN(t1.date) as date,
        t1.locale,
        t1.languages as languages,
        t1.city as city,
        t1.type,
        t1.kpi,
        t1.sub_kpi_1,
        t1.sub_kpi_2,
        t1.sub_kpi_3,
        t1.sub_kpi_4,
        t1.sub_kpi_5,
        
        t1.name_first_invoice,
        t1.opportunity__c,
    
        t1.date_first_invoice,
        t1.first_invoice,
        t1.first_amount,
        t1.original_invoice__c,
        -- t1.name_second_invoice,
        -- MAX(t1.date_second_invoice) as date_last_invoice,
        SUM(t1.first_amount)/COUNT(t1.first_invoice) as amount_first_invoice,
        SUM(t1.first_amount) as sum_first_amount,
        SUM(t1.second_amount) as sum_second_amount,
        COUNT(t1.first_invoice) as number_invoices,
        CASE WHEN t1.original_invoice__c IS NULL THEN SUM(t1.first_amount)
             ELSE SUM(t1.first_amount)/COUNT(t1.first_invoice) + SUM(t1.second_amount)
              END as final_amount_invoiced
        
    FROM
    
        (SELECT

            -- TO_CHAR(o.issued__c,'YYYY-MM') as date_part,
            MIN(o.issued__c::date) as date,
            LEFT(o.locale__c,2) as locale,
            o.locale__c as languages,
            oooo.delivery_area__c as city,
            CAST('B2B' as varchar) as type,
            CAST('Revenue Adjusted' as varchar) as kpi,
            CAST('Netto' as varchar) as sub_kpi_1,
            CAST('Monthly' as varchar) as sub_kpi_2,
            op.provider as sub_kpi_3,
            o.service_type__c as sub_kpi_4,
            CAST('-' as varchar) as sub_kpi_5,
            o.sfid as first_invoice,
            o.name as name_first_invoice,           
            o.opportunity__c,
            o.createddate as date_first_invoice,
            o.amount__c as first_amount,
            oo.original_invoice__c,
            oo.name as name_second_invoice,
            oo.createddate as date_second_invoice,
            oo.sfid as second_invoice,
            oo.amount__c as second_amount   
            
        FROM
            
            salesforce.invoice__c o
            
        LEFT JOIN
            
            salesforce.invoice__c oo
            
        ON
            
            o.sfid = oo.original_invoice__c
            
        LEFT JOIN
            
            salesforce.order ooo
            
        ON 
            
            o.opportunity__c = ooo.opportunityid

        LEFT JOIN
                
            salesforce.opportunity oooo
            
        ON 
        
            o.opportunity__c = oooo.sfid
            
        LEFT JOIN

            bi.order_provider op
            
        ON
        
            oooo.sfid = op.opportunityid
            
        WHERE
            
            o.opportunity__c IS NOT NULL
            AND o.test__c IS FALSE
            AND o.issued__c IS NOT NULL
            AND o.original_invoice__c IS NULL

            -- AND (oo.parent_invoice__c = 'a000J00000yP4cCQAS' OR o.sfid = 'a000J00000yP4cCQAS')
            
        GROUP BY
            
            -- TO_CHAR(o.issued__c,'YYYY-MM'),
            LEFT(o.locale__c,2),
            o.locale__c,
            oooo.delivery_area__c,
            o.name,
            o.opportunity__c,
            o.issued__c,
            o.createddate,
            o.sfid,
            o.amount__c,
            oo.original_invoice__c,
            oo.name,
            oo.createddate,
            oo.sfid,
            oo.amount__c,
            op.provider,
            o.service_type__c
            
        ORDER BY
        
            date desc) as t1
            
            
    GROUP BY
    
        TO_CHAR(t1.date,'YYYY-MM'),
        t1.locale,
        t1.languages,
        t1.city,
        t1.type,
        t1.kpi,
        t1.sub_kpi_1,
        t1.sub_kpi_2,
        t1.sub_kpi_3,
        t1.sub_kpi_4,
        t1.sub_kpi_5,
        t1.name_first_invoice,
        t1.opportunity__c,
        t1.date_first_invoice,
        t1.first_invoice,
        t1.first_amount,
        t1.original_invoice__c) as t2
        
        
GROUP BY

    t2.date_part,
    t2.date,
    t2.locale,
    t2.languages,
    t2.city,
    t2.type,
    t2.kpi,
    t2.sub_kpi_1,
    t2.sub_kpi_2,
    t2.sub_kpi_3,
    t2.sub_kpi_4,
    t2.sub_kpi_5
    -- t2.first_invoice,
    -- t2.amount_first_invoice,
    -- t2.number_invoices,
    -- t2.original_invoice__c
    
ORDER BY

    date_part desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- New Opps Revenue
-- Desc: Revenue of new started opportunities

SELECT 
  TO_CHAR(t1.first_order, 'YYYY-MM') as year_month, 
  MIN(t1.first_order) as mindate, 
  LEFT(t1.locale__c, 2) as locale, 
  t1.locale__c as languages, 
  t1.delivery_area__c, 
  CAST('B2B' as varchar) as type, 
  CAST('New' as varchar) as kpi, 
  CAST('Revenue' as varchar) as sub_kpi_1, 
  CAST('Monthly' as varchar) as sub_kpi_2, 
  CAST('Opps Started' as varchar) as sub_kpi_3, 
  CASE WHEN t1.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4, 
  CAST('-' as varchar) as sub_kpi_5, 
  SUM(t1.potential) as value 
FROM 
  (
    SELECT 
      -- This first query is building a table showing the first order date for every opportunity
      o.sfid, 
      o.name, 
      o.locale__c, 
      o.email__c, 
      o.delivery_area__c, 
      o.acquisition_channel__c, 
      MIN(oo.effectivedate) as first_order, 
      ooo.potential 
    FROM 
      salesforce.opportunity o 
      LEFT JOIN salesforce.order oo ON o.sfid = oo.opportunityid 
      LEFT JOIN bi.potential_revenue_per_opp ooo ON o.sfid = ooo.opportunityid 
      LEFT JOIN salesforce.contract__c cont ON o.sfid = cont.opportunity__c 
    WHERE 
      -- o.status__c = 'RUNNING'
      o.test__c IS FALSE 
      AND oo.test__c IS FALSE 
      AND oo."status" IN (
        'INVOICED', 'PENDING TO START', 'FULFILLED', 
        'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
      ) 
      AND oo.professional__C IS NOT NULL 
      AND cont.service_type__c = 'maintenance cleaning' 
    GROUP BY 
      o.sfid, 
      o.name, 
      o.locale__c, 
      o.delivery_area__c, 
      o.acquisition_channel__c, 
      o.email__c, 
      ooo.potential
  ) as t1 
GROUP BY 
  TO_CHAR(t1.first_order, 'YYYY-MM'), 
  LEFT(t1.locale__c, 2), 
  t1.locale__c, 
  t1.delivery_area__c, 
  CASE WHEN t1.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END 
ORDER BY 
  TO_CHAR(t1.first_order, 'YYYY-MM') desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Churned Revenue
-- Desc: Revenue of churned opportunities

SELECT

    TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
    MIN(table1.date_churn::date) as date,
    LEFT(table1.country,2) as locale,
    table1.locale__c as languages,
    -- CAST('-' as varchar) as city,
    table1.delivery_area__c as city,
    CAST('B2B' as varchar) as type,
    CAST('Churn' as varchar) as kpi,
    CAST('Revenue' as varchar) as sub_kpi_1,
    
    CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
              WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
              WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
              WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
              WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
              WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
              WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
              WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
              WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
              WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
              WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
              WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
              WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
              WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
              WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
              WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
              WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
              WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'            
              ELSE '>M18'
              END as sub_kpi_2,
    
    CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
          WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
          ELSE 'Unlimited'
          END as sub_kpi_3,
          
    CASE WHEN table1.grand_total < 250 THEN '<250€'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
          WHEN table1.grand_total IS NULL THEN 'Unknown'
          ELSE '>1000€'
          END as sub_kpi_4,
              
    CASE WHEN table1.grand_total < 250 THEN 'Very Small'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
          WHEN table1.grand_total IS NULL THEN 'Unknown'
          ELSE 'Key Account'
          END as sub_kpi_5,
    -- table1.opportunityid,
    SUM(table1.grand_total) as value

FROM
    
    (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
             -- It's the last day on which they are making money
        LEFT(oo.locale__c, 2) as country,
        oo.locale__c,
        oo.delivery_area__c,
        o.opportunityid,
        ooo.potential as grand_total,
        'last_order' as type_date,
        MIN(o.effectivedate) as date_start,
        MAX(o.effectivedate) as date_churn
        
    FROM
    
        salesforce.order o
        
    LEFT JOIN
    
        salesforce.opportunity oo
        
    ON 
    
        o.opportunityid = oo.sfid
        
    LEFT JOIN
    
        bi.potential_revenue_per_opp ooo
        
    ON
    
        o.opportunityid = ooo.opportunityid

    LEFT JOIN

        salesforce.contract__c cont
    
    ON 

        oo.sfid = cont.opportunity__c
        
    WHERE
    
        o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
        AND o.test__c IS FALSE
        AND o.professional__c IS NOT NULL
        AND oo.status__c IN ('RESIGNED', 'CANCELLED')
        AND oo.test__c IS FALSE
        -- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
        AND cont.service_type__c = 'maintenance cleaning'
    
    GROUP BY
    
        LEFT(oo.locale__c, 2),
        oo.locale__c,
        oo.delivery_area__c,
        ooo.potential,
        type_date,
        o.opportunityid) as table1
        
GROUP BY

    TO_CHAR(table1.date_churn, 'YYYY-MM'),
    -- CASE WHEN oooo.grand_total__c IS NULL THEN oooo.pph__c*4.3*oooo.hours_weekly__c ELSE oooo.grand_total__c END,
    table1.country,
    table1.locale__c,
    table1.delivery_area__c,
    sub_kpi_2,
    sub_kpi_3,
    sub_kpi_4,
    sub_kpi_5
    -- table1.opportunityid
    
ORDER BY

    TO_CHAR(table1.date_churn, 'YYYY-MM') desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Offboarding Revenue & Retention Revenue
-- Desc: Revenue of opportunities in the status of offboarding/retention

SELECT

    table3.year_month,
    table3.date,
    table3.locale,
    table3.languages,
    table3.city,
    table3.type,
    table3.kpi,
    table3.sub_kpi_1,   
    table3.sub_kpi_2,
    table3.sub_kpi_3,
    table3.sub_kpi_4,
    table3.sub_kpi_5,
    -- table3.opportunityid,
    SUM(CASE WHEN table3.value > 0 THEN table3.grand_total ELSE 0 END) as value

FROM
    
    (SELECT
    
        year_month,
        date,
        locale,
        languages,
        city,
        type,
        kpi,
        sub_kpi_1,  
        sub_kpi_2,
        sub_kpi_3,
        sub_kpi_4,
        sub_kpi_5,
        table2.opportunityid,
        SUM(table2.grand_total) as grand_total,
        SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
    
    FROM    
        
        (SELECT 
        
            table1.year_month as year_month,
            MIN(table1.ymd) as date,
            table1.ymd_max as max_date,
            table1.country as locale,
            table1.locale__c as languages,
            table1.polygon as city,
            CAST('B2B' as varchar) as type,
            CAST('Offboarding/Retention' as varchar) as kpi,
            CAST('Revenue' as varchar) as sub_kpi_1,    
            CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
                  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
                  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
                  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
                  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
                  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
                  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
                  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
                  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
                  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
                  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
                  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
                  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
                  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
                  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
                  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
                  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
                  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
                  ELSE '>M18'
                  END as sub_kpi_2,
            table1.status_now as sub_kpi_3,
            CASE WHEN table1.grand_total < 250 THEN '<250€'
              WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
              WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
              ELSE '>1000€'
              END as sub_kpi_4,
                  
            CASE WHEN table1.grand_total < 250 THEN 'Very Small'
              WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
              WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
              ELSE 'Key Account'
              END as sub_kpi_5,
            table1.opportunityid,
            table1.category,
            table1.grand_total
            -- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
            
        FROM
            
            (SELECT
            
                t2.*,
                o.status__c as status_now,
                ooo.potential as grand_total
            
            FROM
                
                (SELECT
                    
                        time_table.*,
                        table_dates.*,
                        CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
                    
                    FROM    
                        
                        (SELECT
                            
                            '1'::integer as key,    
                            TO_CHAR(i, 'YYYY-MM') as year_month,
                            MIN(i) as ymd,
                            MAX(i) as ymd_max
                            -- i::date as date 
                            
                        FROM
                        
                            generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
                            
                        GROUP BY
                        
                            key,
                            year_month
                            -- date
                            
                        ORDER BY 
                        
                            year_month desc) as time_table
                            
                    LEFT JOIN
                    
                        (SELECT
                        
                            '1'::integer as key_link,
                            t1.country,
                            t1.locale__c,
                            t1.delivery_area__c as polygon,
                            t1.opportunityid,
                            t1.date_start,
                            TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
                            CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
                            CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
                            
                        
                        FROM
                            
                            ((SELECT
            
                                LEFT(o.locale__c, 2) as country,
                                o.locale__c,
                                o.opportunityid,
                                oo.delivery_area__c,
                                MIN(o.effectivedate) as date_start
                            
                            FROM
                            
                                salesforce.order o

                            LEFT JOIN
                
                                salesforce.opportunity oo
                                
                            ON 
                            
                                o.opportunityid = oo.sfid

                            LEFT JOIN

                                salesforce.contract__c cont
                            
                            ON 

                                oo.sfid = cont.opportunity__c
                                
                            WHERE
                            
                                o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                                AND o.type = 'cleaning-b2b'
                                AND o.professional__c IS NOT NULL
                                AND o.test__c IS FALSE
                                AND cont.service_type__c = 'maintenance cleaning'
                                
                            GROUP BY
                            
                                o.opportunityid,
                                o.locale__c,
                                oo.delivery_area__c,
                                LEFT(o.locale__c, 2))) as t1
                                
                        LEFT JOIN
                        
                            (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                                     -- It's the last day on which they are making money
                                LEFT(o.locale__c, 2) as country,
                                o.opportunityid,
                                'last_order' as type_date,
                                MAX(o.effectivedate) as date_churn
                                
                            FROM
                            
                                salesforce.order o
                                
                            LEFT JOIN
                            
                                salesforce.opportunity oo
                                
                            ON 
                            
                                o.opportunityid = oo.sfid

                            LEFT JOIN

                                salesforce.contract__c cont
                            
                            ON 

                                oo.sfid = cont.opportunity__c
                                
                            WHERE
                            
                                o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                                AND oo.status__c IN ('RESIGNED', 'CANCELLED')
                                AND oo.test__c IS FALSE
                                AND o.professional__c IS NOT NULL
                                AND o.test__c IS FALSE
                                AND cont.service_type__c = 'maintenance cleaning'
                            
                            GROUP BY
                            
                                LEFT(o.locale__c, 2),
                                type_date,
                                o.opportunityid) as t2
                                
                        ON
                        
                            t1.opportunityid = t2.opportunityid) as table_dates
                            
                    ON 
                    
                        time_table.key = table_dates.key_link
                        
                ) as t2
                    
            LEFT JOIN
            
                salesforce.opportunity o
                
            ON
            
                t2.opportunityid = o.sfid
                
            LEFT JOIN
            
                salesforce.contract__c oo
                
            ON
            
                t2.opportunityid = oo.opportunity__c

            LEFT JOIN

                bi.potential_revenue_per_opp ooo

            ON
                
                t2.opportunityid = ooo.opportunityid
    
            WHERE
    
                o.test__c IS FALSE
                AND o.status__c IN ('OFFBOARDING', 'RETENTION')
                AND oo.service_type__c = 'maintenance cleaning'
                -- AND oo.active__c IS TRUE 
                -- AND oo.status__c IN ('ACCEPTED', 'SIGNED')
                
            GROUP BY
            
                    ooo.potential,
                    o.status__c,
                    t2.key, 
                    t2.year_month,
                    t2.ymd,
                    t2.ymd_max,
                    t2.key_link,
                    t2.country,
                    t2.locale__c,
                    t2.polygon,
                    t2.opportunityid,
                    t2.date_start,
                    t2.year_month_start,
                    t2.date_churn,
                    t2.year_month_churn,
                    t2.category
                ) as table1
                
        GROUP BY
        
            table1.year_month,
            table1.ymd_max,
            table1.country,
            LEFT(table1.locale__c, 2),
            table1.locale__c,
            table1.polygon,
            CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
                  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
                  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
                  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
                  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
                  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
                  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
                  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
                  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
                  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
                  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
                  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
                  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
                  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
                  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
                  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
                  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
                  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
                  ELSE '>M18'
                  END,
                  sub_kpi_3,
            sub_kpi_4,
            sub_kpi_5,
            table1.category,
            table1.opportunityid,
            table1.grand_total
            
        ORDER BY
        
            table1.year_month desc)  as table2
            
    WHERE
    
        -- table2.opportunityid = '0060J00000qCkNQQA0'
    
        table2.category = 'RUNNING'
            
    GROUP BY
    
        year_month,
        date,
        max_date,
        locale,
        languages,
        city,
        type,
        kpi,
        sub_kpi_1,  
        sub_kpi_2,
        sub_kpi_3,
        sub_kpi_4,
        sub_kpi_5,
        table2.opportunityid
        
    ORDER BY 
    
        year_month desc) as table3
        
GROUP BY

    table3.year_month,
    table3.date,
    table3.locale,
    table3.languages,
    table3.city,
    table3.type,
    table3.kpi,
    table3.sub_kpi_1,   
    table3.sub_kpi_2,
    table3.sub_kpi_3,
    table3.sub_kpi_4,
    table3.sub_kpi_5
    -- table3.opportunityid
    
ORDER BY

    table3.year_month desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Churned Opps (Contract Based)
-- Desc: Number of churned opportunities

SELECT

    TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
    MIN(table1.date_churn::date) as date,
    LEFT(table1.country,2) as locale,
    table1.locale__c as languages,
    -- CAST('-' as varchar) as city,
    table1.delivery_area__c as city,
    CAST('B2B' as varchar) as type,
    CAST('Churn Beta' as varchar) as kpi,
    CAST('Count opps' as varchar) as sub_kpi_1,
    
    CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
              WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
              WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
              WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
              WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
              WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
              WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
              WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
              WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
              WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
              WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
              WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
              WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
              WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
              WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
              WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
              WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
              WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'            
              ELSE '>M18'
              END as sub_kpi_2,
    
    CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
          
    CASE WHEN table1.grand_total < 250 THEN '<250€'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
          ELSE '>1000€'
          END as sub_kpi_4,
              
    CASE WHEN table1.grand_total < 250 THEN 'Very Small'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
          ELSE 'Key Account'
          END as sub_kpi_5,
    COUNT(DISTINCT table1.opportunityid) as value

FROM
    
    (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
             -- It's the last day on which they are making money
        LEFT(oo.locale__c, 2) as country,
        oo.locale__c,
        oo.delivery_area__c,
        o.opportunityid,
        ct.name as name_closer,
        oo.closed_by__c as closed_by,
        oo.ownerid,
        usr.name as name_owner,
        ooo.potential as grand_total,
        'last_order' as type_date,
        MIN(o.effectivedate) as date_start,
        MAX(t4.date_churn) as date_churn
        
    FROM
    
        salesforce.order o
        
    LEFT JOIN
    
        salesforce.opportunity oo
        
    ON 
    
        o.opportunityid = oo.sfid
        
    LEFT JOIN
        
        salesforce.user ct
        
    ON
    
        oo.closed_by__c = ct.sfid
        
    LEFT JOIN
        
        salesforce.user usr
        
    ON
    
        oo.ownerid = usr.sfid
        
    LEFT JOIN
    
        bi.potential_revenue_per_opp ooo
        
    ON
    
        o.opportunityid = ooo.opportunityid
        
    LEFT JOIN
    
        (SELECT
        
            t3.*,
            CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
            CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
        
        FROM    
            
            (SELECT
            
                t1.country,
                t1.opportunityid,
                t1.date_last_order,
                t2.date_churn as contract_end
            
            FROM
            
                (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                         -- It's the last day on which they are making money
                    LEFT(o.locale__c, 2) as country,
                    o.opportunityid,
                    'last_order' as type_date,
                    MAX(o.effectivedate) as date_last_order
                    
                FROM
                
                    salesforce.order o
                    
                LEFT JOIN
                
                    salesforce.opportunity oo
                    
                ON 
                
                    o.opportunityid = oo.sfid
                    
                LEFT JOIN

                    salesforce.contract__c cont
                    
                ON
                
                    oo.sfid = cont.opportunity__c
                    
                WHERE
                
                    o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                    AND oo.status__c IN ('RESIGNED', 'CANCELLED')
                    AND o.type = 'cleaning-b2b'
                    AND o.professional__c IS NOT NULL
                    AND o.test__c IS FALSE
                    AND oo.test__c IS FALSE
                    AND cont.service_type__c = 'maintenance cleaning'
                
                GROUP BY
                
                    LEFT(o.locale__c, 2),
                    type_date,
                    o.opportunityid) as t1
                    
            LEFT JOIN
            
                (SELECT -- Here we make the list of opps with their confirmed end or end date when available
                                        
                    o.opportunity__c as opportunityid,
                    MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
                
                FROM
                
                    salesforce.contract__c o
                
                WHERE
                
                    o.test__c IS FALSE
                    AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
                    AND o.service_type__c LIKE 'maintenance cleaning'
                    -- AND o.active__c IS TRUE 
                    -- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
                    
                GROUP BY
                
                    o.opportunity__c) as t2 
            
            ON
            
                t1.opportunityid = t2.opportunityid) as t3) as t4
                
    ON
    
        o.opportunityid = t4.opportunityid
        
    WHERE
    
        o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
        AND oo.status__c IN ('RESIGNED', 'CANCELLED')
        AND oo.test__c IS FALSE
        AND o.test__c IS FALSE
        AND o.professional__c IS NOT NULL
    
    GROUP BY
    
        LEFT(oo.locale__c, 2),
        oo.locale__c,
        oo.delivery_area__c,
        ct.name,
        oo.closed_by__c,
        oo.ownerid,
        usr.name,
        type_date,
        grand_total,
        o.opportunityid) as table1
        
GROUP BY

    TO_CHAR(table1.date_churn, 'YYYY-MM'),
    table1.country,
    table1.locale__c,
    table1.delivery_area__c,
    sub_kpi_2,
    sub_kpi_3,
    sub_kpi_4,
    sub_kpi_5
    
ORDER BY

    TO_CHAR(table1.date_churn, 'YYYY-MM') desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Young Churn (<42 days)
-- Desc: Number of opportunities churned when the date of churn occurred in 42 days after the start date (6 weeks of orders planned)

SELECT 
  t2.year_month_churn, 
  MIN(t2.mindate) as mindate, 
  t2.country as locale, 
  t2.locale__c as languages, 
  t2.city as city, 
  CAST('B2B' as varchar) as type, 
  CAST('Young churn' as varchar) as kpi, 
  CAST('Opps' as varchar) as sub_kpi_1, 
  CAST('Monthly' as varchar) as sub_kpi_2, 
  CAST('-' as varchar) as sub_kpi_3, 
  CAST('-' as varchar) as sub_kpi_4, 
  CAST('-' as varchar) as sub_kpi_5, 
  SUM(t2.young_churn) as value 
FROM 
  (
    SELECT 
      t1.year_month_churn, 
      MIN(t1.date_churn) as mindate, 
      t1.country, 
      t1.locale__c, 
      t1.city, 
      -- t1.year_month_churn,
      -- t1.date_churn,
      COUNT(DISTINCT t1.new_opps) as new_opps, 
      SUM(
        CASE WHEN t1.churn_opps IS NOT NULL THEN 1 ELSE 0 END
      ) as opps_churned, 
      SUM(
        CASE WHEN DATE_PART(
          'day', t1.date_churn - t1.date_start
        ) < 42 THEN 1 ELSE 0 END
      ) as young_churn 
    FROM 
      (
        SELECT 
          t1.* 
        FROM 
          (
            SELECT 
              list_start.year_month as year_month_start, 
              list_start.date_start, 
              list_start.country, 
              list_start.locale__c, 
              list_start.city, 
              list_start.opportunityid as new_opps, 
              list_churn.opportunityid as churn_opps, 
              list_churn.year_month as year_month_churn, 
              list_churn.date_churn 
            FROM 
              (
                SELECT 
                  LEFT(o.locale__c, 2) as country, 
                  o.locale__c, 
                  oo.delivery_area__c as city, 
                  MIN(
                    TO_CHAR(o.effectivedate, 'YYYY-MM')
                  ) as year_month, 
                  o.opportunityid, 
                  MIN(o.effectivedate):: timestamp as date_start 
                FROM 
                  salesforce.order o 
                  LEFT JOIN salesforce.opportunity oo ON o.opportunityid = oo.sfid 
                  LEFT JOIN salesforce.contract__c cont ON oo.sfid = cont.opportunity__c 
                WHERE 
                  o.status IN (
                    'INVOICED', 'PENDING TO START', 'FULFILLED', 
                    'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
                  ) 
                  AND o.type = 'cleaning-b2b' 
                  AND o.professional__c IS NOT NULL 
                  AND o.test__c IS FALSE 
                  AND cont.service_type__c = 'maintenance cleaning' 
                GROUP BY 
                  o.opportunityid, 
                  o.locale__c, 
                  oo.delivery_area__c, 
                  LEFT(o.locale__c, 2)
              ) as list_start 
              LEFT JOIN (
                SELECT 
                  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                  -- It's the last day on which they are making money
                  LEFT(oo.locale__c, 2) as country, 
                  o.opportunityid, 
                  MAX(
                    TO_CHAR(o.effectivedate, 'YYYY-MM')
                  ) as year_month, 
                  MAX(o.effectivedate):: timestamp as date_churn 
                FROM 
                  salesforce.order o 
                  LEFT JOIN salesforce.opportunity oo ON o.opportunityid = oo.sfid 
                  LEFT JOIN salesforce.contract__c cont ON oo.sfid = cont.opportunity__c 
                WHERE 
                  o.status IN (
                    'INVOICED', 'PENDING TO START', 'FULFILLED', 
                    'NOSHOW CUSTOMER', 'PENDING ALLOCATION'
                  ) 
                  AND oo.status__c IN ('RESIGNED', 'CANCELLED') 
                  AND oo.test__c IS FALSE 
                  AND o.test__c IS FALSE 
                  AND cont.service_type__c = 'maintenance cleaning' 
                GROUP BY 
                  LEFT(oo.locale__c, 2), 
                  o.opportunityid
              ) as list_churn ON list_start.opportunityid = list_churn.opportunityid
          ) as t1 
          LEFT JOIN salesforce.opportunity oo ON t1.new_opps = oo.sfid
      ) as t1 
    GROUP BY 
      -- year_month_start,
      year_month_churn, 
      t1.country, 
      t1.locale__c, 
      t1.city 
    ORDER BY 
      year_month_churn desc
  ) as t2 
WHERE 
  t2.year_month_churn IS NOT NULL 
GROUP BY 
  t2.year_month_churn, 
  t2.country, 
  t2.locale__c, 
  t2.city 
ORDER BY 
  t2.year_month_churn desc;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Churn Before Start
-- Desc: Number of opportunities churned before start date

ELECT

    TO_CHAR(list_churn.confirmed_end__c,'YYYY-WW') as date_part,
    MIN(list_churn.confirmed_end__c::date) as date,
    LEFT(list_churn.locale__c, 2) as locale,
    list_churn.locale__c as languages,
    list_churn.delivery_area__c as city,
    'B2B' as type,  
    'Churn' as kpi,
    'Before start'  as sub_kpi_1,
    'Weekly' as sub_kpi_2,
    CAST('-' as varchar) as sub_kpi_3,
    CAST('-' as varchar) as sub_kpi_4,
    CAST('-' as varchar) as sub_kpi_5,
    COUNT(DISTINCT list_churn.sfid) as value

FROM


    (SELECT
    
        o.sfid,
        o.name,
        o.locale__c,
        o.delivery_area__c,
        o.grand_total__c,
        -- ooo.amount__c,
        oo.start__c,
        oo.duration__c,
        oo.end__c,
        oo.resignation_date__c,
        oo.confirmed_end__c
    
    
    FROM
    
        salesforce.opportunity o
        
    LEFT JOIN
    
        salesforce.contract__c oo
        
    ON
    
        o.sfid = oo.opportunity__c
        
    LEFT JOIN
    
        salesforce.invoice__c ooo
        
    ON
    
        o.sfid = ooo.opportunity__c
    
    WHERE
    
        o.test__c IS FALSE
        AND o.status__c IN ('RESIGNED', 'CANCELLED')
        AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
        AND oo.service_type__c = 'maintenance cleaning'
        AND ooo.service_type__c = 'maintenance cleaning'
        -- AND ooo.issued__c::text IS NULL
        -- AND ooo.issued__c::text = '2019-04-30'
        -- AND LEFT(oo.confirmed_end__c::text, 7) = '2019-05'
        
    GROUP BY
    
        o.sfid,
        o.name,
        o.grand_total__c,
        o.locale__c,
        o.delivery_area__c,
        -- ooo.amount__c,
        oo.start__c,
        oo.duration__c,
        oo.end__c,
        oo.resignation_date__c,
        oo.confirmed_end__c) as list_churn
        
LEFT JOIN

    (SELECT

        o.opportunityid,
        MIN(o.effectivedate) as first_order
    
    FROM
    
        salesforce.order o
        
    WHERE
    
        o.test__c IS FALSE
        AND o."type" = 'cleaning-b2b'
        AND o."status" IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'CANCELLED CUSTOMER')
        AND o.opportunityid IS NOT NULL
        
    GROUP BY
    
        o.opportunityid) as dates
        
ON

    list_churn.sfid = dates.opportunityid
    
WHERE

    dates.first_order IS NULL
    
GROUP BY

    TO_CHAR(list_churn.confirmed_end__c,'YYYY-WW'),
    LEFT(list_churn.locale__c, 2),
    list_churn.locale__c,
    list_churn.delivery_area__c;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Churned Revenue (Contract Based)
-- Desc: Potential revenue of opportunities churned

SELECT

    TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
    MIN(table1.date_churn::date) as date,
    LEFT(table1.country,2) as locale,
    table1.locale__c as languages,
    -- CAST('-' as varchar) as city,
    table1.delivery_area__c as city,
    CAST('B2B' as varchar) as type,
    CAST('Churn Beta' as varchar) as kpi,
    CAST('Revenue' as varchar) as sub_kpi_1,
    
    CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
              WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
              WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
              WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
              WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
              WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
              WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
              WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
              WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
              WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
              WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
              WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
              WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
              WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
              WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
              WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
              WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
              WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'            
              ELSE '>M18'
              END as sub_kpi_2,
    
    CASE WHEN table1.name_closer IS NULL THEN table1.name_owner ELSE table1.name_closer END as sub_kpi_3,
          
    CASE WHEN table1.grand_total < 250 THEN '<250€'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
          WHEN table1.grand_total IS NULL THEN 'Unknown'
          ELSE '>1000€'
          END as sub_kpi_4,
              
    CASE WHEN table1.grand_total < 250 THEN 'Very Small'
          WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
          WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
          WHEN table1.grand_total IS NULL THEN 'Unknown'
          ELSE 'Key Account'
          END as sub_kpi_5,
    -- table1.opportunityid,
    SUM(table1.grand_total) as value

FROM
    
    (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
             -- It's the last day on which they are making money
        LEFT(oo.locale__c, 2) as country,
        oo.locale__c,
        oo.delivery_area__c,
        o.opportunityid,
        ct.name as name_closer,
        oo.closed_by__c as closed_by,
        oo.ownerid,
        usr.name as name_owner,
        ooo.potential as grand_total,
        'last_order' as type_date,
        MIN(o.effectivedate) as date_start,
        MAX(t4.date_churn) as date_churn
        
    FROM
    
        salesforce.order o
        
    LEFT JOIN
    
        salesforce.opportunity oo
        
    ON 
    
        o.opportunityid = oo.sfid
        
    LEFT JOIN
        
        salesforce.user ct
        
    ON
    
        oo.closed_by__c = ct.sfid
        
    LEFT JOIN
        
        salesforce.user usr
        
    ON
    
        oo.ownerid = usr.sfid
        
    LEFT JOIN
    
        bi.potential_revenue_per_opp ooo
        
    ON
    
        o.opportunityid = ooo.opportunityid
        
    LEFT JOIN
    
        (SELECT
        
            t3.*,
            CASE WHEN t3.contract_end IS NULL THEN t3.date_last_order ELSE t3.contract_end END as date_churn,
            CASE WHEN t3.date_last_order > t3.contract_end THEN 'Wrong confirmed end' ELSE 'Ok' END as check_date
        
        FROM    
            
            (SELECT
            
                t1.country,
                t1.opportunityid,
                t1.date_last_order,
                t2.date_churn as contract_end
            
            FROM
            
                (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
                         -- It's the last day on which they are making money
                    LEFT(o.locale__c, 2) as country,
                    o.opportunityid,
                    'last_order' as type_date,
                    MAX(o.effectivedate) as date_last_order
                    
                FROM
                
                    salesforce.order o
                    
                LEFT JOIN
                
                    salesforce.opportunity oo
                    
                ON 
                
                    o.opportunityid = oo.sfid
                    
                LEFT JOIN

                    salesforce.contract__c cont
                    
                ON
                
                    oo.sfid = cont.opportunity__c
                    
                WHERE
                
                    o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
                    AND oo.status__c IN ('RESIGNED', 'CANCELLED')
                    AND o.type = 'cleaning-b2b'
                    AND o.professional__c IS NOT NULL
                    AND o.test__c IS FALSE
                    AND oo.test__c IS FALSE
                    AND cont.service_type__c = 'maintenance cleaning'
                
                GROUP BY
                
                    LEFT(o.locale__c, 2),
                    type_date,
                    o.opportunityid) as t1
                    
            LEFT JOIN
            
                (SELECT -- Here we make the list of opps with their confirmed end or end date when available
                                        
                    o.opportunity__c as opportunityid,
                    MAX(CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END) as date_churn
                
                FROM
                
                    salesforce.contract__c o
                
                WHERE
                
                    o.test__c IS FALSE
                    AND o.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
                    AND o.service_type__c LIKE 'maintenance cleaning'
                    -- AND o.active__c IS TRUE 
                    -- AND CASE WHEN o.confirmed_end__c IS NULL THEN o.end__c ELSE o.confirmed_end__c END IS NULL
                    
                GROUP BY
                
                    o.opportunity__c) as t2 
            
            ON
            
                t1.opportunityid = t2.opportunityid) as t3) as t4
                
    ON
    
        o.opportunityid = t4.opportunityid
        
    WHERE
    
        o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
        AND o.test__c IS FALSE
        AND o.professional__c IS NOT NULL
        AND oo.status__c IN ('RESIGNED', 'CANCELLED')
        AND oo.test__c IS FALSE
        -- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
    
    GROUP BY
    
        LEFT(oo.locale__c, 2),
        oo.locale__c,
        oo.delivery_area__c,
        ooo.potential,
        ct.name,
        oo.closed_by__c,
        oo.ownerid,
        usr.name,
        type_date,
        o.opportunityid) as table1
        
GROUP BY

    TO_CHAR(table1.date_churn, 'YYYY-MM'),
    -- CASE WHEN oooo.grand_total__c IS NULL THEN oooo.pph__c*4.3*oooo.hours_weekly__c ELSE oooo.grand_total__c END,
    table1.country,
    table1.locale__c,
    table1.delivery_area__c,
    sub_kpi_2,
    sub_kpi_3,
    sub_kpi_4,
    sub_kpi_5
    -- table1.opportunityid
    
ORDER BY

    TO_CHAR(table1.date_churn, 'YYYY-MM') desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- New Inbound/Outbound Signed
-- Desc: Number of new inbound/outbound opportunities signed by Sales, based on their closedate and stages ('WRITTEN CONFIRMATION', 'WON','PENDING')

SELECT 
  TO_CHAR(t2.closedate, 'YYYY-MM') as year_month_signed, 
  MIN(t2.closedate) as mindate, 
  LEFT(t2.locale__c, 2) as locale, 
  t2.locale__c as languages, 
  t2.delivery_area__c as city, 
  CAST('B2B' as varchar) as type, 
  CAST('New' as varchar) as kpi, 
  CAST('Count' as varchar) as sub_kpi_1, 
  CAST('Monthly' as varchar) as sub_kpi_2, 
  CAST('Opps Signed Sales' as varchar) as sub_kpi_3, 
  CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4, 
  CAST('-' as varchar) as sub_kpi_5, 
  COUNT(DISTINCT t2.sfid) as value 
FROM 
  salesforce.opportunity t2 
  LEFT JOIN salesforce.order o ON t2.sfid = o.opportunityid 
  LEFT JOIN salesforce.contract__c cont ON t2.sfid = cont.opportunity__c 
WHERE 
  t2.test__c IS FALSE -- AND t2.stagename IN ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
  AND t2.stagename IN (
    'WRITTEN CONFIRMATION', 'WON', 'PENDING'
  ) 
  AND t2.closed_by__c IS NOT NULL 
  AND cont.service_type__c = 'maintenance cleaning' 
GROUP BY 
  year_month_signed, 
  locale, 
  languages, 
  city, 
  CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END 
ORDER BY 
  year_month_signed desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- New Inbound/Outbound Signed €
-- Desc: Number of new inbound/outbound opportunities signed by Sales, based on their closedate and stages ('WRITTEN CONFIRMATION', 'WON','PENDING')

SELECT 
  t3.year_month_signed, 
  MIN(t3.mindate) as mindate, 
  t3.locale, 
  t3.languages, 
  t3.city, 
  t3.type, 
  t3.kpi, 
  t3.sub_kpi_1, 
  t3.sub_kpi_2, 
  t3.sub_kpi_3, 
  t3.sub_kpi_4, 
  t3.sub_kpi_5, 
  SUM(t3.revenue) as value 
FROM 
  (
    SELECT 
      TO_CHAR(t2.closedate, 'YYYY-MM') as year_month_signed, 
      MIN(t2.closedate) as mindate, 
      LEFT(t2.locale__c, 2) as locale, 
      t2.locale__c as languages, 
      t2.delivery_area__c as city, 
      CAST('B2B' as varchar) as type, 
      CAST('New' as varchar) as kpi, 
      CAST('Revenue' as varchar) as sub_kpi_1, 
      CAST('Monthly' as varchar) as sub_kpi_2, 
      CAST('Opps Signed Sales' as varchar) as sub_kpi_3, 
      CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_4, 
      CAST('-' as varchar) as sub_kpi_5, 
      t2.sfid, 
      CASE WHEN t2.grand_total__c IS NULL THEN t2.amount ELSE t2.grand_total__c END as revenue 
    FROM 
      salesforce.opportunity t2 
      LEFT JOIN salesforce.order o ON t2.sfid = o.opportunityid 
      LEFT JOIN salesforce.contract__c cont ON t2.sfid = cont.opportunity__c 
    WHERE 
      t2.test__c IS FALSE -- AND t2.stagename IN ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
      AND t2.stagename IN (
        'WRITTEN CONFIRMATION', 'WON', 'PENDING'
      ) 
      AND cont.service_type__c = 'maintenance cleaning' 
    GROUP BY 
      year_month_signed, 
      locale, 
      languages, 
      city, 
      CASE WHEN t2.acquisition_channel__c in ('inbound', 'web') THEN 'Inbound' ELSE 'Outbound' END, 
      t2.sfid, 
      CASE WHEN t2.grand_total__c IS NULL THEN t2.amount ELSE t2.grand_total__c END 
    ORDER BY 
      year_month_signed desc
  ) as t3 
GROUP BY 
  t3.year_month_signed, 
  t3.locale, 
  t3.languages, 
  t3.city, 
  t3.type, 
  t3.kpi, 
  t3.sub_kpi_1, 
  t3.sub_kpi_2, 
  t3.sub_kpi_3, 
  t3.sub_kpi_4, 
  t3.sub_kpi_5 
ORDER BY 
  t3.year_month_signed desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Leadgen Costs
-- Desc: Cost of SEM tools to generate leads(likelies)

SELECT 
  LEFT(o.start_date :: text, 7) as date_part, 
  MIN(o.start_date) as mindate, 
  o.locale, 
  o.languages, 
  o.city, 
  'B2B' as type, 
  'Marketing Stats' as kpi, 
  'SEM Costs' as sub_kpi_1, 
  'Actual' as sub_kpi_2, 
  '-' as sub_kpi_3, 
  '-' as sub_kpi_4, 
  '-' as sub_kpi_5, 
  TRUNC(
    SUM(o.value), 
    2
  ) as value 
FROM 
  public.marketing_spending o 
WHERE 
  o.kpi = 'Marketing Costs' 
GROUP BY 
  LEFT(o.start_date :: text, 7), 
  o.locale, 
  o.languages, 
  o.city 
ORDER BY 
  LEFT(o.start_date :: text, 7) desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Leads Generated
-- Desc: Number of leads (likelies) generated

SELECT 
  LEFT(o.createddate :: text, 7) as date_part, 
  MIN(o.createddate) as mindate, 
  LEFT(o.locale__c, 2) as locale, 
  o.locale__c as languages, 
  '-' as city, 
  'B2B' as type, 
  'Marketing Stats' as kpi, 
  'Likelies' as sub_kpi_1, 
  'Actual' as sub_kpi_2, 
  CASE WHEN o.stage__c = 'QUALIFIED' THEN 'Qualified' WHEN o.stage__c = 'ENDED' THEN 'Ended' ELSE 'Pipeline' END as sub_kpi_3, 
  '-' as sub_kpi_4, 
  '-' as sub_kpi_5, 
  COUNT(DISTINCT o.sfid) as likelies 
FROM 
  salesforce.likeli__c o 
WHERE 
  o.type__c = 'B2B' 
  AND o.acquisition_channel__c IN ('inbound', 'web') 
  AND LEFT(o.locale__c, 2) = 'de' 
  AND o.test__c IS FALSE 
  AND (
    (
      o.lost_reason__c NOT LIKE 'invalid - sem duplicate'
    ) 
    OR o.lost_reason__c IS NULL
  ) 
  AND acquisition_channel__c NOT LIKE 'outbound' 
  AND company_name__c NOT LIKE '%test%' 
  AND company_name__c NOT LIKE '%bookatiger%' 
  AND email__c NOT LIKE '%bookatiger%' 
  AND o.name NOT LIKE '%test%' 
GROUP BY 
  LEFT(o.createddate :: text, 7), 
  LEFT(o.locale__c, 2), 
  o.locale__c, 
  sub_kpi_3 
ORDER BY 
  LEFT(o.createddate :: text, 7) desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



