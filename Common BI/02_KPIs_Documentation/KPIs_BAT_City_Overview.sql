
-- KPIs of BAT City Overview report

-- Tableau dashboard: https://dub01.online.tableau.com/#/site/battigercave/views/BATCityOverview/CityView
-- Table: bi.topline_kpi
-- SQL Function: bi.mfunc_city_overview()


-- B2B Net Revenue
-- Desc: Net invoiced revenue of B2B

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'Invoiced Revenue' as kpi,
  CAST('B2B' as text) as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2b')
  AND o.effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year;

-- B2C Net Revenue
-- Desc: Net invoiced revenue of B2C

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  'Invoiced Revenue' as kpi,
  CAST('B2C' as text) as breakdown,
  SUM(o.gmv_eur_net) as revenue_net

FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND o.test__c IS FALSE 
  AND o.type IN ('cleaning-b2c')
  AND o.effectivedate >= '2018-01-01'
  
GROUP BY

  Month,
  Year;

-- Total Net Revenue
-- Desc: Total invoiced revenue of B2B and B2C

SELECT
    EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
    SUM(o.gmv_eur_net) AS total_net_revenue
FROM
    bi.orders o
WHERE
    o.effectivedate < CURRENT_DATE
    AND o.status IN ('INVOICED',
    'FULFILLED',
    'NOSHOW CUSTOMER',
    'PENDING TO START')
    AND o.test__c IS FALSE
    AND o.type IN ('cleaning-b2c', 'cleaning-b2b')
    AND o.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year;

-- Booked GMV
-- Desc: Revenue booked for B2C orders where order status is not 'Cancelled Faked' or 'Cancelled Mistake'

SELECT
  EXTRACT(Month FROM Order_Creation__C::Date) as Month,
  EXTRACT(Year FROM Order_Creation__C::Date) as Year,
  MIN(Order_Creation__C::Date) as Mindate,
  'GROUP' as value,
  CAST('Booked GMV' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year;

-- One-Off %
-- Desc: Invoiced revenue share of non-recurring B2C orders

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  upper(LEFT(locale__c,2)) as locale,
  'Invoiced Revenue' as kpi,
  CAST('One-Off' as text) as breakdown,
   CASE WHEN min(effectivedate::date) < '2018-01-01'
        THEN SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.08
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
       ELSE  SUM(CASE WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'de' THEN GMV_eur/1.19
                    WHEN type not in ('cleaning-b2b') and left(locale__c,2) = 'ch' THEN GMV_eur/1.077
                    WHEN type in ('cleaning-b2c') and left(locale__c,2) = 'nl' THEN GMV_eur/1.06 
                    WHEN type not in ('cleaning-b2c','cleaning-b2b') and left(locale__c,2) = 'de' THEN (GMV_eur*0.2)/1.19
                    WHEN left(locale__c,2) = 'at' THEN (GMV_eur*0.2)/1.2
                    WHEN type in ('cleaning-b2b') THEN GMV_eur ELSE GMV_eur/1.19 END)
      END
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and (recurrency__c is null or recurrency__c = '0')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  locale;

-- B2B Subcontractor Revenue
-- Desc: Revenue share of subcontractors

SELECT
    EXTRACT(MONTH FROM effectivedate::DATE) AS MONTH,
    EXTRACT(YEAR FROM effectivedate::DATE) AS YEAR,
    MIN(effectivedate::DATE) AS DATE,
    UPPER(LEFT(delivery_area, 2)) AS citya,
    CAST('B2B Revenue' AS TEXT) AS kpi,
    CASE WHEN (t2.Subcon LIKE '%BAT%'
    OR t2.Subcon LIKE '%BOOK%') THEN 'BAT Cleaner'
    ELSE 'Sub Cleaner' END AS contractor,
    SUM(GMV_eur) AS value
FROM
    bi.orders t1
LEFT JOIN (
    SELECT
        a.*,
        t2.name AS subcon
    FROM
        Salesforce.Account a
    JOIN Salesforce.Account t2 ON
        (t2.sfid = a.parentid)
    WHERE
        a.status__c NOT IN ('SUSPENDED')
        AND a.test__c = '0'
        AND a.name NOT LIKE '%test%'
        AND (a.type__c LIKE 'cleaning-b2c'
        OR (a.type__c LIKE '%cleaning-b2c;cleaning-b2b%')
        OR a.type__c LIKE 'cleaning-b2b')) AS t2 ON
    (t1.professional__c = t2.sfid)
WHERE
    status NOT LIKE '%CANCELLED%'
    AND status NOT IN ('INTERNAL ERROR')
    AND order_type = '2'
    AND t1.effectivedate >= '2018-01-01'
GROUP BY
    YEAR,
    MONTH,
    citya,
    contractor;

-- GPM %
-- Desc: Gross profit margin

SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'DE' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('-' as text) as breakdown,
  (CASE WHEN sum(revenue) > 0 THEN SUM(gp)/sum(revenue) ELSE 0 END) as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%de%'
GROUP BY
  year,
  month;

-- GPM % B2B
-- Desc: Gross profit margin of B2B

SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'DE' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2B' as text) as breakdown,
  CASE WHEN sum(b2b_revenue) > 0 THEN SUM(b2b_gp)/sum(b2b_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%de%'
GROUP BY
  year,
  month;

-- GPM % B2C
-- Desc: Gross profit margin of B2C

SELECT
  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  min(date::date) as date,
  'DE' as citya,
  CAST('GPM %' as text) as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN sum(b2c_revenue) > 0 THEN SUM(b2c_gp)/sum(b2c_revenue) ELSE 0 END as value
FROM
  bi.gpm_city_over_time
WHERE
  delivery_area like '%de%'
GROUP BY
  year,
  month;

-- Acquisitions
-- Desc: Number of acquired B2C customers

SELECT
  EXTRACT(MONTH FROM Order_Creation__c::Date) as Month,
  EXTRACT(YEAR FROM Order_Creation__c::date) as year,
  min(Order_Creation__c::date) as mindate,
  city,
  'Acquisitions' as kpi,
  marketing_channel as breakdown,
  COUNT(1) as value
FROM
  bi.orders
WHERE
  Status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  and test__c = '0'
  and Acquisition_New_Customer__c = '1'
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  year,
  city,
  breakdown;
-- Total Customers
-- Desc: Number of total customers 

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(locale__c,2)) as citya,
  'Total Customers' as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;

-- B2B Customers
-- Desc: Total number of customers from B2C 

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(locale__c,2)) as citya,
  'Total Customers' as kpi,
  CAST('B2C' as text) as breakdown,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and order_type = '1'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;

-- B2C Customers
-- Desc: Total number of customers from B2B

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(locale__c,2)) as citya,
  'Total Customers' as kpi,
  CAST('B2C' as text) as breakdown,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and order_type = '2'
  AND effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  citya;

-- COP Cleaner
-- Desc: Number of cleaners(professionals) that has ongoing contracts

SELECT
  EXTRACT(MONTH FROM max_date::date) as Month,
  EXTRACT(YEAR FROM max_date::date) as Year,
  MIN(max_date::date) as mindate,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as cop
FROM
(SELECT
  TO_CHAR(date,'YYYY-MM') as Month,
  MAX(Date) as max_date
FROM
  (select i::date as date from generate_series('2016-01-01', 
  current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
  Month) as a,
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
WHERE
  left(b.locale__c,2) in ('de','nl')
 GROUP BY
  Year,
  Month,
  max_date::date,
  delivery_areas__c;

-- Active Cleaners BAT
-- Desc: Number of active cleaners of BAT where order status is 'INVOICED' or 'PENDING TO START' or 'FULFILLED'

SELECT
    COUNT(DISTINCT(o.professional__c)) AS active_cleaners_BAT,
    TO_CHAR(effectivedate, 'YYYY-MM') AS year_month
FROM
    salesforce.order o
LEFT JOIN salesforce.account a ON
    a.sfid = o.professional__c
WHERE
    a.company_name__c ILIKE '%BAT%'
    AND LEFT(o.delivery_Area__c, 2) = 'de'
    AND o.test__c = '0'
    AND a.test__c = '0'
    AND o.status IN('INVOICED',
    'PENDING TO START',
    'FULFILLED')
GROUP BY
    year_month


-- Active cleaners partners
-- Desc: Number of cleaners of partners where order status is 'INVOICED' or 'PENDING TO START' or 'FULFILLED'


SELECT
    COUNT(DISTINCT(o.professional__c)) AS active_cleaners_partners,
    TO_CHAR(effectivedate, 'YYYY-MM') AS year_month
FROM
    salesforce.order o
LEFT JOIN salesforce.account a ON
    a.sfid = o.professional__c
WHERE
    a.company_name__c NOT ILIKE '%BAT%'
    AND LEFT(o.delivery_Area__c, 2) = 'de'
    AND effectivedate >= '2019-01-01'
    AND o.test__c = '0'
    AND a.test__c = '0'
    AND o.status IN('INVOICED',
    'PENDING TO START',
    'FULFILLED')
GROUP BY 
	year_month

-- Hours per Cleaner BAT
-- Desc: Number of hours realised by cleaners of BAT where order status 'INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START'

SELECT
    EXTRACT(YEAR FROM effectivedate::DATE) AS YEAR,
    EXTRACT(WEEK FROM effectivedate::DATE) AS WEEK,
    MIN(effectivedate::DATE) AS DATE,
    t2.polygon AS POLYGON,
    CAST('Hours per Cleaner BAT' AS TEXT) AS kpi,
    SUM(Order_Duration__c)/ COUNT(DISTINCT(Professional__c))::NUMERIC AS value
FROM
    (
    SELECT
        t1.*,
        t2.name AS subcon
    FROM
        Salesforce.Account t1
    JOIN Salesforce.Account t2 ON
        (t2.sfid = t1.parentid)
    WHERE
        t1.status__c NOT IN ('SUSPENDED')
        AND t1.test__c = '0'
        AND t1.name NOT LIKE '%test%'
        AND (t1.type__c LIKE 'cleaning-b2c'
        OR (t1.type__c LIKE '%cleaning-b2c;cleaning-b2b%')
        OR t1.type__c LIKE 'cleaning-b2b')
        AND t2.name LIKE '%BAT Business Services GmbH%') t1
JOIN bi.orders t2 ON
    (t1.sfid = t2.professional__c)
WHERE
    status IN ('INVOICED',
    'FULFILLED',
    'NOSHOW CUSTOMER',
    'PENDING TO START')
    AND LEFT(t2.locale__C, 2) IN ('de',
    'nl',
    'ch',
    'at')
    AND t2.effectivedate < CURRENT_DATE
    AND t2.effectivedate >= '2018-01-01'
GROUP BY
    WEEK,
    YEAR,
    t2.polygon
ORDER BY
    YEAR DESC,
    WEEK DESC,
    t2.polygon;


-- Hours per Cleaner Partners (realised)
-- Desc: Number of hours realised by cleaners of partners where order status 'INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START'

SELECT
    EXTRACT(YEAR FROM effectivedate::DATE) AS YEAR,
    EXTRACT(WEEK FROM effectivedate::DATE) AS WEEK,
    MIN(effectivedate::DATE) AS DATE,
    UPPER(LEFT(t2.polygon, 2)) AS POLYGON,
    CAST('Hours per Cleaner BAT' AS TEXT) AS kpi,
    SUM(Order_Duration__c)/ COUNT(DISTINCT(Professional__c))::NUMERIC AS value
FROM
    (
    SELECT
        t1.*,
        t2.name AS subcon
    FROM
        Salesforce.Account t1
    JOIN Salesforce.Account t2 ON
        (t2.sfid = t1.parentid)
    WHERE
        t1.status__c NOT IN ('SUSPENDED')
        AND t1.test__c = '0'
        AND t1.name NOT LIKE '%test%'
        AND (t1.type__c LIKE 'cleaning-b2c'
        OR (t1.type__c LIKE '%cleaning-b2c;cleaning-b2b%')
        OR t1.type__c LIKE 'cleaning-b2b')
        AND t2.name LIKE '%BAT Business Services GmbH%') t1
JOIN bi.orders t2 ON
    (t1.sfid = t2.professional__c)
WHERE
    status IN ('INVOICED',
    'FULFILLED',
    'NOSHOW CUSTOMER',
    'PENDING TO START')
    AND LEFT(t2.locale__C, 2) IN ('de',
    'nl',
    'ch',
    'at')
    AND t2.effectivedate < CURRENT_DATE
    AND t2.effectivedate >= '2018-01-01'
GROUP BY
    WEEK,
    YEAR,
    UPPER(LEFT(t2.polygon, 2))
ORDER BY
    YEAR DESC,
    WEEK DESC;

-- Total Hours
-- Desc: Total number of hours realised

SELECT
    EXTRACT(MONTH FROM effectivedate::DATE) AS MONTH,
    EXTRACT(YEAR FROM effectivedate::DATE) AS YEAR,
    MIN(effectivedate::DATE) AS mindate,
    t2.city AS city,
    CAST('Total Hours' AS TEXT) AS kpi,
    CAST('-' AS TEXT) AS breakdown,
    SUM(t2.Order_Duration__c) AS value
FROM
    bi.orders t2
WHERE
    status IN ('INVOICED',
    'FULFILLED',
    'NOSHOW CUSTOMER',
    'PENDING TO START')
    AND t2.effectivedate < CURRENT_DATE
    AND LEFT(t2.locale__C, 2) IN ('de',
    'nl',
    'ch',
    'at')
    AND effectivedate >= '2018-01-01'
GROUP BY
    MONTH,
    YEAR,
    City
ORDER BY
    YEAR DESC,
    MONTH DESC,
    city DESC;


-- Offboardings
-- Number of leaving cleaners

SELECT
  EXTRACT(MONTH FROM hr_contract_end__c::date) as Month,
  EXTRACT(YEAR FROM hr_contract_end__c::date) as Year,
  MIN(hr_contract_end__c::date) as mindate,
  upper(left(delivery_areas__c,2)) as localea,
  CAST('Churn Cleaner' as text) as kpi,
  CAST('-' as text) as breakdown,
  CAST(COUNT(1) as decimal) as value
FROM
(SELECT
    a.*
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE  (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
  and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as account
WHERE
  hr_contract_end__c < current_date
  and LEFT(Locale__c,2) in ('de','nl')
GROUP BY
  Month,
  Year,
  localea;

-- Score
-- Desc: Average rating score of cleaners
SELECT
    EXTRACT(MONTH FROM m.mindate)::TEXT AS MONTH,
    EXTRACT(YEAR FROM m.mindate)::TEXT AS YEAR,
    MIN(m.mindate::DATE) AS mindate1,
    UPPER(LEFT(m.delivery_area, 2)) AS city,
    CAST('Score' AS TEXT) AS kpi,
    '-'::TEXT AS breakdown,
    ROUND(AVG(m.score_cleaners::NUMERIC), 1) AS value
FROM
    bi.margin_per_cleaner m
WHERE
    m.delivery_area NOT IN ('de-hannover',
    'de-monchengladbach',
    'de-wuppertal',
    'de-potsdam',
    'de-bochum',
    'de-dortmund',
    'de-duisburg',
    'de-leipzig',
    'de-manheim',
    'de-bonn;de-cologne',
    'de-dusseldorf;de-cologne')
GROUP BY
    MONTH,
    YEAR,
    city,
    kpi,
    breakdown
ORDER BY
    YEAR DESC,
    MONTH DESC;

-- CPL
-- Desc: Cost per lead, i.e. total cost(classifieds cost, SEM cost, Facebook cost) per lead generated

SELECT
    EXTRACT(MONTH FROM t2.Cleaningdate)::TEXT AS MONTH,
    EXTRACT(YEAR FROM t2.Cleaningdate)::TEXT AS YEAR,
    MIN(t2.Cleaningdate) AS mindate,
    UPPER(t2.city::TEXT) AS city,
    'Active_cleaners'::TEXT AS kpi,
    '-'::TEXT AS breakdown,
    COUNT(DISTINCT(t2.professional)) AS value
FROM
    (
    SELECT
        LEFT(a.polygon, 2) AS city,
        o.sfid AS sfid,
        a.professional__c AS professional,
        o.hr_contract_start__c::DATE AS StartDate,
        o.hr_contract_end__c::DATE AS EndDate,
        a.effectivedate AS CleaningDate,
        a.status AS Status,
        SUM(a.order_duration__c) AS Hours
    FROM
        bi.orders a
    INNER JOIN salesforce.account o ON
        (a.professional__c = o.sfid)
    WHERE
        o.test__c = '0'
        AND a.test__c = '0'
        AND effectivedate >= '2016-01-01'
        AND a.type IN ('cleaning-b2c',
        'cleaning-b2b',
        'cleaning-b2b;cleaning-b2c',
        'cleaning-b2c;cleaning-b2b')
        AND a.status IN ('INVOICED',
        'FULFILLED',
        'NOSHOW CUSTOMER')
        AND (o.type__c LIKE '%cleaning-b2c%'
        OR o.type__c LIKE '%cleaning-b2b%')
        AND LEFT(a.locale__c, 2) IN ('de',
        'nl',
        'ch',
        'at')
        AND a.polygon IS NOT NULL
    GROUP BY
        o.sfid,
        professional,
        a.polygon,
        StartDate,
        Cleaningdate,
        Status,
        EndDate) AS t2
GROUP BY
    MONTH,
    YEAR,
    city
ORDER BY
    mindate DESC,
    city ASC;

-- CAC
-- Desc: Total cost per cleaner, i.e. onboarding cost of acquired cleaners

SELECT
    EXTRACT(MONTH
FROM
    t1.mindate)::TEXT AS MONTH,
    EXTRACT(YEAR
FROM
    t1.mindate)::TEXT AS YEAR,
    t1.mindate AS mindate,
    UPPER(t1.polygon1) AS city,
    t1.kpi AS kpi,
    '-'::TEXT AS breakdown,
    ROUND((CASE WHEN t1.numero > 0 THEN t1.sum / t1.numero ELSE t1.sum END)::NUMERIC, 1) AS value
FROM
    (
    SELECT
        LEFT(c.polygon, 2) AS polygon1,
        CAST('CAC' AS TEXT) AS kpi,
        EXTRACT(MONTH
    FROM
        c.date)::TEXT AS MONTH,
        EXTRACT(YEAR
    FROM
        c.date)::TEXT AS YEAR,
        MIN(c.date) AS mindate,
        SUM(c.onboardings) AS numero,
        SUM(c.classifieds_costs + c.sem_costs + c.fb_costs) AS SUM
    FROM
        bi.cleaners_costsperonboarding_polygon c
    WHERE
        c.polygon NOT IN ('de-hannover',
        'de-monchengladbach',
        'de-wuppertal',
        'de-potsdam',
        'de-bochum',
        'de-dortmund',
        'de-duisburg',
        'de-leipzig',
        'de-manheim',
        'de-bonn;de-cologne',
        'de-dusseldorf;de-cologne')
    GROUP BY
        polygon1,
        YEAR,
        MONTH
    ORDER BY
        YEAR DESC,
        MONTH DESC,
        mindate DESC) t1;

-- Weekdays Revenue
-- Revenue of operating cities by weekdays

SELECT
    t8.year,
    CONCAT('WK', t8.week::TEXT) AS WEEK,
    t8.day_of_week,
    t8.polygon,
    CASE
        WHEN t8.order_type = 2 THEN 'cleaning-b2b'
        ELSE 'cleaning-b2c'
    END AS TYPE,
    ROUND(AVG(revenue)) AS revenue
FROM
    (
    SELECT
        EXTRACT(YEAR
    FROM
        o.effectivedate) AS YEAR,
        EXTRACT(WEEK
    FROM
        o.effectivedate) AS WEEK,
        TO_CHAR(o.effectivedate, 'day') AS day_of_week,
        o.polygon,
        o.order_type,
        SUM(o.gmv_eur_net) AS revenue
    FROM
        bi.orders o
    WHERE
        o.status IN ('INVOICED',
        'NOSHOW CUSTOMER',
        'PENDING TO START',
        'FULFILLED')
        -- AND o.effectivedate > (current_date - 60)
        -- AND o.effectivedate < current_date
        AND o.polygon IS NOT NULL
        AND o.effectivedate NOT IN ('2017-04-14',
        '2017-04-16',
        '2017-05-01',
        '2017-06-05',
        '2017-10-03',
        '2017-10-03',
        '2017-12-25',
        '2017-12-26')
        AND o.effectivedate >= '2018-01-01'
    GROUP BY
        WEEK,
        day_of_week,
        o.effectivedate,
        o.polygon,
        o.order_type
    ORDER BY
        WEEK,
        day_of_week,
        o.polygon
        -- o.type
) AS t8
WHERE
    t8.order_type IS NOT NULL
GROUP BY
    t8.year,
    t8.week,
    t8.day_of_week,
    t8.polygon,
    t8.order_type
ORDER BY
    YEAR,
    WEEK,
    t8.polygon;

-- PPH Overall
-- Desc: Price per hour realised

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('-' as text) as breakdown,
  CASE WHEN (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) > 0 THEN
    SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV_eur
  WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date-2 THEN GMV_eur*1.19 ELSE 0 END)/  (SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END)) ELSE 0 END as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and left(locale__c,2) != 'ch'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

-- PPH Recurrent
-- Desc: Price per hour realised for recurrent acquisition channel

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('Recurrent' as text) as breakdown,
  SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and acquisition_channel__c = 'recurrent'
  and t2.order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

-- PPH Website
-- Desc: Price per hour realised for website acquisition channel

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  replace(city, '+', '') as citya,
  'PPH' as kpi,
  CAST('One-Off' as text) as breakdown,
  SUM(GMV_eur)/SUM(Order_Duration__c) as value
FROM 
  bi.orders t2
WHERE 
  test__c = '0'
  and status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','CANCELLED SHORTTERM PROFESSIONAL')
  and acquisition_channel__c = 'web'
  and t2.order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

-- PPH B2C
-- Desc: Price per hour realised for B2C orders

SELECT
  EXTRACT(MONTH FROM effectivedate::date) as Month,
  EXTRACT(YEAR FROM effectivedate::date) as Year,
  min(effectivedate::date) as date,
  UPPER(LEFT(replace(city, '+', ''),2)) as citya,
  'PPH' as kpi,
  CAST('B2C' as text) as breakdown,
  CASE WHEN SUM(Order_Duration__c) > '0' THEN SUM(GMV_eur)/SUM(Order_Duration__c) ELSE 0 END as PPH
FROM
  bi.orders t2
WHERE
  test__c = '0'
  and order_type = '1'
  AND t2.effectivedate >= '2018-01-01'
GROUP BY
  Month,
  Year,
  Citya;

-- PPH B2B
-- Desc: Price per hour realised for B2B orders

SELECT

  EXTRACT(MONTH FROM date::date) as Month,
  EXTRACT(YEAR FROM date::date) as Year,
  MIN(date::date) as date,
  UPPER(LEFT(polygon,2)) as citya,
  'PPH' as kpi,
  CAST('B2B' as text) as breakdown,
  SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END)/SUM(t2.total_invoiced_hours) as PPH
  
FROM

  bi.b2borders t2
  
GROUP BY

  Month,
  Year,
  Citya;


-- M1 Return Rate
-- Return rate in 1 month

SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  LEFT(city,2) as locale,
  CAST('M1 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  locale;

-- M3 Return Rate
-- Desc: Return rate in 3 months

SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  LEFT(city,2) as locale,
  CAST('M3 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  locale;
    
-- M6 Return Rate
-- Desc: Return rate in 6 months
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  LEFT(city,2) as locale,
  CAST('M6 RR' as text) as kpi,
  CAST('-' as text) as breakdown,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_overall 
WhERE
  returning_month = '6'
GROUP BY
  month,
  year,
  locale;

-- Utilization %
-- Desc: Amount of an professonal's available time that's used for productive and billable work

SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  'DE' as citya,
  'Utilization GPM' as kpi,
  '-' as channel,
  CASE WHEN SUM(working_hours) > 0 THEN SUM(capped_work_hours)/SUM(working_hours) ELSE 0 END as Utilization
FROM
  bi.margin_per_cleaner
WHERE
  delivery_area like 'de%'
GROUP BY
  Month,
  year;
