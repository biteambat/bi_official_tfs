
DROP TABLE IF EXISTS bi.b2c_cost_per_likelies;
CREATE TABLE bi.b2c_cost_per_likelies AS

	SELECT 
		t1.likeli_creation as date,
		t1.locale,
		t1.source_channel,
		t1.nb_likelies,
		t2.cost

	FROM bi.temp_b2c_likelies t1
		JOIN bi.coststransform t2
			ON t1.likeli_creation = t2.dates
			AND t1.locale = t2.locale
			AND t1.source_channel = t2.channel
			AND t1.source_channel IN ('Brand Marketing','SEM','SEO','Display','Facebook')


;
