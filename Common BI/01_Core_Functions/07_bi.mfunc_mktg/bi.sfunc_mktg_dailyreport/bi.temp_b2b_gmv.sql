
--Net executed GMV

DROP TABLE IF EXISTS bi.temp_b2b_gmv;
CREATE TABLE bi.temp_b2b_gmv AS

		(
		SELECT

			'All' as locale,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END) as gmv_lw,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) < (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END)::numeric/4 as gmv_l4w

		FROM bi.orders

		WHERE test__c = '0'
			AND status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL', 'PENDING TO START', 'FULFILLED')
			AND order_type = '2'
			AND EXTRACT(WEEK from effectivedate) >= (EXTRACT(WEEK from current_date) - 5)
			AND EXTRACT(WEEK from effectivedate) < EXTRACT(WEEK from current_date)
			AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)
		)

	UNION

		(
		SELECT

			LEFT(locale__c, 2) as locale,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END) as gmv_lw,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) < (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END)::numeric/4 as gmv_l4w

		FROM bi.orders

		WHERE test__c = '0'
			AND status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL', 'PENDING TO START', 'FULFILLED')
			AND order_type = '2'
			AND EXTRACT(WEEK from effectivedate) >= (EXTRACT(WEEK from current_date) - 5)
			AND EXTRACT(WEEK from effectivedate) < EXTRACT(WEEK from current_date)
			AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)

		GROUP BY 
			LEFT(locale__c, 2)

		ORDER BY locale asc)

;