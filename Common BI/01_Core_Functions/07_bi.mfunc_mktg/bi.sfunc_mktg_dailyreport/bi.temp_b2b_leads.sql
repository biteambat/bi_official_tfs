
-- # Inbound leads

DROP TABLE IF EXISTS bi.temp_b2b_leads;
CREATE TABLE bi.temp_b2b_leads AS

	(
	SELECT
		LEFT(locale__c, 2) as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as leads_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as leads_l4w

	FROM salesforce.likeli__c

	WHERE type__c = 'B2B'
		AND EXTRACT(WEEK from createddate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from createddate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from createddate) = EXTRACT(YEAR from current_date)
		AND locale__c IS NOT NULL
		AND locale__c NOT LIKE ('at-%')
		AND acquisition_channel__c IN ('web', 'inbound')

	GROUP BY LEFT(locale__c, 2)

	ORDER BY locale asc
	)

	UNION

	(
	SELECT
		'All' as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as leads_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as leads_l4w

	FROM salesforce.likeli__c

	WHERE type__c = 'B2B'
		AND EXTRACT(WEEK from createddate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from createddate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from createddate) = EXTRACT(YEAR from current_date)
		AND locale__c IS NOT NULL
		AND locale__c NOT LIKE ('at-%')
		AND acquisition_channel__c IN ('web', 'inbound')
	)


--Inbound Leads Conversion Rate

	(SELECT
		locale,

		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_lw,

		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_l4w

	FROM bi.b2b_likelie_cvr_weekly

	WHERE EXTRACT(WEEK from date) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from date) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from date) = EXTRACT(YEAR from current_date)

	GROUP BY locale

	ORDER BY locale)

	UNION

	(SELECT
		'All' as locale,

		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_lw,

		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_l4w

	FROM bi.b2b_likelie_cvr_weekly

	WHERE EXTRACT(WEEK from date) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from date) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from date) = EXTRACT(YEAR from current_date)
	)