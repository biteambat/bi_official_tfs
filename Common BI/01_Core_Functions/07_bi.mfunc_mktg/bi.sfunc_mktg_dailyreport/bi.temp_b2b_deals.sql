
-- New deals signed

DROP TABLE IF EXISTS bi.temp_b2b_deals;
CREATE TABLE bi.temp_b2b_deals AS

	(
	SELECT
		LEFT(locale__c, 2) as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as deals_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as deals_l4w


	FROM salesforce.opportunity

	WHERE stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
		AND test__c = '0'
		AND EXTRACT(WEEK from closedate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from closedate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)

	GROUP BY
		LEFT(locale__c, 2)
	ORDER BY
		locale asc
	)

	UNION

	(
	SELECT
		'All' as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as deals_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as deals_l4w

	FROM salesforce.opportunity

	WHERE stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
		AND test__c = '0'
		AND EXTRACT(WEEK from closedate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from closedate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)
	)