
DROP TABLE IF EXISTS bi.acquisitions_last100D_channel;

	CREATE TABLE bi.acquisitions_last100D_channel AS

		SELECT 

		o.order_creation__c::timestamp::date as orderdate,
		left(o.locale__c,2) as country,
		o.marketing_channel as marketing_channel,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' and order_creation__c::date = customer_creation_date::date THEN 1 ELSE NULL END) as acquisitions,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status LIKE ('%CANCELLED%') and order_creation__c::date = customer_creation_date::date THEN 1 ELSE NULL END) as cancelled_acquisitions

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND o.order_creation__c >= Current_date - interval '100 days'
			AND order_creation__c::timestamp::date < current_date 
			and order_type = '1'

		GROUP BY orderdate, country, marketing_channel

		ORDER BY orderdate desc, country asc, marketing_channel asc

	;