
DROP TABLE IF EXISTS bi.temp_coops_runningcohortanalysis;
	CREATE TABLE bi.temp_coops_runningcohortanalysis AS

	SELECT

			t1.contact__c,
			t1.voucher as acquisition_voucher,
			t1.locale,
			t1.acquisition_date,
			CASE WHEN (SUM(CASE WHEN (t2.order_creation__c::date between (t1.acquisition_date) and (t1.acquisition_date + interval '30 days')) THEN 1 ELSE 0 END)) > 0 THEN 1 ELSE 0 END AS rebooked_m1,
			CASE WHEN (SUM(CASE WHEN (t2.order_creation__c::date between (t1.acquisition_date + interval '30 days') and (t1.acquisition_date + interval '60 days')) THEN 1 ELSE 0 END)) > 0 THEN 1 ELSE 0 END AS rebooked_m2,
			CASE WHEN (SUM(CASE WHEN (t2.order_creation__c::date between (t1.acquisition_date + interval '60 days') and (t1.acquisition_date + interval '90 days')) THEN 1 ELSE 0 END)) > 0 THEN 1 ELSE 0 END AS rebooked_m3

		FROM bi.temp_coops_listcustomers t1

		LEFT JOIN bi.orders t2 ON (
			t2.contact__c = t1.contact__c 
			AND t2.test__c = '0'
			AND t2.acquisition_new_customer__c = '0'
			AND t2.status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
			AND t2.order_type = '1'
			)

		WHERE t1.acquisition_date <= (current_date - interval '31 days')

		GROUP BY t1.contact__c, t1.voucher, t1.locale, t1.acquisition_date
		ORDER BY t1.acquisition_date desc, t1.locale asc, t1.contact__c asc 

	;