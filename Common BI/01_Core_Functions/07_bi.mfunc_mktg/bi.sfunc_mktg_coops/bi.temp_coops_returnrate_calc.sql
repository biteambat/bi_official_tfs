
DROP TABLE IF EXISTS bi.temp_coops_returnrate_calc;
	CREATE TABLE bi.temp_coops_returnrate_calc AS

		SELECT
			t1.acquisition_voucher,
			t1.locale,
			COUNT(DISTINCT t1.contact__c) as nb_acquisitions,
			SUM(t1.rebooked_m1) as nb_m1_rebookings,
			SUM(t1.rebooked_m2) as nb_m2_rebookings,
			SUM(t1.rebooked_m3) as nb_m3_rebookings

		FROM bi.temp_coops_runningcohortanalysis t1

		GROUP BY t1.acquisition_voucher, t1.locale

		ORDER BY t1.acquisition_voucher asc, t1.locale asc

	;