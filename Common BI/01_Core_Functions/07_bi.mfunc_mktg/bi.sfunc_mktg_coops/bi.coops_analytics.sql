
	DROP TABLE IF EXISTS bi.coops_analytics;
	CREATE TABLE bi.coops_analytics AS 

		SELECT

			MAX(CASE
				WHEN voucher__c like 'RUEGEN3HP___' THEN 'RUEGEN3HPXXX'
				WHEN voucher__c like 'TUTTI___' THEN 'TUTTIXXX'
				WHEN voucher__c like 'TIEFKUEHL___' THEN 'TIEFKUEHLXXX'
				WHEN voucher__c like 'STU71___' THEN 'STU71XXX'
				WHEN voucher__c like 'SECCO___' THEN 'SECCOXXX'
				WHEN voucher__c like 'PROPER3___' THEN 'PROPER3XXX'
				WHEN voucher__c like 'PROPER10___' THEN 'PROPER10XXX'
				WHEN voucher__c like 'LOVE___' THEN 'LOVEXXX'
				WHEN voucher__c like 'GOURMESSO___' THEN 'GOURMESSOXXX'
				WHEN voucher__c like 'DURGOL___' THEN 'DURGOLXXX'
				WHEN voucher__c like 'DEINDEAL3___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEINDEAL4___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEINDEAL5___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEAL3___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEAL4___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEAL5___' THEN 'DEINDEAL'
				WHEN voucher__c like 'OPTIO10___' THEN 'OPTIO10XXX'
				WHEN voucher__c like 'OPTIO20___' THEN 'OPTIO20XXX'
				WHEN voucher__c like 'OPTIO40___' THEN 'OPTIO40XXX'
				WHEN voucher__c like 'OPTIO64___' THEN 'OPTIO64XXX'
				WHEN voucher__c like 'VDCO3___' THEN 'VDCO3XXX'
				WHEN voucher__c like 'VDCO4___' THEN 'VDCO4XXX'
				WHEN voucher__c like 'VDCO5___' THEN 'VDCO5XXX'
				WHEN (voucher__c like 'PUTZCHECKER___' OR voucher__c like 'PUTZCHECKER__') THEN 'PUTZCHECKERXXX'
			ELSE o.voucher__c END) as voucher_code,

			left(o.locale__c,2) as locale,
			SUM(CASE WHEN o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE','CANCELLED NO MANPOWER') THEN 1 ELSE 0 END) as total_redemptions,
			SUM(CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as invoiced_redemptions,
			SUM(CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL') THEN o.gmv_eur ELSE 0 END) as invoiced_gmv,
			SUM(t1.nb_acquisitions) as nb_acquisitions,
			SUM(t1.nb_m1_rebookings) as nb_m1_rebookings,
			SUM(t1.nb_m2_rebookings) as nb_m2_rebookings,
			SUM(t1.nb_m3_rebookings) as nb_m3_rebookings,
			MAX(CASE WHEN o.status in ('INVOICED') THEN o.order_creation__c::date ELSE NULL END)::date as last_acquisition_date

		FROM bi.orders o

		LEFT JOIN bi.temp_coops_returnrate_calc t1 ON (t1.acquisition_voucher = o.voucher__c and t1.locale = left(o.locale__c,2))

		WHERE o.test__c = '0'
			AND o.order_type = '1'
			AND (o.voucher__c in ('MOVE24',
			'RUEGEN3HPXXX',
			'XMASBUTLER',
			'XMASBUTLER20',
			'XKM3TLMJY3',
			'WUNDER20',
			'WUNDER20',
			'TUTTI35',
			'TUTTI35',
			'TUTTI50',
			'TUTTIXXX',
			'TOTFCV9KRC',
			'TIEFKUEHLXXX',
			'STUDIO20',
			'STU71xxx',
			'STARTUP20',
			'STAR50',
			'SECCOXXX',
			'SAIL20',
			'S4EYHD2WKR',
			'S4EYHD2WKR',
			'RUG30',
			'RUG30',
			'RIC35',
			'PUTZCHECKER14',
			'PROPER20',
			'PROPER3XXX',
			'PROPER10XXX',
			'PAY25',
			'PAY50',
			'OFFICE20',
			'MOVE20',
			'MOVAGO20',
			'MCTIGER20',
			'LOVE30',
			'LOVEXXXX',
			'LOVEXXXX',
			'HOMIFY20',
			'GOURMESSOXXXX',
			'GOLF20',
			'FLAC1',
			'FLAC2',
			'FLACONI25',
			'FLACONI25',
			'FLATSS20',
			'FLIGHT20',
			'DURGOLXXX',
			'DURGOLXXX',
			'DSPUTZ15',
			'DELIVEROO20',
			'BXXBAT10',
			'BXXBAT1H',
			'BXXBAT20',
			'BON1H',
			'BENIFY20',
			'BEPONY22',
			'AMAZON20',
			'AMTIGER1H',
			'AMTIGER20',
			'AMZ1',
			'AMZ2',
			'ANNA35',
			'ANNA50',
			'AXXBAT10',
			'AXXBAT1H',
			'AXXBAT20',
			'2NH33BHZRD',
			'78EI4OO6Z1', 
			'78EI4OO6Z1',
			'OPTIO10XXX',
			'OPTIO20XXX',
			'OPTIO40XXX',
			'OPTIO64XXX',
			'MYTIGER20',
			'PUTZCHECKERXXX',
			'INCENT1P',
			'BUY20',
			'HOME50',
			'LADIES50',
			'MEN40',
			'TUT50',
			'PUTZ50',
			'TRYNGO50',
			'CPLATTE40',
			'AFSTA50',
			'CPPLACE50',
			'CPQUALI50',
			'WINGS20',
			'BASEL35',
			'BERN35',
			'STGALLEN35',
			'LUZERN35',
			'ZURICH35',
			'ZURICHSAUBER',
			'LAUSANNE35',
			'GENEVA35',
			'ZUG35',
			'WINTI35',
			'TAMEDIA40',
			'TIGERCHS35',
			'TUTTI35',
			'ANNA50',
			'STAR50',
			'TRAM35',
			'BATFR75',
			'TUT50',
			'KUS25',
			'FUW50',
			'ALBA35',
			'RIC50',
			'UBS40',
			'FEM50',
			'PUTZ50',
			'PUTZ35',
			'UBS40',
			'TAG50',
			'TRIBUNE50',
			'20MIN50',
			'20MIN70',
			'BLATT50',
			'FRIDAY50',
			'BILAN50',
			'RAB50',
			'SONNTAG50',
			'FUW50',
			'LEMATIN50',
			'TRYNGO50',
			'SSQUAD40',
			'MEN40',
			'HOME50',
			'LADIES50',
			'CPLATTE40',
			'PUTZ40',
			'BMINTER50',
			'50XMAS',
			'50SONNTAG',
			'VDXMGW1',
			'STAR50',
			'GRATIS50',
			'ENCORE50',
			'SAUBER30',
			'CPCAKE50',
			'CPQUALI50',
			'CPLATTEVXN',
			'CPLATTEVFQ',
			'CPLATTEKKL',
			'CPLATTEGNF',
			'CPLATTEDZT',
			'CPLATTECMV',
			'CPLATTEKLQ',
			'CPLATTEJHP',
			'CPLATTEBCS',
			'CPLATTENSH',
			'AFSTA50',
			'BMPST25',
			'50FEM',
			'BM50MARIN',
			'BM75TIGER',
			'BM75BAT',
			'CPALLIANZ50',
			'BMDRVE40',
			'BMDRVA40',
			'BMDRVG40',
			'BMDRV40',
			'BM1DRV40',
			'50GEWINN',
			'50PUTZ20',
			'50LADIES',
			'50ANNA',
			'BM40PUTZ7',
			'BM40PUTZH',
			'BM40PUTZU',
			'BM40PUTZ6',
			'BM40PUTZL',
			'AFMOM50',
			'AF60SWISS',
			'BM50SWISS',
			'RADIOUIDA',
			'RADIOLCIM',
			'RADIOFPDV',
			'RADIOSIMA',
			'BM35ZURICH',
			'BM35BASEL',
			'BM35BERN',
			'BM40PUTZ5',
			'BM40PUTZ4',
			'50REGIONAL',
			'BM50PUTZ',
			'CP50WASH',
			'TIGERKITCH10',
			'50SONNTAG',
			'DELDRIVHC',
			'DELDRIVAY',
			'DELDRIVIH',
			'DELDRIVFT',
			'DELDRIVLW')
				OR voucher__c like 'RUEGEN3HP___'
				OR voucher__c like 'TUTTI___'
				OR voucher__c like 'TIEFKUEHL___'
				OR voucher__c like 'STU71___'
				OR voucher__c like 'SECCO___'
				OR voucher__c like 'PROPER3___'
				OR voucher__c like 'PROPER10___'
				OR voucher__c like 'LOVE___'
				OR voucher__c like 'GOURMESSO___'
				OR voucher__c like 'DURGOL___'
				OR voucher__c like 'DEINDEAL3___'
				OR voucher__c like 'DEINDEAL4___'
				OR voucher__c like 'DEINDEAL5___'
				OR voucher__c like 'DEAL3___'
				OR voucher__c like 'DEAL4___'
				OR voucher__c like 'DEAL5___'
				OR voucher__c like 'OPTIO10___'
				OR voucher__c like 'OPTIO20___'
				OR voucher__c like 'OPTIO40___'
				OR voucher__c like 'OPTIO64___'
				OR voucher__c like 'PUTZCHECKER___'
				OR voucher__c like 'VDCO3___'
				OR voucher__c like 'VDCO4___'
				OR voucher__c like 'VDCO5___'
			)

		GROUP BY (CASE
				WHEN voucher__c like 'RUEGEN3HP___' THEN 'RUEGEN3HPXXX'
				WHEN voucher__c like 'TUTTI___' THEN 'TUTTIXXX'
				WHEN voucher__c like 'TIEFKUEHL___' THEN 'TIEFKUEHLXXX'
				WHEN voucher__c like 'STU71___' THEN 'STU71XXX'
				WHEN voucher__c like 'SECCO___' THEN 'SECCOXXX'
				WHEN voucher__c like 'PROPER3___' THEN 'PROPER3XXX'
				WHEN voucher__c like 'PROPER10___' THEN 'PROPER10XXX'
				WHEN voucher__c like 'LOVE___' THEN 'LOVEXXX'
				WHEN voucher__c like 'GOURMESSO___' THEN 'GOURMESSOXXX'
				WHEN voucher__c like 'DURGOL___' THEN 'DURGOLXXX'
				WHEN voucher__c like 'DEINDEAL3___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEINDEAL4___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEINDEAL5___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEAL3___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEAL4___' THEN 'DEINDEAL'
				WHEN voucher__c like 'DEAL5___' THEN 'DEINDEAL'
				WHEN voucher__c like 'OPTIO10___' THEN 'OPTIO10XXX'
				WHEN voucher__c like 'OPTIO20___' THEN 'OPTIO20XXX'
				WHEN voucher__c like 'OPTIO40___' THEN 'OPTIO40XXX'
				WHEN voucher__c like 'OPTIO64___' THEN 'OPTIO64XXX'
				WHEN voucher__c like 'VDCO3___' THEN 'VDCO3XXX'
				WHEN voucher__c like 'VDCO4___' THEN 'VDCO4XXX'
				WHEN voucher__c like 'VDCO5___' THEN 'VDCO5XXX'
				WHEN (voucher__c like 'PUTZCHECKER___' OR voucher__c like 'PUTZCHECKER__') THEN 'PUTZCHECKERXXX'
			ELSE o.voucher__c END),
			left(o.locale__c,2)

		ORDER BY voucher_code asc, locale asc

	;