
DROP TABLE IF EXISTS bi.runrate_dow;
CREATE TABLE bi.runrate_dow AS

SELECT 
		t1.locale,
		'Last 4 weeks' as kpi,
		t1.polygon,
		t1.dow,
		AVG(t1.gmv_net) as dow_rr

FROM

	(SELECT
		LEFT(locale__c, 2) as locale,
		polygon as polygon,
		effectivedate::date as orderdate,
		EXTRACT(DOW FROM effectivedate) as dow,
		SUM(CASE WHEN gmv_eur_net IS NULL THEN 0 ELSE gmv_eur_net END) as gmv_net

	FROM bi.orders

	WHERE test__c = '0'
		AND order_type = '1'
		AND status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')

		AND ((    EXTRACT(MONTH FROM current_date) = 1 
		     AND EXTRACT(WEEK FROM current_date) <= 1
			  AND (   (EXTRACT(YEAR FROM effectivedate) = EXTRACT(YEAR FROM current_date) AND EXTRACT(WEEK FROM effectivedate) <= 2)
			       OR (EXTRACT(YEAR FROM effectivedate) = (EXTRACT(YEAR FROM current_date) - 1) AND EXTRACT(WEEK FROM effectivedate) >= 50)))
			       
			OR	(    EXTRACT(MONTH FROM current_date) = 1 
		     AND EXTRACT(WEEK FROM current_date) <= 2
			  AND (   (EXTRACT(YEAR FROM effectivedate) = EXTRACT(YEAR FROM current_date) AND EXTRACT(WEEK FROM effectivedate) <= 2)
			       OR (EXTRACT(YEAR FROM effectivedate) = (EXTRACT(YEAR FROM current_date) - 1) AND EXTRACT(WEEK FROM effectivedate) >= 51)))
					 
			OR	(    EXTRACT(MONTH FROM current_date) = 1 
		     AND EXTRACT(WEEK FROM current_date) <= 3
			  AND (   (EXTRACT(YEAR FROM effectivedate) = EXTRACT(YEAR FROM current_date) AND EXTRACT(WEEK FROM effectivedate) <= 3)
			       OR (EXTRACT(YEAR FROM effectivedate) = (EXTRACT(YEAR FROM current_date) - 1) AND EXTRACT(WEEK FROM effectivedate) >= 52)))	
			       
			OR (    EXTRACT(MONTH FROM current_date) = 1
			  AND EXTRACT(WEEK FROM current_date) <= 4	
			  AND (   (EXTRACT(YEAR FROM effectivedate) = EXTRACT(YEAR FROM current_date) AND EXTRACT(WEEK FROM effectivedate) <= 4)
			       OR (EXTRACT(YEAR FROM effectivedate) = (EXTRACT(YEAR FROM current_date) - 1) AND EXTRACT(WEEK FROM effectivedate) >= 53)))
			
			OR (    EXTRACT(MONTH FROM current_date) > 1
			  AND EXTRACT(WEEK FROM effectivedate) >= (EXTRACT(WEEK FROM current_date) - 4)
			  AND EXTRACT(YEAR FROM effectivedate) = EXTRACT(YEAR FROM current_date)))	
			  
		AND effectivedate < (current_date - interval '2 days')

	GROUP BY
		LEFT(locale__c, 2),
		polygon,
		effectivedate::date,
		EXTRACT(DOW FROM effectivedate)

	ORDER BY orderdate asc, locale asc, polygon asc
	) t1

GROUP BY
	t1.locale,
	t1.polygon,
	t1.dow
ORDER BY
	t1.dow asc,
	t1.locale asc,
	t1.polygon asc;