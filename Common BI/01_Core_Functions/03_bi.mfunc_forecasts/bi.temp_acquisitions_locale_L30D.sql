
DROP TABLE IF EXISTS bi.temp_acquisitions_locale_L30D;
	CREATE TABLE bi.temp_acquisitions_locale_L30D AS

		SELECT
		left(o.locale__c,2) as locale,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.order_creation__c >= (current_date - interval '30 days') THEN 1 ELSE NULL END) as locale_acquisitions_L30D,
		SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.order_creation__c >= (current_date - interval '90 days') AND o.order_creation__c < (current_date - interval '60 days') THEN 1 ELSE NULL END) as locale_acquisitions_L60to90D

		FROM bi.orders o

		WHERE o.test__c = '0'
			AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND order_type = '1'
			AND o.marketing_channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND o.order_creation__c >= (current_date - interval '90 days')

		GROUP BY left(o.locale__c,2)

		ORDER BY left(o.locale__c,2)

	;