
DROP TABLE IF EXISTS bi.forecast_growth;
CREATE TABLE bi.forecast_growth AS

SELECT 

    f.*, 
    hour_prediction_base + hour_prediction_base*growth_agg as predicted_hour

FROM 

    (SELECT 
    
        concat('CW',EXTRACT(WEEK from first_day_of_cw)) as calendarweek, 
        z.*, 
        CASE WHEN ranking >= 9 THEN sum(growth_rate) OVER (ORDER BY delivery_area, first_day_of_cw ROWS BETWEEN 8 PRECEDING AND 4 PRECEDING) ELSE NULL END as growth_agg
        
    FROM 
    
        (SELECT 
            
            y.*, 
            CASE WHEN round(cast((growth/pre_hours) as numeric),2) > 0.05 or round(cast((growth/pre_hours) as numeric),2) < -0.05 THEN NULL ELSE round(cast((growth/pre_hours) as numeric),2) END as growth_rate
            
         FROM 
         
            (SELECT 
             
                x.*, 
                rank() OVER (PARTITION BY delivery_area ORDER BY first_day_of_cw asc) as ranking, 
                lag(hours) OVER (PARTITION BY delivery_area ORDER BY first_day_of_cw asc) as pre_hours, 
                lag(hours,5) OVER (ORDER BY delivery_area, first_day_of_cw) as hour_prediction_base, 
                hours - lag(hours) OVER (ORDER BY delivery_area, first_day_of_cw) as growth, 
                AVG(hours) OVER(ORDER BY delivery_area rows 7 preceding) AS mean7d
                
            FROM 
            
                (SELECT 
                
                    a.first_day_of_cw,
                    a.delivery_area, 
                    b.hours, 
                    b.hours_mean
                    
                FROM 
                
                    (SELECT 
                    
                      CAST(date as date) as first_day_of_cw,
                      UNNEST(array (
                                            SELECT 
                                            
                                                DISTINCT delivery_area
                                            
                                            FROM bi.orders)) as delivery_area
                                            
                    FROM 
                    
                        generate_series('2016-01-04'::date, now()::date+28, '1 week'::interval) date) a
                                  
                LEFT JOIN 
                
                    (SELECT 
                                
                        cast(date_trunc('week', order_date) as date) as first_day_of_cw,
                        delivery_area, 
                        SUM(test) as hours_mean, 
                        SUM(hours) as hours
                        
                    FROM 
                    
                        (SELECT 
                           
                            a.*, 
                            rank() OVER (PARTITION BY delivery_area ORDER BY order_date asc) as ranking, 
                            avg(hours) OVER (PARTITION BY dow, delivery_area ORDER BY order_date asc ROWS BETWEEN 2 PRECEDING AND 0 FOLLOWING) as test
                        
                        FROM 
                        
                            (SELECT
                            
                              delivery_area, 
                              date_part('dow',  cast(order_start__c as date)) as dow, 
                              CAST(order_start__c as date) as order_date, 
                              SUM(order_duration__c) hours
                                    
                            FROM 
                            
                                bi.orders
                            
                            WHERE 
                            
                                order_start__c BETWEEN '2016-01-04' AND CAST(date_trunc('week', now()) as date)+7
                                AND "status" = 'INVOICED'   
                                GROUP BY delivery_area, order_date) a
                            ) x
                            
                        GROUP BY first_day_of_cw, delivery_area
                        
                    ) b
                    
        ON a.first_day_of_cw = b.first_day_of_cw AND a.delivery_area = b.delivery_area) x) y) z) f;