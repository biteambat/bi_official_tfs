
DROP TABLE IF EXISTS bi.invoiced_forecast_b2c_b2b;
    CREATE TABLE bi.invoiced_forecast_b2c_b2b AS

        SELECT
            f.locale,
            CASE WHEN f.city_polygon is null then 'other' else f.city_polygon END as city_polygon,
            SUM(f.forecast) as invoiced_gmv_forecast,
            (CASE WHEN (AVG(b.bookings_forecast + b.supply_forecast)) IS NOT NULL THEN (AVG(b.bookings_forecast + b.supply_forecast))
                ELSE 0 END)
            as b2b_forecast,
            SUM(f.forecast) + (CASE WHEN (AVG(b.bookings_forecast + b.supply_forecast)) IS NOT NULL THEN (AVG(b.bookings_forecast + b.supply_forecast)) ELSE 0 END) 
            as b2c_b2b_invoiced_forecast,
            CASE 
            WHEN f.city_polygon = 'at-vienna' THEN 0
            WHEN f.city_polygon = 'ch-zurich' THEN 0
            WHEN f.city_polygon = 'nl-amsterdam' THEN 0 
            WHEN f.city_polygon = 'de-berlin' then 202006
            ELSE 0 END as b2c_target,
            CASE 
            WHEN f.city_polygon = 'at-vienna' THEN 0
            WHEN f.city_polygon = 'ch-zurich' THEN 0
            WHEN f.city_polygon = 'nl-amsterdam' THEN 0
            WHEN f.city_polygon = 'de-berlin' THEN 290362
            ELSE 0 END as b2b_target

        FROM bi.invoiced_gmv_forecast_polygon f

            LEFT JOIN bi.b2b_forecast b ON f.locale = b.locale 
                AND f.city_polygon = b.polygon

        GROUP BY f.locale, city_polygon

    ;