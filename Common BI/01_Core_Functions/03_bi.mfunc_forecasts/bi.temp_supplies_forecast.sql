
DROP TABLE IF EXISTS bi.temp_supplies_forecast;
	CREATE TABLE bi.temp_supplies_forecast AS 

		SELECT
			t1.locale,
			t1.polygon,
			SUM(t1.supply_revenue) as supply_forecast

		FROM 
			(SELECT
			 left(t1.locale__c,2) as locale,
			 t1.polygon,
			 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
			 bi.orders t1
			LEFT JOIN
			 Salesforce.Opportunity t2
			ON
			 (t2.sfid = t1.opportunityid)
			WHERE
			 type = 'cleaning-b2b'
			 and status NOT LIKE ('%CANCELLED%')
			 and EXTRACT(MONTH from Effectivedate) = EXTRACT(MONTH from current_date)
			 and EXTRACT(YEAR from Effectivedate) = EXTRACT(YEAR from current_date)
			GROUP BY
			 t2.name,
			 contact__c,
			 contact_name__c,
			 left(t1.locale__c,2),
			 t1.polygon) t1

		GROUP BY t1.locale, t1.polygon

	;