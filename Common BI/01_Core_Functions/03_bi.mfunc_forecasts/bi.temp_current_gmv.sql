
DROP TABLE IF EXISTS bi.temp_current_gmv;
CREATE TABLE bi.temp_current_gmv AS

	SELECT
		t1.date,
		EXTRACT(DOW FROM t1.date) as dow,
		LEFT(t2.locale__c,2) as locale,
		t2.polygon as polygon,
		SUM(CASE WHEN (t2.gmv_eur_net IS NULL 
										OR t2.status NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER') 
										OR t2.effectivedate >= (current_date - interval '2 days')) 
				THEN 0 
				 ELSE t2.gmv_eur_net END) as current_gmv

	FROM bi.temp_timeseries t1

	LEFT JOIN bi.orders t2
		ON t1.date = t2.effectivedate

	WHERE t2.test__c = '0'
		AND t2.order_type = '1'

	GROUP BY t1.date, EXTRACT(DOW FROM t1.date), LEFT(t2.locale__c,2), t2.polygon

	ORDER BY t1.date asc, locale asc, polygon asc

;