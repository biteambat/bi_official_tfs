
DROP TABLE IF EXISTS bi.temp_b2b_bookings_forecast;

	CREATE TABLE bi.temp_b2b_bookings_forecast AS

		SELECT
			left(o.locale__c,2) as locale,

			o.polygon as polygon,

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)) as days_left_currMonth,

			SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur_net ELSE 0 END) as actual_sales_currMonth,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
			THEN o.gmv_eur_net ELSE 0 END)
			/
			14) as run_rate_L14D,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur_net ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
			THEN o.gmv_eur_net ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as bookings_forecast,

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND o.acquisition_new_customer__c = '1'
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur_net ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
						AND o.acquisition_new_customer__c = '1'
			THEN o.gmv_eur_net ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as acquisition_forecast,


			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date <= current_date::date
						AND o.acquisition_new_customer__c = '0'
						AND EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)
						AND EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)
			THEN o.gmv_eur_net ELSE 0 END)

				+

			(SUM(CASE WHEN o.status not like ('%CANCELLED%') AND o.status not like ('%ERROR%')
						AND o.effectivedate::date > current_date
						AND o.effectivedate::date <= (current_date + interval '14 days')
						AND o.acquisition_new_customer__c = '0'
			THEN o.gmv_eur_net ELSE 0 END)
			/
			14)
			
				*

			MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from current_date)))

			as rebookings_forecast

		FROM bi.orders o

		WHERE o.test__c = '0' 
			AND o.order_type = '2'
			AND o.status not like ('%CANCELLED%')

		GROUP BY left(o.locale__c,2), o.polygon

		ORDER BY locale asc, o.polygon asc

	;