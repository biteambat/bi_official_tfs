
DROP TABLE IF EXISTS bi.invoiced_gmv_forecast_polygon;
    CREATE TABLE bi.invoiced_gmv_forecast_polygon AS

        SELECT
            LEFT(o.locale__c,2) as locale,

            o.polygon as city_polygon,
        MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) )) as days_left_currMonth,

            SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END) as invoiced_gmv_currmonth,

            (SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND      (o.effectivedate::date <= current_date::date - 1) AND (o.effectivedate::date > current_date::date - 16)  THEN o.gmv_eur ELSE 0 END)/15)::numeric as rr_L15D,




            (SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END))
                + 

            ((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 16) THEN o.gmv_eur ELSE 0 END)/15)::numeric)
                *
                (MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) ))) 

            as forecast,




            SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END) as invoiced_acq_currmonth,

            (SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate < current_date - 1) AND (o.effectivedate >= current_date - 16)THEN 1 ELSE 0 END)/15)::numeric as rr_L15D_acq,

            (SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate < current_date - 1) THEN 1 ELSE 0 END)) 
                + ((SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate < current_date - 1) AND (o.effectivedate >= current_date - 16)THEN 1 ELSE 0 END)/15)::numeric) 
                * (MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-2) )))

            as forecast_invoiced_acquisitions,



            CASE WHEN (EXTRACT (DAY from current_date)) > '10' THEN

                                ((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (EXTRACT(month from o.effectivedate) = EXTRACT(month from current_date)) AND (EXTRACT(year from o.effectivedate) = EXTRACT(year from current_date)) AND (o.effectivedate <= current_date - 1) THEN o.gmv_eur ELSE 0 END))
                                    + 

                                ((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 16) THEN o.gmv_eur ELSE 0 END)/15)::numeric)
                                    *
                                    (MAX(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - extract(day from (current_date-1) ))) )

            ELSE


                                (((SUM( CASE WHEN o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING TO START','FULFILLED') AND (o.effectivedate <= current_date - 1) AND (o.effectivedate > current_date - 31) THEN o.gmv_eur ELSE 0 END)/30)::numeric)
                                    *
                                    (    DATE_PART('days', DATE_TRUNC('month', current_date::date) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)))

            END


            as adjusted_forecast


        FROM bi.orders o

        WHERE o.test__c = '0' and order_type = '1' and (LEFT(o.locale__c,2) = LEFT(o.polygon,2) OR o.polygon IS NULL)

        GROUP BY LEFT(o.locale__c,2), o.polygon

        ORDER BY locale asc, city_polygon asc
    ;