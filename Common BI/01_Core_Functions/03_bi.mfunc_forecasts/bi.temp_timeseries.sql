
DROP TABLE IF EXISTS bi.temp_timeseries;
CREATE TABLE bi.temp_timeseries AS

	SELECT 
		date::date

	FROM generate_series
	(
	  date_trunc('month', current_date)::date,
	  (date_trunc('month', current_date) + interval '1 MONTH - 1 day')::date,
	  '1 day'::interval
	)date
;