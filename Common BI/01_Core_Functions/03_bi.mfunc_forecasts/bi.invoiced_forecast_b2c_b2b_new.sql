
DROP TABLE IF EXISTS bi.invoiced_forecast_b2c_b2b_new;
CREATE TABLE bi.invoiced_forecast_b2c_b2b_new AS

SELECT

    t1.locale,
    t1.polygon,
    t1.kpi::text,

    SUM(t1.forecast) as b2c_forecast,
        CASE 
        WHEN t1.polygon = 'at-vienna' THEN 0
        WHEN t1.polygon = 'ch-zurich' THEN 0
        WHEN t1.polygon = 'nl-amsterdam' THEN 0 
        WHEN t1.polygon = 'de-berlin' then 202006   
        ELSE 0 END as b2c_target,
        CASE WHEN t2.b2b_forecast IS NULL THEN 0 ELSE t2.b2b_forecast END
    as b2b_forecast,
        CASE 
        WHEN t1.polygon = 'at-vienna' THEN 0
        WHEN t1.polygon = 'ch-zurich' THEN 0
        WHEN t1.polygon = 'nl-amsterdam' THEN 0
        WHEN t1.polygon = 'de-berlin' THEN 290362
        ELSE 0 END as b2b_target

FROM 

    bi.weekday_forecast t1

LEFT JOIN

    (SELECT 
    
        locale,
        polygon,
        SUM(bookings_forecast + CASE WHEN supply_forecast IS NULL THEN 0 ELSE supply_forecast END) as b2b_forecast
        
    FROM
        bi.b2b_forecast
        
    GROUP BY locale, polygon) as t2

ON 

    t1.locale = t2.locale
    AND t1.polygon = t2.polygon

WHERE
    
    left(t1.polygon,2) = t1.locale or t1.polygon = 'Other' or t1.polygon is null

GROUP BY 

    t1.locale, 
    t1.polygon, 
    t2.b2b_forecast,
    t1.kpi
 
ORDER BY 

    t1.locale asc, 
    t1.polygon asc

;