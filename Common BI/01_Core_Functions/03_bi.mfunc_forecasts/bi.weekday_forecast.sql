
INSERT INTO bi.runrate_dow

SELECT 
        t1.locale,
        'Expected Current Month' as kpi,
        t1.polygon,
        t1.dow,
        AVG(t1.gmv_net) as dow_rr

FROM

    (SELECT
        LEFT(locale__c, 2) as locale,
        polygon as polygon,
        effectivedate::date as orderdate,
        EXTRACT(DOW FROM effectivedate) as dow,
        SUM(CASE WHEN gmv_eur_net IS NULL THEN 0 ELSE gmv_eur_net END) as gmv_net

    FROM bi.orders

    WHERE 
    
        test__c = '0'
        AND order_type = '1'
        AND status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')

        AND EXTRACT(YEAR FROM effectivedate) = EXTRACT(YEAR FROM current_date)
        AND EXTRACT(MONTH FROM effectivedate) = EXTRACT(MONTH FROM current_date)    

    GROUP BY
        LEFT(locale__c, 2),
        polygon,
        effectivedate::date,
        EXTRACT(DOW FROM effectivedate)

    ORDER BY orderdate asc, locale asc, polygon asc
    ) t1

GROUP BY
    t1.locale,
    t1.polygon,
    t1.dow
ORDER BY
    t1.dow asc,
    t1.locale asc,
    t1.polygon asc;


DROP TABLE IF EXISTS bi.temp_timeseries;
CREATE TABLE bi.temp_timeseries AS

    SELECT 
        date::date

    FROM generate_series
    (
      date_trunc('month', current_date)::date,
      (date_trunc('month', current_date) + interval '1 MONTH - 1 day')::date,
      '1 day'::interval
    )date
;

DROP TABLE IF EXISTS bi.temp_current_gmv;
CREATE TABLE bi.temp_current_gmv AS

    SELECT
        t1.date,
        EXTRACT(DOW FROM t1.date) as dow,
        LEFT(t2.locale__c,2) as locale,
        t2.polygon as polygon,
        SUM(CASE WHEN (t2.gmv_eur_net IS NULL 
                                        OR t2.status NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER') 
                                        OR t2.effectivedate >= (current_date - interval '2 days')) 
                THEN 0 
                 ELSE t2.gmv_eur_net END) as current_gmv

    FROM bi.temp_timeseries t1

    LEFT JOIN bi.orders t2
        ON t1.date = t2.effectivedate

    WHERE t2.test__c = '0'
        AND t2.order_type = '1'

    GROUP BY t1.date, EXTRACT(DOW FROM t1.date), LEFT(t2.locale__c,2), t2.polygon

    ORDER BY t1.date asc, locale asc, polygon asc

;

DROP TABLE IF EXISTS bi.weekday_forecast;
CREATE TABLE bi.weekday_forecast AS

    SELECT
        t2.kpi::text,
        t1.date,
        t1.dow,
        t1.locale,
        CASE WHEN t1.polygon is null then 'Other' ELSE t1.polygon  END as polygon,
        t1.current_gmv,
        CASE WHEN t2.dow_rr IS NULL THEN 0 ELSE t2.dow_rr END as dow_rr,
        CASE 
        WHEN t1.date::date = '2017-06-06'::date THEN 0
        WHEN t1.current_gmv = 0 THEN 
            (CASE WHEN t2.dow_rr IS NULL THEN 0 ELSE t2.dow_rr END)
        
        ELSE t1.current_gmv END as forecast
    FROM bi.temp_current_gmv t1
    LEFT JOIN bi.runrate_dow t2
        ON t1.dow = t2.dow
        AND t1.locale = t2.locale
        AND t1.polygon = t2.polygon

;

DROP TABLE IF EXISTS bi.runrate_dow;
DROP TABLE IF EXISTS bi.temp_timeseries;
DROP TABLE IF EXISTS bi.temp_current_gmv;