
-- Channel share L30D ====> ONLY FOR RR P1 THEN !!!!

	DROP TABLE IF EXISTS bi.temp_acquisition_share_L30D;
	CREATE TABLE bi.temp_acquisition_share_L30D AS
		
		SELECT

			left(o.locale__c,2) as locale,
			CASE WHEN o.marketing_channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE o.marketing_channel END as channel,
			SUM(CASE WHEN o.order_creation__c >= (current_date - interval '30 days') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END)::numeric/AVG(a.locale_acquisitions_L30D)::numeric as channel_share_L30D,
			SUM(CASE WHEN o.order_creation__c >= (current_date - interval '90 days') AND o.order_creation__c < (current_date - interval '60 days') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END)::numeric/AVG(a.locale_acquisitions_L60to90D)::numeric as channel_share_L60to90D

		FROM bi.orders o

		JOIN bi.temp_acquisitions_locale_L30D a ON a.locale = left(o.locale__c,2)

		WHERE o.test__c = '0'
			AND o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
			AND order_type = '1'
			AND o.marketing_channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND o.order_creation__c >= (current_date - interval '90 days')

		GROUP BY left(o.locale__c,2), CASE WHEN o.marketing_channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE o.marketing_channel END

		ORDER BY locale asc, channel asc

	;