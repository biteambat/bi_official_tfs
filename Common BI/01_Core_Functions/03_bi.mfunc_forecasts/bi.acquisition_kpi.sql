
DROP TABLE IF EXISTS bi.acquisition_kpi;
CREATE TABLE bi.acquisition_kpi as 
SELECT
    LEFT(locale__c,2)::text as locale,
    'Actuals'::text as kpi,
    CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE a.Marketing_Channel END as Channel,
    COUNT(1) as value
FROM
    bi.orders a
WHERE
    Acquisition_New_Customer__c = '1'
    and test__c = '0'
    and order_type = '1'
    and order_creation__c::date = customer_creation_date::date
    and (EXTRACT(MONTH FROM Order_Creation__c::date) = EXTRACT(MONTH FROM  current_date) and EXTRACT(YEAR FROM Order_Creation__c::date) = EXTRACT(YEAR FROM  current_date))
    and status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
GROUP BY
    locale,
    Channel;

INSERT INTO bi.acquisition_kpi  
SELECT
    LEFT(locale__c,2) as locale,
    'Runrate per Day' as kpi,
    CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
    CAST(COUNT(1)/9 as decimal) as Acquisitions
FROM
    bi.orders a
WHERE
    Acquisition_New_Customer__c = '1'
    and test__c = '0'
    and order_type = '1'
    and order_creation__c::date = customer_creation_date::date
    and Order_Creation__c between (cast(current_date as date)- interval '9 days') and (cast(current_date as date)- interval '1 days')
    and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
    locale,
    Channel;
INSERT INTO bi.acquisition_kpi
SELECT
    a.locale,
    'Forecast' as kpis,
    CASE WHEN a.channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE a.channel END as Channel,
    acquisitions+(b.runrate*(month_days-days)) as value
FROM    
(SELECT
    LEFT(locale__c,2) as locale,
    'Actuals' as kpi,
    CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
                          DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
    Extract(day from current_date-1) as days,
    COUNT(1) as Acquisitions
FROM
    bi.orders a
WHERE
    Acquisition_New_Customer__c = '1'
    and order_type = '1'
    and order_creation__c::date = customer_creation_date::date
    and test__c = '0'
    and (EXTRACT(MONTH FROM Order_Creation__c::date) = EXTRACT(MONTH FROM  current_date) and EXTRACT(YEAR FROM Order_Creation__c::date) = EXTRACT(YEAR FROM  current_date))
    and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
GROUP BY
    locale,
    kpi,
    channel,
    days,
    month_days) as a
LEFT JOIN
    (SELECT
    LEFT(locale__c,2) as locale,
    'Runrate per Day' as kpi,
    CASE WHEN a.marketing_channel in ('SEM Brand','SEO Brand','DTI','Brand Marketing Offline') THEN 'Brand Marketing' ELSE marketing_channel END as Channel,
    round(CAST(CAST(COUNT(1) as decimal)/15 as decimal),2) as runrate
FROM
    bi.orders a
WHERE
    Acquisition_New_Customer__c = '1'
    and order_type = '1'
    and test__c = '0'
    and Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days')
    and status not in ('CANCELLED FAKED','CANCELLED SKIPPED')
    and order_creation__c::date = customer_creation_date::date
GROUP BY
    locale,
    channel) as b
ON
    a.locale = b.locale and a.channel = b.channel;
    
INSERT INTO bi.acquisition_kpi Values('at','Target','Facebook','0');
INSERT INTO bi.acquisition_kpi Values('at','Target','SEO','0');
INSERT INTO bi.acquisition_kpi Values('at','Target','Brand Marketing','0');
INSERT INTO bi.acquisition_kpi Values('at','Target','SEM','0');
INSERT INTO bi.acquisition_kpi Values('at','Target','Voucher Campaigns','0');
INSERT INTO bi.acquisition_kpi Values('at','Target','Display','0');
INSERT INTO bi.acquisition_kpi Values('at','Target','Unattributed','0');

INSERT INTO bi.acquisition_kpi Values('ch','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Voucher Campaigns','12');
INSERT INTO bi.acquisition_kpi Values('ch','Target','SEM','41');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Brand Marketing','103');
INSERT INTO bi.acquisition_kpi Values('ch','Target','SEO','50');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Facebook','10');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Newsletter','0');
INSERT INTO bi.acquisition_kpi Values('ch','Target','Display','8');

INSERT INTO bi.acquisition_kpi Values('de','Target','Brand Marketing','309');
INSERT INTO bi.acquisition_kpi Values('de','Target','Display','12');
INSERT INTO bi.acquisition_kpi Values('de','Target','SEM','80');
INSERT INTO bi.acquisition_kpi Values('de','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('de','Target','Voucher Campaigns','0');
INSERT INTO bi.acquisition_kpi Values('de','Target','SEO','153');
INSERT INTO bi.acquisition_kpi Values('de','Target','Newsletter','0');
INSERT INTO bi.acquisition_kpi Values('de','Target','Facebook','18');

INSERT INTO bi.acquisition_kpi Values('nl','Target','SEM','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Brand Marketing','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Display','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Voucher Campaigns','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Unattributed','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','SEO','0');
INSERT INTO bi.acquisition_kpi Values('nl','Target','Facebook','0');