
DELETE FROM bi.invoiced_acq_locked_polygon WHERE forecast_date = current_date;

    INSERT INTO bi.invoiced_acq_locked_polygon

        SELECT
            current_date::date as forecast_date,
            i.locale::text as locale,
            i.city_polygon::text as city_polygon,
            i.forecast_invoiced_acquisitions::numeric as forecast
        FROM bi.invoiced_gmv_forecast_polygon i

    ;