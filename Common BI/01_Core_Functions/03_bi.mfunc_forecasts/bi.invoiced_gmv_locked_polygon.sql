
DELETE FROM bi.invoiced_gmv_locked_polygon WHERE forecast_date = current_date;

    INSERT INTO bi.invoiced_gmv_locked_polygon

        SELECT
            current_date::date as forecast_date,
            i.locale as locale,
            i.city_polygon as city_polygon,
            i.forecast as forecast
        FROM bi.invoiced_gmv_forecast_polygon i

    ;