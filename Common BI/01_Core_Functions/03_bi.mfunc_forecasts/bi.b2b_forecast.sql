
    DROP TABLE IF EXISTS bi.b2b_forecast;

    CREATE TABLE bi.b2b_forecast AS
    SELECT
    left(locale__c,2) as locale,
    CASE WHEN polygon is null THEN 'Other' ELSE polygon END as polygon,
    0 as acquisition_forecast,
    0 as rebookings_forecast,
    SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as bookings_forecast,
    0 as supply_forecast
FROM    
    bi.b2borders 
WHERE
    TO_CHAR(current_date,'YYYY-MM') = year_month
GROUP BY
    locale,
    polygon;