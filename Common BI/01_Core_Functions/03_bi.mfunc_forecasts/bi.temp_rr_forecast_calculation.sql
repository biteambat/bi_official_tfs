
DROP TABLE IF EXISTS bi.temp_rr_forecast_calculation;

	CREATE TABLE bi.temp_rr_forecast_calculation AS 

		SELECT
			t1.locale,
			t1.channel,
			t1.channel_share_L30D,
			t1.channel_share_L60to90D,
			t2.rr_avg_L15D,
			t3.rr_avg_L15D as rr_avg_L60to90D

		FROM bi.temp_acquisition_share_L30D t1

		JOIN bi.temp_RR_L30D t2 ON t1.locale = t2.locale
			AND t1.channel = t2.channel 
			AND t2.period = 'p1'

		JOIN bi.temp_RR_L30D t3 ON t1.locale = t3.locale
			AND t1.channel = t3.channel 
			AND t3.period = 'p3'

	;