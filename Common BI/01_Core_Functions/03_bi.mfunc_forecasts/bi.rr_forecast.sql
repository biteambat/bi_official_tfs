
    DROP TABLE IF EXISTS bi.temp_acquisitions_locale_L30D;
    CREATE TABLE bi.temp_acquisitions_locale_L30D AS

        SELECT
        left(o.locale__c,2) as locale,
        SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.order_creation__c >= (current_date - interval '30 days') THEN 1 ELSE NULL END) as locale_acquisitions_L30D,
        SUM(CASE WHEN o.acquisition_new_customer__c = '1' AND o.order_creation__c >= (current_date - interval '90 days') AND o.order_creation__c < (current_date - interval '60 days') THEN 1 ELSE NULL END) as locale_acquisitions_L60to90D

        FROM bi.orders o

        WHERE o.test__c = '0'
            AND o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
            AND order_type = '1'
            AND o.marketing_channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
            AND o.order_creation__c >= (current_date - interval '90 days')

        GROUP BY left(o.locale__c,2)

        ORDER BY left(o.locale__c,2)

    ;

    -- avg RR L30D per channel and locale

    DROP TABLE IF EXISTS bi.temp_RR_L30D;
    CREATE TABLE bi.temp_RR_L30D AS

        SELECT
            r.locale as locale,
            r.channel as channel,
            'p1'::text as period,
            SUM(r.cohort_return_rate * r.acquisitions)/SUM(r.acquisitions) as rr_avg_L15D

        FROM bi.retention_marketing r
        WHERE r.date < current_date and r.date >= (current_date - interval '15 days')
            AND r.kpi_type = 'Channel analysis'
            AND r.channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
            AND r.acquisitions > '0'
            AND r.period = 'p1'
        GROUP BY r.locale, r.channel, period
        ORDER BY r.locale asc, r.channel asc

    ;

    INSERT INTO bi.temp_RR_L30D
        
        SELECT
            r.locale as locale,
            r.channel as channel,
            'p3'::text as period,
            SUM(r.cohort_return_rate * r.acquisitions)/SUM(r.acquisitions) as rr_avg_L15D

        FROM bi.retention_marketing r
        WHERE r.date < current_date and r.date >= (current_date - interval '15 days')
            AND r.kpi_type = 'Channel analysis'
            AND r.channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
            AND r.acquisitions > '0'
            AND r.period = 'p3'
        GROUP BY r.locale, r.channel, period
        ORDER BY r.locale asc, r.channel asc

    ;

    -- Channel share L30D ====> ONLY FOR RR P1 THEN !!!!

    DROP TABLE IF EXISTS bi.temp_acquisition_share_L30D;
    CREATE TABLE bi.temp_acquisition_share_L30D AS
        
        SELECT

            left(o.locale__c,2) as locale,
            CASE WHEN o.marketing_channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE o.marketing_channel END as channel,
            SUM(CASE WHEN o.order_creation__c >= (current_date - interval '30 days') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END)::numeric/AVG(a.locale_acquisitions_L30D)::numeric as channel_share_L30D,
            SUM(CASE WHEN o.order_creation__c >= (current_date - interval '90 days') AND o.order_creation__c < (current_date - interval '60 days') AND o.acquisition_new_customer__c = '1' THEN 1 ELSE NULL END)::numeric/AVG(a.locale_acquisitions_L60to90D)::numeric as channel_share_L60to90D

        FROM bi.orders o

        JOIN bi.temp_acquisitions_locale_L30D a ON a.locale = left(o.locale__c,2)

        WHERE o.test__c = '0'
            AND o.status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')
            AND order_type = '1'
            AND o.marketing_channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
            AND o.order_creation__c >= (current_date - interval '90 days')

        GROUP BY left(o.locale__c,2), CASE WHEN o.marketing_channel in ('SEM Brand','SEO Brand','DTI') THEN 'Brand Marketing' ELSE o.marketing_channel END

        ORDER BY locale asc, channel asc

    ;

    DROP TABLE IF EXISTS bi.temp_rr_forecast_calculation;

    CREATE TABLE bi.temp_rr_forecast_calculation AS 

        SELECT
            t1.locale,
            t1.channel,
            t1.channel_share_L30D,
            t1.channel_share_L60to90D,
            t2.rr_avg_L15D,
            t3.rr_avg_L15D as rr_avg_L60to90D

        FROM bi.temp_acquisition_share_L30D t1

        JOIN bi.temp_RR_L30D t2 ON t1.locale = t2.locale
            AND t1.channel = t2.channel 
            AND t2.period = 'p1'

        JOIN bi.temp_RR_L30D t3 ON t1.locale = t3.locale
            AND t1.channel = t3.channel 
            AND t3.period = 'p3'

    ;

    DROP TABLE IF EXISTS bi.rr_forecast;
    CREATE TABLE bi.rr_forecast AS

        SELECT
            f.locale,
            SUM(f.channel_share_L30D*f.rr_avg_L15D) as rr_m1_forecast,
            SUM(f.channel_share_L60to90D*f.rr_avg_L60to90D) as rr_m2_forecast
        FROM bi.temp_rr_forecast_calculation f

        GROUP BY f.locale

        ORDER BY f.locale asc

    ;

    DROP TABLE IF EXISTS bi.temp_acquisitions_locale_L30D;
    DROP TABLE IF EXISTS bi.temp_RR_L30D;
    DROP TABLE IF EXISTS bi.temp_acquisition_share_L30D;
    DROP TABLE IF EXISTS bi.temp_rr_forecast_calculation;