
-- avg RR L30D per channel and locale

	DROP TABLE IF EXISTS bi.temp_RR_L30D;
	CREATE TABLE bi.temp_RR_L30D AS

		SELECT
			r.locale as locale,
			r.channel as channel,
			'p1'::text as period,
			SUM(r.cohort_return_rate * r.acquisitions)/SUM(r.acquisitions) as rr_avg_L15D

		FROM bi.retention_marketing r
		WHERE r.date < current_date and r.date >= (current_date - interval '15 days')
			AND r.kpi_type = 'Channel analysis'
			AND r.channel NOT IN ('Unattributed','Newsletter','Facebook Organic')
			AND r.acquisitions > '0'
			AND r.period = 'p1'
		GROUP BY r.locale, r.channel, period
		ORDER BY r.locale asc, r.channel asc

	;