
DROP TABLE IF EXISTS bi.daily_classifiedscosts_polygon;
CREATE TABLE bi.daily_classifiedscosts_polygon AS

	 SELECT
	  t1.date as date,
	  LEFT(t2.polygon,2) as locale,
	  t2.polygon as polygon,
	  sum(CASE WHEN t2.start_date <= t1.date AND t2.end_date >= t1.date THEN daily_costs_eur else 0 end) as daily_costs
	 FROM bi.temp_dateseries t1,
	 external_data.zapier_onboardingcosts t2
	 GROUP BY
	  t1.date,
	  LEFT(t2.polygon,2),
	  t2.polygon
	 ORDER BY 
		t1.date desc,
		t2.polygon asc

;