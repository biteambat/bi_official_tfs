
DROP TABLE IF EXISTS bi.daily_signup_sem_polygon;
CREATE TABLE bi.daily_signup_sem_polygon AS

	SELECT 
		* 
	FROM
		(
		SELECT 
			date,
			locale,
			CASE WHEN polygon = 'nl-other' THEN 'nl-amsterdam' ELSE polygon END as polygon,
			SUM(adwords_cost_signup + display_cost_signup) as cost
		FROM bi.costsgrouping_adwords
		GROUP BY date, locale, CASE WHEN polygon = 'nl-other' THEN 'nl-amsterdam' ELSE polygon END
		ORDER BY date desc, polygon asc, locale asc
		) as t1

	WHERE t1.cost > 0

;