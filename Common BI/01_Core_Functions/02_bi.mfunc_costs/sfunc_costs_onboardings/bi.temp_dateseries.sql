
DROP TABLE IF EXISTS bi.temp_dateseries;
CREATE TABLE bi.temp_dateseries AS 

	(SELECT i::date as date FROM generate_series('2016-05-25', 
	  current_date - interval '1 day', '1 day'::interval) i)

;