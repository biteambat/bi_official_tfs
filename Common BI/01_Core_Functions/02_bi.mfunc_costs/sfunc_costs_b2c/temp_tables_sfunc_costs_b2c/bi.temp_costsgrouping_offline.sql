
DROP TABLE IF EXISTS bi.temp_costsgrouping_offline;
CREATE TABLE bi.temp_costsgrouping_offline AS

	SELECT 
		date::date,
		LEFT(t1.polygon, 2) as locale,
		CASE WHEN t1.polygon like '%-other' THEN 'no-polygon' ELSE t1.polygon END as polygon,
		SUM(CASE WHEN t1.daily_costs_eur IS NULL THEN 0 ELSE t1.daily_costs_eur END) as offline_cost

	FROM generate_series(
	  '2016-08-01'::date,
	  current_date::date,
	  '1 day'::interval
	) date

	LEFT JOIN external_data.zapier_offlinecosts t1
		ON date >= t1.start_date
		AND date <= t1.end_date
		AND t1.manual_validation = 'Yes'
		AND t1.formula_validation = 'Yes'

	GROUP BY date::date, 
		LEFT(t1.polygon, 2),
		t1.polygon

	ORDER BY date::date asc,
		t1.polygon asc

;