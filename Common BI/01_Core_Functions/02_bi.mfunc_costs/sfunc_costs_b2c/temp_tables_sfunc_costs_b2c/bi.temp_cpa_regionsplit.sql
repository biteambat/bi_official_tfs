
DROP TABLE IF EXISTS bi.temp_cpa_regionsplit;
CREATE TABLE bi.temp_cpa_regionsplit AS

	SELECT 
		t1.locale::text, 
		t1.district::text,
		SUM(t1.cost) as district_cost

	FROM bi.etl_adwords_costsperpolygon t1

	WHERE t1.date >= (current_date - interval '90 days') AND t1.city NOT LIKE ('%-other') AND t1.city NOT IN ('no-polygon')

	GROUP BY t1.locale, t1.district
	ORDER BY t1.locale asc, t1.district asc

;