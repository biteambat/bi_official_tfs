
DROP TABLE IF EXISTS bi.temp_pcentofdistrict;
CREATE TABLE bi.temp_pcentofdistrict AS

	SELECT

		t1.locale::text,
		t1.district::text,
		t1.city::text,
		CASE WHEN SUM(t2.district_cost) > 0 THEN SUM(t1.city_cost)::numeric/SUM(t2.district_cost) ELSE 1 END as pcent_of_districtcost

	FROM bi.temp_cpa_citysplit t1

	JOIN bi.temp_cpa_regionsplit t2 

	ON (t1.district = t2.district AND t1.locale = t2.locale)

	GROUP BY t1.locale, t1.district, t1.city, t2.locale, t2.district

	ORDER BY t1.locale asc, t1.district asc, t1.city asc

;