
DROP TABLE IF EXISTS bi.temp_timeseries_w_polygons;
CREATE TABLE bi.temp_timeseries_w_polygons AS

	SELECT 
		date::date as date,
		t2.locale,
		t2.polygon
	FROM generate_series(
	  '2016-08-01'::date,
	  current_date::date,
	  '1 day'::interval
	) date

	JOIN 
	(SELECT DISTINCT
		LEFT(locale__c, 2) as locale,
		CASE WHEN polygon IS NULL THEN 'no-polygon' ELSE polygon END as polygon
	FROM bi.orders
	WHERE test__c = '0' AND order_type = '1'
	ORDER BY locale asc, polygon asc) t2 
	ON 1=1
;