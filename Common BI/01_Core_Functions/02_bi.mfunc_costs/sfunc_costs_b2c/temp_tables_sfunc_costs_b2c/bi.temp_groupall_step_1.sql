
DROP TABLE IF EXISTS bi.temp_groupall_step_1;
CREATE TABLE bi.temp_groupall_step_1 AS

	SELECT
		t1.date,
		t1.locale,
		CASE WHEN t2.district IS NULL THEN 'Other' ELSE t2.district END as district,
		t1.polygon

	FROM bi.temp_timeseries_w_polygons t1

	LEFT JOIN bi.costsgrouping_adwords t2
		ON t1.date = t2.date 
		AND t1.locale = t2.locale 
		AND t1.polygon = t2.polygon

	WHERE t1.date <= t2.date

	ORDER BY date asc, locale asc, polygon asc
;