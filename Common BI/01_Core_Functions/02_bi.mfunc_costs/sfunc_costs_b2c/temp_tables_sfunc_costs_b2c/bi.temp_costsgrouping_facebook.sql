
DROP TABLE IF EXISTS bi.temp_costsgrouping_facebook;
CREATE TABLE bi.temp_costsgrouping_facebook AS

	SELECT 
		t1.date::date,
		t1.locale::text,
		t1.region::text,
		SUM(t1.spend) as facebook_costs

	FROM bi.etl_facebook_costsperdistrict t1

	GROUP BY 
	t1.date,
	t1.locale,
	t1.region

	ORDER BY
	t1.date desc,
	t1.locale asc,
	t1.region asc

;