
DROP TABLE IF EXISTS bi.temp_channels_acq_discounts;
CREATE TABLE bi.temp_channels_acq_discounts AS

	SELECT
		t5.order_creation__c::date as date,
		CASE WHEN LEFT(t5.locale__c, 2) IS NOT NULL THEN LEFT(t5.locale__c, 2) ELSE 'No locale' END as locale,
		CASE WHEN t5.polygon IS NULL THEN 'no-polygon' ELSE t5.polygon END as polygon,

		SUM(CASE WHEN t5.marketing_channel = 'SEM' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as sem_discount,
		SUM(CASE WHEN t5.marketing_channel = 'SEM Brand' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as sem_brand_discount,
		SUM(CASE WHEN t5.marketing_channel = 'Display' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as display_discount,
		SUM(CASE WHEN t5.marketing_channel = 'Facebook' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as facebook_discount,
		SUM(CASE WHEN t5.marketing_channel = 'Facebook Organic' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as facebook_organic_discount,
		SUM(CASE WHEN t5.marketing_channel = 'Brand Marketing Offline' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as offline_discount,
		SUM(CASE WHEN t5.marketing_channel = 'Voucher Campaigns' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as vouchers_cost,
		SUM(CASE WHEN t5.marketing_channel = 'Newsletter' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as newsletter_discount,
		SUM(CASE WHEN t5.marketing_channel = 'SEO' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as seo_discount,
		SUM(CASE WHEN t5.marketing_channel = 'SEO Brand' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as seo_brand_discount,
		SUM(CASE WHEN t5.marketing_channel = 'Youtube Paid' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as youtube_discount,

		SUM(CASE WHEN t5.marketing_channel = 'SEM' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as sem_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'SEM Brand' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as sem_brand_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Display' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as display_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Facebook' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as facebook_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Facebook Organic' AND t5.acquisition_new_customer__c = '0'  AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as facebook_organic_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Brand Marketing Offline' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as offline_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Voucher Campaigns' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as vouchers_cost_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Newsletter' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as newsletter_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'SEO' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as seo_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'SEO Brand' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as seo_brand_discount_reb,
		SUM(CASE WHEN t5.marketing_channel = 'Youtube Paid' AND t5.acquisition_new_customer__c = '0' AND t5.status NOT LIKE ('%CANCELLED%') THEN discount__c ELSE 0 END)::numeric as youtube_discount_reb,

		SUM(CASE WHEN t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as all_acq,
		SUM(CASE WHEN t5.marketing_channel = 'SEM' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as sem_acq,
		SUM(CASE WHEN t5.marketing_channel = 'SEM Brand' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as sem_brand_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Display' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as display_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Facebook' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as facebook_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Facebook Organic' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as facebook_organic_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Brand Marketing Offline' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as offline_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Voucher Campaigns' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as vouchers_acq,
		SUM(CASE WHEN t5.marketing_channel = 'DTI' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as dti_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Newsletter' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as newsletter_acq,
		SUM(CASE WHEN t5.marketing_channel = 'SEO' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as seo_acq,
		SUM(CASE WHEN t5.marketing_channel = 'SEO Brand' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as seo_brand_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Youtube Paid' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as youtube_acq,
		SUM(CASE WHEN t5.marketing_channel = 'Unattributed' AND t5.acquisition_new_customer__c = '1' AND t5.order_creation__c::date = t5.customer_creation_date::date THEN 1 ELSE 0 END)::numeric as other_acq,
		MAX(new_subscribers) as all_subscribers
	FROM bi.orders t5
	LEFT JOIn
		(SELECT
	mindate as date,
	locale,
	polygon,
	COUNT(DISTINCT(contact__c)) as new_subscribers
FROM(
SELECT
	contact__c,
	polygon,
	LEFT(locale__c,2) as locale,
	min(order_Creation__c::date) as mindate
FROM
	bi.orders
WHERE
	status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
	and recurrency__c > '0'
GROUP BY
	contact__c,
	polygon,
	locale) as a
GROUP BY
	Date,
	polygon,
	locale) as t2
ON
	(LEFT(t5.locale__c,2) = t2.locale and t5.polygon = t2.polygon and t5.order_creation__c::date = t2.date)

	WHERE t5.test__c = '0'
		AND t5.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
		AND t5.order_creation__c::date >= '2016-08-01'
		AND t5.order_type = '1'

	GROUP BY t5.order_creation__c::date,
		LEFT(t5.locale__c, 2),
		t5.polygon

	ORDER BY t5.order_creation__c::date desc,
		LEFT(t5.locale__c, 2) asc,
		t5.polygon asc	
;