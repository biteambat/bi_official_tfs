
DROP TABLE IF EXISTS bi.temp_cpa_citysplit;
CREATE TABLE bi.temp_cpa_citysplit AS
	
	SELECT 
		t1.locale::text, 
		t1.district::text, 
		t1.city::text,
		SUM(t1.cost) as city_cost

	FROM bi.etl_adwords_costsperpolygon t1

	WHERE t1.date >= (current_date - interval '90 days') AND t1.city NOT LIKE ('%-other') AND t1.city NOT IN ('no-polygon')

	GROUP BY t1.locale, t1.district, t1.city
	ORDER BY t1.locale asc, t1.district asc, t1.city asc

;