
DROP TABLE IF EXISTS bi.temp_costsgrouping_fbleverate;
CREATE TABLE bi.temp_costsgrouping_fbleverate AS

	SELECT 
		t1.cost_date::date as date,
		LOWER(t1.locale)::text as locale,
		
		(CASE WHEN
			region IN ('Baden-Württemberg') THEN 'Baden-Wurttemberg'
			WHEN region IN ('Bayern') THEN 'Bavaria'
			WHEN region IN ('Berlin', 'Brandenburg') THEN 'Berlin'
			WHEN region IN  ('Bremen', 'Niedersachsen') THEN 'Lower Saxony & Bremen'
			WHEN region IN ('Hamburg', 'Schleswig-Holstein') THEN 'Hamburg & Schleswig-Holstein'
			WHEN region IN ('Rheinland-Pfalz', 'Saarland') THEN 'Rhineland-Palatinate & Saarland'
			WHEN region IN ('Hessen') THEN 'Hesse'
			WHEN region IN ('Mecklenburg-Vorpommern') THEN 'Mecklenburg-Vorpommern'
			WHEN region IN ('Nordrhein-Westfalen') THEN 'North Rhine-Westphalia'
			WHEN region IN ('Sachsen', 'Saxony-Anhalt') THEN 'Saxony & Saxony-Anhalt'
			WHEN region IN ('Thüringen') THEN 'Thuringia'

			WHEN region IN ('Basel-City', 'Basel-Landschaft') THEN 'Basel-Stadt'
			WHEN region IN ('Bern') THEN 'Canton of Bern'
			WHEN region IN ('Canton of Geneva') THEN 'Canton of Geneva'
			WHEN region IN  ('Vaud') THEN 'Canton of Vaud'
			WHEN region IN ('Luzern') THEN 'Canton of Lucerne'
			WHEN region IN ('Canton of St. Gallen') THEN 'Canton of St. Gallen'
			WHEN region IN ('Zürich') THEN 'Canton of Zurich'

			WHEN region LIKE ('%Amsterdam%') THEN 'Amsterdam'
			WHEN region LIKE ('%Vienna%') THEN 'Vienna'

			WHEN region NOT IN ('Basel-City', 'Basel-Landschaft', 'Bern', 'Canton of Geneva', 'Vaud', 'Luzern','Canton of St. Gallen', 'Zürich', 'Baden-Württemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen', 'Niedersachsen', 'Hamburg', 'Schleswig-Holstein', 'Rheinland-Pfalz', 'Saarland', 'Hessen', 'Mecklenburg-Vorpommern', 'Nordrhein-Westfalen', 'Sachsen', 'Saxony-Anhalt', 'Thüringen') THEN 'Other'
			ELSE 'Other' END)::text

		as region,

		SUM(t1.cost) as fbleverate_costs

	FROM external_data.zapier_fbleverate t1

	GROUP BY 
	t1.cost_date,
	LOWER(t1.locale),
		(CASE WHEN
			region IN ('Baden-Württemberg') THEN 'Baden-Wurttemberg'
			WHEN region IN ('Bayern') THEN 'Bavaria'
			WHEN region IN ('Berlin', 'Brandenburg') THEN 'Berlin'
			WHEN region IN  ('Bremen', 'Niedersachsen') THEN 'Lower Saxony & Bremen'
			WHEN region IN ('Hamburg', 'Schleswig-Holstein') THEN 'Hamburg & Schleswig-Holstein'
			WHEN region IN ('Rheinland-Pfalz', 'Saarland') THEN 'Rhineland-Palatinate & Saarland'
			WHEN region IN ('Hessen') THEN 'Hesse'
			WHEN region IN ('Mecklenburg-Vorpommern') THEN 'Mecklenburg-Vorpommern'
			WHEN region IN ('Nordrhein-Westfalen') THEN 'North Rhine-Westphalia'
			WHEN region IN ('Sachsen', 'Saxony-Anhalt') THEN 'Saxony & Saxony-Anhalt'
			WHEN region IN ('Thüringen') THEN 'Thuringia'

			WHEN region IN ('Basel-City', 'Basel-Landschaft') THEN 'Basel-Stadt'
			WHEN region IN ('Bern') THEN 'Canton of Bern'
			WHEN region IN ('Canton of Geneva') THEN 'Canton of Geneva'
			WHEN region IN  ('Vaud') THEN 'Canton of Vaud'
			WHEN region IN ('Luzern') THEN 'Canton of Lucerne'
			WHEN region IN ('Canton of St. Gallen') THEN 'Canton of St. Gallen'
			WHEN region IN ('Zürich') THEN 'Canton of Zurich'

			WHEN region LIKE ('%Amsterdam%') THEN 'Amsterdam'
			WHEN region LIKE ('%Vienna%') THEN 'Vienna'

			WHEN region NOT IN ('Basel-City', 'Basel-Landschaft', 'Bern', 'Canton of Geneva', 'Vaud', 'Luzern','Canton of St. Gallen', 'Zürich', 'Baden-Württemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen', 'Niedersachsen', 'Hamburg', 'Schleswig-Holstein', 'Rheinland-Pfalz', 'Saarland', 'Hessen', 'Mecklenburg-Vorpommern', 'Nordrhein-Westfalen', 'Sachsen', 'Saxony-Anhalt', 'Thüringen') THEN 'Other'
			ELSE 'Other' END)::text
	
	ORDER BY
	date desc,
	locale asc,
	region asc

;