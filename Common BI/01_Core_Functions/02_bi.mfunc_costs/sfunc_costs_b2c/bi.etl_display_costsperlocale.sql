
DROP TABLE IF EXISTS bi.etl_display_costsperlocale;
CREATE TABLE bi.etl_display_costsperlocale AS

	SELECT
		date::date,
		t1.locale as locale,
		CASE WHEN t1.locale IN ('de', 'ch') THEN 'Other'
				WHEN t1.locale = 'at' THEN 'Vienna'
				WHEN t1.locale = 'nl' THEN 'Region of Amsterdam'
				ELSE NULL END::text
		as district,
		CASE WHEN t1.polygon LIKE ('%-other') THEN 'no-polygon' ELSE t1.polygon END::text as polygon,
		SUM(CASE WHEN t1.daily_costs_eur IS NULL THEN 0 ELSE t1.daily_costs_eur END) as cost

	FROM generate_series(
		'2016-07-01'::date,
		current_date::date,
		'1 day'::interval
	) date

	LEFT JOIN external_data.zapier_displaycosts t1
			ON date >= t1.start_date
			AND date <= t1.end_date
			AND t1.manual_validation = 'Yes'
			AND t1.formula_validation = 'Yes'

	GROUP BY date::date, t1.locale, 	CASE WHEN t1.locale IN ('de', 'ch') THEN 'Other'
				WHEN t1.locale = 'at' THEN 'Vienna'
				WHEN t1.locale = 'nl' THEN 'Region of Amsterdam'
				ELSE NULL END::text, CASE WHEN t1.polygon LIKE ('%-other') THEN 'no-polygon' ELSE t1.polygon END::text
	ORDER BY date::date desc, polygon asc

;