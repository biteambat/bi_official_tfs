
DROP TABLE IF EXISTS bi.offline_cost_channel;
CREATE TABLE bi.offline_cost_channel AS

	SELECT 
		date::date,
		LEFT(t1.polygon, 2) as locale,
		t1.polygon as polygon,
		t1.campaign_type as campaign_type,
		SUM(CASE WHEN t1.daily_costs_eur IS NULL THEN 0 ELSE t1.daily_costs_eur END) as cost

	FROM generate_series(
	  '2016-08-01'::date,
	  current_date::date,
	  '1 day'::interval
	) date

	LEFT JOIN external_data.zapier_offlinecosts t1
		ON date >= t1.start_date
		AND date <= t1.end_date
		AND t1.manual_validation = 'Yes'
		AND t1.formula_validation = 'Yes'

	GROUP BY date::date, 
		LEFT(t1.polygon, 2),
		t1.polygon,
		t1.campaign_type

	ORDER BY date::date asc,
		t1.polygon asc, t1.campaign_type asc

;