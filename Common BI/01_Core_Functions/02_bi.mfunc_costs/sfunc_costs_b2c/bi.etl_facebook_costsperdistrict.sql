
DROP TABLE IF EXISTS bi.etl_facebook_costsperdistrict;
CREATE TABLE bi.etl_facebook_costsperdistrict AS

	SELECT
				date as date,
				LOWER(LEFT(ad_account,2)) as locale,
		
				(CASE 
					WHEN LOWER(LEFT(ad_account,2)) = 'at' THEN 'Vienna'
		
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Baden-Württemberg') THEN 'Baden-Wurttemberg'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Bayern') THEN 'Bavaria'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Berlin', 'Brandenburg') THEN 'Berlin'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN  ('Bremen', 'Niedersachsen') THEN 'Lower Saxony & Bremen'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Hamburg', 'Schleswig-Holstein') THEN 'Hamburg & Schleswig-Holstein'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Rheinland-Pfalz', 'Saarland') THEN 'Rhineland-Palatinate & Saarland'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Hessen') THEN 'Hesse'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Mecklenburg-Vorpommern') THEN 'Mecklenburg-Vorpommern'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Nordrhein-Westfalen') THEN 'North Rhine-Westphalia'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Sachsen', 'Saxony-Anhalt') THEN 'Saxony & Saxony-Anhalt'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Thüringen') THEN 'Thuringia'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region NOT IN ('Baden-Württemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen', 'Niedersachsen', 'Hamburg', 'Schleswig-Holstein', 'Rheinland-Pfalz', 'Saarland', 'Hessen', 'Mecklenburg-Vorpommern', 'Nordrhein-Westfalen', 'Sachsen', 'Saxony-Anhalt', 'Thüringen') THEN 'Other'
		
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Basel-City', 'Basel-Landschaft') THEN 'Basel-Stadt'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Bern') THEN 'Canton of Bern'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Canton of Geneva') THEN 'Canton of Geneva'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN  ('Vaud') THEN 'Canton of Vaud'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Luzern') THEN 'Canton of Lucerne'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Canton of St. Gallen') THEN 'Canton of St. Gallen'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Zürich') THEN 'Canton of Zurich'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region NOT IN ('Basel-City', 'Basel-Landschaft', 'Bern', 'Canton of Geneva', 'Vaud', 'Luzern','Canton of St. Gallen', 'Zürich') THEN 'Other'
		
					WHEN LOWER(LEFT(ad_account,2)) = 'nl' THEN 'Amsterdam'
		
					ELSE 'Other' END)::text
		
				as region,
		
				ad_account as ad_account,
				campaign as campaign,
				ad_set as ad_set,
				ad as ad,
				SUM(spend) as spend
		
		
				FROM external_data.facebook_campaigns
		
				GROUP BY 

					date, 
					LOWER(LEFT(ad_account,2)), 
		
					(CASE 
					WHEN LOWER(LEFT(ad_account,2)) = 'at' THEN 'Vienna'
		
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Baden-Württemberg') THEN 'Baden-Wurttemberg'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Bayern') THEN 'Bavaria'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Berlin', 'Brandenburg') THEN 'Berlin'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN  ('Bremen', 'Niedersachsen') THEN 'Lower Saxony & Bremen'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Hamburg', 'Schleswig-Holstein') THEN 'Hamburg & Schleswig-Holstein'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Rheinland-Pfalz', 'Saarland') THEN 'Rhineland-Palatinate & Saarland'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Hessen') THEN 'Hesse'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Mecklenburg-Vorpommern') THEN 'Mecklenburg-Vorpommern'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Nordrhein-Westfalen') THEN 'North Rhine-Westphalia'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Sachsen', 'Saxony-Anhalt') THEN 'Saxony & Saxony-Anhalt'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region IN ('Thüringen') THEN 'Thuringia'
					WHEN LOWER(LEFT(ad_account,2)) = 'de' AND region NOT IN ('Baden-Württemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen', 'Niedersachsen', 'Hamburg', 'Schleswig-Holstein', 'Rheinland-Pfalz', 'Saarland', 'Hessen', 'Mecklenburg-Vorpommern', 'Nordrhein-Westfalen', 'Sachsen', 'Saxony-Anhalt', 'Thüringen') THEN 'Other'
		
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Basel-City', 'Basel-Landschaft') THEN 'Basel-Stadt'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Bern') THEN 'Canton of Bern'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Canton of Geneva') THEN 'Canton of Geneva'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN  ('Vaud') THEN 'Canton of Vaud'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Luzern') THEN 'Canton of Lucerne'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Canton of St. Gallen') THEN 'Canton of St. Gallen'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region IN ('Zürich') THEN 'Canton of Zurich'
					WHEN LOWER(LEFT(ad_account,2)) = 'ch' AND region NOT IN ('Basel-City', 'Basel-Landschaft', 'Bern', 'Canton of Geneva', 'Vaud', 'Luzern','Canton of St. Gallen', 'Zürich') THEN 'Other'
		
					WHEN LOWER(LEFT(ad_account,2)) = 'nl' THEN 'Amsterdam'
		
					ELSE 'Other' END)::text,

					ad_account,
					campaign,
					ad_set,
					ad

				ORDER BY date desc, locale asc, region asc, ad_account asc, campaign asc, ad_set asc, ad asc

;