
DROP TABLE IF EXISTS bi.costsgrouping_adwords;
CREATE TABLE bi.costsgrouping_adwords AS

	SELECT 
		t1.date::date,
		t1.locale::text,
		t1.district::text,
		t1.city::text as polygon,
		SUM(CASE WHEN t1.account IN ('Bookatiger AT Booking Search', 'Bookatiger CH Booking Search DE', 'Bookatiger CH Booking Search FR', 'Bookatiger DE Booking Search', 'Bookatiger NL Booking Search') AND t1.campaign NOT LIKE '%Brand%' THEN t1.cost ELSE 0 END) as adwords_cost,

		SUM(CASE WHEN t1.account IN ('Bookatiger AT Booking Search', 'Bookatiger CH Booking Search DE', 'Bookatiger CH Booking Search FR', 'Bookatiger DE Booking Search', 'Bookatiger NL Booking Search') AND t1.campaign LIKE '%Brand%' THEN t1.cost ELSE 0 END) as adwords_brand_cost,

		SUM(CASE WHEN t1.account IN ('Bookatiger AT Booking GDN', 'Bookatiger CH Booking GDN', 'Bookatiger DE Booking GDN', 'Bookatiger NL Booking GDN') THEN t1.cost ELSE 0 END) as display_cost,

		SUM(CASE WHEN t1.account IN ('Bookatiger Business DE Search', 'Bookatiger Business CH Search', 'Bookatiger Business NL Search', 'Bookatiger Business AT Search') THEN t1.cost ELSE 0 END) as adwords_cost_b2b,
		SUM(CASE WHEN t1.account IN ('Bookatiger Business DE GDN', 'Bookatiger Business AT GDN', 'Bookatiger Business CH GDN', 'Bookatiger Business NL GDN') THEN t1.cost ELSE 0 END) as display_cost_b2b,

		SUM(CASE WHEN t1.account IN ('Bookatiger DE Signup Search', 'Bookatiger NL Signup Search', 'Bookatiger CH Signup Search', 'Bookatiger AT Signup Search') THEN t1.cost ELSE 0 END) as adwords_cost_signup,
		SUM(CASE WHEN t1.account IN ('Bookatiger DE Signup GDN', 'Bookatiger NL Signup GDN', 'Bookatiger AT Signup GDN', 'Bookatiger CH Signup GDN') THEN t1.cost ELSE 0 END) as display_cost_signup,

		SUM(CASE WHEN t1.account IN ('Bookatiger DE YouTube', 'Bookatiger CH YouTube', 'Bookatiger AT YouTube', 'Bookatiger NL YouTube') THEN t1.cost ELSE 0 END) as youtube_cost

	FROM bi.etl_adwords_costsperpolygon t1

	GROUP BY 
	t1.date,
	t1.locale,
	t1.district,
	t1.city

	ORDER BY
	t1.date desc,
	t1.locale asc,
	t1.district asc,
	t1.city asc

;