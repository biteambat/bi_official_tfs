
DROP TABLE IF EXISTS bi.cpacalcpolygon;
CREATE TABLE bi.cpacalcpolygon AS

	SELECT

		t0.date::date,
		CASE WHEN t0.locale::text IS NULL THEN 'No locale' ELSE t0.locale::text END as locale,
		CASE WHEN t0.polygon = 'no-polygon' THEN 'Other'::text ELSE t0.district::text END as district,
		t0.polygon::text,

		SUM(CASE WHEN t1.adwords_cost IS NULL THEN 0 ELSE t1.adwords_cost END) as sem_cost,
		SUM(CASE WHEN t1.adwords_brand_cost IS NULL THEN 0 ELSE t1.adwords_brand_cost END) as sem_brand_cost,
		SUM(CASE WHEN t7.cost IS NULL THEN 0 ELSE t7.cost END) as seo_cost,
		SUM(CASE WHEN t1.display_cost IS NULL THEN 0 ELSE t1.display_cost END) + AVG(CASE WHEN t6.cost IS NULL THEN 0 ELSE t6.cost END) as display_cost,
		
		SUM(CASE WHEN t2.facebook_costs IS NULL THEN 0 ELSE t2.facebook_costs END)*SUM(CASE WHEN (t3.pcent_of_districtcost) IS NULL THEN 0 ELSE t3.pcent_of_districtcost END)
		+ SUM(CASE WHEN t8.fbleverate_costs IS NULL THEN 0 ELSE t8.fbleverate_costs END)*SUM(CASE WHEN (t3.pcent_of_districtcost) IS NULL THEN 0 ELSE t3.pcent_of_districtcost END)
		as facebook_cost,

		SUM(CASE WHEN t4.offline_cost IS NULL THEN 0 ELSE t4.offline_cost END) as offline_cost,
		SUM(CASE WHEN t5.vouchers_cost IS NULL THEN 0 ELSE t5.vouchers_cost END) as vouchers_cost,
		SUM(CASE WHEN t5.vouchers_cost_reb IS NULL THEN 0 ELSE t5.vouchers_cost_reb END) as vouchers_cost_reb,
		SUM(CASE WHEN t1.youtube_cost IS NULL THEN 0 ELSE t1.youtube_cost END) as youtube_cost,

		SUM(CASE WHEN t5.sem_discount IS NULL THEN 0 ELSE t5.sem_discount END) as sem_discount,
		SUM(CASE WHEN t5.sem_brand_discount IS NULL THEN 0 ELSE t5.sem_brand_discount END) as sem_brand_discount,
		SUM(CASE WHEN t5.display_discount IS NULL THEN 0 ELSE t5.display_discount END) as display_discount,
		SUM(CASE WHEN t5.facebook_discount IS NULL THEN 0 ELSE t5.facebook_discount END) as facebook_discount,
		SUM(CASE WHEN t5.facebook_organic_discount IS NULL THEN 0 ELSE t5.facebook_organic_discount END) as facebook_organic_discount,
		SUM(CASE WHEN t5.offline_discount IS NULL THEN 0 ELSE t5.offline_discount END) as offline_discount,
		SUM(CASE WHEN t5.newsletter_discount IS NULL THEN 0 ELSE t5.newsletter_discount END) as newsletter_discount,
		SUM(CASE WHEN t5.seo_discount IS NULL THEN 0 ELSE t5.seo_discount END) as seo_discount,
		SUM(CASE WHEN t5.seo_brand_discount IS NULL THEN 0 ELSE t5.seo_brand_discount END) as seo_brand_discount,
		SUM(CASE WHEN t5.youtube_discount IS NULL THEN 0 ELSE t5.youtube_discount END) as youtube_discount,

		SUM(CASE WHEN t5.sem_discount_reb IS NULL THEN 0 ELSE t5.sem_discount_reb END) as sem_discount_reb,
		SUM(CASE WHEN t5.sem_brand_discount_reb IS NULL THEN 0 ELSE t5.sem_brand_discount_reb END) as sem_brand_discount_reb,
		SUM(CASE WHEN t5.display_discount_reb IS NULL THEN 0 ELSE t5.display_discount_reb END) as display_discount_reb,
		SUM(CASE WHEN t5.facebook_discount_reb IS NULL THEN 0 ELSE t5.facebook_discount_reb END) as facebook_discount_reb,
		SUM(CASE WHEN t5.facebook_organic_discount_reb IS NULL THEN 0 ELSE t5.facebook_organic_discount_reb END) as facebook_organic_discount_reb,
		SUM(CASE WHEN t5.offline_discount_reb IS NULL THEN 0 ELSE t5.offline_discount_reb END) as offline_discount_reb,
		SUM(CASE WHEN t5.newsletter_discount_reb IS NULL THEN 0 ELSE t5.newsletter_discount_reb END) as newsletter_discount_reb,
		SUM(CASE WHEN t5.seo_discount_reb IS NULL THEN 0 ELSE t5.seo_discount_reb END) as seo_discount_reb,
		SUM(CASE WHEN t5.seo_brand_discount_reb IS NULL THEN 0 ELSE t5.seo_brand_discount_reb END) as seo_brand_discount_reb,
		SUM(CASE WHEN t5.youtube_discount_reb IS NULL THEN 0 ELSE t5.youtube_discount_reb END) as youtube_discount_reb,

		SUM(CASE WHEN t5.all_acq IS NULL THEN 0 ELSE t5.all_acq END) as all_acq,
		SUM(CASE WHEN t5.sem_acq IS NULL THEN 0 ELSE t5.sem_acq END) as sem_acq,
		SUM(CASE WHEN t5.sem_brand_acq IS NULL THEN 0 ELSE t5.sem_brand_acq END) as sem_brand_acq,
		SUM(CASE WHEN t5.display_acq IS NULL THEN 0 ELSE t5.display_acq END) as display_acq,
		SUM(CASE WHEN t5.facebook_acq IS NULL THEN 0 ELSE t5.facebook_acq END) as facebook_acq,
		SUM(CASE WHEN t5.facebook_organic_acq IS NULL THEN 0 ELSE t5.facebook_organic_acq END) as facebook_organic_acq,
		SUM(CASE WHEN t5.offline_acq IS NULL THEN 0 ELSE t5.offline_acq END) as offline_acq,
		SUM(CASE WHEN t5.vouchers_acq IS NULL THEN 0 ELSE t5.vouchers_acq END) as vouchers_acq,
		SUM(CASE WHEN t5.dti_acq IS NULL THEN 0 ELSE t5.dti_acq END) as dti_acq,
		SUM(CASE WHEN t5.newsletter_acq IS NULL THEN 0 ELSE t5.newsletter_acq END) as newsletter_acq,
		SUM(CASE WHEN t5.seo_acq IS NULL THEN 0 ELSE t5.seo_acq END) as seo_acq,
		SUM(CASE WHEN t5.seo_brand_acq IS NULL THEN 0 ELSE t5.seo_brand_acq END) as seo_brand_acq,
		SUM(CASE WHEN t5.youtube_acq IS NULL THEN 0 ELSE t5.youtube_acq END) as youtube_acq,
		SUM(CASE WHEN t5.other_acq IS NULL THEN 0 ELSE t5.other_acq END) as other_acq,
		SUM(CASE WHEN t5.all_subscribers is NULL THEN 0 ELSE t5.all_subscribers END) all_subscribers

	FROM bi.temp_groupall_step_1 t0

	LEFT JOIN bi.costsgrouping_adwords t1
		ON t0.date = t1.date 
		AND t0.locale = t1.locale
		AND t0.district = t1.district
		AND t0.polygon = t1.polygon

	LEFT JOIN bi.temp_costsgrouping_facebook t2 
		ON t0.date = t2.date
		AND t0.locale = t2.locale
		AND t0.district = t2.region

	LEFT JOIN bi.temp_costsgrouping_fbleverate t8 
		ON t0.date = t8.date
		AND t0.locale = t8.locale
		AND t0.district = t8.region

	LEFT JOIN bi.temp_pcentofdistrict t3
		ON t0.locale = t3.locale
		AND t0.district = t3.district
		AND t0.polygon = t3.city

	LEFT JOIN bi.temp_costsgrouping_offline t4
		ON t0.date = t4.date
		AND t0.locale = t4.locale
		AND t0.polygon = t4.polygon

	LEFT JOIN bi.temp_channels_acq_discounts t5
		ON t0.date = t5.date
		AND t0.locale = t5.locale
		AND t0.polygon = t5.polygon

	LEFT JOIN bi.etl_display_costsperlocale t6
		ON t0.date = t6.date
		AND t0.locale = t6.locale
		AND (CASE WHEN t0.polygon = 'no-polygon' THEN 'Other'::text ELSE t0.district::text END) = t6.district
		AND t0.polygon = t6.polygon

	LEFT JOIN bi.etl_seo_costsperlocale t7
		ON t0.date = t7.date
		AND t0.locale = t7.locale
		AND (CASE WHEN t0.polygon = 'no-polygon' THEN 'Other'::text ELSE t0.district::text END) = t7.district
		AND t0.polygon = t7.polygon



	GROUP BY 
		t0.date,
		t0.locale,
		CASE WHEN t0.polygon = 'no-polygon' THEN 'Other'::text ELSE t0.district::text END,
		t0.polygon

	ORDER BY
		t0.date desc,
		t0.locale asc,
		district asc,
		t0.polygon asc

;