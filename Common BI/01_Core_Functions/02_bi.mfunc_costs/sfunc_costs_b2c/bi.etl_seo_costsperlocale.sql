
DROP TABLE IF EXISTS bi.etl_seo_costsperlocale;
CREATE TABLE bi.etl_seo_costsperlocale AS

	SELECT
		date::date,
		LEFT(t1.polygon,2) as locale,
		'Other'::text as district,
		'No-polygon'::text as polygon,
		SUM(CASE WHEN t1.daily_costs_eur IS NULL THEN 0 ELSE t1.daily_costs_eur END) as cost

	FROM generate_series(
		'2016-07-01'::date,
		current_date::date,
		'1 day'::interval
	) date

	LEFT JOIN external_data.zapier_seocosts t1
			ON date >= t1.start_date
			AND date <= t1.end_date
			AND t1.manual_validation = 'Yes'
			AND t1.formula_validation = 'Yes'

	GROUP BY date::date, LEFT(t1.polygon,2), district, polygon 
	ORDER BY date::date desc, polygon asc, locale asc

;