
DROP TABLE IF EXISTS bi.b2b_leadgencosts;
CREATE TABLE bi.b2b_leadgencosts AS

		SELECT 
		t3.date,
		t3.locale,
		t3.cost as sem_cost,
		SUM(CASE WHEN t4.leadgen_cost IS NULL THEN 0 ELSE t4.leadgen_cost END) as leadgentools_cost

		FROM

				(SELECT
					t1.date,
					CASE 
						WHEN t1.account::text LIKE 'Bookatiger Business DE%' THEN 'de'::text 
						WHEN t1.account::text LIKE 'Bookatiger Business CH%' THEN 'ch'::text
						WHEN t1.account::text LIKE 'Bookatiger Business NL%' THEN 'nl'::text
						WHEN t1.account::text LIKE 'Bookatiger Business AT%' THEN 'at'::text
						ELSE '-'::text
					END	as locale,
					SUM(CASE WHEN t1.account LIKE '%Bookatiger CH%' THEN t1.cost::numeric*0.92/1000000 ELSE t1.cost::numeric/1000000 END) as cost
				FROM 

					external_data.adwords_city t1

					-- bi.adwords_city_temp t1

				WHERE LEFT(t1.campaign, 3) = 'B2B'
				GROUP BY t1.date, 		
					CASE 
						WHEN t1.account::text LIKE 'Bookatiger Business DE%' THEN 'de'::text 
						WHEN t1.account::text LIKE 'Bookatiger Business CH%' THEN 'ch'::text
						WHEN t1.account::text LIKE 'Bookatiger Business NL%' THEN 'nl'::text
						WHEN t1.account::text LIKE 'Bookatiger Business AT%' THEN 'at'::text
						ELSE '-'::text
					END
				ORDER BY t1.date desc, locale asc) as t3

			LEFT JOIN

				(SELECT 
					date::date,
					'de'::text as locale,
					SUM(CASE WHEN t1.daily_costs_eur IS NULL THEN 0 ELSE t1.daily_costs_eur END) as leadgen_cost

				FROM generate_series(
				  '2017-01-01'::date,
				  current_date::date,
				  '1 day'::interval
				) date

				LEFT JOIN external_data.zapier_b2bcosts t1
					ON date >= t1.start_date
					AND date <= t1.end_date
					AND t1.manual_validation = 'Yes'
					AND t1.formula_validation = 'Yes'

				GROUP BY date::date, 
					locale

				ORDER BY date::date desc) as t4

				ON t3.date = t4.date AND t3.locale = t4.locale

		GROUP BY t3.date,
			t3.locale,
			t3.cost

		ORDER BY date desc, locale asc

;