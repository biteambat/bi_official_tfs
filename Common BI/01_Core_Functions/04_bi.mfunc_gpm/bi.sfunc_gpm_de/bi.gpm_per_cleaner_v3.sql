
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3;
CREATE TABLE bi.gpm_per_cleaner_v3 as 
select
    year_month,
    MIN(mindate) as mindate,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    -- min(mindate) as mindate,
    holiday,
    absence_hours,
    working_hours,
	total_hours,
	salary_payed,
	SUM(b2c_revenue) as b2c_revenue,
	SUM(b2b_revenue) as b2b_revenue,
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-salary_payed)
	ELSE b2b_revenue-(salary_payed*b2b_share) END) as b2b_gp,
		SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-salary_payed)
	ELSE b2c_revenue-(salary_payed*b2c_share) END) as b2c_gp,
	gp,
	GMV,
	Revenue
from
     bi.gpm_per_cleaner_v3_temp
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    total_hours,
    absence_hours,
    salary_payed,
    gp,
    gmv,
    revenue,
    days_worked,
    worked_hours;