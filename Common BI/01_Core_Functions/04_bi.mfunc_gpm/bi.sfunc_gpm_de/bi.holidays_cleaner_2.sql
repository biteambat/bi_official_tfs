
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner as a,
	(select i::date as date from generate_series('2016-01-01', 
  '2018-12-12', '1 day'::interval) i) as b
WHERE
	status__c in ('approved');

------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and status__c in ('approved');