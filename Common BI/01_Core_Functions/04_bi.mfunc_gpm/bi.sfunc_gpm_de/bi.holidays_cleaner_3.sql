
DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN t1.type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN t1.type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM 
	bi.holidays_cleaner_2 t1
JOIN
	Salesforce.Account t2
ON
	(t1.account__c = t2.sfid and (t1.date < t2.hr_contract_end__c OR t2.hr_contract_end__c IS NULL))
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;

---------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
	and date < current_date
GROUP BY
	date,
	account__c;	