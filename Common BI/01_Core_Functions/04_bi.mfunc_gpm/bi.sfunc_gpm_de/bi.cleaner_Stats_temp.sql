
DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT

	a.sfid as professional__c,
	-- a.delivery_areas__c as delivery_areas,
	CASE WHEN a.delivery_areas__c IS NULL THEN CASE WHEN a.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN a.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN a.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE a.delivery_areas__c
											END as delivery_areas,
	a.type__c,
	a.hr_contract_start__c::date as contract_start,
	a.hr_contract_end__c::date as contract_end,
	
	(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) as weekly_hours
	
FROM

   Salesforce.Account a
   
JOIN

   Salesforce.Account t2
   
ON

   (t2.sfid = a.parentid)

WHERE 

	a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%' and a.name not like '%Test%' and a.name not like '%TEST%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
	and  a.test__c = '0'
	and left(a.locale__c,2) = 'de'
	
GROUP BY

	a.sfid,
	a.type__c,
	a.delivery_areas__c,
	a.hr_contract_start__c,
	a.hr_contract_end__c,
	a.hr_contract_weekly_hours_min__c,
	a.locale__c;