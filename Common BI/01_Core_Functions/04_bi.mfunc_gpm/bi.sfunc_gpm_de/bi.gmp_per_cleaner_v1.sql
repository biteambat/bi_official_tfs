
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours
FROM
	bi.cleaner_Stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);