
DROP TABLE IF EXISTS bi.holidays_per_cleaner;
CREATE TABLE bi.holidays_per_cleaner as 
SELECT
	t1.*,
	t2.delivery_areas__c as delivery_area,
	availabilie_cleaner_days
FROM
	bi.holidays_cleaner_3 t1
LEFT JOIN  
	Salesforce.Account t2
ON
	(t1.account__c = t2.sfid )
LEFT JOIN
	(SELECT
	delivery_areas__c,
	SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as availabilie_cleaner_days
FROM(
SELECT
	a.sfid,
	a.delivery_areas__c,
	SUM(CASE WHEN a.availability_monday__c is not null THEN 1 ELSE 0 END) as availability_monday,
	SUM(CASE WHEN a.availability_tuesday__c is not null THEN 1 ELSE 0 END) as availability_tuesday,
	SUM(CASE WHEN a.availability_wednesday__c is not null THEN 1 ELSE 0 END) as availability_wednesday,
	SUM(CASE WHEN a.availability_thursday__c is not null THEN 1 ELSE 0 END) as availability_thursday,
	SUM(CASE WHEN a.availability_friday__c is not null THEN 1 ELSE 0 END) as availability_friday,
	SUM(CASE WHEN a.availability_saturday__c is not null THEN 1 ELSE 0 END) as availability_saturday
FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
	and left(a.locale__c,2) in ('de','nl', 'ch')
	and ((current_date::date between a.hr_contract_start__c and a.hr_contract_end__c) AND (left(a.locale__c,2) in ('de','nl')) 
		OR ((current_date::date between a.hr_contract_start__c AND current_date) AND (left(a.locale__c,2) in ('ch'))))
GROUP BY
	a.sfid,
	a.delivery_areas__c) as a
GROUP BY
	delivery_areas__c) as b
ON
	(t2.delivery_areas__c = b.delivery_areas__c);