
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
		GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
		case
		when year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
		when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
		else calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
		END AS days_worked
from (
SELECT	
   to_char(date,'YYYY-MM') as Year_Month,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
	contract_start,
	contract_end,
	type__c,
	SUM(hours) as worked_hours,
	cast(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
	SUM(GMV) as GMV,
	SUM(GMV/1.19) as total_revenue,
	SUM(B2C_GMV/1.19) as B2C_Revenue,
	SUM(B2B_GMV/1.19) as B2B_Revenue,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2b_share,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2c_share,
	MAX(Weekly_hours) as weekly_hours,
	MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))/7 )as monthly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Month,
	professional__c,
	contract_start,
	days_of_month,
	type__c,
	contract_end,
	delivery_area,
	calc_end
	) xx;

