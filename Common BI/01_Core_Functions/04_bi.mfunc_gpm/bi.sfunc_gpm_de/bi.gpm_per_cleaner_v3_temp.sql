
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp as 
select
    year_month,
    MIN(mindate) as mindate,
    professional__c,
    delivery_area,
    worked_hours,
    type__c,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    -- min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as absence_hours,
    (monthly_hours/days_of_month)*days_worked as working_hours,
	CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours) ELSE ((monthly_hours/days_of_month)*days_worked) END as total_hours,


	CASE WHEN mindate < '2017-01-01' THEN

			CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.25 ELSE ((monthly_hours/days_of_month)*days_worked)*12.25 END 

		WHEN (mindate >= '2017-01-01' AND mindate < '2018-01-01') THEN

			CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.5 ELSE ((monthly_hours/days_of_month)*days_worked)*12.5 END 

		ELSE 

			CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.9 ELSE ((monthly_hours/days_of_month)*days_worked)*12.9 END 

		END

		as salary_payed,


	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV/1.19) as Revenue,
	SUM(GMV/1.19) - (CASE WHEN mindate < '2017-01-01' THEN

			CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.25 ELSE ((monthly_hours/days_of_month)*days_worked)*12.25 END 

		WHEN (mindate >= '2017-01-01' AND mindate < '2018-01-01') THEN

			CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.5 ELSE ((monthly_hours/days_of_month)*days_worked)*12.5 END 

		ELSE 

			CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*12.9 ELSE ((monthly_hours/days_of_month)*days_worked)*12.9 END 

		END)


	  as gp


from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > '0'
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    days_worked,
    worked_hours;