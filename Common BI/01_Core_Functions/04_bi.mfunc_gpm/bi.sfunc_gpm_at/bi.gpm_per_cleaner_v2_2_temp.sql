
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_2_temp as 

SELECT

	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
	
FROM

	bi.gpm_per_cleaner_v2_temp t1
	
LEFT JOIN

	 bi.holidays_cleaner_4_temp t2
	 
ON

	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month)
	
ORDER BY

	t1.year_month,
	t1.name;