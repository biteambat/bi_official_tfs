
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
	MIN(mindate) as mindate,
   professional__c,
   delivery_area,
   worked_hours,
   contract_start,
   contract_end,
   0 as monthly_hours,
   0 as weekly_hours,
   days_worked,
   days_of_month,
   0 as sickness,
   -- MIN(mindate) as mindate,
   0 as holiday,
   0 as absence_hours,
   0 as working_hours,
   0 as total_hours,
	(SUM(B2C_Revenue) + SUM(B2B_Revenue))-0.2*(SUM(B2C_Revenue) + SUM(B2B_Revenue)) as salary_payed,
	
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	
	SUM(margin_b2b) as b2b_gp,
   SUM(margin_b2c) as b2c_gp,
		  
	0.2*(SUM(B2C_Revenue) + SUM(B2B_Revenue))  as gp,
	-- SUM(margin_b2b) + SUM(margin_b2c) as new_gp,
	SUM(GMV) as GMV,
	(SUM(B2C_Revenue) + SUM(B2B_Revenue)) as Revenue
	
	-- CASE WHEN (SUM(B2C_Revenue) + SUM(B2B_Revenue)) > 0 THEN  0.2*(SUM(B2C_Revenue) + SUM(B2B_Revenue))/(SUM(B2C_Revenue) + SUM(B2B_Revenue)) ELSE 0 END as gpm,
	-- CASE WHEN (SUM(B2C_Revenue) + SUM(B2B_Revenue)) > 0 THEN SUM(margin_b2b) + SUM(margin_b2c)/(SUM(B2C_Revenue) + SUM(B2B_Revenue)) ELSE 0 END as new_gpm
	
	
FROM

     bi.gpm_per_cleaner_v2_2_temp
     
WHERE

    LEFT(delivery_area,2) = 'at'
    AND days_worked > '0'
    
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    days_worked,
    worked_hours;