
-- GPM PER CLEANER TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
CREATE TABLE bi.gmp_per_cleaner_temp as 

SELECT

	t1.professional__c,
	t1.pph__c,
	-- t1.polygon as delivery_area__c,
	CASE WHEN t1.polygon IS NULL THEN CASE WHEN t1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN t1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN t1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE t1.polygon
											END as delivery_areas__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN gmv_eur
	         WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date
					  THEN CASE WHEN t1.pph__c < 13
					  				THEN gmv_eur*1.12
					  				ELSE gmv_eur*1.2
					  				END
				ELSE 0 END) as GMV,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN gmv_eur ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date
					  THEN CASE WHEN t1.pph__c < 13
					  				THEN gmv_eur*1.12
					  				ELSE gmv_eur*1.2
					  				END
				ELSE 0 END) as b2b_gmv,
				
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN order_Duration__c ELSE 0 END) + SUM(CASE WHEN (type = 'cleaning-b2b') AND Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2c_hours,
	SUM(CASE WHEN (type = 'cleaning-b2b') AND Status IN  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2b_hours,
	
	CASE WHEN t1.pph__c < 13 
		THEN 		
		((SUM(CASE 
	         WHEN (Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date) THEN gmv_eur*1.12 
				ELSE 0 END))/1.12)*0.2	
		ELSE 
		((SUM(CASE
	         WHEN (Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date) THEN gmv_eur*1.2
				ELSE 0 END))/1.2)*0.2
		END 
		as margin_b2b,			
		
CASE WHEN t1.pph__c < 13 
		THEN 
		((SUM(CASE 
				WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN gmv_eur
				ELSE 0 END))/1.12)*0.2
		ELSE 
		((SUM(CASE 
				WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN gmv_eur
				ELSE 0 END))/1.2)*0.2
		END 
		as margin_b2c
		
FROM

	bi.orders t1
	
WHERE

	t1.professional__c IS NOT NULL
	AND LEFT(t1.polygon,2) IN ('at')
	
GROUP BY

	t1.professional__c,
	t1.pph__c,
	t1.polygon,
	date,
	locale__c;