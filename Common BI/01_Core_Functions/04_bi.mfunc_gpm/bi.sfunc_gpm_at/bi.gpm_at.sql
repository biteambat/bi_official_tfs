
DROP TABLE IF EXISTS bi.gpm_at;
CREATE TABLE bi.gpm_at as

SELECT

	t2.name,
	t2.rating__c as rating,
	t5.Score_cleaners,
	CASE WHEN t2.hr_contract_start__c >= (current_date - 10) THEN 'New' ELSE 'Old' END as flag_new,
	t5.category,
	t5.existence,
	t2.type__c,
	t1.*,
	CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
	0 as contract_morning_hours,
	t3.Working_Hours_Morning,
	availability_Share,
	0 as morning_ur,
	0 as contract_hours_afternoon,
	t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
	0 as afternoon_ur,
	0 as capped_hours_morning,
	0 as capped_hours_afternoon

FROM

	bi.gpm_per_cleaner_v3_temp_temp t1
	
LEFT JOIN
	Salesforce.Account t2	
ON
	(t1.professional__c = t2.sfid)
	
LEFT JOIN
	bi.order_distribution_temp t3	
ON
	(t1.professional__c = t3.professional__c and t1.year_month = t3.month)
	
LEFT JOIN
	bi.cleaner_ur_temp_temp t4	
ON
	(t1.professional__c = t4.sfid)
	
LEFT JOIN
	bi.Performance_cleaners t5	
ON
	(t1.professional__c = t5.sfid);

	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;