
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
CREATE TABLE bi.holidays_cleaner_3_temp as 

SELECT

	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
	
FROM

	bi.holidays_cleaner_2_temp
	
WHERE

	weekday != '0'
	AND date < current_date
	
GROUP BY

	date,
	account__c;	