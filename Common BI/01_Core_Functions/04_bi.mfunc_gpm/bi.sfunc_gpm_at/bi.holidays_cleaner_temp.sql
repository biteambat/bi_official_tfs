
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
CREATE TABLE bi.holidays_cleaner_temp as

SELECT

	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
	
FROM

	salesforce.hr__c
	
WHERE

	type__c != 'unpaid';