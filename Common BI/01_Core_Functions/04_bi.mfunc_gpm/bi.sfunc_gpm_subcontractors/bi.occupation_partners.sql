
DROP TABLE IF EXISTS bi.occupation_partners;
CREATE TABLE bi.occupation_partners as


SELECT

	table3.year_month,
	table3.effectivedate,
	table3.polygon,
	table3.active_opp,
	table3.name_opp,
	SUM(table3.hours) as hours_on_opp,
	table3.partner as company,
	table3.number_employees_tot,
	-- table3.hourly_cost_sub,
	
	CASE WHEN table3.effectivedate >= '2018-01-01' THEN table3.hourly_cost_sub
		
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%DADI%') THEN 17.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Crystal%') THEN 17
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%PureX%') THEN 19.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Sunny%') THEN 18.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%facilitas%') THEN 17.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE table3.hourly_cost_sub
			  
			  END as hourly_cost_sub,
			  
	table3.cleaner,
	SUM(table4.hours_occupied) as hours_occupied_partner,
	MIN(table3.number_employees_tot*3*4.3*5) as total_capacity_partner
	
FROM

	(SELECT
	
		table1.*,
		table2.name as name_opp
		
	FROM
	
		(SELECT
		
			opps.*,
			CASE WHEN partners.company_name__c IS NULL THEN 'BAT' ELSE partners.company_name__c END as partner,
			partners.name_cleaner as cleaner,
			partners.number_employees_tot,
			partners.hourly_cost_sub
		
		FROM
		
			(SELECT
						
				TO_CHAR(oo.effectivedate,'YYYY-MM') as year_month,
				oo.effectivedate,
				-- CONCAT('WK', (EXTRACT(WEEK FROM oo.effectivedate)::text)) as week,
				CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
				oo.contact__c as active_opp,
				oo.professional__c as cleaner_assigned,
				SUM(oo.order_duration__c) as hours
				
			FROM
			
				bi.orders oo
			
			WHERE
			
				oo.type = 'cleaning-b2b'
				AND oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
				AND oo.effectivedate > '2016-01-01'
				AND oo.test__c = FALSE
				
			GROUP BY
			
				year_month,
				effectivedate,
				oo.contact__c,
				oo.professional__c,
				oo.polygon,
				oo.locale__c
				
			ORDER BY
			
				year_month desc,
				oo.polygon) as opps
				
		LEFT JOIN
		
			(SELECT
	
				a.name as name_partner,
				a.status__c,
				a.type__c,
				a.hr_contract_start__c as contract_start,
				a.hr_contract_end__c as contract_end,
				a.sfid as sfid_partner,
				a.parentid as parentid_company,
				a.company_name__c,
				CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
																WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
																WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
																ELSE 'at-other'
																END
														ELSE b.delivery_areas__c
														END as polygon,
				a.numberofemployees as number_employees_tot,
				a.pph__c as hourly_cost_sub,
				b.parentid as parentid_cleaner,
				b.sfid as sfid_cleaner,
				b.name as name_cleaner
				
			FROM
			
				salesforce.account a
				
			LEFT JOIN
			
				salesforce.account b
				
			ON 
			
				a.sfid = b.parentid
				
			WHERE
			
				-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
				a.type__c = 'partner'
				AND a.type__c NOT LIKE 'master'
				AND a.company_name__c NOT LIKE '%BAT%'
				AND a.company_name__c IS NOT NULL
				AND a.company_name__c NOT LIKE ''
				AND LOWER(a.name) not like '%test%'
				AND LOWER(a.company_name__c) not like '%test%'
				AND a.test__c IS FALSE
				) as partners
				
				
		ON 
		
			opps.cleaner_assigned = partners.sfid_cleaner) as table1	
			
	LEFT JOIN
	
		salesforce.contact table2
		
	ON
	
		table1.active_opp = table2.sfid) as table3
		
LEFT JOIN

	(SELECT
	
		occupation_partners.year_month,
		occupation_partners.effectivedate,
		occupation_partners.partner,
		SUM(occupation_partners.hours) as hours_occupied
	
	FROM
		
		(SELECT
		
			table1.*,
			table2.name as name_opp
			
		FROM
		
			(SELECT
			
				opps.*,
				CASE WHEN partners.company_name__c IS NULL THEN 'BAT' ELSE partners.company_name__c END as partner,
				partners.name_cleaner as cleaner,
				partners.number_employees_tot,
				partners.hourly_cost_sub
			
			FROM
			
				(SELECT
							
					TO_CHAR(oo.effectivedate,'YYYY-MM') as year_month,
					oo.effectivedate,
					-- CONCAT('WK', (EXTRACT(WEEK FROM oo.effectivedate)::text)) as week,
					CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
					oo.contact__c as active_opp,
					oo.professional__c as cleaner_assigned,
					SUM(oo.order_duration__c) as hours
					
				FROM
				
					bi.orders oo
				
				WHERE
				
					oo.type = 'cleaning-b2b'
					AND oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
					AND oo.effectivedate > '2016-01-01'
					AND oo.test__c = FALSE
					
				GROUP BY
				
					year_month,
					effectivedate,
					oo.contact__c,
					oo.professional__c,
					oo.polygon,
					oo.locale__c
					
				ORDER BY
				
					year_month desc,
					oo.polygon) as opps
					
			LEFT JOIN
			
				(SELECT
	
					a.name as name_partner,
					a.status__c,
					a.type__c,
					a.hr_contract_start__c as contract_start,
					a.hr_contract_end__c as contract_end,
					a.sfid as sfid_partner,
					a.parentid as parentid_company,
					a.company_name__c,
					CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
																	WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
																	WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
																	ELSE 'at-other'
																	END
															ELSE b.delivery_areas__c
															END as polygon,
					a.numberofemployees as number_employees_tot,
					a.pph__c as hourly_cost_sub,
					b.parentid as parentid_cleaner,
					b.sfid as sfid_cleaner,
					b.name as name_cleaner
					
				FROM
				
					salesforce.account a
					
				LEFT JOIN
				
					salesforce.account b
					
				ON 
				
					a.sfid = b.parentid
					
				WHERE
				
					-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
					a.type__c = 'partner'
					AND a.type__c NOT LIKE 'master'
					AND a.company_name__c NOT LIKE '%BAT%'
					AND a.company_name__c IS NOT NULL
					AND a.company_name__c NOT LIKE ''
					AND LOWER(a.name) not like '%test%'
					AND LOWER(a.company_name__c) not like '%test%'
					AND a.test__c IS FALSE
					) as partners
					
					
			ON 
			
				opps.cleaner_assigned = partners.sfid_cleaner) as table1	
				
		LEFT JOIN
		
			salesforce.contact table2
			
		ON
		
			table1.active_opp = table2.sfid) occupation_partners
			
	GROUP BY
	
		occupation_partners.year_month,
		occupation_partners.effectivedate,
		occupation_partners.partner
		
	ORDER BY
	
		year_month desc,
		partner ) as table4
		
ON 

	table3.year_month = table4.year_month
	AND table3.partner = table4.partner
		
GROUP BY

	table3.year_month,
	table3.effectivedate,
	table3.polygon,
	table3.active_opp,
	table3.name_opp,		
	table3.partner,
	table3.number_employees_tot,
	table3.hourly_cost_sub,
	table3.cleaner
	
ORDER BY 

	year_month desc,
	polygon;