
DROP TABLE IF EXISTS bi.balance_opps;
CREATE TABLE bi.balance_opps AS

SELECT

	LEFT(t2.locale__c, 2) as locale,
	t1.createddate,
	t1.opportunityid,
	'Churned'::text as kpi,
	(- COUNT (DISTINCT t1.opportunityid))::integer as count,
	(- SUM(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END))::numeric as total

FROM

	(SELECT

		MAX(oo.createddate) as createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
			AND LEFT(o.polygon, 2) LIKE 'de') t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.createddate > '2016-12-31'
		AND oo.newvalue IN ('RESIGNED', 'CANCELLED')
		
	GROUP BY 
		
		oo.opportunityid) t1
	
LEFT JOIN

	salesforce.opportunity t2

	
ON 

	t1.opportunityid = t2.sfid
	
WHERE

	-- LEFT(t2.locale__C, 2) = 'de'
	t2.name NOT LIKE 'test%'
	
GROUP BY 
	
	locale,
	t1.createddate,
	t1.opportunityid;
	
	
INSERT INTO bi.balance_opps	

SELECT

	LEFT(t2.locale__c, 2) as locale,
	t1.createddate,
	t1.opportunityid,
	'Signed'::text as kpi,
	COUNT (DISTINCT t1.opportunityid),
	SUM(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END) as total

FROM

	(SELECT

		MAX(oo.createddate) as createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
			AND LEFT(o.polygon, 2) LIKE 'de') t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.createddate > '2016-12-31'
		AND oo.newvalue IN ('PENDING')
		
	GROUP BY 
		
		oo.opportunityid) t1
	
LEFT JOIN

	salesforce.opportunity t2

	
ON 

	t1.opportunityid = t2.sfid
	
WHERE

	-- LEFT(t2.locale__C, 2) = 'de'
	t2.name NOT LIKE 'test%'
	
GROUP BY 
	
	locale,
	t1.createddate,
	t1.opportunityid;