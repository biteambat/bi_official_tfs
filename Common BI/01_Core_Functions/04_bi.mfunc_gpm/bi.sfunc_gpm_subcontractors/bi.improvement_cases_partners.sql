
DROP TABLE IF EXISTS bi.improvement_cases_partners;
CREATE TABLE bi.improvement_cases_partners AS

SELECT

	EXTRACT(YEAR FROM c.createddate) as year,
	EXTRACT(MONTH FROM c.createddate) as month,
	c.createddate,
	t2.company_name__c,
	
	COUNT(DISTINCT c.casenumber)

FROM

	(SELECT

	t0.*,
	a.parentid

FROM

	(SELECT
	
		s.company_name__c,
		s.name_account,
		t1.cleaners as sfid_cleaner
	
	FROM
	
		(SELECT
		
			DISTINCT(sfid_professional) as cleaners
			
		FROM
		
			bi.gpm_subcontractor) as t1
		
			LEFT JOIN
			
				bi.gpm_subcontractor s
				
			ON 
			
				t1.cleaners = s.sfid_professional) as t0
				
		LEFT JOIN
		
			salesforce.account a 
			
		ON
		
			t0.sfid_cleaner = a.sfid
			
		WHERE
		
			t0.company_name__c IS NOT NULL
			
		GROUP BY
		
			t0.company_name__c,
			t0.name_account,
			t0.sfid_cleaner,
			parentid) as t2
			
LEFT JOIN

	salesforce.case c
	
ON

	t2.parentid = c.accountid
	
WHERE

	c.reason = 'Professional - Improvement'
	
GROUP BY
	
	year,
	month,
	createddate,
	t2.company_name__c,
	t2.name_account;