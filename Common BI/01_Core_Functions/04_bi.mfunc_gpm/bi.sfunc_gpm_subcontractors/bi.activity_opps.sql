
DROP TABLE IF EXISTS bi.activity_opps;
CREATE TABLE bi.activity_opps as 

SELECT

	t2.year_month,
	min(t2.effectivedate) as effectivedate,
	t2.polygon,
	COUNT(DISTINCT t2.number_active_opps) as number_active_opps,
	SUM(t2.monthly_hours) as monthly_hours,
	MAX(t2.number_partners) as number_partners,
	MAX(t2.number_active_partners) as number_active_partners,
	MAX(t3.total_prof) as all_professionals,
	MAX(t3.partners_prof) as professionals_partners,
	MAX(t3.total_hours) as total_hours,
	MAX(t3.hours_partners) as hours_partners

FROM

	(SELECT
	
		t1.year_month,
		t1.effectivedate,
		-- t1.week,
		t1.polygon,
		t1.number_active_opps,
		t1.monthly_hours,
		t1.number_partners,
		(CASE WHEN (active_partners.number_active_partners IS NULL) THEN 0 ELSE active_partners.number_active_partners END) as number_active_partners
	
	FROM
	
		(SELECT
		
			number_opps.year_month,
			number_opps.effectivedate,
			number_opps.polygon,
			number_opps.number_active_opps,
			number_opps.monthly_hours,
			(CASE WHEN (number_partners.number_partners IS NULL) THEN 0 ELSE number_partners.number_partners END) as number_partners
		
		FROM
		
			(SELECT
			
				TO_CHAR(oo.effectivedate,'YYYY-MM') as year_month,
				oo.effectivedate,
				-- CONCAT('WK', (EXTRACT(WEEK FROM oo.effectivedate)::text)) as week,
				CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
				oo.contact__c as number_active_opps,
				-- COUNT(DISTINCT oo.contact__c) as number_active_opps,
				SUM(oo.order_duration__c) as monthly_hours
				
			FROM
			
				bi.orders oo
			
			WHERE
			
				oo.type = 'cleaning-b2b'
				AND oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
				AND oo.effectivedate > '2016-01-01'
				AND oo.test__c = FALSE
				
			GROUP BY
			
				year_month,
				oo.effectivedate,
				oo.polygon,
				oo.locale__c,
				oo.contact__c
				
			ORDER BY
			
				year_month desc,
				oo.polygon) as number_opps
				
		LEFT JOIN 
		
			(SELECT
		
				year_month,
				polygon,
				COUNT(DISTINCT company_name) as number_partners
				
			FROM
			
				bi.employees_partners
				
			WHERE
			
				activity = 'Active'
				
			GROUP BY
			
				year_month,
				polygon
				
			ORDER BY
			
				year_month desc,
				polygon desc) as number_partners		
		ON 
		
			number_opps.year_month = number_partners.year_month
			AND number_opps.polygon = number_partners.polygon) as t1
			
	LEFT JOIN
	
		(SELECT
	
			TO_CHAR(date_orders,'YYYY-MM') as year_month,
			polygon,
			COUNT(DISTINCT company_name__c) as number_active_partners
			
		FROM
		
			bi.orders_subcontractors
			
		WHERE
		
			date_orders IS NOT NULL
			
		GROUP BY
		
			year_month,
			polygon
			
		ORDER BY
		
			year_month desc,
			polygon desc) as active_partners
	
	ON
	
		t1.year_month = active_partners.year_month
		AND t1.polygon = active_partners.polygon) as t2
		
LEFT JOIN

	(SELECT
	
		TO_CHAR(t4.effectivedate,'YYYY-MM') as year_month,
		-- CONCAT('WK', (EXTRACT(WEEK FROM t4.effectivedate)::text)) as week,
		t4.polygon,
		COUNT(DISTINCT t4.all_prof) as total_prof,
		COUNT(DISTINCT t4.partners_prof) as partners_prof,
		SUM(t4.order_duration__c) as total_hours,
		SUM(CASE WHEN (t4.partners_prof IS NOT NULL) THEN t4.order_duration__c ELSE 0 END) as hours_partners
	
	FROM
	
		(SELECT
		
			o.effectivedate,
			CASE WHEN o.polygon IS NULL THEN CASE WHEN o.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN o.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN o.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE o.polygon
											END
											as polygon,
			o.sfid,
			o.professional__c as all_prof,
			t3.sfid_cleaner as partners_prof,
			o.order_duration__c
		
		FROM
		
			bi.orders o
			
		LEFT JOIN
		
			(SELECT	
					
					oo.name,
					a.sfid as sfid_cleaner,
					oo.name_company,
					oo.parentid,
					oo.start_partner,
					oo.end_partner	
					
				FROM
				
				
					(SELECT
							
						t2.name,
						t2.sfid,
						t2.company_name__c as name_company,
						t1.parentid,
						t2.hr_contract_start__c as start_partner,
						t2.hr_contract_end__c as end_partner
						
					FROM
					
						Salesforce.Account t1
						
					JOIN
					
						Salesforce.Account t2
						
					ON
					
						(t2.sfid = t1.parentid)
						
					WHERE
					
						(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
						
					GROUP BY
					
						t2.name,
						t2.sfid,
						t2.company_name__c,
						t1.parentid,
						t2.hr_contract_start__c,
						t2.hr_contract_end__c) as oo
						
				LEFT JOIN
				
					salesforce.account a
					
				ON 
				
					oo.sfid = a.parentid) as t3
		
		ON 
		
			o.professional__c = t3.sfid_cleaner
					
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c = FALSE
			AND o.effectivedate > '2016-01-01'
			AND o.effectivedate < current_date + 60
			AND o."status" IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
			
		ORDER BY
		
			o.effectivedate desc) as t4
			
	-- WHERE
	
		-- partners_prof IS NOT NULL
			
	GROUP BY
	
		year_month,
		t4.polygon
		
	ORDER BY
	
		year_month desc,
		t4.polygon desc)as t3	
		
ON 

	t2.year_month = t3.year_month
	AND t2.polygon = t3.polygon

GROUP BY

	t2.year_month,
	t2.polygon
	
ORDER BY

	t2.year_month desc,
	t2.polygon desc;