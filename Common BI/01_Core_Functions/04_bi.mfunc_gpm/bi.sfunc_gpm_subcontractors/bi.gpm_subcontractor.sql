
DROP TABLE IF EXISTS bi.gpm_subcontractor;
CREATE TABLE bi.gpm_subcontractor as

SELECT
			
	t1.company_name__c as company_name__c,	
	t1.name_cleaner as name_account,	
	TO_CHAR(o.effectivedate::date,'YYYY-MM') as year_month,
	o.professional__c as sfid_professional,
	
	CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.hourly_cost_sub
			  
			  END as pph__c,
	
	-- t1.hourly_cost_sub as pph__c,
	t1.polygon as delivery_area,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.order_duration__c END) as worked_hours,
	t1.type__c,
	t1.contract_start,
	t1.contract_end,
	1 as monthly_hours,
	1 as weekly_hours,
	1 as days_worked,
	1 as days_of_month,
	min(o.effectivedate)::date as mindate,
	1 as working_hours,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END) as revenue,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END) as gmv,
	0 as B2C_Revenue,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END) as B2B_Revenue,
	0 as b2c_share,
	1 as b2b_share,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																			  
																																			  ELSE t1.hourly_cost_sub
																																			  
																																			  END)*o.order_duration__c END) as gp,
   CASE WHEN (SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW%') THEN 0 ELSE o.eff_pph*o.order_duration__c END)) > 0 THEN (SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																																																		  
																																																																		  ELSE t1.hourly_cost_sub																																																																  
																																																																		  END)*o.order_duration__c END))/(SUM(CASE WHEN (o.status LIKE '%CANCELLED%') THEN 0 ELSE o.eff_pph*o.order_duration__c END)) ELSE 0 END as gpm_subcontractor
	
FROM

	(SELECT
			
			a.name as name_partner,
			a.status__c,
			a.type__c,
			a.hr_contract_start__c as contract_start,
			a.hr_contract_end__c as contract_end,
			a.sfid as sfid_partner,
			a.parentid as parentid_company,
			a.company_name__c,
			CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
															WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
															WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
															ELSE 'at-other'
															END
													ELSE b.delivery_areas__c
													END as polygon,
			a.numberofemployees as number_employees_tot,
			a.pph__c as hourly_cost_sub,
			b.parentid as parentid_cleaner,
			b.sfid as sfid_cleaner,
			b.name as name_cleaner
			
		FROM
		
			salesforce.account a
			
		LEFT JOIN
		
			salesforce.account b
			
		ON 
		
			a.sfid = b.parentid
			
		WHERE
		
			-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
			a.type__c = 'partner'
			AND a.type__c NOT LIKE 'master'
			AND a.company_name__c NOT LIKE '%BAT%'
			AND a.company_name__c IS NOT NULL
			AND a.company_name__c NOT LIKE ''
			AND LOWER(a.name) not like '%test%'
			AND LOWER(a.company_name__c) not like '%test%'
			AND a.test__c IS FALSE
		) as t1
		
LEFT JOIN

	bi.orders o
	
ON 

	t1.sfid_cleaner = o.professional__c
	
GROUP BY

	year_month,
	t1.name_partner,
    t1.status__c,
    t1.type__c,
	t1.sfid_partner,
	t1.contract_start,
	t1.contract_end,
	t1.parentid_company,
	t1.company_name__c,
	t1.number_employees_tot,
	t1.hourly_cost_sub,
	t1.parentid_cleaner,
	t1.sfid_cleaner,
	t1.name_cleaner,
	sfid_professional,
	t1.polygon,
	o.effectivedate
	
ORDER BY

	year_month;