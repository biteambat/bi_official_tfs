
DROP TABLE IF EXISTS bi.cleaner_geoloc_temp;
CREATE TABLE bi.cleaner_geoloc_temp as 

SELECT  

   t1.name,
   t1.sfid,
   t1.parentid,
   t1.new_areas,
   t1.pph__c,
   CASE WHEN t1.new_areas IS NOT NULL THEN t1.new_areas ELSE t2.key__c END as delivery_area,
   ST_Intersects( 
   ST_SetSRID(ST_Point(t1.longitude__c::float, t1.latitude__c::float), 4326), 
   ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
     
FROM
 
   bi.account_temp t1,
   salesforce.delivery_area__c t2
      
GROUP BY
 
   t1.name,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.new_areas,
	delivery_area,
	flag_polygon
     
HAVING
 
    ST_Intersects(
    ST_SetSRID(ST_Point(t1.longitude__c::float, t1.latitude__c::float), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true
    
UNION

	SELECT  

   t1.name,
   t1.sfid,
   t1.parentid,
   t1.new_areas,
   t1.pph__c,
   t1.new_areas,
 	TRUE as flag_polygon
     
FROM
 
   bi.account_temp t1
      
GROUP BY
 
   t1.name,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.new_areas,
	flag_polygon
     
HAVING
     	t1.New_areas IS NOT NULL;