
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
   professional__c,
   pph__c,
   delivery_area,
   worked_hours,
   type__c,
   contract_start,
   contract_end,
   monthly_hours,
   weekly_hours,
   days_worked,
   days_of_month,
   MIN(mindate) as mindate,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	worked_hours*pph__c as salary_payed,
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV) - SUM(worked_hours*pph__c)  as gp,
    CASE WHEN SUM(GMV) > 0 THEN (SUM(GMV) - SUM(worked_hours*pph__c))/SUM(GMV) ELSE 0 END as gpm_subcontractor

FROM

     bi.gpm_per_cleaner_v2_temp
     
WHERE

    LEFT(delivery_area,2) IN ('de', 'nl', 'ch')
    AND days_worked > '0'
    
GROUP BY
    year_month,
    professional__c,
    pph__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    days_worked,
    worked_hours;