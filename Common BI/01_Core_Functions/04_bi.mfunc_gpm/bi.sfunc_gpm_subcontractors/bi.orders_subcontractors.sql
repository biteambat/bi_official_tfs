
DROP TABLE IF EXISTS bi.orders_subcontractors;
CREATE TABLE bi.orders_subcontractors AS


SELECT

	*

FROM

	(SELECT
	
		t2.name,
		t2.company_name__c as name_company,
		t1.parentid,
		t2.hr_contract_start__c as start_partner,
		t2.hr_contract_end__c as end_partner
		
	FROM
	
		Salesforce.Account t1
		
	JOIN
	
		Salesforce.Account t2
		
	ON
	
		(t1.parentid = t2.sfid)
		
	WHERE
	
		(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
		
	GROUP BY
	
		t2.name,
		t2.company_name__c,
		t1.parentid,
		t2.hr_contract_start__c,
		t2.hr_contract_end__c) as table1
		
LEFT JOIN


	(SELECT

		-- This table gives us all the orders executed by the partners until today included
		-- It includes also the partners that are not working in collaboration with us
		-- We have the number of their employees, their cost/hour, the price and duration of the orders...
				
		o.effectivedate::date as date_orders,
		t1.name_partner,
		 t1.status__c,
		t1.sfid_partner,
		t1.parentid_company,
		t1.company_name__c,
		t1.number_employees_tot,
		t1.hourly_cost_sub,
		t1.parentid_cleaner,
		t1.sfid_cleaner,
		t1.name_cleaner,
		o.professional__c as sfid_professional,
		o.contact__c as contact_customer,
		o.sfid as order_reference,
		o.status as status_order,
		UPPER(LEFT(t1.polygon, 2)) as country,
		t1.polygon,
		o.pph__c as price_order,
		o.eff_pph,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.order_duration__c END as order_duration,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.eff_pph*o.order_duration__c END as revenue,
		
		CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.hourly_cost_sub
			  
			  END as cost_sub_per_hour,
		
		-- t1.hourly_cost_sub as cost_sub_per_hour,
		
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as cost_sub,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																					  
																																					  ELSE t1.hourly_cost_sub
																																					  
																																					  END)*o.order_duration__c END as gp,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE (CASE WHEN (o.eff_pph*o.order_duration__c) > 0 THEN (o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																																	  
																																																	  ELSE t1.hourly_cost_sub
																																																	  
																																																	  END)*o.order_duration__c)/(o.eff_pph*o.order_duration__c) ELSE 0 END) END as gpm

	FROM

		(SELECT
	
			a.name as name_partner,
			a.status__c,
			a.type__c,
			a.hr_contract_start__c as contract_start,
			a.hr_contract_end__c as contract_end,
			a.sfid as sfid_partner,
			a.parentid as parentid_company,
			a.company_name__c,
			CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
															WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
															WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
															ELSE 'at-other'
															END
													ELSE b.delivery_areas__c
													END as polygon,
			a.numberofemployees as number_employees_tot,
			a.pph__c as hourly_cost_sub,
			b.parentid as parentid_cleaner,
			b.sfid as sfid_cleaner,
			b.name as name_cleaner
			
		FROM
		
			salesforce.account a
			
		LEFT JOIN
		
			salesforce.account b
			
		ON 
		
			a.sfid = b.parentid
			
		WHERE
		
			-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
			a.type__c = 'partner'
			AND a.type__c NOT LIKE 'master'
			AND a.company_name__c NOT LIKE '%BAT%'
			AND a.company_name__c IS NOT NULL
			AND a.company_name__c NOT LIKE ''
			AND LOWER(a.name) not like '%test%'
			AND LOWER(a.company_name__c) not like '%test%'
			AND a.test__c IS FALSE
			) as t1
		
	LEFT JOIN
	
		bi.orders o
	
	ON 
	
		t1.sfid_cleaner = o.professional__c
	
	-- WHERE 
	
		-- o.effectivedate <= current_date
	
	ORDER BY
	
		o.effectivedate desc,
		name_partner) as table2
			
ON

	table1.name_company = table2.company_name__c;