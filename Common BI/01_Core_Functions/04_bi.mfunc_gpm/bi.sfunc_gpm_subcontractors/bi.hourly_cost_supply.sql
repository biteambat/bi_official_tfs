
DROP TABLE IF EXISTS bi.hourly_cost_supply;
CREATE TABLE bi.hourly_cost_supply AS

SELECT

	t1.year_month,
	t1.opportunity__c,
	t1.cost_supply,
	t2.hours,
	CASE WHEN t2.hours IS NULL THEN t1.cost_supply ELSE t1.cost_supply/t2.hours END as hourly_cost_supply

FROM

	(SELECT
	
		TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
		o.opportunity__c,
	
		
		SUM(o.total_amount__c) as cost_supply
	
	FROM
	
		salesforce.productlineitem__c o
		
	GROUP BY
	
		year_month,
		o.opportunity__c
		
	ORDER BY
	
		year_month desc) as t1	
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(oo.effectivedate, 'YYYY-MM') as year_month,
		oo.opportunityid,
	
		
		SUM(oo.order_duration__c) as hours
	
	FROM
	
		salesforce.order oo
		
	GROUP BY
	
		year_month,
		oo.opportunityid
		
	ORDER BY
	
		year_month desc) as t2
		
ON

	t1.opportunity__c = t2.opportunityid
	AND t1.year_month = t2.year_month
	
GROUP BY

	t1.year_month,
	t1.opportunity__c,
	t1.cost_supply,
	t2.hours
	
ORDER BY

	t1.year_month desc;