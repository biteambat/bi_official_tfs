
DROP TABLE IF EXISTS bi.employees_partners;
CREATE TABLE bi.employees_partners AS

SELECT

	t1.year_month,
	CONCAT(LEFT(t1.year_month,4)::text,'-',RIGHT(t1.year_month,2)::text,'-01')::date as date,
	t1.country,
	t1.polygon,
	t1.company_name__c as company_name,
	t1.number_employees,
	CASE WHEN t1.times_active > 0 THEN 'Active' ELSE 'Inactive' END as activity
	
FROM

	(SELECT
	
		TO_CHAR(o.date,'YYYY-MM') as year_month,
		-- CONCAT(LEFT(TO_CHAR(o.date,'YYYY-MM'), 4)::text,'-',RIGHT(TO_CHAR(o.date,'YYYY-MM'),2)::text,'-01')::date as date,
		-- EXTRACT(YEAR FROM o.date) as year,
		-- EXTRACT(MONTH FROM o.date) as month,
		-- MIN(o.date) as mindate,
		o.upper as country,
		o.polygon,
		o.company_name__c,
		MAX(o.number_employees_tot) number_employees,
		SUM(CASE WHEN o.status_at_time = 'Active' THEN 1 ELSE 0 END) as times_active
		
	FROM
	
		bi.activity_partners o
			
	GROUP BY
	
		-- year,
		-- month,
		year_month,
		-- date,
		o.upper,
		o.polygon,
		company_name__c
	
		
	ORDER BY
	
		year_month desc,
		-- date desc,
		company_name__c) as t1;