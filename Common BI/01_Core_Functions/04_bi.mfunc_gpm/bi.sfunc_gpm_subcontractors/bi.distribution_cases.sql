
DROP TABLE IF EXISTS bi.distribution_cases;
CREATE TABLE bi.distribution_cases AS

SELECT

	table5.*,
	table4.professional__c,
	table4.number_cases::text,
	table4.year,
	table4.month,
	table4.createddate,
	table4.parentid,
	table4.company_name,
	-- table4.order__c,
	table4.status,
	table4.reason,
	table4.ownerid,
	-- table1.sfid as sfid_case,
	table4.casenumber,
	table4.subject,
	-- table4.description,
	table4.contactid,
	table4.accountid

FROM

	(SELECT
	
		o.sfid as sfid_order,
		COUNT(DISTINCT casenumber) as number_cases,
		o.professional__c,
		table3.*
	
	
	FROM
	
		(SELECT
		
			EXTRACT(YEAR FROM table1.createddate) as year,
			EXTRACT(MONTH FROM table1.createddate) as month,
			table1.createddate,
			table2.parentid,
			CASE WHEN table2.parentid IS NULL THEN table2.name ELSE table2.company_name__c END as company_name,
			table1.order__c,
			table1.status,
			table1.reason,
			table1.ownerid,
			-- table1.sfid as sfid_case,
			table1.casenumber,
			table1.subject,
			table1.description,
			table1.contactid,
			table1.accountid
			
		
		FROM
		
		
			(SELECT
			
				*
				
			FROM
			
				salesforce.case c
				
			WHERE
			
				(c.type IN ('KA', 'TO', 'PM') OR (c.type = 'Pool' AND LOWER(c.origin) = 'partner%'))
				AND (c.reason LIKE '%Improvement%' OR c.reason LIKE '%improvement%')
				AND (c.ownerid LIKE '%00520000003IpXO%' OR c.ownerid LIKE '%00520000003cfbs%' OR c.ownerid LIKE '%00520000003bJ8E%' OR c.ownerid LIKE '%00520000005IuU4%' OR c.ownerid LIKE '%00520000003Ihko%'
				      OR c.ownerid LIKE '%0050J0000074nTT%' OR c.ownerid LIKE '%00520000003InJZ%')) as table1
			      
		LEFT JOIN
		
			(SELECT
		        t1.*,
		        t2.name as subcon
		      FROM
		       Salesforce.Account t1
		        JOIN
		            Salesforce.Account t2
		        ON
		            (t2.sfid = t1.parentid)
		    
		        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
		      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
		      and t2.name NOT LIKE '%BAT Business Services GmbH%') as table2
		      
		ON 
		
			table1.accountid = table2.parentid    
			
		GROUP BY
		
			year,
			month,
			table1.createddate,
			table2.parentid,
			company_name,
			table1.order__c,
			table1.status,
			table1.reason,
			table1.ownerid,
			table1.sfid,
			table1.casenumber,
			table1.subject,
			table1.description,
			table1.contactid,
			table1.accountid
		      
		ORDER BY
		
			year desc,
			month desc,
			table1.createddate,
			table2.parentid,
			company_name,
			table1.order__c,
			table1.status,
			table1.reason,
			table1.ownerid,
			table1.sfid,
			table1.casenumber,
			table1.subject,
			table1.description,
			table1.contactid,
			table1.accountid) as table3
			
	LEFT JOIN
	
		bi.orders o
		
	ON 
	
		table3.order__c = o.sfid
		
	GROUP BY
	
		o.sfid,
		o.professional__c,
		table3.year,
		table3.month,
		table3.createddate,
		table3.parentid,
		table3.company_name,
		table3.order__c,
		table3.status,
		table3.reason,
		table3.ownerid,
		-- table3.sfid,
		table3.casenumber,
		table3.subject,
		table3.description,
		table3.contactid,
		table3.accountid) as table4
		
LEFT JOIN

	(SELECT
	
		t1.*,
		a.name
	
	FROM
	
		(SELECT
		
			EXTRACT(YEAR FROM o.effectivedate) as year2,
			EXTRACT(MONTH FROM o.effectivedate) as month2,
			o.polygon,
			o.professional__c as cleaner_sfid,
			SUM(o.order_duration__c) as hours_performed,
			COUNT(DISTINCT o.sfid) as number_orders
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
			AND o.effectivedate < current_date
			AND o.type LIKE '%b2b%'
			
		GROUP BY
		
			year2,
			month2,
			o.polygon,
			cleaner_sfid
			
		ORDER BY
		
			year2 desc,
			month2 desc,
			o.polygon,
			cleaner_sfid) as t1
			
	LEFT JOIN
	
		(SELECT
		
	     t1.*,
	     t2.name as subcon
	     
	   FROM
	   
	    	Salesforce.Account t1
	    	
	   JOIN
	   
	      Salesforce.Account t2
	      
	   ON
	   
	      (t2.sfid = t1.parentid)
	 
	   WHERE 
			
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
			and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') as a
		
	ON
	
		t1.cleaner_sfid = a.sfid
	
	WHERE
	
		a.name IS NOT NULL
		
	
	ORDER BY
	
		year2 desc,
		month2 desc,
		t1.polygon,
		cleaner_sfid
		) as table5

ON

	table4.year = table5.year2
	AND table4.month = table5.month2
	AND table4.professional__c = table5.cleaner_sfid
	
WHERE

	table4.company_name IS NOT NULL
	
ORDER BY

	table4.year desc,
	table4.month desc;