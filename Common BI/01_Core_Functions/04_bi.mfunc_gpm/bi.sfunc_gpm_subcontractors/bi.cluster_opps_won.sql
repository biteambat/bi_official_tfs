
DROP TABLE IF EXISTS bi.cluster_opps_won;
CREATE Table bi.cluster_opps_won as 


SELECT

	table1.*,
	o.sfid,
	o.customer__c
	
FROM

	(SELECT
	
		o.first_order_date,
		EXTRACT(year FROM o.first_order_date) as year,
		EXTRACT(MONTH FROM o.first_order_date) as month,
		EXTRACT(WEEK FROM o.first_order_date) as week,
		CASE WHEN o.polygon IS NULL THEN CASE WHEN o.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN o.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN o.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE o.polygon
											END
											as polygon,
		o.opportunity_id,
		o.contact__c,
		o.name as name_opp,
		MIN(o.grand_total__c) as revenue_won,
		CASE WHEN o.grand_total__c < 250 THEN '< 250€'
			  WHEN (o.grand_total__c >= 250 AND o.grand_total__c < 750) THEN '250€ <= AND < 750€'
			  WHEN o.grand_total__c >= 750 THEN '>= 750€'
			  ELSE 'Unknown'
			  END as category
		
	FROM
	
		bi.b2borders o
		
	WHERE
	
		o.date < current_date
		AND o.opportunity_id IS NOT NULL
	
	GROUP BY
	
		o.first_order_date,
		year,
		month,
		week,
		o.polygon,
		o.opportunity_id,
		o.contact__c,
		o.name,
		category,
		o.locale__c
		
	ORDER BY
	
		o.first_order_date desc,
		year desc,
		month desc,
		week desc) as table1
		
LEFT JOIN


	salesforce.opportunity o
	
	
ON 

	table1.opportunity_id = o.sfid;