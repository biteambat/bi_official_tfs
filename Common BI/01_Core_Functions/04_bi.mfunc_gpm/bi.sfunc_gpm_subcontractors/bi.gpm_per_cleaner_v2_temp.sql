
-- GPM PER CLEANER V2

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_temp as 

SELECT 

	t2.*, 
	GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
	CASE
	WHEN year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
	WHEN to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
	ELSE calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
	END AS days_worked
	
FROM (

	SELECT	
	
	   to_char(date,'YYYY-MM') as Year_Month,
	   MIN(date) as mindate,
	   t1.name,
	   t1.sfid,
		t1.sfid as professional__c,
		
		CASE WHEN date >= '2018-01-01' THEN t1.pph__c 
		
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.85
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.pph__c
			  
			  END as pph__c,
		delivery_areas as delivery_area,
		DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
		contract_start,
		contract_end,
		type__c,
		SUM(hours) as worked_hours,
		CAST(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
		SUM(GMV) as GMV,
		SUM(GMV) as total_revenue,
		SUM(B2C_GMV) as B2C_Revenue,
		SUM(B2B_GMV) as B2B_Revenue,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2b_share,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2c_share,
		MAX(Weekly_hours) as weekly_hours,
		MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL - DATE_TRUNC('month', DATE))/7 )as monthly_hours
		
	FROM
	
		bi.gmp_per_cleaner_v1_temp t1
		
	WHERE
	
		date >= '2016-01-01'
		
	GROUP BY
	
	   Year_Month,
		professional__c,
		t1.sfid,
		t1.date,
		t1.name,
		t1.company_name__c,
		t1.pph__c,
		contract_start,
		days_of_month,
		type__c,
		contract_end,
		delivery_area,
		calc_end
		) as t2;