
-- CLEANER STATS TEMP

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.name,
	t1.sfid as cleanerid,
	t1.parentid,
	t2.company_name__c,
	t2.parentid as sub_id,
	t2.sfid,
	t2.status__c,
	t1.delivery_area as delivery_areas,
	t2.type__c,
	t2.hr_contract_start__c::date as contract_start,
	t2.hr_contract_end__c::date as contract_end,
	0 as weekly_hours,	
	
	t2.pph__c as pph_subcontractor,
	t1.pph__c as pph_cleaner
FROM
	 bi.cleaner_geoloc_temp t1
LEFT JOIN
	Salesforce.Account t2
ON
	(t1.parentid = t2.sfid)
WHERE 

	(t1.name NOT LIKE '%BAT%' AND t1.name NOT LIKE '%BOOK%') 
	AND t2.test__c = '0' 
	AND t2.name NOT LIKE '%test%' AND t2.name NOT LIKE '%Test%' AND t2.name NOT LIKE '%TEST%'
	AND t2.type__c = 'cleaning-b2b'
	AND t1.name NOT LIKE '%FreshFolds%';