
DROP TABLE IF EXISTS bi.cluster_opps_lost;
CREATE Table bi.cluster_opps_lost as 



SELECT

	t3.*,
	oo.name,
	-- oo.polygon,
	CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
	MAX(oo.returning_month) as existence_churn,
	CASE WHEN t3.revenue_lost IS NULL THEN NULL 
					
												 ELSE CASE WHEN t3.revenue_lost < 250 THEN '< 250€'
															  WHEN (t3.revenue_lost >= 250 AND t3.revenue_lost < 750) THEN '250€ <= AND < 750€'
															  WHEN t3.revenue_lost >= 750 THEN '>= 750€'
															  ELSE 'Unknown'
															  END
												 END as category
FROM

	(SELECT
	
		t1.createddate as date_lost,
		EXTRACT(year FROM t1.createddate) as year,
		EXTRACT(MONTH FROM t1.createddate) as month,
		EXTRACT(WEEK FROM t1.createddate) as week,
		COUNT (DISTINCT t1.opportunityid) as lost_opportunities,
		t2.sfid,
		t2.customer__c,
		
		t2.grand_total__c,
		MIN(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END) as revenue_lost
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		
	GROUP BY
	
		oo.createddate,
		oo.opportunityid) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid
		
	GROUP BY
	
		t1.createddate,
		year,
		month,
		week,
		t2.sfid,
		t2.customer__c,
		t2.grand_total__c
		
	ORDER BY
	
		year desc,
		month desc,
		week desc) as t3
	
LEFT JOIN 

	bi.b2borders oo
	
ON

	t3.sfid = oo.opportunity_id
	
GROUP BY

	t3.date_lost,
	t3.year,
	t3.month,
	t3.week,
	t3.lost_opportunities,
	t3.sfid,
	t3.customer__c,
	t3.grand_total__c,
	t3.revenue_lost,
	oo.name,
	oo.polygon,
	oo.locale__c
	
ORDER BY

	year desc,
	month desc,
	week desc;