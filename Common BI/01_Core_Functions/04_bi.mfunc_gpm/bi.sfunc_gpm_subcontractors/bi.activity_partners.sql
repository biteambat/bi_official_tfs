
DROP TABLE IF EXISTS bi.activity_partners;
CREATE TABLE bi.activity_partners AS

SELECT

	*
	
FROM

(SELECT

	t5.date,
	CASE WHEN t4.partner_start_date <= t5.date AND ((t4.partner_end_date > t5.date) OR (t4.partner_end_date IS NULL)) THEN 'Active' ELSE 'Unactive' END as status_at_time,
	t4.name_partner,
	t4.partner_start_date,
	t4.partner_end_date,
	t4.status__c,
	t4.sfid_partner,
	t4.parentid_company,
	t4.hr_contract_start__c,
	t4.hr_contract_end__c,
	t4.company_name__c,
	t4.number_employees_tot,
	UPPER(t4.country),
	t4.polygon,
	t4.hourly_cost_sub,
	t4.parentid_cleaner,
	t4.sfid_cleaner,
	t4.name_cleaner


FROM

	(SELECT
			
		1 as connecting_key,
		i::date as date 
		
	FROM
	
		generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i) as t5

	FULL JOIN
	
	
	(SELECT
	
		1 as connecting_key,
		t3.hr_contract_start__c as partner_start_date,
		t3.hr_contract_end__c as partner_end_date,
		t1.*
			
	FROM	
	
		(SELECT
  
		    a.name as name_partner,
		    a.status__c,
		    a.type__c,
		    a.hr_contract_start__c,
		    a.hr_contract_end__c,
		    a.sfid as sfid_partner,
		    a.parentid as parentid_company,
		    a.company_name__c,
		    LEFT(b.delivery_areas__c, 2) as country,
		    CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
		                            WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
		                            WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
		                            ELSE 'at-other'
		                            END
		                        ELSE b.delivery_areas__c
		                        END as polygon,
		    a.numberofemployees as number_employees_tot,
		    a.pph__c as hourly_cost_sub,
		    b.parentid as parentid_cleaner,
		    b.sfid as sfid_cleaner,
		    b.name as name_cleaner,
		    b.status__c as status_cleaner
		    
		  FROM
		  
		    salesforce.account a
		    
		  LEFT JOIN
		  
		    salesforce.account b
		    
		  ON 
		  
		    a.sfid = b.parentid
		    
		  WHERE
		  
		    -- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
		    a.type__c = 'partner'
		    AND a.type__c NOT LIKE 'master'
		    AND a.company_name__c NOT LIKE '%BAT%'
		    AND a.company_name__c IS NOT NULL
		    AND a.company_name__c NOT LIKE ''
		    AND LOWER(a.name) not like '%test%'
		    AND LOWER(a.company_name__c) not like '%test%'
		    AND a.test__c IS FALSE) t1
	
	LEFT JOIN
	
		(SELECT
			t2.name,
			t2.company_name__c,
			t1.parentid,
			t2.hr_contract_start__c,
			t2.hr_contract_end__c
			
		FROM
		
			Salesforce.Account t1
			
		JOIN
		
			Salesforce.Account t2
			
		ON
		
			(t1.parentid = t2.sfid)
			
		WHERE
		
			(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
			
		GROUP BY
		
			t2.name,
			t2.company_name__c,
			t1.parentid,
			t2.hr_contract_start__c,
			t2.hr_contract_end__c) as t3
			
	ON
	
		t1.company_name__c = t3.company_name__c) t4
	
ON 

	t5.connecting_key = t4.connecting_key

ORDER BY


	t5.date desc) as t6;