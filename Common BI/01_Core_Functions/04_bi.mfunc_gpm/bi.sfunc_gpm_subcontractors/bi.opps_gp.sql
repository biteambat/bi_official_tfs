
DROP TABLE IF EXISTS bi.opps_gp;
CREATE TABLE bi.opps_gp AS

SELECT

	table2.date_orders,
	table2.country,
	table2.polygon,
	table2.name,
	table2.name_company,
	table2.parentid,
	table2.sfid_cleaner,
	table2.contact_customer,
	o.contact__c as sfid_opp,
	o.name as name_opp,
	table2.order_reference,
	table2.order_duration,
	table2.revenue,
	table2.eff_pph,
	table2.cost_sub_per_hour,
	table2.cost_sub,
	table2.gp,
	table2.gpm
	
FROM

	bi.orders_subcontractors table2
	
LEFT JOIN

	bi.b2borders o
	
ON

	table2.contact_customer = o.contact__c
	
GROUP BY

	table2.date_orders,
	table2.country,
	table2.polygon,
	table2.name,
	table2.name_company,
	table2.parentid,
	table2.sfid_cleaner,
	table2.contact_customer,
	o.contact__c,
	o.name,
	table2.order_reference,
	table2.order_duration,
	table2.revenue,
	table2.eff_pph,
	table2.cost_sub_per_hour,
	table2.cost_sub,
	table2.gp,
	table2.gpm;