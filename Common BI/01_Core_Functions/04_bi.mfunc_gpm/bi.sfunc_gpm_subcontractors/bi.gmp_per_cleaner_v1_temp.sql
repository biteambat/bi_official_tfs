
-- GMP PER CLEANER V1 TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
CREATE TABLE bi.gmp_per_cleaner_v1_temp as 

SELECT

	t1.name,
	t1.company_name__c,
	t1.cleanerid as sfid,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	t2.gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours,
	CASE WHEN t1.pph_cleaner IS NOT NULL THEN t1.pph_cleaner ELSE t1.pph_subcontractor END as pph__c
	
FROM

	bi.cleaner_Stats_temp t1
	
LEFT JOIN

	bi.gmp_per_cleaner_temp t2
	
ON

	(t1.cleanerid = t2.professional__c);