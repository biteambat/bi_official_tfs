
DROP TABLE IF EXISTS bi.gp_cleaners_sub;
CREATE TABLE bi.gp_cleaners_sub AS

SELECT

	t4.*,
	-- a1.delivery_areas__c,
	CASE WHEN a1.delivery_areas__c IS NULL THEN CASE WHEN a1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN a1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN a1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE a1.delivery_areas__c
											END
											as delivery_areas__c,
											
	CASE WHEN t4.effectivedate >= '2018-01-01' THEN a1.pph__c 
		
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%DADI%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Crystal%') THEN 17
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%PureX%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Sunny%') THEN 18.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%facilitas%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE a1.pph__c
			  
			  END as pph__c,
	a1.pph__c as without_changes,
	
	-- t4.hours*a1.pph__c as cost_sub,
	
	t4.hours*(CASE WHEN t4.effectivedate >= '2018-01-01' THEN a1.pph__c 
		
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%DADI%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Crystal%') THEN 17
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%PureX%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Sunny%') THEN 18.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%facilitas%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE a1.pph__c
			  
			  END) as cost_sub,
	
	t4.gmv_eur_net - t4.hours*(CASE WHEN effectivedate >= '2018-01-01' THEN a1.pph__c 
		
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%DADI%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Crystal%') THEN 17
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%PureX%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Sunny%') THEN 18.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%facilitas%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE a1.pph__c
			  
			  END) as  gp
		
	-- t4.gmv_eur_net - t4.hours*a1.pph__c as  gp

FROM


	(SELECT
	
	
		t3.*,
		a.name
	
	FROM
	
		(SELECT
		
			EXTRACT(YEAR FROM o.effectivedate) as year,
			EXTRACT(MONTH FROM o.effectivedate) as month,
			o.effectivedate,
			o.sfid,
			o.status,
			t4.name as name_partner,
			t4.sfid_cleaner as cleaner_partner,
			MIN(o.order_duration__c) as hours,
			MIN(o.gmv_eur_net) as gmv_eur_net
			
		FROM
		
			(SELECT	
								
				oo.name,
				a.sfid as sfid_cleaner,
				oo.name_company,
				oo.parentid,
				oo.start_partner,
				oo.end_partner,
				oo.pph__c
				
			FROM
			
			
				(SELECT
						
					t2.name,
					t2.sfid,
					t2.company_name__c as name_company,
					t1.parentid,
					t2.hr_contract_start__c as start_partner,
					t2.hr_contract_end__c as end_partner,
					t2.pph__c
					
				FROM
				
					Salesforce.Account t1
					
				JOIN
				
					Salesforce.Account t2
					
				ON
				
					(t2.sfid = t1.parentid)
					
				WHERE
				
					(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
					
				GROUP BY
				
					t2.name,
					t2.sfid,
					t2.company_name__c,
					t1.parentid,
					t2.hr_contract_start__c,
					t2.hr_contract_end__c,
					t2.pph__c) as oo
					
			LEFT JOIN
			
				salesforce.account a
				
			ON 
			
				oo.sfid = a.parentid) as t4
				
		LEFT JOIN
		
			bi.orders o
			
		ON 
		
			t4.sfid_cleaner = o.professional__c
			
		WHERE 
		
			o.effectivedate IS NOT NULL
			-- AND EXTRACT(YEAR FROM o.effectivedate) < 2018
			-- AND EXTRACT(MONTH FROM o.effectivedate) = 9
			-- AND EXTRACT(YEAR FROM o.effectivedate) > 2016
			AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'FULFILLED', 'PENDING TO START')
			
		GROUP BY
		
			year,
			month,
			o.effectivedate,
			o.status,
			o.sfid,
			name_partner,
			cleaner_partner
			
		ORDER BY
		
			o.effectivedate desc,
			o.status,
			name_partner,
			cleaner_partner) as t3
			
	LEFT JOIN
	
		salesforce.account a 
		
	ON
	
		t3.cleaner_partner = a.sfid
		
	ORDER BY
	
		year desc,
		month desc,
		effectivedate desc) as t4
		
LEFT JOIN

	salesforce.account a1
	
ON

	t4.name_partner = a1.name
	
WHERE

	a1.parentid IS NULL
	AND EXTRACT(YEAR FROM effectivedate) = 2017
	AND t4.name_partner LIKE '%Sunny%'
	
ORDER BY
	
	year desc,
	month desc,
	effectivedate desc;