
DROP TABLE IF EXISTS bi.account_temp;
CREATE TABLE bi.account_temp as 

SELECT  

   t1.name,
   t1.company_name__c,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.delivery_areas__c,
   CASE WHEN t1.company_name__c LIKE '%Ali Kocahal%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Akzent Facility Services GmbH%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Sadettin Akal%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%ATIS Gebäudereinigung de- Cologne%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%blitzgebäudereinigung de-Hamburg%' THEN 'de-hamburg'
		  WHEN t1.company_name__c LIKE '%Kostic%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Crystal Facility Service GmbH de-berlin%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Herr Kovacs%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Jefa Gebäudereinigung%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%KNM Gebäudeservice GmbH%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%Medical Clean Management GmbH de-munich%' THEN 'de-munich'
		  WHEN t1.company_name__c LIKE '%Dordevic%' THEN 'de-munich'
		  WHEN t1.company_name__c LIKE '%Stamm´s Haus & Hof Service%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%TMG Gebäudereinigung und Service de-stuttgart%' THEN 'de-stuttgart'
		  WHEN t1.company_name__c LIKE '%1636583753%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Kovacs%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Korkmaz%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Czerwinski%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%Kordian%' THEN 'de-frankfurt'
	ELSE 
		CASE WHEN t1.delivery_areas__c IS NULL THEN CASE WHEN t1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN t1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN t1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE t1.delivery_areas__c
											END
										
	END as new_areas,
	t1.longitude__c,
	t1.latitude__c
     
FROM
 
   Salesforce.account t1
WHERE
	t1.type__c = 'cleaning-b2b'
      
GROUP BY
 
   t1.name,
   t1.company_name__c,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.delivery_areas__c,
   t1.longitude__c,
	t1.latitude__c,
   new_areas,
   t1.locale__c;