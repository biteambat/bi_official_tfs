
DROP TABLE IF EXISTS bi.stage_opps;
CREATE Table bi.stage_opps as 

SELECT

	o.createddate,
	EXTRACT(YEAR FROM o.createddate) as year,
	EXTRACT(month FROM o.createddate) as month,
	o.name,
	o.stagename,
	SUM(o.grand_total__c) as revenue
	
FROM

	salesforce.opportunity o
	
GROUP BY

	o.createddate,
	year,
	month,
	name,
	stagename
	
ORDER BY

	year desc,
	month desc;