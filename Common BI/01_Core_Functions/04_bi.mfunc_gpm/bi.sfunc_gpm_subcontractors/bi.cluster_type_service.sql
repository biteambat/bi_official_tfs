
DROP TABLE IF EXISTS bi.cluster_type_service;
CREATE Table bi.cluster_type_service as 
	
SELECT

	table5.first_order_date,
	table5.year,
	table5.month,
	table5.week,
	table5.polygon,
	table5.opportunity_id,
	table5.name_opp,
	table5.revenue_won,
	table5.category,
	CASE WHEN table5.sfid_cleaner IS NULL THEN 'Served by BAT' ELSE 'Served by Partner' END as type_service

FROM
	
	(SELECT
	
		table3.*,
		table4.sfid as sfid_cleaner
	
	FROM	
		
		
		(SELECT
		
			table2.*,
			o.professional__c
		
		FROM	
			
			(SELECT
			
				table1.*,
				o.sfid,
				o.customer__c,
				CASE WHEN table1.revenue_won < 250 THEN '< 250€'
					  WHEN (table1.revenue_won >= 250 AND table1.revenue_won < 750) THEN '250€ <= AND < 750€'
					  WHEN table1.revenue_won >= 750 THEN '>= 750€'
					  ELSE 'Unknown'
					  END as category
				
			FROM
			
				(SELECT
				
					o.first_order_date,
					EXTRACT(year FROM o.first_order_date) as year,
					EXTRACT(MONTH FROM o.first_order_date) as month,
					EXTRACT(WEEK FROM o.first_order_date) as week,
					CASE WHEN o.polygon IS NULL THEN CASE WHEN o.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN o.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN o.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE o.polygon
											END
											as polygon,
					o.opportunity_id,
					o.contact__c,
					o.name as name_opp,
					MIN(o.grand_total__c) as revenue_won
					
					
				FROM
				
					bi.b2borders o
					
				WHERE
				
					o.date < current_date
					AND o.opportunity_id IS NOT NULL
				
				GROUP BY
				
					o.first_order_date,
					year,
					month,
					week,
					o.polygon,
					o.opportunity_id,
					o.contact__c,
					o.name,
					o.locale__c
					
				ORDER BY
				
					o.first_order_date desc,
					year desc,
					month desc,
					week desc) as table1
					
			LEFT JOIN
			
			
				salesforce.opportunity o
				
				
			ON 
			
				table1.opportunity_id = o.sfid) as table2
				
		
		LEFT JOIN
		
			bi.orders o
			
		ON 
		
			table2.contact__c = o.contact__c
			AND table2.first_order_date = o.effectivedate
			AND table2.polygon = o.polygon) as table3
			
	LEFT JOIN		
			
		(SELECT
	        t1.*,
	        t2.name as subcon
	      FROM
	       Salesforce.Account t1
	        JOIN
	            Salesforce.Account t2
	        ON
	            (t2.sfid = t1.parentid)
	    
	        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	      and t2.name NOT LIKE '%BAT Business Services GmbH%')	as table4
			
	ON
	
		table3.professional__c = table4.sfid) as table5	
	
GROUP BY

	table5.first_order_date,
	table5.year,
	table5.month,
	table5.week,
	table5.polygon,
	table5.opportunity_id,
	table5.name_opp,
	table5.revenue_won,
	table5.category,
	table5.sfid_cleaner
	
ORDER BY

  table5.first_order_date desc;