
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN sickness IS NULL THEN 0 ELSE sickness END as sickness,
	CASE WHEN holiday IS NULL THEN 0 ELSE holiday END as holiday
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month);