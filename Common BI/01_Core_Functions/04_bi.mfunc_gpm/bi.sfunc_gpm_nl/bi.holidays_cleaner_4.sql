
DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(sickness_flag) as sickness,
	SUM(holiday_flag) as holiday
FROM
	bi.holidays_cleaner_3
GROUP BY
	Year_Month,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;