
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and date < current_date::date
	and status__c in ('approved');
