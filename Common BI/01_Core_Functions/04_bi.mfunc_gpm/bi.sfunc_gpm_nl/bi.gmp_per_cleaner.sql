
DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN GMV__c
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date THEN GMV__c*1.06 ELSE 0 END) as GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN GMV__c ELSE 0 END) as B2C_GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date THEN GMV__c*1.06 ELSE 0 END) as 	B2B_GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date THEN Order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN Order_Duration__c ELSE 0 END) as B2C_Hours,
	SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date THEN Order_Duration__c 		ELSE 0 END) as B2B_Hours
FROM
	bi.orders t1
WHERE
	left(locale__c,2) = 'nl'
GROUP BY
	t1.professional__c,
	date;