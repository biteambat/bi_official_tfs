
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp as 
SELECT
    Year_month,
    MIN(mindate) as mindate,
    professional__c,
    delivery_area,
    worked_hours,
    type__c,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    -- min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(holiday) as holiday_hours,
    (weekly_hours/5)*(sickness) as sickness_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness)+worked_hours as total_hours,
	
	CASE WHEN ((weekly_hours/5)*(sickness - holiday))+worked_hours >= (monthly_hours/days_of_month)*days_worked 
		  THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73 
		  ELSE CASE WHEN ((weekly_hours/5)*(sickness - holiday)+worked_hours) > 0 
		  				THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73
						ELSE 0
						END
		  END as salary_payed,
	
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV/1.06) as Revenue,
	SUM(GMV/1.06) -  CASE WHEN ((weekly_hours/5)*(sickness - holiday))+worked_hours >= (monthly_hours/days_of_month)*days_worked 
						  THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73 
						  ELSE CASE WHEN ((weekly_hours/5)*(sickness - holiday)+worked_hours) > 0 
						  				THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73
										ELSE 0
										END
						  END as gp

FROM
     bi.gpm_per_cleaner_v2_2
     
WHERE

    LEFT(delivery_area,2) = 'nl'
    AND days_worked > 0
    
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    holiday_hours,
    sickness_hours,
    days_worked,
    worked_hours;