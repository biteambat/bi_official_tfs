
DROP TABLE IF EXISTS bi.gpm_ch;
CREATE TABLE bi.gpm_ch as

SELECT

	t2.name,
	CAST(t2.rating__c as decimal) as rating,
	t5.Score_cleaners,
	CASE WHEN t2.hr_contract_start__c >= (current_date - 10) THEN 'New' ELSE 'Old' END as flag_new,
	t5.category,
	t5.existence,
	t2.type__c,
	t1.*,
	CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
	CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END as contract_morning_hours,
	t3.Working_Hours_Morning,
	availability_Share,
	CASE WHEN availability_share is null or availability_share = 0 THEN 0 ELSE cast(t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END) END as morning_ur,
	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END as contract_hours_afternoon,
	t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
	CASE WHEN (1-availability_Share) is null or (1-availability_Share) = 0 THEN 0 ELSE cast(t3.total_hours-t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*(1-availability_Share) END) END as afternoon_ur,
	CASE WHEN availability_Share is null THEN 0 ELSE CASE WHEN t3.Working_Hours_Morning > working_hours*availability_Share THEN working_hours*availability_Share ELSE t3.Working_Hours_Morning END END capped_hours_morning,
	CASE WHEN (1-availability_Share) = 0 THEN 0 ELSE CASE WHEN t3.total_hours-t3.working_hours_morning  > working_hours*	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END THEN working_hours*(	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END) ELSE t3.total_hours-t3.working_hours_morning END END capped_hours_afternoon

FROM

	bi.gpm_per_cleaner_v4_temp t1
	
LEFT JOIN
	Salesforce.Account t2	
ON
	(t1.professional__c = t2.sfid)
	
LEFT JOIN
	bi.order_distribution_temp t3	
ON
	(t1.professional__c = t3.professional__c and t1.year_month = t3.month)
	
LEFT JOIN
	bi.cleaner_ur_temp_temp t4	
ON
	(t1.professional__c = t4.sfid)
	
LEFT JOIN
	bi.Performance_cleaners t5	
ON
	(t1.professional__c = t5.sfid);

	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v4_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;