
-- GPM PER CLEANER TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
CREATE TABLE bi.gmp_per_cleaner_temp as 

SELECT

	t1.professional__c,
	-- t1.polygon as delivery_area__c,
	CASE WHEN t1.polygon IS NULL THEN CASE WHEN t1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN t1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN t1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE t1.polygon
											END as delivery_areas__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date  THEN gmv_eur
	         WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN gmv_eur*1.2 
				ELSE 0 END) as GMV,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date THEN gmv_eur ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN gmv_eur*1.2 ELSE 0 END) as b2b_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date  THEN order_Duration__c ELSE 0 END) + SUM(CASE WHEN (type = 'cleaning-b2b') AND Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2c_hours,
	SUM(CASE WHEN (type = 'cleaning-b2b') AND Status IN  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2b_hours
		
FROM

	bi.orders t1
	
WHERE

	t1.professional__c IS NOT NULL
	
GROUP BY

	t1.professional__c,
	t1.polygon,
	date,
	locale__c;