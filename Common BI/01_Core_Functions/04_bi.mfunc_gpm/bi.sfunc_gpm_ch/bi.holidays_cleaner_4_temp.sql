
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
CREATE TABLE bi.holidays_cleaner_4_temp as 

SELECT

	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
	
FROM

	bi.holidays_cleaner_3_temp
	
GROUP BY

	Year_Month,
	account__c;
