
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
CREATE TABLE bi.holidays_cleaner_2_temp as 

SELECT

	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
	
FROM

	bi.holidays_cleaner_temp,
	bi.orderdate
	
WHERE

	date > '2016-01-01'
	AND status__c in ('approved');