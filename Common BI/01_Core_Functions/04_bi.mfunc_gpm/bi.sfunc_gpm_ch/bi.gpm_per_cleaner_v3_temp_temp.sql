
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
	MIN(mindate) as mindate,
   professional__c,
   type__c,
   delivery_area,
   worked_hours,
   contract_start,
   contract_end,
   monthly_hours,
   weekly_hours,
   days_worked,
   days_of_month,
   sickness,
   -- MIN(mindate) as mindate,
   holiday,
   (weekly_hours/5)*(sickness+holiday) as absence_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours) ELSE ((monthly_hours/days_of_month)*days_worked) END as total_hours,

	
	CASE WHEN mindate < '2015-10-01' THEN
	
			(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.96
				
			WHEN (mindate >= '2015-10-01' AND mindate < '2017-12-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.92
				
			WHEN (mindate >= '2017-12-01' AND mindate < '2018-01-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.85696
				
			WHEN (mindate >= '2018-01-01' AND mindate < '2018-02-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.85497
				
			WHEN (mindate >= '2018-02-01' AND mindate < '2018-03-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.86243
				
			ELSE 
		
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.86721
				
			END
			
		as salary_payed,
	
	
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	B2C_share,
	B2B_share,
	
	
	--------------------------------------------- OLD VERSION
	
	CASE WHEN mindate < '2015-10-01' THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.96
			
		 WHEN (mindate >= '2015-10-01' AND mindate < '2017-12-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.92
			
		 WHEN (mindate >= '2017-12-01' AND mindate < '2018-01-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.85696
			
		 WHEN (mindate >= '2018-01-01' AND mindate < '2018-02-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.85497
			
		 WHEN (mindate >= '2018-02-01' AND mindate < '2018-03-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.86243
			
		 ELSE
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.86721
			
		END	
		
		
		as b2b_gp,
		
	
	CASE WHEN mindate < '2015-10-01' THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.96
			
		WHEN (mindate >= '2015-10-01' AND mindate < '2017-12-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.92
			
		WHEN (mindate >= '2017-12-01' AND mindate < '2018-01-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.85696
			
		WHEN (mindate >= '2018-01-01' AND mindate < '2018-02-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.85497
			
		WHEN (mindate >= '2018-02-01' AND mindate < '2018-03-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.86243
			
		ELSE
		
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.86721
			
		END
	
		as b2c_gp,
	
	-- CASE WHEN (SUM(B2C_Revenue) + SUM(B2B_Revenue)) > 0
		  -- THEN ((SUM(B2C_Revenue) + SUM(B2B_Revenue)) - SUM(CASE WHEN (weekly_hours/5)*(sickness+holiday)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) ELSE (monthly_hours/days_of_month)*days_worked*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) END))/(SUM(B2C_Revenue) + SUM(B2B_Revenue))
		  -- ELSE 0
		  -- END as gpm_ch,
		  
	(SUM(B2C_Revenue) + SUM(B2B_Revenue)) - SUM(CASE WHEN (weekly_hours/5)*(sickness+holiday)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) ELSE (monthly_hours/days_of_month)*days_worked*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) END)  as gp,
	SUM(GMV) as GMV,
	(SUM(B2C_Revenue) + SUM(B2B_Revenue)) as Revenue	
	
FROM

     bi.gpm_per_cleaner_v2_2_temp
     
WHERE

    LEFT(delivery_area,2) = 'ch'
    AND days_worked > '0'
    
GROUP BY
    year_month,
    mindate,
    professional__c,
    type__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    B2C_share,
	 B2B_share,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    days_worked,
    worked_hours;