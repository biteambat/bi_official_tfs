
-- CLEANER STATS TEMP

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 

SELECT

	a.name,
	a.sfid,	
	a.status__c,
	a.sfid as professional__c,
	-- a.delivery_areas__c as delivery_areas,
	CASE WHEN a.delivery_areas__c IS NULL THEN CASE WHEN a.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN a.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN a.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE a.delivery_areas__c
											END as delivery_areas,
	a.type__c,
	a.hr_contract_start__c::date as contract_start,
	a.hr_contract_end__c::date as contract_end,	
	a.hr_contract_weekly_hours_min__c as weekly_hours
	
FROM

   Salesforce.Account a
   
WHERE 

	a.test__c = '0' AND a.name NOT LIKE '%test%' AND a.name NOT LIKE '%Test%' AND a.name NOT LIKE '%TEST%'
	-- AND (a.type__c LIKE 'cleaning-b2c' OR (a.type__c LIKE '%cleaning-b2c;cleaning-b2b%') OR a.type__c LIKE 'cleaning-b2b')
	AND a.hr_contract_weekly_hours_min__c IS NOT NULL
	AND  a.test__c = '0'
	AND LEFT(a.delivery_areas__c,2) = 'ch'

GROUP BY 

   a.name,
   a.sfid,
   a.status__c,
   professional__c,
   delivery_areas,
   a.type__c,
	contract_start,
	contract_end,
	weekly_hours,
	locale__c;