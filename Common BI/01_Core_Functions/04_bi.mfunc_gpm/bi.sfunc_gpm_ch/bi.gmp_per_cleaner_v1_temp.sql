
-- GMP PER CLEANER V1 TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
CREATE TABLE bi.gmp_per_cleaner_v1_temp as 

SELECT

	t1.name,
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	t2.gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours
	
FROM

	bi.cleaner_Stats_temp t1
	
LEFT JOIN

	bi.gmp_per_cleaner_temp t2
	
ON

	(t1.professional__c = t2.professional__c);