
DROP TABLE IF EXISTS bi.temp_city_overview_4;
CREATE TABLE bi.temp_city_overview_4 AS

  SELECT

  c.orderdate::date as date,
  c.locale::text as locale,
  CAST('B2C' as varchar) as unit,

  NULL::text as polygon,
  SUM(c.sem_non_brand + c.sem_brand + c.seo + c.facebook + c.criteo + c.sociomantic + c.gdn + c.youtube + c.coops + c.tvcampaign + c.offline_marketing + c.other_voucher)::numeric as total_costs

  FROM bi.cpacalclocale c
  
  GROUP BY c.orderdate::date, c.locale

  ORDER BY date desc, locale asc

;