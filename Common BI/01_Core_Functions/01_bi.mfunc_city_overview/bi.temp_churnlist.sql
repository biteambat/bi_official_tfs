
DROP TABLE IF EXISTS bi.temp_churnlist;
CREATE TABLE bi.temp_churnlist AS

  SELECT

    t2.*

  FROM bi.temp_acquiredlist t1

  JOIN bi.temp_churns t2 ON t1.customer_id__c = t2.customer_id

;