
DROP TABLE IF EXISTS bi.temp_city_overview_3;
CREATE TABLE bi.temp_city_overview_3 AS

  SELECT
    t1.date::date as date,
    LEFT(t1.locale__c,2)::text as locale,
    CAST('B2C' as varchar) as unit,
    t2.polygon::text as polygon,
    COUNT(DISTINCT t1.customer_id)::numeric as churned_customers

  FROM bi.temp_churnlist t1

  JOIN bi.orders t2 ON t1.Order_Id = t2.Order_Id__c

  WHERE t2.test__c = '0' 
    AND order_type = '1'
    AND t1.date::date < current_date::date
    AND t2.effectivedate >= '2018-01-01'
  
  GROUP BY t1.date::date, LEFT(t1.locale__c,2) , t2.polygon

  ORDER BY date desc, locale asc, polygon asc

;