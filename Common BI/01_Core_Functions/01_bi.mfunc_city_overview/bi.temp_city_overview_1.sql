
DROP TABLE IF EXISTS bi.temp_city_overview_1;
CREATE TABLE bi.temp_city_overview_1 AS

  SELECT
    o.effectivedate::date as date,
    LEFT(o.locale__c,2)::text as locale,
    CAST('B2C' as varchar) as unit,
    o.polygon::text as polygon,
    COUNT(DISTINCT CASE WHEN (o.acquisition_new_customer__c = '1' AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER') and o.acquisition_customer_creation__c::date = c.createddate::date) THEN o.sfid ELSE NULL END)::numeric as invoiced_acquisitions,
    SUM(CASE WHEN o.status IN ('INVOICED', 'NOSHOW CUSTOMER') THEN o.gmv_eur_net ELSE 0 END)::numeric as invoiced_gmv_gross, 
    SUM(CASE WHEN o.status IN ('INVOICED', 'NOSHOW CUSTOMER') and recurrency__c > '0' THEN o.gmv_eur_net ELSE 0 END)::numeric as invoiced_gmv_recurrent_gross, 
      SUM(CASE WHEN o.status IN ('INVOICED', 'NOSHOW CUSTOMER') THEN o.order_duration__c ELSE 0 END)::numeric as invoiced_hours, 
    COUNT(DISTINCT o.customer_id__c)::numeric as active_customers, 
    COUNT(DISTINCT CASE WHEN (o.acquisition_new_customer__c = '0' AND o.acquisition_channel__c = 'web') or (o.acquisition_customer_creation__c::date != c.createddate::date and acquisition_new_customer__c = '1') THEN o.customer_id__c ELSE NULL END)::numeric as reactivated_customers,

    COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED NO MANPOWER' THEN o.sfid ELSE NULL END)::numeric as orders_no_manpower, 
    COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED NO MANPOWER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN o.sfid ELSE NULL END)::numeric as divisor_NMP_calc, 

    COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED CUSTOMER' THEN o.sfid ELSE NULL END)::numeric as orders_skipped, 
    COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED CUSTOMER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN o.sfid ELSE NULL END)::numeric as divisor_skipped_calc 
  
  FROM bi.orders o
  JOIN Salesforce.Contact c
  ON (o.contact__c = c.sfid)

  WHERE o.test__c = '0'
    AND o.order_type = '1'
    AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
    AND o.effectivedate::date < current_date::date
    AND o.effectivedate >= '2018-01-01'

  GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon

  ORDER BY date desc, locale asc, polygon asc

;


INSERT INTO bi.temp_city_overview_1

  SELECT
    o.effectivedate::date as date,
    LEFT(o.locale__c,2)::text as locale,
    CAST('B2B' as varchar) as unit,
    o.polygon::text as polygon,
    0 as invoiced_acquisitions,
    SUM(CASE WHEN o.status not like '%CANCELLED%' and o.status not like '%ERROR%' and o.status not like '%NOSHOW PROFESSIONAL%' and effectivedate::date < current_date::date THEN o.gmv_eur_net ELSE 0 END)::numeric as invoiced_gmv_gross, 
    0 as invoiced_gmv_recurrent_gross,
    SUM(CASE WHEN o.status not like '%CANCELLED%' and o.status not like '%ERROR%' and o.status not like '%NOSHOW PROFESSIONAL%' and effectivedate::date < current_date::date THEN o.order_duration__c ELSE 0 END)::numeric as invoiced_hours,
    COUNT(DISTINCT o.customer_id__c)::numeric as active_customers, 
    0 as reactivated_customers,

    COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED NO MANPOWER' THEN o.sfid ELSE NULL END)::numeric as orders_no_manpower,
    COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED NO MANPOWER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') or status not like '%ERROR%' THEN o.sfid ELSE NULL END)::numeric as divisor_NMP_calc, 

    COUNT(DISTINCT CASE WHEN o.status = 'CANCELLED CUSTOMER' THEN o.sfid ELSE NULL END)::numeric as orders_skipped, 
    COUNT(DISTINCT CASE WHEN o.status IN ('CANCELLED CUSTOMER', 'INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') or status not like '%ERROR%' THEN o.sfid ELSE NULL END)::numeric as divisor_skipped_calc

  FROM bi.orders o
  JOIN Salesforce.Contact c
  ON (o.contact__c = c.sfid)

  WHERE o.test__c = '0'
    AND o.order_type = '2'
    AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
    AND o.effectivedate::date < current_date::date
    AND o.effectivedate >= '2018-01-01'

  GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon

  ORDER BY date desc, locale asc, polygon asc

;