
DROP TABLE IF EXISTS bi.churns_motivations;
  CREATE TABLE bi.churns_motivations AS

    SELECT
      t1.date,
      t1.event_name,
      t1.orderdate,
      LEFT(t1.locale__c,2) as locale,
      t1.polygon,
      t1.order_id,
      t1.error_note__c,
      COUNT(DISTINCT(t2.order_id)) as Count_NMP_L28D,
      COUNT(DISTINCT(t3.order_id)) as Count_Badrate_L28D

    FROM bi.temp_orders_terminated_2 t1
      LEFT JOIN bi.temp_orders_NMP t2
        ON t1.customer_id = t2.customer_id 
        AND (t2.date) >= (t1.date - interval '28 days')
      LEFT JOIN bi.temp_badrate_orders t3
        ON t1.customer_id = t3.customer_id
        AND (t3.orderdate) >= (t1.date - interval '28 days')

    GROUP BY t1.date, t1.event_name, t1.orderdate, LEFT(t1.locale__c,2), t1.polygon, t1.order_id, t1.error_note__c

    ORDER BY t1.date desc

  ;