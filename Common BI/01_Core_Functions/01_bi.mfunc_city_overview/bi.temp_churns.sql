
DROP TABLE IF EXISTS bi.temp_churns;
CREATE TABLE bi.temp_churns as 
SELECT

  created_at::date as date,
  created_at as time,
  CASE WHEN order_Json->>'Type' IN ('cleaning-b2c', '1') THEN CAST('B2C' as varchar) ELSE CAST('B2B' as varchar) END as unit,
  (order_Json->>'Order_Start__c') as orderdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id,
  order_Json->>'Type' as type_order
  FROM
   events.sodium
   
  WHERE  event_name = 'Order Event:CANCELLED TERMINATED'

;