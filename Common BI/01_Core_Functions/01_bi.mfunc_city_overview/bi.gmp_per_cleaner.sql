
DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
  t1.professional__c,
  Effectivedate::date as date,
  SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER') and (type = 'cleaning-b2c' or type = '60')THEN GMV__c
    WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222' and effectivedate::date < current_Date-2) THEN GMV__c*1.19 ELSE 0 END) as GMV,
  SUM(CASE WHEN type = 'cleaning-b2c'  and Status in  ('INVOICED','NOSHOW CUSTOMER') THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN type = 'cleaning-b2b'  and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date-2 THEN Order_Duration__c ELSE 0 END) as Hours

FROM
  bi.orders t1
  
WHERE

  t1.effectivedate >= '2018-01-01'
GROUP BY
  t1.professional__c,
  date;