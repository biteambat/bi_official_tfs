
DROP TABLE IF EXISTS bi.left_date;
CREATE TABLE bi.left_date as  
SELECT
  professional_json->>'Id' as sfid,
  min(created_at::date) as date
FROM
  events.sodium
WHERE
  event_name in ('Account Event:LEFT','Account Event:TERMINATED','Account Event:SUSPENDED')
GROUP BY
  sfid;