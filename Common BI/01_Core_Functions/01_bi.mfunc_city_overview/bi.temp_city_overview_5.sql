
DROP TABLE IF EXISTS bi.temp_city_overview_5;
CREATE TABLE bi.temp_city_overview_5 AS

  SELECT

  u.mindate::date as date,
  LEFT(u.delivery_area,2)::text as locale,
  u.delivery_area::text as polygon,
  CAST('B2C' as varchar) as unit,
  SUM(u.weekly_hours)::numeric as weekly_contract_hours,
  SUM(u.worked_hours)::numeric as weekly_worked_hours,
  SUM(u.revenue)::numeric as weekly_revenue_generated,
  SUM(u.salary_payed)::numeric as weekly_salary_paid

  FROM bi.gpm_weekly_cleaner u

  WHERE mindate < current_date
  
  GROUP BY u.mindate::date, LEFT(u.delivery_area,2), u.delivery_area

  ORDER BY date desc, locale asc, polygon asc

;