
DROP TABLE IF EXISTS bi.customer_cohorts_recurrent;
CREATE TABLE bi.customer_cohorts_recurrent as   
SELECT
  a.first_order_month as cohort,
  b.order_month,
  b.order_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.city,
  a.acquisition_channel,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer
FROM
(SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer
FROM
  bi.customer_cohorts_exp
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer
FROM
  bi.customer_cohorts_exp
) as b
ON
  (a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel);


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
  city,
  'M1 RR Recurrent' as kpi,
  CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  city,
  kpi,
  channel;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
      case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M1 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '1'
GROUP BY
  month,
  year,
  city,
  kpi;


INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
        case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M3 RR Recurrent' as kpi,
  CASE WHEN acquisition_channel in ('SEO Brand','SEM Brand','DTI') THEN 'Brand Marketing' ELSE acquisition_channel END as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  city,
  kpi,
  channel;

INSERT INTO bi.topline_kpi
SELECT
  EXTRACT(MONTH FROM mindate) as month,
  EXTRACT(YEAR FROM Mindate) as year,
  min(mindate),
        case
  when city = 'at-vienna' THEN 'AT-Vienna'
  WHEN city = 'ch-zurich' THEN 'CH-Zurich'
  WHEN city = 'ch-bern' THEN 'CH-Bern'
  WHEN city = 'ch-geneva' THEN 'CH-Geneva'
  WHEN city = 'ch-basel' THEN 'CH-Basel'
  WHEN city = 'ch-lausanne' then 'CH-Lausanne'
  WHEN city = 'ch-lucerne' then 'CH-Lucerne'
  when city = 'ch-geneve' then 'CH-Geneva'
  when city = 'de-berlin' THEN 'DE-Berlin'
  when city = 'ch-stgallen' THEN 'CH-St.Gallen'
  when city = 'de-bonn' THEN 'DE-Bonn'
  WHEN city = 'de-cologne' THEN 'DE-Cologne'
  WHEN city = 'de-dortmund' THEN 'DE-Dortmund'
  WHEN city = 'de-duisburg' THEN 'DE-Duisburg'
  WHEN city = 'de-dusseldorf' THEN 'DE-Dusseldorf'
  WHEN city = 'de-essen' THEN 'DE-Essen'
  WHEN city = 'de-frankfurt' THEN 'DE-Frankfurt am Main'
  WHEN city = 'de-hamburg' THEN 'DE-Hamburg'
  WHEN city = 'de-mainz' THEN 'DE-Mainz'
  WHEN city = 'de-manheim' THEN 'DE-Mannheim'
  WHEN city = 'de-munich' THEN 'DE-Munich'
  WHEN city = 'de-nuremberg' THEN 'DE-Nuremberg'
  WHEN city = 'de-stuttgart' THEN 'DE-Stuttgart'
  WHEN city = 'nl-amsterdam' THEN 'NL-Amsterdam'
  WHEN city = 'nl-hague' THEN 'NL-The Hague'
  ELSE 'Other'
  END as citya,
  'M3 RR Recurrent' as kpi,
  'All' as channel,
  SUM(CAST(returning_customer as decimal))/SUM(total_cohort) as return_rate
FROM
  bi.customer_cohorts_recurrent
WhERE
  returning_month = '3'
GROUP BY
  month,
  year,
  city;