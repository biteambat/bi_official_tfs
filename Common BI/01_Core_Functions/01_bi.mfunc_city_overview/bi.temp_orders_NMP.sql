
DROP TABLE IF EXISTS bi.temp_orders_NMP;
  CREATE TABLE bi.temp_orders_NMP AS

    SELECT
      created_at::date as date,
      event_name,
      (order_Json->>'Order_Start__c') as orderdate,
      order_Json->>'Locale__c' as Locale__c,
      order_Json->>'Order_Id__c' as Order_id,
      order_Json->>'Contact__c' as customer_id,
      order_Json->>'Recurrency__c' as recurrency

    FROM
      events.sodium

    WHERE
      (event_name in ('Order Event:CANCELLED-NO-MANPOWER'))
      and created_at >= '2018-01-01'
      and order_Json->>'Recurrency__c' > '0'

  ;