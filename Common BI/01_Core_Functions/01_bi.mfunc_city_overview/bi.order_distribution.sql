
DROP TABLE IF EXISTS bi.order_distribution;
CREATE TABLE bi.order_distribution as 
SELECT
  professional__c,
  to_char(effectivedate::date,'YYYY-MM') as Month,
    SUM(CASE WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and EXTRACT(HOUR FROM Order_End__c+ Interval '2 hours') < 13 THEN Order_Duration__c
        WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and extract(hour from order_end__c+ Interval '2 hours') > 13 THEN 13 - EXTRACT(HOUR FROM order_Start__c+ Interval '2 hours') ELSE 0 END) as Working_Hours_Morning,
        SUM(ORder_Duration__c) as total_hours
FROM
  bi.orders
WHERE
  test__c = '0'
  and status = 'INVOICED'
  AND effectivedate >= '2018-01-01'
GROUP BY
  professional__c,
  month;