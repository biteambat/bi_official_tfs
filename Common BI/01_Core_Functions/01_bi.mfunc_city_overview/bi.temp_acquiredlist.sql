
DROP TABLE IF EXISTS bi.temp_acquiredlist;
CREATE TABLE bi.temp_acquiredlist AS

  SELECT DISTINCT

    customer_id__c

  FROM bi.orders 

  WHERE test__c = '0'
    AND acquisition_new_customer__c = '1'
    -- AND order_type = '1'
    AND status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
    AND effectivedate >= '2018-01-01'

;