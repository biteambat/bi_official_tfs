
DROP TABLE IF EXISTS bi.gpm_city_over_time;
CREATE TABLE bi.gpm_city_over_time as   
SELECT
  year_month,
  delivery_area,
  min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
  SUM(GP) as GP,
  SUM(b2c_gp) as b2c_gp,
  SUM(b2b_gp) as b2b_gp
FROM
  bi.gpm_city
GROUP BY
  year_month,
  delivery_area;

INSERT INTO bi.gpm_city_over_time 
SELECT
  year_month,
  'DE',
  min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
  SUM(GP) as GP,
  SUM(b2c_gp) as b2c_gp,
  SUM(b2b_gp) as b2b_gp
FROM
  bi.gpm_city
WHERE
  delivery_area like 'de%'
GROUP BY
  year_month;

  
INSERT INTO bi.gpm_city_over_time 
SELECT
  year_month,
  'NL',
  min(mindate) as date,
    sum(revenue) as revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
  SUM(GP) as GP,
  SUM(b2c_gp) as b2c_gp,
  SUM(b2b_gp) as b2b_gp
FROM
  bi.gpm_city
WHERE
  delivery_area like 'nl%'
GROUP BY
  year_month;