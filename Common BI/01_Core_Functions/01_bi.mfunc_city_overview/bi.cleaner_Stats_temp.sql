
DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
  t1.sfid as professional__c,
  t1.delivery_areas__c as delivery_areas,
  t1.hr_contract_start__c::date as contract_start,
  t1.hr_contract_end__c::date as contract_end,
  t1.hr_contract_weekly_hours_min__c as weekly_hours
FROM
  Salesforce.Account t1
    
JOIN

   Salesforce.Account t2
   
ON

   (t2.sfid = t1.parentid)

WHERE
  (t1.type__c like '%cleaning-b2c%' or t1.type__c like '%cleaning-b2b%')
  and (t2.name like '%BAT%' or t2.name like '%BOOK%') 
  and  t1.test__c = '0'
  and (t1.status__c in ('ACTIVE','BETA') OR t1.sfid = '0010J00001n8r9d')
GROUP BY
  t1.sfid,
  t1.delivery_areas__c,
  t1.hr_contract_start__c,
  t1.hr_contract_end__c,
  t1.hr_contract_weekly_hours_min__c;