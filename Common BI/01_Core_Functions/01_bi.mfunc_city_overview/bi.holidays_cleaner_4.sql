
DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
  EXTRACT(WEEK FROM Date) as week,
  account__c,
  SUM(holiday_flag) as holiday,
  SUM(sickness_flag) as sickness
FROM
  bi.holidays_cleaner_3
GROUP BY
  EXTRACT(WEEK FROM Date),
  account__c;