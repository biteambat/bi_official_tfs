
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
  t1.*,
  CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
  CASE WHEN sickness is null then 0 ELSE sickness END as sickness
FROM
  bi.gpm_per_cleaner_v2 t1
LEFT JOIN
   bi.holidays_cleaner_4 t2
ON
  (t1.professional__c = t2.account__c and EXTRACT(WEEK FROM t1.mindate) = t2.week);