
DROP TABLE IF EXISTS bi.evolution_cities;
CREATE TABLE bi.evolution_cities AS


SELECT

  t1.*,
  t2.average_last_4weeks,
  t2.revenue_week_before,
  t2.revenue_last_week,
  'cleaning-b2c'::text as type

FROM


  (SELECT
  
    to_char(o.effectivedate,'YYYY-MM') as Year_Month,
    EXTRACT(YEAR FROM o.effectivedate) as year,
    EXTRACT(MONTH FROM o.effectivedate) as month,
    o.polygon,
    SUM(o.gmv_eur_net)/4.3 as avg_revenue_net_per_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2c'
    AND o.effectivedate > '2018-01-01'
    -- AND EXTRACT(MONTH FROM o.effectivedate) < EXTRACT(MONTH FROM current_date)
    -- AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
    
  GROUP BY
  
    Year_Month,
    year,
    month,
    o.polygon
    
  ORDER BY
  
    year desc,
    month desc,
    o.polygon) t1
    
LEFT JOIN

  (SELECT

    o.polygon,

    --------------------------- THIS PART HAS TO BE USED FOR JANUARY

    -- (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 51 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 52 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 3) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    -- SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    -- SUM(CASE WHEN (EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date) - 1) AND (EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date))) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week

     --------------------------- THIS PART HAS TO BE USED ALL THE TIME EXCEPT FOR JANUARY

    (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 5) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 4) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 3) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 1) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2c'
    AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date) 
    AND o.effectivedate >= '2018-01-01'
    -- OR EXTRACT(YEAR FROM o.effectivedate) = (EXTRACT(YEAR FROM current_date) - 1))
    
  GROUP BY
  
    o.polygon
    
  ORDER BY
  
    o.polygon) t2
    
ON 

  t1.polygon = t2.polygon;
  
INSERT INTO bi.evolution_cities 
  
SELECT

  t1.*,
  t2.average_last_4weeks,
  t2.revenue_week_before,
  t2.revenue_last_week,
  'cleaning-b2b'::text as type

FROM


  (SELECT
  
    to_char(o.effectivedate,'YYYY-MM') as Year_Month,
    EXTRACT(YEAR FROM o.effectivedate) as year,
    EXTRACT(MONTH FROM o.effectivedate) as month,
    o.polygon,
    SUM(o.gmv_eur_net)/4.3 as avg_revenue_net_per_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2b'
    AND o.effectivedate > '2018-01-01'
    -- AND EXTRACT(MONTH FROM o.effectivedate) < EXTRACT(MONTH FROM current_date)
    -- AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
    
  GROUP BY
  
    Year_Month,
    year,
    month,
    o.polygon
    
  ORDER BY
  
    year desc,
    month desc,
    o.polygon) t1
    
LEFT JOIN

  (SELECT

    o.polygon,

    --------------------------- THIS PART HAS TO BE USED FOR JANUARY

    -- (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 51 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 52 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 3) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    -- SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    -- SUM(CASE WHEN (EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date) - 1) AND (EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date))) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week

     --------------------------- THIS PART HAS TO BE USED ALL THE TIME EXCEPT FOR JANUARY

    (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 5) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 4) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 3) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 1) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2b'
    AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date) 
    -- OR EXTRACT(YEAR FROM o.effectivedate) = (EXTRACT(YEAR FROM current_date) - 1))
    
    
  GROUP BY
  
    o.polygon
    
  ORDER BY

    o.polygon) t2
    
ON 

  t1.polygon = t2.polygon;  
  
  
----------------------------------------------------------------------------------


INSERT INTO bi.evolution_cities

SELECT

  t1.*,
  t2.average_last_4weeks,
  t2.revenue_week_before,
  t2.revenue_last_week,
  'cleaning-b2c'::text as type

FROM


  (SELECT
  
    to_char(o.effectivedate,'YYYY-MM') as Year_Month,
    EXTRACT(YEAR FROM o.effectivedate) as year,
    EXTRACT(MONTH FROM o.effectivedate) as month,
    UPPER(LEFT(o.polygon, 2)) as polygon,
    SUM(o.gmv_eur_net)/4.3 as avg_revenue_net_per_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2c'
    AND o.effectivedate >= '2018-01-01'
    -- AND EXTRACT(MONTH FROM o.effectivedate) < EXTRACT(MONTH FROM current_date)
    -- AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
    
  GROUP BY
  
    Year_Month,
    year,
    month,
    UPPER(LEFT(o.polygon, 2))
    
  ORDER BY
  
    year desc,
    month desc,
    UPPER(LEFT(o.polygon, 2))) t1
    
LEFT JOIN

  (SELECT
  
     UPPER(LEFT(o.polygon, 2)) as polygon,

    --------------------------- THIS PART HAS TO BE USED FOR JANUARY

    -- (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 51 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 52 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 3) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    -- SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    -- SUM(CASE WHEN (EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date) - 1) AND (EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date))) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week

     --------------------------- THIS PART HAS TO BE USED ALL THE TIME EXCEPT FOR JANUARY

    (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 5) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 4) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 3) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 1) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2c'
    AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date) 
    AND o.effectivedate >= '2018-01-01'
    -- OR EXTRACT(YEAR FROM o.effectivedate) = (EXTRACT(YEAR FROM current_date) - 1))
    
  GROUP BY
  
    UPPER(LEFT(o.polygon, 2))
    
  ORDER BY
  
    UPPER(LEFT(o.polygon, 2))) t2
    
ON 

  UPPER(LEFT(t1.polygon, 2)) = UPPER(LEFT(t2.polygon, 2));
  
  
  
INSERT INTO bi.evolution_cities 
  
SELECT

  t1.*,
  t2.average_last_4weeks,
  t2.revenue_week_before,
  t2.revenue_last_week,
  'cleaning-b2b'::text as type

FROM


  (SELECT
  
    to_char(o.effectivedate,'YYYY-MM') as Year_Month,
    EXTRACT(YEAR FROM o.effectivedate) as year,
    EXTRACT(MONTH FROM o.effectivedate) as month,
    UPPER(LEFT(o.polygon, 2)) as polygon,
    SUM(o.gmv_eur_net)/4.3 as avg_revenue_net_per_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2b'
    AND o.effectivedate >= '2018-01-01'
    -- AND EXTRACT(MONTH FROM o.effectivedate) < EXTRACT(MONTH FROM current_date)
    -- AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
    
  GROUP BY
  
    Year_Month,
    year,
    month,
    UPPER(LEFT(o.polygon, 2))
    
  ORDER BY
  
    year desc,
    month desc,
    UPPER(LEFT(o.polygon, 2))) t1
    
LEFT JOIN

  (SELECT

    UPPER(LEFT(o.polygon, 2)) as polygon,

    --------------------------- THIS PART HAS TO BE USED FOR JANUARY

    -- (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 51 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = 52 THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END)
    -- + SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 3) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    -- SUM(CASE WHEN ((EXTRACT(WEEK FROM o.effectivedate)) = (EXTRACT(WEEK FROM current_date) - 2) AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    -- SUM(CASE WHEN (EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(WEEK FROM current_date) - 1) AND (EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date))) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week

     --------------------------- THIS PART HAS TO BE USED ALL THE TIME EXCEPT FOR JANUARY

    (SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 5) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 4) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 3) THEN o.gmv_eur_net ELSE 0 END)
    + SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END))/4 as average_last_4weeks,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 2) THEN o.gmv_eur_net ELSE 0 END) as revenue_week_before,
    SUM(CASE WHEN EXTRACT(WEEK FROM o.effectivedate) = (EXTRACT(week FROM current_date) - 1) THEN o.gmv_eur_net ELSE 0 END) as revenue_last_week
  
  FROM
  
    bi.orders o
    
    
  WHERE
  
    o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
    AND o.type = 'cleaning-b2b'
    AND EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date) 
    AND o.effectivedate >= '2018-01-01'
    -- OR EXTRACT(YEAR FROM o.effectivedate) = (EXTRACT(YEAR FROM current_date) - 1))

  GROUP BY
  
    UPPER(LEFT(o.polygon, 2))
    
  ORDER BY

    UPPER(LEFT(o.polygon, 2))) t2
    
ON 

  UPPER(LEFT(t1.polygon, 2)) = UPPER(LEFT(t2.polygon, 2));