
DROP TABLE IF EXISTS bi.gpm_per_cleaner_per_week;
CREATE TABLE bi.gpm_per_cleaner_per_week as 
select
    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as absence_hours,
  (weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,

  (CASE WHEN MIN(mindate) <= '2016-12-31'
  THEN
  SUM(CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > weekly_hours THEN (((weekly_hours/5)*(sickness+holiday))+worked_hours)*12.5 ELSE weekly_hours*12.5 END)
  ELSE
  SUM(CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > weekly_hours THEN (((weekly_hours/5)*(sickness+holiday))+worked_hours)*12.5 ELSE weekly_hours*12.5 END)
  END)
  as salary_payed,

  SUM(GMV) as GMV,
  SUM(GMV/1.19) as Revenue,
  contract_end-mindate as diff_week_contractend
from
     bi.gpm_per_cleaner_v2_2
where
    LEFT(delivery_area,2) = 'de'
    and days_worked > 0
GROUP BY
    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
   diff_week_contractend;
  

  
INSERT INTO bi.gpm_per_cleaner_per_week 
SELECT

    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as absence_hours,
  (weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
   CASE WHEN ((weekly_hours/5)*(sickness))+worked_hours > weekly_hours THEN (((weekly_hours/5)*(sickness+holiday))+worked_hours)*14.73 ELSE weekly_hours*14.73 END as salary_payed,
  SUM(GMV) as GMV,
  SUM(GMV/1.06) as Revenue,
  contract_end-mindate as diff_week_contractend
  
FROM

     bi.gpm_per_cleaner_v2_2
     
WHERE

    LEFT(delivery_area,2) = 'nl'
    AND days_worked > 0
    
GROUP BY

    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    diff_week_contractend; 


INSERT INTO bi.gpm_per_cleaner_per_week 
SELECT

    Year_Week,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    sickness,
    min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(sickness+holiday) as absence_hours,
    (weekly_hours/5)*(sickness+holiday)+worked_hours as total_hours,
    CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
     THEN
      CASE WHEN worked_hours < 1 THEN 0
         ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*25.03 ELSE worked_hours*24.45 END)
         END
    WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
    THEN   
      CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END)
         ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*25.03 ELSE worked_hours*24.45 END)
         END
    WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
    THEN  
      CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*25.03 ELSE 2*24.45 END)
         ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*25.03 ELSE worked_hours*24.45 END)
         END
    WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
    THEN  
      CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*25.03 ELSE 3*24.45 END)
         ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*25.03 ELSE worked_hours*24.45 END)
         END
    WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
    THEN  
      CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*25.03 ELSE 4*24.45 END)
         ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*25.03 ELSE worked_hours*24.45 END)
         END
    ELSE 0
    END as salary_payed,
    SUM(GMV) as GMV,
    SUM(GMV/1.06) as Revenue,
    contract_end-mindate as diff_week_contractend
  
FROM

     bi.gpm_per_cleaner_v2_2
     
WHERE

    LEFT(delivery_area,2) = 'ch'
    AND days_worked > 0
    
GROUP BY

    Year_Week,
    professional__c,
    weekly_hours,
    worked_hours,
    contract_start,
    contract_end,
    weekly_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    diff_week_contractend; 