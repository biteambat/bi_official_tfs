
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
    GREATEST(concat(to_char(mindate,'YYYY-MM'),'-01')::date,contract_start) as calc_start,
    case
    when to_char(mindate,'YYYY-MM') = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(to_char(mindate,'YYYY-MM'),'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
    when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN   cast(LEAST(now(), contract_end, (date_trunc('MONTH', mindate::date) + INTERVAL '1 MONTH - 1 day')::date) as date) - GREATEST(concat(to_char(mindate,'YYYY-MM'),'-02')::date,contract_start) 
    else  cast(LEAST(now(), contract_end, (date_trunc('MONTH', mindate::date) + INTERVAL '1 MONTH - 1 day')::date) as date) - GREATEST(concat(to_char(mindate,'YYYY-MM'),'-01')::date,contract_start) + 1
    END AS days_worked
from (
SELECT  
   EXTRACT(WEEK FROM Date) as Year_Week,
   min(date) as mindate,
  professional__c,
  delivery_areas as delivery_area,
  7 as days_of_month,
  contract_start,
  contract_end,
  SUM(hours) as worked_hours,
  SUM(GMV) as GMV,
  MAX(Weekly_hours) as weekly_hours
FROM
  bi.gmp_per_cleaner_v1 t1
WHERE
  date >= '2018-01-01'
GROUP BY
   Year_Week,
  professional__c,
  contract_start,
  contract_end,
  delivery_area ) xx;