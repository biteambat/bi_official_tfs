
DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
  date,
  account__c,
  MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
  MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
  bi.holidays_cleaner_2
WHERE
  weekday != '0'
GROUP BY
  date,
  account__c;