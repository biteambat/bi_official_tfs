
DROP TABLE IF EXISTS bi.behavior_cities;
CREATE TABLE bi.behavior_cities as

-------------------------------------------- Holidays

SELECT

  EXTRACT(YEAR FROM b.date::date) as year,
    EXTRACT(week FROM b.date::date) as week,
  b.date as date,
  a.delivery_areas__c as polygon,
  'Holidays'::text as kpi,
  SUM(b.holiday_flag) as value
  
FROM

  bi.holidays_cleaner_3 b
  
LEFT JOIN

  salesforce.account a  
  
ON

  b.account__c = a.sfid
  
WHERE

  date <= current_date  

GROUP BY

  b.date,
  a.delivery_areas__c
  
ORDER BY

  b.date desc,
  a.delivery_areas__c;

-------------------------------------------- Holidays per country

INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM b.date::date) as year,
    EXTRACT(week FROM b.date::date) as week,
  b.date as date,
  UPPER(LEFT(a.delivery_areas__c, 2)) as polygon,
  'Holidays'::text as kpi,
  SUM(b.holiday_flag) as value
  
FROM

  bi.holidays_cleaner_3 b
  
LEFT JOIN

  salesforce.account a  
  
ON

  b.account__c = a.sfid
  
WHERE

  date <= current_date  

GROUP BY

  b.date,
  UPPER(LEFT(a.delivery_areas__c, 2))
  
ORDER BY

  b.date desc;
  
--------------------------------------------- Sickness country

INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM b.date::date) as year,
    EXTRACT(week FROM b.date::date) as week,
  b.date as date,
  UPPER(LEFT(a.delivery_areas__c, 2)) as polygon,
  'Sickness'::text as kpi,
  SUM(b.sickness_flag) as value
  
  
FROM

  bi.holidays_cleaner_3 b
  
LEFT JOIN

  salesforce.account a  
  
ON

  b.account__c = a.sfid
  
WHERE

  date <= current_date  

GROUP BY

  b.date,
  UPPER(LEFT(a.delivery_areas__c, 2))
  
ORDER BY

  b.date desc;

--------------------------------------------- Sickness

INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM b.date::date) as year,
    EXTRACT(week FROM b.date::date) as week,
  b.date as date,
  a.delivery_areas__c as polygon,
  'Sickness'::text as kpi,
  SUM(b.sickness_flag) as value
  
  
FROM

  bi.holidays_cleaner_3 b
  
LEFT JOIN

  salesforce.account a  
  
ON

  b.account__c = a.sfid
  
WHERE

  date <= current_date  

GROUP BY

  b.date,
  a.delivery_areas__c
  
ORDER BY

  b.date desc;

--------------------------------------------- UR%

INSERT INTO bi.behavior_cities  
  
SELECT

  EXTRACT(YEAR FROM a.mindate::date) as year,
    EXTRACT(week FROM a.mindate::date) as week,
  a.mindate as date,
  a.delivery_area as polygon,
  'UR%'::text as kpi,
  SUM(CASE WHEN a.worked_hours < a.weekly_hours THEN a.worked_hours ELSE a.weekly_hours END)/SUM(a.weekly_hours) as value
  
FROM

  bi.gpm_weekly_cleaner a
  
WHERE

  a.mindate < current_date  
    
GROUP BY

  year,
  week,
  a.mindate,
  a.delivery_area,
  kpi
  
ORDER BY

  a.mindate desc,
  a.delivery_area;  

--------------------------------------------- UR% countries

INSERT INTO bi.behavior_cities  
  
SELECT

  EXTRACT(YEAR FROM a.mindate::date) as year,
    EXTRACT(week FROM a.mindate::date) as week,
  a.mindate as date,
  UPPER(LEFT(a.delivery_area, 2)) as polygon,
  'UR%'::text as kpi,
  SUM(CASE WHEN a.worked_hours < a.weekly_hours THEN a.worked_hours ELSE a.weekly_hours END)/SUM(a.weekly_hours) as value
  
FROM

  bi.gpm_weekly_cleaner a
  
WHERE

  a.mindate < current_date  
    
GROUP BY

  year,
  week,
  a.mindate,
  UPPER(LEFT(a.delivery_area, 2)),
  kpi
  
ORDER BY

  a.mindate desc;
    
--------------------------------------------- COP Polygons DE & NL

INSERT INTO bi.behavior_cities  

SELECT

  EXTRACT(YEAR FROM max_date::date) as Year,
  EXTRACT(week FROM max_date::date) as week,
  MIN(max_date::date) as date,
  delivery_areas__c as polygon,
  CAST('COP Cleaner' as text) as kpi,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as value

FROM

  (SELECT
    EXTRACT(WEEK FROM date) as week,
    MAX(Date) as max_date
  FROM
    (select i::date as date from generate_series('2016-01-01', 
    current_date::date, '1 day'::interval) i) as dateta
    GROUP BY
    week) as a,
  (SELECT
      a.*,
      CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
    FROM
      Salesforce.Account a
  JOIN
    Salesforce.Account t2
   ON
    (t2.sfid = a.parentid)
  WHERE 
    a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
      AND (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
    AND a.hr_contract_weekly_hours_min__c >= 11) as b
  
WHERE

  LEFT(b.locale__c,2) IN ('de','nl')
  AND a.max_date < current_date
  
GROUP BY

  year,
  week,
  max_date::date,
  delivery_areas__c
  
ORDER BY

  year desc,
  week desc,
  delivery_areas__c;

--------------------------------------------- COP Polygons DE & NL --- MJ

INSERT INTO bi.behavior_cities  

SELECT

  EXTRACT(YEAR FROM max_date::date) as Year,
  EXTRACT(week FROM max_date::date) as week,
  MIN(max_date::date) as date,
  delivery_areas__c as polygon,
  CAST('COP Cleaner MJ' as text) as kpi,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as value

FROM

  (SELECT
    EXTRACT(WEEK FROM date) as week,
    MAX(Date) as max_date
  FROM
    (select i::date as date from generate_series('2016-01-01', 
    current_date::date, '1 day'::interval) i) as dateta
    GROUP BY
    week) as a,
  (SELECT
      a.*,
      CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
    FROM
      Salesforce.Account a
  JOIN
    Salesforce.Account t2
   ON
    (t2.sfid = a.parentid)
  WHERE 
    a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
      AND (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
    AND a.hr_contract_weekly_hours_min__c < 11) as b
      
WHERE

  LEFT(b.locale__c,2) IN ('de')
  AND a.max_date < current_date
  
GROUP BY

  year,
  week,
  max_date::date,
  delivery_areas__c
  
ORDER BY

  year desc,
  week desc,
  delivery_areas__c;
    
------------------------------------------------------ COP DE & NL
  
INSERT INTO bi.behavior_cities
  
SELECT

  EXTRACT(YEAR FROM max_date::date) as Year,
  EXTRACT(week FROM max_date::date) as week,
  MIN(max_date::date) as date,
  UPPER(LEFT(delivery_areas__c, 2)) as polygon,
  CAST('COP Cleaner' as text) as kpi,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as value

FROM

  (SELECT
    EXTRACT(WEEK FROM date) as week,
    MAX(Date) as max_date
  FROM
    (select i::date as date from generate_series('2016-01-01', 
    current_date::date, '1 day'::interval) i) as dateta
    GROUP BY
    week) as a,
  (SELECT
      a.*,
      CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
    FROM
      Salesforce.Account a
  JOIN
    Salesforce.Account t2
   ON
    (t2.sfid = a.parentid)
  WHERE 
    a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
      AND (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
    AND a.hr_contract_weekly_hours_min__c >= 11) as b
  
WHERE

  LEFT(b.locale__c,2) IN ('de','nl')
  AND a.max_date < current_date
  
GROUP BY

  year,
  week,
  max_date::date,
  UPPER(LEFT(delivery_areas__c, 2))
  
ORDER BY

  year desc,
  week desc;  
  
------------------------------------------------------ COP DE & NL ---- MJ
  
INSERT INTO bi.behavior_cities
  
SELECT

  EXTRACT(YEAR FROM max_date::date) as Year,
  EXTRACT(week FROM max_date::date) as week,
  MIN(max_date::date) as date,
  UPPER(LEFT(delivery_areas__c, 2)) as polygon,
  CAST('COP Cleaner MJ' as text) as kpi,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date <= new_contract_end THEN sfid ELSE null END)) as value

FROM

  (SELECT
    EXTRACT(WEEK FROM date) as week,
    MAX(Date) as max_date
  FROM
    (select i::date as date from generate_series('2016-01-01', 
    current_date::date, '1 day'::interval) i) as dateta
    GROUP BY
    week) as a,
  (SELECT
      a.*,
      CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
    FROM
      Salesforce.Account a
  JOIN
    Salesforce.Account t2
   ON
    (t2.sfid = a.parentid)
  WHERE 
    a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
      AND (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')
    AND a.hr_contract_weekly_hours_min__c < 11) as b
  
WHERE

  LEFT(b.locale__c,2) IN ('de')
  AND a.max_date < current_date
  
GROUP BY

  year,
  week,
  max_date::date,
  UPPER(LEFT(delivery_areas__c, 2))
  
ORDER BY

  year desc,
  week desc;  


------------------------------------------------------ COP cities CH
  
INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM max_date::date) as year,
  EXTRACT(week FROM max_date::date) as week,
  MIN(max_date::date) as mindate,
  delivery_areas__c as citya,
  CAST('COP Cleaner' as text) as kpi,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and (max_date < new_contract_end or hr_contract_end__c is null) THEN sfid ELSE null END)) as value

FROM

  (SELECT
    EXTRACT(WEEK FROM date) as week,
    MAX(Date) as max_date
  FROM
    (select i::date as date from generate_series('2016-01-01', 
    current_date::date, '1 day'::interval) i) as dateta
  GROUP BY 
    week
    ) as a,
  
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
      ) as b
  
WHERE

  LEFT(b.delivery_areas__c,2) = 'ch'
  AND a.max_date < current_date
  
GROUP BY

  year,
  week,
  max_date::date,
  delivery_areas__c
  
ORDER BY

  year desc,
  week desc,
  max_date desc;

------------------------------------------------------ COP CH
  
INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM max_date::date) as year,
  EXTRACT(week FROM max_date::date) as week,
  MIN(max_date::date) as mindate,
  UPPER(LEFT(delivery_areas__c, 2)) as citya,
  CAST('COP Cleaner' as text) as kpi,
  COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and (max_date < new_contract_end or hr_contract_end__c is null) THEN sfid ELSE null END)) as value

FROM

  (SELECT
    EXTRACT(WEEK FROM date) as week,
     MAX(Date) as max_date
  FROM
    (select i::date as date from generate_series('2016-01-01', 
    current_date::date, '1 day'::interval) i) as dateta
  GROUP BY
    week
    ) as a,
  
  (SELECT
    a.*,
    CASE WHEN a.hr_contract_end__c IS NULL THEN '2030-12-31' ELSE a.hr_contract_end__c END as new_contract_end
  FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
      ) as b
  
WHERE

  LEFT(b.delivery_areas__c,2) = 'ch'
  AND a.max_date < current_date
  
GROUP BY

  year,
  week,
  max_date::date,
  UPPER(LEFT(delivery_areas__c, 2))
  
ORDER BY

  year desc,
  week desc,
  max_date desc;
    
---------------------------------------------------- Hours per cleaner

INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM effectivedate::date) as year,
    EXTRACT(week FROM effectivedate::date) as week,
  MIN(effectivedate::date) as date,
  t2.polygon as polygon,
  CAST('Hours per Cleaner BAT' as text) as kpi,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c))::numeric as value
  
FROM

  (SELECT
  
    t1.*,
    t2.name as subcon
    
  FROM
  
    Salesforce.Account t1
   
  JOIN
  
    Salesforce.Account t2
    
  ON
     
    (t2.sfid = t1.parentid)
  
  
  WHERE 
  
    t1.status__c NOT IN ('SUSPENDED') AND t1.test__c = '0' AND t1.name not like '%test%'
    AND (t1.type__c LIKE 'cleaning-b2c' OR (t1.type__c LIKE '%cleaning-b2c;cleaning-b2b%') OR t1.type__c LIKE 'cleaning-b2b')
    AND t2.name LIKE '%BAT Business Services GmbH%') t1
    
JOIN 

  bi.orders t2
  
ON

  (t1.sfid = t2.professional__c)
  
WHERE

  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  AND t2.effectivedate < current_date
  AND t2.effectivedate >= '2018-01-01'

GROUP BY

  WEEK,
  Year,
  t2.polygon
  
ORDER BY

  year desc,
  week desc,
  t2.polygon;
  
---------------------------------------------------- Hours per cleaner countries

INSERT INTO bi.behavior_cities

SELECT

  EXTRACT(YEAR FROM effectivedate::date) as year,
    EXTRACT(week FROM effectivedate::date) as week,
  MIN(effectivedate::date) as date,
  UPPER(LEFT(t2.polygon, 2)) as polygon,
  CAST('Hours per Cleaner BAT' as text) as kpi,
  SUM(Order_Duration__c)/COUNT(DISTINCT(Professional__c))::numeric as value
  
FROM

  (SELECT
  
    t1.*,
    t2.name as subcon
    
  FROM
  
    Salesforce.Account t1
   
  JOIN
  
    Salesforce.Account t2
    
  ON
     
    (t2.sfid = t1.parentid)
  
  
  WHERE 
  
    t1.status__c NOT IN ('SUSPENDED') AND t1.test__c = '0' AND t1.name not like '%test%'
    AND (t1.type__c LIKE 'cleaning-b2c' OR (t1.type__c LIKE '%cleaning-b2c;cleaning-b2b%') OR t1.type__c LIKE 'cleaning-b2b')
    AND t2.name LIKE '%BAT Business Services GmbH%') t1
    
JOIN 

  bi.orders t2
  
ON

  (t1.sfid = t2.professional__c)
  
WHERE

  status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
  AND LEFT(t2.locale__C,2) in ('de','nl', 'ch', 'at')
  AND t2.effectivedate < current_date
  AND t2.effectivedate >= '2018-01-01'

GROUP BY

  WEEK,
  Year,
  UPPER(LEFT(t2.polygon, 2))
  
ORDER BY

  year desc,
  week desc;