
DROP TABLE IF EXISTS bi.cleaner_availability_morning;
CREATE TABLE bi.cleaner_availability_morning as
SELECT
  sfid,
  CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) END as availability_monday,
  CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) < 0  THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) END as availability_tuesday,
  CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) < 0 THEN 0 ELSE  (13-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) END as availability_wednesday,
  CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) END as availability_thursday,
  CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) END as availability_friday,
  CASE WHEN (13-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) is null or (13-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) < 0 THEN 0 ELSE (13-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) END as availability_saturday,
  CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_monday__c,'-',1) as time)) END as total_monday,
  CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_tuesday__c,'-',1) as time)) END as total_tuesday,
  CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_wednesday__c,'-',1) as time)) END as total_wednesday,
  CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_thursday__c,'-',1) as time)) END as total_thursday, 
  CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_friday__c,'-',1) as time)) END as total_friday,
  CASE WHEN (EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time))) is null THEN 0 ELSE  EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',2) as time))-EXTRACT(HOUR FROM CAST(split_part(availability_saturday__c,'-',1) as time)) END as total_saturday
FROM
  Salesforce.Account
WHERE
  (Status__c in ('ACTIVE','BETA') OR sfid = '0010J00001n8r9d')
  and type__c like '%cleaning-b2c%' or type__c like '%cleaning-b2b%';

DROP TABLE IF EXISTS bi.cleaner_ur_temp;
CREATE TABLE bi.cleaner_ur_temp as  
SELECT 
  sfid,
  SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as availability_morning,
  SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) as availability_total,
  CASE WHEN SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) = '0' THEN 0 ELSE CAST(SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as decimal)/SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) END as availability_Share
FROM
  bi.cleaner_availability_morning
GROUP BY
  sfid;