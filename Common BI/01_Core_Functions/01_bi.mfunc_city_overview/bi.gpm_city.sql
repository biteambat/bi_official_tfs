
DROP TABLE IF EXISTS bi.gpm_city;
CREATE TABLE bi.gpm_city as   
select
    year_month,
    delivery_area,
    min(mindate) as mindate,
    sum( Revenue) as Revenue,
    sum(b2b_revenue) as b2b_revenue,
    sum(b2c_revenue) as b2c_revenue,
  SUM(GP) as GP,
  SUM(b2c_gp) as b2c_gp,
  SUM(b2b_gp) as b2b_gp
FROM
  bi.gpm_per_cleaner_v3
GROUP BY
  year_month,
  delivery_area;