
DROP TABLE IF EXISTS bi.temp_badrate_orders;
  CREATE TABLE bi.temp_badrate_orders AS

    SELECT
      order_id__c as order_id,
      effectivedate::date as orderdate,
      order_start__c::date as date,
      customer_id__c as customer_id

    FROM
      bi.orders

    WHERE
      rating_service__c in ('1', '2', '3')
      and effectivedate::date >= '2018-01-01'
      and recurrency__c > '0'
      and test__c = '0'
      and status in ('INVOICED')

  ;