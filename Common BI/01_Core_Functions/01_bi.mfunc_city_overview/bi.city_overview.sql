
DROP TABLE IF EXISTS bi.city_overview;
CREATE TABLE bi.city_overview AS

  SELECT
    t1.*,
    t6.invoiced_acquisitions,
    t6.invoiced_gmv_recurrent_gross,
    t6.invoiced_gmv_gross,
    t6.invoiced_hours,
    t6.active_customers,
    t6.reactivated_customers,
    t6.orders_no_manpower,
    t6.divisor_nmp_calc,
    t6.orders_skipped,
    t6.divisor_skipped_calc,
    t2.all_acquisitions,
    t3.churned_customers,
    t4.total_costs,
    t5.weekly_contract_hours,
    t5.weekly_worked_hours,
    t5.weekly_revenue_generated,
    t5.weekly_salary_paid,
    t7.total_marketing_costs,
    all_acquisitions_marketing


  FROM 
    (SELECT
  polygon,
  Left(polygon,2) as locale,
  date,
  CAST('B2C' as varchar) as Unit
FROM
  bi.orders,
    (select i::date as date from generate_series('2016-06-01', 
  '2020-12-31', '1 day'::interval) i) b
WHERE
  effectivedate::date between current_date::date - Interval '60 days' and current_date::date
GROUP BY
  polygon,
  locale,
  date
UNION
SELECT
  polygon,
  Left(polygon,2) as locale,
  date,
  CAST('B2B' as varchar) as Unit
FROM
  bi.orders,
    (select i::date as date from generate_series('2016-06-01', 
  '2020-12-31', '1 day'::interval) i) b
WHERE
  effectivedate::date between current_date::date - Interval '60 days' and current_date::date
GROUP BY
  polygon,
  locale,
  date  ) as t1
  
  
  LEFT JOIN bi.temp_city_overview_1 t6
      ON t1.date = t6.date 
      AND t1.locale = t6.locale
      AND t1.polygon = t6.polygon
      and t1.unit = t6.unit

  LEFT JOIN bi.temp_city_overview_2 t2 
    ON t1.date = t2.date 
      AND t1.locale = t2.locale
      AND t1.polygon = t2.polygon
      and t1.unit = t2.unit

  LEFT JOIN bi.temp_city_overview_3 t3
    ON t1.date = t3.date 
      AND t1.locale = t3.locale
      AND t1.polygon = t3.polygon
      AND t1.unit = t3.unit

  LEFT JOIN bi.temp_city_overview_4 t4
    ON t1.date = t4.date 
      AND t1.locale = t4.locale
      AND t1.polygon = t4.polygon
      and t1.unit = t4.unit

  LEFT JOIN bi.temp_city_overview_5 t5
    ON t1.date = t5.date 
      AND t1.locale = t5.locale
      AND t1.polygon = t5.polygon
      and t1.unit = t5.unit
      
    LEFT JOIN bi.temp_city_overview_6 t7
    ON t1.date = t7.date 
      AND t1.locale = t7.locale
      AND t1.polygon = t7.polygon
      and t1.unit = t7.unit   


;