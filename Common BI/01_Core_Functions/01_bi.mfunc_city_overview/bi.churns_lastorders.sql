
DROP TABLE IF EXISTS bi.churns_lastorders;
  CREATE TABLE bi.churns_lastorders AS

    SELECT
      t1.date as churndate,
      t1.customer_id,
      t1.recurrency as recurrency_plan,
      CASE WHEN t2.error_note__c LIKE '%Customer blocked%' THEN 'System churn' ELSE 'Customer churn' END::text as churntype,
      COUNT(DISTINCT CASE WHEN t2.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL') THEN t2.sfid ELSE NULL END) as orders_l3m

    FROM
      bi.temp_orders_terminated t1

    LEFT JOIN
      bi.orders t2 ON t1.customer_id = t2.contact__c
        AND t2.effectivedate::date < t1.date
        AND t2.effectivedate::date >= (t1.date - interval '3 months')

    WHERE t1.ordertype = 'cleaning-b2c'

    GROUP BY 
      t1.date,
      t1.customer_id,
      t1.recurrency,
      CASE WHEN t2.error_note__c LIKE '%Customer blocked%' THEN 'System churn' ELSE 'Customer churn' END::text

    ORDER BY customer_id asc
  ;