
DROP TABLE IF EXISTS bi.temp_city_overview_6;
CREATE TABLE bi.temp_city_overview_6 as 
SELECT
  date,
  locale,
  polygon,
  CAST('B2C' as varchar) as unit,
  SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+youtube_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+newsletter_discount+seo_discount+seo_brand_discount+youtube_discount) as total_marketing_costs,
  sum(all_acq) as all_acquisitions_marketing
FROM
  bi.cpacalcpolygon 
GROUP BY
  date,
  polygon,
  locale;