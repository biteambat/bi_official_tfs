
DROP TABLE IF EXISTS bi.temp_city_overview_2;
CREATE TABLE bi.temp_city_overview_2 AS

  SELECT
    o.order_creation__c::date as date,
    LEFT(o.locale__c,2)::text as locale,
    CAST('B2C' as varchar) as unit,
    o.polygon::text as polygon,
    COUNT(DISTINCT CASE WHEN (o.acquisition_new_customer__c = '1') and o.acquisition_customer_creation__c::date = c.createddate::date THEN o.sfid ELSE NULL END)::numeric as all_acquisitions

  FROM bi.orders o
  JOIN Salesforce.Contact c
  ON (o.contact__c = c.sfid)

  WHERE o.test__c = '0'
    AND o.order_type = '1'
    AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')
    AND o.order_creation__c::date < current_date::date
    AND o.effectivedate >= '2018-01-01'

  GROUP BY o.order_creation__c::date, LEFT(o.locale__c,2), o.polygon

  ORDER BY date desc, locale asc, polygon asc

;