
DROP TABLE IF EXISTS bi.temp_orders_terminated_2;
  CREATE TABLE bi.temp_orders_terminated_2 AS

    SELECT

      t1.date,
      t1.event_name,
      t1.orderdate,
      t1.locale__c,
      t1.order_id,
      t1.customer_id,
      t1.recurrency,
      t2.polygon,
      t2.error_note__c

    FROM bi.temp_orders_terminated t1

      LEFT JOIN bi.orders t2 
        ON t1.order_id = t2.order_id__c

  ;