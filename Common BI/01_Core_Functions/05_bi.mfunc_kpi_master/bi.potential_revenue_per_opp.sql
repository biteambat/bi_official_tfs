
DROP TABLE IF EXISTS bi.potential_revenue_per_opp;
CREATE TABLE bi.potential_revenue_per_opp AS


-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 06/11/2018
-- Short Description: Calculation of the revenue per opportunity (average last 3 months of existence, only the months with invoiced amount > 0 taken into account. 
-- If the customer started in the current month we take the grand total or the amount for the potential)
		
SELECT

	t1.*,
	t1.last_month + t1.last_month_2 + t1.last_month_3 as invoiced_last_3months,
	(CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END) as divider,
	CASE WHEN 
			(CASE WHEN TO_CHAR(t1.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN t1.grand_total__c
				  ELSE CASE WHEN (CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END) > 0
				            THEN (t1.last_month + t1.last_month_2 + t1.last_month_3)/((CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END)) 
				  				ELSE t1.grand_total__c
				  				END
				  END) IS NULL THEN 0
		  ELSE
		  	 (CASE WHEN TO_CHAR(t1.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN t1.grand_total__c
				  ELSE CASE WHEN (CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END) > 0
				            THEN (t1.last_month + t1.last_month_2 + t1.last_month_3)/((CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END)) 
				  				ELSE t1.grand_total__c
				  				END
				  END)
		  END as potential,
	CASE WHEN t1.grand_total__c > 0 THEN
			(((CASE WHEN 
					(CASE WHEN TO_CHAR(t1.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN t1.grand_total__c
						  ELSE CASE WHEN (CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END) > 0
						            THEN (t1.last_month + t1.last_month_2 + t1.last_month_3)/((CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END)) 
						  				ELSE t1.grand_total__c
						  				END
						  END) IS NULL THEN 0
				  ELSE
				  	 (CASE WHEN TO_CHAR(t1.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN t1.grand_total__c
						  ELSE CASE WHEN (CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END) > 0
						            THEN (t1.last_month + t1.last_month_2 + t1.last_month_3)/((CASE WHEN t1.last_month > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_2 > 0 THEN 1 ELSE 0 END) + (CASE WHEN t1.last_month_3 > 0 THEN 1 ELSE 0 END)) 
						  				ELSE t1.grand_total__c
						  				END
						  END)
				  END)/t1.grand_total__c) - 1)
			ELSE 0
			END as difference
		  
FROM	
	
	(SELECT
	
		t0.country,
		t0.locale__c,
		t0.opportunityid,
		t0.delivery_area__c,	
		t0.date_start,
		t00.date_churn,
		t0.grand_total__c,
		CASE WHEN TO_CHAR(t0.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN 0
			  ELSE (CASE WHEN t00.date_churn IS NULL
					       THEN SUM(CASE WHEN TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR((current_date - 31), 'YYYY-MM') THEN o.amount__c ELSE 0 END)
					       ELSE SUM(CASE WHEN TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR((t00.date_churn), 'YYYY-MM') THEN o.amount__c ELSE 0 END)
					       END)/1.19
			  END as last_month,
		CASE WHEN TO_CHAR(t0.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN 0
			  ELSE (CASE WHEN t00.date_churn IS NULL
					       THEN SUM(CASE WHEN TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR((current_date - 62), 'YYYY-MM') THEN o.amount__c ELSE 0 END)
					       ELSE SUM(CASE WHEN TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR((t00.date_churn - 31), 'YYYY-MM') THEN o.amount__c ELSE 0 END)
					       END)/1.19
			  END as last_month_2,
		CASE WHEN TO_CHAR(t0.date_start, 'YYYY-MM') = TO_CHAR(current_date, 'YYYY-MM') THEN 0
				  ELSE (CASE WHEN t00.date_churn IS NULL
						       THEN SUM(CASE WHEN TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR((current_date - 93), 'YYYY-MM') THEN o.amount__c ELSE 0 END)
						       ELSE SUM(CASE WHEN TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR((t00.date_churn - 62), 'YYYY-MM') THEN o.amount__c ELSE 0 END)
						       END)/1.19
				  END as last_month_3
	
	FROM
	
		(SELECT
				
			LEFT(o.locale__c, 2) as country,
			o.locale__c,
			o.opportunityid,
			oo.delivery_area__c,
			CASE WHEN oo.grand_total__c IS NULL THEN oo.amount ELSE oo.grand_total__c END as grand_total__c,
			MIN(o.effectivedate) as date_start
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND o.type = 'cleaning-b2b'
			AND o.professional__c IS NOT NULL
			AND o.test__c IS FALSE
			
		GROUP BY
		
			o.opportunityid,
			CASE WHEN oo.grand_total__c IS NULL THEN oo.amount ELSE oo.grand_total__c END,
			o.locale__c,
			oo.delivery_area__c,
			LEFT(o.locale__c, 2)) as t0
			
	LEFT JOIN
	
		(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
								         -- It's the last day on which they are making money
			LEFT(o.locale__c, 2) as country,
			o.opportunityid,
			'last_order' as type_date,
			MAX(o.effectivedate) as date_churn
			
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.status__c IN ('RESIGNED', 'CANCELLED')
			AND o.type = 'cleaning-b2b'
			AND o.professional__c IS NOT NULL
			AND o.test__c IS FALSE
			AND oo.test__c IS FALSE
		
		GROUP BY
		
			LEFT(o.locale__c, 2),
			type_date,
			o.opportunityid) as t00
			
	ON
	
		t0.opportunityid = t00.opportunityid
			
	LEFT JOIN
	
		salesforce.invoice__c o
		
	ON
	
		t0.opportunityid = o.opportunity__c
		
	GROUP BY
	
		t0.country,
		t0.locale__c,
		t0.opportunityid,
		t0.delivery_area__c,
		t0.grand_total__c,
		t0.date_start,
		t00.date_churn) as t1
		
WHERE

	t1.opportunityid IS NOT NULL
	
ORDER BY

	t1.date_start desc;