
DROP TABLE IF EXISTS bi.AllOrders2_tableaucohorts;
CREATE TABLE bi.AllOrders2_tableaucohorts as 
SELECT
 CAST(acquisition_customer_creation__c as date) as acquisition_date,
 Contact__c,
 Order_Id__c,
 CAST(Order_Creation__c as Date) as creation_date,
 Recurrency__c,
 status,
 marketing_channel,
 LEFT(Locale__c,2)
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and order_type = '1'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
ORDER BY
  Contact__c,
  CAST(Order_Creation__c as Date);
 
CREATE INDEX IDX141 ON bi.AllOrders2_tableaucohorts(contact__c);