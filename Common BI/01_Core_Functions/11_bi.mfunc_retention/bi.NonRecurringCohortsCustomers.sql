
DROP TABLE IF EXISTS bi.NonRecurringCohortsCustomers;
CREATE TABLE bi.NonRecurringCohortsCustomers (acquisition_date varchar(20) DEFAULT NULL,  OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,   creation_date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, acquisition_channel varchar(200) default null, locale varchar(20) default null, voucher__c varchar(255) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.NonRecurringCohortsCustomers
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Order_Creation__c as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Order_Creation__c as Date) as creation_date,
  Recurrency__c,
  status,
  marketing_channel,
  LEFT(locale__c,2),
  voucher__c
FROM
  bi.orders  t1
WHERE
  t1.test__c = '0'
  and order_type = '1'
  and (Recurrency__c is null or Recurrency__c = 0)
  and t1.Status in ('PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED')
ORDER BY
  Contact__c,
  Cast(Order_Creation__c as Date);