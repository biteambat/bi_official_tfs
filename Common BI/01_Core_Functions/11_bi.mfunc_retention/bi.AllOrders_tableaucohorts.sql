
DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
CREATE TABLE bi.AllOrders_tableaucohorts as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 Contact__c,
 Order_Id__c,
 CAST(effectivedate as Date) as start_date,
 Recurrency__c,
 status,
 order_duration__c,
 type,
 pph__c,
 polygon,
 marketing_channel,
 rating_professional__c,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as funnel
FROM
  bi.orders t1
WHERE
  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','CANCELLED NO MANPOWE','NOSHOW CUSTOMER','CANCELLED CUSTOMER SHORTTERM','CANCELLED PROFESSIONAL SHORTTERM','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and order_type = '1'
ORDER BY
  Contact__c,
  CAST(effectivedate as Date);
 
CREATE INDEX IDX10 ON bi.AllOrders_tableaucohorts(contact__c);

----------------------------------

DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts;
CREATE TABLE bi.AllOrders_tableaucohorts as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 contact__c,
 Order_Id__c,
 CAST(Effectivedate as Date) as Start_date,
 LEFT(t1.locale__c,2) as locale,
 t1.Recurrency__c,
 status,
 gmv_eur_net,
 order_duration__c,
 marketing_channel,
 type as ordertype,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
LEFT JOIN
  Salesforce.Opportunity t2
ON
  (t2.sfid = t1.opportunityid)
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','FULFILLED') 
  and order_type = '2'
  and contact__c is not null
ORDER BY
  contact__c,
  CAST(Effectivedate as Date);
 
CREATE INDEX IDX10 ON bi.AllOrders_tableaucohorts(contact__c);