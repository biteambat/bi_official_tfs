
DROP TABLE IF EXISTS bi.allCohortsCustomers_tableaucohorts;
CREATE TABLE bi.allCohortsCustomers_tableaucohorts(acquisition_date varchar(200) DEFAULT NULL,  city varchar(55) DEFAULT null, OrderNo SERIAL, contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,   creation_date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, order_duration__c integer default null, professional varchar default null, ordertype varchar default null, acquisition_channel varchar(200) default null, acquisition_voucher varchar (200), pph varchar(200) default null, rating varchar(50) default null, funnel varchar(50) default null, polygon varchar(50), PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.allCohortsCustomers_tableaucohorts(acquisition_date,city,OrderNo,contact__c,Order_Id__c,creation_date,Recurrency__c,status,order_duration__c,professional,ordertype,acquisition_channel,acquisition_voucher,pph,rating,funnel,polygon)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  contact__c,
  Order_Id__c,
  effectivedate::date as creation_date,
  Recurrency__c,
  status,
  order_duration__c,
  professional__c,
  type,
  marketing_channel,
  voucher__c,
  pph__c,
   rating_professional__c,
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as funnel,
  polygon
FROM
  bi.orders t1
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and contact__c is not null
  and order_type = '1'
ORDER BY
  contact__c,
  Cast(effectivedate as Date);