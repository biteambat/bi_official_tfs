
DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts_b2b;
CREATE TABLE bi.RecurringCustomer_tableaucohorts_b2b as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  sub_kpi,
  acquisition_channel,
  ordertype,
  locale,
  funnel as acquisition_funnel
FROM
  bi.RecurringCohortsCustomers_tableaucohorts_b2b
WHERE
  OrderNo = 1;