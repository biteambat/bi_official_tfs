
DROP TABLE IF EXISTS bi.customer_cohorts_overall_exp;
CREATE TABLE bi.customer_cohorts_overall_exp as 
SELECT
  first_order_month,
  order_month,
  min(order_date) as order_date,
  min(first_order_Date) as start_date,
  city,
  acquisition_channel,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
  bi.allCustomerCohort 
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  city,
  acquisition_channel;