
DROP TABLE IF EXISTS bi.saas_kpis;
CREATE TABLE bi.saas_kpis as 
SELECT
  LEFT(city,2) as locale,
  city as city,
  Order_Month as Month,
    CASE 
  WHEN Recurrency between 1 and 7 THEN CAST('7' as varchar)
  WHEN Recurrency between 8 and 14 THEN CAST('14' as varchar)
  WHEN Recurrency > 27 THEN CAST('28' as varchar)
  ELSE CAST(Recurrency as varchar) END as breakdown,
  CAST('New Recurrent Customer' as varchar) as kpi,
  SUM(CASE WHEN returning_month = 0 THEN Returning_Customer ELSE 0 END) as value
FROM
  bi.customer_cohorts_recurrent_aggr
GROUP BY
  locale,
  city,
  breakdown,
  Month;

INSERT INTO bi.saas_kpis  
SELECT
  LEFT(city,2) as locale,
  city as city,
  Order_Month as Month,
  CASE 
  WHEN Recurrency between 1 and 7 THEN '7'
  WHEN Recurrency between 8 and 14 THEN '14'
  WHEN Recurrency > 27 THEN '28'
  ELSE Recurrency END as breakdown,
  CAST('Total Recurrent Customer EOM' as varchar) as kpi,
  SUM(Returning_Customer) as value  
FROM
  bi.customer_cohorts_recurrent_aggr
GROUP BY
  locale,
  city,
  breakdown,
  Month;
  
INSERT INTO bi.saas_kpis  
SELECT
  LEFT(city,2) as locale,
  city as city,
  TO_CHAR(mindate::date + INTERVAL '1 Month','YYYY-MM') as Month,
    CASE 
  WHEN Recurrency between 1 and 7 THEN '7' 
  WHEN Recurrency between 8 and 14 THEN '14'
  WHEN Recurrency > 27 THEN '28'
  ELSE Recurrency END as breakdown,
  CAST('Total Recurrent Customer BOM' as varchar) as kpi,
  SUM(Returning_Customer) as value  
FROM
  bi.customer_cohorts_recurrent_aggr
GROUP BY
  locale,
  city,
  breakdown,
  Month;  


INSERT INTO bi.saas_kpis  
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
  '-' as breakdown,
  CAST('Gross Recurrent Revenue' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c > '0'
GROUP BY
  locale,
  polygon,
  breakdown,
  month;
  
INSERT INTO bi.saas_kpis
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Gross Recurrent Revenue 7d' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 1 and 7
GROUP BY
  locale,
  polygon,
  breakdown,
  month;

INSERT INTO bi.saas_kpis  
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Gross Recurrent Revenue 14d' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 8 and 14
GROUP BY
  locale,
  breakdown,
  polygon,
  month;

INSERT INTO bi.saas_kpis
SELECT
  LEFT(Locale__c,2) as locale,
   polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Gross Recurrent Revenue 28d' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 15 and 28
GROUP BY
  locale,
  polygon,
  breakdown,
  month;
  
INSERT INTO bi.saas_kpis  
SELECT
   LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
GROUP BY
  locale,
  breakdown,
  polygon,
  month;
  
INSERT INTO bi.saas_kpis
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer 7d' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 1 and 7
GROUP BY
  locale,
  polygon,
  breakdown,
  month;

INSERT INTO bi.saas_kpis  
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer 14d' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 8 and 14
GROUP BY
  locale,
  polygon,
  breakdown,
  month;

INSERT INTO bi.saas_kpis
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
    TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Recurrent Customer 28d' as varchar) as kpi,
  COUNT(DISTINCT(contact__c)) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c between 15 and 28
GROUP BY
  locale,
  polygon,
  breakdown,
  month;

INSERT INTO bi.saas_kpis  
SELECT
  LEFT(Locale__c,2) as locale,
  polygon,
  TO_CHAR(Effectivedate::date,'YYYY-MM') as Month,
    '-' as breakdown,
  CAST('Trial Revenues' as varchar) as kpi,
  SUM(GMV_eur) as value
FROM
  bi.orders
WHERE
  Order_type = '1'
  and status in ('INVOICED','NOSHOW PROFESSIONAL','NOSHOW CUSTOMER')
  and recurrency__c = '0'
GROUP BY
  locale,
  polygon,
  breakdown,
  month;