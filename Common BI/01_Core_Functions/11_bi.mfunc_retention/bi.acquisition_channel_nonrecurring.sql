
DROP TABLE IF EXISTS bi.acquisition_channel_nonrecurring;
CREATE TABLE bi.acquisition_channel_nonrecurring as 
SELECT
  Contact__c,
  acquisition_channel,
  voucher__c as acquisition_voucher,
  locale
FROM
    bi.NonRecurringCohortsCustomers
WHERE
  OrderNo = '1';