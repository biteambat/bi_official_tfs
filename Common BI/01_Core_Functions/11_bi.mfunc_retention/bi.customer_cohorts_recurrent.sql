
DROP TABLE IF EXISTS bi.customer_cohorts_recurrent;
CREATE TABLE bi.customer_cohorts_recurrent as   
SELECT
  a.first_order_month as cohort,
  a.start_date::date as start_date,
  b.order_month,
  b.order_date as mindate,
  CASE 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) = CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) 
  WHEN CAST(EXTRACT(YEAR FROM b.order_date::date) as integer) != CAST(EXTRACT(YEAR FROM b.start_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM b.order_date::date) as integer)-CAST(EXTRACT(YEAR FROM b.start_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM b.order_date::date) as integer)- CAST(EXTRACT(MONTH FROM a.start_date::date) as integer) )
  ELSE 0 END as returning_month,
  a.city,
  a.acquisition_channel,
  a.distinct_customer as total_cohort,
  b.distinct_customer as returning_customer,
  b.hours,
  GMV
FROM
(SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer
FROM
  bi.customer_cohorts_exp
WHERE
  first_order_month = order_month) as a
LEFT JOIN
  (SELECT
  first_order_month,
  order_month,
  start_date,
  order_date,
  city,
  acquisition_channel,
  distinct_customer,
  Hours,
  GMV
FROM
  bi.customer_cohorts_exp
) as b
ON
  (a.first_order_month = b.first_order_month and a.city = b.city and a.acquisition_channel = b.acquisition_channel);