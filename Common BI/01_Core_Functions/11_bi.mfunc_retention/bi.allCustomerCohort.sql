
DROP TABLE IF EXISTS bi.allCustomerCohort;
CREATE TABLE bi.allCustomerCohort as 
SELECT
  t1.contact__c,
  city,
  first_order_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(start_date as date),'YYYY-MM') as Order_Month,
  cast(start_date as date) as order_date,
  status,
  order_duration__c,
  ordertype,
  acquisition_channel,
  acquisitionvoucher as acquisition_voucher,
  pph,
  pph__c,
  distinct_cleaner,
  polygon,
  nmp,
  acquisition_funnel,
  acquisition_rating,
  acquisition_recurrency
FROM
  bi.allCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.contact__c = t2.contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(start_date as date),'YYYY-MM'));
