
DROP TABLE IF EXISTS bi.customer_cohorts_overall_b2b;
CREATE TABLE bi.customer_cohorts_overall_b2b as 
SELECT
  first_order_month,
  order_month,
  min(start_date) as order_date,
  min(first_order_Date) as start_date,
  left(city,2) as locale,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer
FROM
  bi.b2bcohorts 
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  LEFT(city,2);