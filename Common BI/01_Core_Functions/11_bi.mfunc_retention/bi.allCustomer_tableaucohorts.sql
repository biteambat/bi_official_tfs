
DROP TABLE IF EXISTS bi.allCustomer_tableaucohorts;
CREATE TABLE bi.allCustomer_tableaucohorts as 
SELECT
  t1.contact__c,
  creation_date::date as first_order_date,
  ordertype,
  acquisition_channel,
  CASE
  WHEN acquisition_voucher like '%CGP%' THEN 'Groupon'
  WHEN acquisition_voucher like '%CDD%' THEN 'DailyDeal'
  WHEN acquisition_voucher like '%CLM%' THEN 'Limango'
  WHEN acquisition_voucher like '%CRB%' THEN 'Rublys'
  WHEN acquisition_voucher like '%CM%' THEN 'CM'
  WHEN acquisition_voucher like '%DEINDEAL%' or acquisition_voucher like '%DEAL%' THEN 'DeinDeal'
  ELSE acquisition_voucher END as acquisitionvoucher,
  pph,
  distinct_cleaner,
  nmp,
  t1.funnel as acquisition_funnel,
  t1.rating as acquisition_rating,
  t1.Recurrency__c as acquisition_recurrency
FROM
  bi.allCohortsCustomers_tableaucohorts t1
LEFT JOIN
  bi.allcohortscustomers_stats t2
ON
  (t1.contact__c = t2.contact__c) 
WHERE
  OrderNo = 1;