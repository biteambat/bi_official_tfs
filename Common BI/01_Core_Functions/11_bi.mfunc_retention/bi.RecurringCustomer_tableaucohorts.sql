
DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;
CREATE TABLE bi.RecurringCustomer_tableaucohorts as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  sub_kpi,
  acquisition_channel,
  ordertype,
  locale,
  voucher__c as acquisition_voucher,
  discount__c as acquisition_discount,
  funnel as acquisition_funnel
FROM
  bi.RecurringCohortsCustomers_tableaucohorts
WHERE
  OrderNo = 1;
  
CREATE INDEX IDX145 ON bi.RecurringCustomer_tableaucohorts(contact__c);

---------

DROP TABLE IF EXISTS bi.RecurringCustomer_tableaucohorts;
CREATE TABLE bi.RecurringCustomer_tableaucohorts as 
SELECT
  Contact__c,
  Start_Date as first_order_date,
  acquisition_channel,
  ordertype,
  locale,
  sub_kpi,
  funnel as acquisition_funnel,
  amount
FROM
  bi.RecurringCohortsCustomers_tableaucohorts
WHERE
  OrderNo = 1;