
DROP TABLE IF EXISTS bi.NonRecurringCustomerCohort;
CREATE TABLE bi.NonRecurringCustomerCohort as 
SELECT
  t1.Contact__c,
  to_char(cast(acquisition_date as date),'YYYY-MM') as AcquisitionMonth,
  Order_Id__c,
  to_char(cast(creation_date as date),'YYYY-MM')  as Order_Month,
  status,
  t2.acquisition_channel,
  t1.locale,
  acquisition_voucher
FROM
  bi.NonRecurringCohortsCustomers t1
LEFT JOIN
  bi.acquisition_channel_nonrecurring t2
ON
  (t1.Contact__c = t2.Contact__c) ;