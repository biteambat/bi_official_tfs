
DROP TABLE IF EXISTS bi.customer_cohorts_exp;
CREATE TABLE bi.customer_cohorts_exp as 
SELECT
  first_order_month,
  order_month,
  min(start_Date) as order_date,
  min(first_order_Date) as start_date,
  city,
  acquisition_channel,
  COUNT(DISTINCT(Contact__c)) as distinct_Customer,
  SUM(Order_Duration__c) as Hours,
  SUM(GMV_eur) as GMV
FROM
  bi.RecurringCustomerCohort
WHERE
  to_char(current_date,'YYYY-MM') >=  first_order_month
  and to_char(current_date,'YYYY-MM') >= order_Month
GROUP BY
  first_order_month,
  order_month,
  city,
  acquisition_channel;