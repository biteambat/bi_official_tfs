
DROP TABLE IF EXISTS bi.TEMP_acquisition_list;
CREATE TABLE bi.TEMP_acquisition_list AS

  SELECT DISTINCT

    o.effectivedate::date as acquisition_date,
    o.customer_id__c::text as customer_id,
    LEFT(o.locale__c,2)::text as locale,
    o.polygon::text as polygon,
    o.marketing_channel::text as acquisition_channel,
    o.recurrency__c::text as acquisition_recurrency,
    CASE WHEN (o.voucher__c <> '' AND o.voucher__c IS NOT NULL AND o.voucher__c <> '0') THEN o.voucher__c ELSE 'No Voucher' END as acquisition_voucher

    FROM bi.orders o
    JOIN salesforce.contact c
    ON (o.contact__c = c.sfid)

    WHERE o.test__c = '0'
      AND o.order_type = '1'
      AND o.acquisition_new_customer__c = '1'
      AND o.status = 'INVOICED'
      AND o.effectivedate::date <= (current_date - interval '60 days')
      AND o.order_creation__c::date = c.createddate::date

    ORDER BY acquisition_date desc, locale asc, polygon asc, acquisition_channel asc, acquisition_recurrency asc, acquisition_voucher asc

;