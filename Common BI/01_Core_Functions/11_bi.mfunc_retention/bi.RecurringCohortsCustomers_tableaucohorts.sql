
DROP TABLE IF EXISTS bi.RecurringCohortsCustomers_tableaucohorts;
CREATE TABLE bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date varchar(20) DEFAULT NULL, sub_kpi text, city varchar(55) DEFAULT null, OrderNo SERIAL, Contact__c varchar(255) NOT NULL,   Order_Id__c varchar(255) DEFAULT NULL,  Start_Date varchar(15) DEFAULT NULL,   Recurrency__c integer DEFAULT NULL,   status varchar(40) NOT NULL, gmv_eur_net varchar(40) NULL, order_duration__c integer default null, acquisition_channel varchar(40) NOT NULL, ordertype varchar NULL, locale varchar NULL, funnel varchar(50) default null,amount varchar(50) default null, PRIMARY KEY(contact__c,OrderNo) );

INSERT INTO bi.RecurringCohortsCustomers_tableaucohorts(acquisition_date, sub_kpi, city,OrderNo,contact__c,Order_Id__c,Start_Date,Recurrency__c,status,gmv_eur_net,order_duration__c,acquisition_channel,ordertype,locale,funnel,amount)
SELECT
  CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
  CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi,
  city,
  row_number() OVER (PARTITION BY contact__c Order by contact__c,Cast(Effectivedate as Date)) as a,
  Contact__c,
  Order_Id__c,
  Cast(Effectivedate as Date) as Start_Date,
  t1.Recurrency__c,
  status,
  gmv_eur_net,
  order_duration__c,
  marketing_channel,
  type,
  LEFT(t1.locale__c,2),
  CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel,
  amount
FROM
  bi.orders t1
LEFT JOIN
  Salesforce.Opportunity t2
ON
  (t2.sfid = t1.opportunityid)
WHERE
  t1.test__c = '0'
  and order_type = '2'
  and contact__c is not null
  and  t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED','FULFILLED') 
 ORDER BY
  contact__c,
  Cast(Order_Creation__c as Date);