
DROP TABLE IF EXISTS bi.RecurringCustomerCohort;
CREATE TABLE bi.RecurringCustomerCohort as 
SELECT
  t1.Contact__c,
  city,
  first_order_date,
  recurrency__c,
  start_date,
  to_char(cast(first_order_date as date),'YYYY-MM') as First_Order_Month,
  Order_Id__c,
  to_char(cast(Start_date as date),'YYYY-MM') as Order_Month,
  status,
  order_duration__c,
  gmv_eur,
  acquisition_voucher,
  acquisition_channel,
  acquisition_discount,
  t1.sub_kpi,
  t1.ordertype,
  t1.locale,
  acquisition_funnel
FROM
  bi.RecurringCustomer_tableaucohorts t1
JOIN
  bi.AllOrders_tableaucohorts t2
on
  (t1.Contact__c = t2.Contact__c and to_char(cast(first_order_date as date),'YYYY-MM') <=  to_char(cast(Start_Date as date),'YYYY-MM'));