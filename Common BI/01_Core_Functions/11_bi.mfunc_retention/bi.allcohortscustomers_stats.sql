
DROP TABLE IF EXISTS bi.allcohortscustomers_stats;
CREATE TABLE bi.allcohortscustomers_stats as 
SELECT
  contact__c,
  COUNT(DISTINCT(Professional)) as Distinct_Cleaner,
  SUM(CASE WHEN Status = 'CANCELLED NO MANPOWER' THEN 1 ELSE 0 END) as NMP
FROM
   bi.allCohortsCustomers_tableaucohorts
GROUP BY
  contact__c;