
DROP TABLE IF EXISTS bi.AllOrders_tableaucohorts_b2b;
CREATE TABLE bi.AllOrders_tableaucohorts_b2b as 
SELECT
 CAST(t1.Acquisition_Customer_Creation__c as Date) as acquisition_date,
 city,
 contact__c,
 Order_Id__c,
 CAST(Effectivedate as Date) as Start_date,
 LEFT(t1.locale__c,2) as locale,
 t1.Recurrency__c,
 status,
 order_duration__c,
 gmv_eur,
 marketing_channel,
 type as ordertype,
 CASE WHEN acquisition_tracking_id__c = 'appbooking' THEN 'Subscription' ELSE 'Conversion' END as Funnel
FROM
  bi.orders t1
JOIn
  Salesforce.Opportunity t2
 ON
  (t1.opportunityid = t2.sfid)
WHERE
  t1.test__c = '0'
  and t1.Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
  and order_type = '2'
  and contact__c is not null
  and stagename != 'IRREGULAR'
ORDER BY
  contact__c,
  CAST(Effectivedate as Date);