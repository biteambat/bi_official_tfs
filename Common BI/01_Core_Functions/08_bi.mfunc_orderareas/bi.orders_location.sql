
DELETE FROM bi.orders_location WHERE Order_Creation__c::date >= (current_date - interval '1 day');

INSERT INTO bi.orders_location 
	SELECT
		t1.order_id__c,
		t3.key__c as delivery_area,
		t3.key__c as efficiency_area,
		t3.key__c as polygon,
		t1.Order_Creation__c
	FROM
	    Salesforce.Order t1
	LEFT JOIn
		bi.orders_in_polygon t3
	ON
		(t1.order_id__c = t3.order_id__c)

	WHERE t1.Order_Creation__c::date >= (current_date - interval '1 day')
;