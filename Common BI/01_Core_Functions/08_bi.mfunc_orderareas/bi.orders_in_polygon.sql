
DELETE FROM bi.orders_in_polygon WHERE Order_Creation__c::date >= (current_date - interval '1 day');

INSERT INTO bi.orders_in_polygon 
SELECT  
    Order_Id__c,
    Order_Creation__c,
    Shippingcity,
    key__c,
ST_Intersects( 
    ST_SetSRID(ST_Point(shippinglongitude, shippinglatitude), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(delivery_area__c.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
FROM
    Salesforce.Order,
    salesforce.delivery_area__c
WHERE
     Order_Creation__c::date >= (current_date - interval '1 day')
GROUP BY
    Order_Id__c,
    Order_Creation__c,
    Shippingcity,
    key__c,
    flag_polygon
HAVING
    ST_Intersects( 
    ST_SetSRID(ST_Point(shippinglongitude, shippinglatitude), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(delivery_area__c.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true;