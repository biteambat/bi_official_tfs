
DROP TABLE IF EXISTS bi.recurrent_temp1;
CREATE TABLE bi.recurrent_temp1 as 
SELECT
	created_at::date as date,
	created_at as time,
	order_Json->>'Order_Creation__c' as orderdate,
	order_Json->>'Locale__c' as Locale__c,
	order_Json->>'Order_Id__c' as Order_id,
	order_Json->>'Contact__c' as customer_id
FROM
	events.sodium
WHERE
	event_name = 'Order Event:CANCELLED TERMINATED';