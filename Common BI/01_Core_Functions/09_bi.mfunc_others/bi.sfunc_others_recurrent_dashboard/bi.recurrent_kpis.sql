
DROP TABLE IF EXISTS bi.recurrent_temp1;
CREATE TABLE bi.recurrent_temp1 as 
SELECT
    created_at::date as date,
    created_at as time,
    order_Json->>'Order_Creation__c' as orderdate,
    order_Json->>'Locale__c' as Locale__c,
    order_Json->>'Order_Id__c' as Order_id,
    order_Json->>'Contact__c' as customer_id
FROM
    events.sodium
WHERE
    event_name = 'Order Event:CANCELLED TERMINATED';


DROP TABLE IF EXISTS bi.recurrent_kpis;
CREATE TABLE bi.recurrent_kpis as   
SELECT
    date::date as date,
    left(t1.locale__c,2) as locale,
    city,
    'Recurrent Cancellations' as kpi,
    COUNT(DISTINCT(customer_id)) as value 
FROM
     bi.recurrent_temp1 t1
JOIN
    bi.orders t2
ON 
    (t1.customer_id = t2.customer_id__c and test__c = '0' and recurrency__c > 6 and status = 'INVOICED' and effectivedate::date + Interval '28 days' >= date::date)
WHERE
    order_type = '1'
GROUP BY
    date,
    city,
    locale,
    kpi;
    

INSERT INTO bi.recurrent_kpis
SELECT
    date,
    locale,
    city,
    CASE
    WHEN Orders = 1 THEN '1 Order'::text
    WHEN Orders between 2 and 3 THEN '2-3 Orders'::text
    WHEN Orders between 4 and 5 THEN '4-5 Orders'::text
    WHEN Orders > 5 THEN '>5 Orders'::text
    ELSE 'Nothing'::text END as KPI,
    COUNT(DISTINCT(Customer_Id__c)) as Customer
FROM
    bi.recurrent_customer2
GROUP BY
    date,
    locale,
    city,
    kpi;


INSERT INTO bi.recurrent_kpis   
SELECT
    Effectivedate::date as date,
    LEFT(locale__c,2) as locale,
    city,
    'New Recurrent' as kpi,
    COUNT(DISTINCT(CUstomer_Id__c)) as Distinct_Customer
FROM
    bi.orders
WHERE
    Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','ALLOCATION AUTO','ALLOCATION PAUSED','INVOICED')
    and test__c = '0'
    and Acquisition_Channel__c in ('web','ios','android')
    and recurrency__c > 6
    and order_type = '1'
GROUP BY
    Effectivedate,
    locale,
    city;

INSERT INTO bi.recurrent_kpis   
SELECT
    Effectivedate::date as date,
    LEFT(locale__c,2) as locale,
    city,
    'Existing Recurrent 7d' as kpi,
    COUNT(DISTINCT(CUstomer_Id__c)) as Distinct_Customer
FROM
    bi.orders
WHERE
    Status in ('WAITING CONFIRMATION','WAITING FOR RESCHEDULE','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','ALLOCATION AUTO','ALLOCATION PAUSED','INVOICED')
    and test__c = '0'
    and Acquisition_Channel__c = 'recurrent'
    and recurrency__c > 6
    and order_type = '1'
GROUP BY
    Date,
    locale,
    city;
    
INSERT INTO bi.recurrent_kpis   
SELECT
    Effectivedate::date as date,
    LEFT(locale__c,2) as locale,
    city,
    'Skipping Customer' as kpi,
    COUNT(DISTINCT(CUstomer_Id__c)) as Distinct_Customer
FROM
    bi.orders
WHERE
    Status in ('CANCELLED SKIPPED')
    and test__c = '0'
    and Acquisition_Channel__c = 'recurrent'
    and recurrency__c > 6
    and order_type = '1'
GROUP BY
    Date,
    locale,
    city;