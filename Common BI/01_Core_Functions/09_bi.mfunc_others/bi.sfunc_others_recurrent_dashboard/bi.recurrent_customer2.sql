
DROP TABLE IF EXISTS bi.recurrent_customer2;
CREATE TABLE bi.recurrent_customer2 as
SELECT
    date::date as date,
    left(t1.locale__c,2) as locale,
    city,
    Customer_Id__c,
    COUNT(DISTINCT(Order_Id__c)) as Orders
FROM
     bi.recurrent_temp1 t1
JOIN
    bi.orders t2
ON 
    (t1.customer_id = t2.customer_id__c and test__c = '0' and recurrency__c > 6 and status = 'INVOICED' and effectivedate::date + Interval '45 days' >= date::date)
WHERE
    order_type = '1'
GROUP BY
    date,
    locale,
    city,
    Customer_id__c;