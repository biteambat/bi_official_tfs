
DELETE FROM public.marketing_spending;

INSERT INTO public.marketing_spending

SELECT

	l."Date" as start_date,
	l."Date" as end_date,	
	LEFT(LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)), 2) as locale,	
	LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)) as languages,	
	LOWER(SUBSTRING(l."CampaignName" from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')) as "city",	
	LEFT(l."CampaignName", 3) as type,	
	'Marketing Costs' as kpi,	
	'Adwords' as sub_kpi_1,
		
	CASE WHEN l."AccountDescriptiveName" = 'Reinigung Direkt' THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services' END as sub_kpi_2,
	
	LOWER(substring(l."AdGroupName" from '[0-9]{3}_[A-Z]{2}_([^\+]+)')) as sub_kpi_3,
	'New' as sub_kpi_4,
	'SEM B2B' as sub_kpi_5,
	'' as sub_kpi_6,
	'' as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	SUM(l."Cost"::numeric/1000000) as value
	
	
	-- CASE WHEN l."AccountDescriptiveName" IN ('unbounce rei') THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services'
		
	-- lower(substring(l.campaignname from 'tpc:(.[^\,]+)')) as "keyword",
	-- lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as "city",

FROM

	public.adwords_adgroup_report l

GROUP BY

	l."Date",
	l."AccountDescriptiveName",
	LOWER(SUBSTRING(l."CampaignName" from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')),
	LEFT(LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)), 2),
	LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)),
	LEFT(l."CampaignName", 3),
	LOWER(substring(l."AdGroupName" from '[0-9]{3}_[A-Z]{2}_([^\+]+)'));

---------------------------------------------------------------------------------------------

INSERT INTO public.marketing_spending	

SELECT

	l.date as start_date,
	l.date as end_date,	
	LEFT(LOWER(RIGHT(LEFT(l.campaign, 9), 5)), 2) as locale,	
	LOWER(RIGHT(LEFT(l.campaign, 9), 5)) as languages,	
	LOWER(SUBSTRING(l.campaign from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')) as "city",	
	LEFT(l.campaign, 3) as type,	
	'Marketing Costs' as kpi,	
	'Bing' as sub_kpi_1,
		
	CASE WHEN l.account = 'Reinigung Direkt DE' THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services' END as sub_kpi_2,
	
	LOWER(substring(l.adgroup from '[0-9]{3}_[A-Z]{2}_([^\+]+)')) as sub_kpi_3,
	'New' as sub_kpi_4,
	'SEM B2B' as sub_kpi_5,
	'' as sub_kpi_6,
	'' as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	SUM(l.cost::numeric) as value
	
	
	-- CASE WHEN l."AccountDescriptiveName" IN ('unbounce rei') THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services'
		
	-- lower(substring(l.campaignname from 'tpc:(.[^\,]+)')) as "keyword",
	-- lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as "city",

FROM

	public.bing_ads_report l

GROUP BY

	l.date,
	l.account,
	LOWER(SUBSTRING(l.campaign from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')),
	LEFT(LOWER(RIGHT(LEFT(l.campaign, 9), 5)), 2),
	LOWER(RIGHT(LEFT(l.campaign, 9), 5)),
	LEFT(l.campaign, 3),
	LOWER(substring(l.adgroup from '[0-9]{3}_[A-Z]{2}_([^\+]+)'));

---------------------------------------------------------------------------------------------
	
	
INSERT INTO public.marketing_spending	
	
SELECT

	TO_CHAR(l.createddate, 'YYYY-MM-DD')::date as start_date,
	TO_CHAR(l.createddate, 'YYYY-MM-DD')::date as end_date,	
	
	LEFT(l.locale__c, 2) as locale,	
	l.locale__c as languages,	
	
	LOWER(SUBSTRING(REPLACE(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
	
		
	l.type__c as type,	
	'Likelies' as kpi,
	CASE 	
		WHEN LOWER(l.acquisition_channel_ref__c) = 'anygrowth' and l.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'
	
		WHEN LOWER(l.acquisition_channel_ref__c) = 'reveal' and l.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'
	
		WHEN (l.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
		
		WHEN (l.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
		
		WHEN (l.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'
	
		WHEN ((lower(l.acquisition_channel__c) = 'inbound') AND lower(l.acquisition_tracking_id__c) = 'classifieds')					
		THEN 'Classifieds'
					
		WHEN (l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	
	         AND l.createddate::date <= '2017-02-08'
	
	     THEN 'SEM Brand'::text
	
	     WHEN ((((l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goob%')
	
	     	AND l.createddate::date > '2017-02-08'
	     	AND ((l.acquisition_channel_params__c LIKE '%goob%') AND (l.acquisition_channel_params__c LIKE '%bt:b2b%' OR l.acquisition_channel_params__c LIKE '%bt: b2b%' OR l.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
	     	)
	
	     THEN 'SEM Brand B2B'::text
	
	     WHEN (((l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goob%')
	
	     	AND l.createddate::date > '2017-02-08'
			AND ((l.acquisition_channel_params__c LIKE '%goob%') AND (l.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%' ))
	     	
	     THEN 'SEM Brand B2C'::text
	     
	
	     WHEN (((l.acquisition_channel_params__c::text ~~ '%goog%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goog%')
	
	     	AND l.createddate::date <= '2017-02-08'
	     THEN 'SEM'::text
	
	     WHEN (l.acquisition_channel_params__c::text ~~ '%goog%'::text) AND (l.acquisition_channel_params__c LIKE '%bt: b2b%' OR l.acquisition_channel_params__c LIKE '%"bt":"b2b%') THEN 'Adwords'
	     
	     WHEN l.acquisition_channel_params__c::text ~~ '%ysm%'::text THEN 'Bing'
	
	     WHEN ((((l.acquisition_channel_params__c::text ~~ '%goog%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goog%')
	
	     	AND l.createddate::date > '2017-02-08' 
	     	AND ((l.acquisition_channel_params__c LIKE '%goog%') AND (l.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%')))
	     	OR l.acquisition_channel_params__c::text LIKE '%goog%'
	     THEN 'SEM B2C'::text
	     
	
	     WHEN (l.acquisition_channel_params__c::text ~~ '%disp%'::text OR l.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%facebook%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	     THEN 'Display'::text
	                 
	
	     WHEN l.acquisition_channel_params__c::text ~~ '%ytbe%'::text
	     THEN 'Youtube Paid'::text
	
	
	     WHEN (l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_ref__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text
	         AND LOWER(acquisition_tracking_id__c) LIKE '%b2b seo page form&'
	     THEN 'SEO B2B'::text
	     
	
	     WHEN ((l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_ref__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text
			AND LOWER(acquisition_tracking_id__c) NOT LIKE '%b2b seo page form&')
	
	     	OR
	     		((acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL AND (acquisition_tracking_id__c LIKE '%b2b%' OR acquisition_tracking_id__c LIKE '%tfs%')))
	
	     THEN 'SEO'::text
	     
	     WHEN (l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goog%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	         THEN 'SEO Brand'::text
	         
	     WHEN (l.acquisition_channel_params__c::text ~~ '%batfb%'::text  
	             OR l.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%facebook%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
	     THEN 'Facebook'::text
	
	
	     WHEN l.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%email%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%vero%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%batnl%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%fullname%'::text
	             OR l.acquisition_channel_params__c::text ~~ '%invoice%'::text 
	     THEN 'Newsletter'::text
	     
	     WHEN (l.acquisition_channel_params__c::text !~~ '%goog%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	             AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	             AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%fb%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%clid%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%utm%'::text 
	             AND l.acquisition_channel_params__c::text <> ''::text)
	     	OR (acquisition_channel__c IS NULL AND acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL)
	     THEN 'DTI'::text
	     
	     ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as sub_kpi_1,	

	CASE WHEN (l.acquisition_tracking_id__c LIKE 'rd %' OR l.acquisition_tracking_id__c LIKE '%unbounce_reinigungdirekt%') THEN 'Reinigungdirekt'
	     WHEN (l.acquisition_tracking_id__c LIKE 'tfs %' OR l.acquisition_tracking_id__c LIKE '%unbounce_tfs%') THEN 'Tiger Facility Services'
		  ELSE 'Other'
		  END as sub_kpi_2,
	l.acquisition_tracking_id__c as sub_kpi_3,
	lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as sub_kpi_4,
	-- LOWER(SUBSTRING(l.acquisition_channel_params__c from 'tpc:(.[^\,]+)')) as sub_kpi_3,
	
	'SEM B2B' as sub_kpi_5,
	
	l.stage__c as sub_kpi_6,
	l.lost_reason__c as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	COUNT(DISTINCT l.sfid) as value

FROM

	salesforce.likeli__c l
	
WHERE

	l.type__c = 'B2B'
	AND l.acquisition_channel__c IN ('web', 'inbound')
	AND l.createddate > '2017-12-31'	
	AND l.company_name__c NOT LIKE '%test%'
	AND l.company_name__c NOT LIKE '%bookatiger%'
	AND l.email__c NOT LIKE '%bookatiger%'
	
	AND lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) NOT LIKE '%test%'
	
GROUP BY

	start_date,
	end_date,	
	l.acquisition_channel_params__c,
	LEFT(l.locale__c, 2),	
	l.locale__c,	
	type,	
	kpi,	
	sub_kpi_1,	
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5,
	l.stage__c,
	l.lost_reason__c,
	sub_kpi_8,
	sub_kpi_9,
	sub_kpi_10
	
ORDER BY

	start_date desc;