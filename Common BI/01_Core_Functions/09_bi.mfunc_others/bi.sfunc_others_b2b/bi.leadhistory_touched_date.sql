
DROP TABLE IF EXISTS bi.leadhistory_touched_date;
CREATE TABLE bi.leadhistory_touched_date as 
SELECT
	parentid,
	min(createddate::date) as createddate
FROM
	salesforce.likeli__history t1
WHERE
	field = 'Owner'
	and createddate::date > '2017-02-01'
	and oldvalue in ('API Tiger','MC Tiger','IT Tiger','Alejandra Chavez','Renan Ribeiro','Austin Marcus')
	and newvalue not in ('Alejandra Chavez','Renan Ribeiro','Austin Marcus','API Tiger','MC TIGER','IT TIGER')
GROUP BY
	parentid;