
DROP TABLE IF EXISTS bi.opportunity_churn;
CREATE TABLE bi.opportunity_churn as 
	
SELECT

	-- CASE WHEN t4.monthly_revenue_loss > 700 THEN '> 700€'
		  -- WHEN (t4.monthly_revenue_loss > 250 AND t4.monthly_revenue_loss < 700) THEN '250€ - 700€'
		  -- WHEN t4.monthly_revenue_loss < 250 THEN '> 250€'
		  -- ELSE NULL
		  -- END as cluster,
	t4.*

FROM	
	
	
	(SELECT
	
		MIN(t1.date_churn) as date_churn,
		t1.year,
		t1.month,
		t1.opportunityid,
		t1.name,
		t1.locale,
		t2.polygon,
		-- t1.stagename,
		t1.churn_reason,
		t2.hours as monthly_hours_loss,
		grand_total__c,
		t2.monthly_revenue as monthly_revenue_loss,
		t1.acquisition_channel__c as acquisition_channel
		
	FROM
	
		(SELECT

			opphistory.*,
			op.name,
			left(op.locale__c,2) as locale,
			op.churn_reason__c as churn_reason,
			op.grand_total__c,
			op.acquisition_channel__c
		
		FROM
		
			(SELECT
				
				EXTRACT(YEAR FROM o.createddate) as year,
				EXTRACT(MONTH FROM o.createddate) as month,	
				MAX(o.createddate::date) as date_churn,
				o.opportunityid
				-- o.stagename
			
			FROM
			
				salesforce.opportunityfieldhistory o
				
			WHERE
			
				o.newvalue IN ('DECLINED', 'TERMINATED', 'RESIGNED', 'CANCELLED')
				
			GROUP BY
			
				o.opportunityid,
				-- o.stagename,
				year,
				month
				
			ORDER BY
				
				year desc,
				month desc,
				o.opportunityid) as opphistory
			
		LEFT JOIN 
		
			salesforce.opportunity op
			
		ON 
		
			opphistory.opportunityid = op.sfid) as t1
			
	LEFT JOIN
		
		(SELECT

			t3.opportunityid,
			t3.polygon,
			MAX(t3.hours) as hours,
			MAX(t3.monthly_revenue) as monthly_revenue
		
		FROM
		
			(SELECT
		
				EXTRACT(YEAR FROM o.effectivedate) as year,
				EXTRACT(MONTH FROM o.effectivedate) as month,
				o.opportunityid,
				o.polygon,
				SUM(CASE WHEN o.eff_pph IS NULL THEN o.pph__c*o.order_duration__c ELSE o.eff_pph*o.order_duration__c END) as monthly_revenue,
				SUM(o.order_duration__c) as hours
							
			FROM
			
				bi.orders o
				
			WHERE
			
				o.order_type = 2
				AND (o.effectivedate >= '2018-01-01')
								
			GROUP BY
			
				year,
				month,
				o.opportunityid,
				o.polygon) as t3
		
		GROUP BY 
		
			t3.opportunityid,
			t3.polygon
			) as t2
				
	ON 
	
		t1.opportunityid = t2.opportunityid
		
	GROUP BY
	
		t1.year,
		t1.month,
		t1.opportunityid,
		t1.name,
		t1.locale,
		t2.polygon,
		-- t1.stagename,
		t1.churn_reason,
		monthly_hours_loss,
		grand_total__c,
		t2.monthly_revenue,
		t1.acquisition_channel__c
	
	ORDER BY
	
		year desc,
		month desc) as t4;