
DROP TABLE IF EXISTS bi.cohorts_starters;
CREATE TABLE bi.cohorts_starters as

SELECT

	t2.sfid,
	t2.potential,
	t2.grand_total__c,
	t2.year_month_closed,
	t2.closedate,
	CASE WHEN 
	         (CASE WHEN t2.valid_orders = 0 THEN 'Never Started' ELSE 'Started' END) = 'Never Started'
	     THEN NULL
	     ELSE first_order
	     END as first_order,
   t2.valid_orders,
   t2.invalid_orders,
   t2.revenue,
	CASE WHEN t2.valid_orders = 0 THEN 'Never Started' ELSE 'Started' END as status_start

FROM	
	
	(SELECT
	
		t1.*,
		CASE WHEN t1.grand_total__c IS NULL THEN t1.potential ELSE t1.grand_total__c END as revenue
	
	FROM	
		
		(SELECT
		
			o.sfid,
			pot.potential,
			o.grand_total__c,
			TO_CHAR(o.closedate,'YYYY-MM') as year_month_closed,
			o.closedate,
			MIN(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN (oo.effectivedate + 10000)
			         ELSE oo.effectivedate 
						END) as first_order,
			SUM(CASE WHEN oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as valid_orders,
		   SUM(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as invalid_orders
		
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp pot
			
		ON
		
			o.sfid = pot.opportunityid
			
		LEFT JOIN
		
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c
			
		WHERE
		
			o.test__c IS FALSE
			AND LEFT(o.locale__C, 2) = 'de'
			-- AND oo."status" NOT IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND o.stagename IN ('WON', 'PENDING')
			AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
			
			o.sfid,
			o.grand_total__c,
			TO_CHAR(o.closedate,'YYYY-MM'),
			o.closedate,
			pot.potential) AS t1) as t2;