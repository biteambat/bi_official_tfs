
DROP TABLE IF EXISTS bi.forecast_revenue;
CREATE TABLE bi.forecast_revenue as 

SELECT

	'Churned Customers Last Month' as kpi,
	o.sfid,
	o.name,
	o.delivery_area__c,
	ooo.service_type__c,
	-- oo.confirmed_end__c,
	o.grand_total__c,
	-- SUM(ooo.amount__c)/1.19 amount_invoiced,

	-SUM(ooo.amount__c)/1.19 as on_top_this_month

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND oo.service_type__c = 'maintenance cleaning'
	AND ooo.service_type__c = 'maintenance cleaning'
	
GROUP BY

	o.sfid,
	o.name,
	o.delivery_area__c,
	ooo.service_type__c,
	o.grand_total__c
	-- oo.confirmed_end__c
	;
	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
	
SELECT

	'Churned Customers This month' as kpi,
	t1.sfid,
	t1.name,
	t1.delivery_area__c,
	t1.service_type__c,
	t1.grand_total__c,
	t1.on_top_this_month

FROM	
	
	(SELECT
	
		o.sfid,
		o.name,
		o.delivery_area__c,
		ooo.service_type__c,
		o.grand_total__c,
		oo.pph__c,
		SUM(ooo.amount__c)/1.19 amount_invoiced_last_month,
		oo.confirmed_end__c,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		EXTRACT(DAY FROM oo.confirmed_end__c) as days_to_invoice,
		CASE WHEN o.grand_total__c IS NOT NULL THEN
		          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     END as to_pay_this_month,
		CASE WHEN o.grand_total__c IS NOT NULL THEN
		          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     END
		- SUM(ooo.amount__c)/1.19 as on_top_this_month
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.sfid = oo.opportunity__c
		
	LEFT JOIN
	
		salesforce.invoice__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
		
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c IN ('RESIGNED', 'CANCELLED', 'OFFBOARDING')
		AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		-- AND ooo.issued__c::text IS NULL
		AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date)::text, 7)
		AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
		AND oo.service_type__c = 'maintenance cleaning'
		AND ooo.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.delivery_area__c,
		ooo.service_type__c,
		o.grand_total__c,
		oo.pph__c,
		oo.confirmed_end__c) as t1;
		
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
		
SELECT

	'New Customers Last Month' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	inv.service_type__c,
	-- t1.confirmed_end__c,
	-- t1.first_order,	
	t1.grand_total__c,
	-- SUM(inv.amount__c)/1.19 as invoiced_last_month,
	t1.grand_total__c - SUM(inv.amount__c)/1.19 as on_top_this_month
	
FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.delivery_area__c,
		oo.name,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c) as t1
		
LEFT JOIN

	salesforce.invoice__c inv
	
ON

	t1.opportunityid = inv.opportunity__c
		
WHERE
	
	LEFT(t1.first_order::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7)
	     OR LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
	     OR t1.confirmed_end__c IS NULL)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7) OR t1.confirmed_end__c IS NULL)
	AND inv.service_type__c = 'maintenance cleaning'
	
GROUP BY
	
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.grand_total__c,
	inv.service_type__c
	-- t1.confirmed_end__c
	;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'New Customers This month' as kpi,
	t2.opportunityid,
	t2.name,
	t2.delivery_area__c,
	t2.service_type__c,
	t2.grand_total__c,
	t2.on_top_this_month

FROM		
	
	(SELECT
	
		t1.opportunityid,
		t1.name,
		t1.delivery_area__c,
		t1.service_type__c,
		t1.confirmed_end__c,
		t1.first_order,
		t1.grand_total__c,
		EXTRACT(DAY FROM t1.first_order) as day_first_order,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1 as days_to_invoice,
		(t1.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1) as on_top_this_month
	        	
	FROM	
		
		(SELECT
		
			o.opportunityid,
			oo.name,
			oo.delivery_area__c,
			ooo.service_type__c,
			oo.grand_total__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c,
			MIN(o.effectivedate) as first_order
		
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
		
		LEFT JOIN
	
			salesforce.contract__c ooo
		
		ON
	
			o.opportunityid = ooo.opportunity__c
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND LEFT(o.locale__c, 2) = 'de'
			-- AND (ooo.active__c IS TRUE )
			AND ooo.service_type__c = 'maintenance cleaning'
			AND oo.test__c IS FALSE
			AND (LEFT(ooo.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
		       OR ooo.confirmed_end__c IS NULL)
			    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
					
		GROUP BY
		
			o.opportunityid,
			oo.name,
			oo.grand_total__c,
			oo.delivery_area__c,
			ooo.service_type__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c) as t1
			
	WHERE
		
		LEFT(t1.first_order::text, 7) = LEFT((current_date)::text, 7)
		
	GROUP BY
		
		t1.opportunityid,
		t1.name,
		t1.grand_total__c,
		t1.delivery_area__c,
		t1.service_type__c,
		t1.confirmed_end__c,
		t1.first_order) as t2;
	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
		
SELECT

	'Difference PPH' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.service_type__c,
	t1.pph__c,
	t1.invoiced_oct - t1.invoiced_sept as on_top_this_month
	
FROM

	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		ooo.service_type__c,
		oo.grand_total__c,
		ooo.pph__c,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END) as hours_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END) as hours_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END) as hours_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END) as hours_oct,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_oct,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-11' THEN o.order_duration__c ELSE 0 END) as hours_nov,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-11' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_nov,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-12' THEN o.order_duration__c ELSE 0 END) as hours_dec,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-12' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_dec
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN
	
		salesforce.contract__c ooo
	
	ON
	
		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		AND oo.grand_total__c IS NULL
		AND ooo.pph__c IS NOT NULL
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		ooo.service_type__c,
		oo.grand_total__c,
		ooo.pph__c) as t1;		

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Invoiced Maintenance Cleaning Last Month' as kpi,
	inv.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END as grand_total,
	SUM(inv.amount__c)/1.19 as revenue_last_month


FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND CASE WHEN RIGHT(current_date::text, 2)::integer < 5
	         THEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - '2 MONTH'::INTERVAL)::text, 7) 
	         ELSE LEFT(inv.issued__c::text, 7) = LEFT((current_date - '1 MONTH'::INTERVAL)::text, 7) 
	         END 
	AND inv.parent_invoice__c IS NULL
	AND inv.service_type__c LIKE 'maintenance cleaning'
	
GROUP BY

	inv.opportunity__c,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Cancellations This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	-12039 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Other Discounts This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	-7817 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Extra Orders This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	1666 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Manual Invoices This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	3740 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;


---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Blocked Not Invoiced This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	-1454 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Grand Total Adjustments This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	1143 as on_top_this_month -- Average of the last 6 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
	
SELECT

	'Extra Services This Month' as kpi,
	t3.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	t3.service_type__c,
	t3.money as grand_total,
	SUM(t3.invoiced_this_month) as on_top_this_month

FROM	
			
	(SELECT
	
		TO_CHAR(NOW(), 'YYYY-MM') as this_month,
		t2.sfid,
		t2.opportunity__c,
		t2.name,
		t2.service_type__c,
		t2.recurrency__c,
		t2.note__c,
		t2.date_start,
		t2.money,
		t2.number_days_this_month,
		t2.days_to_invoice,
		CASE WHEN TO_CHAR(t2.date_start, 'YYYY-MM') = TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('water dispenser', 'coffee machine')
			  THEN t2.days_to_invoice*t2.money/30.4
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('water dispenser', 'coffee machine')
			  THEN t2.money
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('fruits') AND t2.recurrency__c = 'weekly'
			  THEN t2.money*cal.mondays
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('fruits') AND t2.recurrency__c = 'biweekly'
			  THEN t2.money*cal.mondays/2
			  
			  WHEN (TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c NOT IN ('water dispenser', 'coffee machine', 'fruits') AND t2.note__c LIKE '%' || TO_CHAR(NOW(), 'YYYY-MM') || '%'
			       OR TO_CHAR(t2.date_start, 'YYYY-MM') = TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c NOT IN ('water dispenser', 'coffee machine', 'fruits'))
			  THEN t2.money
			  
			  WHEN t2.sfid = 'a1C0J000008hzECUAY' THEN 8855
			  
			  WHEN t2.sfid = 'a1C0J000008E8mMUAS' THEN 932.4
			  
			  WHEN t2.sfid = 'a1C0J000009ZpvyUAC' THEN 7*16
			  
			  ELSE 0
			  
			  END as invoiced_this_month
	
	FROM	
		
		(SELECT	
			
			t1.sfid,
			t1.opportunity__c,
			t1.name,
			t1.service_type__c,
			t1.recurrency__c,
			t1.note__c,
			t1.date_start,
			t1.money,
			DATE_PART('days', DATE_TRUNC('month', t1.date_start) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
			DATE_PART('days', DATE_TRUNC('month', t1.date_start) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.date_start) + 1 as days_to_invoice		
			
		FROM
			
			(SELECT
			
				o.sfid,
				o.opportunity__c,
				o.name,
				o.service_type__c,
				o.recurrency__c,
				o.note__c,
				CASE WHEN o.effective_start__c IS NULL THEN o.start__c ELSE o.effective_start__c END as date_start,
				SUM(o.grand_total__c) as money
			
			FROM
			
				salesforce.contract__c o
				
			WHERE
			
				o.service_type__c NOT LIKE 'maintenance cleaning'
				AND o.active__c IS TRUE
				AND o.test__c IS FALSE
				
			GROUP BY
			
				o.sfid,
				o.opportunity__c,
				o.name,
				o.service_type__c,
				o.effective_start__c,
				o.start__c,
				o.recurrency__c,
				o.note__c
				
			ORDER BY
			
				o.sfid,
				o.service_type__c,
				o.effective_start__c) as t1) as t2
				
	LEFT JOIN
	
		bi.calendar_matrix cal
		
	ON
	
		TO_CHAR(NOW(), 'YYYY-MM') = cal."year_month") as t3
		
LEFT JOIN

	salesforce.opportunity o
	
ON

	o.sfid = t3.opportunity__c
	
WHERE

	t3.invoiced_this_month > 0
	
GROUP BY

	t3.opportunity__c,
	o.name,
	o.delivery_area__c,
	t3.service_type__c,
	t3.money;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Extra Services Last Month' as kpi,
	inv.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END as grand_total,
	SUM(inv.amount__c) as revenue_last_month



FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND CASE WHEN RIGHT(current_date::text, 2)::integer < 8
	         THEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - '2 MONTH'::INTERVAL)::text, 7) 
	         ELSE LEFT(inv.issued__c::text, 7) = LEFT((current_date - '1 MONTH'::INTERVAL)::text, 7) 
	         END 
	AND inv.parent_invoice__c IS NULL
	AND inv.service_type__c NOT LIKE 'maintenance cleaning'
	
GROUP BY

	inv.opportunity__c,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END;