
-- Contains the dates of the first and last orders of each opportunity


DROP TABLE IF EXISTS bi.first_last_orders;
CREATE TABLE bi.first_last_orders AS 

SELECT

    '1'::integer as key_link,
    t1.country,
    t1.locale__c,
    t1.delivery_area__c as polygon,
    t1.opportunityid,
    t1.date_first_order,
    TO_CHAR(t1.date_first_order, 'YYYY-MM') || '-01' as year_month_first_order,
    CASE WHEN t2.last_order IS NULL THEN '2099-12-31'::date ELSE t2.last_order END as date_last_order,
    CASE WHEN t2.last_order IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.last_order, 'YYYY-MM') || '-01' END as year_month_lat_order
    
FROM
    
    ((SELECT

        LEFT(o.locale__c, 2) as country,
        o.locale__c,
        o.opportunityid,
        o.delivery_area__c,
        MIN(o.effectivedate) as date_first_order
    
    FROM
    
        salesforce.order o
        
    WHERE
    
        o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
        AND o.type = 'cleaning-b2b'
        AND o.professional__c IS NOT NULL
        AND o.test__c IS FALSE
        AND o.effectivedate >= '2018-01-01'
        
    GROUP BY
    
        o.opportunityid,
        o.locale__c,
        o.delivery_area__c,
        LEFT(o.locale__c, 2))) as t1
        
LEFT JOIN

    (SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
             -- It's the last day on which they are making money
        LEFT(o.locale__c, 2) as country,
        o.opportunityid,
        'last_order' as type_date,
        MAX(o.effectivedate) as last_order
        
    FROM
    
        salesforce.order o
        
    LEFT JOIN
    
        salesforce.opportunity oo
        
    ON 
    
        o.opportunityid = oo.sfid
        
    WHERE
    
        o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
        AND o.type = 'cleaning-b2b'
        AND o.professional__c IS NOT NULL
        AND o.test__c IS FALSE
        AND oo.test__c IS FALSE
    
    GROUP BY
    
        LEFT(o.locale__c, 2),
        type_date,
        o.opportunityid) as t2
        
ON

    t1.opportunityid = t2.opportunityid;