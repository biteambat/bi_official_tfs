
DROP TABLE IF EXISTS bi.click_to_signed;
CREATE TABLE bi.click_to_signed AS

SELECT

	LEFT(created.locale__c, 2) as country,
	-- created.sfid,
	created.created_date,
	-- contract_sent.sent_date,
	EXTRACT(EPOCH FROM (contract_sent.sent_date - created.created_date))/3600::numeric as time_creation_contract_sent,
	(click.clicks_or_reloads) as clicks_or_reloads,
	-- click.first_click,
	EXTRACT(EPOCH FROM (click.first_click - contract_sent.sent_date))/3600::numeric  as time_contract_sent_click,
	-- click.last_click_or_reload,
	-- click.time_first_to_last_click,
	-- signed.signed_date::time,
	EXTRACT(EPOCH FROM (signed.signed_date -  created.created_date))/3600::numeric  as time_creation_signed,
	EXTRACT(EPOCH FROM (signed.signed_date - contract_sent.sent_date))/3600::numeric  as time_contract_sent_signed,
	EXTRACT(EPOCH FROM (signed.signed_date - click.first_click))/3600::numeric  as time_first_click_signed
	
	-- EXTRACT(EPOCH FROM '2 months 3 days 12 hours 65 minutes'::INTERVAL)/60

FROM

	(SELECT
	
		o.createddate as created_date,
		o.sfid,
		o.locale__c
	
	FROM
	
		salesforce.opportunity o

	WHERE

		o.createddate >= '2018-01-22' ) as created

LEFT JOIN

	(SELECT

		t1.opportunityid,
		t1.createddate as signed_date
		
	FROM
	
	(SELECT
	
		DISTINCT oo.opportunityid,
		oo.createddate
	
	FROM
	
		salesforce.opportunityfieldhistory oo
					
	WHERE
	
		oo.newvalue IN ('PENDING')) t1
		
	ORDER BY
	
		t1.createddate desc) as signed
				
ON 

	created.sfid = signed.opportunityid
	
LEFT JOIN

	(SELECT 

	   metadata_json#>>'{opportunity,Id}' as "opp_id",
	   COUNT(id) as "clicks_or_reloads",
	   MIN(created_at) as "first_click",
	   MAX(created_at) as "last_click_or_reload",
		MAX(created_at) - MIN(created_at) as time_first_to_last_click
	            
	FROM       
	
		events.sodium
	
	WHERE 
	
	   event_name = 'opportunity:get_offer'
	   AND id > 3300000
	            
	GROUP BY    
	
		"opp_id") as click
		
ON

	created.sfid = click.opp_id
	
LEFT JOIN

	(SELECT

		t1.opportunityid,
		t1.createddate as sent_date
		
	FROM
	
	(SELECT
	
		DISTINCT oo.opportunityid,
		oo.createddate
	
	FROM
	
		salesforce.opportunityfieldhistory oo
					
	WHERE
	
		oo.newvalue IN ('CONTRACT SEND OUT')) t1
		
	ORDER BY
	
		t1.createddate desc) as contract_sent
		
ON

	created.sfid = contract_sent.opportunityid

GROUP BY

	LEFT(created.locale__c, 2),
	created.created_date,
	time_creation_contract_sent,
	time_contract_sent_click,
	time_creation_signed,
	time_contract_sent_signed,
	time_first_click_signed,
	clicks_or_reloads
	
ORDER BY 

	created.created_date desc;