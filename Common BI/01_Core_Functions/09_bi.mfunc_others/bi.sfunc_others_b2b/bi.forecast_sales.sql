
DROP TABLE IF EXISTS bi.forecast_sales;
CREATE TABLE bi.forecast_sales AS


SELECT

	year,
	month,
	country,
	channel,
	SUM(t3.number_opportunities) as actual_opportunities,
	SUM(t3.forecast_opportunities) as forecast_opps,
	CASE WHEN (country = 'DE' AND channel = 'INBOUND') THEN 50 
		  WHEN (country = 'DE' AND channel = 'OUTBOUND') THEN 20
		  ELSE (CASE WHEN (country = 'CH' AND channel = 'INBOUND')  THEN 4 
		             WHEN (country = 'CH' AND channel = 'OUTBOUND')  THEN 4
		  				 ELSE (CASE WHEN (country = 'NL' AND channel = 'INBOUND') THEN 7 
						            WHEN (country = 'NL' AND channel = 'OUTBOUND') THEN 7
						            ELSE NULL END)
						 END)
		  END as target,
	CASE WHEN (country = 'DE' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/50
	     WHEN (country = 'DE' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/20 
		  ELSE (CASE WHEN (country = 'CH' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/4
	     				 WHEN (country = 'CH' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/4
		  				ELSE (CASE WHEN (country = 'NL' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/7
	     				 			  WHEN (country = 'NL' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/7
	     				 			  ELSE NULL
	     				 			  END)
						END)
		  END as runrate

FROM

	(SELECT
	
		UPPER(LEFT(locale__c, 2)) as country,
		EXTRACT(YEAR FROM t2.closedate) as year,
		EXTRACT(MONTH FROM t2.closedate) as month,
		EXTRACT(DAY FROM t2.closedate) as day,
		DATE_PART('days', DATE_TRUNC('month', t2.closedate) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_in_month,
		CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'INBOUND' ELSE 'OUTBOUND' END as channel,
		SUM(CASE WHEN t2.stagename IN ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING') THEN 1 ELSE 0 END) as number_opportunities,
		ROUND((SUM(CASE WHEN t2.stagename IN ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING') THEN 1 ELSE 0 END)/EXTRACT(DAY FROM current_date))*DATE_PART('days', DATE_TRUNC('month', t2.closedate) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)) as forecast_opportunities
	
	FROM
	
		(SELECT

			*
			
		FROM
		
			salesforce.opportunity
			
		WHERE
		
			stagename in ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING')
			-- AND (closedate::date BETWEEN '2017-06-01' AND '2017-06-30')
			) as t2
	
	GROUP BY
	
		country,
		year,
		month,
		day,
		number_days_in_month,
		channel
		
	ORDER BY
	
		year,
		month) as t3
		
WHERE

	year = EXTRACT(YEAR FROM current_date)
	AND month =EXTRACT(MONTH FROM current_date)
		
GROUP BY

	country,
	year,
	month,
	channel;