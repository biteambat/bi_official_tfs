
-- Short Description: this table contains the data concerning the profitability of the BAT cleaners on each opportunity served

DROP TABLE IF EXISTS bi.profitability_BAT_opps;
CREATE TABLE bi.profitability_BAT_opps AS 

SELECT

	TO_CHAR(o.issued__c, 'YYYY-MM') as year_month,
	MIN(o.issued__c) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	oo.delivery_area__c as city,
	'B2B' as type,
	'Revenue' as kpi,
	'BAT' as sub_kpi_1,
	oo.company_name as sub_kpi_2,
	oo.opportunity as sub_kpi_3,
	oo.professional__c,
	oo.pro_name,
	SUM(ooo.order_duration__c) as hours,
	SUM(ooo.order_duration__c)*12.9 as cost_cleaner,
	MAX(o.amount__c)/1.19 as revenue,
	(MAX(o.amount__c)/1.19)/SUM(ooo.order_duration__c) revenue_per_hour,
	CASE WHEN MAX(o.amount__c)/1.19 > 0 THEN
				(MAX(o.amount__c)/1.19 - SUM(ooo.order_duration__c)*12.9)/MAX(o.amount__c)/1.19
		  ELSE 0
		  END as gpm
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	bi.order_provider oo
	
ON

	o.opportunity__c = oo.opportunityid
	
LEFT JOIN

	salesforce.order ooo
	
ON

	o.opportunity__c = ooo.opportunityid
	AND TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR(ooo.effectivedate, 'YYYY-MM')
	
WHERE

	oo.provider = 'BAT'
	AND ooo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND ooo.effectivedate <= current_date
	AND ooo.type = 'cleaning-b2b'
	AND ooo.effectivedate >= '2018-01-01'
	
GROUP BY

	year_month,
	LEFT(o.locale__c, 2),
	o.locale__c,
	oo.delivery_area__c,
	oo.company_name,
	oo.opportunity,
	oo.professional__c,
	oo.pro_name
	
ORDER BY

	year_month desc;