
-- contains the information regarding the activities of the partners on the portal (validation of orders, comments...)

DELETE FROM bi.monitoring_portal_partners WHERE year_month = TO_CHAR(current_date::date,'YYYY-MM');

INSERT INTO bi.monitoring_portal_partners

SELECT

	t5.*,
	t6.origin as origin_case,
	t6.subject,
	COUNT(DISTINCT t6.sfid_case) as number_cases

FROM

	(SELECT
	
	  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
	  MIN(effectivedate::date) as mindate,
	  MAX(effectivedate::date) as maxdate,
	  t3.delivery_areas__c as polygon,
	  t3.subcon as name_partner,
	  t3.sfid_partner,
	  COUNT(DISTINCT t4.sfid) as total_orders,
	  SUM(t4.order_duration__c) as hours,
	  COUNT(DISTINCT t4.professional__c) as number_prof,
	  COUNT(DISTINCT t4.contact__c) as number_customers,
	  SUM(CASE WHEN t4.status IN ('FULFILLED') THEN 1 ELSE 0 END) as fulfilled,
	  SUM(CASE WHEN t4.status IN ('INVOICED') THEN 1 ELSE 0 END) as invoiced,
	  SUM(CASE WHEN t4.status IN ('PENDING TO START') THEN 1 ELSE 0 END) as pts,
	  SUM(CASE WHEN t4.status IN ('NOSHOW CUSTOMER') THEN 1 ELSE 0 END) as noshow_customer,
	  SUM(CASE WHEN t4.status IN ('NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as noshow_professional,
	  SUM(CASE WHEN t4.status IN ('CANCELLED NOMANPOWER') THEN 1 ELSE 0 END) as nomanpower,
	  SUM(CASE WHEN t4.status LIKE '%ERROR%' THEN 1 ELSE 0 END) as error,
	  SUM(CASE WHEN t4.status LIKE '%MISTAKE%' THEN 1 ELSE 0 END) as mistake
  
	FROM
	
	  (SELECT
	  
	     t1.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t1
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t1.parentid)
	     
		WHERE 
		
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	   	and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
	      
	JOIN 
	
	  salesforce.order t4
	  
	ON
	
	  (t3.sfid = t4.professional__c)
	  
	WHERE
	
	  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
	  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
	  and LEFT(t4.locale__c,2) IN ('de')
	  and t4.effectivedate < current_date
	  AND t4.effectivedate >= '2018-01-01'
	  AND t4.type = 'cleaning-b2b'
	  AND t4.test__c IS FALSE
	
	GROUP BY
	
	  year_month,
	  polygon,
	  t3.subcon,
	  t3.sfid_partner
	  
	  
	ORDER BY
		
	  year_month,
	  polygon,
	  t3.subcon	) as t5
	  
LEFT JOIN	

	(SELECT

		TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
		MIN(o.createddate) as mindate,
		a.delivery_areas__c,
		a.company_name__c,
		a.sfid_partner,
		-- a.name,
		-- a.sfid as sfid_partner,
		o.sfid as sfid_case,
		o.accountid as sfid_cleaner,
		COUNT(DISTINCT o.contactid) as number_customers,
		COUNT(DISTINCT o.sfid) as number_cases,
		COUNT(DISTINCT oo.sfid) as number_orders,
		COUNT(DISTINCT oo.contact__c) as number_customers2,
		COUNT(DISTINCT oo.professional__c) as number_prof,
		SUM(oo.order_duration__c) as hours,
		SUM(CASE WHEN oo."status" = 'FULFILLED' THEN 1 ELSE 0 END) as orders_validated,
		o.origin,
		-- o.reason,
		o.subject
		
	FROM
	
		(SELECT
	  
	     t1.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t1
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t1.parentid)
	     
		WHERE 
		
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	   	and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') a
	   	
	LEFT JOIN
	
		salesforce."case" o
	
	ON
	
		 a.sfid = o.accountid
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		a.sfid = oo.professional__c
		AND TO_CHAR(o.createddate, 'YYYY-MM') = TO_CHAR(oo.effectivedate, 'YYYY-MM') 
		AND oo.effectivedate >= '2018-01-01'
		
	WHERE
	
		(o.origin = 'Partner portal' OR o.origin LIKE 'Partner - portal%')
		AND LOWER(o.description) NOT LIKE '%test%'
		AND LOWER(a.name) NOT LIKE '%test%'
		AND LOWER(a.company_name__c) NOT LIKE '%test%'
		AND LOWER(a.name) NOT LIKE '%book a cat%'
		AND oo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NO MANPOWER')
		
		AND LEFT(oo.locale__C,2) in ('de')
	   AND oo.test__c IS FALSE
		AND LOWER(o.description) NOT LIKE '%test%'
		
	GROUP BY
	
		year_month,
		a.company_name__c,
		a.sfid_partner,
		o.sfid,
		o.accountid,
		o.contactid,
		-- a.name,
		-- a.sfid,
		-- o.sfid,
		a.delivery_areas__c,
		o.origin,
		-- o.reason,
		o.subject
		
	ORDER BY
	
		year_month desc) as t6
		
ON

	t5.sfid_partner = t6.sfid_partner
	AND t5.year_month = t6.year_month
	
GROUP BY

	t5.year_month,
	t5.mindate,
	t5.maxdate,
	t5.polygon,
	t5.name_partner,
	t5.sfid_partner,
	t5.total_orders,
	t5.hours,
	t5.number_prof,
	t5.number_customers,
	t5.fulfilled,
	t5.invoiced,
	t5.pts,
	t5.noshow_customer,
	t5.noshow_professional,
	t5.nomanpower,
	t5.error,
	t5.mistake,
	t6.origin,
	t6.subject

ORDER BY

	year_month desc;