
DROP TABLE IF EXISTS bi.churn_rate;
CREATE TABLE bi.churn_rate AS
				
				
SELECT 	distinct main.year_month as year_month,
			-- On 27/03/2018 chandra: adding locale to the query to use as filter
			main.locale,
			new,
			total_bom,
			total_eom,
			round(((new.new - ((total_eom.total_eom) - (total_bom.total_bom))) / total_bom.total_bom) * 100, 2) AS churn_percent,
			lbs.left_before_start AS left_before_start_churn_percent
FROM bi.b2b_monthly_kpis main
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS new
		FROM bi.b2b_monthly_kpis _inn
			WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'New'
			GROUP BY year_month, locale
	) new
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS total_bom
		FROM bi.b2b_monthly_kpis _inn
			WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'Total BOM'
			GROUP BY year_month, locale
	) total_bom
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS total_eom
		FROM bi.b2b_monthly_kpis _inn
		WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'Total EOM'
			GROUP BY year_month, locale
	) total_eom
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	to_char(min_sign_date, 'YYYY-MM') AS signing_month,
					locale,
					round((SUM(CASE
							WHEN year_diff IS NULL AND status = 'LEFT' THEN unique_customers
								ELSE 0
							END) / sum(unique_customers)) * 100, 2) AS left_before_start

		FROM bi.closemonth_stats _inn
		WHERE _inn.signing_month = main.year_month
			AND _inn.locale = main.locale
		GROUP BY to_char(min_sign_date, 'YYYY-MM'), locale

	) lbs
ON TRUE
WHERE main."year_month" <= to_char(now(), 'YYYY-MM')
GROUP BY 
			main.year_month,
			main.locale,
			new.new,
			total_bom.total_bom,
			total_eom.total_eom,
			lbs.left_before_start
ORDER BY main.year_month desc;	