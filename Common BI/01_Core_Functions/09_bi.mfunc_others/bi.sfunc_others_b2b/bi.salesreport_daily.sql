
-- B2B Sales Dashboard

DROP TABLE IF EXISTS bi.salesreport_daily;
CREATE TABLE bi.salesreport_daily as 

-- Signed Deals - Actual
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Deals' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	ROUND(COUNT(*),2) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
	
-- Signed Revenue - Actual	
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Revenue' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	SUM(grand_total__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
-- Signed Hours - Actual		
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Hours' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	
	CASE WHEN LEFT(locale__c,2) ='de' THEN SUM(grand_total__c)/25.9 
	WHEN LEFT(locale__c,2) ='nl' THEN SUM(grand_total__c)/27.9 
	ELSE 0 END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;	



-- Signed Revenue - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
sub1.value*sub2.value AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Deals')
	AND sub2.kpi in ('Average Revenue')
		AND sub1.sub_kpi in ('Target');	
	
-- Signed Hours - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Hours' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Average PPH')
	AND sub1.sub_kpi in ('Target');
	
-- Average PPH - Actual

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Average Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Signed Deals')
	AND sub1.sub_kpi in ('Actual');