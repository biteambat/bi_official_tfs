
-- Short Description: this table contains gives insights on the efficiency of the keywords used in marketing (CVR%, spendings...)

DROP TABLE IF EXISTS bi.keywords_efficiency;
CREATE TABLE bi.keywords_efficiency AS 

SELECT

	o.year_month,
	MIN(o.date) as mindate,
	o.locale__c,
	o.city,
	o.keyword,
	COUNT(DISTINCT o.sfid) as leads_created,
	SUM(CASE WHEN o.converted_in_opportunity = 'Yes' THEN 1 ELSE 0 END) as opps_created,
	SUM(CASE WHEN o.stage__c IN ('ENDED') THEN 1 ELSE 0 END) as lead_ended,
	SUM(CASE WHEN o.stage__c NOT IN ('ENDED', 'WON', 'PENDING') THEN 1 ELSE 0 END) as pipeline,
	SUM(CASE WHEN o.stage_opp IN ('WON', 'PENDING') THEN 1 ELSE 0 END) as customer_converted,
	MAX(o.spending) as spending,
	SUM(o.grand_total__c) as grand_total

FROM

	bi.cvr_leads o
	
GROUP BY

	o.year_month,
	o.locale__c,
	o.city,
	o.keyword
	
ORDER BY
	
	year_month desc;