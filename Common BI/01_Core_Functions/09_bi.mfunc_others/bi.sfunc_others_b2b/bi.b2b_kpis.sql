
DROP TABLE IF EXISTS bi.b2b_kpis;
CREATE TABLE bi.b2b_kpis AS

	SELECT -- INVOICED GMV

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced GMV'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- INVOICED HOURS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced hours'::text as kpi,
		SUM(o.order_duration__c)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

/*
INSERT INTO bi.b2b_kpis -- MIN PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Min PPH'::text as kpi,
		ROUND(MIN(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;



INSERT INTO bi.b2b_kpis -- MAX PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Max PPH'::text as kpi,
		ROUND(MAX(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;*/

INSERT INTO bi.b2b_kpis -- AVERAGE PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Average PPH'::text as kpi,
		CASE WHEN SUM(o.order_duration__c) > 0 THEN ROUND((SUM(o.gmv_eur)/SUM(o.order_duration__c))::numeric,1) ELSE NULL END as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- ACTIVE CLEANERS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;


INSERT INTO bi.b2b_kpis

	SELECT
		year::int as year,
	  	month::int as month,
	  	NULL::int as week,
	  	NULL::date as weekdate,
	  	locale::text as locale,
		city::text as city,
		'Supplies revenue'::text as kpi,
		SUM(Supply_revenue) as value
	FROM(
			SELECT
				 EXTRACT(YEAR from t1.Effectivedate) as year,
				 EXTRACT(MONTH from t1.Effectivedate) as month,
				 left(t1.locale__c,2) as locale,
				 t1.polygon as city,
					t2.sfid,
				 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
				 bi.orders t1
			LEFT JOIN
				 Salesforce.Opportunity t2
			ON
				 (t2.sfid = t1.opportunityid)
			WHERE
				 type = '222' 
				 AND EXTRACT(YEAR from t1.Effectivedate) <= EXTRACT(YEAR from current_date)
				 AND EXTRACT(MONTH from t1.Effectivedate) <= EXTRACT(MONTH from current_date)
				 AND status NOT LIKE ('%CANCELLED%')
				 AND (t1.effectivedate >= '2018-01-01')
			GROUP BY
				 left(t1.locale__c,2),
				 t1.polygon,
				 t2.sfid,
				 EXTRACT(YEAR from t1.Effectivedate),
				 EXTRACT(MONTH from t1.Effectivedate)) as a
	GROUP BY
		year,
		month,
		locale,
		city
	ORDER BY
		year,
		month,
		locale,
		city

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced GMV'::text as kpi,
		ROUND(SUM(o.gmv_eur)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced GMV'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced hours'::text as kpi,
		ROUND(SUM(o.order_duration__c)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced hours'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Active cleaners'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;