
-- Signed Customers


DROP TABLE IF EXISTS bi.b2b_daily_kpis;
CREATE TABLE bi.b2b_daily_kpis as 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	
	locale;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale,
	sub_kpi;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	'Net New Hours on Platform' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	'Total gross revenue' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel Inbound' ELSE 'Sales Inbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
	and acquisition_channel__c in ('inbound','web')
GROUP BY
	date,
	locale,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'New Customer churn' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;

-- DE Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'43' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'48' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'35' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer service' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'5' as value;

-- NL Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- CH Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'17' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'3' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- New Customer End

-- New Hours Start

/*INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	left(locale,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(customers) > 0 THEN SUM(hours)/SUM(customers) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		CASE WHEN SUM (plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a
GROUP BY
	locale,
	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(customers) > 0 THEN SUM(hours)/SUM(customers) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(hours) > 0 THEN SUM(amount)/SUM(Hours) ELSE NULL END
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(hours) > 0 THEN SUM(amount)/SUM(Hours) ELSE NULL END
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;
*/

-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'889' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'791' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'97' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('19' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'234' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'270' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('28' as decimal) as value;


-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'306' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'272' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'34' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('39' as decimal) as value;


-- New Hours END

-- New Revenue Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(clients) > 0 THEN SUM(amount)/sum(clients) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
			SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as amount,
		COUNT(DISTINCT(sfid)) as clients
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20446' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'18197' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2249' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'426' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7481' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'6481' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'998' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'499' as value;

-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'11903' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'10594' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1309' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'700' as value;

-- marketing costs



INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-05-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-04-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-03-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','de','Marketing Stats','Cost Team','Actual','38352');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','de','Marketing Stats','Cost Team','Actual','35883');




INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','ch','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','ch','Marketing Stats','Cost Team','Actual','0');


INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','nl','Marketing Stats','Cost Team','Actual','4149');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','nl','Marketing Stats','Cost Team','Actual','3567');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-03-01','nl','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-04-01','nl','Marketing Stats','Cost Team','Actual','0');


-- INSERT INTO bi.b2b_daily_kpis
-- SELECT
-- 	mindate,
-- 	locale,
-- 	'Marketing Stats',
-- 	'Adwords Cost',	
	-- 'Actual',
-- 	costs
-- FROM

	-- (SELECT
	-- 	TO_CHAR(date,'YYYY-MM') as Month,
	-- 	locale,
	-- 	min(date) as mindate,
	-- 	SUM(sem_cost) as costs
	-- FROM
	-- 	bi.b2b_leadgencosts
	-- GROUP BY
	-- 	Month,
	-- 	locale) as a;


INSERT INTO bi.b2b_daily_kpis VALUES ('2018-01-01','de','Marketing Stats','Adwords Cost','Actual','20201');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-02-01','de','Marketing Stats','Adwords Cost','Actual','18805');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-03-01','de','Marketing Stats','Adwords Cost','Actual','9948');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-04-01','de','Marketing Stats','Adwords Cost','Actual','11350');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-05-01','de','Marketing Stats','Adwords Cost','Actual','12085');


INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-07-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*200*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and sub_kpi = 'Outbound'
	and left(locale,2) in ('de','nl')
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;


INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*40*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and left(locale,2) in ('de','nl')
	and sub_kpi = 'Inbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team Inbound',
	'Actual',
	sum(value)*40*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and left(locale,2) in ('de','nl')
	and sub_kpi = 'Inbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
	


INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t2.value) > 0 THEN SUM(t1.value)/SUM(t2.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date,
	t1.locale;
	
INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	'All' as locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t2.value) > 0 THEN SUM(t1.value)/SUM(t2.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date;


INSERT INTO  bi.b2b_daily_kpis
	SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t1.value) > 0 THEN SUM(t2.value)/SUM(t1.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	date,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	min(date) as date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	year_month,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;
	
	
DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team' WHERE sub_kpi = 	'Cost Team Est'; 

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Inbound Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	min(date) as date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team Inbound'
GROUP BY
	locale,
	year_month,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team Inbound' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team Inbound' WHERE sub_kpi = 	'Cost Team Inbound Est'; 


-- New Revenue END

-- TOtal Customers START

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	LEFT(t1.locale__c,2) as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.orders t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunityid = t2.sfid)
WHERE
	TO_CHAR(effectivedate::date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and (status not like '%CANCELLED%' and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL'))
	and order_type = '2'
GROUP BY
	Locale;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'185' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'ch' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'27' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'33' as value;


-- Summary

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Runrate on Target% till now' as text) as type,
	CASE WHEN MAX(alldays) > 0 THEN 

		CASE WHEN (SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays)))*100 > 0 THEN
	
			round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/(SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays))))*100,0) ELSE 0 END 


	ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
	(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
		sub_kpi,
	CAST('Runrate on Target%' as text) as type,
	CASE WHEN SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END) > 0 THEN round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Achieved' as varchar) as type,
	CASE WHEN MAX(current_days) > 0 THEN CAST(SUM(Value) as decimal)/MAX(current_days) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Target' as varchar) as type,
	CASE WHEN MAX(alldays) > 0 THEN CAST(SUM(Value) as decimal)/MAX(alldays) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Target'
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	date,
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Projected Achievement' as varchar) as type,
	(CASE WHEN MAX(current_days) > 0 THEN CAST(SUM(Value) as decimal)/MAX(current_days) ELSE NULL END)*(MAX(alldays)-MAX(current_days))+SUM(Value) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'Current Days' as type,
	MAX(current_days) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;
 	
 INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'All Days' as type,
	MAX(alldays) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Touched Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;

 INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Generated Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	'All' as locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month) as a
GROUP BY
	date;		
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Signed Customers') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(opportunities) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	