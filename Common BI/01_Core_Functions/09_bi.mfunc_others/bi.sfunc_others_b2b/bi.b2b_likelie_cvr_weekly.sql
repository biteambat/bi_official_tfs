
DROP TABLE IF EXISTS bi.b2b_likelie_cvr_weekly;
CREATE TABLE bi.b2b_likelie_cvr_weekly as 
SELECT
	EXTRACT(YEAR FROM t1.createddate) as yearnum,
	EXTRACT(WEEK FROM t1.createddate) as weeknum,
	LEFT(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	min(t1.createddate::date) as date,
	conversion_page,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	EXTRACT(YEAR FROM t1.createddate),
	EXTRACT(WEEK FROM t1.createddate),
	LEFT(t1.locale__c,2),
	Source_Channel,
	landing_page,
	conversion_page

ORDER BY 
	yearnum desc,
	weeknum desc,
	locale asc

;