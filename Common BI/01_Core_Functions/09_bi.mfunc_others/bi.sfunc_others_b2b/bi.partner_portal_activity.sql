
-- Short Description: table containing the activity of the partners on the portal and the use of the application by the cleaners

DROP TABLE IF EXISTS bi.partner_portal_activity;
CREATE TABLE bi.partner_portal_activity AS      

SELECT

    t4.company_name__c,
    t4.delivery_areas__c,
    t4.parentid,
    t4.sfid_cleaner,
    t4.last_order,
    t4.is_active,
    t4.cleaner_w_app,
    -- t4.*,
    MAX(t5.login) as last_login


FROM    
    
    (SELECT
    
        t2.*,
        CASE WHEN (t2.last_order > (current_date -91) AND t2.last_order <= current_date) THEN 'Yes' ELSE 'No' END as is_active,
        CASE WHEN t3.deviceid__c IS NOT NULL THEN 'Yes' ELSE 'No' END as cleaner_w_app,
        t3.last_login__c
    
    FROM
            
        (SELECT
        
            t1.company_name__c,
            t1.delivery_areas__c,
            t1.parentid,
            t1.sfid as sfid_cleaner,
            t1.last_order
        
        FROM    
            
            (SELECT 
                
                a.sfid,
                a.delivery_areas__c,
                a.billingcity,
                a.shippingcity,
                a.parentid,
                a.company_name__c,
                a.status__c,
                MAX(o.effectivedate) as last_order
            
            FROM
                
                salesforce.account a
                
            LEFT JOIN
            
                salesforce.account b
                
            ON 
            
                a.company_name__c = b.name
                
            LEFT JOIN
            
                salesforce.order o
                
            ON
            
                a.sfid = o.professional__c 
                
            WHERE
                
                LOWER(a.name) NOT LIKE '%test%'
                AND a.test__c IS FALSE
                AND b.test__c IS FALSE
                AND LEFT(a.locale__c, 2) = 'de'
                AND a.company_name__c NOT IN ('##')
                AND a.status__c IN ('BETA', 'ACTIVE', 'FROZEN')
                AND a.parentid IS NOT NULL
                AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
                AND o.effectivedate <= current_date
                AND o.effectivedate >= '2018-01-01'
                
            GROUP BY
            
                a.sfid,
                a.delivery_areas__c,
                a.billingcity,
                a.shippingcity,
                a.parentid,
                a.company_name__c,
                a.status__c) as t1
                
        GROUP BY
        
            t1.company_name__c,
            t1.delivery_areas__c,
            t1.parentid,
            t1.last_order,
            t1.sfid) as t2
            
    LEFT JOIN
        
        (SELECT 
        
            parent.sfid, 
            parent.name,
            prof.sfid as sfid_cleaner,
            prof.deviceid__c,
            parent.last_login__c,
            COUNT(prof.sfid) as cleaners_w_app
        
        FROM 
        
            salesforce.account prof
        
        JOIN 
        
            salesforce.account parent 
            
        ON 
        
            prof.parentid = parent.sfid
            AND prof.status__c IN ('BETA', 'ACTIVE', 'FROZEN')
            AND prof.type__c LIKE '%cleaning-b2b%' 
            AND prof.locale__c LIKE 'de-%'
            AND prof.test__c = False 
            AND parent.test__c = False
            AND prof.deviceid__c IS NOT NULL
        
        GROUP BY 
        
            parent.sfid, 
            prof.deviceid__c,
            prof.sfid,
            parent.last_login__c,
            parent.name
            
        ORDER BY
        
            parent.name) as t3
            
    ON
    
        t2.company_name__c = t3.name
        AND t2.sfid_cleaner = t3.sfid_cleaner) as t4
        
LEFT JOIN

    (SELECT 

        name, 
        MAX(last_login__c) as login 
        
    FROM 
    
        salesforce.account 
    
    WHERE 
    
        last_login__c > DATE '2019-01-01' 
        
    GROUP BY 
    
        name 
        
    order by 
    
        login) as t5
        
ON

    t4.company_name__c = t5.name    
        
GROUP BY

    t4.company_name__c,
    t4.delivery_areas__c,
    t4.parentid,
    t4.sfid_cleaner,
    t4.last_order,
    t4.is_active,
    t4.cleaner_w_app;