
-- Short Description: latest valid (INVOICED,FULFILLED or CANCELLED CUSTOMER) order and information about provider (BAT or Partner)

DROP TABLE IF EXISTS 		bi.order_provider;
CREATE TABLE 				bi.order_provider AS 


SELECT DISTINCT ON 	(orders.opportunityid)
			orders.delivery_area__c
			, orders.sfid AS orderid
			, orders.opportunityid
			, opp.name as opportunity
			, orders.effectivedate
			, orders.professional__c
			, prof.pro_name
			, prof.provider
			, prof.company_name
			, prof.company_id as sfid_partner

FROM 		salesforce.order orders

LEFT JOIN 	salesforce.opportunity opp 					ON ( orders.opportunityid = opp.sfid)
LEFT JOIN 	( SELECT 
					pro.sfid															pro_id
					, pro.name															pro_name
					, pro.company_name__c												pro_company_name
					, company.sfid														company_id
					, company.name														company_name
					, CASE 	WHEN company.sfid 	= '0012000001TDMgGAAX' 	THEN 'BAT'
							WHEN company.sfid 	IS NULL					THEN 'unknown'
							ELSE 'Partner' 								END 			provider
					-- ,*
			FROM 		salesforce.account pro
			LEFT JOIN 	salesforce.account company		ON (pro.parentid = company.sfid)
			) AS prof									ON (prof.pro_id = orders.professional__c)

WHERE 		orders.status IN ('INVOICED','FULFILLED','PENDING TO START','NOSHOW CUSTOMER')

ORDER BY 	orders.opportunityid, 
			orders.effectivedate DESC
;