
DROP TABLE IF EXISTS bi.b2b_likelie_cvr;
CREATE Table bi.b2b_likelie_cvr as 
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	left(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	min(t1.createddate::date) as date,
	conversion_page,
	lead_source,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	COUNT(DISTINCT(CASE WHEN t2.stagename IN ('LOST') THEN t2.sfid ELSE NULL END)) as lost,
	COUNT(DISTINCT(CASE WHEN t2.stagename IN ('OFFER SENT', 'VERBAL CONFIRMATION') THEN t2.sfid ELSE NULL END)) as offer_sent_verbal_conf,
	COUNT(DISTINCT(CASE WHEN t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING') THEN t2.sfid ELSE NULL END)) as signed_opps,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c NOT IN ('outbound')
	AND t1.test__c IS FALSE
	AND t1.company_name__c NOT LIKE '%test%'
	AND t1.company_name__c NOT LIKE '%bookatiger%'
	AND (t1.lost_reason__c NOT IN ('invalid - sem duplicate') OR t1.lost_reason__c IS NULL)
	AND LEFT(t1.locale__c, 2) = 'de'
	AND t1.createddate >= '2018-01-01'

	-- AND t2.test__c IS FALSE
	-- and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale,
	lead_source,
	Source_Channel,
	landing_page,
	conversion_page;