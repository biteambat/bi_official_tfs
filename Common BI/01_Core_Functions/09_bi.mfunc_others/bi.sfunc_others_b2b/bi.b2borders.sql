
DROP TABLE IF EXISTS bi.b2borders;
	CREATE TABLE bi.b2borders as 
	SELECT
		t1.contact__c,
		t2.name,
		t2.sfid as opportunity_id,
		t2.delivery_area__c as polygon,
		t2.email__c,
		t2.locale__c,
		t2.contact_name__c,
		CASE 
 		WHEN CAST(EXTRACT(YEAR FROM effectivedate::date) as integer) = CAST(EXTRACT(YEAR FROM first_order_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM effectivedate::date) as integer)- CAST(EXTRACT(MONTH FROM first_order_date::date) as integer) 
  		WHEN CAST(EXTRACT(YEAR FROM effectivedate::date) as integer) != CAST(EXTRACT(YEAR FROM first_order_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM effectivedate::date) as integer)-CAST(EXTRACT(YEAR FROM first_order_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM effectivedate::date) as integer)- CAST(EXTRACT(MONTH FROM first_order_date::date) as integer) )
  		ELSE 0 END as returning_month,
		to_char(Effectivedate::date,'YYYY-MM') as Year_month,
		min(effectivedate::Date) as Date,
		SUM(t1.GMV__c)+MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as total_amount,
		SUM(t1.GMV__c) as Cleaning_Gross_Revenue,
		MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue,
		COUNT(1) as Total_Order_Count, 
		first_order_date,
		TO_CHAR(first_order_date,'YYYY-MM') as cohort,
		SUM(t1.Order_Duration__C) as Total_Invoiced_Hours,
		CASE WHEN SUM(Order_Duration__c) > 0 THEN (SUM(t1.GMV__c))/SUM(Order_Duration__c) ELSE NULL END as PPH,
		COUNT(1),
		SUM(CASE WHEN t3.company_name like '%BAT%' or t3.company_name like '%BOOK%' THEN Order_Duration__c ELSE 0 END) as bat_hour_share,
		SUM(Order_Duration__c) as total_hours,
		grand_total__c,
		CASE WHEN to_char(Effectivedate::date,'YYYY-MM') = to_char(first_order_date,'YYYY-MM') THEN 'New Customer' ELSE 'Existing Customer' End as customer_type,
		t2.closedate::date as sign_date,
		MAX(CASE WHEN to_char(Effectivedate::date,'YYYY-MM') = to_char(first_order_date,'YYYY-MM') THEN (32-EXTRACT(DAY FROM first_order_date))/31*grand_total__c ELSE grand_total__c END) as grand_total_calc
	FROM
		salesforce.order t1
	LEFT JOIN
		Salesforce.Opportunity t2
	ON
		(t2.sfid = t1.opportunityid)
	LEFT JOIN
		(SELECT
 		a.*,
 		t2.name as company_name,
 		t2.pph__c as sub_pph
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED')  and a.test__c = '0' and a.name not like '%test%') as t3
   	ON
   		(t1.professional__c = t3.sfid)
   	LEFT JOIN
   		(SELECT
   			opportunityid,
   			min(effectivedate::date) as first_order_Date
   		FROM
   			bi.orders
   		WHERE
   			status not like '%CANCELLED%'
   		GROUP BY
   			opportunityid) as t4
   	ON
   		(t1.opportunityid = t4.opportunityid)
   	
   	
	WHERE
		t1.type = 'cleaning-b2b'
		and t1.test__c = '0'
		and t1.status not like '%CANCELLED%'

	GROUP BY
		year_month,
		t2.delivery_area__c,
		t2.name,
		cohort,
		t2.email__c,
		t2.sfid,
		t2.locale__c,
		contact__c,
		contact_name__c,
		returning_month,
		first_order_date,
		grand_total__c,
		customer_type,
		sign_date;