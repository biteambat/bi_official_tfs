
-- Short Description: this table contains the info concerning the opportunities created every working day
-- Created on: 22/11/2018

DROP TABLE IF EXISTS bi.cvr_opps;
CREATE TABLE bi.cvr_opps AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as createddate,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END as converted_in_opportunity,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c as closed_by_id,
	u.name as owner_name,
	o.delivery_area__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.sfid = ooo.sfid
	
LEFT JOIN

	salesforce.likeli__C oooo
	
ON

	o.sfid = oooo.opportunity__c
	
LEFT JOIN

	salesforce.user u
	
ON

	o.closed_by__c = u.sfid
	
WHERE

	o.test__c IS FALSE
	AND oooo.acquisition_channel__c IN ('web', 'inbound')
	AND LEFT(o.locale__c, 2) = 'de'
	
GROUP BY

	year_month,
	o.createddate,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	o.acquisition_channel__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c,
	u.name,
	o.delivery_area__c
	
ORDER BY

	TO_CHAR(o.createddate, 'YYYY-MM-DD') desc;