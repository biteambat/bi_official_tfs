
DROP TABLE IF EXISTS bi.variation_grand_total;
CREATE TABLE bi.variation_grand_total as
		
		
SELECT

	t4.year_week,
	MIN(t4.mindate) as mindate,
	SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as oldvalue,
	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) as newvalue,

	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) - SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as variation

FROM
			
	(SELECT
	
		t1.year_week,
		t1.opportunityid,
		t1.mindate,
		t1.maxdate,
		t2.oldvalue,
		t3.newvalue
				
	FROM
	
		
		(SELECT -- Here I create the list of first and last changes, every week, for every opportunity
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			MIN(o.createddate) as mindate,
			MAX(o.createddate) as maxdate,
			o.opportunityid
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('1')
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.opportunityid
			
		ORDER BY
		
			year_week desc) as t1
			
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every week
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_week desc) as t2
		
	ON
	
		t1.mindate = t2.createddate
		AND t1.opportunityid = t2.opportunityid
	
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every week
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_week desc) as t3
		
	ON
	
		t1.maxdate = t3.createddate
		AND t1.opportunityid = t3.opportunityid
		
	ORDER BY
	
		t1.year_week desc) as t4
		
GROUP BY

	t4.year_week
	
ORDER BY 

	t4.year_week desc;