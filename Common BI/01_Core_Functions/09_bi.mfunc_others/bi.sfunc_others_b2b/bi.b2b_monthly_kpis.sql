
DROP TABLE IF EXISTS bi.b2b_monthly_kpis;
CREATE TABLE bi.b2b_monthly_kpis as 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(t2.sfid)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid and LOWER(t2.name) not like '%test%')
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL','WAITING FOR RESCHEDULE')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
		
INSERT INTO bi.b2b_monthly_kpis 	
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as revenue
FROM
	bi.b2borders 
GROUP BY
	year_month,
	locale,
	city;
	
	

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(ORder_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New (Signed Before)' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	TO_CHAR(sign_date,'YYYY-MM') < Year_MOnth
	and customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;
	
INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New (Signed same month)' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	TO_CHAR(sign_date,'YYYY-MM') = Year_MOnth
	and customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;	
	
INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(CASE WHEN TO_CHAR(Effectivedate::date,'YYYY-MM') = TO_CHAR(first_order::date,'YYYY-MM') THEN Order_Duration__c ELSE NULL END) as value
	
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
LEFT JOIN
(SELECT
	t2.sfid,
	min(effectivedate::date) as first_order
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	t2.sfid) as t3
ON
	(t2.sfid = t3.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;	

INSERT INTO bi.b2b_monthly_kpis	
SELECT
	year_month,
	min(date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN TO_CHAR(first_date::date,'YYYY-MM') = TO_CHAR(date::date,'YYYY-MM') THEN (CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) ELSE 0 END) as revenue

FROM
	bi.b2borders a
LEFT JOIN
	(SELECT
		name,
		min(date) as first_date
	FROM
		bi.b2borders	
	GROUP BY
		name) as b
ON
	(a.name = b.name)
GROUP BY
	year_month,
	locale,
	city;

------------------------------------------------------------------------------------------------------- Revenue Churned Opps 27/03/2018

INSERT INTO bi.b2b_monthly_kpis
SELECT

	to_char(churn_date,'YYYY-MM') as year_month,
	min(churn_date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CAST('-' as varchar) as city,
	CAST('Churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'B2B' as customer_type,
	SUM(total) as revenue

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		t3.potential as total
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND (o.effectivedate >= '2018-01-01')
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		-- EXTRACT(WEEK FROM oo.createddate) = 6
		-- AND EXTRACT(year FROM oo.createddate) = 2018
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		-- AND oo.name NOT LIKE '%test%'
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid

	LEFT JOIN

		bi.potential_revenue_per_opp t3

	ON

		t1.opportunityid = t3.opportunityid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t3.potential
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_month,
	locale,
	city
	
ORDER BY

	year_month desc;
		
------------------------------------------------------------------------------------------------------- Revenue Churned Opps 27/03/2018

INSERT INTO bi.b2b_monthly_kpis
SELECT

	to_char(churn_date,'YYYY-MM') as year_month,
	min(churn_date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CAST('-' as varchar) as city,
	CAST('Churned Opps' as varchar) as kpi,
	CAST('Customer' as varchar) as type,
	COUNT(DISTINCT t3.opportunityid) as customers
	

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		t3.potential as total
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND (o.effectivedate >= '2018-01-01')
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		-- EXTRACT(WEEK FROM oo.createddate) = 6
		-- AND EXTRACT(year FROM oo.createddate) = 2018
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		-- AND oo.name NOT LIKE '%test%'
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid

	LEFT JOIN

		bi.potential_revenue_per_opp t3

	ON

		t1.opportunityid = t3.opportunityid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t3.potential
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_month,
	locale,
	city
	
ORDER BY

	year_month desc;
		

-------------------------------------------------------------------------------------------------------

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(t2.sfid)) as sfid
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis 	
SELECT
	TO_CHAR(date::date + Interval '1 Month','YYYY-MM') as Year_Months,
	min(date::date + Interval '1 Month')::date as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as revenue
FROM
	bi.b2borders 
GROUP BY
	year_months,
	locale,
	city;


INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(Order_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;