
DROP TABLE IF EXISTS bi.working_days_monthly;
CREATE TABLE bi.working_days_monthly as 

SELECT

	t3.year_month,
	t3.date,
	t3.weekday,
	-- t3.number_weekday,
	t3.day_type,
	t3.working_day_number,
	t3.non_working_day_number,
	CASE WHEN t3.day_type = 'Working Day'
	     THEN CONCAT(t3.day_type, ' - ', t3.working_day_number)
		  ELSE CONCAT(t3.day_type, ' - ', t3.non_working_day_number)
		  END as day_type_number,
	t3.number_working_days_in_month
	
FROM	
		
	(SELECT
	
		CASE WHEN t0.day_type = 'Working Day' THEN ROW_NUMBER() OVER 
		                 (PARTITION BY t0.year_month
		                  ORDER BY t0.day_type desc,
								         t0.date asc) ELSE 0 END as working_day_number,
		CASE WHEN t0.day_type = 'Non Working Day' THEN ROW_NUMBER() OVER 
		                 (PARTITION BY t0.year_month
		                  ORDER BY t0.day_type asc,
								         t0.date asc) ELSE 0 END as non_working_day_number,
		t0.year_month,
		t0.date,
		t0.weekday,
		t0.number_weekday,
		t0.day_type,
		t2.number_working_days_in_month
	
	FROM
		
		(SELECT 
		
			TO_CHAR(i, 'YYYY-MM') as year_month,
			i::date as date,
			TO_CHAR(i, 'day') as weekday,
			EXTRACT(DOW FROM i) as number_weekday,
			CASE WHEN EXTRACT(DOW FROM i) IN ('1', '2', '3', '4', '5') 
			          AND i NOT IN ('2018-01-01', '2018-03-30', '2018-04-02', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03', '2018-12-25', '2018-12-26',
							            '2019-01-01', '2019-04-19', '2019-04-22', '2019-05-01', '2019-05-30', '2019-06-10', '2019-10-03', '2019-12-25', '2019-12-26',
							            '2020-01-01', '2020-04-10', '2020-04-13', '2020-05-01', '2020-05-21', '2020-06-01', '2020-10-03', '2020-12-25', '2020-12-26',
							            '2021-01-01', '2021-04-02', '2021-04-05', '2021-05-01', '2021-05-13', '2021-05-24', '2021-10-03', '2021-12-25', '2021-12-26')
				  THEN 'Working Day' ELSE 'Non Working Day' END as day_type
			
		FROM 
		
			generate_series('2017-12-31', '2022-01-01', '1 day'::interval) i
			
		ORDER BY
		
			date desc) as t0
			
	LEFT JOIN
	
		(SELECT	
			
			year_month,
			SUM(CASE WHEN day_type = 'Working Day' THEN 1 ELSE 0 END) as number_working_days_in_month
		
		FROM
		
			
			(SELECT 
			
				TO_CHAR(i, 'YYYY-MM') as year_month,
				i::date as date,
				TO_CHAR(i, 'day') as weekday,
				EXTRACT(DOW FROM i) as number_weekday,
				CASE WHEN EXTRACT(DOW FROM i) IN ('1', '2', '3', '4', '5') 
				          AND i NOT IN ('2018-01-01', '2018-03-30', '2018-04-02', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03', '2018-12-25', '2018-12-26',
							               '2019-01-01', '2019-04-19', '2019-04-22', '2019-05-01', '2019-05-30', '2019-06-10', '2019-10-03', '2019-12-25', '2019-12-26',
							               '2020-01-01', '2020-04-10', '2020-04-13', '2020-05-01', '2020-05-21', '2020-06-01', '2020-10-03', '2020-12-25', '2020-12-26',
							               '2021-01-01', '2021-04-02', '2021-04-05', '2021-05-01', '2021-05-13', '2021-05-24', '2021-10-03', '2021-12-25', '2021-12-26')
												
				              
				
					  THEN 'Working Day' ELSE 'Non Working Day' END as day_type
				
			FROM 
			
				generate_series('2017-12-31', '2021-01-01', '1 day'::interval) i
				
			ORDER BY
			
				date desc) as t1
				
		GROUP BY
		
			year_month
			
		ORDER BY
		
			year_month desc) as t2
	
	ON
	
		t0.year_month = t2.year_month
		
	ORDER BY
	
		-- t0.day_type desc,
		t0.date desc) as t3;