
DROP TABLE IF EXISTS bi.closemonth_stats;
CREATE TABLE bi.closemonth_stats as 
SELECT
	Signing_Month,
	start_month,
	locale,
	min(sign_date) as min_sign_date,
	((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) as Year_Diff,
	CASE WHEN stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING') and status__c in ('RESIGNED','CANCELLED') THEN 'LEFT' ELSE 'PENDING' END as status,
	SUM(unique_customers) as Unique_Customers
FROM(
SELECt
	to_char(closedate::date,'YYYY-MM') as Signing_Month,
	TO_CHAR(Order_Start::date,'YYYY-MM') as Start_Month,
	min(closedate::date) as Sign_Date,
	min(order_Start::Date) as Start_date,
	stagename,
	status__c,
	LEFT(Locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as unique_customers
FROM
	Salesforce.Opportunity t1
LEFT JOIN
	(SELECT
		t3.opportunityid,
		min(effectivedate::date) as Order_Start
	FROM
		bi.orders t3
	WHERE
		t3.status not like '%CANCELLED%'
		AND (t3.effectivedate >= '2018-01-01')
	GROUP BY
		t3.opportunityid) as t2
ON
	(t1.sfid = t2.opportunityid)
WHERE
	t1.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Signing_Month,
	Start_Month,
	status__c,
	locale,
	stagename) as a
GROUP BY
	Signing_Month,
	Start_Month,
	status,
	locale,
	Year_Diff
HAVING
	((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) > -1 or ((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) is null;