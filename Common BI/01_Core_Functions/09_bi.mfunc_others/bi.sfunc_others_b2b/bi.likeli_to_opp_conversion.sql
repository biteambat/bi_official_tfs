
DROP TABLE IF EXISTS bi.likeli_to_opp_conversion;
CREATE TABLE bi.likeli_to_opp_conversion as
SELECT
	t2.sfid as opp_id,
	t1.acquisition_tracking_id__c,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t1.createddate::date as lead_created,
	t2.createddate::date as opp_created,
	t2.closedate::date as signed_date,
	extract(day from age(t2.closedate::date,t2.createddate::date)) as time_to_convert,
	t4.potential as Monthly_Amount,
	CASE WHEN t2.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING') THEN 1 ELSE 0 END as Conversion_Signed,
	CASE WHEN t2.stagename not in  ('NEGOTIATION','NEW','OFFER SENT','POSITIVE FEEDBACK','PENDING','WON') THEN 1 ELSE 0 END as Conversion_Finished,
	CASE WHEN t3.hours > 0 THEN 1 ELSE 0 END as Conversion_Started
FROM
	salesforce.likeli__c t1
JOIN 
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)

LEFT JOIN
	bi.potential_revenue_per_opp t4

ON

	t1.opportunity__c = t4.opportunityid

LEFT JOIN
	(SELECT
		t3.opportunity_id,
		sum(total_hours) as hours
	FROM
		bi.b2borders t3
	GROUP BY
		opportunity_id) as t3
ON
	(t2.sfid = t3.opportunity_id);