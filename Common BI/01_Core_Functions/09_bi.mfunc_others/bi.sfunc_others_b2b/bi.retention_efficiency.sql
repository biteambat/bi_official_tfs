
-- Short Description: this table gives insights concerning the retention efficiency

DROP TABLE IF EXISTS bi.retention_efficiency;
CREATE TABLE bi.retention_efficiency AS 

SELECT
	
	DISTINCT o.opportunityid,
	MAX(o.createddate)::date as retention_date,
	oo.name,
	TRUNC(t1.potential::numeric, 2) as potential,
	CASE WHEN t1.potential < 250 THEN 'Very Small'
			  WHEN t1.potential >= 250 AND t1.potential < 500 THEN 'Small'
			  WHEN t1.potential >= 500 AND t1.potential < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END,
	oo.delivery_area__c,
	ooo.provider,
	ooo.company_name,
	ooo.pro_name,
	oo.status__c,
	oo.churn_reason__c,
	MAX(t2.resignation_date__c)::date as resignation_date,
	MAX(t2.confirmed_end__c)::date as confirmed_end

FROM

	salesforce.opportunityfieldhistory o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
LEFT JOIN

	bi.order_provider ooo
	
ON

	o.opportunityid = ooo.opportunityid
	
LEFT JOIN

	bi.potential_revenue_per_opp t1
	
ON 

	o.opportunityid = t1.opportunityid
	
LEFT JOIN

	salesforce.contract__c t2
	
ON

	o.opportunityid = t2.opportunity__c
	
-- LEFT JOIN

	-- salesforce.natterbox_call_reporting_object__c t3
	
-- ON

	-- o.opportunityid = t3.opportunity__c
	
WHERE

	o.newvalue = 'RETENTION'
	AND oo.test__c IS FALSE
	AND t2.service_type__c LIKE 'maintenance cleaning'
	-- AND t2.active__c IS TRUE
	-- AND t3.calldirection__c = 'Outbound'
	-- AND t3.callerlastname__c = 'Kharoo'
	-- AND t3.createddate >= o.createddate
	
GROUP BY

	o.opportunityid,
	oo.name,
	t1.potential,
	CASE WHEN t1.potential < 250 THEN 'Very Small'
			  WHEN t1.potential >= 250 AND t1.potential < 500 THEN 'Small'
			  WHEN t1.potential >= 500 AND t1.potential < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END,
	oo.delivery_area__c,
	ooo.provider,
	ooo.company_name,
	ooo.pro_name,
	oo.status__c,
	oo.churn_reason__c
	
ORDER BY

	retention_date desc;