
DELETE FROM bi.b2b_pph_eff;	
	
INSERT INTO bi.b2b_pph_eff	
SELECT
	opportunity_id,
	year_month,
	grand_total__c,
	grand_total_calc,
	total_invoiced_hours,
	grand_total_calc/total_invoiced_hours as Eff_PPH
FROM
	bi.b2borders
WHERE
	year_month > '2017-02'
	and grand_total_calc > 0;