
DROP TABLE IF EXISTS bi.b2b_likelies;
CREATE TABLE bi.b2b_likelies AS

SELECT
		t.*,

		CASE 	


				WHEN LOWER(t.acquisition_channel_ref__c) = 'anygrowth' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'

				WHEN LOWER(t.acquisition_channel_ref__c) = 'reveal' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'

				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
				
				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
				
				WHEN (t.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'

				WHEN ((lower(t.acquisition_channel__c) = 'inbound') AND lower(t.acquisition_tracking_id__c) = 'classifieds')					
				THEN 'Classifieds'
							
				WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text

		            AND t.createddate::date <= '2017-02-08'

		        THEN 'SEM Brand'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goob%')

		        	AND t.createddate::date > '2017-02-08'
		        	AND ((t.acquisition_channel_params__c LIKE '%goob%') AND (t.acquisition_channel_params__c LIKE '%bt:b2b%' OR t.acquisition_channel_params__c LIKE '%bt: b2b%' OR t.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
		        	)

		        THEN 'SEM Brand B2B'::text

		        WHEN (((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goob%')

		        	AND t.createddate::date > '2017-02-08'
					AND ((t.acquisition_channel_params__c LIKE '%goob%') AND (t.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%' ))
		        	
		        THEN 'SEM Brand B2C'::text
		        

		        WHEN (((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date <= '2017-02-08'
		        THEN 'SEM'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date > '2017-02-08'
		        	AND (((t.acquisition_channel_params__c LIKE '%goog%') AND (t.acquisition_channel_params__c LIKE '%bt: b2b%' OR t.acquisition_channel_params__c LIKE '%bt:b2b%' OR t.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
		        	))
		        	 OR
		            (t.acquisition_tracking_id__c LIKE 'unbounce_reinigungdirekt' AND t.createddate::date <= '2017-11-15')
		            OR
		            	acquisition_tracking_id__c LIKE 'rd%'

		            OR
		            	acquisition_channel_params__c LIKE '%ysm%'

		        THEN 'SEM B2B'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date > '2017-02-08' 
		        	AND ((t.acquisition_channel_params__c LIKE '%goog%') AND (t.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%')))
		        	OR t.acquisition_channel_params__c::text LIKE '%goog%'
		        THEN 'SEM B2C'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text


		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text
		            AND LOWER(acquisition_tracking_id__c) LIKE '%b2b seo page form&'
		        THEN 'SEO B2B'::text
		        

		        WHEN ((t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text
					AND LOWER(acquisition_tracking_id__c) NOT LIKE '%b2b seo page form&')

		        	OR
		        		((acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL AND (acquisition_tracking_id__c LIKE '%b2b%' OR acquisition_tracking_id__c LIKE '%tfs%')))

		        THEN 'SEO'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		            THEN 'SEO Brand'::text
		            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
		                OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text 
		        THEN 'Newsletter'::text
		        

		        WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text)
		        	OR (acquisition_channel__c IS NULL AND acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL)
		        THEN 'DTI'::text

		        
        

        		
		        
		        -- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'Reinigungsdirekt'

		        ---------------------------------------------------------------------------------------------------------

		        -- WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'b2c home form'
		        -- WHEN acquisition_tracking_id__c like '%b2c home funnel%' THEN 'b2c home funnel'
		        -- WHEN acquisition_tracking_id__c like '%b2c funnel availability request%' THEN 'b2c funnel availability request'

		        -- WHEN acquisition_tracking_id__c like '%b2b funnel%' THEN 'b2b funnel'
		        -- WHEN acquisition_tracking_id__c like '%availability request%' THEN 'availability request'
		        -- WHEN acquisition_tracking_id__c like '%price page funnel%' THEN 'price page funnel'
		        -- WHEN acquisition_tracking_id__c like '%help page funnel%' THEN 'help page funnel'
		        -- WHEN acquisition_tracking_id__c like '%about page funnel%' THEN 'about page funnel'
		        -- WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'b2b home form'
		        -- WHEN acquisition_tracking_id__c like '%b2b home funnel%' THEN 'b2b home funnel'
		        -- WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'chatbot'
		        -- WHEN acquisition_tracking_id__c like '%b2b amp%' THEN 'b2b amp'
		        -- WHEN acquisition_tracking_id__c like '%footer property management cb%' THEN 'footer property management cb'
		        -- WHEN acquisition_tracking_id__c like '%footer newsletter b2b%' THEN 'footer newsletter b2b'
		        -- WHEN acquisition_tracking_id__c like '%price page form%' THEN 'price page form'
		        -- WHEN acquisition_tracking_id__c like '%help page form%' THEN 'help page form'
		        -- WHEN acquisition_tracking_id__c like '%about page form%' THEN 'about page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b contact page form%' THEN 'b2b contact page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b service page form%' THEN 'b2b service page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b about page form%' THEN 'b2b about page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b floating cb%' THEN 'b2b floating cb'
		        -- WHEN acquisition_tracking_id__c like '%b2b floating newsletter%' THEN 'b2b floating newsletter'
		        -- WHEN acquisition_tracking_id__c like '%b2b seo page form%' THEN 'b2b seo page form'
		        -- WHEN acquisition_tracking_id__c like '%unbounce%' THEN 'unbounce'
		        -- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'unbounce_reinigungdirekt'
		        -- WHEN acquisition_tracking_id__c like '%unbound_tfs%' THEN 'unbound_tfs'

		        /*WHEN t.acquisition_channel_params__c::text ~~ '%coop%'
		            OR t.acquisition_channel_params__c::text ~~ '%afnt%'
		            OR t.acquisition_channel_params__c::text ~~ '%putzchecker%'
		        THEN 'Affiliate/Coops'*/
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		CASE WHEN t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c NOT IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}','{"src":"step1"}') THEN 'B2B Homepage' -- THE FIRST PAGE THE LEAD ACTUALLY VISITED ON THE WEBSITE
				WHEN (t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}')) OR t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Homepage'
				WHEN t.acquisition_channel_params__c IN ('{"src":"step1"}') THEN 'B2C Funnel'
				
				ELSE 'Unknown' END
		as landing_page,

		CASE WHEN t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Funnel' -- WHERE THE LEAD TYPED-IN ITS INFORMATON
				WHEN t.acquisition_tracking_id__c IS NULL THEN 'B2B Funnel'
				ELSE 'Unknown' END
		as conversion_page,
		CASE
		WHEN acquisition_tracking_id__c like '%b2b funnel%' and direct_relation__c = TRUE THEN 'B2B Funnel Direct'
		WHEN acquisition_tracking_id__c like '%b2b funnel%' and direct_relation__c = FALSE THEN 'B2B Funnel Partner'
		WHEN acquisition_tracking_id__c like '%b2b home cb%' THEN 'B2B Home CB'
		WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'B2B Home Form'
		WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'B2C Home Form'
		WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'Chatbot'
		-- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'Reinigungdirekt'
		-- WHEN acquisition_tracking_id__c like '%unbounce%' and acquisition_tracking_id__c not like '%unbounce_reinigungdirekt%'  THEN 'Unbounce'
		-- WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'b2c home form'

		-------------------------------------------------------------------------------------------------------------------  

        WHEN acquisition_tracking_id__c like '%b2c home funnel%' THEN 'b2c home funnel'
        WHEN acquisition_tracking_id__c like '%b2c funnel availability request%' THEN 'b2c funnel availability request'

        -- WHEN acquisition_tracking_id__c like '%b2b funnel%' THEN 'b2b funnel'
        WHEN acquisition_tracking_id__c like '%availability request%' THEN 'availability request'
        WHEN acquisition_tracking_id__c like '%price page funnel%' THEN 'price page funnel'
        WHEN acquisition_tracking_id__c like '%help page funnel%' THEN 'help page funnel'
        WHEN acquisition_tracking_id__c like '%about page funnel%' THEN 'about page funnel'
        -- WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'b2b home form'
        WHEN acquisition_tracking_id__c like '%b2b home funnel%' THEN 'b2b home funnel'
        WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'chatbot'
        WHEN acquisition_tracking_id__c like '%b2b amp%' THEN 'b2b amp'
        WHEN acquisition_tracking_id__c like '%footer property management cb%' THEN 'footer property management cb'
        WHEN acquisition_tracking_id__c like '%footer newsletter b2b%' THEN 'footer newsletter b2b'
        WHEN acquisition_tracking_id__c like '%price page form%' THEN 'price page form'
        WHEN acquisition_tracking_id__c like '%help page form%' THEN 'help page form'
        WHEN acquisition_tracking_id__c like '%about page form%' THEN 'about page form'
        WHEN acquisition_tracking_id__c like '%b2b contact page form%' THEN 'b2b contact page form'
        WHEN acquisition_tracking_id__c like '%b2b service page form%' THEN 'b2b service page form'
        WHEN acquisition_tracking_id__c like '%b2b about page form%' THEN 'b2b about page form'
        WHEN acquisition_tracking_id__c like '%b2b floating cb%' THEN 'b2b floating cb'
        WHEN acquisition_tracking_id__c like '%b2b floating newsletter%' THEN 'b2b floating newsletter'
        WHEN acquisition_tracking_id__c like '%b2b seo page form%' THEN 'b2b seo page form'
        WHEN acquisition_tracking_id__c like 'unbounce' THEN 'unbounce'
        WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'unbounce reinigungdirekt'
        WHEN acquisition_tracking_id__c like '%unbounce_tfs%' THEN 'unbounce tfs'


        WHEN acquisition_tracking_id__c like '%price_page_funnel_b2b%' THEN 'price page funnel b2b'
        WHEN acquisition_tracking_id__c like '%quality_page_funnel_b2b%' THEN 'quality page funnel b2b'
        WHEN acquisition_tracking_id__c like '%quality_page_funnel_b2c%' THEN 'quality page funnel b2c'
        WHEN acquisition_tracking_id__c like '%how_it_works_page_funnel_b2b%' THEN 'how it works page funnel b2b'
        WHEN acquisition_tracking_id__c like '%how_it_works_page_funnel_b2c%' THEN 'how it works page funnel b2c'
        WHEN acquisition_tracking_id__c like '%about_us_page_funnel_b2b%' THEN 'about us page funnel b2b'
        WHEN acquisition_tracking_id__c like '%about_us_page_funnel_b2c%' THEN 'about us page funnel b2c'
        WHEN acquisition_tracking_id__c like '%about_us_page%' THEN 'about us page'

		ELSE 'Other' END as lead_source


	FROM
	Salesforce.likeli__c t
	WHERE
	createddate::date > '2017-01-01'
	and type__c = 'B2B'
	-- and t.test__c IS FALSE
	AND acquisition_channel__c NOT LIKE 'outbound'
	AND company_name__c NOT LIKE '%test%'
	AND company_name__c NOT LIKE '%bookatiger%'
	AND email__c NOT LIKE '%bookatiger%'
	AND ((t.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR t.lost_reason__c IS NULL)
	-- AND o.acquisition_channel__c IN ('inbound', 'web')
	-- AND LEFT(o.locale__c, 2) = 'de'
	AND t.test__c IS FALSE
	AND t.name NOT LIKE '%test%';