
-- Short Description: CM Cases created yesterday (used for the case inbound history tracking)

DROP TABLE IF EXISTS bi.CM_cases_created_yesterday;


CREATE TABLE bi.CM_cases_created_yesterday AS
SELECT *
FROM bi.cm_cases_basis ca
WHERE ca.case_createddate::date = 'YESTERDAY' ;