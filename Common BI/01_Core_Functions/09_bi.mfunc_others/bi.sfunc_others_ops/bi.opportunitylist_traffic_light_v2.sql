-- THE NEW TRAFFIC LIGHT

DROP TABLE IF EXISTS bi.opportunitylist_traffic_light_v2;


CREATE TABLE bi.opportunitylist_traffic_light_v2 AS
SELECT t1.locale ,
       t1.Customer ,
       t1.Opportunity_Name ,
       t1.Opportunity ,
       t1.Status ,
       t1.Opps ,
       t1.Owner,
       t1.delivery_area ,
       t1.operated_by ,
       t1.operated_by_detail
FROM
    (SELECT opp.locale__c AS locale ,
            opp.customer__c AS Customer ,
            opp.name AS Opportunity_Name ,
            opp.sfid AS Opportunity ,
            opp.status__c AS Status ,
            CAST (COUNT (opp.sfid) OVER (PARTITION BY opp.customer__c) AS BIGINT) AS Opps ,
                 tuser.name AS OWNER --			, delivery_area__c													AS delivery_area_opp
 ,
                 CASE
                     WHEN Opp.delivery_area__c IS NOT NULL THEN Opp.delivery_area__c
                     ELSE 'unknown'
                 END AS delivery_area ,
                 last_order.operated_by ,
                 last_order.operated_by_detail ,
                 SUM(CASE
                         WHEN cont.service_type__c NOT LIKE '%maintenance cleaning%' THEN 1
                         ELSE 0
                     END) AS not_maintenance ,
                 SUM(CASE
                         WHEN cont.service_type__c LIKE '%maintenance cleaning%' THEN 1
                         ELSE 0
                     END) AS maintenance
     FROM Salesforce.opportunity Opp
     LEFT JOIN salesforce.user tuser ON (Opp.ownerid = tuser.sfid) -- --
 -- -- last order details
 -- -- served by BAT (TFS) or Partner

     LEFT JOIN
         (SELECT DISTINCT ON (opportunityid) opportunityid AS Opp ,
                             effectivedate::date,
                             sfid AS orderid ,
                             delivery_area__c ,
                             professional__c AS Professional ,
                             operated_by ,
                             operated_by_detail
          FROM salesforce."order" AS orders
          LEFT JOIN
              (SELECT Professional.sfid AS professional ,
                      CASE
                          WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT'
                          ELSE 'Partner'
                      END AS operated_by ,
                      CASE
                          WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT'
                          ELSE Partner.name
                      END AS operated_by_detail
               FROM Salesforce.Account Professional
               LEFT JOIN Salesforce.Account Partner ON (Professional.parentid = Partner.sfid) ) AS ProfessionalDetails ON (orders.professional__c = ProfessionalDetails.professional)
          WHERE opportunityid IS NOT NULL
          ORDER BY opportunityid,
                   effectivedate::date DESC ) AS last_order ON (last_order.Opp = Opp.sfid)
     LEFT JOIN salesforce.contract__c cont ON opp.sfid = cont.opportunity__c
     WHERE opp.test__c = FALSE
         AND opp.status__c IN ('ONBOARDED',
                               'RUNNING',
                               'RENEGOTIATION',
                               'RETENTION',
                               'OFFBOARDING')
         AND cont.active__c IS TRUE
         AND cont.test__c IS FALSE
     GROUP BY opp.locale__c ,
              opp.delivery_area__c ,
              opp.customer__c ,
              opp.name,
              opp.sfid,
              opp.status__c ,
              OWNER ,
              last_order.operated_by ,
              last_order.operated_by_detail) AS t1
WHERE t1.maintenance > 0 ;