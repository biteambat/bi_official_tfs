-- Short Description: CM Cases, open and closed since 2018-11-01 (new case setup)

DROP TABLE IF EXISTS bi.CM_cases_basis;


CREATE TABLE bi.CM_cases_basis AS
SELECT cas.sfid case_ID ,
       cas.casenumber case_number ,
       cas.createddate case_createddate ,
       cas.isclosed case_isclosed ,
       cas.ownerid case_ownerid ,
       u.name case_owner ,
       cas.origin case_origin ,
       cas.type case_type ,
       cas.reason case_reason ,
       cas.status case_status ,
       cas.contactid contactid ,
       co.name contact_name ,
       co.type__c contact_type ,
       co.company_name__c contact_companyname ,
       cas.order__c orderid ,
       o.type order_type ,
       cas.accountid professionalid ,
       a.name professional ,
       a.company_name__c professional_companyname ,
       cas.opportunity__c opportunityid ,
       opp.name opportunity ,
       opp.grand_total__c grand_total
FROM salesforce.CASE cas
LEFT JOIN salesforce.user u ON cas.ownerid = u.sfid
LEFT JOIN salesforce.opportunity opp ON cas.opportunity__c = opp.sfid
LEFT JOIN salesforce.contact co ON cas.contactid = co.sfid
LEFT JOIN salesforce.account a ON cas.accountid = a.sfid
LEFT JOIN salesforce.order o ON cas.order__c = o.sfid
WHERE (opp.test__c = FALSE
       OR opp.test__c IS NULL) -- just CM B2B
--	excluded case owner
AND ((CASE
          WHEN u.name LIKE '%Accounting%' THEN 1
          WHEN u.name LIKE '%TOShared%' THEN 1
          WHEN u.name LIKE '%marketing%' THEN 1
          WHEN u.name LIKE '%Marketing%' THEN 1
          WHEN u.name LIKE '%BAT B2B Admin Queue%' THEN 1
          WHEN u.name LIKE '%Nicolai%' THEN 1
          WHEN u.name LIKE '%Bätcher%' THEN 1
          WHEN u.name LIKE '%Kharoo%' THEN 1
          WHEN u.name LIKE '%Haferkorn%' THEN 1
          WHEN u.name LIKE '%Heumer%' THEN 1
          WHEN u.name LIKE '%Frank_Wendt%' THEN 1
          WHEN u.name LIKE '%Ahlers%' THEN 1
          WHEN u.name LIKE '%Ribeiro%' THEN 1
          WHEN u.name LIKE '%Klonaris%' THEN 1
          WHEN u.name LIKE '%Kiekebusch%' THEN 1
          WHEN u.name LIKE '%Steven%' THEN 1
          WHEN u.name LIKE '%Heesch-Müller%' THEN 1
          WHEN u.name LIKE '%Stolzenburg%' THEN 1
          WHEN u.name LIKE '%Feldhaus%' THEN 1
          WHEN u.name LIKE '%Adorador%' THEN 1
          WHEN u.name LIKE '%Devrient%' THEN 1
          WHEN u.name LIKE '%Wagner%' THEN 1
          ELSE 0
      END) = 0 --2
 )
    AND (-- old case setup (3 AND NOT 11)
 ( (CASE
        WHEN cas.origin LIKE 'B2B - Contact%' THEN 1
        WHEN cas.origin LIKE 'B2B de - Customer dashboard%' THEN 1
        WHEN cas.origin LIKE 'B2B DE%' THEN 1
        WHEN cas.origin LIKE 'B2B CH%' THEN 1
        WHEN cas.origin LIKE 'TFS CM%' THEN 1
        ELSE 0
    END) = 1 -- 3

  AND (CASE
           WHEN cas.type = 'CLM HR' THEN 1
           WHEN cas.type = 'CLM' THEN 1
           WHEN cas.type = 'CM B2C' THEN 1
           WHEN cas.type = 'Sales' THEN 1
           WHEN cas.type = 'PM' THEN 1
           ELSE 0
       END) = 0 -- 11
 )
         OR ( (CASE
                   WHEN cas.origin LIKE 'CM%' THEN 1
                   WHEN cas.origin LIKE 'Insurance' THEN 1
                   WHEN cas.origin LIKE '%checkout%' THEN 1
                   WHEN cas.origin LIKE '%partner portal' THEN 1
                   ELSE 0
               END) = 1 -- 4

             AND (CASE
                      WHEN cas.type = 'KA' THEN 1
                      WHEN cas.type = 'B2B' THEN 1
                      ELSE 0
                  END) = 1 -- 5
 ) -- new case setup

         OR ((CASE
                  WHEN cas.origin LIKE 'B2B customer%' THEN 1
                  ELSE 0
              END) = 1 -- 3

             AND (CASE
                      WHEN cas.type = 'CM B2B' THEN 1
                      WHEN cas.type = 'Pool' THEN 1
                      WHEN cas.type = 'TFS - CM' THEN 1
                      ELSE 0
                  END) = 1 -- 11
 )
         OR ( (CASE
                   WHEN cas.type = 'CM B2B' THEN 1
                   WHEN cas.type = 'TFS - CM' THEN 1
                   ELSE 0
               END) = 1 ) )
    AND cas.createddate::date >= '2018-11-01' ;