
-- Second table allowing to calculate the drops of traffic light

DROP TABLE IF EXISTS bi.tfl_drops;
CREATE TABLE bi.tfl_drops AS 

SELECT

    year,
    week,
    mindate,
    opportunity_name,
    opportunity,
    status,
    owner,
    delivery_area,
    potential,
    CASE WHEN drop_tfl > 2 THEN 2 ELSE drop_tfl END as drop_tfl

FROM
    
    (SELECT
    
        date_part('year', o.date) as year,
        date_part('week', o.date) as week,
        MIN(o.date) as mindate,
        o.opportunity_name,
        o.opportunity,
        o."status",
        o.owner,
        o.delivery_area,
        MAX(o.potential) as potential,
        SUM(o.drop_today) as drop_tfl
            
    FROM
    
        bi.opportunity_traffic_light_drops o
        
    GROUP BY
    
        date_part('year', o.date),
        date_part('week', o.date),
        o.opportunity_name,
        o.opportunity,
        o."status",
        o.owner,
        o.delivery_area
        
    ORDER BY
    
        o.opportunity_name) as t1;