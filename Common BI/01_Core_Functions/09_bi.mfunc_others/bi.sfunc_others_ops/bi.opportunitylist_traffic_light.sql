-- Short Description: the list of all opps which should get n traffic light

DROP TABLE IF EXISTS bi.opportunitylist_traffic_light;


CREATE TABLE bi.opportunitylist_traffic_light AS
SELECT locale__c AS locale ,
       customer__c AS Customer ,
       opp.name AS Opportunity_Name ,
       opp.sfid AS Opportunity ,
       status__c AS Status ,
       CAST (COUNT (opp.sfid) OVER (PARTITION BY customer__c) AS BIGINT) AS Opps ,
            tuser.name AS OWNER --			, delivery_area__c													AS delivery_area_opp
 ,
            CASE
                WHEN Opp.delivery_area__c IS NOT NULL THEN Opp.delivery_area__c
                ELSE 'unknown'
            END AS delivery_area ,
            last_order.operated_by ,
            last_order.operated_by_detail
FROM Salesforce.opportunity Opp
LEFT JOIN salesforce.user tuser ON (Opp.ownerid = tuser.sfid) -- --
-- -- last order details
-- -- served by BAT (TFS) or Partner

LEFT JOIN
    (SELECT DISTINCT ON (opportunityid) opportunityid AS Opp ,
                        effectivedate::date,
                        sfid AS orderid ,
                        delivery_area__c ,
                        professional__c AS Professional ,
                        operated_by ,
                        operated_by_detail
     FROM salesforce."order" AS orders
     LEFT JOIN
         (SELECT Professional.sfid AS professional ,
                 CASE
                     WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT'
                     ELSE 'Partner'
                 END AS operated_by ,
                 CASE
                     WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT'
                     ELSE Partner.name
                 END AS operated_by_detail
          FROM Salesforce.Account Professional
          LEFT JOIN Salesforce.Account Partner ON (Professional.parentid = Partner.sfid) ) AS ProfessionalDetails ON (orders.professional__c = ProfessionalDetails.professional)
     WHERE opportunityid IS NOT NULL
     ORDER BY opportunityid,
              effectivedate::date DESC ) AS last_order ON (last_order.Opp = Opp.sfid)
WHERE test__c = FALSE
    AND status__c IN ('ONBOARDED',
                      'RUNNING',
                      'RENEGOTIATION',
                      'RETENTION',
                      'OFFBOARDING')
GROUP BY locale__c ,
         opp.delivery_area__c ,
         customer__c ,
         opp.name,
         opp.sfid,
         status__c ,
         OWNER ,
         last_order.operated_by ,
         last_order.operated_by_detail -- Short Description: list of all opps which should get an traffic light
-- Created on: 04/06/2019

DROP TABLE IF EXISTS bi.opportunitylist_traffic_light;


CREATE TABLE bi.opportunitylist_traffic_light AS
SELECT locale__c AS locale ,
       customer__c AS Customer ,
       opp.name AS Opportunity_Name ,
       opp.sfid AS Opportunity ,
       status__c AS Status ,
       CAST (COUNT (opp.sfid) OVER (PARTITION BY customer__c) AS BIGINT) AS Opps ,
            tuser.name AS OWNER --			, delivery_area__c													AS delivery_area_opp
 ,
            CASE
                WHEN Opp.delivery_area__c IS NOT NULL THEN Opp.delivery_area__c
                ELSE 'unknown'
            END AS delivery_area ,
            last_order.operated_by ,
            last_order.operated_by_detail
FROM Salesforce.opportunity Opp
LEFT JOIN salesforce.user tuser ON (Opp.ownerid = tuser.sfid) -- --
-- -- last order details
-- -- served by BAT (TFS) or Partner

LEFT JOIN
    (SELECT DISTINCT ON (opportunityid) opportunityid AS Opp ,
                        effectivedate::date,
                        sfid AS orderid ,
                        delivery_area__c ,
                        professional__c AS Professional ,
                        operated_by ,
                        operated_by_detail
     FROM salesforce."order" AS orders
     LEFT JOIN
         (SELECT Professional.sfid AS professional ,
                 CASE
                     WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT'
                     ELSE 'Partner'
                 END AS operated_by ,
                 CASE
                     WHEN Partner.name LIKE '%BAT Business Services GmbH%' THEN 'BAT'
                     ELSE Partner.name
                 END AS operated_by_detail
          FROM Salesforce.Account Professional
          LEFT JOIN Salesforce.Account Partner ON (Professional.parentid = Partner.sfid) ) AS ProfessionalDetails ON (orders.professional__c = ProfessionalDetails.professional)
     WHERE opportunityid IS NOT NULL
     ORDER BY opportunityid,
              effectivedate::date DESC ) AS last_order ON (last_order.Opp = Opp.sfid)
WHERE test__c = FALSE
    AND status__c IN ('ONBOARDED',
                      'RUNNING',
                      'RENEGOTIATION',
                      'RETENTION',
                      'OFFBOARDING')
GROUP BY locale__c ,
         opp.delivery_area__c ,
         customer__c ,
         opp.name,
         opp.sfid,
         status__c ,
         OWNER ,
         last_order.operated_by ,
         last_order.operated_by_detail ;