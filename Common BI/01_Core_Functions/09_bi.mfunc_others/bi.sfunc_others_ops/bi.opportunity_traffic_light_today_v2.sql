-- Short Description: Opportunity Traffic Light TODAY. Synced with Salesforce Marketing Cloud.

DROP TABLE IF EXISTS bi.opportunity_traffic_light_today_v2;


CREATE TABLE bi.opportunity_traffic_light_today_v2 AS
SELECT *
FROM bi.opportunity_traffic_light_new_v2
WHERE date = (CURRENT_DATE - INTERVAL '0 DAY') ;