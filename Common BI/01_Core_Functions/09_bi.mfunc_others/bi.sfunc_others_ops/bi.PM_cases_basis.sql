
DROP TABLE IF EXISTS bi.PM_cases_basis;


CREATE TABLE bi.PM_cases_basis AS
SELECT ca.sfid case_ID ,
       ca.casenumber case_number ,
       ca.createddate case_createddate ,
       ca.isclosed case_isclosed ,
       ca.ownerid case_ownerid ,
       u.name case_owner ,
       ca.origin case_origin ,
       ca.type case_type ,
       ca.reason case_reason ,
       ca.status case_status ,
       ca.contactid contactid ,
       co.name contact_name ,
       co.type__c contact_type ,
       co.company_name__c contact_companyname ,
       ca.order__c orderid ,
       o.type order_type ,
       ca.accountid professionalid ,
       a.name professional ,
       a.company_name__c professional_companyname ,
       ca.opportunity__c opportunityid ,
       opp.name opportunity ,
       opp.grand_total__c grand_total
FROM salesforce.CASE ca
LEFT JOIN salesforce.user u ON ca.ownerid = u.sfid
LEFT JOIN salesforce.opportunity opp ON ca.opportunity__c = opp.sfid
LEFT JOIN salesforce.contact co ON ca.contactid = co.sfid
LEFT JOIN salesforce.account a ON ca.accountid = a.sfid
LEFT JOIN salesforce.order o ON ca.order__c = o.sfid -- 2 AND (3 OR 4)

WHERE (ca.test__c = FALSE
       OR ca.test__c IS NULL) --	excluded case owner
 AND((CASE
          WHEN u.name LIKE '%Standke%' THEN 1
          ELSE 0
      END) = 0 -- 2
 )
    AND (ca.type = 'PM' -- 3

         OR (ca.type = 'Pool'
             AND ca.origin = 'PM - Team' )-- 4
 )
    AND ca.createddate::date >= '2018-01-01' --		AND ca.casenumber = '00517627' 								- in case you are looking for a case -- its case^2
;