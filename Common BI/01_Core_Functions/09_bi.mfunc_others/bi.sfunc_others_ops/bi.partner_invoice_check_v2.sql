-- Short Description: partner invoice forecast details for the invoice check done by PM Version 2

DROP TABLE IF EXISTS bi.partner_invoice_check_v2;


CREATE TABLE bi.partner_invoice_check_v2 AS
SELECT TO_CHAR(o.effectivedate,
               'YYYY-MM') AS year_month ,
       CASE
           WHEN a.locale LIKE ('ch-%') THEN 'ch'
           WHEN a.locale LIKE ('de-%') THEN 'de'
           ELSE 'unknown'
       END AS locale,
       a.partnername partner ,
       a.partnerid partnerID ,
       a.partnerpph partnerpph ,
       COUNT(opp.sfid) OVER (PARTITION BY opp.sfid,
                                          TO_CHAR(o.effectivedate, 'YYYY-MM')) AS partner_per_opp ,
                            opp.name opportunity ,
                            opp.sfid opportunityID ,
                            opp.status__c opportunity_status ,
                            opp.monthly_partner_costs__c ppp_monthly_partner_costs ,
                            opp.grand_total__c opp_grand_total ,
                            invoice.net amount_invoice ,
                            opp.hours_weekly__c opp_weekly_hours ,
                            opp.hours_weekly__c*4.33 opp_AVG_monthly_hours ,
                            oo.max_hours opp_MAX_monthly_hours ,
                            MIN (o.effectivedate::date) AS first_order_date_monthly ,
                                first_month.first_order first_month ,
                                first_month.last_order last_order ,
                                one_offs.acquistion_type AS oneoff_acquistion_type ,
                                one_offs.package AS oneoff_package -- number of order
 ,
                                COUNT(*) orders ,
                                     SUM (CASE
                                              WHEN o.status LIKE 'FULFILLED' THEN 1
                                              ELSE 0
                                          END) AS FULFILLED ,
                                         SUM (CASE
                                                  WHEN o.status LIKE 'INVOICED' THEN 1
                                                  ELSE 0
                                              END) AS INVOICED ,
                                             SUM (CASE
                                                      WHEN o.status LIKE 'CANCELLED_CUSTOMER' THEN 1
                                                      ELSE 0
                                                  END) AS CANCELLED_CUSTOMER ,
                                                 SUM (CASE
                                                          WHEN o.status LIKE 'CANCELLED_MISTAKE' THEN 1
                                                          ELSE 0
                                                      END) AS CANCELLED_MISTAKE ,
                                                     SUM (CASE
                                                              WHEN o.status LIKE 'CANCELLED_MISTAKE'
                                                                   AND (o.quick_note__c ILIKE '%public_holiday%'
                                                                        OR o.error_note__c ILIKE '%public_holiday%') THEN 1
                                                              ELSE 0
                                                          END) AS CANCELLED_MISTAKE_public_holidays ,
                                                         SUM (CASE
                                                                  WHEN o.status LIKE 'CANCELLED_PROFESSIONAL'THEN 1
                                                                  ELSE 0
                                                              END) AS CANCELLED_PROFESSIONAL ,
                                                             SUM (CASE
                                                                      WHEN o.status LIKE 'CANCELLED_TERMINATED' THEN 1
                                                                      ELSE 0
                                                                  END) AS CANCELLED_TERMINATED ,
                                                                 SUM (CASE
                                                                          WHEN o.status LIKE 'NOSHOW_CUSTOMER' THEN 1
                                                                          ELSE 0
                                                                      END) AS NOSHOW_CUSTOMER ,
                                                                     SUM (CASE
                                                                              WHEN o.status LIKE 'NOSHOW_PROFESSIONAL' THEN 1
                                                                              ELSE 0
                                                                          END) AS NOSHOW_PROFESSIONAL ,
                                                                         SUM (CASE
                                                                                  WHEN o.status LIKE 'PENDING_ALLOCATION' THEN 1
                                                                                  ELSE 0
                                                                              END) AS PENDING_ALLOCATION,
                                                                             SUM (CASE
                                                                                      WHEN o.status LIKE 'PENDING_TO_START' THEN 1
                                                                                      ELSE 0
                                                                                  END) AS PENDING_TO_START -- hours (order duration)
 ,
                                                                                 SUM (CASE
                                                                                          WHEN o.status LIKE 'FULFILLED' THEN o.order_duration__c
                                                                                          ELSE 0
                                                                                      END) AS h_FULFILLED ,
                                                                                     SUM (CASE
                                                                                              WHEN o.status LIKE 'INVOICED' THEN o.order_duration__c
                                                                                              ELSE 0
                                                                                          END) AS h_INVOICED ,
                                                                                         SUM (CASE
                                                                                                  WHEN o.status LIKE 'CANCELLED_CUSTOMER' THEN o.order_duration__c
                                                                                                  ELSE 0
                                                                                              END) AS h_CANCELLED_CUSTOMER ,
                                                                                             SUM (CASE
                                                                                                      WHEN o.status LIKE 'CANCELLED_MISTAKE' THEN o.order_duration__c
                                                                                                      ELSE 0
                                                                                                  END) AS h_CANCELLED_MISTAKE ,
                                                                                                 SUM (CASE
                                                                                                          WHEN o.status LIKE 'CANCELLED_MISTAKE'
                                                                                                               AND (o.quick_note__c ILIKE '%public_holiday%'
                                                                                                                    OR o.error_note__c ILIKE '%public_holiday%') THEN o.order_duration__c
                                                                                                          ELSE 0
                                                                                                      END) AS h_CANCELLED_MISTAKE_public_holidays ,
                                                                                                     SUM (CASE
                                                                                                              WHEN o.status LIKE 'CANCELLED_PROFESSIONAL'THEN o.order_duration__c
                                                                                                              ELSE 0
                                                                                                          END) AS h_CANCELLED_PROFESSIONAL ,
                                                                                                         SUM (CASE
                                                                                                                  WHEN o.status LIKE 'CANCELLED_TERMINATED' THEN o.order_duration__c
                                                                                                                  ELSE 0
                                                                                                              END) AS h_CANCELLED_TERMINATED ,
                                                                                                             SUM (CASE
                                                                                                                      WHEN o.status LIKE 'NOSHOW_CUSTOMER' THEN o.order_duration__c
                                                                                                                      ELSE 0
                                                                                                                  END) AS h_NOSHOW_CUSTOMER ,
                                                                                                                 SUM (CASE
                                                                                                                          WHEN o.status LIKE 'NOSHOW_PROFESSIONAL' THEN o.order_duration__c
                                                                                                                          ELSE 0
                                                                                                                      END) AS h_NOSHOW_PROFESSIONAL ,
                                                                                                                     SUM (CASE
                                                                                                                              WHEN o.status LIKE 'PENDING_ALLOCATION' THEN o.order_duration__c
                                                                                                                              ELSE 0
                                                                                                                          END) AS h_PENDING_ALLOCATION,
                                                                                                                         SUM (CASE
                                                                                                                                  WHEN o.status LIKE 'PENDING_TO_START' THEN o.order_duration__c
                                                                                                                                  ELSE 0
                                                                                                                              END) AS h_PENDING_TO_START,
                                                                                                                             extra_orders.extra_hours AS h_extra_hours
FROM salesforce.order o
LEFT JOIN salesforce.opportunity opp ON o.opportunityid = opp.sfid
LEFT JOIN
    (SELECT MONTH ,
            opp ,
            SUM(amount__c)/1.19 AS net
     FROM
         ( SELECT TO_CHAR(i.issued__c, 'YYYY-MM') AS MONTH ,
                  i.opportunity__c AS opp ,
                  i.amount__c
          FROM salesforce.invoice__c i
          LEFT JOIN salesforce.invoice__c i2 ON i.sfid = i2.original_invoice__c
          WHERE i.original_invoice__c IS NULL
              AND i.opportunity__c IS NOT NULL
          UNION SELECT TO_CHAR(i.issued__c, 'YYYY-MM') AS MONTH ,
                       i2.opportunity__c AS opp ,
                       i2.amount__c
          FROM salesforce.invoice__c i
          LEFT JOIN salesforce.invoice__c i2 ON i.sfid = i2.original_invoice__c
          WHERE i.opportunity__c IS NOT NULL ) AS t1
     GROUP BY MONTH ,
              opp ) AS invoice ON o.opportunityid = invoice.opp
AND TO_CHAR(o.effectivedate,
            'YYYY-MM') = MONTH --LEFT JOIN 	(	SELECT 			pro.sfid			proid
--								, partner.sfid 		partnerid
--								, partner.name		partnername
--								, pro.locale__c		locale
--								, partner.pph__c	partnerpph
--
--				FROM 			salesforce.account 	pro
--				LEFT JOIN 		salesforce.account 	partner 	ON 	pro.parentid = partner.sfid
--				WHERE 			pro.parentid		IS NOT NULL
--			) 						AS a	ON o.served_by__c		= a.partnerid
 -- update: 2019-06-14 NOT IN DOCUMENTATION
 -- in case the opp was served by the same partner but different professionals, the previos JOIN created multiple rows per opp which ended up in MPC*2 in tableau.
-- there is no need for professional fields and meanwhile the account.served_by__c field is mapped =)

LEFT JOIN
    (SELECT partner.sfid partnerid ,
            partner.name partnername ,
            partner.locale__c locale ,
            partner.pph__c partnerpph
     FROM salesforce.account partner) AS a ON o.served_by__c = a.partnerid -- max monthly hours and first order date monthly

LEFT JOIN
    (SELECT max_hours.opportunityid ,
            max_hours.year_month ,
            MIN (max_hours.first_order_date_monthly) AS first_order_date_monthly ,
                SUM (max_hours.max_h) AS max_hours
     FROM
         (SELECT basic.*,
                 SUM (CASE
                          WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday THEN 1
                          ELSE 0
                      END) AS CW_max ,
                     basic.duration * CEIL(SUM (CASE
                                                    WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday THEN 1.0
                                                    ELSE 0.0
                                                END) / CASE
                                                           WHEN basic.recurrency = 7 THEN 1.0
                                                           WHEN basic.recurrency = 14 THEN 2.0
                                                           WHEN basic.recurrency = 21 THEN 3.0
                                                           WHEN basic.recurrency = 28 THEN 4.0
                                                           ELSE 1
                                                       END) AS max_h
          FROM
              (SELECT TO_CHAR (t1.order_date::date, 'YYYY-MM') AS year_month ,
                      t1.opportunityid AS opportunityid ,
                      MIN(t1.order_date::date) AS first_order_date_monthly ,
                      t1.recurrency AS recurrency ,
                      t1.order_duration AS duration ,
                      ROW_NUMBER() OVER (PARTITION BY t1.opportunityid, TO_CHAR (order_date::date, 'YYYY-MM')
                                         ORDER BY order_date::date) AS r
               FROM
                   (SELECT opportunityid ,
                           MIN (effectivedate::date) AS order_date ,
                               SUM(order_duration__c) AS order_duration ,
                               recurrency__c AS recurrency
                    FROM salesforce."order" orders
                    WHERE status IN ('PENDING TO START',
                                     'FULFILLED',
                                     'NOSHOW CUSTOMER',
                                     'INVOICED')
                        AND orders.opportunityid IS NOT NULL
                        AND orders.recurrency__c <> '0'
                    GROUP BY opportunityid ,
                             effectivedate::date ,
                             recurrency__c) t1
               GROUP BY t1.opportunityid,
                        TO_CHAR (t1.order_date::date, 'YYYY-MM'),
                        t1.order_duration,
                        t1.order_date,
                        t1.recurrency
               ORDER BY t1.opportunityid,
                        year_month) basic
          LEFT JOIN salesforce.opportunity opp2 ON basic.opportunityid = opp2.sfid
          LEFT JOIN bi.working_days_monthly days ON (basic.year_month = days.year_month)
          WHERE basic.r <= opp2.times_per_week__c
          GROUP BY basic.opportunityid ,
                   basic.duration ,
                   basic.first_order_date_monthly ,
                   basic.year_month ,
                   basic.r ,
                   basic.recurrency ) AS max_hours
     GROUP BY max_hours.opportunityid ,
              max_hours.year_month ) AS oo ON (o.opportunityid = oo.opportunityid
                                               AND oo.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))-- first month / last month
LEFT JOIN
    (SELECT o.opportunityid opp ,
            MIN (effectivedate::date) AS first_order ,
                MAX (effectivedate::date) AS last_order
     FROM salesforce."order" o
     WHERE status IN ('PENDING TO START',
                      'FULFILLED',
                      'NOSHOW CUSTOMER',
                      'INVOICED')
         AND o.opportunityid IS NOT NULL
     GROUP BY o.opportunityid ) AS first_month ON first_month.opp = o.opportunityid -- extra hours
LEFT JOIN
    (SELECT TO_CHAR (effectivedate::date, 'YYYY-MM') AS year_month ,
            o.opportunityid AS opportunityid ,
            COUNT(*) AS extra_orders ,
            SUM(o.order_duration__c) AS extra_hours
     FROM salesforce.order o
     WHERE status IN ('PENDING TO START',
                      'FULFILLED',
                      'NOSHOW CUSTOMER',
                      'INVOICED')
         AND o.opportunityid IS NOT NULL --	AND TO_CHAR (o.effectivedate::date, 'YYYY-MM') = '2019-01'

         AND o.recurrency__c = 0
         AND o.pph__c = '29.9'
     GROUP BY o.opportunityid ,
              TO_CHAR (effectivedate::date, 'YYYY-MM')) AS extra_orders ON (extra_orders.opportunityid = o.opportunityid
                                                                            AND extra_orders.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM')) -- one offs
LEFT JOIN
    (SELECT opportunity AS opportunityid ,
            TO_CHAR (closed_date::date, 'YYYY-MM') AS year_month ,
            acquisition_type AS acquistion_type ,
            PACKAGE AS PACKAGE
     FROM bi.b2b_additional_booking one_off
     WHERE TYPE = 'one-off' ) AS one_offs ON (one_offs.opportunityid = o.opportunityid
                                              AND one_offs.year_month = TO_CHAR(o.effectivedate, 'YYYY-MM'))
WHERE o.test__c = FALSE
    AND o.effectivedate::date >= '2018-01-01'
    AND o.type IN ('cleaning-b2b',
                   'cleaning-window')
    AND (a.locale LIKE ('de-%')
         OR a.locale LIKE ('ch-%'))
    AND a.partnerid IS NOT NULL
GROUP BY a.partnername ,
         a.locale ,
         a.partnerid ,
         a.partnerpph ,
         opp.name ,
         opp.sfid,
         opp.monthly_partner_costs__c ,
         opp.grand_total__c ,
         TO_CHAR(o.effectivedate,
                 'YYYY-MM') , opp.hours_weekly__c ,
                              invoice.net ,
                              oo.max_hours,
                              first_month.first_order ,
                              opp.status__c,
                              first_month.last_order ,
                              extra_orders.extra_hours ,
                              one_offs.acquistion_type ,
                              one_offs.package ;