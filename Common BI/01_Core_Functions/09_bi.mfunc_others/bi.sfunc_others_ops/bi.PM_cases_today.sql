
-- Short Description: PM Case Backlog today (used for the backlog history tracking)

DROP TABLE IF EXISTS bi.PM_cases_today;


CREATE TABLE bi.PM_cases_today AS
SELECT *
FROM bi.PM_cases_basis ca
WHERE ca.case_isclosed = FALSE ;