
-- Short Description: CM Case Backlog today (used for the backlog history tracking)

DROP TABLE IF EXISTS bi.CM_cases_today;


CREATE TABLE bi.CM_cases_today AS
SELECT *
FROM bi.CM_cases_basis ca
WHERE ca.case_isclosed = FALSE ;