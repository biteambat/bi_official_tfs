-- CM B2B Service Level SVL calculation

DROP TABLE IF EXISTS bi.CM_cases_service_level_basis;


CREATE TABLE bi.CM_cases_service_level_basis AS
SELECT ca.case_id case_ID ,
       ca.case_number case_number ,
       ca.case_createddate date1_opened ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE
           ELSE MIN(cahi.createddate)
       END date2_closed ,
       CAST('New Case' AS varchar) AS TYPE ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN 'open'
           ELSE 'closed'
       END closed ,
       COUNT(*) AS value ,
       cahi.event_by AS event_by
FROM bi.CM_cases_basis ca -- date2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID = cahi.case_ID
AND cahi.createddate >= ca.case_createddate
WHERE ca.case_createddate::date >= '2018-01-01' --			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2

GROUP BY ca.case_id,
         ca.case_number,
         ca.case_createddate,
         cahi.event_by
UNION ALL
SELECT ca.case_ID case_ID ,
       ca.case_number case_number ,
       hi.createddate date1_opened ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE
           ELSE MIN(cahi.createddate)
       END date2_closed ,
       CAST('Reopened Case' AS varchar) AS TYPE ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN 'open'
           ELSE 'closed'
       END closed ,
       COUNT(*) AS value ,
       cahi.event_by AS event_by
FROM salesforce.casehistory hi
INNER JOIN bi.CM_cases_basis ca ON hi.caseid = ca.case_ID -- date2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID = cahi.case_ID
AND cahi.createddate >= hi.createddate
WHERE -- reopened cases
 hi.field = 'Status'
    AND (hi.newvalue LIKE 'Reopened'
         OR hi.newvalue LIKE 'In Progress'
         OR hi.newvalue LIKE 'New'
         OR hi.newvalue LIKE 'Escalated')
    AND hi.oldvalue LIKE 'Closed'
    AND hi.createddate::date >= '2018-01-01' --			AND ca.case_number	 = '00506591'			-- in case you are looking for a case -- its case^2

GROUP BY ca.case_id ,
         ca.case_number ,
         hi.createddate ,
         cahi.event_by
UNION ALL
SELECT ca.case_ID case_ID ,
       ca.case_number case_number ,
       hi.createddate date1_opened ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE
           ELSE MIN(cahi.createddate)
       END date2_closed ,
       CAST('Type Change' AS varchar) AS TYPE ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN 'open'
           ELSE 'closed'
       END closed ,
       COUNT(*) AS value ,
       cahi.event_by AS event_by
FROM salesforce.casehistory hi
INNER JOIN bi.CM_cases_basis ca ON hi.caseid = ca.case_ID -- date 2
LEFT JOIN bi.CM_cases_service_level_basis_closed cahi ON ca.case_ID = cahi.case_ID
AND cahi.createddate >= hi.createddate
WHERE -- type change
 hi.field = 'Type'
    AND hi.newvalue = 'CM B2B'
    AND hi.oldvalue NOT IN ('B2B',
                            'KA')
    AND hi.createddate::date <> ca.case_createddate::date
    AND hi.createddate::date >= '2018-01-01'
    AND ca.case_origin NOT LIKE '%B2B customer%'
    AND ca.case_origin NOT LIKE 'CM - Team'
GROUP BY ca.case_id ,
         ca.case_number ,
         hi.createddate ,
         cahi.event_by
UNION ALL
SELECT ca.case_ID case_ID ,
       ca.case_number case_number ,
       hi.createddate date1_opened ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN CURRENT_DATE
           ELSE MIN(cahi.createddate)
       END date2_closed ,
       CAST('# Reopened' AS varchar) AS TYPE ,
       CASE
           WHEN MIN(cahi.createddate) IS NULL THEN 'open'
           ELSE 'closed'
       END closed ,
       COUNT(*) AS value ,
       hi.createdbyid AS event_by
FROM salesforce.casehistory hi
INNER JOIN bi.CM_cases_basis ca ON hi.caseid = ca.case_ID -- date 2
LEFT JOIN salesforce.casehistory cahi ON ca.case_ID = cahi.caseid
AND cahi.field IN ('Status')
AND cahi.newvalue IN ('Reopened')
AND cahi.createddate <= hi.createddate
WHERE -- count reopened cases in the past
 hi.field = 'Status'
    AND hi.newvalue LIKE 'Reopened'
    AND hi.createddate::date >= '2018-01-01' --			AND ca.case_number	 = '00530089'

GROUP BY ca.case_id ,
         ca.case_number ,
         hi.createddate ,
         hi.createdbyid
ORDER BY case_id ,
         date1_opened ,
         date2_closed ,
         event_by ;