
-- CM B2B Service Level SVL calculation: case history table when case closed after the 2018-01-01

DROP TABLE IF EXISTS bi.CM_cases_service_level_basis_closed;


CREATE TABLE bi.CM_cases_service_level_basis_closed AS
SELECT cahi.caseid case_ID ,
       cahi.createddate ,
       cahi.createdbyid AS event_by
FROM salesforce.casehistory cahi
WHERE cahi.field IN ('created',
                     'Status')
    AND cahi.newvalue IN ('Closed')
    AND cahi.createddate::date >= '2018-01-01' --			AND ca.case_number = '00544379'	 				-- in case you are looking for a case -- its case^2
 ;