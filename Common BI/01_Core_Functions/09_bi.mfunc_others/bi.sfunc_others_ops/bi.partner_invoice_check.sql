-- Short Description: partner invoice forecast details for the invoice check done by PM (OLD)

DROP TABLE IF EXISTS bi.partner_invoice_check;


CREATE TABLE bi.partner_invoice_check AS
SELECT TO_CHAR(o.effectivedate,
               'YYYY-MM') AS year_month ,
       CASE
           WHEN a.locale LIKE ('ch-%') THEN 'ch'
           WHEN a.locale LIKE ('de-%') THEN 'de'
           ELSE 'unknown'
       END AS locale,
       a.partnername partner ,
       a.partnerid partnerID ,
       a.partnerpph partnerpph ,
       opp.name opportunity ,
       opp.sfid opportunityID ,
       opp.monthly_partner_costs__c ppp_monthly_partner_costs ,
       opp.grand_total__c opp_grand_total ,
       invoiced.amount__c/1.19 AS amount_invoiced --, o.effectivedate				order_effectivedate
 --, o.professional__c
 --, o.status					order_status
 --, o.order_duration__c	        order_duration
 --,*
 -- number of order
 ,
       COUNT(*) orders ,
            SUM (CASE
                     WHEN o.status LIKE 'FULFILLED' THEN 1
                     ELSE 0
                 END) AS FULFILLED ,
                SUM (CASE
                         WHEN o.status LIKE 'INVOICED' THEN 1
                         ELSE 0
                     END) AS INVOICED ,
                    SUM (CASE
                             WHEN o.status LIKE 'CANCELLED_CUSTOMER' THEN 1
                             ELSE 0
                         END) AS CANCELLED_CUSTOMER ,
                        SUM (CASE
                                 WHEN o.status LIKE 'CANCELLED_MISTAKE' THEN 1
                                 ELSE 0
                             END) AS CANCELLED_MISTAKE ,
                            SUM (CASE
                                     WHEN o.status LIKE 'CANCELLED_PROFESSIONAL'THEN 1
                                     ELSE 0
                                 END) AS CANCELLED_PROFESSIONAL ,
                                SUM (CASE
                                         WHEN o.status LIKE 'CANCELLED_TERMINATED' THEN 1
                                         ELSE 0
                                     END) AS CANCELLED_TERMINATED ,
                                    SUM (CASE
                                             WHEN o.status LIKE 'NOSHOW_CUSTOMER' THEN 1
                                             ELSE 0
                                         END) AS NOSHOW_CUSTOMER ,
                                        SUM (CASE
                                                 WHEN o.status LIKE 'NOSHOW_PROFESSIONAL' THEN 1
                                                 ELSE 0
                                             END) AS NOSHOW_PROFESSIONAL ,
                                            SUM (CASE
                                                     WHEN o.status LIKE 'PENDING_ALLOCATION' THEN 1
                                                     ELSE 0
                                                 END) AS PENDING_ALLOCATION,
                                                SUM (CASE
                                                         WHEN o.status LIKE 'PENDING_TO_START' THEN 1
                                                         ELSE 0
                                                     END) AS PENDING_TO_START -- hours (order duration)
 ,
                                                    SUM (CASE
                                                             WHEN o.status LIKE 'FULFILLED' THEN o.order_duration__c
                                                             ELSE 0
                                                         END) AS h_FULFILLED ,
                                                        SUM (CASE
                                                                 WHEN o.status LIKE 'INVOICED' THEN o.order_duration__c
                                                                 ELSE 0
                                                             END) AS h_INVOICED ,
                                                            SUM (CASE
                                                                     WHEN o.status LIKE 'CANCELLED_CUSTOMER' THEN o.order_duration__c
                                                                     ELSE 0
                                                                 END) AS h_CANCELLED_CUSTOMER ,
                                                                SUM (CASE
                                                                         WHEN o.status LIKE 'CANCELLED_MISTAKE' THEN o.order_duration__c
                                                                         ELSE 0
                                                                     END) AS h_CANCELLED_MISTAKE ,
                                                                    SUM (CASE
                                                                             WHEN o.status LIKE 'CANCELLED_PROFESSIONAL'THEN o.order_duration__c
                                                                             ELSE 0
                                                                         END) AS h_CANCELLED_PROFESSIONAL ,
                                                                        SUM (CASE
                                                                                 WHEN o.status LIKE 'CANCELLED_TERMINATED' THEN o.order_duration__c
                                                                                 ELSE 0
                                                                             END) AS h_CANCELLED_TERMINATED ,
                                                                            SUM (CASE
                                                                                     WHEN o.status LIKE 'NOSHOW_CUSTOMER' THEN o.order_duration__c
                                                                                     ELSE 0
                                                                                 END) AS h_NOSHOW_CUSTOMER ,
                                                                                SUM (CASE
                                                                                         WHEN o.status LIKE 'NOSHOW_PROFESSIONAL' THEN o.order_duration__c
                                                                                         ELSE 0
                                                                                     END) AS h_NOSHOW_PROFESSIONAL ,
                                                                                    SUM (CASE
                                                                                             WHEN o.status LIKE 'PENDING_ALLOCATION' THEN o.order_duration__c
                                                                                             ELSE 0
                                                                                         END) AS h_PENDING_ALLOCATION,
                                                                                        SUM (CASE
                                                                                                 WHEN o.status LIKE 'PENDING_TO_START' THEN o.order_duration__c
                                                                                                 ELSE 0
                                                                                             END) AS h_PENDING_TO_START
FROM salesforce.order o
LEFT JOIN salesforce.opportunity opp ON o.opportunityid = opp.sfid
LEFT JOIN salesforce.invoice__c invoiced ON (o.opportunityid = invoiced.opportunity__c
                                             AND TO_CHAR(o.effectivedate, 'YYYY-MM') = TO_CHAR(invoiced.issued__c, 'YYYY-MM'))
LEFT JOIN
    (SELECT pro.sfid proid ,
            pro.name proname ,
            pro.parentid parentid ,
            partner.sfid partnerid ,
            partner.name partnername ,
            pro.locale__c locale ,
            partner.pph__c partnerpph
     FROM salesforce.account pro
     LEFT JOIN salesforce.account partner ON pro.parentid = partner.sfid
     WHERE pro.parentid IS NOT NULL ) AS a ON o.professional__c = a.proid -- LEFT JOIN 	salesforce.contact		co 		ON ca.contactid 		= co.sfid

WHERE o.test__c = FALSE
    AND o.effectivedate::date >= '2018-01-01' --AND o.effectivedate::date 		<= '2018-10-31'

    AND o.type IN ('cleaning-b2b',
                   'cleaning-window')
    AND (a.locale LIKE ('de-%')
         OR a.locale LIKE ('ch-%'))
    AND a.parentid IS NOT NULL
GROUP BY a.partnername ,
         a.locale ,
         a.partnerid ,
         a.partnerpph ,
         opp.name ,
         opp.sfid,
         opp.monthly_partner_costs__c ,
         opp.grand_total__c ,
         invoiced.amount__c ,
         TO_CHAR(o.effectivedate,
                 'YYYY-MM') --, o.status
;