-- Short Description: 	Opportunity Traffic Light TODAY for tableau
-- -------------------	(History and current AVG result in one table for tableau)

DROP TABLE IF EXISTS bi.opportunity_traffic_light_tableau_v2;


CREATE TABLE bi.opportunity_traffic_light_tableau_v2 AS
SELECT history.date AS history_date ,
       history.locale AS history_locale ,
       history.customer AS history_customer --, history.opportunity_name							AS history_opportunity_name
 ,
       history.opportunity AS history_opportunity ,
       history.opps AS history_opps ,
       history.traffic_light_noshow AS history_traffic_light_noshow ,
       history.traffic_light_cancelled_pro AS history_traffic_light_cancelled_pro ,
       history.traffic_light_cancelled_customer AS history_traffic_light_cancelled_customer ,
       history.traffic_light_inbound_not_complaints AS history_traffic_light_inbound_calls ,
       history.traffic_light_lost_calls AS history_traffic_light_lost_calls ,
       history.traffic_light_complaint_calls AS history_traffic_light_complaint_calls ,
       history.traffic_light_createdcases AS history_traffic_light_createdcases ,
       history.traffic_light_retention AS history_traffic_light_retention ,
       history.traffic_light_invoicecorrection AS history_traffic_light_invoicecorrection ,
       history.traffic_light_professionalimprovement AS history_traffic_light_professionalimprovement ,
       history.traffic_light_partnerimprovement AS history_traffic_light_improvementsall ,
       history.traffic_light_improvementsall AS history_traffic_light_partnerimprovement ,
       history.traffic_light_bad_feedback AS history_traffic_light_bad_feedback ,
       history.avg_traffic_light AS history_avg_traffic_light ,
       current.date AS current__date ,
       current.opportunity AS current_opportunity ,
       current.opportunity_name AS current_opportunity_name ,
       current.delivery_area AS current_delivery_area ,
       current.status AS current_status ,
       current.owner AS current_owner ,
       current.operated_by AS operated_by ,
       current.operated_by_detail AS operated_by_detail ,
       current.traffic_light_noshow AS current_traffic_light_noshow ,
       current.traffic_light_cancelled_pro AS current_traffic_light_cancelled_pro ,
       current.traffic_light_cancelled_customer AS current_traffic_light_cancelled_customer ,
       current.traffic_light_inbound_not_complaints AS current_traffic_light_inbound_calls ,
       current.traffic_light_lost_calls AS current_traffic_light_lost_calls ,
       current.traffic_light_createdcases AS current_traffic_light_createdcases ,
       current.traffic_light_retention AS current_traffic_light_retention ,
       current.traffic_light_invoicecorrection AS current_traffic_light_invoicecorrection ,
       current.traffic_light_professionalimprovement AS current_traffic_light_professionalimprovement ,
       current.traffic_light_partnerimprovement AS current_traffic_light_partnerimprovement ,
       current.traffic_light_improvementsall AS current_traffic_light_improvementsall ,
       current.traffic_light_bad_feedback AS current_traffic_light_bad_feedback ,
       current.avg_traffic_light AS current_avg_traffic_light
FROM bi.opportunity_traffic_light_new_v2 AS history
LEFT JOIN
    ( SELECT *
     FROM bi.opportunity_traffic_light_new_v2
     WHERE date = (CURRENT_DATE - INTERVAL '0 DAY') ) AS CURRENT ON (history.opportunity = current.opportunity)
WHERE history.date BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '8 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY') ;