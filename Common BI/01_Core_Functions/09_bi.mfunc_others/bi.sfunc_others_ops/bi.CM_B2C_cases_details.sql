-- CM B2C Case details

DROP TABLE IF EXISTS bi.CM_B2C_cases_details;


CREATE TABLE bi.CM_B2C_cases_details AS
SELECT cas.sfid case_ID ,
       cas.casenumber case_number ,
       cas.createddate case_createddate ,
       cas.origin case_origin ,
       u.name case_owner
FROM salesforce.CASE cas
LEFT JOIN salesforce.user u ON cas.ownerid = u.sfid
WHERE (cas.test__c = FALSE
       OR cas.test__c IS NULL)-- just CM B2C
--	excluded case owner

    AND ((CASE
              WHEN u.name LIKE '%Accounting%' THEN 1
              WHEN u.name LIKE '%TOShared%' THEN 1
              WHEN u.name LIKE '%marketing%' THEN 1
              WHEN u.name LIKE '%Marketing%' THEN 1
              WHEN u.name LIKE '%BAT B2B Admin Queue%' THEN 1
              WHEN u.name LIKE '%Nicolai%' THEN 1
              WHEN u.name LIKE '%Bätcher%' THEN 1
              WHEN u.name LIKE '%Kharoo%' THEN 1
              WHEN u.name LIKE '%Haferkorn%' THEN 1
              WHEN u.name LIKE '%Heumer%' THEN 1
              WHEN u.name LIKE '%Ahlers%' THEN 1
              WHEN u.name LIKE '%Ribeiro%' THEN 1
              WHEN u.name LIKE '%Klonaris%' THEN 1
              WHEN u.name LIKE '%Kiekebusch%' THEN 1
              WHEN u.name LIKE '%Steven%' THEN 1
              WHEN u.name LIKE '%Wagner%' THEN 1
              WHEN u.name LIKE '%Adorador%' THEN 1
              WHEN u.name LIKE '%Stolzenburg%' THEN 1
              WHEN u.name LIKE '%Feldhaus%' THEN 1
              WHEN u.name LIKE '%Frank_Wendt%' THEN 1
              WHEN u.name LIKE '%Adorador%' THEN 1
              WHEN u.name LIKE '%Devrient%' THEN 1
              WHEN u.name LIKE '%Heesch-Müller%' THEN 1
              ELSE 0
          END) = 0 --2
 )
    AND (-- new case setup
 ((CASE
       WHEN cas.type = 'CM B2C' THEN 1
       ELSE 0
   END) = 1 -- 11
 ) -- old case setup

         OR ((CASE
                  WHEN cas.origin LIKE 'CM%' THEN 1
                  WHEN cas.origin LIKE 'B2C Customer%' THEN 1
                  WHEN cas.origin LIKE 'Insurance' THEN 1
                  ELSE 0
              END) = 1 -- 3

             AND (CASE
                      WHEN cas.type = 'General' THEN 1
                      WHEN cas.type = 'Pool' THEN 1
                      ELSE 0
                  END) = 1 -- 11
 ) )
    AND cas.test__c IS NOT TRUE
    AND cas.createddate::date >= '2018-01-01' ;