
-- Short Description: Opportunity Traffic Light of today. Synced with Salesforce Marketing Cloud. 

DROP TABLE IF EXISTS bi.opportunity_traffic_light_today;


CREATE TABLE bi.opportunity_traffic_light_today AS
SELECT *
FROM bi.opportunity_traffic_light_new
WHERE date = (CURRENT_DATE - INTERVAL '0 DAY') ;

