-- Short Description: PM Cases created yesterday (used for the case inbound history tracking)

DROP TABLE IF EXISTS bi.PM_cases_created_yesterday;


CREATE TABLE bi.PM_cases_created_yesterday AS
SELECT *
FROM bi.PM_cases_basis ca
WHERE ca.case_createddate::date = 'YESTERDAY' -- 5
;