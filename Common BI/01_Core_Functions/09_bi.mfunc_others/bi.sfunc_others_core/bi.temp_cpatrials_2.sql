
DROP TABLE IF EXISTS bi.temp_cpatrials_2;
CREATE TABLE bi.temp_cpatrials_2 AS

  SELECT
    countr as Country,
    creation_date as Date,
    sum(New_CustoID) as New_Subs,
    sum(New_CustoID_no_trial) as New_Subs_no_trial,
    sum(Mkg_Costs) as Total_costs_Mkg,
    12,25 * sum(Trial_hours) as Trial_Costs
    
  FROM bi.temp_cpatrials_1

  GROUP BY
    Country,
    Date,
    New_CustoID

;