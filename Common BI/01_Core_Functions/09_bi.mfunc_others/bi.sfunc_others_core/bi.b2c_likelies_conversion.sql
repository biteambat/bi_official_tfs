
DROP TABLE IF EXISTS bi.b2c_likelies_conversion;
CREATE TABLE bi.b2c_likelies_conversion AS

  SELECT
  c.*,
  SUM(a.acquisitions) as nb_acquisitions,
  ROUND(SUM(a.acquisitions)::numeric/c.nb_likelies,4) as conversion_ratio

  FROM bi.b2c_cost_per_likelies c
    
    JOIN bi.acquisitions_last100d_channel a 
      ON c.date = a.orderdate
      AND c.locale = a.country
      AND c.source_channel = (CASE WHEN a.marketing_channel in ('DTI','SEM Brand','SEO Brand') THEN 'Brand Marketing'
                              ELSE a.marketing_channel END)
      AND c.date < current_date
      AND a.acquisitions::numeric < c.nb_likelies

  WHERE c.nb_likelies > 5

  GROUP BY c.date, c.locale, c.source_channel, c.nb_likelies, c.cost

  ORDER BY c.date desc, c.locale asc, c.source_channel asc

;