
DROP TABLE IF EXISTS bi.international_cockpit_temp3;
CREATE TABLE bi.international_cockpit_temp3 as 
SELECT
  created_at::date as date,
  LEFT(REPLACE(CAST(professional_json->'Locale__c' as varchar),'"',''),2) as locale,
  COUNT(1) as Offboardings
FROM
  events.sodium
WHERE
  event_name in ('Account Event:LEFT','Account Event:TERMINATED')
GROUP BY
  date,
  locale;