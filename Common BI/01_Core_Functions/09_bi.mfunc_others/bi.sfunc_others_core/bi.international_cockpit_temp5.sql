
DROP TABLE IF EXISTS bi.international_cockpit_temp5;
CREATE TABLE bi.international_cockpit_temp5 as  
SELECT
  date,
    locale,
    SUM(sem_cost+sem_brand_cost+display_cost+facebook_cost+offline_cost+vouchers_cost+sem_discount+sem_brand_discount+display_discount+facebook_discount+facebook_organic_discount+offline_discount+seo_discount+seo_brand_discount+youtube_discount) as costs,
    sum(all_acq) as Acquisitions
FROM
    bi.cpacalcpolygon
GROUP BY
    locale,
    date;