
-- CPA with trials


DROP TABLE IF EXISTS bi.temp_cpatrials_1;
CREATE TABLE bi.temp_cpatrials_1 AS

  SELECT
    *
  FROM
     (SELECT
      left(locale__c,2) as co,
      order_creation__c::date as creation_date,
      count(distinct(customer_id__c)) as New_CustoID,
      sum(case when recurrency__c = '0' THEN order_duration__c ELSE 0 END) as Trial_hours,
      count(distinct(case when recurrency__c > '0' THEN customer_id__c ELSE NULL END)) as New_CustoID_no_trial

    FROM
      bi.orders

    WHERE
      order_creation__c::date >= '2016-07-01'
      and acquisition_new_customer__c = '1'
      and test__c = '0'
      and status NOT IN ('CANCELLED FAKED','CANCELLED MISTAKE')

    GROUP BY
      co,
      creation_date) as t1

    
    LEFT JOIN

    (SELECT
      orderdate as DateofOrder,
      locale as countr,
      sum(sem_non_brand + sem_brand + seo + facebook + criteo + sociomantic + gdn + youtube + coops + tvcampaign + offline_marketing + other_voucher + deindeal_voucher) as Mkg_Costs

    FROM
      bi.cpacalclocale

    WHERE
      orderdate between '2016-07-01' and '2016-09-18'

    GROUP BY
      DateofOrder,
      countr) as t2 


    ON (t1.Creation_date::date = t2.DateofOrder::date and t1.co = t2.countr)

;