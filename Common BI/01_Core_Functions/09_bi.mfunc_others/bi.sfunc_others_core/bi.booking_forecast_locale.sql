
DROP TABLE IF EXISTS bi.booking_forecast_locale;
CREATE TABLE bi.booking_forecast_locale as 
SELECT
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days))  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)) as rebookings_Forecast,
  t3.acquisition_forecast as b2b_acquisitions_forecast,
  t3.rebookings_forecast as b2b_rebookings_forecast,
  t3.supply_forecast as b2b_supply_forecast,
  (t3.bookings_forecast + t3.supply_forecast) as b2b_forecast
FROM
  bi.booking_forecast_2 t1
left join
  bi.booking_forecast_1 t2
ON 
  (t1.locale = t2.locale)
left join
  (SELECt
  locale,
  SUM(acquisition_forecast) as acquisition_forecast,
  SUM(rebookings_forecast) as rebookings_forecast,
  SUM(bookings_forecast) as bookings_forecast,
  Sum(supply_forecast) as supply_forecast
FROM
 bi.b2b_forecast 
GROUP BY
  locale) as t3
ON
  (t1.locale = t3.locale)
WHERE
  t1.locale in ('ch','at')
GROUP BY
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  Rebookings_Forecast,
  month_days,
  days,
  t3.acquisition_forecast,
  t3.rebookings_forecast,
  t3.bookings_forecast,
  t3.supply_forecast;

INSERT INTO bi.booking_forecast_locale
SELECT
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days)*1.0)  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)*1.0) as rebookings_Forecast,
  t3.acquisition_forecast as b2b_acquisitions_forecast,
  t3.rebookings_forecast as b2b_rebookings_forecast,
  t3.supply_forecast as b2b_supply_forecast,
  (t3.bookings_forecast + t3.supply_forecast) as b2b_forecast
FROM
  bi.booking_forecast_2 t1
left join
  bi.booking_forecast_1 t2
ON 
  (t1.locale = t2.locale)
left join
  (SELECt
  locale,
  SUM(acquisition_forecast) as acquisition_forecast,
  SUM(rebookings_forecast) as rebookings_forecast,
  SUM(bookings_forecast) as bookings_forecast,
  Sum(supply_forecast) as supply_forecast
FROM
 bi.b2b_forecast 
GROUP BY
  locale) as t3
ON (t1.locale = t3.locale)
WHERE
  t1.locale in ('de')
GROUP BY
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  Rebookings_Forecast,
  month_days,
  days,
  t3.acquisition_forecast,
  t3.rebookings_forecast,
  t3.bookings_forecast,
  t3.supply_forecast;


INSERT INTO bi.booking_forecast_locale
SELECT
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
   Acquisition_hours_this_month+(acquisition_runrate_per_day*(month_days-days))  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  rebookings_hours_this_month+(rebookings_runrate_per_day*(month_days-days)) as rebookings_Forecast,
  t3.acquisition_forecast as b2b_acquisitions_forecast,
  t3.rebookings_forecast as b2b_rebookings_forecast,
  t3.supply_forecast as b2b_supply_forecast,
  (t3.bookings_forecast + t3.supply_forecast) as b2b_forecast
FROM
  bi.booking_forecast_2 t1
left join
  bi.booking_forecast_1 t2
ON 
  (t1.locale = t2.locale)
left join
  (SELECt
  locale,
  SUM(acquisition_forecast) as acquisition_forecast,
  SUM(rebookings_forecast) as rebookings_forecast,
  SUM(bookings_forecast) as bookings_forecast,
  Sum(supply_forecast) as supply_forecast
FROM
 bi.b2b_forecast 
GROUP BY
  locale) as t3
ON (t1.locale = t3.locale)
WHERE
  t1.locale = 'nl'
GROUP BY
  t1.locale,
  Acquisition_hours_this_month,
  acquisition_runrate_per_day,
  Acquisition_Forecast,
  Rebookings_hours_this_month,
  Rebookings_runrate_per_day,
  Rebookings_Forecast,
  month_days,
  days,
  t3.acquisition_forecast,
  t3.rebookings_forecast,
  t3.bookings_forecast,
  t3.supply_forecast;