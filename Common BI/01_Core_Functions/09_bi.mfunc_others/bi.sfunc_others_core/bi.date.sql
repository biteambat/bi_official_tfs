
DROP TABLE IF EXISTS bi.date;
CREATE TABLE bi.date as 
SELECT
  Order_Creation__c::date as date,
  locale  
FROM
  bi.orders t1,
  unnest(array['de','ch','at','nl']) as locale
GROUP BY
  date,
  locale;