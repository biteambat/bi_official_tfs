
DELETE FROM bi.locked_forecast_history WHERE date = current_date;
INSERT INTO bi.locked_forecast_history
  SELECT
    current_date as date,
    t1.locale as locale,
    (t1.acquisition_forecast + t1.rebookings_Forecast) as forecast
  FROM bi.booking_forecast_locale t1

;