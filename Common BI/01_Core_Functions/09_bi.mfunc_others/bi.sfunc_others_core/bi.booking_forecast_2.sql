
DROP TABLE IF EXISTS bi.booking_forecast_2;
CREATE TABLE bi.booking_forecast_2 as 
SELECT
  LEFT(locale__c,2) as locale,
  SUM(CASE WHEN Acquisition_New_Customer__c = '1' THEN GMV_Eur ELSE 0 END) as Acquisition_hours_this_month,
  SUM(CASE WHEN Acquisition_New_Customer__c = '0' THEN GMV_Eur ELSE 0 END) as Rebookings_hours_this_month
FROM
  bi.orders
WHERE
   Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','NOSHOW CUSOTMER','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
     AND EXTRACT(MONTH FROM Current_Date) = EXTRACT(MONTH FROM ORder_Creation__c::date) and EXTRACT(Year FROM ORder_Creation__c::date) = EXTRACT(YEAR FROM current_Date)
     AND order_type = '1'
GROUP BY
  left(locale__c,2); 