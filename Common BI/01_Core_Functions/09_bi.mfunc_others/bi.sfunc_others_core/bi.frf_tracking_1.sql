
-- FRF Program

DROP TABLE IF EXISTS bi.frf_tracking_1;
CREATE TABLE bi.frf_tracking_1 as 
SELECT
  Order_Id__c,
  referred_by__c,
  Order_Creation__c,
  Voucher__c,
  status
FROM
  bi.orders
WHERE
  Voucher__c = 'FRF'
  and referred_by__c is not null;