
-- Order events

DELETE FROM main.order_events WHERE created_at::date = (current_date - interval '1 day');
INSERT INTO main.order_events

  select
        created_at, 
        replace(event_name, 'Order Event:','') as order_event, 
        order_json::json->>'Id' as Order_ID, 
        order_json->>'EffectiveDate' as Order_Start_Date
  from events.sodium
  where substring(event_name,1,12) = 'Order Event:'
    and created_at::date= (current_date - interval '1 day')

;