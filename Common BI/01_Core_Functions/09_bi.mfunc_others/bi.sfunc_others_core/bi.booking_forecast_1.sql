
-- GMV Forecast

DROP TABLE IF EXISTS bi.booking_forecast_1;
CREATE TABLE bi.booking_forecast_1 as 
SELECT
  LEFT(locale__c,2) as locale,
  round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '1' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/14),0) as Acquisition_Runrate_Per_Day,
    round((CAST(SUM(CASE WHEN Acquisition_New_Customer__c = '0' and (Voucher__c not in ('HERBSTPUTZ10','WINTER10','WINTER15','WINTER25','HITZEFREI15','HERBSTTIGER10','CRMNFR15','PUTZEN10','PUTZEN15','SAUBER20','IMMOTIGER20') or Voucher__c = '' or Voucher__c is null) THEN GMV_Eur ELSE 0 END) as decimal)/14),0) as Rebookings_Runrate_Per_Day,
    DATE_PART('days', 
        DATE_TRUNC('month', NOW()) 
        + '1 MONTH'::INTERVAL 
        - DATE_TRUNC('month', NOW())
    ) as month_days,
   Extract(day from current_date-1) as days
FROM
  bi.orders
WHERE
  Status in ('WAITING CONFIRMATION','NOSHOW PROFESSIONAL','WAITING FOR RESCHEDULE','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','PENDING TO INVOICE','PENDING VALIDATION','WAITING FOR ACCEPTANCE','INVOICED','ALLOCATION AUTO','ALLOCATION PAUSED') 
    AND Order_Creation__c between (cast(current_date as date)- interval '15 days') and (cast(current_date as date)- interval '1 days')
  and order_type = '1' 
GROUP BY
  LEFT(locale__c,2);