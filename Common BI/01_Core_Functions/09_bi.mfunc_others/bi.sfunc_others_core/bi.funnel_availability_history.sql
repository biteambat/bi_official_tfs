
-- Funnel Availability History

DELETE FROM bi.funnel_availability_history WHERE date = current_Date::Date;

INSERT INTO bi.funnel_availability_history
SELECT
  current_date::date as date,
  *
FROM
  bi.funnel_availability;