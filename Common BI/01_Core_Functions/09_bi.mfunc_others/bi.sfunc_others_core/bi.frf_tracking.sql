
DROP TABLE IF EXISTS bi.frf_tracking;
CREATE TABLE bi.frf_tracking as 
SELECT
  t2.sfid as salesforce_id,
  t2.name as Referrer,
  SUM(CASE WHEN t1.status not like '%CANCELLED%' THEN 1 ELSE 0 END) as Valid_Referred_Orders,
  SUM(CASE WHEN t1.status like '%CANCELLED%' THEN 1 ELSE 0 END) as Cancelled_Referred_Orders,
  SUM(CASE WHEN t1.status not like '%CANCELLED%' THEN 1 ELSE 0 END) +SUM(CASE WHEN t1.status like '%CANCELLED%' THEN 1 ELSE 0 END) as All_Orders
FROM
  bi.frf_tracking_1 t1
JOIN
  salesforce.contact t2
ON
  (t1.referred_by__c = t2.sfid)
GROUP BY
  t2.sfid,
  t2.name;