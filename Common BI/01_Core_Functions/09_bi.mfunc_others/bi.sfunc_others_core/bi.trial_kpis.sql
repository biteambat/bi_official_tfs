
DROP TABLE IF EXISTS  bi.trial_kpis;
CREATE TABLE bi.trial_kpis as 
SELECT
  t2.date as date,
  LEFT(locale,2) as locale,
  CAST('trial customer' as varchar) as kpi,
  count(distinct(t1.customer_Id__c)) as value
FROM
(SELECT
  customer_id__c,
  LEFT(locale__c,2) as locale,
  SUM(CASE WHEN (acquisition_channel__c = 'web' and (recurrency__c = '0' or recurrency__c is null) and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null)) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
GROUP BY
  customer_id__c,
  locale
HAVING
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
  (SELECT
      customer_id__c,
      min(order_Creation__c::date) as date
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null)and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
  customer_id__c) as t2
ON
  (t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
    (SELECT
      customer_id__c,
      min(order_Creation__c::date) as date
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
  customer_id__c) as t3
ON
  (t1.customer_id__c = t3.customer_id__c) 
GROUP BY
  t1.locale,
  t2.date;
  
INSERT INTO bi.trial_kpis
SELECT
  t2.date as date,
  t1.locale,
  'subscriber CVR' as kpi,
  count(distinct(t1.customer_Id__c)) as unique_customer
FROM
(SELECT
  customer_id__c,
  LEFT(t1.locale__c,2) as locale,
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
GROUP BY
  customer_id__c,
  locale
HAVING
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
  (SELECT
      customer_id__c,
      min(order_Creation__c::date) as date
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
  customer_id__c) as t2
ON
  (t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
    (SELECT
      customer_id__c,
      min(order_Creation__c::date) as date
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
  customer_id__c) as t3
ON
  (t1.customer_id__c = t3.customer_id__c) 
WHERE
  t3.date is not null
GROUP BY
  t1.locale,
  t2.date;
  
INSERT INTO bi.trial_kpis
  SELECT
  t2.date as date,
  t1.locale,
  'Trials CVR' as kpi,
  count(distinct(t1.customer_Id__c)) as unique_customer
FROM
(SELECT
  customer_id__c,
  LEFT(t1.locale__c,2) as locale,
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Trial_Order,
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c >= '7' and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END) as Subscription_Order
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
GROUP BY
  customer_id__c,
  locale
HAVING
  SUM(CASE WHEN acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%') THEN 1 ELSE 0 END)  > '0') as t1
LEFT JOIN
  (SELECT
      customer_id__c,
      min(order_Creation__c::date) as date
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and acquisition_channel__c = 'web' and recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
  customer_id__c) as t2
ON
  (t1.customer_id__c = t2.customer_id__c) 
LEFT JOIN
    (SELECT
      customer_id__c,
      min(order_Creation__c::date) as date
FROM
  bi.orders
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and acquisition_channel__c = 'web' and recurrency__c > '6' and (status not like '%CANCELLED%' and status not like '%ERROR%')
GROUP BY
  customer_id__c) as t3
ON
  (t1.customer_id__c = t3.customer_id__c) 
GROUP BY
  t1.locale,
  t2.date;


INSERT INTO bi.trial_kpis
SELECT
  order_Creation__c::date as date,
  LEFT(t1.locale__c,2) as locale,
  'All Website Orders' as All_Orders,
  SUM(CASE WHEN acquisition_channel__c = 'web' THEN 1 ELSE 0 END) as all_website_orders
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
GROUP BY
  date,
  locale;

INSERT INTO bi.trial_kpis 
SELECT
  order_Creation__c::date as date,
  LEFT(t1.locale__c,2) as locale,
  'All Website Trial Orders' as All_Orders,
  SUM(CASE WHEN acquisition_channel__c = 'web' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (recurrency__c = '0' or recurrency__c is null) THEN 1 ELSE 0 END) as all_website_orders
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
GROUP BY
  date,
  locale;
  
INSERT INTO bi.trial_kpis 
SELECT
  order_Creation__c::date as date,
  LEFT(t1.locale__c,2) as locale,
  'All Website Trial Acquisitions' as All_Orders,
  SUM(CASE WHEN acquisition_channel__c = 'web' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and (recurrency__c = '0' or recurrency__c is null) THEN 1 ELSE 0 END) as all_website_orders
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and acquisition_New_customer__c = '1'
GROUP BY
  date,
  locale;
  
INSERT INTO bi.trial_kpis
SELECT
  order_Creation__c::date as date,
  LEFT(t1.locale__c,2) as locale,
  'All Website Acquisitions' as All_Orders,
  SUM(CASE WHEN acquisition_channel__c = 'web' THEN 1 ELSE 0 END) as all_website_orders
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and acquisition_new_customer__c = '1'
GROUP BY
  date,
  locale;
  
INSERT INTO bi.trial_kpis
SELECT
  order_Creation__c::date as date,
  LEFT(t1.locale__c,2) as locale,
  CASE 
  WHEN recurrency__c = '0' and (acquisition_tracking_id__c != 'express-checkout' or acquisition_tracking_id__c is null) and acquisition_channel__c in ('web','ios','android') THEN 'Funnel Trials'
  WHEN (IP__c = '62.72.67.58' or acquisition_channel__c = 'salesforce') and acquisition_tracking_id__c = 'express-checkout' THEN 'Last Minute CM'
  WHEN IP__c != '62.72.67.58' and acquisition_tracking_id__c = 'express-checkout' and acquisition_channel__c in ('web','ios','android')  THEN 'Last Minute Customer'
  ELSE 'Other Orders' END as types,
  
  COUNT(DISTINCT(Order_Id__c)) as all_website_orders
FROM
  bi.orders t1
WHERE
  ORder_Creation__c::date >= '2016-07-08'
  and test__c = '0' 
  and order_type = '1'
  and recurrency__c = '0'
GROUP BY
  date,
  locale,
  types;