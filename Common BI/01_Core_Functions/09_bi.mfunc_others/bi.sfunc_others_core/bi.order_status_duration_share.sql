
DROP TABLE IF EXISTS bi.order_status_duration_share;
CREATE TABLE bi.order_status_duration_share AS

SELECT

   o.sfid,
   o.effectivedate,
   EXTRACT(week from o.effectivedate) as week,
   EXTRACT(month from o.effectivedate) as month,
   UPPER(left(o.locale__c,2)) as country,
   o.polygon,
   o.status,
   o.order_duration__c,
   o.professional__c,
   a.name,
   a.status__c

FROM 

  bi.orders o 

LEFT JOIN 

  Salesforce.Account a

ON 

  (o.professional__c = a.sfid)

WHERE 

  o.test__c = '0'
  AND o.effectivedate between current_date-60 and current_date+30
  AND o.status in ('INVOICED','PENDING TO START','CANCELLED NOT THERE YET','CANCELLED NO MANPOWER','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL', 'CANCELLED CUSTOMER SHORT TERM', 'CANCELLED TERMINATED', 'CANCELLED SKIPPED')
  AND professional__c IS NOT NULL

GROUP BY 

   o.sfid,
   o.effectivedate,
   upper(left(o.locale__c,2)),
   o.polygon,
   o.status,
   o.order_duration__c,
   o.professional__c,
   EXTRACT(month from o.effectivedate),
   a.name,
   a.status__c;