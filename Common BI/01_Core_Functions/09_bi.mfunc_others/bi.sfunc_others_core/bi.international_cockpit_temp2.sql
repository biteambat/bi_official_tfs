
DROP TABLE IF EXISTS bi.international_cockpit_temp2;
CREATE TABLE bi.international_cockpit_temp2 as 
SELECT
  date,
  locale,
  SUM(Cleaners_inactive) as cleaner_inactive,
  SUM(Cleaners_active) as Cleaner_active
FROM
  bi.cleaneractivity_change
GROUP BY
  date,
  locale;