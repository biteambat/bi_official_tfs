
DROP TABLE IF EXISTS bi.international_cockpit_temp4;
CREATE TABLE bi.international_cockpit_temp4 as 
SELECT
  CASE WHEN type__c = '60' then hr_contract_start__c::date ELSE createddate::date END as date,
  LEFT(locale__C,2) as locale,
  COUNT(1) as Onboardings
FROM
  salesforce.account
WHERE
  test__c = '0'
  and Status__c != 'SUBCONTRACTOR' and Status__c != ''
GROUP BY
  date,
  locale;   