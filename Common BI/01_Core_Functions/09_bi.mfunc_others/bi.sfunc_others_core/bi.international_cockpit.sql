
DROP TABLE IF EXISTS bi.international_cockpit;
CREATE TABLE bi.international_cockpit as 
SELECT
  t1.*,
  t3.cleaner_inactive,
  t3.cleaner_active,
  CASE WHEN t4.onboardings is null then 0 else t4.onboardings END as onboardings,
  CASE WHEN t5.offboardings is null then 0 else t5.offboardings END as offboardings,
  t6.costs,
  t6.acquisitions,
  t12.leads as leads_verification,
  t12.onboardings as onboardings_verification,
  t12.costs as lead_costs,
  rebookings_retention_m3,
  acquisitions_retention_m3,
  rebookings_retention_m1,
  acquisitions_retention_m1
FROM
  bi.international_cockpit_temp1 t1 
LEFT JOIn
  bi.international_cockpit_temp2 t3
ON
  (t1.locale = t3.locale and t1.date = t3.date)
LEFT JOIN
  bi.international_cockpit_temp3 t5
ON
  (t1.locale = t5.locale and t1.date = t5.date)
LEFT JOIN
  bi.international_cockpit_temp4 t4
ON
  (t1.locale = t4.locale and t1.date = t4.date)
LEFT JOIN
  bi.international_cockpit_temp5 t6
ON
  (t1.locale = t6.locale and t1.date = t6.date)
LEFT JOIN
  (SELECT
  acquisition_date + Interval '60 days' as date,
  locale,
  CAST(SUM(rebooking_m1) as decimal) as rebookings_retention_m1,
  COUNT(DISTINCT(customer_id)) as acquisitions_retention_m1
FROM
  bi.retention_data
GROUP BY
  date,
  locale) t8
ON
  (t1.locale = t8.locale and t1.date = t8.date)
LEFT JOIN
  (SELECT
  acquisition_date + Interval '120 days' as date,
  locale,
  CAST(SUM(rebooking_m3) as decimal) as rebookings_retention_m3,
  COUNT(DISTINCT(customer_id)) as acquisitions_retention_m3
FROM
  bi.retention_data
GROUP BY
  date,
  locale) t9
ON
  (t1.locale = t9.locale and t1.date = t9.date)
LEFT JOIN 
  (SELECT
  date,
  locale,
  sum(classifieds_costs+sem_costs+fb_costs) as costs,
  sum(leads) as leads,
  sum(onboardings) as onboardings
FROM
  bi.cleaners_costsperonboarding_polygon
GROUP BY
  date,
  locale) as t12
ON (t1.locale = t12.locale and t1.date = t12.date)

WHERE
  t1.date >= '2015-01-01';