
DELETE FROM bi.visits_polygon_matching WHERE server_date = (current_date - interval '1 day');
INSERT INTO bi.visits_polygon_matching

	SELECT
	    t1.server_date,
	    t1.visit_id,
	    t1.city,
	    t2.key__c,
		ST_Intersects( 
	    ST_SetSRID(ST_Point(t1.longitude::float, t1.latitude::float), 4326), 
	    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
	FROM
	    external_data.piwik_visits t1,
	    salesforce.delivery_area__c t2
	WHERE
	     t1.server_date = (current_date - interval '1 day')
	GROUP BY
		t1.server_date,
	    t1.visit_id,
	    t1.city,
	    t2.key__c,
	    flag_polygon
	HAVING
	    ST_Intersects(
	    ST_SetSRID(ST_Point(t1.longitude::float, t1.latitude::float), 4326), 
	    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true
;