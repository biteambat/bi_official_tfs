
-- Filters: Only users which really had a transaction. <=> Users for which COUNTD(transaction_id) > 0 &&& Channel NOT IN ('Job traffic').

DROP TABLE IF EXISTS bi.temp_attribution_sample;
CREATE TABLE bi.temp_attribution_sample AS
	SELECT

			t2.*
		FROM

			(
			(SELECT 
				visitor_id,
				COUNT(DISTINCT transaction_id) as ntransactions
			FROM 
				bi.etl_piwik_visits
			GROUP BY visitor_id) as t1

			JOIN bi.etl_piwik_visits t2 
			ON t1.ntransactions > 0 
			AND t1.visitor_id = t2.visitor_id 
			AND t2.attributed_transaction IS NOT NULL
			AND t2.transaction_timestamp IS NOT NULL
			AND t2.converting_visit IS NOT NULL

			)

	ORDER BY t2.visitor_id asc, t2.visit_num asc, t2.firstaction_unixtime asc

;
