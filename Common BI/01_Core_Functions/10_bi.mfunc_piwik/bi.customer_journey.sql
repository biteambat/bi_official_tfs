
DROP TABLE IF EXISTS bi.customer_journey;
CREATE TABLE bi.customer_journey AS

		SELECT
				visitor_id as visitor_id,
				MAX(CONCAT(CASE WHEN transaction_id IS NOT NULL THEN transaction_id ELSE NULL END)) as transaction_id,
				MAX(CONCAT(CASE WHEN visit_num = 1 THEN marketing_channel ELSE NULL END)) as chan_s1,
				MAX(CONCAT(CASE WHEN visit_num = 2 THEN marketing_channel ELSE NULL END)) as chan_s2,
				MAX(CONCAT(CASE WHEN visit_num = 3 THEN marketing_channel ELSE NULL END)) as chan_s3,
				MAX(CONCAT(CASE WHEN visit_num = 4 THEN marketing_channel ELSE NULL END)) as chan_s4,
				MAX(CONCAT(CASE WHEN visit_num = 5 THEN marketing_channel ELSE NULL END)) as chan_s5,
				MAX(CONCAT(CASE WHEN visit_num = 6 THEN marketing_channel ELSE NULL END)) as chan_s6,
				MAX(CONCAT(CASE WHEN visit_num = 7 THEN marketing_channel ELSE NULL END)) as chan_s7,
				MAX(CONCAT(CASE WHEN visit_num = 8 THEN marketing_channel ELSE NULL END)) as chan_s8,
				MAX(CONCAT(CASE WHEN visit_num = 9 THEN marketing_channel ELSE NULL END)) as chan_s9,
				MAX(CONCAT(CASE WHEN visit_num = 10 THEN marketing_channel ELSE NULL END)) as chan_s10,
				MAX(CONCAT(CASE WHEN transaction_id IS NOT NULL THEN marketing_channel ELSE NULL END)) as chan_conv,
				NULL::int as n_visits,
				NULL::numeric as w_sem,
				NULL::numeric as w_semb,
				NULL::numeric as w_seo,
				NULL::numeric as w_dti,
				NULL::numeric as w_aff,
				NULL::numeric as w_dis,
				NULL::numeric as w_fb,
				NULL::numeric as w_unatt,
				NULL::numeric as w_fbo,
				NULL::numeric as w_newsletter,
				NULL::numeric as w_ref,
				NULL::numeric as w_voucher,
				NULL::numeric as w_brandoff

			FROM bi.temp_attribution_sample

			GROUP BY visitor_id

			ORDER BY visitor_id asc

;