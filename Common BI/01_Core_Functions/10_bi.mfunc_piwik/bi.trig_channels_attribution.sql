
DELETE FROM bi.trig_channels_attribution;
INSERT INTO bi.trig_channels_attribution
	SELECT
		t1.visitor_id,
		t1.transaction_id,
		t1.chan_s1,
		t1.chan_s2,
		t1.chan_s3,
		t1.chan_s4,
		t1.chan_s5,
		t1.chan_s6,
		t1.chan_s7,
		t1.chan_s8,
		t1.chan_s9,
		t1.chan_s10,
		t1.chan_conv,
		(CASE WHEN t1.chan_s1 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s2 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s3 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s4 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s5 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s6 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s7 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s8 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s9 != '' THEN 1 ELSE 0 END 
			+ CASE WHEN t1.chan_s10 != '' THEN 1 ELSE 0 END)::int as n_visits,
		t1.w_sem,
		t1.w_semb,
		t1.w_seo,
		t1.w_dti,
		t1.w_aff,
		t1.w_dis,
		t1.w_fb,
		t1.w_unatt,
		t1.w_fbo,
		t1.w_newsletter,
		t1.w_ref,
		t1.w_voucher,
		t1.w_brandoff

	FROM bi.customer_journey t1
;