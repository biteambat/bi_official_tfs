
DROP TABLE IF EXISTS bi.piwik_funnel_conversion;
CREATE TABLE bi.piwik_funnel_conversion AS

    SELECT
        t1.server_date as date,
        LEFT(t1.polygon, 2) as locale,
        t1.polygon,
        t1.visitor_type,
        t1.marketing_channel,

        COUNT(DISTINCT t1.visit_id) as total_visits,
        COUNT(DISTINCT CASE WHEN t1.b2c_visits_leads > 0 THEN t1.visit_id ELSE NULL END) as b2c_visits_leads,
        COUNT(DISTINCT CASE WHEN t1.b2c_visits_step_1 > 0 THEN t1.visit_id ELSE NULL END) as b2c_visits_s1,
        COUNT(DISTINCT CASE WHEN t1.b2c_visits_step_2 > 0 THEN t1.visit_id ELSE NULL END) as b2c_visits_s2,
        COUNT(DISTINCT CASE WHEN t1.b2c_visits_step_3 > 0 THEN t1.visit_id ELSE NULL END) as b2c_visits_s3,
		
		COUNT(DISTINCT t1.visitor_id) as total_visitors,

		COUNT(DISTINCT CASE WHEN t1.b2c_visitors_leads > 0 THEN t1.visit_id ELSE NULL END) as b2c_visitors_leads,
        COUNT(DISTINCT CASE WHEN t1.b2c_visitors_step_1 > 0 THEN t1.visit_id ELSE NULL END) as b2c_visitors_s1,
        COUNT(DISTINCT CASE WHEN t1.b2c_visitors_step_2 > 0 THEN t1.visit_id ELSE NULL END) as b2c_visitors_s2,
        COUNT(DISTINCT CASE WHEN t1.b2c_visitors_step_3 > 0 THEN t1.visit_id ELSE NULL END) as b2c_visitors_s3,

        COUNT(DISTINCT CASE WHEN t1.transactions > 0 THEN t1.visit_id ELSE NULL END) as total_transactions,

		COUNT(DISTINCT CASE WHEN t1.b2b_visits_likelies > 0 THEN t1.visit_id ELSE NULL END) as b2b_visits_likelies,
        COUNT(DISTINCT CASE WHEN t1.b2b_visits_step_1 > 0 THEN t1.visit_id ELSE NULL END) as b2b_visits_s1,
        COUNT(DISTINCT CASE WHEN t1.b2b_visits_step_2 > 0 THEN t1.visit_id ELSE NULL END) as b2b_visits_s2,

		COUNT(DISTINCT CASE WHEN t1.b2b_visitors_likelies > 0 THEN t1.visit_id ELSE NULL END) as b2b_visitors_likelies,
        COUNT(DISTINCT CASE WHEN t1.b2b_visitors_step_1 > 0 THEN t1.visit_id ELSE NULL END) as b2b_visitors_s1,
        COUNT(DISTINCT CASE WHEN t1.b2b_visitors_step_2 > 0 THEN t1.visit_id ELSE NULL END) as b2b_visitors_s2


    FROM

	        (SELECT

	            t1.visit_id,
				t2.visitor_id,
	            t1.server_date,
	            t2.polygon,
				t2.visitor_type,
				t2.marketing_channel,

				COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2B / Likelie' THEN t1.visit_id ELSE NULL END) as b2b_visits_likelies,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2B / Funnel - S1' THEN t1.visit_id ELSE NULL END) as b2b_visits_step_1,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2B / Funnel - S2' THEN t1.visit_id ELSE NULL END) as b2b_visits_step_2,

				COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2B / Likelie' THEN t2.visitor_id ELSE NULL END) as b2b_visitors_likelies,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2B / Funnel - S1' THEN t2.visitor_id ELSE NULL END) as b2b_visitors_step_1,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2B / Funnel - S2' THEN t2.visitor_id ELSE NULL END) as b2b_visitors_step_2,

				COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Lead' THEN t1.visit_id ELSE NULL END) as b2c_visits_leads,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Funnel - S1' THEN t1.visit_id ELSE NULL END) as b2c_visits_step_1,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Funnel - S2' THEN t1.visit_id ELSE NULL END) as b2c_visits_step_2,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Funnel - S3' THEN t1.visit_id ELSE NULL END) as b2c_visits_step_3,
				COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Booking' THEN t1.visit_id ELSE NULL END) as b2c_conversions,

	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Lead' THEN t2.visitor_id ELSE NULL END) as b2c_visitors_leads,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Funnel - S1' THEN t2.visitor_id ELSE NULL END) as b2c_visitors_step_1,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Funnel - S2' THEN t2.visitor_id ELSE NULL END) as b2c_visitors_step_2,
	            COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Funnel - S3' THEN t2.visitor_id ELSE NULL END) as b2c_visitors_step_3,
				COUNT(DISTINCT CASE WHEN t1.goal_name = 'B2C / Booking' THEN t2.visitor_id ELSE NULL END) as b2c_clients,

	            COUNT(DISTINCT CASE WHEN 
	                    t1.transaction_id IS NOT NULL 
	                    AND t1.transaction_id <> ''
	                    AND t1.transaction_total IS NOT NULL 
	                    AND t1.transaction_total > 0
	                THEN t1.transaction_id ELSE NULL END) as transactions

	        FROM external_data.piwik_actions t1

					JOIN bi.etl_piwik_visits t2 ON t1.visit_id = t2.visit_id

	        WHERE t1.server_date > '2016-12-11'

	        GROUP BY t1.visit_id, t1.server_date, t2.polygon, t2.visitor_type,	t2.marketing_channel, t2.visitor_id
	        ORDER BY t1.server_date desc, t1.visit_id asc, t2.polygon asc, t2.visitor_type asc, t2.marketing_channel asc) 

        as t1

    GROUP BY server_date, LEFT(t1.polygon, 2), polygon, visitor_type, marketing_channel

    ORDER BY server_date desc, locale asc, polygon asc, visitor_type asc, marketing_channel asc

;