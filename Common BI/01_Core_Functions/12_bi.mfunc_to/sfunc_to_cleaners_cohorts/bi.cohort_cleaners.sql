
DROP TABLE IF EXISTS bi.cohort_cleaners;
CREATE TABLE bi.cohort_cleaners AS

SELECT
	s.sfid,
	o.delivery_area as city,
	case when o.type = 'cleaning-b2c' or o.type = 'cleaning-b2b' then 'Employed' else 'Freelance' end as cleaners_type,
	case when (s.hr_contract_start__c is not null and (o.type = 'cleaning-b2c' or o.type = 'cleaning-b2b')) then s.hr_contract_start__c::timestamp::date else s.createddate::timestamp::date end as contract_start_date,
	o.effectivedate as order_date,
	o.order_duration__c as order_duration,
	left(o.locale__c,2) as country,
	s.status__c as cleaner_status,
	ending_reason__c,
	o.gmv_eur_net 
FROM
	bi.orders o
JOIN
	salesforce.account s
		ON s.sfid = o.professional__c

WHERE
	o.test__c = '0'
	and s.test__c = '0'
	and o.status in ('INVOICED')
	and s.type__c not in ('cleaning-b2b')

ORDER BY 
	contract_start_date asc,
	order_date asc

;