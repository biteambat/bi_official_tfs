
-- Creation of the table containing the current COP

DROP TABLE IF EXISTS bi.current_cop;
CREATE TABLE bi.current_cop as 
	
SELECT

	a.delivery_areas__c::text as polygon,
	SUM(CASE WHEN (LEFT(a.delivery_areas__c, 2) = 'de' AND a.hr_contract_weekly_hours_min__c < 11) THEN 0.5
	         WHEN (LEFT(a.delivery_areas__c, 2) = 'de' AND a.hr_contract_weekly_hours_min__c >= 11) THEN 1
	              ELSE 1
	              END) as current_cleaners

FROM

	salesforce.account a
	
JOIN
   
	Salesforce.Account t2

ON
   (t2.sfid = a.parentid) 
	
WHERE

	a.status__c IN ('ACTIVE', 'BETA')
	AND a.status__c NOT IN ('SUSPENDED') 
	AND (t2.name LIKE '%BAT%' OR t2.name LIKE '%BOOK%') 
	AND a.test__c = '0' 
	AND a.name NOT LIKE '%test%'
	
GROUP BY 

	a.delivery_areas__c
	
ORDER BY 

	a.delivery_areas__c;
	
	
INSERT INTO bi.current_cop

SELECT 

	a.delivery_areas__c as polygon,
	COUNT(DISTINCT a.sfid)
	
FROM

	salesforce.account a
	
WHERE

	a.status__c IN ('ACTIVE', 'BETA')
	AND a.delivery_areas__c = 'at-vienna'
	
GROUP BY 

	a.delivery_areas__c;	