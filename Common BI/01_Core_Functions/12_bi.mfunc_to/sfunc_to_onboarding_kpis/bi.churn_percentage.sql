
-- Creation of the table containing the churn% of the cleaners in every polygon, to enter manually 

DROP TABLE IF EXISTS bi.churn_percentage;
CREATE TABLE bi.churn_percentage as 
	
SELECT

	0 as churn_at,
	0.05 as churn_basel,
	0.05 as churn_bern,
	0.07 as churn_geneva,
	0 as churn_lausanne,
	0.07 as churn_lucerne,
	0.0 as churn_stgallen,
	0.03 as churn_zurich,
	0.12 as churn_berlin,
	0.0 as churn_bonn,
	0.05 as churn_cologne,
	0 as churn_dusseldorf,
	0 as churn_essen,
	0.1 as churn_frankfurt,
	0.15 as churn_hamburg,
	0.1 as churn_munich,
	0 as churn_nuremberg,
	0 as churn_potsdam,
	0.2 as churn_stuttgart,
	0 as churn_amsterdam,
	0 as churn_hague
	
FROM

	bi.orders
	
LIMIT 

	1;	