
-- Creation of the table for the invoiced hours in B2B in the lase 4 months (120 days)

DROP TABLE IF EXISTS bi.hours_invoiced_b2b;
CREATE TABLE bi.hours_invoiced_b2b as 

SELECT

	t1.polygon,
	t1.type,
	AVG(t1.hours_invoiced) as average_monthly_hours_b2b

FROM

	(SELECT
	
		EXTRACT(YEAR FROM o.effectivedate) as year,
		EXTRACT(MONTH FROM o.effectivedate) as month,
		o.polygon,
		o.type,
		SUM(o.order_duration__c) as hours_invoiced
		
	FROM
	
		bi.orders o
		
	WHERE
	
		o.effectivedate > current_date - 120
		AND o.effectivedate < current_date
		AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'FULFILLED')
		AND o.type NOT LIKE '%training%'
		AND o.type = 'cleaning-b2b'
		
	GROUP BY
	
		year,
		month,
		o.polygon,
		o.type
		
	ORDER BY
	
		year desc,
		month desc,
		o.polygon,
		o.type) as t1
		
WHERE

	t1.month <= EXTRACT(MONTH FROM current_date)
		
GROUP BY

	t1.polygon,
	t1.type
	
ORDER BY

	t1.polygon;	