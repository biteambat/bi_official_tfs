
DROP TABLE IF EXISTS bi.onboarding_kpis;
CREATE TABLE bi.onboarding_kpis AS

	SELECT

		EXTRACT(YEAR FROM to_date(t1.year_month,'YYYY-MM-DD'))::int AS year,
		EXTRACT(MONTH FROM to_date(t1.year_month,'YYYY-MM-DD'))::int AS month,
		t1.delivery_area::text as delivery_area,
		'Cleaners on platform'::text as kpi,
		SUM(CASE WHEN to_char(t1.contract_start,'YYYY-MM') <= t1.year_month and to_char(t1.contract_end,'YYYY-MM') >= t1.year_month THEN 1 ELSE 0 END) AS value

	FROM bi.margin_per_cleaner t1

	GROUP BY year, month, t1.delivery_area, kpi

	ORDER BY year desc, month desc, delivery_area asc, kpi asc

;

INSERT INTO bi.onboarding_kpis

	SELECT

		EXTRACT(YEAR FROM a.hr_contract_start__c) AS year,
		EXTRACT(MONTH FROM a.hr_contract_start__c) AS month,
		a.delivery_areas__c AS delivery_area,
		'Actual onboardings'::text AS kpi,
		COUNT(DISTINCT a.sfid) AS value

	FROM salesforce.account a

	WHERE a.status__c in ('ACTIVE','BETA') AND hr_contract_start__c IS NOT NULL and type__c != 'cleaning-b2b'

	GROUP BY year, month, delivery_area, kpi

	ORDER BY year desc, month desc, delivery_area asc, kpi asc

;


INSERT INTO bi.onboarding_kpis

	SELECT

		year AS year,
		month AS month,
		delivery_area AS delivery_area,
		'Onboarding target'::text AS kpi,
		onboarding_target AS value

	FROM bi.onboarding_targets

;