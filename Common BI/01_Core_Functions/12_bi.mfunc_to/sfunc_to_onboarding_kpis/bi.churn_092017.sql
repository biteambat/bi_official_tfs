
-- Creation table churn_082017

DROP TABLE IF EXISTS bi.churn_092017;
CREATE TABLE bi.churn_092017 as 

SELECT

	t1.polygon,
	t1.current_cleaners,
	CASE WHEN t1.polygon = 'at-vienna' THEN t2.churn_at
		  WHEN t1.polygon = 'ch-basel' THEN t2.churn_basel
		  WHEN t1.polygon = 'ch-bern' THEN t2.churn_bern
		  WHEN t1.polygon = 'ch-geneva' THEN t2.churn_geneva
		  WHEN t1.polygon = 'ch-lausanne' THEN t2.churn_lausanne
		  WHEN t1.polygon = 'ch-lucerne' THEN t2.churn_lucerne
		  WHEN t1.polygon = 'ch-stgallen' THEN t2.churn_stgallen
		  WHEN t1.polygon = 'ch-zurich' THEN t2.churn_zurich
		  WHEN t1.polygon = 'de-berlin' THEN t2.churn_berlin
		  WHEN t1.polygon = 'de-bonn' THEN t2.churn_bonn
		  WHEN t1.polygon = 'de-cologne' THEN t2.churn_cologne
		  WHEN t1.polygon = 'de-dusseldorf' THEN t2.churn_dusseldorf
		  WHEN t1.polygon = 'de-essen' THEN t2.churn_essen
		  WHEN t1.polygon = 'de-frankfurt' THEN t2.churn_frankfurt
		  WHEN t1.polygon = 'de-hamburg' THEN t2.churn_hamburg
		  WHEN t1.polygon = 'de-munich' THEN t2.churn_munich
		  WHEN t1.polygon = 'de-nuremberg' THEN t2.churn_nuremberg
		  WHEN t1.polygon = 'de-potsdam' THEN t2.churn_potsdam
		  WHEN t1.polygon = 'de-stuttgart' THEN t2.churn_stuttgart
		  WHEN t1.polygon = 'nl-amsterdam' THEN t2.churn_amsterdam
		  WHEN t1.polygon = 'nl-hague' THEN t2.churn_hague
		  ELSE NULL
		  END as current_churn,
	ROUND(CASE WHEN t1.polygon = 'at-vienna' THEN t1.current_cleaners * t2.churn_at
		  WHEN t1.polygon = 'ch-basel' THEN t1.current_cleaners * t2.churn_basel
		  WHEN t1.polygon = 'ch-bern' THEN t1.current_cleaners * t2.churn_bern
		  WHEN t1.polygon = 'ch-geneva' THEN t1.current_cleaners * t2.churn_geneva
		  WHEN t1.polygon = 'ch-lausanne' THEN t1.current_cleaners * t2.churn_lausanne
		  WHEN t1.polygon = 'ch-lucerne' THEN t1.current_cleaners * t2.churn_lucerne
		  WHEN t1.polygon = 'ch-stgallen' THEN t1.current_cleaners * t2.churn_stgallen
		  WHEN t1.polygon = 'ch-zurich' THEN t1.current_cleaners * t2.churn_zurich
		  WHEN t1.polygon = 'de-berlin' THEN t1.current_cleaners * t2.churn_berlin
		  WHEN t1.polygon = 'de-bonn' THEN t1.current_cleaners * t2.churn_bonn
		  WHEN t1.polygon = 'de-cologne' THEN t1.current_cleaners * t2.churn_cologne
		  WHEN t1.polygon = 'de-dusseldorf' THEN t1.current_cleaners * t2.churn_dusseldorf
		  WHEN t1.polygon = 'de-essen' THEN t1.current_cleaners * t2.churn_essen
		  WHEN t1.polygon = 'de-frankfurt' THEN t1.current_cleaners * t2.churn_frankfurt
		  WHEN t1.polygon = 'de-hamburg' THEN t1.current_cleaners * t2.churn_hamburg
		  WHEN t1.polygon = 'de-munich' THEN t1.current_cleaners * t2.churn_munich
		  WHEN t1.polygon = 'de-nuremberg' THEN t1.current_cleaners * t2.churn_nuremberg
		  WHEN t1.polygon = 'de-potsdam' THEN t1.current_cleaners * t2.churn_potsdam
		  WHEN t1.polygon = 'de-stuttgart' THEN t1.current_cleaners * t2.churn_stuttgart
		  WHEN t1.polygon = 'nl-amsterdam' THEN t1.current_cleaners * t2.churn_amsterdam
		  WHEN t1.polygon = 'nl-hague' THEN t1.current_cleaners * t2.churn_hague
		  ELSE NULL
		  END) as churn_092017 
	
FROM

	bi.current_cop t1,
	bi.churn_percentage t2
	
ORDER BY

	t1.polygon;