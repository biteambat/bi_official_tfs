
-- Creation of the table containing the hours of the business plan, to enter manually 

DROP TABLE IF EXISTS bi.hours_business_plan;
CREATE TABLE bi.hours_business_plan as 
	
SELECT

	15649 as de_b2c_082017,
	6664 as de_b2b_082017,
	958 as nl_b2b_082017,
	9582 as ch_b2c_082017,
	15962 as de_b2c_092017,
	7981 as de_b2b_092017,
	1058 as nl_b2b_092017,
	9868 as ch_b2c_092017,
	16760 as de_b2c_102017,
	8959 as de_b2b_102017,
	1182 as nl_b2b_102017,
	9663 as ch_b2c_102017,
	17598 as de_b2c_112017,
	9938 as de_b2b_112017,
	1322 as nl_b2b_112017,
	10189 as ch_b2c_112017,
	17598 as de_b2c_122017,
	9938 as de_b2b_122017,
	1322 as nl_b2b_122017,
	10189 as ch_b2c_122017,

	12565 as de_b2c_012018,
    12977 as de_b2b_012018,
    0 as nl_b2b_012018,
    10210 as ch_b2c_012018,

    13430 as de_b2c_022018,
    13892 as de_b2b_022018,
    0 as nl_b2b_022018,
    10721 as ch_b2c_022018,

    14250 as de_b2c_032018,
    14793 as de_b2b_032018,
    0 as nl_b2b_032018,
    11257 as ch_b2c_032018,

    14959 as de_b2c_042018,
    15632 as de_b2b_042018,
    0 as nl_b2b_042018,
    11820 as ch_b2c_042018,

    15540 as de_b2c_052018,
    16429 as de_b2b_052018,
    0 as nl_b2b_052018,
    12411 as ch_b2c_052018,

    15831 as de_b2c_062018,
    17330 as de_b2b_062018,
    0 as nl_b2b_062018,
    13031 as ch_b2c_062018,

    16101 as de_b2c_072018,
    18279 as de_b2b_072018,
    0 as nl_b2b_072018,
    13683 as ch_b2c_072018,

    16356 as de_b2c_082018,
    19201 as de_b2b_082018,
    0 as nl_b2b_082018,
    14367 as ch_b2c_082018,

    16711 as de_b2c_092018,
    20105 as de_b2b_092018,
    0 as nl_b2b_092018,
    15085 as ch_b2c_092018,

    16997 as de_b2c_102018,
    20993 as de_b2b_102018,
    0 as nl_b2b_102018,
    15839 as ch_b2c_102018,

    17325 as de_b2c_112018,
    21867 as de_b2b_112018,
    0 as nl_b2b_112018,
    16631 as ch_b2c_112018,

    16338 as de_b2c_122018,
    22729 as de_b2b_122018,
    0 as nl_b2b_122018,
    17463 as ch_b2c_122018

	
FROM

	bi.orders
	
LIMIT 

	1;