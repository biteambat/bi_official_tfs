
DROP TABLE IF EXISTS bi.Potential_Onboardings;
CREATE TABLE bi.Potential_Onboardings AS


	SELECT

		t4.citya as Polygon1,
		t4.cop as Professionals_platform,
		t3.Potential_onboardings,
		t3.Ending_Last_30days,
		t3.Ending_Next_30days,
		t3.Starting_Last_30days,
		t3.Starting_Next_30days,
		t3.Netto_60days
	
	FROM
	
		(SELECT
		
		Polygon1,
		Professionals_platform,
		Potential_onboardings,
		Ending_Last_30days,
		Ending_Next_30days,
		Starting_Last_30days,
		Starting_Next_30days,
		Netto_60days
		
		FROM
		
			(SELECT
		
		        a.delivery_area__c as Polygon1,
		        SUM(CASE WHEN a.status IN ('Interview scheduled', 'Waiting for contract', 'Documents after interview', 'Contract ready', 'Waiting for greenlight') THEN 1 ELSE 0 END) as Potential_onboardings
		
		    FROM salesforce.lead a
		
		    WHERE 
		
		        a.lastmodifieddate > current_date - 30
		        AND a.delivery_area__c NOT IN ('de-potsdam', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		
		    GROUP BY
		    
		        a.delivery_area__c
		    
		    ORDER BY
		
		        a.delivery_area__c asc) as t1
		        
		LEFT JOIN
		
		    (SELECT
		    
		        b.delivery_areas__c as Polygon2,
		
		        SUM(CASE WHEN hr_contract_end__c::date >= current_date::date - 30 AND hr_contract_end__c::date <= current_date::date THEN 1 ELSE 0 END) as Ending_Last_30days,
		        SUM(CASE WHEN hr_contract_end__c::date <= current_date::date + 30 AND hr_contract_end__c::date > current_date::date THEN 1 ELSE 0 END) as Ending_Next_30days,
		        SUM(CASE WHEN hr_contract_start__c::date >= current_date::date - 30 AND hr_contract_start__c::date <= current_date::date THEN 1 ELSE 0 END) as Starting_Last_30days,  
		        SUM(CASE WHEN hr_contract_start__c::date <= current_date::date + 30 AND hr_contract_start__c::date > current_date::date THEN 1 ELSE 0 END) as Starting_Next_30days,  
		        (SUM(CASE WHEN hr_contract_start__c::date >= current_date::date - 30 AND hr_contract_start__c::date <= current_date::date THEN 1 ELSE 0 END) + SUM(CASE WHEN hr_contract_start__c::date <= current_date::date + 30 AND hr_contract_start__c::date > current_date::date THEN 1 ELSE 0 END) - SUM(CASE WHEN hr_contract_end__c::date >= current_date::date - 30 AND hr_contract_end__c::date <= current_date::date THEN 1 ELSE 0 END) - SUM(CASE WHEN hr_contract_end__c::date <= current_date::date + 30 AND hr_contract_end__c::date > current_date::date THEN 1 ELSE 0 END)) as Netto_60days,
		        COUNT(DISTINCT CASE WHEN (b.status__c IN ('BETA', 'ACTIVE')) AND (hr_contract_start__c <= current_date::date OR delivery_areas__c IN ('at-vienna')) THEN b.sfid ELSE NULL END)  as Professionals_platform
		
		    FROM salesforce.account b
		
		    GROUP BY
		
		      b.delivery_areas__c
		    
		    ORDER BY
		    
		      b.delivery_areas__c asc) as t2
		        
		ON
		
		    (t1.Polygon1 = t2.Polygon2)) as t3
		    
	LEFT JOIN
	
		(SELECT
		
			EXTRACT(MONTH FROM max_date::date) as Month,
			EXTRACT(YEAR FROM max_date::date) as Year,
			MIN(max_date::date) as mindate,
			delivery_areas__c as citya,
			CAST('COP Cleaner' as text) as kpi,
			CAST('-' as text) as breakdown,
			COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date < hr_contract_end__c THEN sfid ELSE null END)) as cop
			
		FROM
		
		(SELECT
			TO_CHAR(date,'YYYY-MM') as Month,
			MAX(Date) as max_date
		FROM
			(select i::date as date from generate_series('2016-01-01', 
		  current_date::date, '1 day'::interval) i) as dateta
		  GROUP BY
		 	Month) as a,
		 	(SELECT
		 		a.*
		 	FROM
		   Salesforce.Account a
		    JOIN
		        Salesforce.Account t2
		    ON
		        (t2.sfid = a.parentid)
		
		    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
			and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
			
		 GROUP BY
		 	Year,
		 	Month,
		 	max_date::date,
		 	delivery_areas__c) as t4
		 	
	ON
	
		(t3.Polygon1 = t4.citya) 
		
	WHERE
	
		
		t3.Polygon1 = t4.citya
		AND t4.mindate = current_date;