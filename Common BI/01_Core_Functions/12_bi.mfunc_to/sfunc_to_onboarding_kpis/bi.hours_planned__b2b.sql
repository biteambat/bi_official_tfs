
-- Creating the table containing the forecast b2b for every city by using their share and the hours of the business plan

DROP TABLE IF EXISTS bi.hours_planned__b2b;
CREATE TABLE bi.hours_planned__b2b as

SELECT

t1.polygon,
-- t1.type,
t1.average_monthly_hours_b2b,
t1.share_b2b,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_082017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_082017
ELSE 0
END as forecast_b2b_082017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_092017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_092017
ELSE 0
END as forecast_b2b_092017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_102017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_102017
ELSE 0
END as forecast_b2b_102017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_112017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_112017
ELSE 0
END as forecast_b2b_112017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_122017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_122017
ELSE 0
END as forecast_b2b_122017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_012018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_012018
ELSE 0
END as forecast_b2b_012018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_022018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_022018
ELSE 0
END as forecast_b2b_022018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_032018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_032018
ELSE 0
END as forecast_b2b_032018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
ELSE 0
END as forecast_b2b_042018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_052018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_052018
ELSE 0
END as forecast_b2b_052018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_062018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_062018
ELSE 0
END as forecast_b2b_062018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_072018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_072018
ELSE 0
END as forecast_b2b_072018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_082018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_082018
ELSE 0
END as forecast_b2b_082018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_092018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_092018
ELSE 0
END as forecast_b2b_092018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_102018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_102018
ELSE 0
END as forecast_b2b_102018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_112018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_112018
ELSE 0
END as forecast_b2b_112018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_122018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_122018
ELSE 0
END as forecast_b2b_122018




FROM

bi.hours_invoiced__b2b t1,
bi.hours_business_plan t3

GROUP BY

t1.polygon,
t1.average_monthly_hours_b2b,
t1.share_b2b,
forecast_b2b_082017,
forecast_b2b_092017,
forecast_b2b_102017,
forecast_b2b_112017,
forecast_b2b_122017,
forecast_b2b_012018,
forecast_b2b_022018,
forecast_b2b_032018,
forecast_b2b_042018,
forecast_b2b_052018,
forecast_b2b_062018,
forecast_b2b_072018,
forecast_b2b_082018,
forecast_b2b_092018,
forecast_b2b_102018,
forecast_b2b_112018,
forecast_b2b_122018


ORDER BY

  t1.polygon;