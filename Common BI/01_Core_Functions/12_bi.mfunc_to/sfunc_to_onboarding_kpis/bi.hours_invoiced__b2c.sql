
-- Creation of the table containing the average monthly hours b2c and the share for every polygon

DROP TABLE IF EXISTS bi.hours_invoiced__b2c;
CREATE TABLE bi.hours_invoiced__b2c as 

SELECT

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2c,
	CASE WHEN LEFT(polygon, 2) = 'de' THEN t1.average_monthly_hours_b2c/t2.tot_hours_b2c_de 
	     WHEN LEFT(polygon, 2) = 'nl' THEN t1.average_monthly_hours_b2c/t3.tot_hours_b2c_nl 
		  WHEN LEFT(polygon, 2) = 'ch' THEN t1.average_monthly_hours_b2c/t4.tot_hours_b2c_ch
		  ELSE 0
		  END
		  as share_b2c

FROM

	bi.hours_invoiced_b2c t1,
	bi.hours_tot_b2c_de t2,
	bi.hours_tot_b2c_nl t3,
	bi.hours_tot_b2c_ch t4
	
GROUP BY

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2c,
	share_b2c;