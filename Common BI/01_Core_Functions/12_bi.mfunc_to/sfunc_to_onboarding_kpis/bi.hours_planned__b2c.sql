
-- Creating the table containing the forecast b2c for every city by using their share and the hours of the business plan
	
DROP TABLE IF EXISTS bi.hours_planned__b2c;
CREATE TABLE bi.hours_planned__b2c as 

SELECT

	t1.polygon,
	-- t1.type,
	t1.average_monthly_hours_b2c,
	t1.share_b2c,
	CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_082017
	     WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_082017
	     ELSE 0 
	     END as forecast_b2c_082017,
   CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_092017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_092017
        ELSE 0 
        END as forecast_b2c_092017,
   CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_102017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_102017
        ELSE 0 
        END as forecast_b2c_102017,
   CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_112017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_112017
        ELSE 0 
        END as forecast_b2c_112017,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_122017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_122017
        ELSE 0 
        END as forecast_b2c_122017,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_012018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_012018
        ELSE 0 
        END as forecast_b2c_012018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_022018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_022018
        ELSE 0 
        END as forecast_b2c_022018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_032018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_032018
        ELSE 0 
        END as forecast_b2c_032018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_042018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_042018
        ELSE 0 
        END as forecast_b2c_042018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_052018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_052018
        ELSE 0 
        END as forecast_b2c_052018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_062018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_062018
        ELSE 0 
        END as forecast_b2c_062018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_072018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_072018
        ELSE 0 
        END as forecast_b2c_072018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_082018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_082018
        ELSE 0 
        END as forecast_b2c_082018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_092018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_092018
        ELSE 0 
        END as forecast_b2c_092018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_102018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_102018
        ELSE 0 
        END as forecast_b2c_102018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_112018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_112018
        ELSE 0 
        END as forecast_b2c_112018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_122018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_122018
        ELSE 0 
        END as forecast_b2c_122018



		
FROM

	bi.hours_invoiced__b2c t1,
	bi.hours_business_plan t3
	
GROUP BY

	t1.polygon,
	t1.average_monthly_hours_b2c,
	t1.share_b2c,
	forecast_b2c_082017,
	forecast_b2c_092017,
	forecast_b2c_102017,
	forecast_b2c_112017,
	forecast_b2c_122017,
	forecast_b2c_012018,
	forecast_b2c_022018,
	forecast_b2c_032018,
	forecast_b2c_042018,
    forecast_b2c_052018,
    forecast_b2c_062018,
    forecast_b2c_072018,
    forecast_b2c_082018,
    forecast_b2c_092018,
    forecast_b2c_102018,
    forecast_b2c_112018,
    forecast_b2c_122018

	
ORDER BY
	
	t1.polygon;