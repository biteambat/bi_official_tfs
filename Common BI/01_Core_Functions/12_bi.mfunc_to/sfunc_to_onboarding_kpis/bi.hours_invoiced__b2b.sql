
-- Creation of the table containing the average monthly hours b2b and the share for every polygon

DROP TABLE IF EXISTS bi.hours_invoiced__b2b;
CREATE TABLE bi.hours_invoiced__b2b as 

SELECT

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2b,
	CASE WHEN LEFT(polygon, 2) = 'de' THEN t1.average_monthly_hours_b2b/t2.tot_hours_b2b_de 
	     WHEN LEFT(polygon, 2) = 'nl' THEN t1.average_monthly_hours_b2b/t3.tot_hours_b2b_nl 
	     WHEN LEFT(polygon, 2) = 'ch' THEN t1.average_monthly_hours_b2b/t4.tot_hours_b2b_ch
		  ELSE 0
		  END
		  as share_b2b

FROM

	bi.hours_invoiced_b2b t1,
	bi.hours_tot_b2b_de t2,
	bi.hours_tot_b2b_nl t3,
	bi.hours_tot_b2b_ch t4
	
GROUP BY

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2b,
	share_b2b;	