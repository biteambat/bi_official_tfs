
-- Creation table cleaners to hire

DROP TABLE IF EXISTS bi.cleaners_to_hire;
CREATE TABLE bi.cleaners_to_hire as 

SELECT

	t5.polygon,
	t5.current_cleaners as COP,
	ROUND(t6.hours_per_cleaner) as hours_per_cleaner,
	t5.current_churn,
	CASE WHEN t5.cleaners_to_hire_092017 < 0 THEN 0 ELSE t5.cleaners_to_hire_092017 END as cleaners_to_hire_092017,
	CASE WHEN t5.cleaners_to_hire_102017 < 0 THEN 0 ELSE t5.cleaners_to_hire_102017 END as cleaners_to_hire_102017,
	CASE WHEN t5.cleaners_to_hire_112017 < 0 THEN 0 ELSE t5.cleaners_to_hire_112017 END as cleaners_to_hire_112017,
	CASE WHEN t5.cleaners_to_hire_122017 < 0 THEN 0 ELSE t5.cleaners_to_hire_122017 END as cleaners_to_hire_122017,
	CASE WHEN t5.cleaners_to_hire_012018 < 0 THEN 0 ELSE t5.cleaners_to_hire_012018 END as cleaners_to_hire_012018,
	CASE WHEN t5.cleaners_to_hire_022018 < 0 THEN 0 ELSE t5.cleaners_to_hire_022018 END as cleaners_to_hire_022018,
	CASE WHEN t5.cleaners_to_hire_032018 < 0 THEN 0 ELSE t5.cleaners_to_hire_032018 END as cleaners_to_hire_032018,
	CASE WHEN t5.cleaners_to_hire_042018 < 0 THEN 0 ELSE t5.cleaners_to_hire_042018 END as cleaners_to_hire_042018,
	CASE WHEN t5.cleaners_to_hire_052018 < 0 THEN 0 ELSE t5.cleaners_to_hire_052018 END as cleaners_to_hire_052018,
	CASE WHEN t5.cleaners_to_hire_062018 < 0 THEN 0 ELSE t5.cleaners_to_hire_062018 END as cleaners_to_hire_062018,
	CASE WHEN t5.cleaners_to_hire_072018 < 0 THEN 0 ELSE t5.cleaners_to_hire_072018 END as cleaners_to_hire_072018,
	CASE WHEN t5.cleaners_to_hire_082018 < 0 THEN 0 ELSE t5.cleaners_to_hire_082018 END as cleaners_to_hire_082018,
	CASE WHEN t5.cleaners_to_hire_092018 < 0 THEN 0 ELSE t5.cleaners_to_hire_092018 END as cleaners_to_hire_092018,
	CASE WHEN t5.cleaners_to_hire_102018 < 0 THEN 0 ELSE t5.cleaners_to_hire_102018 END as cleaners_to_hire_102018,
	CASE WHEN t5.cleaners_to_hire_112018 < 0 THEN 0 ELSE t5.cleaners_to_hire_112018 END as cleaners_to_hire_112018,
   CASE WHEN t5.cleaners_to_hire_122018 < 0 THEN 0 ELSE t5.cleaners_to_hire_122018 END as cleaners_to_hire_122018,

	ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12) as distributed_monthly_hiring,
    CASE WHEN LEFT(t5.polygon, 2) = 'de' THEN CASE WHEN ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
         										   ELSE ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4)/0.0456
                                                   END 
         ELSE CASE WHEN ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                   ELSE ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4)/0.06
                   END
		 END as weekly_leads_needed
	
FROM

	(SELECT
	
		t3.polygon,
		t3.current_cleaners,
		t3.current_churn,
		t3.churn_092017,
		t3.cleaners_needed_092017,
		t3.cleaners_to_hire_092017,
		ROUND(t3.cleaners_needed_092017*t3.current_churn) as churn_102017,
		t4.cleaners_needed_102017,
		ROUND(t4.cleaners_needed_102017 - t3.cleaners_needed_092017 + t3.cleaners_needed_092017*t3.current_churn) as cleaners_to_hire_102017,
		ROUND(t4.cleaners_needed_102017*t3.current_churn) as churn_112017,
		t4.cleaners_needed_112017,
		ROUND(t4.cleaners_needed_112017 - t4.cleaners_needed_112017 + t4.cleaners_needed_102017*t3.current_churn) as cleaners_to_hire_112017,

		   ROUND(t4.cleaners_needed_112017*t3.current_churn) as churn_122017,
        t4.cleaners_needed_122017,
        ROUND(t4.cleaners_needed_122017 - t4.cleaners_needed_112017 + t4.cleaners_needed_112017*t3.current_churn) as cleaners_to_hire_122017,

        ROUND(t4.cleaners_needed_122017*t3.current_churn) as churn_012018,
        t4.cleaners_needed_012018,
        ROUND(t4.cleaners_needed_012018 - t4.cleaners_needed_122017 + t4.cleaners_needed_122017*t3.current_churn) as cleaners_to_hire_012018,

        ROUND(t4.cleaners_needed_012018*t3.current_churn) as churn_022018,
        t4.cleaners_needed_022018,
        ROUND(t4.cleaners_needed_022018 - t4.cleaners_needed_012018 + t4.cleaners_needed_012018*t3.current_churn) as cleaners_to_hire_022018,

        ROUND(t4.cleaners_needed_022018*t3.current_churn) as churn_032018,
        t4.cleaners_needed_032018,
        ROUND(t4.cleaners_needed_032018 - t4.cleaners_needed_022018 + t4.cleaners_needed_022018*t3.current_churn) as cleaners_to_hire_032018,

        ROUND(t4.cleaners_needed_032018*t3.current_churn) as churn_042018,
        t4.cleaners_needed_042018,
        ROUND(t4.cleaners_needed_042018 - t4.cleaners_needed_032018 + t4.cleaners_needed_032018*t3.current_churn) as cleaners_to_hire_042018,

        ROUND(t4.cleaners_needed_042018*t3.current_churn) as churn_052018,
        t4.cleaners_needed_052018,
        ROUND(t4.cleaners_needed_052018 - t4.cleaners_needed_042018 + t4.cleaners_needed_042018*t3.current_churn) as cleaners_to_hire_052018,

        ROUND(t4.cleaners_needed_052018*t3.current_churn) as churn_062018,
        t4.cleaners_needed_062018,
        ROUND(t4.cleaners_needed_062018 - t4.cleaners_needed_052018 + t4.cleaners_needed_052018*t3.current_churn) as cleaners_to_hire_062018,

        ROUND(t4.cleaners_needed_062018*t3.current_churn) as churn_072018,
        t4.cleaners_needed_072018,
        ROUND(t4.cleaners_needed_072018 - t4.cleaners_needed_062018 + t4.cleaners_needed_062018*t3.current_churn) as cleaners_to_hire_072018,

        ROUND(t4.cleaners_needed_072018*t3.current_churn) as churn_082018,
        t4.cleaners_needed_082018,
        ROUND(t4.cleaners_needed_082018 - t4.cleaners_needed_072018 + t4.cleaners_needed_072018*t3.current_churn) as cleaners_to_hire_082018,

        ROUND(t4.cleaners_needed_082018*t3.current_churn) as churn_092018,
        t4.cleaners_needed_092018,
        ROUND(t4.cleaners_needed_092018 - t4.cleaners_needed_082018 + t4.cleaners_needed_082018*t3.current_churn) as cleaners_to_hire_092018,

        ROUND(t4.cleaners_needed_092018*t3.current_churn) as churn_102018,
        t4.cleaners_needed_102018,
        ROUND(t4.cleaners_needed_102018 - t4.cleaners_needed_092018 + t4.cleaners_needed_092018*t3.current_churn) as cleaners_to_hire_102018,

        ROUND(t4.cleaners_needed_102018*t3.current_churn) as churn_112018,
        t4.cleaners_needed_112018,
        ROUND(t4.cleaners_needed_112018 - t4.cleaners_needed_102018 + t4.cleaners_needed_102018*t3.current_churn) as cleaners_to_hire_112018,

        ROUND(t4.cleaners_needed_112018*t3.current_churn) as churn_122018,
        t4.cleaners_needed_122018,
        ROUND(t4.cleaners_needed_122018 - t4.cleaners_needed_112018 + t4.cleaners_needed_112018*t3.current_churn) as cleaners_to_hire_122018
	
	
	FROM
	
	
		(SELECT
		
			t1.polygon,
			t1.current_cleaners,
			t1.current_churn,
			t1.churn_092017,
			t2.cleaners_needed_092017,
			CASE WHEN (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) < 0 THEN 0 ELSE (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) END as cleaners_to_hire_092017
			
		FROM
		
			bi.churn_092017 t1
		
		LEFT JOIN
		
			bi.cleaners_needed t2
			
		ON
		
			t1.polygon = t2.polygon) as t3
			
	LEFT JOIN
	
		bi.cleaners_needed t4
		
	ON 
	
		t3.polygon = t4.polygon) as t5
		
LEFT JOIN

	bi.hours_per_cleaner as t6
	
ON 

	t5.polygon = t6.polygon;	


----------------------------------------------------------------------


INSERT INTO bi.cleaners_to_hire

SELECT

	t3.polygon,
	t3.COP,
	t3.hours_per_cleaner,
	t3.current_churn,
	CASE WHEN t3.cleaners_to_hire_092017 < 0 THEN 0 ELSE t3.cleaners_to_hire_092017 END as cleaners_to_hire_092017,
	CASE WHEN t3.cleaners_to_hire_102017 < 0 THEN 0 ELSE t3.cleaners_to_hire_102017 END as cleaners_to_hire_102017,
	CASE WHEN t3.cleaners_to_hire_112017 < 0 THEN 0 ELSE t3.cleaners_to_hire_112017 END as cleaners_to_hire_112017,
	CASE WHEN t3.cleaners_to_hire_122017 < 0 THEN 0 ELSE t3.cleaners_to_hire_122017 END as cleaners_to_hire_122017,
   CASE WHEN t3.cleaners_to_hire_012018 < 0 THEN 0 ELSE t3.cleaners_to_hire_012018 END as cleaners_to_hire_012018,
   CASE WHEN t3.cleaners_to_hire_022018 < 0 THEN 0 ELSE t3.cleaners_to_hire_022018 END as cleaners_to_hire_022018,
   CASE WHEN t3.cleaners_to_hire_032018 < 0 THEN 0 ELSE t3.cleaners_to_hire_032018 END as cleaners_to_hire_032018,
   CASE WHEN t3.cleaners_to_hire_042018 < 0 THEN 0 ELSE t3.cleaners_to_hire_042018 END as cleaners_to_hire_042018,
   CASE WHEN t3.cleaners_to_hire_052018 < 0 THEN 0 ELSE t3.cleaners_to_hire_052018 END as cleaners_to_hire_052018,
   CASE WHEN t3.cleaners_to_hire_062018 < 0 THEN 0 ELSE t3.cleaners_to_hire_062018 END as cleaners_to_hire_062018,
   CASE WHEN t3.cleaners_to_hire_072018 < 0 THEN 0 ELSE t3.cleaners_to_hire_072018 END as cleaners_to_hire_072018,
   CASE WHEN t3.cleaners_to_hire_082018 < 0 THEN 0 ELSE t3.cleaners_to_hire_082018 END as cleaners_to_hire_082018,
   CASE WHEN t3.cleaners_to_hire_092018 < 0 THEN 0 ELSE t3.cleaners_to_hire_092018 END as cleaners_to_hire_092018,
   CASE WHEN t3.cleaners_to_hire_102018 < 0 THEN 0 ELSE t3.cleaners_to_hire_102018 END as cleaners_to_hire_102018,
   CASE WHEN t3.cleaners_to_hire_112018 < 0 THEN 0 ELSE t3.cleaners_to_hire_112018 END as cleaners_to_hire_112018,
   CASE WHEN t3.cleaners_to_hire_122018 < 0 THEN 0 ELSE t3.cleaners_to_hire_122018 END as cleaners_to_hire_122018,

	CASE WHEN t3.distributed_monthly_hiring < 0 THEN 0 ELSE t3.distributed_monthly_hiring END as distributed_monthly_hiring,

	-- CASE WHEN ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) = 0 THEN 1
	     -- ELSE ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) 
		  -- END as distributed_weekly_hiring,
    CASE WHEN UPPER(LEFT(t3.polygon, 2)) = 'DE' THEN CASE WHEN ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                                          ELSE ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4)/0.0456
                                          END
         ELSE CASE WHEN ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                   ELSE ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4)/0.06
                   END
			END as weekly_leads_needed

FROM


	(SELECT
	
		UPPER(LEFT(t5.polygon, 2)) as polygon,
		SUM(t5.current_cleaners) as COP,
		AVG(ROUND(t6.hours_per_cleaner)) as hours_per_cleaner,
		AVG(t5.current_churn) as current_churn,
		SUM(t5.cleaners_to_hire_092017) as cleaners_to_hire_092017,
		SUM(t5.cleaners_to_hire_102017) as cleaners_to_hire_102017,
		SUM(t5.cleaners_to_hire_112017) as cleaners_to_hire_112017,
		SUM(t5.cleaners_to_hire_122017) as cleaners_to_hire_122017,
        SUM(t5.cleaners_to_hire_012018) as cleaners_to_hire_012018,
        SUM(t5.cleaners_to_hire_022018) as cleaners_to_hire_022018,
        SUM(t5.cleaners_to_hire_032018) as cleaners_to_hire_032018,
        SUM(t5.cleaners_to_hire_042018) as cleaners_to_hire_042018,
        SUM(t5.cleaners_to_hire_052018) as cleaners_to_hire_052018,
        SUM(t5.cleaners_to_hire_062018) as cleaners_to_hire_062018,
        SUM(t5.cleaners_to_hire_072018) as cleaners_to_hire_072018,
        SUM(t5.cleaners_to_hire_082018) as cleaners_to_hire_082018,
        SUM(t5.cleaners_to_hire_092018) as cleaners_to_hire_092018,
        SUM(t5.cleaners_to_hire_102018) as cleaners_to_hire_102018,
        SUM(t5.cleaners_to_hire_112018) as cleaners_to_hire_112018,
        SUM(t5.cleaners_to_hire_122018) as cleaners_to_hire_122018,

        ROUND(SUM(t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12) as distributed_monthly_hiring

	FROM
	
		(SELECT
		
			t3.polygon,
			t3.current_cleaners,
			t3.current_churn,
			t3.churn_092017,
			t3.cleaners_needed_092017,
			t3.cleaners_to_hire_092017,
			ROUND(t3.cleaners_needed_092017*t3.current_churn) as churn_102017,
			t4.cleaners_needed_102017,
			ROUND(t4.cleaners_needed_102017 - t3.cleaners_needed_092017 + t3.cleaners_needed_092017*t3.current_churn) as cleaners_to_hire_102017,
			ROUND(t4.cleaners_needed_102017*t3.current_churn) as churn_112017,
			t4.cleaners_needed_112017,
			ROUND(t4.cleaners_needed_112017 - t4.cleaners_needed_112017 + t4.cleaners_needed_102017*t3.current_churn) as cleaners_to_hire_112017,

			ROUND(t4.cleaners_needed_112017*t3.current_churn) as churn_122017,
            t4.cleaners_needed_122017,
            ROUND(t4.cleaners_needed_122017 - t4.cleaners_needed_112017 + t4.cleaners_needed_112017*t3.current_churn) as cleaners_to_hire_122017,

            ROUND(t4.cleaners_needed_122017*t3.current_churn) as churn_012018,
            t4.cleaners_needed_012018,
            ROUND(t4.cleaners_needed_012018 - t4.cleaners_needed_122017 + t4.cleaners_needed_122017*t3.current_churn) as cleaners_to_hire_012018,

            ROUND(t4.cleaners_needed_012018*t3.current_churn) as churn_022018,
            t4.cleaners_needed_022018,
            ROUND(t4.cleaners_needed_022018 - t4.cleaners_needed_012018 + t4.cleaners_needed_012018*t3.current_churn) as cleaners_to_hire_022018,

            ROUND(t4.cleaners_needed_022018*t3.current_churn) as churn_032018,
            t4.cleaners_needed_032018,
            ROUND(t4.cleaners_needed_032018 - t4.cleaners_needed_022018 + t4.cleaners_needed_022018*t3.current_churn) as cleaners_to_hire_032018,

            ROUND(t4.cleaners_needed_032018*t3.current_churn) as churn_042018,
            t4.cleaners_needed_042018,
            ROUND(t4.cleaners_needed_042018 - t4.cleaners_needed_032018 + t4.cleaners_needed_032018*t3.current_churn) as cleaners_to_hire_042018,

            ROUND(t4.cleaners_needed_042018*t3.current_churn) as churn_052018,
            t4.cleaners_needed_052018,
            ROUND(t4.cleaners_needed_052018 - t4.cleaners_needed_042018 + t4.cleaners_needed_042018*t3.current_churn) as cleaners_to_hire_052018,

            ROUND(t4.cleaners_needed_052018*t3.current_churn) as churn_062018,
            t4.cleaners_needed_062018,
            ROUND(t4.cleaners_needed_062018 - t4.cleaners_needed_052018 + t4.cleaners_needed_052018*t3.current_churn) as cleaners_to_hire_062018,

            ROUND(t4.cleaners_needed_062018*t3.current_churn) as churn_072018,
            t4.cleaners_needed_072018,
            ROUND(t4.cleaners_needed_072018 - t4.cleaners_needed_062018 + t4.cleaners_needed_062018*t3.current_churn) as cleaners_to_hire_072018,

            ROUND(t4.cleaners_needed_072018*t3.current_churn) as churn_082018,
            t4.cleaners_needed_082018,
            ROUND(t4.cleaners_needed_082018 - t4.cleaners_needed_072018 + t4.cleaners_needed_072018*t3.current_churn) as cleaners_to_hire_082018,

            ROUND(t4.cleaners_needed_082018*t3.current_churn) as churn_092018,
            t4.cleaners_needed_092018,
            ROUND(t4.cleaners_needed_092018 - t4.cleaners_needed_082018 + t4.cleaners_needed_082018*t3.current_churn) as cleaners_to_hire_092018,

            ROUND(t4.cleaners_needed_092018*t3.current_churn) as churn_102018,
            t4.cleaners_needed_102018,
            ROUND(t4.cleaners_needed_102018 - t4.cleaners_needed_092018 + t4.cleaners_needed_092018*t3.current_churn) as cleaners_to_hire_102018,

            ROUND(t4.cleaners_needed_102018*t3.current_churn) as churn_112018,
            t4.cleaners_needed_112018,
            ROUND(t4.cleaners_needed_112018 - t4.cleaners_needed_102018 + t4.cleaners_needed_102018*t3.current_churn) as cleaners_to_hire_112018,

            ROUND(t4.cleaners_needed_112018*t3.current_churn) as churn_122018,
            t4.cleaners_needed_122018,
            ROUND(t4.cleaners_needed_122018 - t4.cleaners_needed_112018 + t4.cleaners_needed_112018*t3.current_churn) as cleaners_to_hire_122018
	
		
		FROM
		
		
			(SELECT
			
				t1.polygon,
				t1.current_cleaners,
				t1.current_churn,
				t1.churn_092017,
				t2.cleaners_needed_092017,
				CASE WHEN (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) < 0 THEN 0 ELSE (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) END as cleaners_to_hire_092017
				
			FROM
			
				bi.churn_092017 t1
			
			LEFT JOIN
			
				bi.cleaners_needed t2
				
			ON
			
				t1.polygon = t2.polygon) as t3
				
		LEFT JOIN
		
			bi.cleaners_needed t4
			
		ON 
		
			t3.polygon = t4.polygon) as t5
			
	LEFT JOIN
	
		bi.hours_per_cleaner as t6
		
	ON 
	
		t5.polygon = t6.polygon
		
	GROUP BY
	
		UPPER(LEFT(t5.polygon, 2))) t3;