
-- Creation of a small table containing only the sum of the average monthly hours b2c NL

DROP TABLE IF EXISTS bi.hours_tot_b2c_nl;
CREATE TABLE bi.hours_tot_b2c_nl as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2c) as tot_hours_b2c_nl
	

FROM

	bi.hours_invoiced_b2c
	
WHERE

	LEFT(polygon, 2) = 'nl';