
DROP TABLE IF EXISTS bi.comparison_forecast;
CREATE TABLE bi.comparison_forecast as

SELECT

	'null'::text as polygon,
	'type_bp_b2c'::text as type_bp,
	0::numeric as hours_082017,
	0::numeric as hours_092017,
	0::numeric as hours_102017,
	0::numeric as hours_112017,
	0::numeric as hours_122017,
	0::numeric as hours_012018,
	0::numeric as hours_022018,
	0::numeric as hours_032018,
	0::numeric as hours_042018,
	0::numeric as hours_052018,
	0::numeric as hours_062018,
	0::numeric as hours_072018,
	0::numeric as hours_082018,
	0::numeric as hours_092018,
	0::numeric as hours_102018,
	0::numeric as hours_112018,
	0::numeric as hours_122018,
	0::numeric as current_hours

FROM

	bi.orders
	
LIMIT 

	1;


DELETE FROM bi.comparison_forecast;


INSERT INTO bi.comparison_forecast

SELECT

	t1.polygon,
	'type_bp_b2c' as type_bp,
	t1.hours_082017_b2c,
	t1.hours_092017_b2c,
	t1.hours_102017_b2c,
	t1.hours_112017_b2c,

	t1.hours_122017_b2c,
	t1.hours_012018_b2c,
	t1.hours_022018_b2c,
	t1.hours_032018_b2c,
	t1.hours_042018_b2c,
	t1.hours_052018_b2c,
	t1.hours_062018_b2c,
	t1.hours_072018_b2c,
	t1.hours_082018_b2c,
	t1.hours_092018_b2c,
	t1.hours_102018_b2c,
	t1.hours_112018_b2c,
	t1.hours_122018_b2c,
	

	SUM(o.order_duration__c) as current_hours

FROM

	(SELECT
	
		*
		
	FROM 
	
		bi.hours_planned) t1
		
LEFT JOIN

	bi.orders o 
	
ON 

	t1.polygon = o.polygon
	
WHERE

	EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
	AND EXTRACT(MONTH FROM o.effectivedate) = EXTRACT(MONTH FROM current_date)
	AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
	AND o.type = 'cleaning-b2c'
	
GROUP BY

	t1.polygon,
	type_bp,
	t1.hours_082017_b2c,
	t1.hours_092017_b2c,
	t1.hours_102017_b2c,
	t1.hours_112017_b2c,
	t1.hours_122017_b2c,
	t1.hours_012018_b2c,
	t1.hours_022018_b2c,
	t1.hours_032018_b2c,
	t1.hours_042018_b2c,
	t1.hours_052018_b2c,
	t1.hours_062018_b2c,
	t1.hours_072018_b2c,
	t1.hours_082018_b2c,
	t1.hours_092018_b2c,
	t1.hours_102018_b2c,
	t1.hours_112018_b2c,
	t1.hours_122018_b2c;
	
	
------------------------------------------------------------------------------------------	

INSERT INTO bi.comparison_forecast
	
SELECT

	t1.polygon,
	'type_bp_b2b' as type_bp,
	t1.hours_082017_b2b,
	t1.hours_092017_b2b,
	t1.hours_102017_b2b,
	t1.hours_112017_b2b,
	t1.hours_122017_b2b,
	t1.hours_012018_b2b,
	t1.hours_022018_b2b,
	t1.hours_032018_b2b,
	t1.hours_042018_b2b,
	t1.hours_052018_b2b,
	t1.hours_062018_b2b,
	t1.hours_072018_b2b,
	t1.hours_082018_b2b,
	t1.hours_092018_b2b,
	t1.hours_102018_b2b,
	t1.hours_112018_b2b,
	t1.hours_122018_b2b,
	SUM(o.order_duration__c)*0.4 as current_hours

FROM

	(SELECT
	
		*
		
	FROM 
	
		bi.hours_planned) t1
		
LEFT JOIN

	bi.orders o 
	
ON 

	t1.polygon = o.polygon
	
WHERE

	EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
	AND EXTRACT(MONTH FROM o.effectivedate) = EXTRACT(MONTH FROM current_date)
	AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
	AND o.type = 'cleaning-b2b'
	
GROUP BY

	t1.polygon,
	type_bp,
	t1.hours_082017_b2b,
	t1.hours_092017_b2b,
	t1.hours_102017_b2b,
	t1.hours_112017_b2b,
	t1.hours_122017_b2b,
	t1.hours_012018_b2b,
	t1.hours_022018_b2b,
	t1.hours_032018_b2b,
	t1.hours_042018_b2b,
	t1.hours_052018_b2b,
	t1.hours_062018_b2b,
	t1.hours_072018_b2b,
	t1.hours_082018_b2b,
	t1.hours_092018_b2b,
	t1.hours_102018_b2b,
	t1.hours_112018_b2b,
	t1.hours_122018_b2b;