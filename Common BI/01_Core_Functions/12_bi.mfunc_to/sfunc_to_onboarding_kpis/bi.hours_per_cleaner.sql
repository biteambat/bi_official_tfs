
DROP TABLE IF EXISTS bi.hours_per_cleaner;
CREATE TABLE bi.hours_per_cleaner as	

SELECT

	t1.year,
	t1.month,	
	t1.polygon,
	t1.hours_invoiced as monthly_hours,
	t1.active_professionals,
	CASE WHEN ((t1.hours_invoiced/t1.active_professionals < 47.3) AND (LEFT(t1.polygon, 2) = 'de')) THEN 47.3
	     WHEN ((t1.hours_invoiced/t1.active_professionals < 40) AND LEFT(t1.polygon, 2) = 'ch') THEN 40
	     ELSE t1.hours_invoiced/t1.active_professionals
	     END as hours_per_cleaner

FROM

	(SELECT
	
		EXTRACT(YEAR FROM o.effectivedate) as year,
		EXTRACT(MONTH FROM o.effectivedate) as month,
		o.polygon,
		SUM(o.order_duration__c) as hours_invoiced,
		COUNT(DISTINCT o.professional__c) as active_professionals
		
	FROM
	
		bi.orders o
	
	LEFT JOIN
	
		salesforce.account a
		
	ON o.professional__c = a.sfid
		
	WHERE
	
		o.effectivedate > current_date - 60
		AND o.effectivedate < current_date
		AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'FULFILLED')
		AND o.type NOT LIKE '%training%'
		AND ((a.hr_contract_weekly_hours_min__c >= 11 AND LEFT(a.delivery_areas__c, 2) = 'de') 
		OR (a.hr_contract_weekly_hours_min__c >= 1 AND LEFT(a.delivery_areas__c, 2) = 'ch')
		OR (a.hr_contract_weekly_hours_min__c >= 1 AND LEFT(a.delivery_areas__c, 2) = 'nl')
		OR ((a.hr_contract_weekly_hours_min__c >= 0 OR a.hr_contract_weekly_hours_min__c IS NULL) AND LEFT(a.delivery_areas__c, 2) = 'at'))
		
	GROUP BY
	
		year,
		month,
		o.polygon
		
	ORDER BY
	
		year desc,
		month desc,
		o.polygon) as t1
		
WHERE

	(EXTRACT(MONTH FROM current_date) = 1 AND t1.month = 12)
	OR (EXTRACT(MONTH FROM current_date) > 1 AND t1.month = EXTRACT(MONTH FROM current_date) - 1)
	-- OR t1.month = EXTRACT(MONTH FROM current_date) - 1
		
GROUP BY

	t1.polygon,
	t1.year,
	t1.month,
	t1.hours_invoiced,
	t1.active_professionals,
	t1.hours_invoiced/t1.active_professionals
	
ORDER BY

	t1.year desc,
	t1.month desc,
	t1.polygon;