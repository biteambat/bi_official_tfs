
-- Creation of a small table containing only the sum of the average monthly hours b2b CH

DROP TABLE IF EXISTS bi.hours_tot_b2c_ch;
CREATE TABLE bi.hours_tot_b2c_ch as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2c) as tot_hours_b2c_ch
	

FROM

	bi.hours_invoiced_b2c
	
WHERE

	LEFT(polygon, 2) = 'ch';