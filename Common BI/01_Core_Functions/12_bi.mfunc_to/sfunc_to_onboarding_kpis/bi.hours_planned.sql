
-- Creating the table containing the forecast b2b/b2c for every city by using the 2 previous tables

DROP TABLE IF EXISTS bi.hours_planned;
CREATE TABLE bi.hours_planned as	


SELECT

	o.polygon,
	0 as hours_082017_b2c,
	0 as hours_082017_b2b,
	0 as hours_092017_b2c,
	0 as hours_092017_b2b,
	0 as hours_102017_b2c,
	0 as hours_102017_b2b,
	0 as hours_112017_b2c,
	0 as hours_112017_b2b,
	0 as hours_122017_b2c,
	0 as hours_122017_b2b,
	0 as hours_012018_b2c,
	0 as hours_012018_b2b,
	0 as hours_022018_b2c,
	0 as hours_022018_b2b,
	0 as hours_032018_b2c,
	0 as hours_032018_b2b,
	0 as hours_042018_b2c,
	0 as hours_042018_b2b,
	0 as hours_052018_b2c,
    0 as hours_052018_b2b,
    0 as hours_062018_b2c,
    0 as hours_062018_b2b,
    0 as hours_072018_b2c,
    0 as hours_072018_b2b,
    0 as hours_082018_b2c,
    0 as hours_082018_b2b,
    0 as hours_092018_b2c,
    0 as hours_092018_b2b,
    0 as hours_102018_b2c,
    0 as hours_102018_b2b,
    0 as hours_112018_b2c,
    0 as hours_112018_b2b,
    0 as hours_122018_b2c,
    0 as hours_122018_b2b

	
FROM

	bi.orders o
	
LIMIT 1;

DELETE FROM bi.hours_planned;	


INSERT INTO bi.hours_planned

SELECT

	main.polygon,
	(CASE WHEN forecast_b2c_082017 is null THEN 0 ELSE forecast_b2c_082017 END) as forecast_aug_b2c,
	(CASE WHEN forecast_b2b_082017 is null THEN 0 ELSE forecast_b2b_082017 END) as forecast_aug_b2b,
	(CASE WHEN forecast_b2c_092017 is null THEN 0 ELSE forecast_b2c_092017 END) as forecast_sep_b2c,
	(CASE WHEN forecast_b2b_092017 is null THEN 0 ELSE forecast_b2b_092017 END) as forecast_sep_b2b,
	(CASE WHEN forecast_b2c_102017 is null THEN 0 ELSE forecast_b2c_102017 END) as forecast_oct_b2c,
	(CASE WHEN forecast_b2b_102017 is null THEN 0 ELSE forecast_b2b_102017 END) as forecast_oct_b2b,
	(CASE WHEN forecast_b2c_112017 is null THEN 0 ELSE forecast_b2c_112017 END) as forecast_nov_b2c,
	(CASE WHEN forecast_b2b_112017 is null THEN 0 ELSE forecast_b2b_112017 END) as forecast_nov_b2b,
	(CASE WHEN forecast_b2c_122017 is null THEN 0 ELSE forecast_b2c_122017 END) as forecast_dec_b2c,
	(CASE WHEN forecast_b2b_122017 is null THEN 0 ELSE forecast_b2b_122017 END) as forecast_dec_b2b,
	(CASE WHEN forecast_b2c_012018 is null THEN 0 ELSE forecast_b2c_012018 END) as forecast_jan_b2c,
	(CASE WHEN forecast_b2b_012018 is null THEN 0 ELSE forecast_b2b_012018 END) as forecast_jan_b2b,
	(CASE WHEN forecast_b2c_022018 is null THEN 0 ELSE forecast_b2c_022018 END) as forecast_fev_b2c,
	(CASE WHEN forecast_b2b_022018 is null THEN 0 ELSE forecast_b2b_022018 END) as forecast_fev_b2b,
	(CASE WHEN forecast_b2c_032018 is null THEN 0 ELSE forecast_b2c_032018 END) as forecast_mar_b2c,
	(CASE WHEN forecast_b2b_032018 is null THEN 0 ELSE forecast_b2b_032018 END) as forecast_mar_b2b,
	(CASE WHEN forecast_b2c_042018 is null THEN 0 ELSE forecast_b2c_042018 END) as forecast_apr_b2c,
	(CASE WHEN forecast_b2b_042018 is null THEN 0 ELSE forecast_b2b_042018 END) as forecast_apr_b2b,

	(CASE WHEN forecast_b2c_052018 is null THEN 0 ELSE forecast_b2c_052018 END) as forecast_mai_b2c,
    (CASE WHEN forecast_b2b_052018 is null THEN 0 ELSE forecast_b2b_052018 END) as forecast_mai_b2b,
    (CASE WHEN forecast_b2c_062018 is null THEN 0 ELSE forecast_b2c_062018 END) as forecast_jun_b2c,
    (CASE WHEN forecast_b2b_062018 is null THEN 0 ELSE forecast_b2b_062018 END) as forecast_jun_b2b,
    (CASE WHEN forecast_b2c_072018 is null THEN 0 ELSE forecast_b2c_072018 END) as forecast_jul_b2c,
    (CASE WHEN forecast_b2b_072018 is null THEN 0 ELSE forecast_b2b_072018 END) as forecast_jul_b2b,
    (CASE WHEN forecast_b2c_082018 is null THEN 0 ELSE forecast_b2c_082018 END) as forecast_aug_b2c,
    (CASE WHEN forecast_b2b_082018 is null THEN 0 ELSE forecast_b2b_082018 END) as forecast_aug_b2b,
    (CASE WHEN forecast_b2c_092018 is null THEN 0 ELSE forecast_b2c_092018 END) as forecast_sep_b2c,
    (CASE WHEN forecast_b2b_092018 is null THEN 0 ELSE forecast_b2b_092018 END) as forecast_sep_b2b,
    (CASE WHEN forecast_b2c_102018 is null THEN 0 ELSE forecast_b2c_102018 END) as forecast_oct_b2c,
    (CASE WHEN forecast_b2b_102018 is null THEN 0 ELSE forecast_b2b_102018 END) as forecast_oct_b2b,
    (CASE WHEN forecast_b2c_112018 is null THEN 0 ELSE forecast_b2c_112018 END) as forecast_nov_b2c,
    (CASE WHEN forecast_b2b_112018 is null THEN 0 ELSE forecast_b2b_112018 END) as forecast_nov_b2b,
    (CASE WHEN forecast_b2c_122018 is null THEN 0 ELSE forecast_b2c_122018 END) as forecast_dec_b2c,
    (CASE WHEN forecast_b2b_122018 is null THEN 0 ELSE forecast_b2b_122018 END) as forecast_dec_b2b

		
FROM

	(SELECT
		polygon
	
	FROM
	
		bi.hours_planned__b2b t2

	UNION
	
	SELECT
	
		polygon
		
	FROM
	
		bi.hours_planned__b2c t1) as main
		
	LEFT JOIN

		bi.hours_planned__b2b t2
		
	ON

		main.polygon = t2.polygon

	LEFT JOIN

		bi.hours_planned__b2c t3
	
	ON

		main.polygon = t3.polygon;