
-- Creating the table containing the number of cleaners necessary in every city

DROP TABLE IF EXISTS bi.cleaners_needed;
CREATE TABLE bi.cleaners_needed as		


SELECT

	t1.polygon,
	ROUND((t1.hours_082017_b2c + t1.hours_082017_b2b)/t2.hours_per_cleaner) as cleaners_needed_082017,
	ROUND((t1.hours_092017_b2c + t1.hours_092017_b2b)/t2.hours_per_cleaner) as cleaners_needed_092017,
	ROUND((t1.hours_102017_b2c + t1.hours_102017_b2b)/t2.hours_per_cleaner) as cleaners_needed_102017,
	ROUND((t1.hours_112017_b2c + t1.hours_112017_b2b)/t2.hours_per_cleaner) as cleaners_needed_112017,
	ROUND((t1.hours_122017_b2c + t1.hours_122017_b2b)/t2.hours_per_cleaner) as cleaners_needed_122017,
	ROUND((t1.hours_012018_b2c + t1.hours_012018_b2b)/t2.hours_per_cleaner) as cleaners_needed_012018,
	ROUND((t1.hours_022018_b2c + t1.hours_022018_b2b)/t2.hours_per_cleaner) as cleaners_needed_022018,
	ROUND((t1.hours_032018_b2c + t1.hours_032018_b2b)/t2.hours_per_cleaner) as cleaners_needed_032018,
	ROUND((t1.hours_042018_b2c + t1.hours_042018_b2b)/t2.hours_per_cleaner) as cleaners_needed_042018,

	ROUND((t1.hours_042018_b2c + t1.hours_052018_b2b)/t2.hours_per_cleaner) as cleaners_needed_052018,
    ROUND((t1.hours_042018_b2c + t1.hours_062018_b2b)/t2.hours_per_cleaner) as cleaners_needed_062018,
    ROUND((t1.hours_042018_b2c + t1.hours_072018_b2b)/t2.hours_per_cleaner) as cleaners_needed_072018,
    ROUND((t1.hours_042018_b2c + t1.hours_082018_b2b)/t2.hours_per_cleaner) as cleaners_needed_082018,
    ROUND((t1.hours_042018_b2c + t1.hours_092018_b2b)/t2.hours_per_cleaner) as cleaners_needed_092018,
    ROUND((t1.hours_042018_b2c + t1.hours_102018_b2b)/t2.hours_per_cleaner) as cleaners_needed_102018,
    ROUND((t1.hours_042018_b2c + t1.hours_112018_b2b)/t2.hours_per_cleaner) as cleaners_needed_112018,
    ROUND((t1.hours_042018_b2c + t1.hours_122018_b2b)/t2.hours_per_cleaner) as cleaners_needed_122018


		
FROM

	bi.hours_planned t1
	
JOIN

	bi.hours_per_cleaner t2

ON

	t1.polygon = t2.polygon
	
ORDER BY

	t1.polygon;