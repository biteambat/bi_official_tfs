
-- Creation of a small table containing only the sum of the average monthly hours b2b NL

DROP TABLE IF EXISTS bi.hours_tot_b2b_nl;
CREATE TABLE bi.hours_tot_b2b_nl as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2b) as tot_hours_b2b_nl
	

FROM

	bi.hours_invoiced_b2b
	
WHERE

	LEFT(polygon, 2) = 'nl';