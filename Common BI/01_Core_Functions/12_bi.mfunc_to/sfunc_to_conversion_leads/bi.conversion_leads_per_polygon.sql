
DROP TABLE IF EXISTS bi.conversion_leads_per_polygon;
CREATE TABLE bi.conversion_leads_per_polygon as 


SELECT

	t2.Country as Country,
	t2.polygon,
	CAST(t2.year_touched as integer),
	CAST(t2.month_touched as integer),
	t2.date_touched1,
	
	t2.Total_leads,
	t2.Touched as Touched,
	
	-- (t2.Touched/t2.Total_leads) as Share_touched,
	CASE WHEN t2.Touched > 0 THEN (t2.Reached/t2.Touched) ELSE 0 END as Share_Reached,
	t2.reached,
	CASE WHEN t2.Touched > 0 THEN (t2.Rejected/t2.Touched) ELSE 0 END as Share_Rejected,
	t2.Rejected,
	CASE WHEN t2.Touched > 0 THEN (t2.Documents_before_contract/t2.Touched) ELSE 0 END as Share_Documents_before_contract,
	t2.Documents_before_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_to_be_scheduled/t2.Touched) ELSE 0 END as Share_Interview_to_be_scheduled,
	t2.Interview_to_be_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_scheduled/t2.Touched) ELSE 0 END as Share_Interview_scheduled,
	t2.Interview_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_contract/t2.Touched) ELSE 0 END as Share_Waiting_for_contract,
	t2.Waiting_for_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.After_interview/t2.Touched) ELSE 0 END as Share_After_interview,
	t2.After_interview,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_greenlight/t2.Touched) ELSE 0 END as Share_Waiting_for_greenlight,
	t2.Waiting_for_greenlight,
	CASE WHEN t2.Touched > 0 THEN (t2.Converted/t2.Touched) ELSE 0 END as Share_Converted_touched,
	CASE WHEN t2.Total_leads > 0 THEN (t2.Converted/t2.Total_leads) ELSE 0 END as Share_Converted_total,
	
	t2.Converted as Converted
	
	
FROM


	(SELECT
	
		t1.countrycode as Country,
		t1.polygon,
		-- CAST(t1.year_created as integer),
		-- CAST(t1.month_created as integer),
		-- date_created,
		CAST(t1.year_touched as integer),
		CAST(t1.month_touched as integer),
		date_touched1,
		
		t1.C0::numeric as Total_leads,
		(t1.C1 + t1.C1bis + t1.C2 + t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9 + t1.C10)::numeric as New_leads,
		(t1.C0 - t1.C1 - t1.C1new)::numeric as Touched,
		(t1.C0 - t1.C1 - t1.C1new - t1.C2)::numeric as Reached,
		(t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Documents_before_contract,
		(t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_to_be_scheduled,
		(t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_scheduled,
		(t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Waiting_for_contract,
		(t1.C7 + t1.C8 + t1.C9)::numeric as After_interview,
		(t1.C7 + t1.C9)::numeric as Waiting_for_greenlight,
		
		(t1.C11)::numeric as Converted,
		(t1.C10)::numeric as Rejected
		
	
	FROM
	
		
		(SELECT
		
			a.countrycode,
			a.delivery_area__c as polygon,
			-- EXTRACT(year from a.createddate) as year_created,
			-- EXTRACT(month from a.createddate) as month_created,
			-- MIN(a.createddate::date) date_created,
			EXTRACT(year from leadtouched.date_touched) as year_touched,
			EXTRACT(month from leadtouched.date_touched) as month_touched,
			MIN(leadtouched.date_touched::date) date_touched1,
		
			COUNT(a.createddate) as C0,
		
			SUM(CASE WHEN a.status IN ('New lead','New Leads') THEN 1 ELSE 0 END) as C1,
			SUM(CASE WHEN a.status IN ('QA Call', 'Hold', 'Double Entry', 'Avoid', 'Contact Later') THEN 1 ELSE 0 END) as C1bis,
			SUM(CASE WHEN a.status IN ('Inactive', 'Reactivate') THEN 1 ELSE 0 END) as C1new,
			SUM(CASE WHEN a.status IN ('First contact outstanding', 'Never reached') THEN 1 ELSE 0 END) as C2,
			SUM(CASE WHEN a.status IN ('Documents before contract') THEN 1 ELSE 0 END) as C3,
			SUM(CASE WHEN a.status IN ('Interview to be scheduled') THEN 1 ELSE 0 END) as C4,
			SUM(CASE WHEN a.status IN ('Interview scheduled') THEN 1 ELSE 0 END) as C5,
			SUM(CASE WHEN a.status IN ('Waiting for contract') THEN 1 ELSE 0 END) as C6,
			SUM(CASE WHEN a.status IN ('Waiting for greenlight') THEN 1 ELSE 0 END) as C7,
			SUM(CASE WHEN a.status IN ('Documents after interview', 'Contract ready') THEN 1 ELSE 0 END) as C8,
			SUM(CASE WHEN a.status IN ('Contact/Account created') THEN 1 ELSE 0 END) as C9,
			SUM(CASE WHEN a.status IN ('Rejected') THEN 1 ELSE 0 END) as C10,
			COUNT(a.converteddate) as C11
		
		FROM
		
			salesforce.lead a


		LEFT JOIN

			bi.timelapses_recruitment_process leadtouched

		ON

			a.sfid = leadtouched.leadid
				
				
		WHERE
		
			a.createddate > '2016-06-01'
			AND a.delivery_area__c IS NOT NULL
			AND a.countrycode IN ('DE', 'AT', 'NL', 'CH')
			AND a.delivery_area__c IN ('at-vienna', 'ch-basel', 'ch-bern', 'ch-geneva', 'ch-lausanne', 'ch-lucerne', 'ch-stgallen', 'ch-zurich', 'de-berlin', 'de-bonn', 'de-cologne', 'de-dusseldorf', 'de-essen', 'de-frankfurt', 'de-hamburg', 'de-mainz', 'de-manheim', 'de-munich', 'de-nuremberg', 'de-stuttgart', 'nl-amsterdam', 'nl-hague')
			AND (a.note__c NOT LIKE '%SUBS%' OR a.note__c IS NULL)

				
		GROUP BY
		
			a.countrycode,
			a.delivery_area__c,
			-- year_created,
			-- month_created,
			year_touched,
			month_touched
				
		
		ORDER BY 
		
			a.countrycode asc,
			a.delivery_area__c asc,
			month_touched asc
			
			) t1
			
				) t2;