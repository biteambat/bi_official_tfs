
DROP TABLE IF EXISTS bi.temp_results;
CREATE TABLE bi.temp_results AS

SELECT
     
   a.sfid as sfid,
   a.name as cleaners,
   a.delivery_areas__c as polygons,
   Margin_cleaner.rating as rating,
   Margin_cleaner.global_gpm as global_gpm,
   Margin_cleaner.UR as ur,
          
   ROUND((CASE WHEN (Margin_cleaner.sickness != 0) THEN (1/Margin_cleaner.sickness::numeric) ELSE 2 END), 2) as sickness_coeff,
   Margin_cleaner.worked_hours        
      
FROM salesforce.account a       
      
LEFT JOIN
     
   (SELECT
      
      m.year_month,
      m.professional__c as sfid,
      m.name,
      (CASE WHEN m.rating IS NOT NULL THEN m.rating ELSE 2.5 END) as rating,
      m.delivery_area,
      SUM(m.sickness) as sickness,
      SUM(m.worked_hours) as worked_hours,
      SUM(m.gp) as GP,
      SUM(m.revenue) as revenue,
      CASE WHEN sum(m.revenue) > 0 THEN SUM(m.gp)/sum(m.revenue) ELSE 0 END as global_gpm,
      CASE WHEN LEFT(m.delivery_area, 2) IN ('at') THEN 7 ELSE SUM(m.monthly_hours) END as expected_worked_hours,

      CASE WHEN m.delivery_area = 'at-vienna'
           THEN CASE WHEN MAX(m.weekly_hours) > 0 THEN (SUM(m.worked_hours)/(112*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(112*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(112*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 
                              END)
                     END 
           WHEN m.delivery_area = 'ch-basel'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(43*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(43*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(43*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 
                              END)
                     END
           WHEN m.delivery_area = 'ch-bern'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(54*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(54*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                                THEN (SUM(m.worked_hours)/(54*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                                ELSE 2 
                                END)
                     END
           WHEN m.delivery_area = 'ch-geneva'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 
                              END)
                     END 
           WHEN m.delivery_area = 'ch-lausanne'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(38*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(38*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(38*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 END)
                     END
           WHEN m.delivery_area = 'ch-lucerne'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(65*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(65*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                                THEN (SUM(m.worked_hours)/(65*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                     END
           WHEN m.delivery_area = 'ch-stgallen'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(50*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(50*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                        THEN (SUM(m.worked_hours)/(50*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                        ELSE 2 END)
                 END
           WHEN m.delivery_area = 'ch-zurich'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(64*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(64*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                          THEN (SUM(m.worked_hours)/(64*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                 END
           WHEN m.delivery_area = 'nl-amsterdam'
           THEN CASE WHEN MAX(m.weekly_hours) > 2 THEN (SUM(m.worked_hours)/(76*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(76*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                          THEN (SUM(m.worked_hours)/(76*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                 END
           WHEN m.delivery_area = 'nl-hague'
           THEN CASE WHEN MAX(m.weekly_hours) > 2 THEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                            THEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                            ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-berlin'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-bonn'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-cologne'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                          THEN (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-dusseldorf'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-essen'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-frankfurt'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END) 
                 END
           WHEN m.delivery_area = 'de-hamburg'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-hannover'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-mainz'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-munich'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-nuremberg'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                        THEN (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                        ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-stuttgart'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END) 
                 END
           ELSE 0
           END
               as UR

   FROM bi.margin_per_cleaner m,
        bi.number_days_month as days_in_month

       
   WHERE 
      
      CAST(RIGHT(m.year_month,2) as numeric) = EXTRACT(month from current_date)
      AND CAST(LEFT(m.year_month,4) as numeric) = EXTRACT(year from current_date)
      AND days_in_month.Month::numeric = EXTRACT(MONTH FROM current_date)
       
   GROUP BY 
      
      m.year_month,
      m.professional__c,
      m.name,
      m.rating,
      m.delivery_area) as Margin_cleaner
      
ON
     
   (Margin_cleaner.sfid = a.sfid)
      
WHERE
     
   a.status__c IN ('BETA', 'ACTIVE')
   AND a.test__c = '0'
   AND LEFT(a.locale__c, 2) IN ('de', 'nl', 'ch', 'at')
   AND a.delivery_areas__c IS NOT NULL
   AND (a.hr_contract_start__c <= (current_date - interval '90 days') OR a.hr_contract_start__c IS NULL) 
   AND Margin_cleaner.UR IS NOT NULL
      
GROUP BY
     
   a.sfid,
   a.delivery_areas__c,
   a.name,
   Margin_cleaner.rating,
   Margin_cleaner.global_gpm,
   Margin_cleaner.UR,
   Margin_cleaner.sickness,
   Margin_cleaner.worked_hours
      
ORDER BY 
   
    polygons
    ;