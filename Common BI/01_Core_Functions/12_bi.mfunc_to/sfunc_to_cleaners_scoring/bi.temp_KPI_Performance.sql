
DROP TABLE IF EXISTS bi.temp_KPI_Performance;
CREATE TABLE bi.temp_KPI_Performance AS

SELECT
      
   t1.sfid,
   t1.polygons,
   t1.cleaners,
   t1.rating,
   t1.global_gpm,
   t1.UR,
   t1.sickness_coeff,       
   t3.Terminated_coeff,
   t3.Cancelled_pro_coeff,
   t3.Noshow_professional_coeff
        
FROM bi.temp_results t1

LEFT JOIN bi.temp_cancels t3 ON t1.sfid = t3.sfid

ORDER BY t1.polygons asc 
    
    ;