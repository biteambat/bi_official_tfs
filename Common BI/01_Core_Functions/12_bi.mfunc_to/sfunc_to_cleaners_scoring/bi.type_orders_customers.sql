
DROP TABLE IF EXISTS bi.type_orders_customers;
CREATE TABLE bi.type_orders_customers AS

-- Here I create this table to have the historical split of the orders for each of our customers

SELECT

  DISTINCT o.contact__c as customers,
  SUM(CASE WHEN o.status IN ('INVOICED') THEN 1 ELSE 0 END) as number_invoiced,
  SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as number_terminated,
  SUM(CASE WHEN o.status IN ('CANCELLED NO MANPOWER') THEN 1 ELSE 0 END) as number_nmp,
  SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as number_cancelled_pro,
  SUM(CASE WHEN o.status NOT IN ('INVOICED', 'CANCELLED TERMINATED', 'CANCELLED NO MANPOWER', 'CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as number_others,
  COUNT(DISTINCT o.sfid) - SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as all_orders_wo_terminated, 
  COUNT(DISTINCT o.sfid) as tot_orders  
  
FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.contact__c IS NOT NULL
  
GROUP BY

  o.contact__c
  
ORDER BY 

  o.contact__c,
  number_invoiced desc
  ;