
DROP TABLE IF EXISTS bi.churn_trigger;
CREATE TABLE bi.churn_trigger AS  

SELECT

  a.sfid,
  table4.*

FROM

  salesforce.account a
  
LEFT JOIN

  (SELECT
  
    table3.*,
    CASE WHEN table3.ratio_churn < 0.05 THEN 5
        WHEN 0.05 <= table3.ratio_churn AND table3.ratio_churn < 0.1 THEN 4
        WHEN 0.1 <= table3.ratio_churn AND table3.ratio_churn < 0.3 THEN 3
        WHEN 0.3 <= table3.ratio_churn AND table3.ratio_churn < 0.5 THEN 2
        WHEN table3.ratio_churn >= 0.5 THEN 1
        ELSE 0
        END as power_trigger_churn
        
  FROM
  
    (SELECT
    
      table1.professional__c,
      table1.duration,
      table1.number_customers as number_customers_invoiced,
      table2.number_churns,
      CASE WHEN table2.number_churns IS NULL THEN 0
          WHEN (table2.number_churns/table1.number_customers::numeric)::numeric <= 1 THEN (table2.number_churns/table1.number_customers::numeric)::numeric 
          ELSE 1
          END as ratio_churn
    
    
    FROM
    
      (SELECT

        -- Here we have all the active cleaners of the last 90 days with the average duration of the orders and the number of customers they had
      
        o.professional__c,
        AVG(o.order_duration__c) as duration,
        COUNT(DISTINCT o.contact__c) as number_customers
        
      FROM  
      
        bi.orders o
        
      WHERE
      
        o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
        AND o.effectivedate::date < current_date
         AND o.effectivedate::date >= (current_date - interval '90 days')
        
      GROUP BY
      
        o.professional__c
        
      ORDER BY
      
        o.professional__c,
        number_customers desc) as table1
    
    LEFT JOIN
      
      (SELECT
      
        t1.last_cleaner,
        COUNT(DISTINCT t1.customer_id) as number_churns
        
      FROM
      
        bi.list_churned_customers t1
        
        
      WHERE 
      
        t1.last_cleaner IS NOT NULL
        
      GROUP BY
      
        t1.last_cleaner) as table2
        
    ON 
    
      table1.professional__c = table2.last_cleaner) as table3) as table4

ON 

  a.sfid = table4.professional__c
  
WHERE

  a.status__c IN ('ACTIVE', 'BETA')
  AND table4.professional__c IS NOT NULL;