
DROP TABLE IF EXISTS bi.temp_KPI_Performance2;
CREATE TABLE bi.temp_KPI_Performance2 AS

SELECT
      
   t4.sfid,
   t4.polygons,
   t4.cleaners,
   t4.rating,
   t4.global_gpm,
   t4.UR,
      
   -- t4.Terminated_coeff as Terminated,
   (CASE WHEN (t4.Terminated_coeff = 2) 
        THEN 5 
      ELSE (CASE WHEN ((0.33 <= t4.Terminated_coeff) AND (t4.Terminated_coeff <= 1)) 
                 THEN 4 
              ELSE (CASE WHEN (((0.25) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.33))) 
                         THEN 3 
                     ELSE (CASE WHEN (((0.2) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.25))) 
                                THEN 2 
                            ELSE (CASE WHEN (t4.Terminated_coeff < (0.2)) 
                                       THEN 1 
                                    ELSE NULL 
                                    END) END) END) END) END) as Coeff_terminated, 

   -- t4.Cancelled_pro_coeff as CancelledPro,
  (CASE WHEN ((1 <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff <= 2)) 
        THEN 5 
      ELSE (CASE WHEN ((0.33 <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < 1)) 
                 THEN 4 
              ELSE (CASE WHEN (((0.2) <= t4.Cancelled_pro_coeff AND (t4.Cancelled_pro_coeff < 0.33))) 
                         THEN 3 
                     ELSE (CASE WHEN (((0.125) <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < (0.2))) 
                                THEN 2 
                            ELSE (CASE WHEN (t4.Cancelled_pro_coeff < (0.125)) 
                                       THEN 1 
                                    ELSE NULL 
                                    END) END) END) END) END) as Coeff_cancelled_pro,

   -- t4.Noshow_professional_coeff as Noshow,
   (CASE WHEN (t4.Noshow_professional_coeff = 2) 
        THEN 5 
      ELSE (CASE WHEN ((0.5 <= t4.Noshow_professional_coeff) AND (t4.Noshow_professional_coeff <= 1)) 
                 THEN 4 
              ELSE (CASE WHEN ((0.33) <= t4.Noshow_professional_coeff) AND (t4.Noshow_professional_coeff < 0.5) 
                         THEN 3 
                     ELSE (CASE WHEN ((0.2) <= t4.Noshow_professional_coeff ) AND (t4.Noshow_professional_coeff  < (0.33))  
                                THEN 2 
                            ELSE (CASE WHEN t4.Noshow_professional_coeff < (0.2) 
                                       THEN 1 
                                    ELSE NULL END) END) END) END) END) as Coeff_noshow_professional,
       
   -- t4.sickness_coeff as Sickness,
   (CASE WHEN ((1 <= t4.sickness_coeff) AND (t4.sickness_coeff <= 2)) 
        THEN 5 
      ELSE (CASE WHEN (((0.33) <= t4.sickness_coeff) AND (t4.sickness_coeff < 1)) 
                 THEN 4 
              ELSE (CASE WHEN (((0.2) <= t4.sickness_coeff) AND (t4.sickness_coeff < (0.33))) 
                         THEN 3 
                     ELSE (CASE WHEN ((0.125 <= t4.sickness_coeff) AND (t4.sickness_coeff < 0.2))  
                                THEN 2 
                            ELSE (CASE WHEN (t4.sickness_coeff < 0.125) 
                                       THEN 1 
                                    ELSE NULL END) END) END) END) END) as Coeff_sickness             
        
FROM bi.temp_KPI_Performance t4

ORDER BY t4.polygons asc 
    ;