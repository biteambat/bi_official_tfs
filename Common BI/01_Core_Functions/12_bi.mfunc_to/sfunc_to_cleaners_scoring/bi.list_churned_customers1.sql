
DROP TABLE IF EXISTS bi.list_churned_customers1;
CREATE TABLE bi.list_churned_customers1 AS

-- I merged the 2 last tables to have the list of churned customers with their last INVOICED order and last cleaner before churn

SELECT

  table1.*,
  oo.sfid as sfid_last_invoiced_orders

FROM

  (SELECT
  
    o.date_churn,
    o.event_name,
    o.ordertype,
    o.customer_id,
    i.date_last_invoiced,
    i.polygon,
    i.professional__c as last_cleaner
    
  FROM
  
    bi.churned_customers1 o
    
  LEFT JOIN
  
    bi.last_invoiced i
      
  ON
  
    o.customer_id = i.contact__c    
    
  ORDER BY 
  
    date_churn desc) table1
    
LEFT JOIN

  bi.orders oo
  
ON

  table1.customer_id = oo.contact__c
  AND table1.date_last_invoiced = oo.effectivedate
  AND table1.polygon = oo.polygon
  AND table1.last_cleaner = oo.professional__c
  AND table1.ordertype = oo.type
  
ORDER BY

  date_churn desc,
  customer_id
  ;