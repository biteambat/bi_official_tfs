
DROP TABLE IF EXISTS bi.list_churned_customers;
CREATE TABLE bi.list_churned_customers AS 

-- To obtain the final version of the churned customers' table I link it to the historical split of the orders

SELECT

  t1.*,
  t2.number_invoiced,
  t2.number_terminated,
  t2.number_nmp,
  t2.number_cancelled_pro,
  t2.number_others,
  t2.all_orders_wo_terminated,
  t2.tot_orders
  
FROM

  bi.list_churned_customers1 t1
  
LEFT JOIN

  bi.type_orders_customers t2
  
ON 

  t1.customer_id = t2.customers;