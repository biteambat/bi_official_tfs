
DROP TABLE IF EXISTS bi.rating_weekdays;
CREATE TABLE bi.rating_weekdays as

SELECT 

  EXTRACT(YEAR FROM o.effectivedate) as year,
  EXTRACT(MONTH FROM o.effectivedate) as month, 
  TO_CHAR(o.effectivedate, 'day')::text as day_of_week,
  o.effectivedate,
  o.rating_professional__c, 
  o.polygon as city,
  UPPER(LEFT(o.polygon, 2)) as country
  
FROM
  
  bi.orders o
  
JOIN

  salesforce.account acc
  
ON o.professional__c = acc.sfid 
  
WHERE 
  
  o.test__c = '0'
  AND o.status IN ('INVOICED','FULFILLED')
  AND o.effectivedate > '2015-12-31'
  AND o.polygon IS NOT NULL
  
ORDER BY 
  
  year,
  month,
  effectivedate, 
  city;