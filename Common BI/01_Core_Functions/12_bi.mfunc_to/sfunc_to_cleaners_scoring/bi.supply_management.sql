
DROP TABLE IF EXISTS bi.supply_management;
CREATE TABLE bi.supply_management as

SELECT

  UPPER(LEFT(p.locale__c, 2)) as country,
  p.servicedate__c as service_date,
  p.status__c as status_order,
  p.name as type_product,
  p.quantity__c as quantity,
  p.total_amount__c as total_amount
  


FROM

  salesforce.productlineitem__c p
  
ORDER BY

  p.servicedate__c;