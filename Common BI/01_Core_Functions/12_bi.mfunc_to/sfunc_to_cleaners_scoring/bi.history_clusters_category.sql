
DELETE FROM bi.history_clusters_category WHERE mindate = current_date::date;
INSERT INTO bi.history_clusters_category 

SELECT

  EXTRACT(YEAR FROM date_today) as year,
  EXTRACT(WEEK FROM date_today) as week,
  date_today as mindate,
  polygons,
  AVG(coeff_cancelled_pro) as avg_coeff_cancelled_pro,
  AVG(coeff_noshow_professional) as avg_coeff_noshow_pro,
  AVG(coeff_sickness) as avg_coeff_sickness,
  AVG(coeff_power_churn) as avg_power_churn,
  AVG(score_cleaners) as avg_score_cleaners,
  AVG(rating) as avg_ratings,
  AVG(ur) as avg_ur,
  SUM(CASE WHEN category = 'A' THEN 1 ELSE 0 END) as number_a,
  SUM(CASE WHEN category = 'A' THEN 1 ELSE 0 END)/COUNT(category)::numeric as share_a,
  SUM(CASE WHEN category = 'B' THEN 1 ELSE 0 END) as number_b,
  SUM(CASE WHEN category = 'B' THEN 1 ELSE 0 END)/COUNT(category)::numeric as share_b,
  SUM(CASE WHEN category = 'C' THEN 1 ELSE 0 END) as number_c,
  SUM(CASE WHEN category = 'C' THEN 1 ELSE 0 END)/COUNT(category)::numeric as share_c
  
FROM 

  bi.History_performance_cleaners h
  
WHERE

  -- polygons = 'de-berlin'
  date_today = current_date
  
GROUP BY

  year,
  week,
  date_today,
  polygons
  
ORDER BY 

  year,
  week,
  date_today,
  polygons;