
DROP TABLE IF EXISTS bi.number_days_month;
CREATE TABLE bi.number_days_month AS

SELECT

    TO_CHAR(date,'MM') as Month,
    min(date::date) as Mindate,
    COUNT(DISTINCT(date)) as days_per_month

FROM

(select i::date as date from generate_series('2017-01-01','2018-12-31', '1 day'::interval) i) as a 

GROUP BY
    Month;