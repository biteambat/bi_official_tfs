
DROP TABLE IF EXISTS bi.churned_customers1;
CREATE TABLE bi.churned_customers1 AS

-- Here I take the table of the cancelled terminated events and merge it with bi.orders to see if the customers still have orders in the future or not
--  Then I take the date of the first cancelled terminated and consider it as the date of churn
-- It gives me the list of the customers with their date of churn

SELECT
  
  MIN(table3.date_churn) as date_churn,
  table3.event_name,
  table3.ordertype,
  table3.customer_id

FROM  
  
  (SELECT
  
    table2.date_churn,
    table2.event_name,
    table2.locale__c,
    table2.customer_id,
    table2.recurrency,
    table2.ordertype,
    o.polygon
  
  FROM
  
    
    (SELECT
    
      t3.*,
      table1.sfid_order_future
    
    FROM
    
      bi.temp_orders_terminated t3
      
    LEFT JOIN
    
      (SELECT
      
        t1.date_churn as date_churn,
        t1.customer_id,
        t1.order_startdate as start_old_order,
        t1.event_name as status_old_order,
        t1.recurrency as recurrency_plan,
        t1.ordertype,
        t1.old_professional as sfid_old_professional,
        t2.sfid as sfid_order_future,
        t2.effectivedate as date_order_future,
        t2.status as status_order_future,
        t2.professional__c as future_professional
      
      FROM
      
        bi.temp_orders_terminated t1
      
      LEFT JOIN
      
        bi.orders t2 
        
      ON 
      
        t1.customer_id = t2.contact__c
      
      WHERE 
      
        t2.error_note__c NOT LIKE '%Customer blocked%'
        AND t2.status IN ('PENDING TO START')
        AND t2.effectivedate >= current_date
        -- AND t1.professional IS NOT NULL
      
      GROUP BY 
      
        t1.date_churn,
        t1.customer_id,
        status_old_order,
        t1.order_startdate,
        t1.recurrency,
        t1.old_professional,
        t1.ordertype,
        t2.sfid,
        t2.effectivedate,
        t2.status,
        t2.professional__c
      
      ORDER BY 
      
        customer_id desc,
        start_old_order desc,
        date_order_future desc) table1
        
    ON 
    
      t3.customer_id = table1.customer_id
      
    WHERE 
    
      table1.sfid_order_future IS NULL
      
    ORDER BY
    
      t3.customer_id,
      t3.order_startdate desc) table2
      
  LEFT JOIN
  
    bi.orders o
    
  ON
  
    table2.customer_id = o.contact__c
    
  WHERE 
  
    o.polygon IS NOT NULL
    
  GROUP BY
  
    table2.date_churn,
    table2.event_name,
    table2.locale__c,
    table2.customer_id,
    table2.recurrency,
    table2.ordertype,
    o.polygon
    
  ORDER BY
  
    table2.date_churn desc,
    table2.event_name,
    table2.locale__c desc,
    o.polygon desc,
    table2.customer_id,
    table2.recurrency,
    table2.ordertype) table3
    
WHERE

  table3.ordertype IN ('cleaning-b2c', 'cleaning-b2b')
    
GROUP BY

  table3.event_name,
  table3.ordertype,
  table3.customer_id
  
ORDER BY

  date_churn,
  table3.customer_id
  ;