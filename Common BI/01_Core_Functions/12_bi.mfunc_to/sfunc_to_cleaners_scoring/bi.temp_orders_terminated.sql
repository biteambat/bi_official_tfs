
DROP TABLE IF EXISTS bi.temp_orders_terminated;
CREATE TABLE bi.temp_orders_terminated AS

-- Here I take all the cancelled terminated events that we had in the past

SELECT
  
  created_at::date as date_churn,
  event_name,
  order_Json->>'EffectiveDate' as order_startdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id,
  order_Json->>'Recurrency__c' as recurrency,
  order_Json->>'Type' as ordertype,
  order_Json->>'Professional__c' as old_professional

FROM

  events.sodium

WHERE

  (event_name in ('Order Event:CANCELLED TERMINATED'))
  AND order_Json->>'Recurrency__c' > '0'
  AND (order_Json->>'EffectiveDate')::date < current_date
  
GROUP BY

  created_at,
  event_name,
  order_Json->>'Locale__c',
   order_Json->>'Order_Id__c',
   order_Json->>'Contact__c',
   order_Json->>'Recurrency__c',
   order_Json->>'Type',
   order_Json->>'Professional__c',
   order_Json->>'EffectiveDate'
  
ORDER BY

  order_Json->>'EffectiveDate' desc,
  order_Json->>'Contact__c' 
  ;