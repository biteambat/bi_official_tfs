
DROP TABLE IF EXISTS bi.cleaners_profitability;
CREATE TABLE bi.cleaners_profitability as

SELECT 	t2.*,
	ROUND(AVG(gpm_traffic_light) OVER (PARTITION BY professional__c), 0) AS gpm_traffic_light_avg_round0,
	ROUND(AVG(gpm_traffic_light) OVER (PARTITION BY professional__c), 2) AS gpm_traffic_light_avg_round2,
	CASE
		WHEN t2.contract_end <= now()::date THEN 'Left'
		ELSE 'Still Working'
	END AS current_status,
	SUM(CASE
		WHEN t2.year_month::date = (to_date(to_char(now(), 'YYYY-MM'), 'YYYY-MM') - interval '1 month')::date
		THEN t2.gpm_traffic_light ELSE NULL END)
		OVER (PARTITION BY professional__c)
	AS latest_traffic_light
FROM
(
	SELECT 	t1.*,
		CASE
			WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 2) OR (time_on_platform <= 3 AND gpm_less_than_10_count = 3) THEN 1
			WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 1) THEN 2
			WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 0) THEN 3
			ElSE 0
		END AS rule_1_tl_system,
		CASE
			WHEN (time_on_platform > 3 AND gpm_less_than_10_count >= 3)THEN 1
			WHEN (time_on_platform > 3 AND gpm_less_than_10_count = 2) THEN 2
			WHEN (time_on_platform > 3 AND gpm_less_than_10_count < 2) THEN 3
			ElSE 0
		END AS rule_2_tl_system,
		CASE 
			WHEN 
				(CASE
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 2) OR (time_on_platform <= 3 AND gpm_less_than_10_count = 3) THEN 1
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 1) THEN 2
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 0) THEN 3
					ElSE 0
				END) > 0 
				THEN 
				(CASE
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 2) OR (time_on_platform <= 3 AND gpm_less_than_10_count = 3) THEN 1
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 1) THEN 2
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 0) THEN 3
					ElSE 0
				END)
			ELSE 	
				(CASE
					WHEN (time_on_platform > 3 AND gpm_less_than_10_count >= 3)THEN 1
					WHEN (time_on_platform > 3 AND gpm_less_than_10_count = 2) THEN 2
					WHEN (time_on_platform > 3 AND gpm_less_than_10_count < 2) THEN 3
					ElSE 0
				END)
		END AS gpm_traffic_light
	FROM
	(
		SELECT 	t.*,
				CASE
					WHEN gross_profit_abs = 0 OR revenue = 0 THEN 0
					ELSE gross_profit_abs/revenue 
				END AS gpm_percent,
				CASE
					WHEN 
						(CASE
							WHEN gross_profit_abs = 0 OR revenue = 0 THEN 0
							ELSE gross_profit_abs/revenue 
						END) < 0.10 THEN 1 ELSE 0
				END AS gpm_less_than_10_perc,
				
				SUM(CASE
					WHEN 
						(CASE
							WHEN gross_profit_abs = 0 OR revenue = 0 THEN 0
							ELSE gross_profit_abs/revenue 
						END) < 0.10 THEN 1 ELSE 0
				END) OVER (PARTITION BY professional__c ORDER BY year_month) AS gpm_less_than_10_count
		FROM 
		(
		SELECT	mpc.name,
					mpc.type__c,
					to_date(mpc.year_month, 'YYYY-MM-DD') as year_month,
					mpc.mindate,
					mpc.professional__c,
					emp_id.emp_id,
					mpc.delivery_area,
					mpc.worked_hours,
					mpc.contract_start,
					to_date(to_char(mpc.contract_start, 'YYYY-MM'), 'YYYY-MM') as contract_start_month,
					mpc.contract_end,
					to_date(to_char(mpc.contract_end, 'YYYY-MM'), 'YYYY-MM') as contract_end_month,
					mpc.gp,
					mpc.revenue,
					mpc.salary_payed,
					CASE
						WHEN mpc.delivery_area = 'at-vienna' THEN mpc.gp ELSE (mpc.revenue - mpc.salary_payed)
					END AS gross_profit_abs,
					((to_date(mpc.year_month, 'YYYY-MM-DD') - to_date(to_char(mpc.contract_start, 'YYYY-MM'), 'YYYY-MM'))/30)+1 AS time_on_platform
					
		FROM bi.margin_per_cleaner mpc
		LEFT JOIN
				LATERAL	(
						SELECT  hr_employee_number__c AS emp_id
						FROM salesforce.account a
						WHERE a.sfid = mpc.professional__c
							-- AND hr_employee_number__c is not null
				) emp_id
		ON TRUE
			WHERE to_date(mpc.year_month, 'YYYY-MM-DD') >= now()::date - interval '7 months'
		) t
	) t1
) t2;