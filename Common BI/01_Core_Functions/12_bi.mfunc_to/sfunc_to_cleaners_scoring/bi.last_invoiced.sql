
DROP TABLE IF EXISTS bi.last_invoiced;
CREATE TABLE bi.last_invoiced AS

-- This table display the list of our customers with their last registerd INVOICED order as well as their last cleaner

SELECT
  
  table4.*,
  oo.type,
  oo.professional__c

FROM

  (SELECT
  
    MAX(o.effectivedate) date_last_invoiced,
    o.contact__c,
    o.polygon
    
  FROM
  
    bi.orders o
    
  WHERE
  
    o.status IN ('INVOICED')
    AND o.effectivedate > '2015-12-31'
    AND o.polygon IS NOT NULL
    AND o.contact__c IS NOT NULL 
    AND o.professional__c IS NOT NULL
    AND o.type IN ('cleaning-b2b', 'cleaning-b2c')
    
  GROUP BY
  
    o.contact__c,
    o.polygon
  
  ORDER BY
  
    date_last_invoiced desc,
    o.contact__c,
    o.polygon) table4
    
LEFT JOIN

  bi.orders oo
  
ON 

  table4.contact__c = oo.contact__c
  AND table4.date_last_invoiced = oo.effectivedate
  AND table4.polygon = oo.polygon
  
WHERE

  oo.polygon IS NOT NULL
  AND oo.contact__c IS NOT NULL 
  AND oo.professional__c IS NOT NULL
  AND oo.type IN ('cleaning-b2b', 'cleaning-b2c')
  
GROUP BY

  table4.date_last_invoiced,
  table4.contact__c,
  table4.polygon,
  oo.type,
  oo.professional__c
  
ORDER BY

  table4.date_last_invoiced desc,
  table4.contact__c,
  table4.polygon,
  oo.type,
  oo.professional__c
  ;