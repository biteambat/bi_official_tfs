
DROP TABLE IF EXISTS bi.temp_cancels;
CREATE TABLE bi.temp_cancels AS

SELECT
      
   t1.Professional as sfid,
   ROUND((CASE WHEN (t1.Terminated = 0) THEN 2 ELSE (1/t1.Terminated::numeric) END), 2) as Terminated_coeff,
   ROUND((CASE WHEN (t1.Cancelled_professional = 0) THEN 2 ELSE (1/t1.Cancelled_professional::numeric) END), 2) as Cancelled_pro_coeff,
   ROUND((CASE WHEN (t1.Noshow_professional = 0) THEN 2 ELSE (1/t1.Noshow_professional::numeric) END), 2) as Noshow_professional_coeff
      
FROM      
      
   (SELECT
       
      o.professional__c as Professional,
      SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as Terminated,
      SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as Cancelled_professional,
      SUM(CASE WHEN o.status IN ('NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as Noshow_professional
         
   FROM bi.orders o 
        
   WHERE
       
      o.status IN ('INVOICED', 'CANCELLED TERMINATED', 'CANCELLED PROFESSIONAL', 'NOSHOW PROFESSIONAL')
      AND o.test__c = '0'
      AND o.effectivedate >= (current_date - 90)
      AND LEFT(o.locale__c, 2) IN ('de', 'nl', 'ch', 'at') 
        
   GROUP BY o.professional__c) as t1;