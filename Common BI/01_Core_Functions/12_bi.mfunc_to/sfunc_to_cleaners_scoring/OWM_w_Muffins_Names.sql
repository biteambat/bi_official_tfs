
DROP TABLE IF EXISTS OWM_w_Muffins_Names;
CREATE TABLE OWM_w_Muffins_Names as

SELECT 

  o.id as "orderID",
  acc.status__c,
  acc.training_01__c,
  acc.training_02__c,
  o.status, 
  o.effectivedate,
  o.rating_professional__c, 
  o.polygon as city, 
  o.professional__c,
  acc.name,
  acc.email__c,
  acc.hr_contract_start__c::timestamp::date
  
FROM

  bi.orders o

JOIN
  salesforce.account acc
    ON o.professional__c = acc.sfid 

WHERE 
  
  o.test__c = '0'
  AND o.status IN ('INVOICED','CANCELLED PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM','NOSHOW PROFESSIONAL', 'FULFILLED')
  AND o.effectivedate > '2015-12-31'

ORDER BY 

  acc.name asc,
  o.effectivedate desc;