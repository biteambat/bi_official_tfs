
DROP TABLE IF EXISTS bi.Performance_cleaners;
CREATE TABLE bi.Performance_cleaners AS

SELECT
     
   current_date::date as date_today,      
   t5.sfid,
   t5.polygons,
   t5.cleaners,
   t5.UR,
   t5.rating,
   -- t5.Terminated,
   -- t5.Coeff_terminated,
   -- t5.CancelledPro,
   t5.Coeff_cancelled_pro,
   -- t5.Noshow, 
   t5.Coeff_noshow_professional,
   -- t5.Sickness,
   t5.Coeff_sickness,
   t.power_trigger_churn as Coeff_power_churn,
    
   (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(4 as decimal)::numeric) as decimal), 1) END) as Score_cleaners,
   
   CASE WHEN (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) > 4 THEN 'A' -- Here we define how the letter is assigned to the cleaners according to there score
        WHEN (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) > 3.5 AND (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) <= 4 THEN 'B'
        WHEN (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) <= 3.5 THEN 'C'
        ELSE 'ERROR'
   END as category,
   
   CASE WHEN a.hr_contract_end__c > current_date 
        THEN 
            CASE WHEN date_part('month', age(current_date, a.hr_contract_start__c)) IS NULL 
            THEN 0 
            ELSE date_part('month', age(current_date, a.hr_contract_start__c)) 
            END
        ELSE 
            CASE WHEN date_part('month', age(a.hr_contract_end__c, a.hr_contract_start__c)) IS NULL 
            THEN 0 
            ELSE date_part('month', age(a.hr_contract_end__c, a.hr_contract_start__c)) 
            END
        END
        as existence
 
        
FROM bi.temp_KPI_Performance2 t5

LEFT JOIN

  bi.churn_trigger t
  
ON

  (t5.sfid = t.sfid)

LEFT JOIN

   Salesforce.account a
   
ON 

   (t5.sfid = a.sfid)

ORDER BY t5.polygons asc;