
DROP TABLE IF EXISTS bi.Planning_cleaners;
CREATE TABLE bi.Planning_cleaners AS


(SELECT

	t1.Mindate as Mindate,
    t1.week,
	t1.sfid,
	t1.name,
	t1.delivery_areas as polygons,
	t1.HoursExpected1Week as hours_expected,
	t1.HoursPlanned as hours_planned,
	t1.Workload	as workload
   
FROM	
		
	(SELECT 
	
		OrdersList.mindate as Mindate,
		OrdersList.Week,
		a.sfid as sfid,
		a.parentid as parentid,
		a.name as name,
		a.hr_contract_start__c as contract_start,
		a.delivery_areas__c as delivery_areas,
		(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) as HoursExpected1Week,
		(CASE WHEN SUM(OrdersList.Hours) > (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) THEN (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) ELSE SUM(OrdersList.Hours) END) as HoursPlanned,
		
		CASE WHEN (SUM(OrdersList.Hours) IS NULL) THEN 0
			ELSE
			(CASE WHEN SUM(OrdersList.Hours) > (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) THEN (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) ELSE SUM(OrdersList.Hours) END)/(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END)::numeric 
			END as Workload	
		
	FROM
	
		Salesforce.account a 
			
	LEFT JOIN
	
		(SELECT 
		
			min(o.effectivedate) as mindate,
			EXTRACT(WEEK FROM o.effectivedate) as Week,
			o.professional__c,
			o.polygon,
			SUM(o.order_duration__c) as Hours		
			
		FROM
			
			bi.orders o
				
		WHERE
			
			o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
			AND o.effectivedate > '2016-12-31'
			
			
		GROUP BY 	
		
			o.professional__c,
			o.polygon,
			week) as OrdersList
					
	ON 
	
		(a.sfid = OrdersList.professional__c)
		
	WHERE
	
	   a.test__c = '0'
	   AND LEFT(a.delivery_areas__c, 2) IN ('de', 'nl', 'ch')
	   AND a.delivery_areas__c IS NOT NULL
	   AND a.status__c IN ('BETA', 'ACTIVE')
	   AND a.name NOT LIKE '%test%'
	   AND a.name NOT LIKE '%TEST%'
	   AND a.name NOT LIKE '%Test%'
		
	GROUP BY
	
		OrdersList.Week,
		Orderslist.mindate,
		a.sfid,
		a.parentid,
		a.name,
		a.delivery_areas__c,
		a.hr_contract_start__c,
		a.hr_contract_weekly_hours_min__c
		
	ORDER BY
	
		a.delivery_areas__c asc, a.name asc) as t1
		
JOIN

  	 Salesforce.Account t2
   
ON

   	(t2.sfid = t1.parentid)

WHERE 

	(t2.name LIKE '%BAT%' OR t2.name LIKE '%BOOK%')
	
GROUP BY

	t1.Mindate,
	t1.Week,
	t1.sfid,
	t1.name,
	t1.delivery_areas,
	t1.HoursExpected1Week,
	t1.HoursPlanned,
	t1.Workload	
	
ORDER BY

	t1.name,
	t1.HoursExpected1Week,
	t1.HoursPlanned,
	t1.Workload)
	
	
UNION


(SELECT

	MIN(t2.Mindate1) as Mindate,
   t2.week,
	'-' as sfid,
	'-' as name,
	t2.Polygons as polygons,
	SUM(t2.HoursExpected1Week) as hours_expected,
	SUM(t2.HoursPlanned) as hours_planned,
	SUM(t2.HoursPlanned)/SUM(t2.HoursExpected1Week)::numeric as workload

FROM

	(SELECT
	
		MIN(t1.Mindate) as Mindate1,
	   t1.week,
		t1.sfid,
		t1.name,
		t1.delivery_areas as Polygons,
		t1.HoursExpected1Week,
		t1.HoursPlanned,
		t1.Workload	
	   
	FROM	
			
		(SELECT 
		
			OrdersList.mindate as Mindate,
			OrdersList.Week,
			a.sfid as sfid,
			a.parentid as parentid,
			a.name as name,
			a.hr_contract_start__c as contract_start,
			a.delivery_areas__c as delivery_areas,
			(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) as HoursExpected1Week,
			(CASE WHEN SUM(OrdersList.Hours) > (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) THEN (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) ELSE SUM(OrdersList.Hours) END) as HoursPlanned,
			(CASE WHEN SUM(OrdersList.Hours) > (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) THEN (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) ELSE SUM(OrdersList.Hours) END)/(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END)::numeric as Workload	
		
			
		FROM
		
			Salesforce.account a 
				
		LEFT JOIN
		
			(SELECT 
			
				MIN(o.effectivedate) as mindate,
				EXTRACT(WEEK FROM o.effectivedate) as Week,
				o.professional__c,
				o.polygon,
				SUM(o.order_duration__c) as Hours		
				
			FROM
				
				bi.orders o
					
			WHERE
				
				o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
				AND o.effectivedate > '2016-12-31'
				
				
			GROUP BY 	
			
				o.professional__c,
				o.polygon,
				week) as OrdersList
						
		ON 
		
			(a.sfid = OrdersList.professional__c)
			
		WHERE
		
		 	a.test__c = '0'
		  	AND LEFT(a.delivery_areas__c, 2) IN ('de', 'nl', 'ch')
		   AND a.delivery_areas__c IS NOT NULL
		   AND a.status__c IN ('BETA', 'ACTIVE')
		   AND a.name NOT LIKE '%test%'
		   AND a.name NOT LIKE '%TEST%'
		   AND a.name NOT LIKE '%Test%'
			
		GROUP BY
		
			OrdersList.Week,
			Orderslist.mindate,
			a.sfid,
			a.parentid,
			a.name,
			a.delivery_areas__c,
			a.hr_contract_start__c,
			a.hr_contract_weekly_hours_min__c
			
		ORDER BY
		
			a.delivery_areas__c asc, a.name asc) as t1
			
	JOIN
	
	  	 Salesforce.Account t2
	   
	ON
	
	   	(t2.sfid = t1.parentid)
	
	WHERE 
	
		(t2.name LIKE '%BAT%' OR t2.name LIKE '%BOOK%')
		
	GROUP BY
	
		t1.Mindate,
		t1.Week,
		t1.sfid,
		t1.name,
		t1.delivery_areas,
		t1.HoursExpected1Week,
		t1.HoursPlanned,
		t1.Workload	
		
	ORDER BY
	
		t1.name,
		t1.HoursExpected1Week,
		t1.HoursPlanned,
		t1.Workload) as t2

GROUP BY

   week,
	Polygons
	
ORDER BY

	week desc, polygons asc)
	
	
UNION


(SELECT

	MIN(t2.Mindate1) as Mindate,
   t2.week,
   '-' as sfid,
	'-' as name,
	UPPER(LEFT(t2.Polygon1, 2)) as polygons,
	SUM(t2.HoursExpected1Week) as SumHoursExpected1Week,
	SUM(t2.HoursPlanned) as SumHoursPlanned,
	SUM(t2.HoursPlanned)/SUM(t2.HoursExpected1Week)::numeric as workload_polygon

FROM

	(SELECT
	
		MIN(t1.Mindate) as Mindate1,
	   t1.week,
		t1.sfid,
		t1.name,
		t1.delivery_areas as Polygon1,
		t1.HoursExpected1Week,
		t1.HoursPlanned,
		t1.Workload	
	   
	FROM	
			
		(SELECT 
		
			OrdersList.mindate as Mindate,
			OrdersList.Week,
			a.sfid as sfid,
			a.parentid as parentid,
			a.name as name,
			a.hr_contract_start__c as contract_start,
			a.delivery_areas__c as delivery_areas,
			(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) as HoursExpected1Week,
			(CASE WHEN SUM(OrdersList.Hours) > (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) THEN (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) ELSE SUM(OrdersList.Hours) END) as HoursPlanned,
			(CASE WHEN SUM(OrdersList.Hours) > (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) THEN (CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END) ELSE SUM(OrdersList.Hours) END)/(CASE WHEN a.hr_contract_weekly_hours_min__c < 11 THEN 1 ELSE a.hr_contract_weekly_hours_min__c END)::numeric as Workload	
		
		FROM
		
			Salesforce.account a 
				
		LEFT JOIN
		
			(SELECT 
			
				MIN(o.effectivedate) as mindate,
				EXTRACT(WEEK FROM o.effectivedate) as Week,
				o.professional__c,
				o.polygon,
				SUM(o.order_duration__c) as Hours		
				
			FROM
				
				bi.orders o
					
			WHERE

				o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL')
				AND o.effectivedate > '2016-12-31'
				
				
			GROUP BY 	
			
				o.professional__c,
				o.polygon,
				week) as OrdersList
						
		ON 
		
			(a.sfid = OrdersList.professional__c)
			
		WHERE
		
		 	a.test__c = '0'
		  	AND LEFT(a.delivery_areas__c, 2) IN ('de', 'nl', 'ch')
		   AND a.delivery_areas__c IS NOT NULL
		   AND a.status__c IN ('BETA', 'ACTIVE')
		   AND a.name NOT LIKE '%test%'
		   AND a.name NOT LIKE '%TEST%'
		   AND a.name NOT LIKE '%Test%'
			
		GROUP BY
		
			OrdersList.Week,
			Orderslist.mindate,
			a.sfid,
			a.parentid,
			a.name,
			a.delivery_areas__c,
			a.hr_contract_start__c,
			a.hr_contract_weekly_hours_min__c
			
		ORDER BY
		
			a.delivery_areas__c asc, a.name asc) as t1
			
	JOIN
	
	  	 Salesforce.Account t2
	   
	ON
	
	   	(t2.sfid = t1.parentid)
	
	WHERE 
	
		(t2.name LIKE '%BAT%' OR t2.name LIKE '%BOOK%')
		
	GROUP BY
	
		t1.Mindate,
		t1.Week,
		t1.sfid,
		t1.name,
		t1.delivery_areas,
		t1.HoursExpected1Week,
		t1.HoursPlanned,
		t1.Workload	
		
	ORDER BY
	
		t1.name,
		t1.HoursExpected1Week,
		t1.HoursPlanned,
		t1.Workload) as t2

GROUP BY

   week,
	polygons
	
ORDER BY

	week desc, polygons asc)
;