
-- job calendar Current

DROP TABLE IF EXISTS bi.muffin_order_distribution;
CREATE TABLE bi.muffin_order_distribution as 
SELECT
	o.City,
	o.contact__c,
	o.professional__c,
	acc.sfid,
	-- Chandra on 2018-03-19: this is a flag to know BAT cleaners and Partner cleaner
	CASE
		WHEN o.professional__c IS NOT NULL AND acc.sfid IS NOT NULL THEN 'Our Cleaner'
		WHEN o.professional__c IS NOT NULL AND acc.sfid IS NULL THEN 'Partner Cleaner'
		WHEN o.professional__c IS  NULL AND acc.sfid IS NULL THEN 'ID Unavailable'
	END AS cleaners_flag,
	status,
	order_type,
	LEFT(o.Locale__c,2) as locale,
	LEFT(o.ShippingPostalCode,3) as zipcode,
	o.Effectivedate::date as date,
	EXTRACT(DOW FROM o.Effectivedate) as weekday,
	EXTRACT(HOUR FROM o.Order_Start__c)+2 as Start_time,
	EXTRACT(HOUR FROM o.Order_End__c)+2 as End_time
FROM
	bi.orders o
-- Chandra on 2018-03-19: This table link will give the list of the cleaners BAT that we have on the platform, all of them
LEFT JOIN
		LATERAL(
				SELECT
					t1.*,
					t2.name as subcon
				FROM Salesforce.Account t1
					JOIN Salesforce.Account t2
				ON (t2.sfid = t1.parentid)
				WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
					and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
					and t2.name LIKE '%BAT Business Services GmbH%'
	) as acc
	ON acc.sfid = o.professional__c
WHERE
	-- Effectivedate::date between '2016-08-15'::date and current_date::date
	-- Chandra 2018-03-19: filter to get orders until the current week instead of current_date as stated above
	-- ((date_part('year'::text, Effectivedate) || '-'::text) || date_part('week'::text, Effectivedate)) between '2016' || '-'::text || extract(week from '2016-08-15'::date) and ((date_part('year'::text, now()) || '-'::text) || date_part('week'::text, now()))
	o.Effectivedate::date >= current_date::date - interval '1 month'
	and o.test__c = '0'
	-- Chandra on 2018-03-19: new addition to filter to only include the orders with the below status (the same filter has been used to calculate gmv_eur_net column in bi.orders view)
	and o.status IN ('ALLOCATION PAUSED', 'FULFILLED','INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','WAITING CONFIRMAITION','WAITING FOR ACCEPTANCE','WAITING FOR RESCHEDULE');