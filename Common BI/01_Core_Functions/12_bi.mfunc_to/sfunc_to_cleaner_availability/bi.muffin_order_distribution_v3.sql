
DROP TABLE IF EXISTS bi.muffin_order_distribution_v3;
CREATE TABLE bi.muffin_order_distribution_v3 as 	
SELECT
	city,
	Status,
	cleaners_flag,
	order_type,
	locale,
	date::date order_date,
	EXTRACT(WEEK FROM date::date) as CW,
	EXTRACT(DOW FROM date::date) as Weekday,
	hour,
	SUM(CASE WHEN EXTRACT(DOW FROM date) = DAY AND HOUR >= bi.muffin_order_distribution_v2.Start_time and hour < bi.muffin_order_distribution_v2.End_time THEN 1 ELSE 0 END) as Orders
FROM
	bi.muffin_order_distribution_v2
GROUP BY
	CW,
	zipcode,
	Status,
	cleaners_flag,
	order_type,
	locale,
	weekday,
	order_date,
	city,
	hour;