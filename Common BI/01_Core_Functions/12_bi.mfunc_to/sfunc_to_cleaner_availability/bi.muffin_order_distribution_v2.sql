
DROP TABLE IF EXISTS bi.muffin_order_distribution_v2;
CREATE TABLE bi.muffin_order_distribution_v2 as 
SELECT
	*
FROM
	bi.muffin_order_distribution,
	unnest(array[0,1,2,3,4,5,6]) as day,
	unnest(array[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) as hour;