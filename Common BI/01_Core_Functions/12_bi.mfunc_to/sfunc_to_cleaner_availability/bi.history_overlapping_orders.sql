
DELETE FROM bi.history_overlapping_orders WHERE date_today = current_date;
INSERT INTO bi.history_overlapping_orders

SELECT
	
	current_date as date_today,
	t1.sfid as sf_orderid1,
	t2.sfid as sf_orderid2,
	t1.effectivedate::date as orderdate,
	t1.polygon,
	t1.professional__c as professional,
	t1.contact__c as contact1,
	t2.contact__c as contact2,
	t1.order_id__c as order_id1,
	t1.type as type1,
	t2.order_id__c as order_id2,
	t2.type as type2,
	t1.order_time__c as order_time1,
	t2.order_time__c as order_time2,
	t1.order_duration__c as order_duration1,
	t2.order_duration__c as order_duration2

FROM

	bi.orders t1

LEFT JOIN

	bi.orders t2

ON

	(t1.professional__c = t2.professional__c AND t1.effectivedate::date = t2.effectivedate::date AND t1.order_id__c != t2.order_id__c)

WHERE

	t1.effectivedate::date > current_date + 2
	AND t1.effectivedate::date < current_date + 16
	AND t1.status NOT LIKE '%CANCELLED%'
	AND t1.order_time__c < t2.order_time__c

GROUP BY

	t1.sfid,
	t1.effectivedate::date,
	t2.sfid,
	t1.polygon,
	t1.contact__c,
	t2.contact__c,
	t1.professional__c,
	t1.order_id__c,
	t1.type,
	t2.order_id__c,
	t2.type,
	t1.order_time__c,
	t2.order_time__c,
	t1.order_duration__c,
	t2.order_duration__c

HAVING

	(EXTRACT(HOUR FROM t2.order_time__c::time) - EXTRACT(HOUR FROM t1.order_time__c::time)+1) <= (t1.order_duration__c );