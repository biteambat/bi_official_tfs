
DROP TABLE IF EXISTS bi.SummaryKPIs_temp10;
CREATE TABLE bi.SummaryKPIs_temp10 AS


SELECT

	EXTRACT(MONTH from t1.mindate1)::text as month,
	EXTRACT(YEAR from t1.mindate1)::text as year,
	t1.mindate1 as mindate,
	UPPER(t1.polygon1) as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	ROUND((CASE WHEN t1.numero > 0 THEN t1.total/t1.numero ELSE t1.total END)::numeric, 1) as value
		
	
FROM
	
	(SELECT
	
		LEFT(c.polygon, 2) as polygon1,
		CAST('CPL' as text) as kpi,
		EXTRACT(MONTH from c.date)::text as month,
		EXTRACT(YEAR from c.date)::text as year,
		MIN(c.date) as mindate1,
		SUM(c.leads) as numero,
		SUM(c.classifieds_costs+c.sem_costs+c.fb_costs) as total
							
	FROM
				
		bi.cleaners_costsperonboarding_polygon c
					
	WHERE 
				
		c.polygon NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
			
	GROUP BY
		
		polygon1,
		kpi,
		year,
		month
	
	ORDER BY 
		
		year desc, 
		month desc, 
		mindate1 desc) t1
		
GROUP BY

	month,
	year,
	mindate,
	city,
	kpi,
	breakdown,
	numero,
	total;