
-- Extracting the #Churn per country on a monthly basis


DROP TABLE IF EXISTS bi.SummaryKPIs_temp6;
CREATE TABLE bi.SummaryKPIs_temp6 AS


SELECT
  	
	EXTRACT(MONTH FROM date)::text as month, -- 
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	UPPER(polygon) as city,
	kpi,
	'-'::text as breakdown,
	SUM(value) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('Churn' as text) as kpi,
		LEFT(a.delivery_areas__c, 2) as Polygon,
		MAX(CASE WHEN ((a.hr_contract_end__c >= month_start) AND (a.hr_contract_end__c <= month_end)) THEN 1 ELSE 0 END) as value,
		month_start as date
					
	FROM
			
		salesforce.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			EXTRACT(year from date),
			EXTRACT(month from date)) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('ch', 'de', 'nl', 'at')
		AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		AND a.hr_contract_end__c IS NOT NULL
		
	GROUP BY
			
		name1,
		sfid1,
		Polygon,
		date
						
	ORDER BY 
			
		polygon asc) as subq 		
		
GROUP BY
	
	year,
	month,
	city,
	kpi
	  	
ORDER BY 
	
	year desc, 
	month desc, 
	city asc;