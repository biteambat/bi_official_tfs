
-- Extracting the worked hours per country on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp18;
CREATE TABLE bi.SummaryKPIs_temp18 AS



SELECT
	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate,
	UPPER(LEFT(m.delivery_area,2)) as city,
	CAST('Worked_hours' as text) as kpi,
	'-'::text as breakdown,
	ROUND(SUM(m.worked_hours)::numeric,1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	city,
	year,
	month
			
ORDER BY 
		
	mindate desc,
	city desc;