
-- Extracting the UR per country on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp16;
CREATE TABLE bi.SummaryKPIs_temp16 AS

SELECT

	EXTRACT(MONTH FROM t1.date)::text as month,
	EXTRACT(YEAR FROM t1.date)::text as year,
	MIN(t1.date::date) as mindate,
	t1.polygon1 as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	CASE WHEN SUM(contract_hours) > 0 THEN 
												 CASE WHEN SUM(worked_hours)<=SUM(contract_hours)
													   THEN SUM(worked_hours)/SUM(contract_hours) 
													   ELSE 1
												 END
		  ELSE 0
		  END as value

FROM

	(SELECT
	
		UPPER(LEFT(m.delivery_area, 2)) as polygon1,
		m.name,
		CAST('UR' as text) as kpi,
		m.mindate as date,
		CASE WHEN m.worked_hours <= m.working_hours THEN m.worked_hours ELSE working_hours END as worked_hours,
		working_hours as contract_hours
						
	FROM
			
		bi.margin_per_cleaner m
				
	WHERE 
			
		m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
						
	GROUP BY 
			
		polygon1,
		date,
		m.name,
		m.worked_hours,
		m.working_hours
				
	ORDER BY 
			
		date desc, polygon1 asc) t1
		
GROUP BY

	city,
	kpi,
	year,
	month
	
ORDER BY

	year desc, month desc, mindate desc, city asc;	