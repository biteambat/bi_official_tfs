
DROP TABLE IF EXISTS bi.SummaryKPIs;
CREATE TABLE bi.SummaryKPIs AS

SELECT

	CAST(0 as text) as month,
	CAST(0 as text) as year,
	CAST('2017-01-01' as date) as mindate,
	CAST(0 as text) as city,
	CAST(0 as text) as kpi,
	CAST(0 as text) as breakdown,
	CAST(0 as numeric) as value;


DELETE FROM bi.SummaryKPIs WHERE month = '0';

INSERT INTO bi.SummaryKPIs

(SELECT
	*
FROM
	bi.SummaryKPIs_temp1)	
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp2)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp3)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp4)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp5)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp6)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp7)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp8)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp9)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp10)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp11)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp12)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp13)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp14)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp15)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp16)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp17)
	
UNION

(SELECT
	*
FROM
	bi.SummaryKPIs_temp18)
	
;