
-- Extracting the number of Active Cleaners on a monthly basis per country


DROP TABLE IF EXISTS bi.SummaryKPIs_temp2;
CREATE TABLE bi.SummaryKPIs_temp2 AS

SELECT
		   
	EXTRACT(MONTH FROM t2.Cleaningdate)::text as month,
	EXTRACT(YEAR FROM t2.Cleaningdate)::text as year,
	MIN(t2.Cleaningdate) as mindate,
	UPPER(t2.city::text) as city,
	'Active_cleaners'::text as kpi,
	'-'::text as breakdown,
	COUNT(DISTINCT(t2.professional)) as value
				
FROM
			
	(SELECT
				
		LEFT(a.polygon, 2) as city,
		o.sfid as sfid,
		a.professional__c as professional,
		o.hr_contract_start__c::date as StartDate,
		o.hr_contract_end__c::date as EndDate,
		a.effectivedate as CleaningDate,
		a.status as Status,
		SUM(a.order_duration__c) as Hours
					 
   FROM bi.orders a INNER JOIN salesforce.account o on (a.professional__c = o.sfid)
			
	WHERE
					
		o.test__c = '0'
		AND a.test__c = '0'
		AND effectivedate >= '2016-01-01'
		AND a.type IN ('cleaning-b2c','cleaning-b2b', 'cleaning-b2b;cleaning-b2c','cleaning-b2c;cleaning-b2b')
		AND a.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER')
		AND (o.type__c LIKE '%cleaning-b2c%' OR o.type__c LIKE '%cleaning-b2b%')
		AND LEFT(a.locale__c,2) IN ('de','nl', 'ch', 'at')
		AND a.polygon IS NOT NULL
					    
	GROUP BY
				
		o.sfid,
		professional,
		a.polygon,
		StartDate,
		Cleaningdate,
		Status,
		EndDate) as t2
			
GROUP BY
			
	month,
	year,
	city
			   
ORDER BY 
			
	mindate desc, 
	city asc;