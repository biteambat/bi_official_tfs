
-- Extracting the #MiniJobbers per city on a monthly basis

DROP TABLE IF EXISTS bi.SummaryKPIs_temp12;
CREATE TABLE bi.SummaryKPIs_temp12 AS


SELECT
    
   EXTRACT(MONTH FROM date)::text as month,
	EXTRACT(YEAR FROM date)::text as year,
	MIN(date) as mindate,
	polygon as city,
	kpi,
	'-'::text as breakdown,
	SUM(VALUE) as value
	  	   
FROM(
	  
	SELECT
	  	
		DISTINCT(a.name) as Name1,
		a.sfid as sfid1,
		CAST('MiniJobbers' as text) as kpi,
		a.delivery_areas__c as Polygon,
		SUM((CASE WHEN ((a.hr_contract_start__c <= month_end) AND ((a.hr_contract_end__c >= month_start) OR (a.hr_contract_end__c IS NULL))) THEN 1 ELSE 0 END)) as value,
		month_start as date
					
	FROM
			
		salesforce.account a,
			
		(SELECT
			
			MIN(date) as month_start,
			MAX(date) as month_end,
			TO_CHAR(date,'YYYY-MM') as month
			
		FROM(	
	
			SELECT 
				
				i::date as date 
					
			FROM generate_series('2016-06-01', '2017-12-12', '1 day'::interval) i) as a
	
		GROUP BY
				
			month) as b
						
	WHERE
			
		a.delivery_areas__c IS NOT NULL
		AND a.delivery_areas__c NOT IN ('pl-poznan', 'de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		AND LEFT(a.delivery_areas__c, 2) IN ('de')
		-- AND a.status__c IN ('LEFT', 'FROZEN', 'TERMINATED', 'ACTIVE', 'BETA')
		AND a.hr_contract_weekly_hours_min__c < 11
		
	GROUP BY
			
		name1,
		sfid1,
		polygon,
		date				
		) as subq 
		
		
GROUP BY
	
	city,
	kpi,
	year,
	month
	  	
ORDER BY
	
	year desc,
	month desc,
	mindate desc, 
	city asc;	