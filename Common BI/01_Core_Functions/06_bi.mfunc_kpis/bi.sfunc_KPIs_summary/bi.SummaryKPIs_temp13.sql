
-- Extracting the average Score of the month per city

DROP TABLE IF EXISTS bi.SummaryKPIs_temp13;
CREATE TABLE bi.SummaryKPIs_temp13 AS


SELECT

	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate1,
	m.delivery_area as city,
	CAST('Score' as text) as kpi,
	'-'::text as breakdown,
	ROUND(AVG(m.score_cleaners::numeric),1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	month,
	year,
	m.delivery_area
	
ORDER BY 
		
	mindate1 desc;