
-- Extracting the average Score of the month per country

DROP TABLE IF EXISTS bi.SummaryKPIs_temp14;
CREATE TABLE bi.SummaryKPIs_temp14 AS

SELECT

	EXTRACT(MONTH FROM m.mindate)::text as month,
	EXTRACT(YEAR FROM m.mindate)::text as year,
	MIN(m.mindate::date) as mindate1,
	UPPER(LEFT(m.delivery_area, 2)) as city,
	CAST('Score' as text) as kpi,
	'-'::text as breakdown,
	ROUND(AVG(m.score_cleaners::numeric),1) as value
					
FROM
		
	bi.margin_per_cleaner m
			
WHERE 
		
	m.delivery_area NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
					
GROUP BY 
		
	month,
	year,
	city,
	kpi,
	breakdown
			
ORDER BY 
		
	year desc, month desc;