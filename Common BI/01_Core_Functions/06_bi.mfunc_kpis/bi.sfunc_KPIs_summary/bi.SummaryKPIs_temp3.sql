
-- Extracting the CAC on a monthly basis per city


DROP TABLE IF EXISTS bi.SummaryKPIs_temp3;
CREATE TABLE bi.SummaryKPIs_temp3 AS


SELECT

	EXTRACT(MONTH from t1.mindate)::text as month,
	EXTRACT(YEAR from t1.mindate)::text as year,
	t1.mindate as mindate,
	t1.polygon1 as city,
	t1.kpi as kpi,
	'-'::text as breakdown,
	ROUND((CASE WHEN t1.numero > 0 THEN t1.sum/t1.numero ELSE t1.sum END)::numeric, 1) as value
		
	
FROM
	
	(SELECT
	
		c.polygon as polygon1,
		CAST('CAC' as text) as kpi,
		EXTRACT(MONTH from c.date)::text as month,
		EXTRACT(YEAR from c.date)::text as year,
		MIN(c.date) as mindate,
		SUM(c.onboardings) as numero,
		SUM(c.classifieds_costs+c.sem_costs+c.fb_costs) as sum
							
	FROM
				
		bi.cleaners_costsperonboarding_polygon c
					
	WHERE 
				
		c.polygon NOT IN ('de-hannover','de-monchengladbach','de-wuppertal','de-potsdam','de-bochum', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
			
	GROUP BY
		
		polygon1,
		year,
		month
			
	ORDER BY 
		
		year desc, 
		month desc) t1;
