
SELECT

	t1.year_month,
	COUNT(DISTINCT t1.new_opps) as new_opps,	
	SUM(CASE WHEN t1.churn_opps IS NOT NULL THEN 1 ELSE 0 END) as opps_churned,
	SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN 1 ELSE 0 END) as young_churn	

FROM	
	
	(SELECT
	
		t1.*
	
	FROM
	
		(SELECT
		
			list_start.year_month,
			list_start.date_start,
			list_start.opportunityid as new_opps,
			list_churn.opportunityid as churn_opps,
			list_churn.date_churn
		
		FROM
			
			(SELECT
	
				LEFT(o.locale__c, 2) as country,
				MIN(TO_CHAR(o.createddate, 'YYYY-MM')) as year_month,
				o.opportunityid,
				MIN(o.effectivedate)::timestamp as date_start
			
			FROM
			
				salesforce.order o
				
			WHERE
			
				o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
				AND o.type = 'cleaning-b2b'
				AND o.professional__c IS NOT NULL
				AND o.test__c IS FALSE
				
			GROUP BY
			
				o.opportunityid,
				LEFT(o.locale__c, 2)) as list_start
				
		LEFT JOIN
		
			(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
		         -- It's the last day on which they are making money
				LEFT(oo.locale__c, 2) as country,
				o.opportunityid,
				MAX(TO_CHAR(o.createddate, 'YYYY-MM')) as year_month,
				MAX(o.effectivedate)::timestamp as date_churn
				
			FROM
			
				salesforce.order o
				
			LEFT JOIN
			
				salesforce.opportunity oo
				
			ON 
			
				o.opportunityid = oo.sfid
				
			WHERE
			
				o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
				AND oo.status__c IN ('RESIGNED', 'CANCELLED')
				AND oo.test__c IS FALSE
			
			GROUP BY
			
				LEFT(oo.locale__c, 2),
				o.opportunityid) as list_churn
				
		ON
		
			list_start.opportunityid = list_churn.opportunityid) as t1
			
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		t1.new_opps = oo.sfid
		
	WHERE 
	
		LEFT(oo.locale__c, 2) = 'de') as t1
		
GROUP BY

	year_month
	
ORDER BY

	year_month desc