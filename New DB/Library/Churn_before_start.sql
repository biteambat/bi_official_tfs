
-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 15/05/2018
-- Short Description: Calculation of churn before start

	
SELECT

	TO_CHAR(t2.churn_date,'YYYY-WW') as date_part,
	MIN(t2.churn_date::date) as date,
	t2.country as locale,
	t2.locale__c as languages,
	t2.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Before start' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
		
	SUM(CASE WHEN t2.churn_date < t2.date_first_order THEN 1 ELSE 0 END) as value	

FROM	
	
	
	(SELECT
	
		t1.sfid,
		t1.country,
		t1.locale__c,
		t1.closedate,
		oo.delivery_area__c,
		MIN(oo.effectivedate) as date_first_order,
		MIN(ooo.createddate) as churn_date
	
	FROM	
		
		(SELECT
		
			o.sfid,
			LEFT(o.locale__c, 2) as country,
			o.locale__c,
			o.closedate
			
		FROM
		
			salesforce.opportunity o
			
		WHERE
		
			o.stagename IN ('WON', 'PENDING')
			AND o.status__c IN ('RESIGNED', 'CANCELLED')) as t1
			
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		t1.sfid = oo.opportunityid
		
	LEFT JOIN
	
		salesforce.opportunityfieldhistory ooo
		
	ON
	
		t1.sfid = ooo.opportunityid
		
	WHERE
	
		ooo.newvalue IN ('CANCELLED', 'RESIGNED')
		-- AND oo.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
		
	GROUP BY
	
		t1.sfid,
		t1.closedate,
		t1.country,
		t1.locale__c,
		oo.delivery_area__c) as t2
		
	
GROUP BY

	date_part,
	t2.country,
	t2.locale__c,
	t2.delivery_area__c;		