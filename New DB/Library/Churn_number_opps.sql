
-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of the opportunities churned
	
SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_week,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	-- CAST('-' as varchar) as city,
	table1.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Count opps' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT table1.opportunityid) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		o.delivery_area__c,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.b2borders ooo
		
	ON
	
		o.opportunityid = ooo.opportunity_id
		
	WHERE
	
		o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		o.delivery_area__c,
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	table1.country,
	table1.locale__c,
	table1.delivery_area__c
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;