
-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of young churn revenue


SELECT

	t2.year_month_churn,
	MIN(t2.mindate) as mindate,
	t2.country as locale,
	t2.locale__c as languages,
	t2.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('Young churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.young_revenue_churn) as value

FROM

	
	(SELECT
	
		t1.year_month_churn,
		MIN(t1.date_churn) as mindate,
		t1.country,
		t1.locale__c,
		t1.delivery_area__c,
		-- t1.year_month_churn,
		-- t1.date_churn,
		COUNT(DISTINCT t1.new_opps) as new_opps,	
		SUM(CASE WHEN t1.churn_opps IS NOT NULL THEN 1 ELSE 0 END) as opps_churned,
		SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN 1 ELSE 0 END) as young_churn,	
		SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN t1.churned_revenue ELSE 0 END) as young_revenue_churn
	
	FROM	
		
		(SELECT
		
			t1.*
		
		FROM
		
			(SELECT
			
				list_start.year_month as year_month_start,
				list_start.date_start,
				list_start.country,
				list_start.locale__c,
				list_start.delivery_area__c,
				list_start.opportunityid as new_opps,
				list_churn.opportunityid as churn_opps,
				list_churn.revenue as churned_revenue,
				list_churn.year_month as year_month_churn,
				list_churn.date_churn
			
			FROM
				
				(SELECT
		
					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					o.delivery_area__c,
					MIN(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					o.opportunityid,
					MIN(o.effectivedate)::timestamp as date_start
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					
				GROUP BY
				
					o.opportunityid,
					o.locale__c,
					o.delivery_area__c,
					LEFT(o.locale__c, 2)) as list_start
					
			LEFT JOIN
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
					LEFT(oo.locale__c, 2) as country,
					o.opportunityid,
					MAX(CASE WHEN oo.amount IS NULL then oo.grand_total__c ELSE oo.amount END) as revenue,
					MAX(TO_CHAR(o.effectivedate, 'YYYY-MM')) as year_month,
					MAX(o.effectivedate)::timestamp as date_churn
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				WHERE
				
					o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND oo.test__c IS FALSE
				
				GROUP BY
				
					LEFT(oo.locale__c, 2),
					o.opportunityid) as list_churn
					
			ON
			
				list_start.opportunityid = list_churn.opportunityid) as t1
				
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			t1.new_opps = oo.sfid
			) as t1
			
	GROUP BY
	
		-- year_month_start,
		year_month_churn,
		t1.country,
		t1.delivery_area__c,
		t1.locale__c
		
	ORDER BY
	
		year_month_churn desc) as t2
		
WHERE

	t2.year_month_churn IS NOT NULL
		
GROUP BY

	t2.year_month_churn,
	t2.country,
	t2.delivery_area__c,
	t2.locale__c
	
ORDER BY

	t2.year_month_churn desc;
