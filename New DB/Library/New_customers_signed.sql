
-- Author: Sylvain Vanhuysse
-- Function: kpi_master
-- Date of Creation: 24/05/2018
-- Short Description: Calculation of new opps signed


SELECT

	TO_CHAR(closedate ,'YYYY-WW') as date_part,
	MIN(closedate::date) as date,
	left(o.locale__c,2) as country,
	o.locale__c as languages,
	oo.delivery_area__c as city,
	CAST('B2B' as varchar) as type,
	CAST('New' as text) as kpi,
	CAST('Opps' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CASE WHEN o.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT(o.sfid)) as value

FROM

	Salesforce.Opportunity o
	
LEFT JOIN

	salesforce.order oo
	
ON

	o.sfid = oo.opportunityid

WHERE

	stagename in ('WON','PENDING')

GROUP BY

	date_part,
	o.locale__c,
	oo.delivery_area__c,
	(CASE WHEN o.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END)
	
ORDER BY

	date_part desc;