-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 04/04/2018
-- Short Description: Calculation of the revenue netto B2B


SELECT

	TO_CHAR(o.effectivedate,'YYYY-WW') as year_week,
	MIN(o.effectivedate::date) as date,
	LEFT(o.locale__c,2) as locale,
	o.locale__c as languages,
	o.polygon as city,
	CAST('B2B' as varchar) as type,
	CAST('Revenue' as varchar) as kpi,
	CAST('Netto' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,

	SUM(o.gmv_eur_net) as value

FROM

	bi.orders o
	
WHERE

	o.effectivedate < current_date
	AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND o.type IN ('cleaning-b2b')
	
GROUP BY

	year_week,
	o.polygon,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
	
ORDER BY

	year_week desc;























