	
SELECT

	LEFT(ooo.locale__c, 2) as country,
	table7.year_month,
	ooo.opportunity_id,
	-- ooo.churn_reason__c
	MAX(CASE WHEN ooo.grand_total__c IS NULL THEN ooo.total_amount ELSE ooo.grand_total__c END) as grand_total,
	SUM(ooo.bat_hour_share) as hours_bat,
	SUM(ooo.total_hours) as tot_hours,
	SUM(ooo.bat_hour_share)/SUM(ooo.total_hours) as bat_share
	-- COUNT(DISTINCT table7.opportunityid) as opps_churned
	
FROM	
		
	(SELECT
	
		t006.year_month,
		t006.opportunityid
		-- MAX(t006.date_churn) as date_churn
		-- COUNT(DISTINCT t006.opportunityid) as churned_opps
	
	FROM
		
		(SELECT

			TO_CHAR(table1.date_churn, 'YYYY-MM') as year_month,
			table1.country,
			table1.opportunityid as opportunityid
		
		FROM
			
			(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
				LEFT(oo.locale__c, 2) as country,
				o.opportunityid,
				'last_order' as type_date,
				MAX(o.effectivedate) as date_churn
				
			FROM
			
				salesforce.order o
				
			LEFT JOIN
			
				salesforce.opportunity oo
				
			ON 
			
				o.opportunityid = oo.sfid
				
			WHERE
			
				o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
				AND oo.status__c IN ('RESIGNED', 'CANCELLED')
				AND oo.test__c IS FALSE
			
			GROUP BY
			
				LEFT(oo.locale__c, 2),
				type_date,
				o.opportunityid) as table1
				
		GROUP BY
		
			TO_CHAR(table1.date_churn, 'YYYY-MM'),
			table1.country,
			table1.opportunityid
			
		ORDER BY
		
			TO_CHAR(table1.date_churn, 'YYYY-MM') desc) as t006
								
	GROUP BY
	
		year_month,
		t006.opportunityid
		-- t006.date_churn
		
	ORDER BY
	
		year_month desc) as table7		
				
LEFT JOIN

	bi.b2borders ooo
	
ON 
	
	table7.opportunityid = ooo.opportunity_id
	
WHERE

	ooo.opportunity_id IS NOT NULL
	
GROUP BY

	LEFT(ooo.locale__c, 2),
	table7.year_month,
	ooo.opportunity_id
	
ORDER BY

	table7.year_month desc,
	country		
				
				
				