		
SELECT

	table3.year_month,
	table3.country,
	COUNT(DISTINCT table3.opportunityid) as churned_opps,
	SUM(table3.grand_total) as revenue_loss

FROM
	
	(SELECT
	
		table2.year_month,
		table2.country,
		table2.opportunityid,
		MAX(CASE WHEN oo.grand_total__c IS NULL THEN oo.total_amount ELSE oo.grand_total__c END) as grand_total
	
	FROM
			
		(SELECT
		
			TO_CHAR(table1.date_churn, 'YYYY-MM') as year_month,
			table1.country,
			table1.opportunityid as opportunityid
		
		FROM
			
			(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
				LEFT(oo.locale__c, 2) as country,
				o.opportunityid,
				'last_order' as type_date,
				MAX(o.effectivedate) as date_churn
				
			FROM
			
				salesforce.order o
				
			LEFT JOIN
			
				salesforce.opportunity oo
				
			ON 
			
				o.opportunityid = oo.sfid
				
			WHERE
			
				o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
				AND oo.status__c IN ('RESIGNED', 'CANCELLED')
				AND oo.test__c IS FALSE
			
			GROUP BY
			
				LEFT(oo.locale__c, 2),
				type_date,
				o.opportunityid) as table1
				
		GROUP BY
		
			TO_CHAR(table1.date_churn, 'YYYY-MM'),
			table1.country,
			table1.opportunityid
			
		ORDER BY
		
			TO_CHAR(table1.date_churn, 'YYYY-MM') desc) as table2
			
		LEFT JOIN
		
			bi.b2borders oo
			
		ON 
		
			table2.opportunityid = oo.opportunity_id
			
	GROUP BY
	
		table2.year_month,
		table2.country,
		table2.opportunityid) as table3

GROUP BY

	table3.year_month,
	table3.country
				
ORDER BY

	table3.year_month desc				
				
				
				