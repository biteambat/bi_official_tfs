-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 03/04/2018
-- Short Description: Calculation of the number of hours lost with the customers B2B (opportunities) who churned


SELECT

	TO_CHAR(churn_date,'YYYY-WW') as year_week,
	MIN(churn_date::date) as date,
	LEFT(locale__c,2) as locale,
	locale__c as languages,
	CAST('-' as varchar) as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Hours' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,

	ROUND(SUM(t3.hours_weekly__c))*4.33 as value

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		SUM(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END) as total,
		t2.hours_weekly__c
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t2.hours_weekly__c
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_week,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
ORDER BY

	year_week desc;
		
		
		
		