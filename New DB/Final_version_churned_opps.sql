	
	
SELECT

	TO_CHAR(table1.date_churn, 'YYYY-MM') as year_month,
	table1.country,
	COUNT(DISTINCT table1.opportunityid) as churned_opps

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	table1.country
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc
	
		
		
		
		