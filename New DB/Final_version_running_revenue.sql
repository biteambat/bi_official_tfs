			

SELECT

	table3.year_month,
	table3.country,
	COUNT(DISTINCT table3.opportunityid) as opps_running,
	SUM(table3.grand_total) as revenue_running

FROM

	(SELECT
	
		table2.year_month,
		table2.country,
		table2.opportunityid as opportunityid,
		MAX(CASE WHEN oo.grand_total__c IS NULL THEN oo.total_amount ELSE oo.grand_total__c END) as grand_total
	
	FROM
				
		(SELECT	
		
			table1.year_month,
			table1.country,
			table1.opportunityid,
			SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as active_opps
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								
							GROUP BY
							
								o.opportunityid,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(oo.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							WHERE
							
								o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND oo.test__c IS FALSE
							
							GROUP BY
							
								LEFT(oo.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid) as table1
				
		GROUP BY
		
			table1.year_month,
			table1.country,
			table1.opportunityid
			
		ORDER BY
		
			table1.year_month desc) as table2
			
	LEFT JOIN
	
		bi.b2borders oo
	
	ON 
	
		table2.opportunityid = oo.opportunity_id	
			
	WHERE
	
		table2.active_opps = 1		
			
	GROUP BY
	
		table2.year_month,
		table2.country,
		table2.opportunityid) as table3	
		
GROUP BY

	table3.year_month,
	table3.country
	
ORDER BY 

	table3.year_month desc,
	table3.country