const { inspect }   = require("util");
const _             = require("lodash");
const when          = require("when");
const PG            = require("services/src/pg");
const ad_costs      = require("./marketing/adwords_cost");

const pg            = new PG("bi");

console.log(`[DAILY-BI][info] Task started Parameters: ${process.argv.slice(2)}`);

function getProcedures() {
    console.log("[DAILY-BI][info] Querying procedures");

    //-- All the daily Procedures will be named "daily$"
    return pg.queryRaw(`
        SELECT  p.proname procedure_name, n.nspname procedure_namespace
        FROM    pg_catalog.pg_namespace n
        JOIN    pg_catalog.pg_proc p
        ON      p.pronamespace = n.oid
        WHERE   n.nspname = 'bi' and p.proname like 'daily$%'
    `);
}

function executeProcedures(procedures) {
    console.log(`[DAILY-BI][info] Executing procedures Data: ${JSON.stringify(procedures)}`);

    const executions = _.map(procedures || [], (procedure) => {
        console.log(`[DAILY-BI][info] Running ${procedure.procedure_name}`);
        return pg.queryRaw(`SELECT * FROM bi.${procedure.procedure_name}(cast(current_date as date))`)
            .catch((error) => {
                console.log(`[daily-bi][error] - Procedure execution error Procedure: ${procedure.procedure_name} - error: ${JSON.stringify(error)}`);
            });
    });

    return when.all(executions);
}

function successHandler() {
    console.log("[DAILY-BI] Task finished with success");
    process.exit(0);
}

function errorHandler(error) {
    console.log(`[daily-bi][error] - Task finished with error - error message: ${error} - error inspect: ${inspect(error).replace(/\s*\n\s*/g, " ")} - stack: ${(error || {}).stack}`);
    process.exit(1);
}

ad_costs.delete_data()
    .then(ad_costs.import_rd)
    .then(ad_costs.import_batb2b)
    .then(getProcedures)
    .then(executeProcedures)
    .then(successHandler)
    .catch(errorHandler);
