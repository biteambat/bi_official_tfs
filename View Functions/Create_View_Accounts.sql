CREATE VIEW main.account AS
	select
	  sfid as Cleaner_ID
	, createddate as Created_At
	, status__c as Status
	, ending_reason__c as Ending_Reason
	, name as Name
	, gender__c as Gender
	, birthdate__c as Birthdate
	, email__c as Email
	, languages__c as Languages
	, nationality__c as Nationality
	, can__c as Skills
	, has_vehicle__c as Has_Vehicle
	, rating as Rating
	, lastactivitydate as Last_Activity_Date
	, legal_backgroundcheck__c as Legal_Backgroundcheck
	, legal_contract__c as Legal_Contract
	, legal_contract_ip__c as Legal_Contract_IP
	, phone as Phone
	, billingstreet as Billing_Street
	, billingpostalcode as Billing_Postal_Code
	, billingcity as Billing_City
	, billingcountry as Billing_Country
	, locale__c as Locale
	, billinglatitude as Billing_Latitude
	, billinglongitude as Billing_Longitude
	, availability_text__c as Availability_Area
	, availability_monday__c as Availability_Monday
	, availability_tuesday__c as Availability_Tuesday
	, availability_wednesday__c as Availability_Wednesday
	, availability_thursday__c as Availability_Thursday
	, availability_friday__c as Availability_Friday
	-- , availability_saturday__c as Availability_Saturday
	, availability_sunday__c as Availability_Sunday
	, availability_last_modified__c as Availability_Last_Modified
	, pph__c as Price_Per_Hour
	, app_version__c as App_Version
	, deviceid__c as Device_ID
	, lastmodifieddate as Last_Modified_Date
	, systemmodstamp as System_Mod_Timestamp
	-- , createdbyid as Created_by
	, ownerid as Owner_ID
	, test__c as Is_Test_Account
	, prp_friend__c as Ref_Cleaner_ID
	, prp_funded__c as Ref_Funded
	, prp_referred__c as Ref_Date
	-- , company_name__c as Company_Name
	, parentid as Parent_Cleaner_ID
	from salesforce.account;