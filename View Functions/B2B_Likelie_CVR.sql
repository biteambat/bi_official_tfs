DROP VIEW IF EXISTS bi.history_view;
CREATE VIEW bi.history_view as 
SELECT
	parentid,
	MIN(CASE WHEN newvalue = 'NEW' and field  = 'stage__c' THEN createddate::date else null END) as new_date,
	MIN(CASE WHEN newvalue = 'VALIDATED' and field  = 'stage__c' THEN createddate::date else null END) as validated_date,
	MIN(CASE WHEN newvalue = 'NOT_REACHED' and field  = 'stage__c' THEN createddate::date else null END) as not_reached_date,
	MIN(CASE WHEN newvalue = 'REACHED' and field  = 'stage__c' THEN createddate::date else null END) as reached_date,
	MIN(CASE WHEN newvalue = 'DM_REACHED' and field  = 'stage__c' THEN createddate::date else null END) as dm_reached_date,
	MIN(CASE WHEN newvalue = 'QUALIFIED' and field  = 'stage__c' THEN createddate::date else null END) as qualified_date,
	MIN(CASE WHEN newvalue = 'ENDED' and field  = 'stage__c' THEN createddate::date else null END) as ended_date
	MIN(CASE WHEN newvalue = 
FROM
	salesforce.likeli__history
WHERE
	field  = 'stage__c'
	and createddate::date >= '2017-02-25'
GROUP BY
	parentid;
	
