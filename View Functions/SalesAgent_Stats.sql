DROP VIEW IF EXISTS bi.sales_stats;
CREATE VIEW bi.sales_stats as

SELECT

	TO_CHAR(t2.closedate::date,'YYYY-MM') as Year_Month,
	t3.name,
	left(t1.locale__c,2) as locale,
	t2.closedate::date as mindate,
	COUNT(DISTINCT(CASE WHEN stagename IN ('WON', 'PENDING') THEN t2.sfid ELSE NULL END)) as Closed_Deals,
	COUNT(DISTINCT(CASE WHEN stagename IN ('WON', 'PENDING') and t2.acquisition_channel__c in ('inbound','web') THEN t2.sfid ELSE NULL END)) as Closed_Inbound_Deals,
	COUNT(DISTINCT(CASE WHEN stagename IN ('WON', 'PENDING') and t2.acquisition_channel__c not in ('inbound','web') THEN t2.sfid ELSE NULL END)) as Closed_Outbound_Deals,
	MAX(t5.pipeline) as pipeline,
	COUNT(DISTINCT(CASE WHEN stagename IN ('LOST', 'CONTRACT FAILED') THEN t2.sfid ELSE NULL END)) as lost_deals,
	MAX(total_calls) as total_calls,
	MAX(connected_calls) as connected_calls,
	MAX(calling_days) as calling_days,
	AVG(avg_talktime)* '1 minute'::interval as avg_talktime

FROM
	Salesforce.Likeli__c t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)
LEFT JOIN
	Salesforce.User t3
ON
	(t1.ownerid = t3.sfid)
	
LEFT JOIN

	(SELECT
	
		t3.name,
		t1.ownerid,
		COUNT(DISTINCT(CASE WHEN stagename NOT IN ('LOST', 'CONTRACT FAILED', 'WON', 'PENDING') THEN t2.sfid ELSE NULL END)) as pipeline
	
	FROM
		Salesforce.Likeli__c t1
	JOIN
		Salesforce.opportunity t2
	ON
		(t1.opportunity__c = t2.sfid)
	LEFT JOIN
		Salesforce.User t3
	ON
		(t1.ownerid = t3.sfid)
		
	GROUP BY
	
		t3.name,
		t1.ownerid)	as t5
ON

	(t1.ownerid = t5.ownerid)
	
LEFT JOIN
	
	(SELECT
		TO_CHAR(date::date,'YYYY-MM') as Month,
		TO_CHAR(date::date, 'YYYY-MM-DD') as calldate,
		Name,
		sum(total_calls) as total_calls,
		sum(connected_call) as connected_calls,
		COUNT(DISTINCT(CASE WHEN total_calls > 3 THEN date ELSE null end)) as Calling_Days,
		TO_CHAR((avg_talktime || 'second')::interval, 'HH24:MI:SS') as call_duration,
		AVG(avg_talktime) as avg_talktime
	FROM
		
	bi.sales_calls
	GROUP BY
		name,
		Month,
		calldate,
		TO_CHAR((avg_talktime || 'second')::interval, 'HH24:MI:SS')
		
		ORDER BY
		
			TO_CHAR(date::date,'YYYY-MM') desc,
			calldate desc) as t4
ON
	(t3.name = t4.name and TO_CHAR(t2.closedate::date,'YYYY-MM-DD') = t4.calldate)
	
WHERE

	t2.closedate <= current_date
	AND t1.test__c IS FALSE
	AND t2.test__C IS FALSE
	
GROUP BY
	Year_Month,
	locale,
	t3.Name,
	t2.closedate
	
ORDER BY

	t2.closedate::date desc;
