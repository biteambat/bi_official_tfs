DROP VIEW bi.sales_stats;
CREATE VIEW bi.sales_stats as 
SELECT
	TO_CHAR(t2.Closedate::date,'YYYY-MM') as Year_Month,
	min(closedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	t3.name,
	COUNT(DISTINCT(CASE WHEN stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED') THEN t2.sfid ELSE NULL END)) as Closed_Deals,
	COUNT(DISTINCT(CASE WHEN stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED') and t2.acquisition_channel__c in ('inbound','web') THEN t2.sfid ELSE NULL END)) as Closed_Inbound_Deals,
	COUNT(DISTINCT(CASE WHEN stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED') and t2.acquisition_channel__c not in ('inbound','web') THEN t2.sfid ELSE NULL END)) as Closed_Outbound_Deals,
	COUNT(DISTINCT(CASE WHEN stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','LOST') THEN t2.sfid ELSE NULL END)) as opps_through_process
FROM
	Salesforce.Likeli__c t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)
LEFT JOIN
	Salesforce.User t3
ON
	(t1.ownerid = t3.sfid)
GROUP BY
	Year_Month,
	t3.Name,
	locale;

