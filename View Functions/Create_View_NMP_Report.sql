DROP VIEW IF EXISTS reports.nmp_orders_by_city;
CREATE OR REPLACE VIEW reports.nmp_orders_by_city AS
select a.*, b.city
from (	
		select order_id, order_start_date_utc, order_status
				from main.order
				where order_status in ('INVOICED', 'CANCELLED NO MANPOWER')
			UNION 
		select order_id, order_start_date_utc, 'TOTAL' as order_status
				from main.order
				where acquisition_channel in ('web', 'ios', 'android')
		) a
	left join (
					select sfid as order_id, city
					from bi.orders
				) b
		on a.order_id = b.order_id
;