DROP VIEW IF EXISTS bi.payment_method;
CREATE VIEW bi.payment_method as 
SELECT
	a.sfid as order_id,
	a.method__c
FROM(
SELECT
	t1.sfid,
	t2.order__c,
	t2.method__c,
	sum(t2.refundable_amount__c) as amount
FROM
	Salesforce.Order t1
JOIN
	Salesforce.payment__c t2
ON
	(t2.order__c = t1.sfid)
WHERE
	effectivedate::date > '2017-01-04'
GROUP BY
	t1.sfid,
	t2.order__c,
	t2.method__c
HAVING
	sum(t2.refundable_amount__c) > 0) as a

