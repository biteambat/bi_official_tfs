 SELECT "AccountHistory"."Id" AS id,
    "AccountHistory"."IsDeleted" AS is_deleted,
    "AccountHistory"."AccountId" AS cleaner_id,
    "AccountHistory"."CreatedById" AS created_by,
    "AccountHistory"."CreatedDate" AS created_at,
    "AccountHistory"."Field" AS changed_field,
    "AccountHistory"."OldValue" AS old_value,
    "AccountHistory"."NewValue" AS new_value
   FROM bi."AccountHistory";