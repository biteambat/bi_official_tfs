DROP VIEW IF EXISTS bi.history_view;
CREATE VIEW bi.history_view as 
SELECT
	parentid,
	locale,
	acquisition_tracking_id__c,
	lead_source,
	lead_created,
	times_recycled,
	first_touch_date,
	1 as new_cvr,
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null or not_reached_date is not null or ended_date is not null THEN 1 ELSE 0 END) as touched_cvr,	
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null THEN 1 ELSE 0 END) as reached_cvr,
	MAX(CASE WHEN opp_date is not null or qualified_date is not null THEN 1 ELSE 0 END) as qualified_cvr,
	MAX(CASE WHEN opp_date is not null  THEN 1 ELSE 0 END) as opp_cvr,
	MAX(CASE WHEN ended_date is not null THEN 1 ELSE 0 END) as ended_cvr,
	MAX(CASE WHEN not_reached_date is not null THEN 1 ELSE 0 END) as not_reached_cvr,
	MAX(CASE WHEN dm_reached_date is not null THEN 1 ELSE 0 END) as dm_reached_cvr	
FROM(
SELECT
	t1.parentid,
	t2.acquisition_tracking_id__c,
	LEFT(t2.locale__c,2) as locale,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t2.createddate::date as lead_created,
	t3.createddate::date as first_touch_date,
	CASE WHEN recycle__c is null THEN 0 ELSE recycle__c END as times_recycled,
	MIN(CASE WHEN newvalue = 'NEW' and field  = 'stage__c' THEN t1.createddate::date else null END) as new_date,
	MIN(CASE WHEN newvalue = 'VALIDATED' and field  = 'stage__c' THEN t1.createddate::date else null END) as validated_date,
	MIN(CASE WHEN newvalue = 'NOT REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as not_reached_date,
	MIN(CASE WHEN newvalue = 'REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as reached_date,
	MIN(CASE WHEN newvalue = 'DM REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as dm_reached_date,
	MIN(CASE WHEN newvalue = 'QUALIFIED' and field  = 'stage__c' THEN t1.createddate::date else null END) as qualified_date,
	MIN(CASE WHEN newvalue = 'ENDED' and field  = 'stage__c' THEN t1.createddate::date else null END) as ended_date,
	MIN(CASE WHEN newvalue != ''  and field = 'opportunity__c' THEN t1.createddate::date else null end) as opp_date
	
FROM
	salesforce.likeli__history t1
JOIN
	salesforce.likeli__c t2
ON
	(t1.parentid = t2.sfid)
LEFT JOIN
	bi.leadhistory_touched_date t3
ON
	(t1.parentid = t3.parentid)
WHERE
	field  in ('stage__c','opportunity__c')
GROUP BY
	t1.parentid,
	t2.acquisition_tracking_id__c,
	t2.createddate::date,
	lead_source,
	locale,
	first_touch_date,
	times_recycled) as a
GROUP BY
	parentid,
	lead_source,
	first_touch_date,
	locale,
	acquisition_tracking_id__c,
	lead_created,
	times_recycled;

DROP TABLE IF EXISTS bi.leadhistory_touched_date;
CREATE TABLE bi.leadhistory_touched_date as 
SELECT
	parentid,
	min(createddate::date) as createddate
FROM
	salesforce.likeli__history t1
WHERE
	field = 'Owner'
	and createddate::date > '2017-02-01'
	and oldvalue in ('API Tiger','MC Tiger','IT Tiger','Alejandra Chavez','Renan Ribeiro','Austin Marcus')
	and newvalue not in ('Alejandra Chavez','Renan Ribeiro','Austin Marcus','API Tiger','MC TIGER','IT TIGER')
GROUP BY
	parentid;
	
DROP TABLE IF EXISTS bi.likeli_to_opp_conversion;
CREATE TABLE bi.likeli_to_opp_conversion as
SELECT
	t2.sfid as opp_id,
	t1.acquisition_tracking_id__c,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t1.createddate::date as lead_created,
	t2.createddate::date as opp_created,
	t2.closedate::date as signed_date,
	extract(day from age(t2.closedate::date,t2.createddate::date)) as time_to_convert,
	case when t2.grand_total__c > 0 THEN t2.grand_total__c ELSE t2.amount END as Monthly_Amount,
	CASE WHEN t2.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED') THEN 1 ELSE 0 END as Conversion_Signed,
	CASE WHEN t3.hours > 0 THEN 1 ELSE 0 END as Conversion_Started
FROM
	salesforce.likeli__c t1
JOIN 
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)
LEFT JOIN
	(SELECT
		t3.opportunity_id,
		sum(total_hours) as hours
	FROM
		bi.b2borders t3
	GROUP BY
		opportunity_id) as t3
ON
	(t2.sfid = t3.opportunity_id)

	
DROP VIEW IF EXISTS bi.history_view;
CREATE VIEW bi.history_view as 
SELECT
	parentid,
	locale,
	acquisition_tracking_id__c,
	lead_source,
	lead_created,
	times_recycled,
	first_touch_date,
	1 as new_cvr,
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null or not_reached_date is not null or ended_date is not null THEN 1 ELSE 0 END) as touched_cvr,	
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null THEN 1 ELSE 0 END) as reached_cvr,
	MAX(CASE WHEN opp_date is not null or qualified_date is not null THEN 1 ELSE 0 END) as qualified_cvr,
	MAX(CASE WHEN opp_date is not null  THEN 1 ELSE 0 END) as opp_cvr,
	MAX(CASE WHEN ended_date is not null THEN 1 ELSE 0 END) as ended_cvr,
	MAX(CASE WHEN not_reached_date is not null THEN 1 ELSE 0 END) as not_reached_cvr,
	MAX(CASE WHEN dm_reached_date is not null THEN 1 ELSE 0 END) as dm_reached_cvr	
FROM(
SELECT
	t1.parentid,
	t2.acquisition_tracking_id__c,
	LEFT(t2.locale__c,2) as locale,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t2.createddate::date as lead_created,
	t3.createddate::date as first_touch_date,
	CASE WHEN recycle__c is null THEN 0 ELSE recycle__c END as times_recycled,
	MIN(CASE WHEN newvalue = 'NEW' and field  = 'stage__c' THEN t1.createddate::date else null END) as new_date,
	MIN(CASE WHEN newvalue = 'VALIDATED' and field  = 'stage__c' THEN t1.createddate::date else null END) as validated_date,
	MIN(CASE WHEN newvalue = 'NOT REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as not_reached_date,
	MIN(CASE WHEN newvalue = 'REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as reached_date,
	MIN(CASE WHEN newvalue = 'DM REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as dm_reached_date,
	MIN(CASE WHEN newvalue = 'QUALIFIED' and field  = 'stage__c' THEN t1.createddate::date else null END) as qualified_date,
	MIN(CASE WHEN newvalue = 'ENDED' and field  = 'stage__c' THEN t1.createddate::date else null END) as ended_date,
	MIN(CASE WHEN newvalue != ''  and field = 'opportunity__c' THEN t1.createddate::date else null end) as opp_date
	
FROM
	salesforce.likeli__history t1
JOIN
	salesforce.likeli__c t2
ON
	(t1.parentid = t2.sfid)
LEFT JOIN
	bi.leadhistory_touched_date t3
ON
	(t1.parentid = t3.parentid)
WHERE
	field  in ('stage__c','opportunity__c')
GROUP BY
	t1.parentid,
	t2.acquisition_tracking_id__c,
	t2.createddate::date,
	lead_source,
	locale,
	first_touch_date,
	times_recycled) as a
GROUP BY
	parentid,
	lead_source,
	first_touch_date,
	locale,
	acquisition_tracking_id__c,
	lead_created,
	times_recycled;

