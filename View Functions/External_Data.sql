CREATE OR REPLACE VIEW external_data.adwords_city as 
SELECT * FROm dblink('dbname=postgres port=5432 host= 35.189.238.19
                user=postgres password=g3iwrcMv','SELECT
	*
FROM
	external.adwords_city')  AS t1(	date date,
	country text,
	region text,
	city text,
	account text,
	campaign text,
	ad_group text,
	impressions numeric,
	clicks numeric,
	conversions numeric,
	cost numeric)

CREATE OR REPLACE VIEW external_data.facebook_campaigns as 
SELECT * FROm dblink('dbname=postgres port=5432 host= 35.189.238.19
                user=postgres password=g3iwrcMv','SELECT
	*
FROM
	external.facebook_campaigns')  AS t1(		date text,
	region text,
	ad_account text,
	campaign text,
	ad_set text,
	ad text,
	spend decimal)	
	

CREATE OR REPLACE VIEW external_data.piwik_actions AS 
 SELECT t1.type,
    t1.visit_id,
    t1.url,
    t1.page_id,
    t1.goal_name,
    t1.server_date,
    t1.unixtime,
    t1.servertime_pretty,
    t1.revenue,
    t1.transaction_id,
    t1.city,
    t1.postalcode,
    t1.delivery_area,
    t1.transaction_total,
    t1.discount,
    t1.duration
   FROM dblink('dbname=postgres port=5432 host=  35.189.238.19
                user=postgres password=g3iwrcMv'::text, 'SELECT
	*
FROM
	external.piwik_actions'::text) t1(type text, visit_id text, url text, page_id text, goal_name text, server_date date, unixtime int, servertime_pretty timestamp, revenue numeric, transaction_id text, city text, postalcode text, delivery_area text, transaction_total numeric, discount numeric, duration numeric);


CREATE OR REPLACE VIEW external_data.piwik_visits AS 
 SELECT
    t1.visit_id,
    t1.visitor_id,
    t1.locale,
    t1.region,
    t1.city,
    t1.user_id,
    t1.visit_ip,
    t1.longitude,
    t1.latitude,
    t1.server_date,
    t1.firstaction_unixtime,
    t1.lastaction_unixtime,
    t1.visit_duration_seconds,
    t1.device_type,
    t1.device_model,
    t1.browser_name,
    t1.referrer_name as referrer_type_name,
    t1.referrer_type,
    t1.referrer_name,
    t1.referrer_keyword,
    t1.referrer_url,
    t1.referrer_search_url,
    t1.visitor_type,
    t1.days_since_firstvisit,
    t1.days_since_lastvisit,
    t1.actions,
    t1.goal_conversions,
    t1.visit_converted,
    t1.utm_source,
    t1.utm_medium,
    t1.utm_campaign,
    t1.utm_content,
    t1.utm_term,
    t1.clid,
    t1.voucher_code

   FROM dblink('dbname=postgres port=5432 host=  35.189.238.19
                user=postgres password=g3iwrcMv'::text, 'SELECT
	*
FROM
	external.piwik_visits'::text) t1(visit_id text,
visitor_id text,
locale text,
region text,
city text,
user_id text,
visit_ip text,
longitude text,
latitude text,
server_date date,
firstaction_unixtime int,
lastaction_unixtime int,
visit_duration_seconds int,
device_type text,
device_model text,
browser_name text,
referrer_type_name text,
referrer_type text,
referrer_name text,
referrer_keyword text,
referrer_url text,
referrer_search_url text,
visitor_type text,
days_since_firstvisit int,
days_since_lastvisit int,
actions int,
goal_conversions int,
visit_converted int,
utm_source text,
utm_medium text,
utm_campaign text,
utm_content text,
utm_term text,
clid text,
voucher_code text);


;