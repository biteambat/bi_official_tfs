SELECT
	createddate::date as date,
	COUNT(CASE WHEn acquisition_tracking_id__c like '%b2b home cb%' THEN t1.sfid ELSE null end) as lead_home_cb,
	COUNT(CASE WHEn acquisition_tracking_id__c like '%b2b home form%' THEN t1.sfid ELSE null end) as lead_home_form,
	COUNT(CASE WHEn acquisition_tracking_id__c like '%b2b funnel%' THEN t1.sfid ELSE null end) as lead_funnel,
	COUNT(CASE WHEn acquisition_tracking_id__c like '%b2c home form%' THEN t1.sfid ELSE null end) as lead_b2c,
	COUNT(CASE WHEn acquisition_tracking_id__c like '%unbounce%' THEN t1.sfid ELSE null end) as unbounce
FROM
	Salesforce.likeli__c t1
WHERE
	acquisition_channel__c in ('inbound','web')
	and createddate::date > '2017-05-15'
	and type__c = 'B2B'
	
GROUP BY
	date
	
SELECt
	acquisition_tracking_id__c,
	*
FROM
		Salesforce.likeli__c t1
WHERE
	acquisition_channel__c in ('inbound','web')
	and createddate::date > '2017-06-01'
	and type__c = 'B2B'
