CREATE VIEW main.account_events AS

select created_at, professional_json::json->>'Id' as Cleaner_ID,  professional_json::json->>'Status__c' as Status
from events.sodium
where event_name in (
'Account Event:LEFT'
,'Account Event:TERMINATED'
,'Account Event:SUSPENDED');