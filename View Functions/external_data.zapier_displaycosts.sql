CREATE OR REPLACE VIEW external_data.zapier_displaycosts AS

	SELECT 
		t1.cost_id,
		t1.edit_date,
		t1.start_date,
		t1.end_date,
		t1.channel,
		t1.locale,
		t1.polygon,
		t1.description,
		t1.total_costs_eur,
		t1.daily_costs_eur,
		t1.manual_validation,
		t1.formula_validation
	   FROM dblink('dbname=postgres port=5432 host= 35.189.238.19
                user=postgres password=g3iwrcMv'::text, 'SELECT
	*
FROM
	external.etl_displaycosts'::text) t1 
(cost_id integer, 
edit_date timestamp without time zone, 
start_date date, 
end_date date, 
channel text,
locale text,
polygon text, 
description text, 
total_costs_eur numeric, 
daily_costs_eur numeric, 
manual_validation text, 
formula_validation text);