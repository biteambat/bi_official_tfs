DROP VIEW IF EXISTS bi.sales_calls;
CREATE VIEW bi.sales_calls as 
SELECt
	t2.name,
	t1.systemmodstamp::date as date,
	COUNT(1) as Total_Calls,
	SUM(CASE WHEN callconnected__c = 'Yes' THEN 1 ELSE 0 END) as connected_call,
	AVG(t1.calltalkseconds__c) as avg_talktime
FROM
	Salesforce.natterbox_call_reporting_object__c t1
JOIN
	Salesforce.User t2
ON
	(t1.ownerid = t2.sfid)
WHERE 
	t1.systemmodstamp > '2018-01-01'
	and wrapup_string_1__c like '%B2B%'
GROUP BY
	t2.name,
	date
	