DROP ViEW IF EXISTS bi.b2b_orders_tableau;
CREATE VIEW bi.b2b_orders_tableau as
			SELECT
				contact__c,
				order_Duration__c,
				polygon as polygon,
				        CASE
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date < '2015-10-01'::date THEN t.gmv__c * 0.96::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2015-10-01'::date AND t.order_creation__c::date < '2017-12-01'::date THEN t.gmv__c * 0.92::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2017-12-01'::date AND t.order_creation__c::date < '2018-01-01'::date THEN t.gmv__c * 0.85696::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-01-01'::date AND t.order_creation__c::date < '2018-02-01'::date THEN t.gmv__c * 0.85497::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-02-01'::date AND t.order_creation__c::date < '2018-03-01'::date THEN t.gmv__c * 0.86243::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-03-01'::date AND t.order_creation__c::date < '2018-04-01'::date THEN t.gmv__c * 0.86721::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-04-01'::date AND t.order_creation__c::date < '2018-05-01'::date THEN t.gmv__c * 0.85067::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-05-01'::date AND t.order_creation__c::date < '2018-06-01'::date THEN t.gmv__c * 0.83410::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-06-01'::date AND t.order_creation__c::date < '2018-07-01'::date THEN t.gmv__c * 0.86971::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-07-01'::date AND t.order_creation__c::date < '2018-08-01'::date THEN t.gmv__c * 0.86421::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-08-01'::date AND t.order_creation__c::date < '2018-09-01'::date THEN t.gmv__c * 0.86292::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-09-01'::date AND t.order_creation__c::date < '2018-10-01'::date THEN t.gmv__c * 0.88224::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-10-01'::date AND t.order_creation__c::date < '2018-11-01'::date THEN t.gmv__c * 0.87716::double precision
            WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-11-01'::date AND t.order_creation__c::date < '2018-12-01'::date THEN t.gmv__c * 0.87718::double precision
           
           
				WHEN "left"(t.locale__c::text, 2) = 'ch'::text AND t.order_creation__c::date >= '2018-12-01'::date THEN t.gmv__c * 0.88333::double precision
            ELSE t.gmv__c END as gmv_eur,
				effectivedate as date,
				status,
				order_id__c,
				t.locale__c as locale,
				t2.*
			FROM
				 bi.orders t
			LEFT JOIN
				 Salesforce.Opportunity t2
			ON
				 (t2.sfid = t.opportunityid)
			WHERE
				 type = 'cleaning-b2b'
				 AND status NOT LIKE ('%CANCELLED%')

