DROP VIEW IF EXISTS bi.customer_segmentation;

CREATE VIEW bi.customer_segmentation as 

SELECT
 contact__c,
 country,
 CASE
 WHEN (country in ('de','at','nl') and GMV_Last30D > 120) or (country = 'ch' and GMV_Last30D > 195) THEN CAST('A' as varchar)
 WHEN (country in ('de','at','nl') and GMV_Last30D between 80 and 120) or (country = 'ch' and GMV_Last30D between 130 and 195) THEN CAST('B' as varchar)
 WHEN (country in ('de','at','nl') and GMV_Last30D < 80) or (country = 'ch' and GMV_Last30D < 130) THEN CAST('C' as varchar)
 ELSE 'Other'::varchar END as segmentation

FROM(

		SELECT
		 contact__c,
		 left(locale__c,2) as country,
		 SUM(CASE
		        WHEN "left"(locale__c::text, 2) = 'ch'::text AND order_creation__c::date < '2015-10-01'::date AND status = 'INVOICED' and (effectivedate::date between current_date - 32 and current_date - 2) THEN gmv__c * 0.96::double precision
		        WHEN "left"(locale__c::text, 2) = 'ch'::text AND order_creation__c::date >= '2015-10-01'::date AND status = 'INVOICED' and (effectivedate::date between current_date - 32 and current_date - 2) THEN gmv__c * 0.92::double precision
		        WHEN left(locale__c::text,2) <> 'ch' AND status = 'INVOICED' and (effectivedate::date between current_date - 32 and current_date - 2) THEN gmv__c
						ELSE 0
		    END) as GMV_Last30D
		 
		FROM
		 salesforce.order 

		WHERE
		 (type::text !~~ '%222%'::text OR type::text is NULL) 
		 and left(locale__c,2) in ('de','at','nl','ch')
		 and test__c = '0'

		GROUP BY
		 contact__c,
		 country

		ORDER BY GMV_Last30D desc
	)

 
as t1;