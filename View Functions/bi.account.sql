DROP VIEW IF EXISTS bi.account;
CREATE VIEW bi.account as 

 SELECT account.last_invoice_number_us__c,
    account.id,
    account.otherphone__c,
    account.archive_id__c,
    account.private_insurance__c,
    account.billingcountry,
    account.availability_sunday__c,
    account.createddate,
    account.deviceid__c,
    account.availability_friday__c,
    account.type,
    -- account.availability_saturday__c,
    account.in_test_period__c,
    account.shippingcity,
    account.sfid,
    account.name,
    account.description,
    -- account.billingstatecode,
    account.numberofemployees,
    account.systemmodstamp,
    account.lastreferenceddate,
    account.languages__c,
    account.availability_thursday__c,
    account.legal_contract_ip__c,
    account.last_invoice_number__c,
    -- account.shippingstreet,
    account.rating,
    account.bank_bic__c,
    account.bank_iban__c,
    account.phone,
    account.nationality__c,
    account._hc_err,
    account.birthdate__c,
    account.type__c,
    account.shippingcountrycode,
    account.can__c,
    account.invoiced_us__c,
    account.shippinglongitude,
    account.email__c,
    account.billingstreet,
    account.vat_exempt__c,
    account.ending_reason__c,
    account.datev__c,
    account.shippingcountry,
    account.parentid,
    account.shippinglatitude,
    account._hc_lastop,
    account.lastactivitydate,
    account.test__c,
    account.legal_contract__c,
    account.availability_text__c,
    account.shippingstatecode,
    account.gender__c,
    account.website,
    account.billingcity,
    account.billinglongitude,
    account.legal_backgroundcheck__c,
    account.availability_last_modified__c,
    account.bank_name__c,
    account.has_vehicle__c,
    account.jigsawcompanyid,
    account.contract_commission__c,
    account.shippingstate,
    -- account.createdbyid,
    account.masterrecordid,
    account.billingcountrycode,
    account.availability_tuesday__c,
    -- account.billingstate,
    account.industry,
    account.ownerid,
    account.tax_number__c,
    account.billingpostalcode,
    account.isdeleted,
    account.pph__c,
    account.lastmodifieddate,
    account.prp_referred__c,
    account.prp_friend__c,
    -- account.shippingpostalcode,
    account.lastmodifiedbyid,
    account.app_version__c,
    account.locale__c,
    account.billinglatitude,
    account.prp_funded__c,
    account.status__c,
    account.availability_monday__c,
    account.lastvieweddate,
    -- account.company_name__c,
    account.availability_wednesday__c,
    account.offers_last__c,
    account.offers__c,
    account.absence_end__c,
    account.absence_start__c,
    account.absence_reason__c,
    account.rating__c,
    account.performance_last_month__c,
    account.hr_contract_weekly_hours_min__c,
    account.performance__c,
    account.performance_past_json__c,
    account.hr_contract_end__c,
    account.performance_current_month__c,
    account.hr_employee_number__c,
    account.performance_forecasted_week__c,
    account.performance_next_json__c,
    account.hr_contract_weekly_hours_max__c,
    account.hr_contract_start__c,
    account.performance_invoiced_week__c,
    account.performance_week__c,
    account.hr_insurance_name__c,
    account.utilization_ratio_current_week__c,
    account.performance_ideal_month__c,
    account.utilization_ratio_current_month__c,
    account.imei__c,
    account.hr_social_security_number__c,
    account.performance_past_3m_json__c,
    account.performance_next_3m_json__c,
    account.longitude__c,
    account.latitude__c,
    account.hr_days_unpaid__c,
    account.hr_dates_absent__c,
    account.hr_days_sickness__c,
    account.hr_holidays_max__c,
    account.hr_sickness_max__c,
    account.commuting_threshold__c,
    account.delivery_areas__c,
    account.style__c
   FROM salesforce.account;