DROP VIEW IF EXISTS bi.vouchers_redemptions_L12Months;
CREATE OR REPLACE VIEW bi.vouchers_redemptions_L12Months AS

	SELECT 
		left(locale__c,2) as locale,
		voucher__c,
		MAX(order_creation__c::date)::date as last_redemption,
		COUNT(DISTINCT CASE WHEN order_creation__c::date between (current_date - interval '30 days') and current_date THEN sfid ELSE NULL END) as redemptions_l30D

	FROM bi.orders
		WHERE test__c = '0'
		AND order_creation__c::date > (current_date - interval '1 year')

	GROUP BY voucher__c, left(locale__c,2)

UNION ALL
		SELECT 
		'ALL' as locale,
		voucher__c,
		MAX(order_creation__c::date)::date as last_redemption,
		COUNT(DISTINCT CASE WHEN order_creation__c::date between (current_date - interval '30 days') and current_date THEN sfid ELSE NULL END) as redemptions_l30D

	FROM bi.orders
		WHERE test__c = '0'
		AND order_creation__c::date > (current_date - interval '1 year')
		GROUP BY voucher__c
