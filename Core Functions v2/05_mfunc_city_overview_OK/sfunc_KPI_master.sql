CREATE OR REPLACE FUNCTION bi.sfunc_kpi_master(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_kpi_master';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.kpi_master;
CREATE TABLE bi.kpi_master AS


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 04/04/2018
-- Short Description: Calculation of the revenue netto B2B

SELECT

	TO_CHAR(o.effectivedate,'YYYY-WW') as date_part,
	MIN(o.effectivedate::date) as date,
	LEFT(o.locale__c,2) as locale,
	o.locale__c as languages,
	o.polygon as city,
	CAST('B2B' as varchar) as type,
	CAST('Revenue' as varchar) as kpi,
	CAST('Netto' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,

	SUM(o.gmv_eur_net) as value

FROM

	bi.orders o
	
WHERE

	o.effectivedate < current_date
	AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND o.type IN ('cleaning-b2b')
	
GROUP BY

	date_part,
	o.polygon,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
	
ORDER BY

	date_part desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 04/04/2018
-- Short Description: Calculation of the revenue netto B2C


SELECT

	TO_CHAR(o.effectivedate,'YYYY-WW') as date_part,
	MIN(o.effectivedate::date) as date,
	LEFT(o.locale__c,2) as locale,
	o.locale__c as languages,
	o.polygon as city,
	CAST('B2C' as varchar) as type,
	CAST('Revenue' as varchar) as kpi,
	CAST('Netto' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,

	SUM(o.gmv_eur_net) as value

FROM

	bi.orders o
	
WHERE

	o.effectivedate < current_date
	AND o.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND o.type IN ('cleaning-b2c')
	
GROUP BY

	date_part,
	o.polygon,
	locale,
	languages,
	city,
	type,
	kpi,
	sub_kpi_1,
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5
	
	
ORDER BY

	date_part desc;


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 14/05/2018
-- Short Description: Calculation of the opportunities churned
	
SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_week,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	CAST('-' as varchar) as city,
	-- table1.polygon as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Count opps' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	COUNT(DISTINCT table1.opportunityid) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		-- ooo.polygon,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.b2borders ooo
		
	ON
	
		o.opportunityid = ooo.opportunity_id
		
	WHERE
	
		o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		-- ooo.polygon,
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	table1.country,
	table1.locale__c
	-- table1.polygon
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;
	

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 14/05/2018
-- Short Description: Calculation of the opportunities revenue churned		
		
	
SELECT

	TO_CHAR(table1.date_churn,'YYYY-MM') as year_month,
	MIN(table1.date_churn::date) as date,
	LEFT(table1.country,2) as locale,
	table1.locale__c as languages,
	CAST('-' as varchar) as city,
	-- table1.polygon as city,
	CAST('B2B' as varchar) as type,
	CAST('Churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Weekly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(table1.grand_total) as value

FROM
	
	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		-- ooo.polygon,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as date_churn,
		MAX(CASE WHEN ooo.grand_total__c IS NULL THEN ooo.total_amount ELSE ooo.grand_total__c END) as grand_total
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN
	
		bi.b2borders ooo
		
	ON
	
		o.opportunityid = ooo.opportunity_id
		
	WHERE
	
		o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		-- ooo.polygon,
		type_date,
		o.opportunityid) as table1
		
GROUP BY

	TO_CHAR(table1.date_churn, 'YYYY-MM'),
	table1.country,
	table1.locale__c
	-- table1.polygon
	
ORDER BY

	TO_CHAR(table1.date_churn, 'YYYY-MM') desc;
	

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 14/05/2018
-- Short Description: Calculation of the running opportunities		
			
	
SELECT	

	table1.year_month as year_month,
	MIN(table1.ymd) as date,
	LEFT(table1.locale__c, 2) as locale,
	table1.locale__c as languages,
	CAST('-' as varchar) as city,
	-- table1.polygon as city,
	CAST('B2B' as varchar) as type,
	CAST('Running Opps' as varchar) as kpi,
	CAST('Monthly' as varchar) as sub_kpi_1,
	CAST('-' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	
	SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
FROM
	
	(SELECT
	
		t2.*,
		o.status__c as status_now
	
	FROM
		
		(SELECT
			
				time_table.*,
				table_dates.*,
				CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
			
			FROM	
				
				(SELECT
					
					'1'::integer as key,	
					TO_CHAR(i, 'YYYY-MM') as year_month,
					MIN(i) as ymd
					-- i::date as date 
					
				FROM
				
					generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
					
				GROUP BY
				
					key,
					year_month
					-- date
					
				ORDER BY 
				
					year_month desc) as time_table
					
			LEFT JOIN
			
				(SELECT
				
					'1'::integer as key_link,
					t1.country,
					t1.locale__c,
					-- t1.polygon,
					t1.opportunityid,
					EXTRACT(YEAR FROM t1.date_start) as year_start,
					EXTRACT(MONTH FROM t1.date_start) as month_start,
					MIN(t1.date_start) as year_month_start,
					-- TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
					CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn
					-- CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
					
				
				FROM
					
					((SELECT
	
						LEFT(o.locale__c, 2) as country,
						o.locale__c,
						-- o.delivery_area__c as polygon,
						o.opportunityid,
						MIN(o.effectivedate) as date_start
					
					FROM
					
						salesforce.order o
						
					WHERE
					
						o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
						AND o.type = 'cleaning-b2b'
						AND o.professional__c IS NOT NULL
						AND o.test__c IS FALSE
						
					GROUP BY
					
						o.opportunityid,
						LEFT(o.locale__c, 2),
						o.locale__c
						-- o.delivery_area__c
						)) as t1
						
				LEFT JOIN
				
					(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
					         -- It's the last day on which they are making money
						LEFT(oo.locale__c, 2) as country,
						o.opportunityid,
						'last_order' as type_date,
						MAX(o.effectivedate) as date_churn
						
					FROM
					
						salesforce.order o
						
					LEFT JOIN
					
						salesforce.opportunity oo
						
					ON 
					
						o.opportunityid = oo.sfid
						
					WHERE
					
						o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
						AND oo.status__c IN ('RESIGNED', 'CANCELLED')
						AND oo.test__c IS FALSE
					
					GROUP BY
					
						LEFT(oo.locale__c, 2),
						type_date,
						o.opportunityid) as t2
						
				ON
				
					t1.opportunityid = t2.opportunityid
					
				GROUP BY
				
					key_link,
					t1.country,
					t1.locale__c,
					-- t1.polygon,
					t1.opportunityid,
					year_start,
					month_start,
					t2.date_churn) as table_dates
					
			ON 
			
				time_table.key = table_dates.key_link
				
		) as t2
			
	LEFT JOIN
	
		salesforce.opportunity o
		
	ON
	
		t2.opportunityid = o.sfid) as table1
		
GROUP BY

	table1.year_month,
	table1.country,
	table1.locale__c
	-- table1.polygon
	
ORDER BY

	table1.year_month desc;		
		

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 14/05/2018
-- Short Description: Calculation of the running opportunities revenue				
	

SELECT

	table3.year_month,
	table3.mindate as date,
	table3.country as locale,
	table3.locale__c as languages,
	CAST('-' as varchar) as city,
	CAST('B2B' as varchar) as type,
	CAST('Running' as varchar) as kpi,
	CAST('Revenue' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(table3.grand_total) as value

FROM

	(SELECT
	
		table2.year_month,
		table2.mindate,
		table2.country,
		table2.locale__c,
		table2.opportunityid as opportunityid,
		MAX(CASE WHEN oo.grand_total__c IS NULL THEN oo.total_amount ELSE oo.grand_total__c END) as grand_total
	
	FROM
				
		(SELECT	
		
			table1.year_month,
			MIN(table1.ymd) as mindate,
			table1.country,
			table1.locale__c,
			table1.opportunityid,
			SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as active_opps
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o
								
							LEFT JOIN
	
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							LEFT JOIN
							
								bi.b2borders ooo
								
							ON
							
								o.opportunityid = ooo.opportunity_id
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								
							GROUP BY
							
								o.opportunityid,
								o.locale__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(oo.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							WHERE
							
								o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND oo.test__c IS FALSE
							
							GROUP BY
							
								LEFT(oo.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid) as table1
				
		GROUP BY
		
			table1.year_month,
			table1.country,
			table1.locale__c,
			table1.opportunityid
			
		ORDER BY
		
			table1.year_month desc) as table2
			
	LEFT JOIN
	
		bi.b2borders oo
	
	ON 
	
		table2.opportunityid = oo.opportunity_id	
			
	WHERE
	
		table2.active_opps = 1		
			
	GROUP BY
	
		table2.year_month,
		table2.mindate,
		table2.country,
		table2.locale__c,
		table2.opportunityid) as table3	
		
GROUP BY

	table3.year_month,
	table3.mindate,
	table3.country,
	table3.locale__c
	
ORDER BY 

	table3.year_month desc,
	table3.country;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.kpi_master


-- Author: Sylvain Vanhuysse
-- Function: 
-- Date of Creation: 14/05/2018
-- Short Description: Calculation of young churn


SELECT

	t2.year_month_churn,
	MIN(t2.mindate) as mindate,
	t2.country as locale,
	t2.locale__c as languages,
	CAST('-' as varchar) as city,
	CAST('B2B' as varchar) as type,
	CAST('Young churn' as varchar) as kpi,
	CAST('-' as varchar) as sub_kpi_1,
	CAST('Monthly' as varchar) as sub_kpi_2,
	CAST('-' as varchar) as sub_kpi_3,
	CAST('-' as varchar) as sub_kpi_4,
	CAST('-' as varchar) as sub_kpi_5,
	SUM(t2.young_churn) as value



FROM

	
	(SELECT
	
		t1.year_month_churn,
		MIN(t1.date_churn) as mindate,
		t1.country,
		t1.locale__c,
		-- t1.year_month_churn,
		-- t1.date_churn,
		COUNT(DISTINCT t1.new_opps) as new_opps,	
		SUM(CASE WHEN t1.churn_opps IS NOT NULL THEN 1 ELSE 0 END) as opps_churned,
		SUM(CASE WHEN DATE_PART('day', t1.date_churn - t1.date_start) < 42 THEN 1 ELSE 0 END) as young_churn	
	
	FROM	
		
		(SELECT
		
			t1.*
		
		FROM
		
			(SELECT
			
				list_start.year_month as year_month_start,
				list_start.date_start,
				list_start.country,
				list_start.locale__c,
				list_start.opportunityid as new_opps,
				list_churn.opportunityid as churn_opps,
				list_churn.year_month as year_month_churn,
				list_churn.date_churn
			
			FROM
				
				(SELECT
		
					LEFT(o.locale__c, 2) as country,
					o.locale__c,
					MIN(TO_CHAR(o.createddate, 'YYYY-MM')) as year_month,
					o.opportunityid,
					MIN(o.effectivedate)::timestamp as date_start
				
				FROM
				
					salesforce.order o
					
				WHERE
				
					o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
					AND o.type = 'cleaning-b2b'
					AND o.professional__c IS NOT NULL
					AND o.test__c IS FALSE
					
				GROUP BY
				
					o.opportunityid,
					o.locale__c,
					LEFT(o.locale__c, 2)) as list_start
					
			LEFT JOIN
			
				(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
			         -- It's the last day on which they are making money
					LEFT(oo.locale__c, 2) as country,
					o.opportunityid,
					MAX(TO_CHAR(o.createddate, 'YYYY-MM')) as year_month,
					MAX(o.effectivedate)::timestamp as date_churn
					
				FROM
				
					salesforce.order o
					
				LEFT JOIN
				
					salesforce.opportunity oo
					
				ON 
				
					o.opportunityid = oo.sfid
					
				WHERE
				
					o.status IN ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START')
					AND oo.status__c IN ('RESIGNED', 'CANCELLED')
					AND oo.test__c IS FALSE
				
				GROUP BY
				
					LEFT(oo.locale__c, 2),
					o.opportunityid) as list_churn
					
			ON
			
				list_start.opportunityid = list_churn.opportunityid) as t1
				
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			t1.new_opps = oo.sfid
			) as t1
			
	GROUP BY
	
		-- year_month_start,
		year_month_churn,
		t1.country,
		t1.locale__c
		
	ORDER BY
	
		year_month_churn desc) as t2
		
GROUP BY

	t2.year_month_churn,
	t2.country,
	t2.locale__c
	
ORDER BY

	t2.year_month_churn desc;



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$
LANGUAGE plpgsql VOLATILE

















