﻿
DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_to_cleaners_scoring(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_to_cleaners_scoring';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.temp_cancels;
CREATE TABLE bi.temp_cancels AS

SELECT
      
   t1.Professional as sfid,
   ROUND((CASE WHEN (t1.Terminated = 0) THEN 2 ELSE (1/t1.Terminated::numeric) END), 2) as Terminated_coeff,
   ROUND((CASE WHEN (t1.Cancelled_professional = 0) THEN 2 ELSE (1/t1.Cancelled_professional::numeric) END), 2) as Cancelled_pro_coeff,
   ROUND((CASE WHEN (t1.Noshow_professional = 0) THEN 2 ELSE (1/t1.Noshow_professional::numeric) END), 2) as Noshow_professional_coeff
      
FROM      
      
   (SELECT
       
      o.professional__c as Professional,
      SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as Terminated,
      SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as Cancelled_professional,
      SUM(CASE WHEN o.status IN ('NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as Noshow_professional
         
   FROM bi.orders o 
        
   WHERE
       
      o.status IN ('INVOICED', 'CANCELLED TERMINATED', 'CANCELLED PROFESSIONAL', 'NOSHOW PROFESSIONAL')
      AND o.test__c = '0'
      AND o.effectivedate >= (current_date - 90)
      AND LEFT(o.locale__c, 2) IN ('de', 'nl', 'ch', 'at') 
        
   GROUP BY o.professional__c) as t1;



----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.number_days_month;
CREATE TABLE bi.number_days_month AS

SELECT

    TO_CHAR(date,'MM') as Month,
    min(date::date) as Mindate,
    COUNT(DISTINCT(date)) as days_per_month

FROM

(select i::date as date from generate_series('2017-01-01','2018-12-31', '1 day'::interval) i) as a 

GROUP BY
    Month;

     
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
    
   
DROP TABLE IF EXISTS bi.temp_results;
CREATE TABLE bi.temp_results AS

SELECT
     
   a.sfid as sfid,
   a.name as cleaners,
   a.delivery_areas__c as polygons,
   Margin_cleaner.rating as rating,
   Margin_cleaner.global_gpm as global_gpm,
   Margin_cleaner.UR as ur,
          
   ROUND((CASE WHEN (Margin_cleaner.sickness != 0) THEN (1/Margin_cleaner.sickness::numeric) ELSE 2 END), 2) as sickness_coeff,
   Margin_cleaner.worked_hours        
      
FROM salesforce.account a       
      
LEFT JOIN
     
   (SELECT
      
      m.year_month,
      m.professional__c as sfid,
      m.name,
      (CASE WHEN m.rating IS NOT NULL THEN m.rating ELSE 2.5 END) as rating,
      m.delivery_area,
      SUM(m.sickness) as sickness,
      SUM(m.worked_hours) as worked_hours,
      SUM(m.gp) as GP,
      SUM(m.revenue) as revenue,
      CASE WHEN sum(m.revenue) > 0 THEN SUM(m.gp)/sum(m.revenue) ELSE 0 END as global_gpm,
      CASE WHEN LEFT(m.delivery_area, 2) IN ('at') THEN 7 ELSE SUM(m.monthly_hours) END as expected_worked_hours,

      CASE WHEN m.delivery_area = 'at-vienna'
           THEN CASE WHEN MAX(m.weekly_hours) > 0 THEN (SUM(m.worked_hours)/(112*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(112*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(112*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 
                              END)
                     END 
           WHEN m.delivery_area = 'ch-basel'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(43*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(43*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(43*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 
                              END)
                     END
           WHEN m.delivery_area = 'ch-bern'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(54*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(54*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                                THEN (SUM(m.worked_hours)/(54*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                                ELSE 2 
                                END)
                     END
           WHEN m.delivery_area = 'ch-geneva'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 
                              END)
                     END 
           WHEN m.delivery_area = 'ch-lausanne'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(38*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(38*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                              THEN (SUM(m.worked_hours)/(38*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                              ELSE 2 END)
                     END
           WHEN m.delivery_area = 'ch-lucerne'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(65*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                     ELSE (CASE WHEN (SUM(m.worked_hours)/(65*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                                THEN (SUM(m.worked_hours)/(65*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                     END
           WHEN m.delivery_area = 'ch-stgallen'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(50*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(50*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                        THEN (SUM(m.worked_hours)/(50*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                        ELSE 2 END)
                 END
           WHEN m.delivery_area = 'ch-zurich'
           THEN CASE WHEN MAX(m.weekly_hours) > 10 THEN (SUM(m.worked_hours)/(64*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(64*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                          THEN (SUM(m.worked_hours)/(64*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                 END
           WHEN m.delivery_area = 'nl-amsterdam'
           THEN CASE WHEN MAX(m.weekly_hours) > 2 THEN (SUM(m.worked_hours)/(76*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(76*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                          THEN (SUM(m.worked_hours)/(76*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                 END
           WHEN m.delivery_area = 'nl-hague'
           THEN CASE WHEN MAX(m.weekly_hours) > 2 THEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                            THEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                            ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-berlin'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-bonn'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(61*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-cologne'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                          THEN (SUM(m.worked_hours)/(75*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                          ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-dusseldorf'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(70*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-essen'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(86*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-frankfurt'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END) 
                 END
           WHEN m.delivery_area = 'de-hamburg'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(82*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-hannover'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(78*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-mainz'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(39*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-munich'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(91*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-nuremberg'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                        THEN (SUM(m.worked_hours)/(58*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                        ELSE 2 END)
                 END
           WHEN m.delivery_area = 'de-stuttgart'
           THEN CASE WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2) THEN (SUM(m.worked_hours)/(90*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month))))
                     WHEN (MAX(m.weekly_hours) < 11 AND (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) > 2) THEN 2 
                 ELSE (CASE WHEN (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) < 2 
                      THEN (SUM(m.worked_hours)/(93*((CAST(EXTRACT(DAY FROM current_date) as numeric))/MAX(days_in_month.days_per_month)))) 
                      ELSE 2 END) 
                 END
           ELSE 0
           END
               as UR

   FROM bi.margin_per_cleaner m,
        bi.number_days_month as days_in_month

       
   WHERE 
      
      CAST(RIGHT(m.year_month,2) as numeric) = EXTRACT(month from current_date)
      AND CAST(LEFT(m.year_month,4) as numeric) = EXTRACT(year from current_date)
      AND days_in_month.Month::numeric = EXTRACT(MONTH FROM current_date)
       
   GROUP BY 
      
      m.year_month,
      m.professional__c,
      m.name,
      m.rating,
      m.delivery_area) as Margin_cleaner
      
ON
     
   (Margin_cleaner.sfid = a.sfid)
      
WHERE
     
   a.status__c IN ('BETA', 'ACTIVE')
   AND a.test__c = '0'
   AND LEFT(a.locale__c, 2) IN ('de', 'nl', 'ch', 'at')
   AND a.delivery_areas__c IS NOT NULL
   AND (a.hr_contract_start__c <= (current_date - interval '90 days') OR a.hr_contract_start__c IS NULL) 
   AND Margin_cleaner.UR IS NOT NULL
      
GROUP BY
     
   a.sfid,
   a.delivery_areas__c,
   a.name,
   Margin_cleaner.rating,
   Margin_cleaner.global_gpm,
   Margin_cleaner.UR,
   Margin_cleaner.sickness,
   Margin_cleaner.worked_hours
      
ORDER BY 
   
    polygons
    ;


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_KPI_Performance;
CREATE TABLE bi.temp_KPI_Performance AS

SELECT
      
   t1.sfid,
   t1.polygons,
   t1.cleaners,
   t1.rating,
   t1.global_gpm,
   t1.UR,
   t1.sickness_coeff,       
   t3.Terminated_coeff,
   t3.Cancelled_pro_coeff,
   t3.Noshow_professional_coeff
        
FROM bi.temp_results t1

LEFT JOIN bi.temp_cancels t3 ON t1.sfid = t3.sfid

ORDER BY t1.polygons asc 
    
    ;
       
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_KPI_Performance2;
CREATE TABLE bi.temp_KPI_Performance2 AS

SELECT
      
   t4.sfid,
   t4.polygons,
   t4.cleaners,
   t4.rating,
   t4.global_gpm,
   t4.UR,
      
   -- t4.Terminated_coeff as Terminated,
   (CASE WHEN (t4.Terminated_coeff = 2) 
        THEN 5 
      ELSE (CASE WHEN ((0.33 <= t4.Terminated_coeff) AND (t4.Terminated_coeff <= 1)) 
                 THEN 4 
              ELSE (CASE WHEN (((0.25) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.33))) 
                         THEN 3 
                     ELSE (CASE WHEN (((0.2) <= t4.Terminated_coeff) AND (t4.Terminated_coeff < (0.25))) 
                                THEN 2 
                            ELSE (CASE WHEN (t4.Terminated_coeff < (0.2)) 
                                       THEN 1 
                                    ELSE NULL 
                                    END) END) END) END) END) as Coeff_terminated, 

   -- t4.Cancelled_pro_coeff as CancelledPro,
  (CASE WHEN ((1 <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff <= 2)) 
        THEN 5 
      ELSE (CASE WHEN ((0.33 <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < 1)) 
                 THEN 4 
              ELSE (CASE WHEN (((0.2) <= t4.Cancelled_pro_coeff AND (t4.Cancelled_pro_coeff < 0.33))) 
                         THEN 3 
                     ELSE (CASE WHEN (((0.125) <= t4.Cancelled_pro_coeff) AND (t4.Cancelled_pro_coeff < (0.2))) 
                                THEN 2 
                            ELSE (CASE WHEN (t4.Cancelled_pro_coeff < (0.125)) 
                                       THEN 1 
                                    ELSE NULL 
                                    END) END) END) END) END) as Coeff_cancelled_pro,

   -- t4.Noshow_professional_coeff as Noshow,
   (CASE WHEN (t4.Noshow_professional_coeff = 2) 
        THEN 5 
      ELSE (CASE WHEN ((0.5 <= t4.Noshow_professional_coeff) AND (t4.Noshow_professional_coeff <= 1)) 
                 THEN 4 
              ELSE (CASE WHEN ((0.33) <= t4.Noshow_professional_coeff) AND (t4.Noshow_professional_coeff < 0.5) 
                         THEN 3 
                     ELSE (CASE WHEN ((0.2) <= t4.Noshow_professional_coeff ) AND (t4.Noshow_professional_coeff  < (0.33))  
                                THEN 2 
                            ELSE (CASE WHEN t4.Noshow_professional_coeff < (0.2) 
                                       THEN 1 
                                    ELSE NULL END) END) END) END) END) as Coeff_noshow_professional,
       
   -- t4.sickness_coeff as Sickness,
   (CASE WHEN ((1 <= t4.sickness_coeff) AND (t4.sickness_coeff <= 2)) 
        THEN 5 
      ELSE (CASE WHEN (((0.33) <= t4.sickness_coeff) AND (t4.sickness_coeff < 1)) 
                 THEN 4 
              ELSE (CASE WHEN (((0.2) <= t4.sickness_coeff) AND (t4.sickness_coeff < (0.33))) 
                         THEN 3 
                     ELSE (CASE WHEN ((0.125 <= t4.sickness_coeff) AND (t4.sickness_coeff < 0.2))  
                                THEN 2 
                            ELSE (CASE WHEN (t4.sickness_coeff < 0.125) 
                                       THEN 1 
                                    ELSE NULL END) END) END) END) END) as Coeff_sickness             
        
FROM bi.temp_KPI_Performance t4

ORDER BY t4.polygons asc 
    ;
    
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------    
    
DROP TABLE IF EXISTS bi.temp_orders_terminated;
CREATE TABLE bi.temp_orders_terminated AS

-- Here I take all the cancelled terminated events that we had in the past

SELECT
  
  created_at::date as date_churn,
  event_name,
  order_Json->>'EffectiveDate' as order_startdate,
  order_Json->>'Locale__c' as Locale__c,
  order_Json->>'Order_Id__c' as Order_id,
  order_Json->>'Contact__c' as customer_id,
  order_Json->>'Recurrency__c' as recurrency,
  order_Json->>'Type' as ordertype,
  order_Json->>'Professional__c' as old_professional

FROM

  events.sodium

WHERE

  (event_name in ('Order Event:CANCELLED TERMINATED'))
  AND order_Json->>'Recurrency__c' > '0'
  AND (order_Json->>'EffectiveDate')::date < current_date
  
GROUP BY

  created_at,
  event_name,
  order_Json->>'Locale__c',
   order_Json->>'Order_Id__c',
   order_Json->>'Contact__c',
   order_Json->>'Recurrency__c',
   order_Json->>'Type',
   order_Json->>'Professional__c',
   order_Json->>'EffectiveDate'
  
ORDER BY

  order_Json->>'EffectiveDate' desc,
  order_Json->>'Contact__c' 
  ;
  
-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 


DROP TABLE IF EXISTS bi.churned_customers1;
CREATE TABLE bi.churned_customers1 AS

-- Here I take the table of the cancelled terminated events and merge it with bi.orders to see if the customers still have orders in the future or not
--  Then I take the date of the first cancelled terminated and consider it as the date of churn
-- It gives me the list of the customers with their date of churn

SELECT
  
  MIN(table3.date_churn) as date_churn,
  table3.event_name,
  table3.ordertype,
  table3.customer_id

FROM  
  
  (SELECT
  
    table2.date_churn,
    table2.event_name,
    table2.locale__c,
    table2.customer_id,
    table2.recurrency,
    table2.ordertype,
    o.polygon
  
  FROM
  
    
    (SELECT
    
      t3.*,
      table1.sfid_order_future
    
    FROM
    
      bi.temp_orders_terminated t3
      
    LEFT JOIN
    
      (SELECT
      
        t1.date_churn as date_churn,
        t1.customer_id,
        t1.order_startdate as start_old_order,
        t1.event_name as status_old_order,
        t1.recurrency as recurrency_plan,
        t1.ordertype,
        t1.old_professional as sfid_old_professional,
        t2.sfid as sfid_order_future,
        t2.effectivedate as date_order_future,
        t2.status as status_order_future,
        t2.professional__c as future_professional
      
      FROM
      
        bi.temp_orders_terminated t1
      
      LEFT JOIN
      
        bi.orders t2 
        
      ON 
      
        t1.customer_id = t2.contact__c
      
      WHERE 
      
        t2.error_note__c NOT LIKE '%Customer blocked%'
        AND t2.status IN ('PENDING TO START')
        AND t2.effectivedate >= current_date
        -- AND t1.professional IS NOT NULL
      
      GROUP BY 
      
        t1.date_churn,
        t1.customer_id,
        status_old_order,
        t1.order_startdate,
        t1.recurrency,
        t1.old_professional,
        t1.ordertype,
        t2.sfid,
        t2.effectivedate,
        t2.status,
        t2.professional__c
      
      ORDER BY 
      
        customer_id desc,
        start_old_order desc,
        date_order_future desc) table1
        
    ON 
    
      t3.customer_id = table1.customer_id
      
    WHERE 
    
      table1.sfid_order_future IS NULL
      
    ORDER BY
    
      t3.customer_id,
      t3.order_startdate desc) table2
      
  LEFT JOIN
  
    bi.orders o
    
  ON
  
    table2.customer_id = o.contact__c
    
  WHERE 
  
    o.polygon IS NOT NULL
    
  GROUP BY
  
    table2.date_churn,
    table2.event_name,
    table2.locale__c,
    table2.customer_id,
    table2.recurrency,
    table2.ordertype,
    o.polygon
    
  ORDER BY
  
    table2.date_churn desc,
    table2.event_name,
    table2.locale__c desc,
    o.polygon desc,
    table2.customer_id,
    table2.recurrency,
    table2.ordertype) table3
    
WHERE

  table3.ordertype IN ('cleaning-b2c', 'cleaning-b2b')
    
GROUP BY

  table3.event_name,
  table3.ordertype,
  table3.customer_id
  
ORDER BY

  date_churn,
  table3.customer_id
  ;


-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 

DROP TABLE IF EXISTS bi.last_invoiced;
CREATE TABLE bi.last_invoiced AS

-- This table display the list of our customers with their last registerd INVOICED order as well as their last cleaner

SELECT
  
  table4.*,
  oo.type,
  oo.professional__c

FROM

  (SELECT
  
    MAX(o.effectivedate) date_last_invoiced,
    o.contact__c,
    o.polygon
    
  FROM
  
    bi.orders o
    
  WHERE
  
    o.status IN ('INVOICED')
    AND o.effectivedate > '2015-12-31'
    AND o.polygon IS NOT NULL
    AND o.contact__c IS NOT NULL 
    AND o.professional__c IS NOT NULL
    AND o.type IN ('cleaning-b2b', 'cleaning-b2c')
    
  GROUP BY
  
    o.contact__c,
    o.polygon
  
  ORDER BY
  
    date_last_invoiced desc,
    o.contact__c,
    o.polygon) table4
    
LEFT JOIN

  bi.orders oo
  
ON 

  table4.contact__c = oo.contact__c
  AND table4.date_last_invoiced = oo.effectivedate
  AND table4.polygon = oo.polygon
  
WHERE

  oo.polygon IS NOT NULL
  AND oo.contact__c IS NOT NULL 
  AND oo.professional__c IS NOT NULL
  AND oo.type IN ('cleaning-b2b', 'cleaning-b2c')
  
GROUP BY

  table4.date_last_invoiced,
  table4.contact__c,
  table4.polygon,
  oo.type,
  oo.professional__c
  
ORDER BY

  table4.date_last_invoiced desc,
  table4.contact__c,
  table4.polygon,
  oo.type,
  oo.professional__c
  ;
  
-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 


DROP TABLE IF EXISTS bi.list_churned_customers1;
CREATE TABLE bi.list_churned_customers1 AS

-- I merged the 2 last tables to have the list of churned customers with their last INVOICED order and last cleaner before churn

SELECT

  table1.*,
  oo.sfid as sfid_last_invoiced_orders

FROM

  (SELECT
  
    o.date_churn,
    o.event_name,
    o.ordertype,
    o.customer_id,
    i.date_last_invoiced,
    i.polygon,
    i.professional__c as last_cleaner
    
  FROM
  
    bi.churned_customers1 o
    
  LEFT JOIN
  
    bi.last_invoiced i
      
  ON
  
    o.customer_id = i.contact__c    
    
  ORDER BY 
  
    date_churn desc) table1
    
LEFT JOIN

  bi.orders oo
  
ON

  table1.customer_id = oo.contact__c
  AND table1.date_last_invoiced = oo.effectivedate
  AND table1.polygon = oo.polygon
  AND table1.last_cleaner = oo.professional__c
  AND table1.ordertype = oo.type
  
ORDER BY

  date_churn desc,
  customer_id
  ;
  
-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
-----------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.type_orders_customers;
CREATE TABLE bi.type_orders_customers AS

-- Here I create this table to have the historical split of the orders for each of our customers

SELECT

  DISTINCT o.contact__c as customers,
  SUM(CASE WHEN o.status IN ('INVOICED') THEN 1 ELSE 0 END) as number_invoiced,
  SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as number_terminated,
  SUM(CASE WHEN o.status IN ('CANCELLED NO MANPOWER') THEN 1 ELSE 0 END) as number_nmp,
  SUM(CASE WHEN o.status IN ('CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as number_cancelled_pro,
  SUM(CASE WHEN o.status NOT IN ('INVOICED', 'CANCELLED TERMINATED', 'CANCELLED NO MANPOWER', 'CANCELLED PROFESSIONAL') THEN 1 ELSE 0 END) as number_others,
  COUNT(DISTINCT o.sfid) - SUM(CASE WHEN o.status IN ('CANCELLED TERMINATED') THEN 1 ELSE 0 END) as all_orders_wo_terminated, 
  COUNT(DISTINCT o.sfid) as tot_orders  
  
FROM

  bi.orders o
  
WHERE

  o.effectivedate < current_date
  AND o.contact__c IS NOT NULL
  
GROUP BY

  o.contact__c
  
ORDER BY 

  o.contact__c,
  number_invoiced desc
  ;

  
-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
  
  
DROP TABLE IF EXISTS bi.list_churned_customers;
CREATE TABLE bi.list_churned_customers AS 

-- To obtain the final version of the churned customers' table I link it to the historical split of the orders

SELECT

  t1.*,
  t2.number_invoiced,
  t2.number_terminated,
  t2.number_nmp,
  t2.number_cancelled_pro,
  t2.number_others,
  t2.all_orders_wo_terminated,
  t2.tot_orders
  
FROM

  bi.list_churned_customers1 t1
  
LEFT JOIN

  bi.type_orders_customers t2
  
ON 

  t1.customer_id = t2.customers;


-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 


DROP TABLE IF EXISTS bi.temp_orders_terminated;
DROP TABLE IF EXISTS bi.churned_customers1;
DROP TABLE IF EXISTS bi.list_churned_customers1;
DROP TABLE IF EXISTS bi.type_orders_customers;

    
-----------------------------------------------------------------------------------  
----------------------------------------------------------------------------------- 
----------------------------------------------------------------------------------- 
-----------------------------------------------------------------------------------   
    
    
DROP TABLE IF EXISTS bi.churn_trigger;
CREATE TABLE bi.churn_trigger AS  

SELECT

  a.sfid,
  table4.*

FROM

  salesforce.account a
  
LEFT JOIN

  (SELECT
  
    table3.*,
    CASE WHEN table3.ratio_churn < 0.05 THEN 5
        WHEN 0.05 <= table3.ratio_churn AND table3.ratio_churn < 0.1 THEN 4
        WHEN 0.1 <= table3.ratio_churn AND table3.ratio_churn < 0.3 THEN 3
        WHEN 0.3 <= table3.ratio_churn AND table3.ratio_churn < 0.5 THEN 2
        WHEN table3.ratio_churn >= 0.5 THEN 1
        ELSE 0
        END as power_trigger_churn
        
  FROM
  
    (SELECT
    
      table1.professional__c,
      table1.duration,
      table1.number_customers as number_customers_invoiced,
      table2.number_churns,
      CASE WHEN table2.number_churns IS NULL THEN 0
          WHEN (table2.number_churns/table1.number_customers::numeric)::numeric <= 1 THEN (table2.number_churns/table1.number_customers::numeric)::numeric 
          ELSE 1
          END as ratio_churn
    
    
    FROM
    
      (SELECT

        -- Here we have all the active cleaners of the last 90 days with the average duration of the orders and the number of customers they had
      
        o.professional__c,
        AVG(o.order_duration__c) as duration,
        COUNT(DISTINCT o.contact__c) as number_customers
        
      FROM  
      
        bi.orders o
        
      WHERE
      
        o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
        AND o.effectivedate::date < current_date
         AND o.effectivedate::date >= (current_date - interval '90 days')
        
      GROUP BY
      
        o.professional__c
        
      ORDER BY
      
        o.professional__c,
        number_customers desc) as table1
    
    LEFT JOIN
      
      (SELECT
      
        t1.last_cleaner,
        COUNT(DISTINCT t1.customer_id) as number_churns
        
      FROM
      
        bi.list_churned_customers t1
        
        
      WHERE 
      
        t1.last_cleaner IS NOT NULL
        
      GROUP BY
      
        t1.last_cleaner) as table2
        
    ON 
    
      table1.professional__c = table2.last_cleaner) as table3) as table4

ON 

  a.sfid = table4.professional__c
  
WHERE

  a.status__c IN ('ACTIVE', 'BETA')
  AND table4.professional__c IS NOT NULL;

      
    
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.Performance_cleaners;
CREATE TABLE bi.Performance_cleaners AS

SELECT
     
   current_date::date as date_today,      
   t5.sfid,
   t5.polygons,
   t5.cleaners,
   t5.UR,
   t5.rating,
   -- t5.Terminated,
   -- t5.Coeff_terminated,
   -- t5.CancelledPro,
   t5.Coeff_cancelled_pro,
   -- t5.Noshow, 
   t5.Coeff_noshow_professional,
   -- t5.Sickness,
   t5.Coeff_sickness,
   t.power_trigger_churn as Coeff_power_churn,
    
   (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(4 as decimal)::numeric) as decimal), 1) END) as Score_cleaners,
   
   CASE WHEN (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) > 4 THEN 'A' -- Here we define how the letter is assigned to the cleaners according to there score
        WHEN (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) > 3.5 AND (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) <= 4 THEN 'B'
        WHEN (CASE WHEN ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) IS NULL THEN t5.rating ELSE ROUND(CAST(((t5.rating+t5.Coeff_cancelled_pro+t5.Coeff_noshow_professional+t5.Coeff_sickness+t.power_trigger_churn)/CAST(5 as decimal)::numeric) as decimal), 1) END) <= 3.5 THEN 'C'
        ELSE 'ERROR'
   END as category,
   
   CASE WHEN a.hr_contract_end__c > current_date 
        THEN 
            CASE WHEN date_part('month', age(current_date, a.hr_contract_start__c)) IS NULL 
            THEN 0 
            ELSE date_part('month', age(current_date, a.hr_contract_start__c)) 
            END
        ELSE 
            CASE WHEN date_part('month', age(a.hr_contract_end__c, a.hr_contract_start__c)) IS NULL 
            THEN 0 
            ELSE date_part('month', age(a.hr_contract_end__c, a.hr_contract_start__c)) 
            END
        END
        as existence
 
        
FROM bi.temp_KPI_Performance2 t5

LEFT JOIN

  bi.churn_trigger t
  
ON

  (t5.sfid = t.sfid)

LEFT JOIN

   Salesforce.account a
   
ON 

   (t5.sfid = a.sfid)

ORDER BY t5.polygons asc;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


    
DELETE FROM bi.History_performance_cleaners WHERE date_today = current_date::date;
   
INSERT INTO bi.History_performance_cleaners 
   
(SELECT
     
   *
  
FROM

  bi.Performance_cleaners);


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


DELETE FROM bi.history_clusters_category WHERE mindate = current_date::date;
INSERT INTO bi.history_clusters_category 

SELECT

  EXTRACT(YEAR FROM date_today) as year,
  EXTRACT(WEEK FROM date_today) as week,
  date_today as mindate,
  polygons,
  AVG(coeff_cancelled_pro) as avg_coeff_cancelled_pro,
  AVG(coeff_noshow_professional) as avg_coeff_noshow_pro,
  AVG(coeff_sickness) as avg_coeff_sickness,
  AVG(coeff_power_churn) as avg_power_churn,
  AVG(score_cleaners) as avg_score_cleaners,
  AVG(rating) as avg_ratings,
  AVG(ur) as avg_ur,
  SUM(CASE WHEN category = 'A' THEN 1 ELSE 0 END) as number_a,
  SUM(CASE WHEN category = 'A' THEN 1 ELSE 0 END)/COUNT(category)::numeric as share_a,
  SUM(CASE WHEN category = 'B' THEN 1 ELSE 0 END) as number_b,
  SUM(CASE WHEN category = 'B' THEN 1 ELSE 0 END)/COUNT(category)::numeric as share_b,
  SUM(CASE WHEN category = 'C' THEN 1 ELSE 0 END) as number_c,
  SUM(CASE WHEN category = 'C' THEN 1 ELSE 0 END)/COUNT(category)::numeric as share_c
  
FROM 

  bi.History_performance_cleaners h
  
WHERE

  -- polygons = 'de-berlin'
  date_today = current_date
  
GROUP BY

  year,
  week,
  date_today,
  polygons
  
ORDER BY 

  year,
  week,
  date_today,
  polygons;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.rating_weekdays;
CREATE TABLE bi.rating_weekdays as

SELECT 

  EXTRACT(YEAR FROM o.effectivedate) as year,
  EXTRACT(MONTH FROM o.effectivedate) as month, 
  TO_CHAR(o.effectivedate, 'day')::text as day_of_week,
  o.effectivedate,
  o.rating_professional__c, 
  o.polygon as city,
  UPPER(LEFT(o.polygon, 2)) as country
  
FROM
  
  bi.orders o
  
JOIN

  salesforce.account acc
  
ON o.professional__c = acc.sfid 
  
WHERE 
  
  o.test__c = '0'
  AND o.status IN ('INVOICED','FULFILLED')
  AND o.effectivedate > '2015-12-31'
  AND o.polygon IS NOT NULL
  
ORDER BY 
  
  year,
  month,
  effectivedate, 
  city;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS OWM_w_Muffins_Names;
CREATE TABLE OWM_w_Muffins_Names as

SELECT 

  o.id as "orderID",
  acc.status__c,
  acc.training_01__c,
  acc.training_02__c,
  o.status, 
  o.effectivedate,
  o.rating_professional__c, 
  o.polygon as city, 
  o.professional__c,
  acc.name,
  acc.email__c,
  acc.hr_contract_start__c::timestamp::date
  
FROM

  bi.orders o

JOIN
  salesforce.account acc
    ON o.professional__c = acc.sfid 

WHERE 
  
  o.test__c = '0'
  AND o.status IN ('INVOICED','CANCELLED PROFESSIONAL','CANCELLED PROFESSIONAL SHORTTERM','NOSHOW PROFESSIONAL', 'FULFILLED')
  AND o.effectivedate > '2015-12-31'

ORDER BY 

  acc.name asc,
  o.effectivedate desc;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.supply_management;
CREATE TABLE bi.supply_management as

SELECT

  UPPER(LEFT(p.locale__c, 2)) as country,
  p.servicedate__c as service_date,
  p.status__c as status_order,
  p.name as type_product,
  p.quantity__c as quantity,
  p.total_amount__c as total_amount
  


FROM

  salesforce.productlineitem__c p
  
ORDER BY

  p.servicedate__c;


   
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------   
-- Author: Chandrasen
-- Created on: 26.02.2018
-- Edited on: 13.03.2018 (check source control to see the changes)
-- Description: This query was used to get the data which is required to create 'cleaners profitability traffic light'

DROP TABLE IF EXISTS bi.cleaners_profitability;
CREATE TABLE bi.cleaners_profitability as

SELECT 	t2.*,
	ROUND(AVG(gpm_traffic_light) OVER (PARTITION BY professional__c), 0) AS gpm_traffic_light_avg_round0,
	ROUND(AVG(gpm_traffic_light) OVER (PARTITION BY professional__c), 2) AS gpm_traffic_light_avg_round2,
	CASE
		WHEN t2.contract_end <= now()::date THEN 'Left'
		ELSE 'Still Working'
	END AS current_status,
	SUM(CASE
		WHEN t2.year_month::date = (to_date(to_char(now(), 'YYYY-MM'), 'YYYY-MM') - interval '1 month')::date
		THEN t2.gpm_traffic_light ELSE NULL END)
		OVER (PARTITION BY professional__c)
	AS latest_traffic_light
FROM
(
	SELECT 	t1.*,
		CASE
			WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 2) OR (time_on_platform <= 3 AND gpm_less_than_10_count = 3) THEN 1
			WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 1) THEN 2
			WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 0) THEN 3
			ElSE 0
		END AS rule_1_tl_system,
		CASE
			WHEN (time_on_platform > 3 AND gpm_less_than_10_count >= 3)THEN 1
			WHEN (time_on_platform > 3 AND gpm_less_than_10_count = 2) THEN 2
			WHEN (time_on_platform > 3 AND gpm_less_than_10_count < 2) THEN 3
			ElSE 0
		END AS rule_2_tl_system,
		CASE 
			WHEN 
				(CASE
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 2) OR (time_on_platform <= 3 AND gpm_less_than_10_count = 3) THEN 1
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 1) THEN 2
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 0) THEN 3
					ElSE 0
				END) > 0 
				THEN 
				(CASE
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 2) OR (time_on_platform <= 3 AND gpm_less_than_10_count = 3) THEN 1
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 1) THEN 2
					WHEN (time_on_platform <= 3 AND gpm_less_than_10_count = 0) THEN 3
					ElSE 0
				END)
			ELSE 	
				(CASE
					WHEN (time_on_platform > 3 AND gpm_less_than_10_count >= 3)THEN 1
					WHEN (time_on_platform > 3 AND gpm_less_than_10_count = 2) THEN 2
					WHEN (time_on_platform > 3 AND gpm_less_than_10_count < 2) THEN 3
					ElSE 0
				END)
		END AS gpm_traffic_light
	FROM
	(
		SELECT 	t.*,
				CASE
					WHEN gross_profit_abs = 0 OR revenue = 0 THEN 0
					ELSE gross_profit_abs/revenue 
				END AS gpm_percent,
				CASE
					WHEN 
						(CASE
							WHEN gross_profit_abs = 0 OR revenue = 0 THEN 0
							ELSE gross_profit_abs/revenue 
						END) < 0.10 THEN 1 ELSE 0
				END AS gpm_less_than_10_perc,
				
				SUM(CASE
					WHEN 
						(CASE
							WHEN gross_profit_abs = 0 OR revenue = 0 THEN 0
							ELSE gross_profit_abs/revenue 
						END) < 0.10 THEN 1 ELSE 0
				END) OVER (PARTITION BY professional__c ORDER BY year_month) AS gpm_less_than_10_count
		FROM 
		(
		SELECT	mpc.name,
					mpc.type__c,
					to_date(mpc.year_month, 'YYYY-MM-DD') as year_month,
					mpc.mindate,
					mpc.professional__c,
					emp_id.emp_id,
					mpc.delivery_area,
					mpc.worked_hours,
					mpc.contract_start,
					to_date(to_char(mpc.contract_start, 'YYYY-MM'), 'YYYY-MM') as contract_start_month,
					mpc.contract_end,
					to_date(to_char(mpc.contract_end, 'YYYY-MM'), 'YYYY-MM') as contract_end_month,
					mpc.gp,
					mpc.revenue,
					mpc.salary_payed,
					CASE
						WHEN mpc.delivery_area = 'at-vienna' THEN mpc.gp ELSE (mpc.revenue - mpc.salary_payed)
					END AS gross_profit_abs,
					((to_date(mpc.year_month, 'YYYY-MM-DD') - to_date(to_char(mpc.contract_start, 'YYYY-MM'), 'YYYY-MM'))/30)+1 AS time_on_platform
					
		FROM bi.margin_per_cleaner mpc
		LEFT JOIN
				LATERAL	(
						SELECT  hr_employee_number__c AS emp_id
						FROM salesforce.account a
						WHERE a.sfid = mpc.professional__c
							-- AND hr_employee_number__c is not null
				) emp_id
		ON TRUE
			WHERE to_date(mpc.year_month, 'YYYY-MM-DD') >= now()::date - interval '7 months'
		) t
	) t1
) t2;
		


----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------- 
   
DROP TABLE IF EXISTS bi.temp_cancels;   
DROP TABLE IF EXISTS bi.temp_results;   
DROP TABLE IF EXISTS bi.temp_KPI_Performance;   
DROP TABLE IF EXISTS bi.temp_KPI_Performance2;   
   

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'