DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_to_cleaner_availability(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_to_cleaner_availability';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN

-- job calendar Current

DROP TABLE IF EXISTS bi.muffin_order_distribution;
CREATE TABLE bi.muffin_order_distribution as 
SELECT
	o.City,
	o.contact__c,
	o.professional__c,
	acc.sfid,
	-- Chandra on 2018-03-19: this is a flag to know BAT cleaners and Partner cleaner
	CASE
		WHEN o.professional__c IS NOT NULL AND acc.sfid IS NOT NULL THEN 'Our Cleaner'
		WHEN o.professional__c IS NOT NULL AND acc.sfid IS NULL THEN 'Partner Cleaner'
		WHEN o.professional__c IS  NULL AND acc.sfid IS NULL THEN 'ID Unavailable'
	END AS cleaners_flag,
	status,
	order_type,
	LEFT(o.Locale__c,2) as locale,
	LEFT(o.ShippingPostalCode,3) as zipcode,
	o.Effectivedate::date as date,
	EXTRACT(DOW FROM o.Effectivedate) as weekday,
	EXTRACT(HOUR FROM o.Order_Start__c)+2 as Start_time,
	EXTRACT(HOUR FROM o.Order_End__c)+2 as End_time
FROM
	bi.orders o
-- Chandra on 2018-03-19: This table link will give the list of the cleaners BAT that we have on the platform, all of them
LEFT JOIN
		LATERAL(
				SELECT
					t1.*,
					t2.name as subcon
				FROM Salesforce.Account t1
					JOIN Salesforce.Account t2
				ON (t2.sfid = t1.parentid)
				WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
					and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
					and t2.name LIKE '%BAT Business Services GmbH%'
	) as acc
	ON acc.sfid = o.professional__c
WHERE
	-- Effectivedate::date between '2016-08-15'::date and current_date::date
	-- Chandra 2018-03-19: filter to get orders until the current week instead of current_date as stated above
	-- ((date_part('year'::text, Effectivedate) || '-'::text) || date_part('week'::text, Effectivedate)) between '2016' || '-'::text || extract(week from '2016-08-15'::date) and ((date_part('year'::text, now()) || '-'::text) || date_part('week'::text, now()))
	o.Effectivedate::date >= current_date::date - interval '1 month'
	and o.test__c = '0'
	-- Chandra on 2018-03-19: new addition to filter to only include the orders with the below status (the same filter has been used to calculate gmv_eur_net column in bi.orders view)
	and o.status IN ('ALLOCATION PAUSED', 'FULFILLED','INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','WAITING CONFIRMAITION','WAITING FOR ACCEPTANCE','WAITING FOR RESCHEDULE');


DROP TABLE IF EXISTS bi.muffin_order_distribution_v2;
CREATE TABLE bi.muffin_order_distribution_v2 as 
SELECT
	*
FROM
	bi.muffin_order_distribution,
	unnest(array[0,1,2,3,4,5,6]) as day,
	unnest(array[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) as hour;

DROP TABLE IF EXISTS bi.muffin_order_distribution_v3;
CREATE TABLE bi.muffin_order_distribution_v3 as 	
SELECT
	city,
	Status,
	cleaners_flag,
	order_type,
	locale,
	date::date order_date,
	EXTRACT(WEEK FROM date::date) as CW,
	EXTRACT(DOW FROM date::date) as Weekday,
	hour,
	SUM(CASE WHEN EXTRACT(DOW FROM date) = DAY AND HOUR >= bi.muffin_order_distribution_v2.Start_time and hour < bi.muffin_order_distribution_v2.End_time THEN 1 ELSE 0 END) as Orders
FROM
	bi.muffin_order_distribution_v2
GROUP BY
	CW,
	zipcode,
	Status,
	cleaners_flag,
	order_type,
	locale,
	weekday,
	order_date,
	city,
	hour;

DROP TABLE IF EXISTS bi.workcalendar;
DROP TABLE IF EXISTS bi.utalization_temp12;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.overlapping_orders;
CREATE TABLE bi.overlapping_orders AS

SELECT
	
	current_date as date_today,
	t1.sfid as sf_orderid1,
	t2.sfid as sf_orderid2,
	t1.effectivedate::date as orderdate,
	t1.polygon,
	t1.professional__c as professional,
	t1.contact__c as contact1,
	t2.contact__c as contact2,
	t1.order_id__c as order_id1,
	t1.type as type1,
	t2.order_id__c as order_id2,
	t2.type as type2,
	t1.order_time__c as order_time1,
	t2.order_time__c as order_time2,
	t1.order_duration__c as order_duration1,
	t2.order_duration__c as order_duration2

FROM

	bi.orders t1

LEFT JOIN

	bi.orders t2

ON

	(t1.professional__c = t2.professional__c AND t1.effectivedate::date = t2.effectivedate::date AND t1.order_id__c != t2.order_id__c)

WHERE

	t1.effectivedate::date > current_date + 2
	AND t1.effectivedate::date < current_date + 16
	AND t1.status NOT LIKE '%CANCELLED%'
	AND t1.order_time__c < t2.order_time__c

GROUP BY

	date_today,
	t1.sfid,
	t1.effectivedate::date,
	t2.sfid,
	t1.polygon,
	t1.contact__c,
	t2.contact__c,
	t1.professional__c,
	t1.order_id__c,
	t1.type,
	t2.order_id__c,
	t2.type,
	t1.order_time__c,
	t2.order_time__c,
	t1.order_duration__c,
	t2.order_duration__c

HAVING

	(EXTRACT(HOUR FROM t2.order_time__c::time) - EXTRACT(HOUR FROM t1.order_time__c::time)+1) <= (t1.order_duration__c );


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DELETE FROM bi.history_overlapping_orders WHERE date_today = current_date;
INSERT INTO bi.history_overlapping_orders

SELECT
	
	current_date as date_today,
	t1.sfid as sf_orderid1,
	t2.sfid as sf_orderid2,
	t1.effectivedate::date as orderdate,
	t1.polygon,
	t1.professional__c as professional,
	t1.contact__c as contact1,
	t2.contact__c as contact2,
	t1.order_id__c as order_id1,
	t1.type as type1,
	t2.order_id__c as order_id2,
	t2.type as type2,
	t1.order_time__c as order_time1,
	t2.order_time__c as order_time2,
	t1.order_duration__c as order_duration1,
	t2.order_duration__c as order_duration2

FROM

	bi.orders t1

LEFT JOIN

	bi.orders t2

ON

	(t1.professional__c = t2.professional__c AND t1.effectivedate::date = t2.effectivedate::date AND t1.order_id__c != t2.order_id__c)

WHERE

	t1.effectivedate::date > current_date + 2
	AND t1.effectivedate::date < current_date + 16
	AND t1.status NOT LIKE '%CANCELLED%'
	AND t1.order_time__c < t2.order_time__c

GROUP BY

	t1.sfid,
	t1.effectivedate::date,
	t2.sfid,
	t1.polygon,
	t1.contact__c,
	t2.contact__c,
	t1.professional__c,
	t1.order_id__c,
	t1.type,
	t2.order_id__c,
	t2.type,
	t1.order_time__c,
	t2.order_time__c,
	t1.order_duration__c,
	t2.order_duration__c

HAVING

	(EXTRACT(HOUR FROM t2.order_time__c::time) - EXTRACT(HOUR FROM t1.order_time__c::time)+1) <= (t1.order_duration__c );
	
	
-- Author: Chandrasen
-- Date: 12/03/2018
-- Description: This query creates the table which returns the cleaners actual availability in the current week

DROP TABLE IF EXISTS bi.cleaners_actual_availability;
CREATE TABLE bi.cleaners_actual_availability AS 
 
 SELECT 	jobs_available.*,
 			jobs_booked.jobs_booked,
 			jobs_available.jobs_available - jobs_booked.jobs_booked AS actual_availability,
 			jobs_booked.city as city_booked,	
 			jobs_booked.hour as hour_booked,
 			jobs_booked.locale as locale_booked,
 			jobs_booked.type__c as type__c_booked,
 			jobs_booked.order_start_date
 FROM
 (
 SELECT availabilitymuffin_temp1.city,
    availabilitymuffin_temp1.date,
    availabilitymuffin_temp1.locale,
    availabilitymuffin_temp1.type__c,
    availabilitymuffin_temp1.hour,
    date_part('dow'::text, availabilitymuffin_temp1.date) AS dow_availability,
    sum(
        CASE
            WHEN (((date_part('dow'::text, availabilitymuffin_temp1.date) = (availabilitymuffin_temp1.day)::double precision) AND ((availabilitymuffin_temp1.hour)::double precision >= date_part('hour'::text, availabilitymuffin_temp1.start_availability))) AND ((availabilitymuffin_temp1.hour)::double precision < date_part('hour'::text, availabilitymuffin_temp1.end_availability))) THEN 1
            ELSE 0
        END) AS jobs_available
   FROM ( SELECT availability.sfid,
            availability.city,
            availability.status__c,
            availability.locale,
            availability.type__c,
            availability.date,
            availability.start_availability,
            availability.end_availability,
            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6]) AS day,
            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) AS hour
           FROM ( SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-02'::date AS date,
                    (split_part((account.availability_monday__c)::text, '-'::text, 1))::time without time zone AS start_availability,
                    (split_part((account.availability_monday__c)::text, '-'::text, 2))::time without time zone AS end_availability
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) 
						AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
						AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-03'::date AS day,
                    (split_part((account.availability_tuesday__c)::text, '-'::text, 1))::time without time zone AS start_availability,
                    (split_part((account.availability_tuesday__c)::text, '-'::text, 2))::time without time zone AS end_availability
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) 
						AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
						AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-04'::date AS day,
                    (split_part((account.availability_wednesday__c)::text, '-'::text, 1))::time without time zone AS start_availability_wed,
                    (split_part((account.availability_wednesday__c)::text, '-'::text, 2))::time without time zone AS end_availability_wed
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) 
						AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
						AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-05'::date AS day,
                    (split_part((account.availability_thursday__c)::text, '-'::text, 1))::time without time zone AS start_availability_thu,
                    (split_part((account.availability_thursday__c)::text, '-'::text, 2))::time without time zone AS end_availability_thu
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) 
						AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
						AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-06'::date AS day,
                    (split_part((account.availability_friday__c)::text, '-'::text, 1))::time without time zone AS start_availability_fr,
                    (split_part((account.availability_friday__c)::text, '-'::text, 2))::time without time zone AS end_availability_fr
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) 
						AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
						AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
                UNION
                 SELECT account.sfid,
                    account.delivery_areas__c AS city,
                    account.status__c,
                    "left"((account.locale__c)::text, 2) AS locale,
                    account.type__c,
                    '2015-11-07'::date AS day,
                    (split_part((account.availability_saturday__c)::text, '-'::text, 1))::time without time zone AS start_availability_sa,
                    (split_part((account.availability_saturday__c)::text, '-'::text, 2))::time without time zone AS end_availability_sa
                   FROM salesforce.account
                  WHERE (((account.test__c <> true) 
						AND ((account.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
						AND ((((account.company_name__c)::text ~~ 'BAT%'::text) OR ((account.company_name__c)::text ~~ 'Book%'::text)) OR (account.company_name__c IS NULL)))
					) availability
				) availabilitymuffin_temp1		
  GROUP BY availabilitymuffin_temp1.city, availabilitymuffin_temp1.locale, availabilitymuffin_temp1.type__c, availabilitymuffin_temp1.date, availabilitymuffin_temp1.hour
) jobs_available 
LEFT JOIN
	LATERAL	(
				 SELECT *
				 FROM
				 (
				 SELECT ordermuffin_temp1.city,
				    ordermuffin_temp1.order_start_date,
				    ordermuffin_temp1.locale,
				    ordermuffin_temp1.type__c,
				    ordermuffin_temp1.hour,
				    date_part('dow'::text, ordermuffin_temp1.order_start_date) AS dow,
				    sum(
				        CASE
				            WHEN (date_part('dow'::text, ordermuffin_temp1.order_start_date) = (ordermuffin_temp1.day)::double precision) AND (ordermuffin_temp1.hour)::double precision >= ordermuffin_temp1.order_start_hour AND (ordermuffin_temp1.hour)::double precision < ordermuffin_temp1.order_end_hour THEN 1
				            ELSE 0
				        END) AS jobs_booked
				   FROM ( SELECT t.professional__c,
				            t.ordernumber,
				            "left"((t.locale__c)::text, 2) AS locale,
				            t.delivery_area__c AS city,
				            a.type__c,
				            t.order_start__c,
				            date(t.order_start__c) AS order_start_date,
				            (t.order_start__c)::time without time zone AS order_start_time,
				            (date_part('hour'::text, t.order_start__c)::double precision) + 2 AS order_start_hour,
				            date_part('dow'::text, date(t.order_start__c)) AS order_dow,
				            t.order_end__c,
				            date(t.order_end__c) AS order_end_date,
				            (t.order_end__c)::time without time zone AS order_end_time,
				            (date_part('hour'::text, t.order_end__c)::double precision) + 2 AS order_end_hour,
				            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6]) AS day,
				            unnest(ARRAY[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) AS hour
				           FROM (salesforce."order" t
				             LEFT JOIN salesforce.account a ON (((t.professional__c)::text = (a.sfid)::text)))
				         -- filter to get all the orders specific to current week 
				          WHERE ((((((date_part('year'::text, t.order_start__c) || '-'::text) || date_part('week'::text, t.order_start__c)) = ((date_part('year'::text, now()) || '-'::text) || date_part('week'::text, now()))) 
				          -- filter to exclude all rows with test__c = true
							 AND (a.test__c <> true))  
							 -- filter to include only 'BEAT' and 'ACTIVE'
							 AND ((a.status__c)::text = ANY (ARRAY[('ACTIVE'::character varying)::text, ('BETA'::character varying)::text]))) 
							 -- filter to include only book a tiger cleaners
							 AND ((((a.company_name__c)::text ~~ 'BAT%'::text) OR ((a.company_name__c)::text ~~ 'Book%'::text)) OR (a.company_name__c IS NULL)))
							 -- Chandra on 2018-03-19: new addition to filter to only include the orders with the below status (the same filter has been used to calculate gmv_eur_net column in bi.orders view)
							 AND t.status IN ('ALLOCATION PAUSED', 'FULFILLED','INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','PENDING ALLOCATION','PENDING TO START','WAITING CONFIRMAITION','WAITING FOR ACCEPTANCE','WAITING FOR RESCHEDULE')
				          ORDER BY t.order_start__c DESC, date_part('dow'::text, date(t.order_start__c))) ordermuffin_temp1
				  GROUP BY ordermuffin_temp1.city, ordermuffin_temp1.locale, ordermuffin_temp1.type__c, ordermuffin_temp1.order_start_date, ordermuffin_temp1.hour
				) _inn
				WHERE _inn.hour = jobs_available.hour	
					AND _inn.city = jobs_available.city	
					AND _inn.locale = jobs_available.locale	
					AND _inn.dow = jobs_available.dow_availability	
					AND _inn.type__c = jobs_available.type__c				
	) jobs_booked
ON TRUE;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------




end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'