/*
Procedure used to generate cleaners' return rates and activity rates cohorts !
*/
CREATE OR REPLACE FUNCTION bi.sfunc_to_cleaners_cohorts(crunchdate date) RETURNS void AS 
--CREATE OR REPLACE FUNCTION bi.daily$cleanerscohortstableau (crunchdate date) RETURNS void AS 
$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_to_cleaners_cohorts';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


DROP TABLE IF EXISTS bi.cohort_cleaners;
CREATE TABLE bi.cohort_cleaners AS

SELECT
	s.sfid,
	o.delivery_area as city,
	case when o.type = 'cleaning-b2c' or o.type = 'cleaning-b2b' then 'Employed' else 'Freelance' end as cleaners_type,
	case when (s.hr_contract_start__c is not null and (o.type = 'cleaning-b2c' or o.type = 'cleaning-b2b')) then s.hr_contract_start__c::timestamp::date else s.createddate::timestamp::date end as contract_start_date,
	o.effectivedate as order_date,
	o.order_duration__c as order_duration,
	left(o.locale__c,2) as country,
	s.status__c as cleaner_status,
	ending_reason__c,
	o.gmv_eur_net 
FROM
	bi.orders o
JOIN
	salesforce.account s
		ON s.sfid = o.professional__c

WHERE
	o.test__c = '0'
	and s.test__c = '0'
	and o.status in ('INVOICED')
	and s.type__c not in ('cleaning-b2b')

ORDER BY 
	contract_start_date asc,
	order_date asc

;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'