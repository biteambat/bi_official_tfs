DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_to_onboarding_kpis(crunchdate date) RETURNS void AS
--CREATE OR REPLACE FUNCTION bi.daily$onboarding_kpis(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_to_onboarding_kpis';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
------------------------------------------------------

------------------------------------------------


------------------------------------------------

DROP TABLE IF EXISTS bi.onboarding_kpis;
CREATE TABLE bi.onboarding_kpis AS

	SELECT

		EXTRACT(YEAR FROM to_date(t1.year_month,'YYYY-MM-DD'))::int AS year,
		EXTRACT(MONTH FROM to_date(t1.year_month,'YYYY-MM-DD'))::int AS month,
		t1.delivery_area::text as delivery_area,
		'Cleaners on platform'::text as kpi,
		SUM(CASE WHEN to_char(t1.contract_start,'YYYY-MM') <= t1.year_month and to_char(t1.contract_end,'YYYY-MM') >= t1.year_month THEN 1 ELSE 0 END) AS value

	FROM bi.margin_per_cleaner t1

	GROUP BY year, month, t1.delivery_area, kpi

	ORDER BY year desc, month desc, delivery_area asc, kpi asc

;

INSERT INTO bi.onboarding_kpis

	SELECT

		EXTRACT(YEAR FROM a.hr_contract_start__c) AS year,
		EXTRACT(MONTH FROM a.hr_contract_start__c) AS month,
		a.delivery_areas__c AS delivery_area,
		'Actual onboardings'::text AS kpi,
		COUNT(DISTINCT a.sfid) AS value

	FROM salesforce.account a

	WHERE a.status__c in ('ACTIVE','BETA') AND hr_contract_start__c IS NOT NULL and type__c != 'cleaning-b2b'

	GROUP BY year, month, delivery_area, kpi

	ORDER BY year desc, month desc, delivery_area asc, kpi asc

;


INSERT INTO bi.onboarding_kpis

	SELECT

		year AS year,
		month AS month,
		delivery_area AS delivery_area,
		'Onboarding target'::text AS kpi,
		onboarding_target AS value

	FROM bi.onboarding_targets

;


-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.Potential_Onboardings;
CREATE TABLE bi.Potential_Onboardings AS


	SELECT

		t4.citya as Polygon1,
		t4.cop as Professionals_platform,
		t3.Potential_onboardings,
		t3.Ending_Last_30days,
		t3.Ending_Next_30days,
		t3.Starting_Last_30days,
		t3.Starting_Next_30days,
		t3.Netto_60days
	
	FROM
	
		(SELECT
		
		Polygon1,
		Professionals_platform,
		Potential_onboardings,
		Ending_Last_30days,
		Ending_Next_30days,
		Starting_Last_30days,
		Starting_Next_30days,
		Netto_60days
		
		FROM
		
			(SELECT
		
		        a.delivery_area__c as Polygon1,
		        SUM(CASE WHEN a.status IN ('Interview scheduled', 'Waiting for contract', 'Documents after interview', 'Contract ready', 'Waiting for greenlight') THEN 1 ELSE 0 END) as Potential_onboardings
		
		    FROM salesforce.lead a
		
		    WHERE 
		
		        a.lastmodifieddate > current_date - 30
		        AND a.delivery_area__c NOT IN ('de-potsdam', 'de-dortmund', 'de-duisburg', 'de-leipzig', 'de-manheim', 'de-bonn;de-cologne', 'de-dusseldorf;de-cologne')
		
		    GROUP BY
		    
		        a.delivery_area__c
		    
		    ORDER BY
		
		        a.delivery_area__c asc) as t1
		        
		LEFT JOIN
		
		    (SELECT
		    
		        b.delivery_areas__c as Polygon2,
		
		        SUM(CASE WHEN hr_contract_end__c::date >= current_date::date - 30 AND hr_contract_end__c::date <= current_date::date THEN 1 ELSE 0 END) as Ending_Last_30days,
		        SUM(CASE WHEN hr_contract_end__c::date <= current_date::date + 30 AND hr_contract_end__c::date > current_date::date THEN 1 ELSE 0 END) as Ending_Next_30days,
		        SUM(CASE WHEN hr_contract_start__c::date >= current_date::date - 30 AND hr_contract_start__c::date <= current_date::date THEN 1 ELSE 0 END) as Starting_Last_30days,  
		        SUM(CASE WHEN hr_contract_start__c::date <= current_date::date + 30 AND hr_contract_start__c::date > current_date::date THEN 1 ELSE 0 END) as Starting_Next_30days,  
		        (SUM(CASE WHEN hr_contract_start__c::date >= current_date::date - 30 AND hr_contract_start__c::date <= current_date::date THEN 1 ELSE 0 END) + SUM(CASE WHEN hr_contract_start__c::date <= current_date::date + 30 AND hr_contract_start__c::date > current_date::date THEN 1 ELSE 0 END) - SUM(CASE WHEN hr_contract_end__c::date >= current_date::date - 30 AND hr_contract_end__c::date <= current_date::date THEN 1 ELSE 0 END) - SUM(CASE WHEN hr_contract_end__c::date <= current_date::date + 30 AND hr_contract_end__c::date > current_date::date THEN 1 ELSE 0 END)) as Netto_60days,
		        COUNT(DISTINCT CASE WHEN (b.status__c IN ('BETA', 'ACTIVE')) AND (hr_contract_start__c <= current_date::date OR delivery_areas__c IN ('at-vienna')) THEN b.sfid ELSE NULL END)  as Professionals_platform
		
		    FROM salesforce.account b
		
		    GROUP BY
		
		      b.delivery_areas__c
		    
		    ORDER BY
		    
		      b.delivery_areas__c asc) as t2
		        
		ON
		
		    (t1.Polygon1 = t2.Polygon2)) as t3
		    
	LEFT JOIN
	
		(SELECT
		
			EXTRACT(MONTH FROM max_date::date) as Month,
			EXTRACT(YEAR FROM max_date::date) as Year,
			MIN(max_date::date) as mindate,
			delivery_areas__c as citya,
			CAST('COP Cleaner' as text) as kpi,
			CAST('-' as text) as breakdown,
			COUNT(DISTINCT(CASE WHEN (hr_contract_start__c <= max_date or hr_contract_start__c is null) and max_date < hr_contract_end__c THEN sfid ELSE null END)) as cop
			
		FROM
		
		(SELECT
			TO_CHAR(date,'YYYY-MM') as Month,
			MAX(Date) as max_date
		FROM
			(select i::date as date from generate_series('2016-01-01', 
		  current_date::date, '1 day'::interval) i) as dateta
		  GROUP BY
		 	Month) as a,
		 	(SELECT
		 		a.*
		 	FROM
		   Salesforce.Account a
		    JOIN
		        Salesforce.Account t2
		    ON
		        (t2.sfid = a.parentid)
		
		    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
			and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%') or a.type__c like 'cleaning-b2b')) as b
			
		 GROUP BY
		 	Year,
		 	Month,
		 	max_date::date,
		 	delivery_areas__c) as t4
		 	
	ON
	
		(t3.Polygon1 = t4.citya) 
		
	WHERE
	
		
		t3.Polygon1 = t4.citya
		AND t4.mindate = current_date;


-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------


-- ---------------------------------------------------------------------------         HERE BEGINS THE BIG PIECE OF CODE CREATING THE TABLE FOR THE AUTOMATED HIRING PLAN       -------------------------------------------------------------






-- Creation of the table for the invoiced hours in B2C in the lase 4 months (120 days)

DROP TABLE IF EXISTS bi.hours_invoiced_b2c;
CREATE TABLE bi.hours_invoiced_b2c as 

SELECT
	
	t1.polygon,
	t1.type,
	AVG(t1.hours_invoiced) as average_monthly_hours_b2c

FROM

	(SELECT
	
		EXTRACT(YEAR FROM o.effectivedate) as year,
		EXTRACT(MONTH FROM o.effectivedate) as month,
		o.polygon,
		o.type,
		SUM(o.order_duration__c) as hours_invoiced
		
	FROM
	
		bi.orders o
		
	WHERE
	
		o.effectivedate > current_date - 120
		AND o.effectivedate < current_date
		AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'FULFILLED')
		AND o.type NOT LIKE '%training%'
		AND o.type IN ('cleaning-b2c')
		
	GROUP BY
	
		year,
		month,
		o.polygon,
		o.type
		
	ORDER BY
	
		year desc,
		month desc,
		o.polygon,
		o.type) as t1
		
WHERE

	t1.month <= EXTRACT(MONTH FROM current_date)
		
GROUP BY

	t1.polygon,
	t1.type
	
ORDER BY

	t1.polygon;
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of a small table containing only the sum of the average monthly hours b2b DE

DROP TABLE IF EXISTS bi.hours_tot_b2c_de;
CREATE TABLE bi.hours_tot_b2c_de as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2c) as tot_hours_b2c_de
	

FROM

	bi.hours_invoiced_b2c
	
WHERE

	LEFT(polygon, 2) = 'de';
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of a small table containing only the sum of the average monthly hours b2b CH

DROP TABLE IF EXISTS bi.hours_tot_b2c_ch;
CREATE TABLE bi.hours_tot_b2c_ch as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2c) as tot_hours_b2c_ch
	

FROM

	bi.hours_invoiced_b2c
	
WHERE

	LEFT(polygon, 2) = 'ch';
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of a small table containing only the sum of the average monthly hours b2c NL

DROP TABLE IF EXISTS bi.hours_tot_b2c_nl;
CREATE TABLE bi.hours_tot_b2c_nl as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2c) as tot_hours_b2c_nl
	

FROM

	bi.hours_invoiced_b2c
	
WHERE

	LEFT(polygon, 2) = 'nl';

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of the table containing the average monthly hours b2c and the share for every polygon

DROP TABLE IF EXISTS bi.hours_invoiced__b2c;
CREATE TABLE bi.hours_invoiced__b2c as 

SELECT

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2c,
	CASE WHEN LEFT(polygon, 2) = 'de' THEN t1.average_monthly_hours_b2c/t2.tot_hours_b2c_de 
	     WHEN LEFT(polygon, 2) = 'nl' THEN t1.average_monthly_hours_b2c/t3.tot_hours_b2c_nl 
		  WHEN LEFT(polygon, 2) = 'ch' THEN t1.average_monthly_hours_b2c/t4.tot_hours_b2c_ch
		  ELSE 0
		  END
		  as share_b2c

FROM

	bi.hours_invoiced_b2c t1,
	bi.hours_tot_b2c_de t2,
	bi.hours_tot_b2c_nl t3,
	bi.hours_tot_b2c_ch t4
	
GROUP BY

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2c,
	share_b2c;

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
	
	
-- Creation of the table for the invoiced hours in B2B in the lase 4 months (120 days)

DROP TABLE IF EXISTS bi.hours_invoiced_b2b;
CREATE TABLE bi.hours_invoiced_b2b as 

SELECT

	t1.polygon,
	t1.type,
	AVG(t1.hours_invoiced) as average_monthly_hours_b2b

FROM

	(SELECT
	
		EXTRACT(YEAR FROM o.effectivedate) as year,
		EXTRACT(MONTH FROM o.effectivedate) as month,
		o.polygon,
		o.type,
		SUM(o.order_duration__c) as hours_invoiced
		
	FROM
	
		bi.orders o
		
	WHERE
	
		o.effectivedate > current_date - 120
		AND o.effectivedate < current_date
		AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'FULFILLED')
		AND o.type NOT LIKE '%training%'
		AND o.type = 'cleaning-b2b'
		
	GROUP BY
	
		year,
		month,
		o.polygon,
		o.type
		
	ORDER BY
	
		year desc,
		month desc,
		o.polygon,
		o.type) as t1
		
WHERE

	t1.month <= EXTRACT(MONTH FROM current_date)
		
GROUP BY

	t1.polygon,
	t1.type
	
ORDER BY

	t1.polygon;	
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of a small table containing only the sum of the average monthly hours b2b DE

DROP TABLE IF EXISTS bi.hours_tot_b2b_de;
CREATE TABLE bi.hours_tot_b2b_de as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2b) as tot_hours_b2b_de
	

FROM

	bi.hours_invoiced_b2b
	
WHERE

	LEFT(polygon, 2) = 'de';
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of a small table containing only the sum of the average monthly hours b2b CH

DROP TABLE IF EXISTS bi.hours_tot_b2b_ch;
CREATE TABLE bi.hours_tot_b2b_ch as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2b) as tot_hours_b2b_ch
	

FROM

	bi.hours_invoiced_b2b
	
WHERE

	LEFT(polygon, 2) = 'ch';
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of a small table containing only the sum of the average monthly hours b2b NL

DROP TABLE IF EXISTS bi.hours_tot_b2b_nl;
CREATE TABLE bi.hours_tot_b2b_nl as 

SELECT

	'Total' as total,
	SUM(average_monthly_hours_b2b) as tot_hours_b2b_nl
	

FROM

	bi.hours_invoiced_b2b
	
WHERE

	LEFT(polygon, 2) = 'nl';	
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creation of the table containing the average monthly hours b2b and the share for every polygon

DROP TABLE IF EXISTS bi.hours_invoiced__b2b;
CREATE TABLE bi.hours_invoiced__b2b as 

SELECT

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2b,
	CASE WHEN LEFT(polygon, 2) = 'de' THEN t1.average_monthly_hours_b2b/t2.tot_hours_b2b_de 
	     WHEN LEFT(polygon, 2) = 'nl' THEN t1.average_monthly_hours_b2b/t3.tot_hours_b2b_nl 
	     WHEN LEFT(polygon, 2) = 'ch' THEN t1.average_monthly_hours_b2b/t4.tot_hours_b2b_ch
		  ELSE 0
		  END
		  as share_b2b

FROM

	bi.hours_invoiced_b2b t1,
	bi.hours_tot_b2b_de t2,
	bi.hours_tot_b2b_nl t3,
	bi.hours_tot_b2b_ch t4
	
GROUP BY

	t1.polygon,
	t1.type,
	t1.average_monthly_hours_b2b,
	share_b2b;	
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	

-- Creation of the table containing the hours of the business plan, to enter manually 

DROP TABLE IF EXISTS bi.hours_business_plan;
CREATE TABLE bi.hours_business_plan as 
	
SELECT

	15649 as de_b2c_082017,
	6664 as de_b2b_082017,
	958 as nl_b2b_082017,
	9582 as ch_b2c_082017,
	15962 as de_b2c_092017,
	7981 as de_b2b_092017,
	1058 as nl_b2b_092017,
	9868 as ch_b2c_092017,
	16760 as de_b2c_102017,
	8959 as de_b2b_102017,
	1182 as nl_b2b_102017,
	9663 as ch_b2c_102017,
	17598 as de_b2c_112017,
	9938 as de_b2b_112017,
	1322 as nl_b2b_112017,
	10189 as ch_b2c_112017,
	17598 as de_b2c_122017,
	9938 as de_b2b_122017,
	1322 as nl_b2b_122017,
	10189 as ch_b2c_122017,

	12565 as de_b2c_012018,
    12977 as de_b2b_012018,
    0 as nl_b2b_012018,
    10210 as ch_b2c_012018,

    13430 as de_b2c_022018,
    13892 as de_b2b_022018,
    0 as nl_b2b_022018,
    10721 as ch_b2c_022018,

    14250 as de_b2c_032018,
    14793 as de_b2b_032018,
    0 as nl_b2b_032018,
    11257 as ch_b2c_032018,

    14959 as de_b2c_042018,
    15632 as de_b2b_042018,
    0 as nl_b2b_042018,
    11820 as ch_b2c_042018,

    15540 as de_b2c_052018,
    16429 as de_b2b_052018,
    0 as nl_b2b_052018,
    12411 as ch_b2c_052018,

    15831 as de_b2c_062018,
    17330 as de_b2b_062018,
    0 as nl_b2b_062018,
    13031 as ch_b2c_062018,

    16101 as de_b2c_072018,
    18279 as de_b2b_072018,
    0 as nl_b2b_072018,
    13683 as ch_b2c_072018,

    16356 as de_b2c_082018,
    19201 as de_b2b_082018,
    0 as nl_b2b_082018,
    14367 as ch_b2c_082018,

    16711 as de_b2c_092018,
    20105 as de_b2b_092018,
    0 as nl_b2b_092018,
    15085 as ch_b2c_092018,

    16997 as de_b2c_102018,
    20993 as de_b2b_102018,
    0 as nl_b2b_102018,
    15839 as ch_b2c_102018,

    17325 as de_b2c_112018,
    21867 as de_b2b_112018,
    0 as nl_b2b_112018,
    16631 as ch_b2c_112018,

    16338 as de_b2c_122018,
    22729 as de_b2b_122018,
    0 as nl_b2b_122018,
    17463 as ch_b2c_122018

	
FROM

	bi.orders
	
LIMIT 

	1;
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	

-- Creating the table containing the forecast b2b for every city by using their share and the hours of the business plan
	
DROP TABLE IF EXISTS bi.hours_planned__b2b;
CREATE TABLE bi.hours_planned__b2b as 

SELECT

	t1.polygon,
	-- t1.type,
	t1.average_monthly_hours_b2b,
	t1.share_b2b,
	(CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_082017
	     WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_082017
	     ELSE 0 
	     END)*0.4 as forecast_b2b_082017,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_092017
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_092017
        ELSE 0 
        END)*0.4 as forecast_b2b_092017,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_102017
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_102017
        ELSE 0 
        END)*0.4 as forecast_b2b_102017,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_112017
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_112017
        ELSE 0 
        END)*0.4 as forecast_b2b_112017,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_122017
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_122017
        ELSE 0 
        END)*0.4 as forecast_b2b_122017,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_012018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_012018
        ELSE 0 
        END)*0.4 as forecast_b2b_012018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_022018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_022018
        ELSE 0 
        END)*0.4 as forecast_b2b_022018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_032018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_032018
        ELSE 0 
        END)*0.4 as forecast_b2b_032018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_042018,
      (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_052018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_062018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_072018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_082018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_092018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_102018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_112018,
   (CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
        WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
        ELSE 0 
        END)*0.4 as forecast_b2b_122018

		
FROM

	bi.hours_invoiced__b2b t1,
	bi.hours_business_plan t3
	
GROUP BY

	t1.polygon,
	t1.average_monthly_hours_b2b,
	t1.share_b2b,
	forecast_b2b_082017,
	forecast_b2b_092017,
	forecast_b2b_102017,
	forecast_b2b_112017,
	forecast_b2b_122017,
	forecast_b2b_012018,
	forecast_b2b_022018,
	forecast_b2b_032018,
	forecast_b2b_042018,
	forecast_b2b_052018,
    forecast_b2b_062018,
    forecast_b2b_072018,
    forecast_b2b_082018,
    forecast_b2b_092018,
    forecast_b2b_102018,
    forecast_b2b_112018,
    forecast_b2b_122018

	
ORDER BY
	
	t1.polygon;

	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	

-- Creating the table containing the forecast b2c for every city by using their share and the hours of the business plan
	
DROP TABLE IF EXISTS bi.hours_planned__b2c;
CREATE TABLE bi.hours_planned__b2c as 

SELECT

	t1.polygon,
	-- t1.type,
	t1.average_monthly_hours_b2c,
	t1.share_b2c,
	CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_082017
	     WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_082017
	     ELSE 0 
	     END as forecast_b2c_082017,
   CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_092017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_092017
        ELSE 0 
        END as forecast_b2c_092017,
   CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_102017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_102017
        ELSE 0 
        END as forecast_b2c_102017,
   CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_112017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_112017
        ELSE 0 
        END as forecast_b2c_112017,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_122017
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_122017
        ELSE 0 
        END as forecast_b2c_122017,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_012018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_012018
        ELSE 0 
        END as forecast_b2c_012018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_022018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_022018
        ELSE 0 
        END as forecast_b2c_022018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_032018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_032018
        ELSE 0 
        END as forecast_b2c_032018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_042018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_042018
        ELSE 0 
        END as forecast_b2c_042018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_052018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_052018
        ELSE 0 
        END as forecast_b2c_052018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_062018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_062018
        ELSE 0 
        END as forecast_b2c_062018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_072018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_072018
        ELSE 0 
        END as forecast_b2c_072018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_082018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_082018
        ELSE 0 
        END as forecast_b2c_082018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_092018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_092018
        ELSE 0 
        END as forecast_b2c_092018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_102018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_102018
        ELSE 0 
        END as forecast_b2c_102018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_112018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_112018
        ELSE 0 
        END as forecast_b2c_112018,
    CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2c*t3.de_b2c_122018
        WHEN LEFT(t1.polygon, 2) = 'ch' THEN t1.share_b2c*t3.ch_b2c_122018
        ELSE 0 
        END as forecast_b2c_122018



		
FROM

	bi.hours_invoiced__b2c t1,
	bi.hours_business_plan t3
	
GROUP BY

	t1.polygon,
	t1.average_monthly_hours_b2c,
	t1.share_b2c,
	forecast_b2c_082017,
	forecast_b2c_092017,
	forecast_b2c_102017,
	forecast_b2c_112017,
	forecast_b2c_122017,
	forecast_b2c_012018,
	forecast_b2c_022018,
	forecast_b2c_032018,
	forecast_b2c_042018,
    forecast_b2c_052018,
    forecast_b2c_062018,
    forecast_b2c_072018,
    forecast_b2c_082018,
    forecast_b2c_092018,
    forecast_b2c_102018,
    forecast_b2c_112018,
    forecast_b2c_122018

	
ORDER BY
	
	t1.polygon;
	



-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

-- Creating the table containing the forecast b2b for every city by using their share and the hours of the business plan

DROP TABLE IF EXISTS bi.hours_planned__b2b;
CREATE TABLE bi.hours_planned__b2b as

SELECT

t1.polygon,
-- t1.type,
t1.average_monthly_hours_b2b,
t1.share_b2b,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_082017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_082017
ELSE 0
END as forecast_b2b_082017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_092017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_092017
ELSE 0
END as forecast_b2b_092017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_102017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_102017
ELSE 0
END as forecast_b2b_102017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_112017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_112017
ELSE 0
END as forecast_b2b_112017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_122017
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_122017
ELSE 0
END as forecast_b2b_122017,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_012018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_012018
ELSE 0
END as forecast_b2b_012018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_022018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_022018
ELSE 0
END as forecast_b2b_022018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_032018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_032018
ELSE 0
END as forecast_b2b_032018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_042018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_042018
ELSE 0
END as forecast_b2b_042018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_052018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_052018
ELSE 0
END as forecast_b2b_052018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_062018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_062018
ELSE 0
END as forecast_b2b_062018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_072018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_072018
ELSE 0
END as forecast_b2b_072018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_082018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_082018
ELSE 0
END as forecast_b2b_082018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_092018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_092018
ELSE 0
END as forecast_b2b_092018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_102018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_102018
ELSE 0
END as forecast_b2b_102018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_112018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_112018
ELSE 0
END as forecast_b2b_112018,
CASE WHEN LEFT(t1.polygon, 2) = 'de' THEN t1.share_b2b*t3.de_b2b_122018
WHEN LEFT(t1.polygon, 2) = 'nl' THEN t1.share_b2b*t3.nl_b2b_122018
ELSE 0
END as forecast_b2b_122018




FROM

bi.hours_invoiced__b2b t1,
bi.hours_business_plan t3

GROUP BY

t1.polygon,
t1.average_monthly_hours_b2b,
t1.share_b2b,
forecast_b2b_082017,
forecast_b2b_092017,
forecast_b2b_102017,
forecast_b2b_112017,
forecast_b2b_122017,
forecast_b2b_012018,
forecast_b2b_022018,
forecast_b2b_032018,
forecast_b2b_042018,
forecast_b2b_052018,
forecast_b2b_062018,
forecast_b2b_072018,
forecast_b2b_082018,
forecast_b2b_092018,
forecast_b2b_102018,
forecast_b2b_112018,
forecast_b2b_122018


ORDER BY

	t1.polygon;

	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	
	
-- Creating the table containing the forecast b2b/b2c for every city by using the 2 previous tables

DROP TABLE IF EXISTS bi.hours_planned;
CREATE TABLE bi.hours_planned as	


SELECT

	o.polygon,
	0 as hours_082017_b2c,
	0 as hours_082017_b2b,
	0 as hours_092017_b2c,
	0 as hours_092017_b2b,
	0 as hours_102017_b2c,
	0 as hours_102017_b2b,
	0 as hours_112017_b2c,
	0 as hours_112017_b2b,
	0 as hours_122017_b2c,
	0 as hours_122017_b2b,
	0 as hours_012018_b2c,
	0 as hours_012018_b2b,
	0 as hours_022018_b2c,
	0 as hours_022018_b2b,
	0 as hours_032018_b2c,
	0 as hours_032018_b2b,
	0 as hours_042018_b2c,
	0 as hours_042018_b2b,
	0 as hours_052018_b2c,
    0 as hours_052018_b2b,
    0 as hours_062018_b2c,
    0 as hours_062018_b2b,
    0 as hours_072018_b2c,
    0 as hours_072018_b2b,
    0 as hours_082018_b2c,
    0 as hours_082018_b2b,
    0 as hours_092018_b2c,
    0 as hours_092018_b2b,
    0 as hours_102018_b2c,
    0 as hours_102018_b2b,
    0 as hours_112018_b2c,
    0 as hours_112018_b2b,
    0 as hours_122018_b2c,
    0 as hours_122018_b2b

	
FROM

	bi.orders o
	
LIMIT 1;

DELETE FROM bi.hours_planned;	


INSERT INTO bi.hours_planned

SELECT

	main.polygon,
	(CASE WHEN forecast_b2c_082017 is null THEN 0 ELSE forecast_b2c_082017 END) as forecast_aug_b2c,
	(CASE WHEN forecast_b2b_082017 is null THEN 0 ELSE forecast_b2b_082017 END) as forecast_aug_b2b,
	(CASE WHEN forecast_b2c_092017 is null THEN 0 ELSE forecast_b2c_092017 END) as forecast_sep_b2c,
	(CASE WHEN forecast_b2b_092017 is null THEN 0 ELSE forecast_b2b_092017 END) as forecast_sep_b2b,
	(CASE WHEN forecast_b2c_102017 is null THEN 0 ELSE forecast_b2c_102017 END) as forecast_oct_b2c,
	(CASE WHEN forecast_b2b_102017 is null THEN 0 ELSE forecast_b2b_102017 END) as forecast_oct_b2b,
	(CASE WHEN forecast_b2c_112017 is null THEN 0 ELSE forecast_b2c_112017 END) as forecast_nov_b2c,
	(CASE WHEN forecast_b2b_112017 is null THEN 0 ELSE forecast_b2b_112017 END) as forecast_nov_b2b,
	(CASE WHEN forecast_b2c_122017 is null THEN 0 ELSE forecast_b2c_122017 END) as forecast_dec_b2c,
	(CASE WHEN forecast_b2b_122017 is null THEN 0 ELSE forecast_b2b_122017 END) as forecast_dec_b2b,
	(CASE WHEN forecast_b2c_012018 is null THEN 0 ELSE forecast_b2c_012018 END) as forecast_jan_b2c,
	(CASE WHEN forecast_b2b_012018 is null THEN 0 ELSE forecast_b2b_012018 END) as forecast_jan_b2b,
	(CASE WHEN forecast_b2c_022018 is null THEN 0 ELSE forecast_b2c_022018 END) as forecast_fev_b2c,
	(CASE WHEN forecast_b2b_022018 is null THEN 0 ELSE forecast_b2b_022018 END) as forecast_fev_b2b,
	(CASE WHEN forecast_b2c_032018 is null THEN 0 ELSE forecast_b2c_032018 END) as forecast_mar_b2c,
	(CASE WHEN forecast_b2b_032018 is null THEN 0 ELSE forecast_b2b_032018 END) as forecast_mar_b2b,
	(CASE WHEN forecast_b2c_042018 is null THEN 0 ELSE forecast_b2c_042018 END) as forecast_apr_b2c,
	(CASE WHEN forecast_b2b_042018 is null THEN 0 ELSE forecast_b2b_042018 END) as forecast_apr_b2b,

	(CASE WHEN forecast_b2c_052018 is null THEN 0 ELSE forecast_b2c_052018 END) as forecast_mai_b2c,
    (CASE WHEN forecast_b2b_052018 is null THEN 0 ELSE forecast_b2b_052018 END) as forecast_mai_b2b,
    (CASE WHEN forecast_b2c_062018 is null THEN 0 ELSE forecast_b2c_062018 END) as forecast_jun_b2c,
    (CASE WHEN forecast_b2b_062018 is null THEN 0 ELSE forecast_b2b_062018 END) as forecast_jun_b2b,
    (CASE WHEN forecast_b2c_072018 is null THEN 0 ELSE forecast_b2c_072018 END) as forecast_jul_b2c,
    (CASE WHEN forecast_b2b_072018 is null THEN 0 ELSE forecast_b2b_072018 END) as forecast_jul_b2b,
    (CASE WHEN forecast_b2c_082018 is null THEN 0 ELSE forecast_b2c_082018 END) as forecast_aug_b2c,
    (CASE WHEN forecast_b2b_082018 is null THEN 0 ELSE forecast_b2b_082018 END) as forecast_aug_b2b,
    (CASE WHEN forecast_b2c_092018 is null THEN 0 ELSE forecast_b2c_092018 END) as forecast_sep_b2c,
    (CASE WHEN forecast_b2b_092018 is null THEN 0 ELSE forecast_b2b_092018 END) as forecast_sep_b2b,
    (CASE WHEN forecast_b2c_102018 is null THEN 0 ELSE forecast_b2c_102018 END) as forecast_oct_b2c,
    (CASE WHEN forecast_b2b_102018 is null THEN 0 ELSE forecast_b2b_102018 END) as forecast_oct_b2b,
    (CASE WHEN forecast_b2c_112018 is null THEN 0 ELSE forecast_b2c_112018 END) as forecast_nov_b2c,
    (CASE WHEN forecast_b2b_112018 is null THEN 0 ELSE forecast_b2b_112018 END) as forecast_nov_b2b,
    (CASE WHEN forecast_b2c_122018 is null THEN 0 ELSE forecast_b2c_122018 END) as forecast_dec_b2c,
    (CASE WHEN forecast_b2b_122018 is null THEN 0 ELSE forecast_b2b_122018 END) as forecast_dec_b2b

		
FROM

	(SELECT
		polygon
	
	FROM
	
		bi.hours_planned__b2b t2

	UNION
	
	SELECT
	
		polygon
		
	FROM
	
		bi.hours_planned__b2c t1) as main
		
	LEFT JOIN

		bi.hours_planned__b2b t2
		
	ON

		main.polygon = t2.polygon

	LEFT JOIN

		bi.hours_planned__b2c t3
	
	ON

		main.polygon = t3.polygon;

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	

DROP TABLE IF EXISTS bi.hours_per_cleaner;
CREATE TABLE bi.hours_per_cleaner as	

SELECT

	t1.year,
	t1.month,	
	t1.polygon,
	t1.hours_invoiced as monthly_hours,
	t1.active_professionals,
	CASE WHEN ((t1.hours_invoiced/t1.active_professionals < 47.3) AND (LEFT(t1.polygon, 2) = 'de')) THEN 47.3
	     WHEN ((t1.hours_invoiced/t1.active_professionals < 40) AND LEFT(t1.polygon, 2) = 'ch') THEN 40
	     ELSE t1.hours_invoiced/t1.active_professionals
	     END as hours_per_cleaner

FROM

	(SELECT
	
		EXTRACT(YEAR FROM o.effectivedate) as year,
		EXTRACT(MONTH FROM o.effectivedate) as month,
		o.polygon,
		SUM(o.order_duration__c) as hours_invoiced,
		COUNT(DISTINCT o.professional__c) as active_professionals
		
	FROM
	
		bi.orders o
	
	LEFT JOIN
	
		salesforce.account a
		
	ON o.professional__c = a.sfid
		
	WHERE
	
		o.effectivedate > current_date - 60
		AND o.effectivedate < current_date
		AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'FULFILLED')
		AND o.type NOT LIKE '%training%'
		AND ((a.hr_contract_weekly_hours_min__c >= 11 AND LEFT(a.delivery_areas__c, 2) = 'de') 
		OR (a.hr_contract_weekly_hours_min__c >= 1 AND LEFT(a.delivery_areas__c, 2) = 'ch')
		OR (a.hr_contract_weekly_hours_min__c >= 1 AND LEFT(a.delivery_areas__c, 2) = 'nl')
		OR ((a.hr_contract_weekly_hours_min__c >= 0 OR a.hr_contract_weekly_hours_min__c IS NULL) AND LEFT(a.delivery_areas__c, 2) = 'at'))
		
	GROUP BY
	
		year,
		month,
		o.polygon
		
	ORDER BY
	
		year desc,
		month desc,
		o.polygon) as t1
		
WHERE

	(EXTRACT(MONTH FROM current_date) = 1 AND t1.month = 12)
	OR (EXTRACT(MONTH FROM current_date) > 1 AND t1.month = EXTRACT(MONTH FROM current_date) - 1)
	-- OR t1.month = EXTRACT(MONTH FROM current_date) - 1
		
GROUP BY

	t1.polygon,
	t1.year,
	t1.month,
	t1.hours_invoiced,
	t1.active_professionals,
	t1.hours_invoiced/t1.active_professionals
	
ORDER BY

	t1.year desc,
	t1.month desc,
	t1.polygon;
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	
	
-- Creating the table containing the number of cleaners necessary in every city

DROP TABLE IF EXISTS bi.cleaners_needed;
CREATE TABLE bi.cleaners_needed as		


SELECT

	t1.polygon,
	ROUND((t1.hours_082017_b2c + t1.hours_082017_b2b)/t2.hours_per_cleaner) as cleaners_needed_082017,
	ROUND((t1.hours_092017_b2c + t1.hours_092017_b2b)/t2.hours_per_cleaner) as cleaners_needed_092017,
	ROUND((t1.hours_102017_b2c + t1.hours_102017_b2b)/t2.hours_per_cleaner) as cleaners_needed_102017,
	ROUND((t1.hours_112017_b2c + t1.hours_112017_b2b)/t2.hours_per_cleaner) as cleaners_needed_112017,
	ROUND((t1.hours_122017_b2c + t1.hours_122017_b2b)/t2.hours_per_cleaner) as cleaners_needed_122017,
	ROUND((t1.hours_012018_b2c + t1.hours_012018_b2b)/t2.hours_per_cleaner) as cleaners_needed_012018,
	ROUND((t1.hours_022018_b2c + t1.hours_022018_b2b)/t2.hours_per_cleaner) as cleaners_needed_022018,
	ROUND((t1.hours_032018_b2c + t1.hours_032018_b2b)/t2.hours_per_cleaner) as cleaners_needed_032018,
	ROUND((t1.hours_042018_b2c + t1.hours_042018_b2b)/t2.hours_per_cleaner) as cleaners_needed_042018,

	ROUND((t1.hours_042018_b2c + t1.hours_052018_b2b)/t2.hours_per_cleaner) as cleaners_needed_052018,
    ROUND((t1.hours_042018_b2c + t1.hours_062018_b2b)/t2.hours_per_cleaner) as cleaners_needed_062018,
    ROUND((t1.hours_042018_b2c + t1.hours_072018_b2b)/t2.hours_per_cleaner) as cleaners_needed_072018,
    ROUND((t1.hours_042018_b2c + t1.hours_082018_b2b)/t2.hours_per_cleaner) as cleaners_needed_082018,
    ROUND((t1.hours_042018_b2c + t1.hours_092018_b2b)/t2.hours_per_cleaner) as cleaners_needed_092018,
    ROUND((t1.hours_042018_b2c + t1.hours_102018_b2b)/t2.hours_per_cleaner) as cleaners_needed_102018,
    ROUND((t1.hours_042018_b2c + t1.hours_112018_b2b)/t2.hours_per_cleaner) as cleaners_needed_112018,
    ROUND((t1.hours_042018_b2c + t1.hours_122018_b2b)/t2.hours_per_cleaner) as cleaners_needed_122018


		
FROM

	bi.hours_planned t1
	
JOIN

	bi.hours_per_cleaner t2

ON

	t1.polygon = t2.polygon
	
ORDER BY

	t1.polygon;
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------		
	
-- Creation of the table containing the churn% of the cleaners in every polygon, to enter manually 

DROP TABLE IF EXISTS bi.churn_percentage;
CREATE TABLE bi.churn_percentage as 
	
SELECT

	0 as churn_at,
	0.05 as churn_basel,
	0.05 as churn_bern,
	0.07 as churn_geneva,
	0 as churn_lausanne,
	0.07 as churn_lucerne,
	0.0 as churn_stgallen,
	0.03 as churn_zurich,
	0.12 as churn_berlin,
	0.0 as churn_bonn,
	0.05 as churn_cologne,
	0 as churn_dusseldorf,
	0 as churn_essen,
	0.1 as churn_frankfurt,
	0.15 as churn_hamburg,
	0.1 as churn_munich,
	0 as churn_nuremberg,
	0 as churn_potsdam,
	0.2 as churn_stuttgart,
	0 as churn_amsterdam,
	0 as churn_hague
	
FROM

	bi.orders
	
LIMIT 

	1;	
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------		

-- Creation of the table containing the current COP

DROP TABLE IF EXISTS bi.current_cop;
CREATE TABLE bi.current_cop as 
	
SELECT

	a.delivery_areas__c::text as polygon,
	SUM(CASE WHEN (LEFT(a.delivery_areas__c, 2) = 'de' AND a.hr_contract_weekly_hours_min__c < 11) THEN 0.5
	         WHEN (LEFT(a.delivery_areas__c, 2) = 'de' AND a.hr_contract_weekly_hours_min__c >= 11) THEN 1
	              ELSE 1
	              END) as current_cleaners

FROM

	salesforce.account a
	
JOIN
   
	Salesforce.Account t2

ON
   (t2.sfid = a.parentid) 
	
WHERE

	a.status__c IN ('ACTIVE', 'BETA')
	AND a.status__c NOT IN ('SUSPENDED') 
	AND (t2.name LIKE '%BAT%' OR t2.name LIKE '%BOOK%') 
	AND a.test__c = '0' 
	AND a.name NOT LIKE '%test%'
	
GROUP BY 

	a.delivery_areas__c
	
ORDER BY 

	a.delivery_areas__c;
	
	
INSERT INTO bi.current_cop

SELECT 

	a.delivery_areas__c as polygon,
	COUNT(DISTINCT a.sfid)
	
FROM

	salesforce.account a
	
WHERE

	a.status__c IN ('ACTIVE', 'BETA')
	AND a.delivery_areas__c = 'at-vienna'
	
GROUP BY 

	a.delivery_areas__c;	

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


-- Creation table churn_082017

DROP TABLE IF EXISTS bi.churn_092017;
CREATE TABLE bi.churn_092017 as 

SELECT

	t1.polygon,
	t1.current_cleaners,
	CASE WHEN t1.polygon = 'at-vienna' THEN t2.churn_at
		  WHEN t1.polygon = 'ch-basel' THEN t2.churn_basel
		  WHEN t1.polygon = 'ch-bern' THEN t2.churn_bern
		  WHEN t1.polygon = 'ch-geneva' THEN t2.churn_geneva
		  WHEN t1.polygon = 'ch-lausanne' THEN t2.churn_lausanne
		  WHEN t1.polygon = 'ch-lucerne' THEN t2.churn_lucerne
		  WHEN t1.polygon = 'ch-stgallen' THEN t2.churn_stgallen
		  WHEN t1.polygon = 'ch-zurich' THEN t2.churn_zurich
		  WHEN t1.polygon = 'de-berlin' THEN t2.churn_berlin
		  WHEN t1.polygon = 'de-bonn' THEN t2.churn_bonn
		  WHEN t1.polygon = 'de-cologne' THEN t2.churn_cologne
		  WHEN t1.polygon = 'de-dusseldorf' THEN t2.churn_dusseldorf
		  WHEN t1.polygon = 'de-essen' THEN t2.churn_essen
		  WHEN t1.polygon = 'de-frankfurt' THEN t2.churn_frankfurt
		  WHEN t1.polygon = 'de-hamburg' THEN t2.churn_hamburg
		  WHEN t1.polygon = 'de-munich' THEN t2.churn_munich
		  WHEN t1.polygon = 'de-nuremberg' THEN t2.churn_nuremberg
		  WHEN t1.polygon = 'de-potsdam' THEN t2.churn_potsdam
		  WHEN t1.polygon = 'de-stuttgart' THEN t2.churn_stuttgart
		  WHEN t1.polygon = 'nl-amsterdam' THEN t2.churn_amsterdam
		  WHEN t1.polygon = 'nl-hague' THEN t2.churn_hague
		  ELSE NULL
		  END as current_churn,
	ROUND(CASE WHEN t1.polygon = 'at-vienna' THEN t1.current_cleaners * t2.churn_at
		  WHEN t1.polygon = 'ch-basel' THEN t1.current_cleaners * t2.churn_basel
		  WHEN t1.polygon = 'ch-bern' THEN t1.current_cleaners * t2.churn_bern
		  WHEN t1.polygon = 'ch-geneva' THEN t1.current_cleaners * t2.churn_geneva
		  WHEN t1.polygon = 'ch-lausanne' THEN t1.current_cleaners * t2.churn_lausanne
		  WHEN t1.polygon = 'ch-lucerne' THEN t1.current_cleaners * t2.churn_lucerne
		  WHEN t1.polygon = 'ch-stgallen' THEN t1.current_cleaners * t2.churn_stgallen
		  WHEN t1.polygon = 'ch-zurich' THEN t1.current_cleaners * t2.churn_zurich
		  WHEN t1.polygon = 'de-berlin' THEN t1.current_cleaners * t2.churn_berlin
		  WHEN t1.polygon = 'de-bonn' THEN t1.current_cleaners * t2.churn_bonn
		  WHEN t1.polygon = 'de-cologne' THEN t1.current_cleaners * t2.churn_cologne
		  WHEN t1.polygon = 'de-dusseldorf' THEN t1.current_cleaners * t2.churn_dusseldorf
		  WHEN t1.polygon = 'de-essen' THEN t1.current_cleaners * t2.churn_essen
		  WHEN t1.polygon = 'de-frankfurt' THEN t1.current_cleaners * t2.churn_frankfurt
		  WHEN t1.polygon = 'de-hamburg' THEN t1.current_cleaners * t2.churn_hamburg
		  WHEN t1.polygon = 'de-munich' THEN t1.current_cleaners * t2.churn_munich
		  WHEN t1.polygon = 'de-nuremberg' THEN t1.current_cleaners * t2.churn_nuremberg
		  WHEN t1.polygon = 'de-potsdam' THEN t1.current_cleaners * t2.churn_potsdam
		  WHEN t1.polygon = 'de-stuttgart' THEN t1.current_cleaners * t2.churn_stuttgart
		  WHEN t1.polygon = 'nl-amsterdam' THEN t1.current_cleaners * t2.churn_amsterdam
		  WHEN t1.polygon = 'nl-hague' THEN t1.current_cleaners * t2.churn_hague
		  ELSE NULL
		  END) as churn_092017 
	
FROM

	bi.current_cop t1,
	bi.churn_percentage t2
	
ORDER BY

	t1.polygon;

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


-- Creation table cleaners to hire

DROP TABLE IF EXISTS bi.cleaners_to_hire;
CREATE TABLE bi.cleaners_to_hire as 

SELECT

	t5.polygon,
	t5.current_cleaners as COP,
	ROUND(t6.hours_per_cleaner) as hours_per_cleaner,
	t5.current_churn,
	CASE WHEN t5.cleaners_to_hire_092017 < 0 THEN 0 ELSE t5.cleaners_to_hire_092017 END as cleaners_to_hire_092017,
	CASE WHEN t5.cleaners_to_hire_102017 < 0 THEN 0 ELSE t5.cleaners_to_hire_102017 END as cleaners_to_hire_102017,
	CASE WHEN t5.cleaners_to_hire_112017 < 0 THEN 0 ELSE t5.cleaners_to_hire_112017 END as cleaners_to_hire_112017,
	CASE WHEN t5.cleaners_to_hire_122017 < 0 THEN 0 ELSE t5.cleaners_to_hire_122017 END as cleaners_to_hire_122017,
	CASE WHEN t5.cleaners_to_hire_012018 < 0 THEN 0 ELSE t5.cleaners_to_hire_012018 END as cleaners_to_hire_012018,
	CASE WHEN t5.cleaners_to_hire_022018 < 0 THEN 0 ELSE t5.cleaners_to_hire_022018 END as cleaners_to_hire_022018,
	CASE WHEN t5.cleaners_to_hire_032018 < 0 THEN 0 ELSE t5.cleaners_to_hire_032018 END as cleaners_to_hire_032018,
	CASE WHEN t5.cleaners_to_hire_042018 < 0 THEN 0 ELSE t5.cleaners_to_hire_042018 END as cleaners_to_hire_042018,
	CASE WHEN t5.cleaners_to_hire_052018 < 0 THEN 0 ELSE t5.cleaners_to_hire_052018 END as cleaners_to_hire_052018,
	CASE WHEN t5.cleaners_to_hire_062018 < 0 THEN 0 ELSE t5.cleaners_to_hire_062018 END as cleaners_to_hire_062018,
	CASE WHEN t5.cleaners_to_hire_072018 < 0 THEN 0 ELSE t5.cleaners_to_hire_072018 END as cleaners_to_hire_072018,
	CASE WHEN t5.cleaners_to_hire_082018 < 0 THEN 0 ELSE t5.cleaners_to_hire_082018 END as cleaners_to_hire_082018,
	CASE WHEN t5.cleaners_to_hire_092018 < 0 THEN 0 ELSE t5.cleaners_to_hire_092018 END as cleaners_to_hire_092018,
	CASE WHEN t5.cleaners_to_hire_102018 < 0 THEN 0 ELSE t5.cleaners_to_hire_102018 END as cleaners_to_hire_102018,
	CASE WHEN t5.cleaners_to_hire_112018 < 0 THEN 0 ELSE t5.cleaners_to_hire_112018 END as cleaners_to_hire_112018,
   CASE WHEN t5.cleaners_to_hire_122018 < 0 THEN 0 ELSE t5.cleaners_to_hire_122018 END as cleaners_to_hire_122018,

	ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12) as distributed_monthly_hiring,
    CASE WHEN LEFT(t5.polygon, 2) = 'de' THEN CASE WHEN ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
         										   ELSE ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4)/0.0456
                                                   END 
         ELSE CASE WHEN ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                   ELSE ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4)/0.06
                   END
		 END as weekly_leads_needed
	
FROM

	(SELECT
	
		t3.polygon,
		t3.current_cleaners,
		t3.current_churn,
		t3.churn_092017,
		t3.cleaners_needed_092017,
		t3.cleaners_to_hire_092017,
		ROUND(t3.cleaners_needed_092017*t3.current_churn) as churn_102017,
		t4.cleaners_needed_102017,
		ROUND(t4.cleaners_needed_102017 - t3.cleaners_needed_092017 + t3.cleaners_needed_092017*t3.current_churn) as cleaners_to_hire_102017,
		ROUND(t4.cleaners_needed_102017*t3.current_churn) as churn_112017,
		t4.cleaners_needed_112017,
		ROUND(t4.cleaners_needed_112017 - t4.cleaners_needed_112017 + t4.cleaners_needed_102017*t3.current_churn) as cleaners_to_hire_112017,

		   ROUND(t4.cleaners_needed_112017*t3.current_churn) as churn_122017,
        t4.cleaners_needed_122017,
        ROUND(t4.cleaners_needed_122017 - t4.cleaners_needed_112017 + t4.cleaners_needed_112017*t3.current_churn) as cleaners_to_hire_122017,

        ROUND(t4.cleaners_needed_122017*t3.current_churn) as churn_012018,
        t4.cleaners_needed_012018,
        ROUND(t4.cleaners_needed_012018 - t4.cleaners_needed_122017 + t4.cleaners_needed_122017*t3.current_churn) as cleaners_to_hire_012018,

        ROUND(t4.cleaners_needed_012018*t3.current_churn) as churn_022018,
        t4.cleaners_needed_022018,
        ROUND(t4.cleaners_needed_022018 - t4.cleaners_needed_012018 + t4.cleaners_needed_012018*t3.current_churn) as cleaners_to_hire_022018,

        ROUND(t4.cleaners_needed_022018*t3.current_churn) as churn_032018,
        t4.cleaners_needed_032018,
        ROUND(t4.cleaners_needed_032018 - t4.cleaners_needed_022018 + t4.cleaners_needed_022018*t3.current_churn) as cleaners_to_hire_032018,

        ROUND(t4.cleaners_needed_032018*t3.current_churn) as churn_042018,
        t4.cleaners_needed_042018,
        ROUND(t4.cleaners_needed_042018 - t4.cleaners_needed_032018 + t4.cleaners_needed_032018*t3.current_churn) as cleaners_to_hire_042018,

        ROUND(t4.cleaners_needed_042018*t3.current_churn) as churn_052018,
        t4.cleaners_needed_052018,
        ROUND(t4.cleaners_needed_052018 - t4.cleaners_needed_042018 + t4.cleaners_needed_042018*t3.current_churn) as cleaners_to_hire_052018,

        ROUND(t4.cleaners_needed_052018*t3.current_churn) as churn_062018,
        t4.cleaners_needed_062018,
        ROUND(t4.cleaners_needed_062018 - t4.cleaners_needed_052018 + t4.cleaners_needed_052018*t3.current_churn) as cleaners_to_hire_062018,

        ROUND(t4.cleaners_needed_062018*t3.current_churn) as churn_072018,
        t4.cleaners_needed_072018,
        ROUND(t4.cleaners_needed_072018 - t4.cleaners_needed_062018 + t4.cleaners_needed_062018*t3.current_churn) as cleaners_to_hire_072018,

        ROUND(t4.cleaners_needed_072018*t3.current_churn) as churn_082018,
        t4.cleaners_needed_082018,
        ROUND(t4.cleaners_needed_082018 - t4.cleaners_needed_072018 + t4.cleaners_needed_072018*t3.current_churn) as cleaners_to_hire_082018,

        ROUND(t4.cleaners_needed_082018*t3.current_churn) as churn_092018,
        t4.cleaners_needed_092018,
        ROUND(t4.cleaners_needed_092018 - t4.cleaners_needed_082018 + t4.cleaners_needed_082018*t3.current_churn) as cleaners_to_hire_092018,

        ROUND(t4.cleaners_needed_092018*t3.current_churn) as churn_102018,
        t4.cleaners_needed_102018,
        ROUND(t4.cleaners_needed_102018 - t4.cleaners_needed_092018 + t4.cleaners_needed_092018*t3.current_churn) as cleaners_to_hire_102018,

        ROUND(t4.cleaners_needed_102018*t3.current_churn) as churn_112018,
        t4.cleaners_needed_112018,
        ROUND(t4.cleaners_needed_112018 - t4.cleaners_needed_102018 + t4.cleaners_needed_102018*t3.current_churn) as cleaners_to_hire_112018,

        ROUND(t4.cleaners_needed_112018*t3.current_churn) as churn_122018,
        t4.cleaners_needed_122018,
        ROUND(t4.cleaners_needed_122018 - t4.cleaners_needed_112018 + t4.cleaners_needed_112018*t3.current_churn) as cleaners_to_hire_122018
	
	
	FROM
	
	
		(SELECT
		
			t1.polygon,
			t1.current_cleaners,
			t1.current_churn,
			t1.churn_092017,
			t2.cleaners_needed_092017,
			CASE WHEN (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) < 0 THEN 0 ELSE (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) END as cleaners_to_hire_092017
			
		FROM
		
			bi.churn_092017 t1
		
		LEFT JOIN
		
			bi.cleaners_needed t2
			
		ON
		
			t1.polygon = t2.polygon) as t3
			
	LEFT JOIN
	
		bi.cleaners_needed t4
		
	ON 
	
		t3.polygon = t4.polygon) as t5
		
LEFT JOIN

	bi.hours_per_cleaner as t6
	
ON 

	t5.polygon = t6.polygon;	


----------------------------------------------------------------------


INSERT INTO bi.cleaners_to_hire

SELECT

	t3.polygon,
	t3.COP,
	t3.hours_per_cleaner,
	t3.current_churn,
	CASE WHEN t3.cleaners_to_hire_092017 < 0 THEN 0 ELSE t3.cleaners_to_hire_092017 END as cleaners_to_hire_092017,
	CASE WHEN t3.cleaners_to_hire_102017 < 0 THEN 0 ELSE t3.cleaners_to_hire_102017 END as cleaners_to_hire_102017,
	CASE WHEN t3.cleaners_to_hire_112017 < 0 THEN 0 ELSE t3.cleaners_to_hire_112017 END as cleaners_to_hire_112017,
	CASE WHEN t3.cleaners_to_hire_122017 < 0 THEN 0 ELSE t3.cleaners_to_hire_122017 END as cleaners_to_hire_122017,
   CASE WHEN t3.cleaners_to_hire_012018 < 0 THEN 0 ELSE t3.cleaners_to_hire_012018 END as cleaners_to_hire_012018,
   CASE WHEN t3.cleaners_to_hire_022018 < 0 THEN 0 ELSE t3.cleaners_to_hire_022018 END as cleaners_to_hire_022018,
   CASE WHEN t3.cleaners_to_hire_032018 < 0 THEN 0 ELSE t3.cleaners_to_hire_032018 END as cleaners_to_hire_032018,
   CASE WHEN t3.cleaners_to_hire_042018 < 0 THEN 0 ELSE t3.cleaners_to_hire_042018 END as cleaners_to_hire_042018,
   CASE WHEN t3.cleaners_to_hire_052018 < 0 THEN 0 ELSE t3.cleaners_to_hire_052018 END as cleaners_to_hire_052018,
   CASE WHEN t3.cleaners_to_hire_062018 < 0 THEN 0 ELSE t3.cleaners_to_hire_062018 END as cleaners_to_hire_062018,
   CASE WHEN t3.cleaners_to_hire_072018 < 0 THEN 0 ELSE t3.cleaners_to_hire_072018 END as cleaners_to_hire_072018,
   CASE WHEN t3.cleaners_to_hire_082018 < 0 THEN 0 ELSE t3.cleaners_to_hire_082018 END as cleaners_to_hire_082018,
   CASE WHEN t3.cleaners_to_hire_092018 < 0 THEN 0 ELSE t3.cleaners_to_hire_092018 END as cleaners_to_hire_092018,
   CASE WHEN t3.cleaners_to_hire_102018 < 0 THEN 0 ELSE t3.cleaners_to_hire_102018 END as cleaners_to_hire_102018,
   CASE WHEN t3.cleaners_to_hire_112018 < 0 THEN 0 ELSE t3.cleaners_to_hire_112018 END as cleaners_to_hire_112018,
   CASE WHEN t3.cleaners_to_hire_122018 < 0 THEN 0 ELSE t3.cleaners_to_hire_122018 END as cleaners_to_hire_122018,

	CASE WHEN t3.distributed_monthly_hiring < 0 THEN 0 ELSE t3.distributed_monthly_hiring END as distributed_monthly_hiring,

	-- CASE WHEN ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) = 0 THEN 1
	     -- ELSE ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) 
		  -- END as distributed_weekly_hiring,
    CASE WHEN UPPER(LEFT(t3.polygon, 2)) = 'DE' THEN CASE WHEN ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                                          ELSE ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4)/0.0456
                                          END
         ELSE CASE WHEN ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                   ELSE ROUND(ROUND((t3.cleaners_to_hire_012018 + t3.cleaners_to_hire_022018 + t3.cleaners_to_hire_032018 + t3.cleaners_to_hire_042018 + t3.cleaners_to_hire_052018 + t3.cleaners_to_hire_062018 + t3.cleaners_to_hire_072018 + t3.cleaners_to_hire_082018 + t3.cleaners_to_hire_092018 + t3.cleaners_to_hire_102018 + t3.cleaners_to_hire_112018 + t3.cleaners_to_hire_122018)/12)/4)/0.06
                   END
			END as weekly_leads_needed

FROM


	(SELECT
	
		UPPER(LEFT(t5.polygon, 2)) as polygon,
		SUM(t5.current_cleaners) as COP,
		AVG(ROUND(t6.hours_per_cleaner)) as hours_per_cleaner,
		AVG(t5.current_churn) as current_churn,
		SUM(t5.cleaners_to_hire_092017) as cleaners_to_hire_092017,
		SUM(t5.cleaners_to_hire_102017) as cleaners_to_hire_102017,
		SUM(t5.cleaners_to_hire_112017) as cleaners_to_hire_112017,
		SUM(t5.cleaners_to_hire_122017) as cleaners_to_hire_122017,
        SUM(t5.cleaners_to_hire_012018) as cleaners_to_hire_012018,
        SUM(t5.cleaners_to_hire_022018) as cleaners_to_hire_022018,
        SUM(t5.cleaners_to_hire_032018) as cleaners_to_hire_032018,
        SUM(t5.cleaners_to_hire_042018) as cleaners_to_hire_042018,
        SUM(t5.cleaners_to_hire_052018) as cleaners_to_hire_052018,
        SUM(t5.cleaners_to_hire_062018) as cleaners_to_hire_062018,
        SUM(t5.cleaners_to_hire_072018) as cleaners_to_hire_072018,
        SUM(t5.cleaners_to_hire_082018) as cleaners_to_hire_082018,
        SUM(t5.cleaners_to_hire_092018) as cleaners_to_hire_092018,
        SUM(t5.cleaners_to_hire_102018) as cleaners_to_hire_102018,
        SUM(t5.cleaners_to_hire_112018) as cleaners_to_hire_112018,
        SUM(t5.cleaners_to_hire_122018) as cleaners_to_hire_122018,

        ROUND(SUM(t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12) as distributed_monthly_hiring

	FROM
	
		(SELECT
		
			t3.polygon,
			t3.current_cleaners,
			t3.current_churn,
			t3.churn_092017,
			t3.cleaners_needed_092017,
			t3.cleaners_to_hire_092017,
			ROUND(t3.cleaners_needed_092017*t3.current_churn) as churn_102017,
			t4.cleaners_needed_102017,
			ROUND(t4.cleaners_needed_102017 - t3.cleaners_needed_092017 + t3.cleaners_needed_092017*t3.current_churn) as cleaners_to_hire_102017,
			ROUND(t4.cleaners_needed_102017*t3.current_churn) as churn_112017,
			t4.cleaners_needed_112017,
			ROUND(t4.cleaners_needed_112017 - t4.cleaners_needed_112017 + t4.cleaners_needed_102017*t3.current_churn) as cleaners_to_hire_112017,

			ROUND(t4.cleaners_needed_112017*t3.current_churn) as churn_122017,
            t4.cleaners_needed_122017,
            ROUND(t4.cleaners_needed_122017 - t4.cleaners_needed_112017 + t4.cleaners_needed_112017*t3.current_churn) as cleaners_to_hire_122017,

            ROUND(t4.cleaners_needed_122017*t3.current_churn) as churn_012018,
            t4.cleaners_needed_012018,
            ROUND(t4.cleaners_needed_012018 - t4.cleaners_needed_122017 + t4.cleaners_needed_122017*t3.current_churn) as cleaners_to_hire_012018,

            ROUND(t4.cleaners_needed_012018*t3.current_churn) as churn_022018,
            t4.cleaners_needed_022018,
            ROUND(t4.cleaners_needed_022018 - t4.cleaners_needed_012018 + t4.cleaners_needed_012018*t3.current_churn) as cleaners_to_hire_022018,

            ROUND(t4.cleaners_needed_022018*t3.current_churn) as churn_032018,
            t4.cleaners_needed_032018,
            ROUND(t4.cleaners_needed_032018 - t4.cleaners_needed_022018 + t4.cleaners_needed_022018*t3.current_churn) as cleaners_to_hire_032018,

            ROUND(t4.cleaners_needed_032018*t3.current_churn) as churn_042018,
            t4.cleaners_needed_042018,
            ROUND(t4.cleaners_needed_042018 - t4.cleaners_needed_032018 + t4.cleaners_needed_032018*t3.current_churn) as cleaners_to_hire_042018,

            ROUND(t4.cleaners_needed_042018*t3.current_churn) as churn_052018,
            t4.cleaners_needed_052018,
            ROUND(t4.cleaners_needed_052018 - t4.cleaners_needed_042018 + t4.cleaners_needed_042018*t3.current_churn) as cleaners_to_hire_052018,

            ROUND(t4.cleaners_needed_052018*t3.current_churn) as churn_062018,
            t4.cleaners_needed_062018,
            ROUND(t4.cleaners_needed_062018 - t4.cleaners_needed_052018 + t4.cleaners_needed_052018*t3.current_churn) as cleaners_to_hire_062018,

            ROUND(t4.cleaners_needed_062018*t3.current_churn) as churn_072018,
            t4.cleaners_needed_072018,
            ROUND(t4.cleaners_needed_072018 - t4.cleaners_needed_062018 + t4.cleaners_needed_062018*t3.current_churn) as cleaners_to_hire_072018,

            ROUND(t4.cleaners_needed_072018*t3.current_churn) as churn_082018,
            t4.cleaners_needed_082018,
            ROUND(t4.cleaners_needed_082018 - t4.cleaners_needed_072018 + t4.cleaners_needed_072018*t3.current_churn) as cleaners_to_hire_082018,

            ROUND(t4.cleaners_needed_082018*t3.current_churn) as churn_092018,
            t4.cleaners_needed_092018,
            ROUND(t4.cleaners_needed_092018 - t4.cleaners_needed_082018 + t4.cleaners_needed_082018*t3.current_churn) as cleaners_to_hire_092018,

            ROUND(t4.cleaners_needed_092018*t3.current_churn) as churn_102018,
            t4.cleaners_needed_102018,
            ROUND(t4.cleaners_needed_102018 - t4.cleaners_needed_092018 + t4.cleaners_needed_092018*t3.current_churn) as cleaners_to_hire_102018,

            ROUND(t4.cleaners_needed_102018*t3.current_churn) as churn_112018,
            t4.cleaners_needed_112018,
            ROUND(t4.cleaners_needed_112018 - t4.cleaners_needed_102018 + t4.cleaners_needed_102018*t3.current_churn) as cleaners_to_hire_112018,

            ROUND(t4.cleaners_needed_112018*t3.current_churn) as churn_122018,
            t4.cleaners_needed_122018,
            ROUND(t4.cleaners_needed_122018 - t4.cleaners_needed_112018 + t4.cleaners_needed_112018*t3.current_churn) as cleaners_to_hire_122018
	
		
		FROM
		
		
			(SELECT
			
				t1.polygon,
				t1.current_cleaners,
				t1.current_churn,
				t1.churn_092017,
				t2.cleaners_needed_092017,
				CASE WHEN (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) < 0 THEN 0 ELSE (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) END as cleaners_to_hire_092017
				
			FROM
			
				bi.churn_092017 t1
			
			LEFT JOIN
			
				bi.cleaners_needed t2
				
			ON
			
				t1.polygon = t2.polygon) as t3
				
		LEFT JOIN
		
			bi.cleaners_needed t4
			
		ON 
		
			t3.polygon = t4.polygon) as t5
			
	LEFT JOIN
	
		bi.hours_per_cleaner as t6
		
	ON 
	
		t5.polygon = t6.polygon
		
	GROUP BY
	
		UPPER(LEFT(t5.polygon, 2))) t3;
	
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------	

/*DELETE FROM bi.hiring_plan_history WHERE date = current_date::date;
	
INSERT INTO bi.hiring_plan_history	
	
SELECT

	current_date as date,
	t5.polygon,
	t5.current_cleaners as COP,
	ROUND(t6.hours_per_cleaner) as hours_per_cleaner,
	t5.current_churn,
	t5.cleaners_to_hire_092017,
	t5.cleaners_to_hire_102017,
	t5.cleaners_to_hire_112017,
    t5.cleaners_to_hire_122017,
    t5.cleaners_to_hire_012018,
    t5.cleaners_to_hire_022018,
    t5.cleaners_to_hire_032018,
    t5.cleaners_to_hire_042018,
    t5.cleaners_to_hire_052018,
    t5.cleaners_to_hire_062018,
    t5.cleaners_to_hire_072018,
    t5.cleaners_to_hire_082018,
    t5.cleaners_to_hire_092018,
    t5.cleaners_to_hire_102018,
    t5.cleaners_to_hire_112018,
    t5.cleaners_to_hire_122018,

	ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12) as distributed_monthly_hiring,
    CASE WHEN LEFT(t5.polygon, 2) = 'de' THEN CASE WHEN ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
         										   ELSE ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4)/0.0456
                                                   END 
         ELSE CASE WHEN ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4) = 0 THEN 1 
                   ELSE ROUND(ROUND((t5.cleaners_to_hire_012018 + t5.cleaners_to_hire_022018 + t5.cleaners_to_hire_032018 + t5.cleaners_to_hire_042018 + t5.cleaners_to_hire_052018 + t5.cleaners_to_hire_062018 + t5.cleaners_to_hire_072018 + t5.cleaners_to_hire_082018 + t5.cleaners_to_hire_092018 + t5.cleaners_to_hire_102018 + t5.cleaners_to_hire_112018 + t5.cleaners_to_hire_122018)/12)/4)/0.06
                   END
		 END as weekly_leads_needed
	
FROM

	(SELECT
	
		t3.polygon,
		t3.current_cleaners,
		t3.current_churn,
		t3.churn_092017,
		t3.cleaners_needed_092017,
		t3.cleaners_to_hire_092017,
		ROUND(t3.cleaners_needed_092017*t3.current_churn) as churn_102017,
		t4.cleaners_needed_102017,
		ROUND(t4.cleaners_needed_102017 - t3.cleaners_needed_092017 + t3.cleaners_needed_092017*t3.current_churn) as cleaners_to_hire_102017,
		ROUND(t4.cleaners_needed_102017*t3.current_churn) as churn_112017,
		t4.cleaners_needed_112017,
		ROUND(t4.cleaners_needed_112017 - t4.cleaners_needed_112017 + t4.cleaners_needed_102017*t3.current_churn) as cleaners_to_hire_112017,

		        ROUND(t4.cleaners_needed_112017*t3.current_churn) as churn_122017,
        t4.cleaners_needed_122017,
        ROUND(t4.cleaners_needed_122017 - t4.cleaners_needed_112017 + t4.cleaners_needed_112017*t3.current_churn) as cleaners_to_hire_122017,

        ROUND(t4.cleaners_needed_122017*t3.current_churn) as churn_012018,
        t4.cleaners_needed_012018,
        ROUND(t4.cleaners_needed_012018 - t4.cleaners_needed_122017 + t4.cleaners_needed_122017*t3.current_churn) as cleaners_to_hire_012018,

        ROUND(t4.cleaners_needed_012018*t3.current_churn) as churn_022018,
        t4.cleaners_needed_022018,
        ROUND(t4.cleaners_needed_022018 - t4.cleaners_needed_012018 + t4.cleaners_needed_012018*t3.current_churn) as cleaners_to_hire_022018,

        ROUND(t4.cleaners_needed_022018*t3.current_churn) as churn_032018,
        t4.cleaners_needed_032018,
        ROUND(t4.cleaners_needed_032018 - t4.cleaners_needed_022018 + t4.cleaners_needed_022018*t3.current_churn) as cleaners_to_hire_032018,

        ROUND(t4.cleaners_needed_032018*t3.current_churn) as churn_042018,
        t4.cleaners_needed_042018,
        ROUND(t4.cleaners_needed_042018 - t4.cleaners_needed_032018 + t4.cleaners_needed_032018*t3.current_churn) as cleaners_to_hire_042018,

        ROUND(t4.cleaners_needed_042018*t3.current_churn) as churn_052018,
        t4.cleaners_needed_052018,
        ROUND(t4.cleaners_needed_052018 - t4.cleaners_needed_042018 + t4.cleaners_needed_042018*t3.current_churn) as cleaners_to_hire_052018,

        ROUND(t4.cleaners_needed_052018*t3.current_churn) as churn_062018,
        t4.cleaners_needed_062018,
        ROUND(t4.cleaners_needed_062018 - t4.cleaners_needed_052018 + t4.cleaners_needed_052018*t3.current_churn) as cleaners_to_hire_062018,

        ROUND(t4.cleaners_needed_062018*t3.current_churn) as churn_072018,
        t4.cleaners_needed_072018,
        ROUND(t4.cleaners_needed_072018 - t4.cleaners_needed_062018 + t4.cleaners_needed_062018*t3.current_churn) as cleaners_to_hire_072018,

        ROUND(t4.cleaners_needed_072018*t3.current_churn) as churn_082018,
        t4.cleaners_needed_082018,
        ROUND(t4.cleaners_needed_082018 - t4.cleaners_needed_072018 + t4.cleaners_needed_072018*t3.current_churn) as cleaners_to_hire_082018,

        ROUND(t4.cleaners_needed_082018*t3.current_churn) as churn_092018,
        t4.cleaners_needed_092018,
        ROUND(t4.cleaners_needed_092018 - t4.cleaners_needed_082018 + t4.cleaners_needed_082018*t3.current_churn) as cleaners_to_hire_092018,

        ROUND(t4.cleaners_needed_092018*t3.current_churn) as churn_102018,
        t4.cleaners_needed_102018,
        ROUND(t4.cleaners_needed_102018 - t4.cleaners_needed_092018 + t4.cleaners_needed_092018*t3.current_churn) as cleaners_to_hire_102018,

        ROUND(t4.cleaners_needed_102018*t3.current_churn) as churn_112018,
        t4.cleaners_needed_112018,
        ROUND(t4.cleaners_needed_112018 - t4.cleaners_needed_102018 + t4.cleaners_needed_102018*t3.current_churn) as cleaners_to_hire_112018,

        ROUND(t4.cleaners_needed_112018*t3.current_churn) as churn_122018,
        t4.cleaners_needed_122018,
        ROUND(t4.cleaners_needed_122018 - t4.cleaners_needed_112018 + t4.cleaners_needed_112018*t3.current_churn) as cleaners_to_hire_122018
	
	
	FROM
	
	
		(SELECT
		
			t1.polygon,
			t1.current_cleaners,
			t1.current_churn,
			t1.churn_092017,
			t2.cleaners_needed_092017,
			CASE WHEN (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) < 0 THEN 0 ELSE (t2.cleaners_needed_092017 - t1.current_cleaners + t1.churn_092017) END as cleaners_to_hire_092017
			
		FROM
		
			bi.churn_092017 t1
		
		LEFT JOIN
		
			bi.cleaners_needed t2
			
		ON
		
			t1.polygon = t2.polygon) as t3
			
	LEFT JOIN
	
		bi.cleaners_needed t4
		
	ON 
	
		t3.polygon = t4.polygon) as t5
		
LEFT JOIN

	bi.hours_per_cleaner as t6
	
ON 

	t5.polygon = t6.polygon;
*/

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.comparison_forecast;
CREATE TABLE bi.comparison_forecast as

SELECT

	'null'::text as polygon,
	'type_bp_b2c'::text as type_bp,
	0::numeric as hours_082017,
	0::numeric as hours_092017,
	0::numeric as hours_102017,
	0::numeric as hours_112017,
	0::numeric as hours_122017,
	0::numeric as hours_012018,
	0::numeric as hours_022018,
	0::numeric as hours_032018,
	0::numeric as hours_042018,
	0::numeric as hours_052018,
	0::numeric as hours_062018,
	0::numeric as hours_072018,
	0::numeric as hours_082018,
	0::numeric as hours_092018,
	0::numeric as hours_102018,
	0::numeric as hours_112018,
	0::numeric as hours_122018,
	0::numeric as current_hours

FROM

	bi.orders
	
LIMIT 

	1;


DELETE FROM bi.comparison_forecast;


INSERT INTO bi.comparison_forecast

SELECT

	t1.polygon,
	'type_bp_b2c' as type_bp,
	t1.hours_082017_b2c,
	t1.hours_092017_b2c,
	t1.hours_102017_b2c,
	t1.hours_112017_b2c,

	t1.hours_122017_b2c,
	t1.hours_012018_b2c,
	t1.hours_022018_b2c,
	t1.hours_032018_b2c,
	t1.hours_042018_b2c,
	t1.hours_052018_b2c,
	t1.hours_062018_b2c,
	t1.hours_072018_b2c,
	t1.hours_082018_b2c,
	t1.hours_092018_b2c,
	t1.hours_102018_b2c,
	t1.hours_112018_b2c,
	t1.hours_122018_b2c,
	

	SUM(o.order_duration__c) as current_hours

FROM

	(SELECT
	
		*
		
	FROM 
	
		bi.hours_planned) t1
		
LEFT JOIN

	bi.orders o 
	
ON 

	t1.polygon = o.polygon
	
WHERE

	EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
	AND EXTRACT(MONTH FROM o.effectivedate) = EXTRACT(MONTH FROM current_date)
	AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
	AND o.type = 'cleaning-b2c'
	
GROUP BY

	t1.polygon,
	type_bp,
	t1.hours_082017_b2c,
	t1.hours_092017_b2c,
	t1.hours_102017_b2c,
	t1.hours_112017_b2c,
	t1.hours_122017_b2c,
	t1.hours_012018_b2c,
	t1.hours_022018_b2c,
	t1.hours_032018_b2c,
	t1.hours_042018_b2c,
	t1.hours_052018_b2c,
	t1.hours_062018_b2c,
	t1.hours_072018_b2c,
	t1.hours_082018_b2c,
	t1.hours_092018_b2c,
	t1.hours_102018_b2c,
	t1.hours_112018_b2c,
	t1.hours_122018_b2c;
	
	
------------------------------------------------------------------------------------------	

INSERT INTO bi.comparison_forecast
	
SELECT

	t1.polygon,
	'type_bp_b2b' as type_bp,
	t1.hours_082017_b2b,
	t1.hours_092017_b2b,
	t1.hours_102017_b2b,
	t1.hours_112017_b2b,
	t1.hours_122017_b2b,
	t1.hours_012018_b2b,
	t1.hours_022018_b2b,
	t1.hours_032018_b2b,
	t1.hours_042018_b2b,
	t1.hours_052018_b2b,
	t1.hours_062018_b2b,
	t1.hours_072018_b2b,
	t1.hours_082018_b2b,
	t1.hours_092018_b2b,
	t1.hours_102018_b2b,
	t1.hours_112018_b2b,
	t1.hours_122018_b2b,
	SUM(o.order_duration__c)*0.4 as current_hours

FROM

	(SELECT
	
		*
		
	FROM 
	
		bi.hours_planned) t1
		
LEFT JOIN

	bi.orders o 
	
ON 

	t1.polygon = o.polygon
	
WHERE

	EXTRACT(YEAR FROM o.effectivedate) = EXTRACT(YEAR FROM current_date)
	AND EXTRACT(MONTH FROM o.effectivedate) = EXTRACT(MONTH FROM current_date)
	AND o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
	AND o.type = 'cleaning-b2b'
	
GROUP BY

	t1.polygon,
	type_bp,
	t1.hours_082017_b2b,
	t1.hours_092017_b2b,
	t1.hours_102017_b2b,
	t1.hours_112017_b2b,
	t1.hours_122017_b2b,
	t1.hours_012018_b2b,
	t1.hours_022018_b2b,
	t1.hours_032018_b2b,
	t1.hours_042018_b2b,
	t1.hours_052018_b2b,
	t1.hours_062018_b2b,
	t1.hours_072018_b2b,
	t1.hours_082018_b2b,
	t1.hours_092018_b2b,
	t1.hours_102018_b2b,
	t1.hours_112018_b2b,
	t1.hours_122018_b2b;	
		
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
		
DROP TABLE IF EXISTS bi.hours_invoiced_b2c;
DROP TABLE IF EXISTS bi.hours_tot_b2c_de;
DROP TABLE IF EXISTS bi.hours_tot_b2c_ch;
DROP TABLE IF EXISTS bi.hours_tot_b2c_nl;
DROP TABLE IF EXISTS bi.hours_invoiced__b2c;
DROP TABLE IF EXISTS bi.hours_invoiced_b2b;
DROP TABLE IF EXISTS bi.hours_tot_b2b_de;
DROP TABLE IF EXISTS bi.hours_tot_b2b_ch;
DROP TABLE IF EXISTS bi.hours_tot_b2b_nl;
DROP TABLE IF EXISTS bi.hours_invoiced__b2b;
DROP TABLE IF EXISTS bi.hours_planned__b2b;
DROP TABLE IF EXISTS bi.hours_planned__b2c;
DROP TABLE IF EXISTS bi.hours_per_cleaner;
DROP TABLE IF EXISTS bi.cleaners_needed;
DROP TABLE IF EXISTS bi.churn_percentage;
DROP TABLE IF EXISTS bi.current_cop;
DROP TABLE IF EXISTS bi.churn_092017;


-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------




-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$
LANGUAGE plpgsql VOLATILE