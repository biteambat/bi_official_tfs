
DELIMITER //


CREATE OR REPLACE FUNCTION bi.sfunc_to_conversion_leads(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_to_conversion_leads';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.timelapses_recruitment_process;
CREATE TABLE bi.timelapses_recruitment_process as 


(SELECT

	EXTRACT(year from l.createddate) as created_year,
	EXTRACT(month from l.createddate) as created_month,
	l.countrycode,
	l.delivery_area__c as polygon,
	t1.*
	
FROM 
	
	(SELECT
	
		leadid,
		min(CASE WHEN field = 'created' THEN createddate ELSE NULL END) as created_date,
		min(CASE WHEN field = 'Status' THEN
												 CASE WHEN oldvalue IN ('New lead', 'New Leads')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_touched,
		CASE WHEN (EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('New lead', 'New Leads') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int) = 0 THEN 1 ELSE (EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('New lead', 'New Leads') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int) END as time_to_touched,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents before contract')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_documents_before_contract,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents before contract') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_documents_before_contract,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview to be scheduled')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_interview_to_be_scheduled,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview to be scheduled') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_interview_to_be_scheduled,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview scheduled')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_interview_scheduled,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Interview scheduled') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_interview_scheduled,										 
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for contract')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_waiting_for_contract,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for contract') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_waiting_for_contract,
		min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for greenlight')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_waiting_for_greenlight,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Waiting for greenlight') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_waiting_for_greenlight,
	    min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents after interview', 'Contract ready')
												 	   THEN createddate
												 	   ELSE NULL
												 	   END
												 ELSE NULL
												 END) as date_after_interview,
		EXTRACT(epoch FROM (min(CASE WHEN field = 'Status' THEN CASE WHEN oldvalue IN ('Documents after interview', 'Contract ready') THEN createddate ELSE NULL END ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_after_interview,
		min(CASE WHEN field = 'leadConverted' THEN createddate ELSE NULL END) as conversion_date,												 
		EXTRACT(epoch FROM (min(CASE WHEN field = 'leadConverted' THEN createddate ELSE NULL END) - min(CASE WHEN field = 'created' THEN createddate ELSE NULL END))/86400)::int as time_to_conversion								 
			
			
		
	FROM
	
		salesforce.leadhistory
		
		
	GROUP BY 
	
		leadid
	
		
	ORDER BY
	
		leadid) as t1
		
		
LEFT JOIN

	salesforce.lead l
	
	
ON

	l.sfid = t1.leadid	
	
	
WHERE 

	l.delivery_area__c IS NOT NULL
	AND l.ownerid NOT IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC'))
;

	

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.conversion_leads_per_polygon;
CREATE TABLE bi.conversion_leads_per_polygon as 


SELECT

	t2.Country as Country,
	t2.polygon,
	CAST(t2.year_touched as integer),
	CAST(t2.month_touched as integer),
	t2.date_touched1,
	
	t2.Total_leads,
	t2.Touched as Touched,
	
	-- (t2.Touched/t2.Total_leads) as Share_touched,
	CASE WHEN t2.Touched > 0 THEN (t2.Reached/t2.Touched) ELSE 0 END as Share_Reached,
	t2.reached,
	CASE WHEN t2.Touched > 0 THEN (t2.Rejected/t2.Touched) ELSE 0 END as Share_Rejected,
	t2.Rejected,
	CASE WHEN t2.Touched > 0 THEN (t2.Documents_before_contract/t2.Touched) ELSE 0 END as Share_Documents_before_contract,
	t2.Documents_before_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_to_be_scheduled/t2.Touched) ELSE 0 END as Share_Interview_to_be_scheduled,
	t2.Interview_to_be_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Interview_scheduled/t2.Touched) ELSE 0 END as Share_Interview_scheduled,
	t2.Interview_scheduled,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_contract/t2.Touched) ELSE 0 END as Share_Waiting_for_contract,
	t2.Waiting_for_contract,
	CASE WHEN t2.Touched > 0 THEN (t2.After_interview/t2.Touched) ELSE 0 END as Share_After_interview,
	t2.After_interview,
	CASE WHEN t2.Touched > 0 THEN (t2.Waiting_for_greenlight/t2.Touched) ELSE 0 END as Share_Waiting_for_greenlight,
	t2.Waiting_for_greenlight,
	CASE WHEN t2.Touched > 0 THEN (t2.Converted/t2.Touched) ELSE 0 END as Share_Converted_touched,
	CASE WHEN t2.Total_leads > 0 THEN (t2.Converted/t2.Total_leads) ELSE 0 END as Share_Converted_total,
	
	t2.Converted as Converted
	
	
FROM


	(SELECT
	
		t1.countrycode as Country,
		t1.polygon,
		-- CAST(t1.year_created as integer),
		-- CAST(t1.month_created as integer),
		-- date_created,
		CAST(t1.year_touched as integer),
		CAST(t1.month_touched as integer),
		date_touched1,
		
		t1.C0::numeric as Total_leads,
		(t1.C1 + t1.C1bis + t1.C2 + t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9 + t1.C10)::numeric as New_leads,
		(t1.C0 - t1.C1 - t1.C1new)::numeric as Touched,
		(t1.C0 - t1.C1 - t1.C1new - t1.C2)::numeric as Reached,
		(t1.C3 + t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Documents_before_contract,
		(t1.C4 + t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_to_be_scheduled,
		(t1.C5 + t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Interview_scheduled,
		(t1.C6 + t1.C7 + t1.C8 + t1.C9)::numeric as Waiting_for_contract,
		(t1.C7 + t1.C8 + t1.C9)::numeric as After_interview,
		(t1.C7 + t1.C9)::numeric as Waiting_for_greenlight,
		
		(t1.C11)::numeric as Converted,
		(t1.C10)::numeric as Rejected
		
	
	FROM
	
		
		(SELECT
		
			a.countrycode,
			a.delivery_area__c as polygon,
			-- EXTRACT(year from a.createddate) as year_created,
			-- EXTRACT(month from a.createddate) as month_created,
			-- MIN(a.createddate::date) date_created,
			EXTRACT(year from leadtouched.date_touched) as year_touched,
			EXTRACT(month from leadtouched.date_touched) as month_touched,
			MIN(leadtouched.date_touched::date) date_touched1,
		
			COUNT(a.createddate) as C0,
		
			SUM(CASE WHEN a.status IN ('New lead','New Leads') THEN 1 ELSE 0 END) as C1,
			SUM(CASE WHEN a.status IN ('QA Call', 'Hold', 'Double Entry', 'Avoid', 'Contact Later') THEN 1 ELSE 0 END) as C1bis,
			SUM(CASE WHEN a.status IN ('Inactive', 'Reactivate') THEN 1 ELSE 0 END) as C1new,
			SUM(CASE WHEN a.status IN ('First contact outstanding', 'Never reached') THEN 1 ELSE 0 END) as C2,
			SUM(CASE WHEN a.status IN ('Documents before contract') THEN 1 ELSE 0 END) as C3,
			SUM(CASE WHEN a.status IN ('Interview to be scheduled') THEN 1 ELSE 0 END) as C4,
			SUM(CASE WHEN a.status IN ('Interview scheduled') THEN 1 ELSE 0 END) as C5,
			SUM(CASE WHEN a.status IN ('Waiting for contract') THEN 1 ELSE 0 END) as C6,
			SUM(CASE WHEN a.status IN ('Waiting for greenlight') THEN 1 ELSE 0 END) as C7,
			SUM(CASE WHEN a.status IN ('Documents after interview', 'Contract ready') THEN 1 ELSE 0 END) as C8,
			SUM(CASE WHEN a.status IN ('Contact/Account created') THEN 1 ELSE 0 END) as C9,
			SUM(CASE WHEN a.status IN ('Rejected') THEN 1 ELSE 0 END) as C10,
			COUNT(a.converteddate) as C11
		
		FROM
		
			salesforce.lead a


		LEFT JOIN

			bi.timelapses_recruitment_process leadtouched

		ON

			a.sfid = leadtouched.leadid
				
				
		WHERE
		
			a.createddate > '2016-06-01'
			AND a.delivery_area__c IS NOT NULL
			AND a.countrycode IN ('DE', 'AT', 'NL', 'CH')
			AND a.delivery_area__c IN ('at-vienna', 'ch-basel', 'ch-bern', 'ch-geneva', 'ch-lausanne', 'ch-lucerne', 'ch-stgallen', 'ch-zurich', 'de-berlin', 'de-bonn', 'de-cologne', 'de-dusseldorf', 'de-essen', 'de-frankfurt', 'de-hamburg', 'de-mainz', 'de-manheim', 'de-munich', 'de-nuremberg', 'de-stuttgart', 'nl-amsterdam', 'nl-hague')
			AND (a.note__c NOT LIKE '%SUBS%' OR a.note__c IS NULL)

				
		GROUP BY
		
			a.countrycode,
			a.delivery_area__c,
			-- year_created,
			-- month_created,
			year_touched,
			month_touched
				
		
		ORDER BY 
		
			a.countrycode asc,
			a.delivery_area__c asc,
			month_touched asc
			
			) t1
			
				) t2;



-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.rejection_reasons_per_polygon;
CREATE TABLE bi.rejection_reasons_per_polygon as 


SELECT

	Table1.countrycode as Country,
	Table1.polygon,
	CAST(Table1.year_created as integer),
	CAST(Table1.month_created as integer),
	date_created,
	CAST(Table1.year_touched as integer),
	CAST(Table1.month_touched as integer),
	date_touched1,

	CAST(Table1.Rejected as numeric),
	Table1.Availability as Availability_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Availability as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Availability,
	Table1.Distance as Distance_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Distance as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Distance,
	Table1.Personal_qualities as Personal_qualities_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Personal_qualities as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Personal_qualities,
	Table1.Professional_skills as Professional_skills_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Professional_skills as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Professional_skills,
	Table1.Contractual_reasons as Contractual_reasons_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Contractual_reasons as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Contractual_reasons,
	Table1.Student as Student_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Student as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Student,
	Table1.Other as Other_value,
	(CASE WHEN (CAST(Table1.Rejected as numeric) > 0) THEN ROUND((CAST(Table1.Other as numeric)/CAST(Table1.Rejected as numeric)),3) ELSE 0 END) as Other


FROM

	(SELECT

		a.countrycode,
		a.delivery_area__c as polygon,
		EXTRACT(year from a.createddate) as year_created,
		EXTRACT(month from a.createddate) as month_created,
		MIN(a.createddate::date) date_created,
		EXTRACT(year from leadtouched.date_touched) as year_touched,
		EXTRACT(month from leadtouched.date_touched) as month_touched,
		MIN(leadtouched.date_touched::date) date_touched1,

		
		SUM(CASE WHEN (a.rejection_reason__c IS NULL) THEN 0 ELSE 1 END) as Rejected,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Availability') THEN 1 ELSE 0 END) as Availability,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Distance') THEN 1 ELSE 0 END) as Distance,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('No Work Permit', 'Soft Skills', 'Failed background check', 'Punctuality / reliability', 'No show', 'Appearance', 'Not reached anymore') THEN 1 ELSE 0 END) as Personal_qualities, 

		SUM(CASE WHEN a.rejection_reason__c IN ('Language skills', 'Cleaning skills') THEN 1 ELSE 0 END) as Professional_skills,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Earnings too low', 'Found other Full-time job', 'Wants full-time', 'Minijob') THEN 1 ELSE 0 END) as Contractual_Reasons,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Student') THEN 1 ELSE 0 END) as Student,
		
		SUM(CASE WHEN a.rejection_reason__c IN ('Other (add note)') THEN 1 ELSE 0 END) as Other


	FROM

		salesforce.lead a

	LEFT JOIN

			bi.timelapses_recruitment_process leadtouched

		ON

			a.sfid = leadtouched.leadid
		
	WHERE

		a.createddate > '2016-06-01'
		AND a.delivery_area__c IS NOT NULL
		AND a.countrycode IN ('DE', 'AT', 'NL', 'CH')
		AND a.delivery_area__c IN ('at-vienna', 'ch-basel', 'ch-bern', 'ch-geneva', 'ch-lausanne', 'ch-lucerne', 'ch-stgallen', 'ch-zurich', 'de-berlin', 'de-bonn', 'de-cologne', 'de-dusseldorf', 'de-essen', 'de-frankfurt', 'de-hamburg', 'de-mainz', 'de-manheim', 'de-munich', 'de-nuremberg', 'de-stuttgart', 'nl-amsterdam', 'nl-hague')
		AND a.note__c NOT LIKE '%SUBS%'
		AND a.note__c NOT LIKE '%Subs%'
		AND a.note__c NOT LIKE '%subs%'
		
		
	GROUP BY

		a.countrycode,
		a.delivery_area__c,
		year_created,
		month_created,
		year_touched,
		month_touched

	ORDER BY 

		a.countrycode asc,
		a.delivery_area__c asc,
		year_created asc,
		month_created asc,
		year_touched asc,
		month_touched asc,
		Rejected asc,
		Availability asc,
		Distance asc,
		Personal_qualities asc,
		Professional_skills asc,
		Contractual_reasons asc,
		Student asc,
		Other asc

		) as Table1

GROUP BY

	Country,
	Table1.polygon,
	Table1.year_created,
	Table1.month_created,
	Table1.date_created,
	Table1.year_touched,
	Table1.month_touched,
	date_touched1,
	Table1.Rejected,
	Table1.Availability,
	Table1.Distance,
	Table1.Personal_qualities,
	Table1.Professional_skills,
	Table1.Contractual_reasons,
	Table1.Student,
	Table1.Other
	
ORDER BY

	Country, polygon asc, date_touched1 asc, Rejected asc, Availability asc, Distance asc, Personal_qualities asc, Professional_skills asc, Contractual_reasons asc, Student asc, Other asc;




-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'