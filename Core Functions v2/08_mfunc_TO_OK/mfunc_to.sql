CREATE OR REPLACE FUNCTION bi.mfunc_to(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.mfunc_to';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	PERFORM * FROM bi.sfunc_to_acm_dashboard(crunchdate);
	PERFORM * FROM bi.sfunc_to_cleaners_cohorts(crunchdate);
	PERFORM * FROM bi.sfunc_to_onboarding_kpis(crunchdate);
	PERFORM * FROM bi.sfunc_to_cleaner_availability(crunchdate);
	PERFORM * FROM bi.sfunc_to_cleaners_scoring(crunchdate);
	PERFORM * FROM bi.sfunc_to_planning_cleaners(crunchdate);
	PERFORM * FROM bi.sfunc_to_conversion_leads(crunchdate);

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'