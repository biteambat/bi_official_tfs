CREATE OR REPLACE FUNCTION bi.sfunc_mktg_b2c_likelies(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_mktg_b2c_likelies';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

DROP TABLE IF EXISTS bi.temp_b2c_likelies;
CREATE TABLE bi.temp_b2c_likelies AS	

	SELECT		

		t.createddate::date as likeli_creation,

		left(t.locale__c,2) as locale,

		CASE 
		        
		        WHEN (t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		        THEN 'SEM'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'SEO'::text
		       	            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN ((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
			            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
			            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text)
					
					OR ((t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text)

					OR ((t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text))

				THEN 'Brand Marketing'::text
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		COUNT(1) as nb_likelies

	FROM
	Salesforce.likeli__c t

	WHERE
	t.createddate::date >= '2016-03-01'
	AND t.acquisition_tracking_id__c = 'appbooking'
	
	GROUP BY locale, likeli_creation, source_channel
	ORDER BY likeli_creation desc, locale asc, source_channel asc

;

DROP TABLE IF EXISTS bi.b2c_cost_per_likelies;
CREATE TABLE bi.b2c_cost_per_likelies AS

	SELECT 
		t1.likeli_creation as date,
		t1.locale,
		t1.source_channel,
		t1.nb_likelies,
		t2.cost

	FROM bi.temp_b2c_likelies t1
		JOIN bi.coststransform t2
			ON t1.likeli_creation = t2.dates
			AND t1.locale = t2.locale
			AND t1.source_channel = t2.channel
			AND t1.source_channel IN ('Brand Marketing','SEM','SEO','Display','Facebook')


;

DROP TABLE IF EXISTS bi.b2c_likelies_conversion;

CREATE TABLE bi.b2c_likelies_conversion AS

	SELECT
	c.*,
	SUM(a.acquisitions) as nb_acquisitions,
	ROUND(SUM(a.acquisitions)::numeric/c.nb_likelies,4) as conversion_ratio

	FROM bi.b2c_cost_per_likelies c
		
		JOIN bi.acquisitions_last100d_channel a 
			ON c.date = a.orderdate
			AND c.locale = a.country
			AND c.source_channel = (CASE WHEN a.marketing_channel in ('DTI','SEM Brand','SEO Brand') THEN 'Brand Marketing'
															ELSE a.marketing_channel END)
			AND c.date < current_date

	GROUP BY c.date, c.locale, c.source_channel, c.nb_likelies, c.cost

	ORDER BY c.date desc, c.locale asc, c.source_channel asc

;

DROP TABLE IF EXISTS bi.temp_b2c_likelies;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'