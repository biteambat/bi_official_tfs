CREATE OR REPLACE FUNCTION bi.sfunc_mktg_frfprogram(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_mktg_frfprogram';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	DROP TABLE IF EXISTS bi.frf_tracking_1;
	CREATE TABLE bi.frf_tracking_1 as 
	SELECT
		Order_Id__c,
		referred_by__c,
		Order_Creation__c,
		Voucher__c,
		status
	FROM
		bi.orders
	WHERE
		Voucher__c = 'FRF'
		and referred_by__c is not null;

	DROP TABLE IF EXISTS bi.frf_tracking;
	CREATE TABLE bi.frf_tracking as 
	SELECT
		t2.sfid as salesforce_id,
		t2.name as Referrer,
		SUM(CASE WHEN t1.status not like '%CANCELLED%' THEN 1 ELSE 0 END) as Valid_Referred_Orders,
		SUM(CASE WHEN t1.status like '%CANCELLED%' THEN 1 ELSE 0 END) as Cancelled_Referred_Orders,
		SUM(CASE WHEN t1.status not like '%CANCELLED%' THEN 1 ELSE 0 END) +SUM(CASE WHEN t1.status like '%CANCELLED%' THEN 1 ELSE 0 END) as All_Orders
	FROM
		bi.frf_tracking_1 t1
	JOIN
		salesforce.contact t2
	ON
		(t1.referred_by__c = t2.sfid)
	GROUP BY
		t2.sfid,
		t2.name;

	DROP TABLE IF EXISTS bi.frf_tracking_1;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;
$BODY$ LANGUAGE 'plpgsql'