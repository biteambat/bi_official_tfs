﻿CREATE OR REPLACE FUNCTION bi.sfunc_mktg_fbleadgen(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_mktg_fbleadgen';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


DROP TABLE IF EXISTS bi.fb_voucher_acquisitions;
CREATE TABLE bi.fb_voucher_acquisitions AS

SELECT

  o.order_id__c as order_id,
  order_creation__c::timestamp::date as order_created,
  left(o.delivery_area,2) as locale,
  replace(o.delivery_area,'+','') as city,
  CASE WHEN (o.acquisition_channel_params__c like ('%"utm_source":"facebook"%') and o.acquisition_channel_params__c like ('%"utm_medium":"email"%')) THEN 'Facebook - after emailing'
    WHEN (o.acquisition_channel_params__c like ('%"utm_source":"facebook"%') and o.acquisition_channel_params__c like ('%"utm_medium":"display"%')) THEN 'Facebook - display ad'
    WHEN (o.acquisition_channel_params__c like '{}' or o.acquisition_channel_params__c like ('{"ref":"bookatiger.de"}') or o.acquisition_channel_params__c like ('{"ref":"bookatiger.nl"}') or o.acquisition_channel_params__c like ('{"ref":"bookatiger.at"}') or o.acquisition_channel_params__c like ('{"ref":"bookatiger.ch"}')) THEN 'DTI'
    WHEN (o.acquisition_channel_params__c like ('{"clid":"goob","gkey":"bookatiger","gclid":"%"}') or (o.acquisition_channel_params__c like ('{"clid":"goog","gclid":"%"}'))) THEN 'SEM'
    ELSE ('Other') END as acquisition_tracking,

    CASE WHEN o.voucher__c like ('FBML%') THEN 'Facebook - after emailing'
      WHEN o.voucher__c NOT like ('FBML%') THEN 'Facebook - display ad'
    END as acquisition_tracking_2,
    
  o.voucher__c as voucher,
  acquisition_channel_params__c as acquisition_tag

FROM bi.orders o

WHERE o.marketing_channel = 'Facebook'
  and o.voucher__c in ('WISCHMOP3C','SPRINGFB20','SPRINGFB50','SPRINGFB3P','WISCH1FB','RABATT20','FBPRIMA20','FBPUTZCH40','FBPUTZ20','TIGER1FB','FBTIGER15','FBPUTZ15','FBTIGER50','FBTIGER20','STAR50','FBTIGER30','FBTIGER40','FBTIGER15','FBTIG70','FBML15','FBML20','FBML25','FBML30','FBML50','FBML70')
  and o.test__c = '0'
  and o.status not in ('CANCELLED FAKED','CANCELLED MISTAKE')
  and o.acquisition_new_customer__c = '1'
    and order_type = '1'

ORDER BY order_created desc
;

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;



$BODY$ LANGUAGE 'plpgsql'