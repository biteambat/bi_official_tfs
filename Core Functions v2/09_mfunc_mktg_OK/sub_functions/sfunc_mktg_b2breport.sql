CREATE OR REPLACE FUNCTION bi.sfunc_mktg_dailyreport(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_mktg_dailyreport';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--Net executed GMV

DROP TABLE IF EXISTS bi.temp_b2b_gmv;
CREATE TABLE bi.temp_b2b_gmv AS

		(
		SELECT

			'All' as locale,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END) as gmv_lw,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) < (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END)::numeric/4 as gmv_l4w

		FROM bi.orders

		WHERE test__c = '0'
			AND status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL', 'PENDING TO START', 'FULFILLED')
			AND order_type = '2'
			AND EXTRACT(WEEK from effectivedate) >= (EXTRACT(WEEK from current_date) - 5)
			AND EXTRACT(WEEK from effectivedate) < EXTRACT(WEEK from current_date)
			AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)
		)

	UNION

		(
		SELECT

			LEFT(locale__c, 2) as locale,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) = (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END) as gmv_lw,
			SUM(CASE WHEN EXTRACT(WEEK FROM effectivedate) < (EXTRACT(WEEK FROM current_date)-1) THEN gmv_eur_net ELSE 0 END)::numeric/4 as gmv_l4w

		FROM bi.orders

		WHERE test__c = '0'
			AND status IN ('INVOICED', 'NOSHOW CUSTOMER', 'NOSHOW PROFESSIONAL', 'PENDING TO START', 'FULFILLED')
			AND order_type = '2'
			AND EXTRACT(WEEK from effectivedate) >= (EXTRACT(WEEK from current_date) - 5)
			AND EXTRACT(WEEK from effectivedate) < EXTRACT(WEEK from current_date)
			AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)

		GROUP BY 
			LEFT(locale__c, 2)

		ORDER BY locale asc)

;

--New deals signed

DROP TABLE IF EXISTS bi.temp_b2b_deals;
CREATE TABLE bi.temp_b2b_deals AS

	(
	SELECT
		LEFT(locale__c, 2) as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as deals_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as deals_l4w


	FROM salesforce.opportunity

	WHERE stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
		AND test__c = '0'
		AND EXTRACT(WEEK from closedate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from closedate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)

	GROUP BY
		LEFT(locale__c, 2)
	ORDER BY
		locale asc
	)

	UNION

	(
	SELECT
		'All' as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as deals_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from closedate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as deals_l4w

	FROM salesforce.opportunity

	WHERE stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED')
		AND test__c = '0'
		AND EXTRACT(WEEK from closedate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from closedate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from closedate) = EXTRACT(YEAR from current_date)
	)


--# Inbound leads

DROP TABLE IF EXISTS bi.temp_b2b_leads;
CREATE TABLE bi.temp_b2b_leads AS

	(
	SELECT
		LEFT(locale__c, 2) as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as leads_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as leads_l4w

	FROM salesforce.likeli__c

	WHERE type__c = 'B2B'
		AND EXTRACT(WEEK from createddate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from createddate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from createddate) = EXTRACT(YEAR from current_date)
		AND locale__c IS NOT NULL
		AND locale__c NOT LIKE ('at-%')
		AND acquisition_channel__c IN ('web', 'inbound')

	GROUP BY LEFT(locale__c, 2)

	ORDER BY locale asc
	)

	UNION

	(
	SELECT
		'All' as locale,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) = (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END) as leads_lw,
		COUNT(DISTINCT CASE WHEN EXTRACT(WEEK from createddate) < (EXTRACT(WEEK from current_date) - 1) THEN id ELSE NULL END)::numeric/4 as leads_l4w

	FROM salesforce.likeli__c

	WHERE type__c = 'B2B'
		AND EXTRACT(WEEK from createddate) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from createddate) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from createddate) = EXTRACT(YEAR from current_date)
		AND locale__c IS NOT NULL
		AND locale__c NOT LIKE ('at-%')
		AND acquisition_channel__c IN ('web', 'inbound')
	)


--Inbound Leads Conversion Rate

	(SELECT
		locale,

		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_lw,

		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_l4w

	FROM bi.b2b_likelie_cvr_weekly

	WHERE EXTRACT(WEEK from date) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from date) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from date) = EXTRACT(YEAR from current_date)

	GROUP BY locale

	ORDER BY locale)

	UNION

	(SELECT
		'All' as locale,

		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) = (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_lw,

		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN opportunities ELSE 0 END)::numeric
		/
		SUM(CASE WHEN EXTRACT(WEEK from date) < (EXTRACT(WEEK from current_date) - 1) THEN likelies ELSE 0 END) as cvr_l4w

	FROM bi.b2b_likelie_cvr_weekly

	WHERE EXTRACT(WEEK from date) >= (EXTRACT(WEEK from current_date) - 5)
		AND EXTRACT(WEEK from date) < EXTRACT(WEEK from current_date)
		AND EXTRACT(YEAR from date) = EXTRACT(YEAR from current_date)
	)


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$
LANGUAGE plpgsql VOLATILE