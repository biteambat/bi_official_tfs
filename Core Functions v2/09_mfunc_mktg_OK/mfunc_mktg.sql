CREATE OR REPLACE FUNCTION bi.mfunc_mktg(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.mfunc_mktg';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

	PERFORM * FROM bi.sfunc_mktg_acquisitions_l100d(crunchdate);
	PERFORM * FROM bi.sfunc_mktg_b2c_likelies(crunchdate);
	PERFORM * FROM bi.sfunc_mktg_coops(crunchdate);
	PERFORM * FROM bi.sfunc_mktg_dailyreport(crunchdate);
	PERFORM * FROM bi.sfunc_mktg_fbleadgen(crunchdate);
	PERFORM * FROM bi.sfunc_mktg_frfprogram(crunchdate);

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'