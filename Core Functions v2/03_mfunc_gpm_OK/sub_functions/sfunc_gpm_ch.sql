DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_gpm_ch(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_gpm_ch';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


-- CLEANER STATS TEMP

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 

SELECT

	a.name,
	a.sfid,	
	a.status__c,
	a.sfid as professional__c,
	-- a.delivery_areas__c as delivery_areas,
	CASE WHEN a.delivery_areas__c IS NULL THEN CASE WHEN a.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN a.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN a.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE a.delivery_areas__c
											END as delivery_areas,
	a.type__c,
	a.hr_contract_start__c::date as contract_start,
	a.hr_contract_end__c::date as contract_end,	
	a.hr_contract_weekly_hours_min__c as weekly_hours
	
FROM

   Salesforce.Account a
   
WHERE 

	a.test__c = '0' AND a.name NOT LIKE '%test%' AND a.name NOT LIKE '%Test%' AND a.name NOT LIKE '%TEST%'
	-- AND (a.type__c LIKE 'cleaning-b2c' OR (a.type__c LIKE '%cleaning-b2c;cleaning-b2b%') OR a.type__c LIKE 'cleaning-b2b')
	AND a.hr_contract_weekly_hours_min__c IS NOT NULL
	AND  a.test__c = '0'
	AND LEFT(a.delivery_areas__c,2) = 'ch'

GROUP BY 

   a.name,
   a.sfid,
   a.status__c,
   professional__c,
   delivery_areas,
   a.type__c,
	contract_start,
	contract_end,
	weekly_hours,
	locale__c;
	

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
CREATE TABLE bi.gmp_per_cleaner_temp as 

SELECT

	t1.professional__c,
	-- t1.polygon as delivery_area__c,
	CASE WHEN t1.polygon IS NULL THEN CASE WHEN t1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN t1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN t1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE t1.polygon
											END as delivery_areas__c,
	Effectivedate::date as date,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date  THEN gmv_eur
	         WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN gmv_eur*1.2 
				ELSE 0 END) as GMV,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date THEN gmv_eur ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN gmv_eur*1.2 ELSE 0 END) as b2b_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date  THEN order_Duration__c ELSE 0 END) + SUM(CASE WHEN (type = 'cleaning-b2b') AND Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60' or type = 'training') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2c_hours,
	SUM(CASE WHEN (type = 'cleaning-b2b') AND Status IN  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2b_hours
		
FROM

	bi.orders t1
	
WHERE

	t1.professional__c IS NOT NULL
	
GROUP BY

	t1.professional__c,
	t1.polygon,
	date,
	locale__c;
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GMP PER CLEANER V1 TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
CREATE TABLE bi.gmp_per_cleaner_v1_temp as 

SELECT

	t1.name,
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	t2.gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours
	
FROM

	bi.cleaner_Stats_temp t1
	
LEFT JOIN

	bi.gmp_per_cleaner_temp t2
	
ON

	(t1.professional__c = t2.professional__c);

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER V2

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_temp as 

SELECT 

	t2.*, 
	GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
	CASE
	WHEN year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
	WHEN to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
	ELSE calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
	END AS days_worked
	
FROM (

	SELECT	
	
	   to_char(date,'YYYY-MM') as Year_Month,
	   MIN(date) as mindate,
	   t1.name,
		professional__c,
		delivery_areas as delivery_area,
		DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
		contract_start,
		contract_end,
		type__c,
		SUM(hours) as worked_hours,
		CAST(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
		SUM(GMV) as GMV,
		SUM(B2C_GMV/1.08) + SUM(B2B_GMV/1.2) as total_revenue,
		SUM(B2C_GMV/1.08) as B2C_Revenue,
		SUM(B2B_GMV/1.2) as B2B_Revenue,	
		SUM(b2c_hours) as b2c_hours,
		SUM(b2b_hours) as b2b_hours,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(Hours)) ELSE 0 END as b2b_share,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(Hours)) ELSE 0 END as b2c_share,
		MAX(Weekly_hours) as weekly_hours,
		4 as monthly_hours
		
	FROM
	
		bi.gmp_per_cleaner_v1_temp t1
		
	WHERE
	
		date >= '2016-01-01'
		
	GROUP BY
	
	   Year_Month,
		professional__c,
		t1.name,
		contract_start,
		days_of_month,
		type__c,
		contract_end,
		delivery_area,
		calc_end
		) as t2;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
CREATE TABLE bi.holidays_cleaner_temp as

SELECT

	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
	
FROM

	salesforce.hr__c
	
WHERE

	type__c != 'unpaid';
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
CREATE TABLE bi.holidays_cleaner_2_temp as 

SELECT

	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
	
FROM

	bi.holidays_cleaner_temp,
	bi.orderdate
	
WHERE

	date > '2016-01-01'
	AND status__c in ('approved');

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
CREATE TABLE bi.holidays_cleaner_3_temp as 

SELECT

	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
	
FROM

	bi.holidays_cleaner_2_temp
	
WHERE

	weekday != '0'
	AND date < current_date
	
GROUP BY

	date,
	account__c;	

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
CREATE TABLE bi.holidays_cleaner_4_temp as 

SELECT

	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(holiday_flag) as holiday,
	SUM(sickness_flag) as sickness
	
FROM

	bi.holidays_cleaner_3_temp
	
GROUP BY

	Year_Month,
	account__c;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_2_temp as 

SELECT

	t1.*,
	CASE WHEN holiday is null then 0 ELSE holiday END as holiday,
	CASE WHEN sickness is null then 0 ELSE sickness END as sickness
	
FROM

	bi.gpm_per_cleaner_v2_temp t1
	
LEFT JOIN

	 bi.holidays_cleaner_4_temp t2
	 
ON

	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month)
	
ORDER BY

	t1.year_month,
	t1.name;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
	MIN(mindate) as mindate,
   professional__c,
   type__c,
   delivery_area,
   worked_hours,
   contract_start,
   contract_end,
   monthly_hours,
   weekly_hours,
   days_worked,
   days_of_month,
   sickness,
   -- MIN(mindate) as mindate,
   holiday,
   (weekly_hours/5)*(sickness+holiday) as absence_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	CASE WHEN ((weekly_hours/5)*(sickness+holiday))+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours) ELSE ((monthly_hours/days_of_month)*days_worked) END as total_hours,

	
	CASE WHEN mindate < '2015-10-01' THEN
	
			(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.96
				
			WHEN (mindate >= '2015-10-01' AND mindate < '2017-12-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.92
				
			WHEN (mindate >= '2017-12-01' AND mindate < '2018-01-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.85696
				
			WHEN (mindate >= '2018-01-01' AND mindate < '2018-02-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.85497
				
			WHEN (mindate >= '2018-02-01' AND mindate < '2018-03-01') THEN
			
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.86243
				
			ELSE 
		
				(CASE WHEN CAST(EXTRACT(DAY FROM current_date) as int) < 7
				 THEN
					CASE WHEN worked_hours < 1 THEN 0
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (7 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 14)
				THEN	 
					CASE WHEN worked_hours < 1 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 27.1 ELSE 26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (14 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 21)
				THEN	
					CASE WHEN worked_hours < 2 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 2*27.1 ELSE 2*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN (21 <= CAST(EXTRACT(DAY FROM current_date) as int) AND CAST(EXTRACT(DAY FROM current_date) as int) < 28)
				THEN	
					CASE WHEN worked_hours < 3 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 3*27.1 ELSE 3*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				WHEN CAST(EXTRACT(DAY FROM current_date) as int) >= 28
				THEN	
					CASE WHEN worked_hours < 4 THEN (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 4*27.1 ELSE 4*26.6 END)
						 ELSE (CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)
						 END
				ELSE 0
				END) * 0.86721
				
			END
			
		as salary_payed,
	
	
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	B2C_share,
	B2B_share,
	
	
	--------------------------------------------- OLD VERSION
	
	CASE WHEN mindate < '2015-10-01' THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.96
			
		 WHEN (mindate >= '2015-10-01' AND mindate < '2017-12-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.92
			
		 WHEN (mindate >= '2017-12-01' AND mindate < '2018-01-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.85696
			
		 WHEN (mindate >= '2018-01-01' AND mindate < '2018-02-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.85497
			
		 WHEN (mindate >= '2018-02-01' AND mindate < '2018-03-01') THEN
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.86243
			
		 ELSE
		 
		 	SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2b_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2b_share) END)*0.86721
			
		END	
		
		
		as b2b_gp,
		
	
	CASE WHEN mindate < '2015-10-01' THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.96
			
		WHEN (mindate >= '2015-10-01' AND mindate < '2017-12-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.92
			
		WHEN (mindate >= '2017-12-01' AND mindate < '2018-01-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.85696
			
		WHEN (mindate >= '2018-01-01' AND mindate < '2018-02-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.85497
			
		WHEN (mindate >= '2018-02-01' AND mindate < '2018-03-01') THEN
	
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.86243
			
		ELSE
		
			SUM(CASE 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))/2 
			WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END))
			ELSE b2c_revenue-((CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN worked_hours*27.1 ELSE worked_hours*26.6 END)*b2c_share) END)*0.86721
			
		END
	
		as b2c_gp,
	
	-- CASE WHEN (SUM(B2C_Revenue) + SUM(B2B_Revenue)) > 0
		  -- THEN ((SUM(B2C_Revenue) + SUM(B2B_Revenue)) - SUM(CASE WHEN (weekly_hours/5)*(sickness+holiday)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) ELSE (monthly_hours/days_of_month)*days_worked*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) END))/(SUM(B2C_Revenue) + SUM(B2B_Revenue))
		  -- ELSE 0
		  -- END as gpm_ch,
		  
	(SUM(B2C_Revenue) + SUM(B2B_Revenue)) - SUM(CASE WHEN (weekly_hours/5)*(sickness+holiday)+worked_hours > (monthly_hours/days_of_month)*days_worked THEN ((weekly_hours/5)*(sickness+holiday)+worked_hours)*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) ELSE (monthly_hours/days_of_month)*days_worked*(CASE WHEN delivery_area IN ('de-geneva','de-lausanne') THEN 25.03 ELSE 24.45 END) END)  as gp,
	SUM(GMV) as GMV,
	(SUM(B2C_Revenue) + SUM(B2B_Revenue)) as Revenue	
	
FROM

     bi.gpm_per_cleaner_v2_2_temp
     
WHERE

    LEFT(delivery_area,2) = 'ch'
    AND days_worked > '0'
    
GROUP BY
    year_month,
    mindate,
    professional__c,
    type__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    B2C_share,
	 B2B_share,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    days_worked,
    worked_hours;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v4_temp;
CREATE TABLE bi.gpm_per_cleaner_v4_temp as 


SELECT

	year_month,
	MIN(mindate) as mindate,
   professional__c,
   delivery_area,
   worked_hours,
   contract_start,
   contract_end,
   monthly_hours,
   weekly_hours,
   days_worked,
   days_of_month,
   sickness,
   -- mindate,
   holiday,
   absence_hours,
   working_hours,
	total_hours,
	salary_payed,
	B2C_Revenue,
	B2B_Revenue,
	
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (B2B_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (B2C_revenue-salary_payed)
	ELSE b2b_revenue-(salary_payed*b2b_share) END) as b2b_gp,
		SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-salary_payed)
	ELSE b2c_revenue-(salary_payed*b2c_share) END) as b2c_gp,

	
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (B2B_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (B2C_revenue-salary_payed)
	ELSE b2b_revenue-(salary_payed*b2b_share) END) 
	+
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-salary_payed)
	ELSE b2c_revenue-(salary_payed*b2c_share) END) as gp,
	
	GMV,
	Revenue
		
FROM

	bi.gpm_per_cleaner_v3_temp_temp
	
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    absence_hours,
    days_worked,
    worked_hours,
	 days_of_month,
    sickness,
    mindate,
    holiday,
    absence_hours,
    working_hours,
	 total_hours,
	 salary_payed,
	 B2C_Revenue,
	 B2B_Revenue,
	gmv,
	revenue;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.order_distribution_temp;
CREATE TABLE bi.order_distribution_temp as 

SELECT

	professional__c,
	to_char(effectivedate::date,'YYYY-MM') as Month,
	SUM(CASE WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and EXTRACT(HOUR FROM Order_End__c+ Interval '2 hours') < 13 THEN Order_Duration__c
				WHEN EXTRACT(HOUR FROM Order_Start__c+ Interval '2 hours') < 13 and extract(hour from order_end__c+ Interval '2 hours') > 13 THEN 13 - EXTRACT(HOUR FROM order_Start__c+ Interval '2 hours') ELSE 0 END) as Working_Hours_Morning,
	SUM(Order_Duration__c) as total_hours
	
FROM

	bi.orders
	
WHERE

	test__c = '0'
	and status = 'INVOICED'
	
GROUP BY

	professional__c,
	month;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;
CREATE TABLE bi.cleaner_ur_temp_temp as	

SELECT 

	sfid,
	SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as availability_morning,
	SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) as availability_total,
	CASE WHEN SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) = '0' THEN 0 ELSE CAST(SUM(availability_monday+availability_tuesday+availability_wednesday+availability_thursday+availability_friday+availability_saturday) as decimal)/SUM(total_monday+total_tuesday+total_wednesday+total_thursday+total_friday+total_saturday) END as availability_Share

FROM

	bi.cleaner_availability_morning
	
GROUP BY

	sfid;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_ch;
CREATE TABLE bi.gpm_ch as

SELECT

	t2.name,
	CAST(t2.rating__c as decimal) as rating,
	t5.Score_cleaners,
	CASE WHEN t2.hr_contract_start__c >= (current_date - 10) THEN 'New' ELSE 'Old' END as flag_new,
	t5.category,
	t5.existence,
	t2.type__c,
	t1.*,
	CASE WHEN worked_hours >= working_hours THEN working_hours else worked_hours END as capped_work_hours,
	CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END as contract_morning_hours,
	t3.Working_Hours_Morning,
	availability_Share,
	CASE WHEN availability_share is null or availability_share = 0 THEN 0 ELSE cast(t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*availability_Share END) END as morning_ur,
	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END as contract_hours_afternoon,
	t3.total_hours-t3.working_hours_morning as working_hours_afternoon,
	CASE WHEN (1-availability_Share) is null or (1-availability_Share) = 0 THEN 0 ELSE cast(t3.total_hours-t3.working_hours_morning as decimal)/(CASE WHEN availability_Share is null THEN 0 ELSE working_hours*(1-availability_Share) END) END as afternoon_ur,
	CASE WHEN availability_Share is null THEN 0 ELSE CASE WHEN t3.Working_Hours_Morning > working_hours*availability_Share THEN working_hours*availability_Share ELSE t3.Working_Hours_Morning END END capped_hours_morning,
	CASE WHEN (1-availability_Share) = 0 THEN 0 ELSE CASE WHEN t3.total_hours-t3.working_hours_morning  > working_hours*	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END THEN working_hours*(	CASE WHEN availability_share is null THEN 1 ELSE working_hours*(1-availability_Share) END) ELSE t3.total_hours-t3.working_hours_morning END END capped_hours_afternoon

FROM

	bi.gpm_per_cleaner_v4_temp t1
	
LEFT JOIN
	Salesforce.Account t2	
ON
	(t1.professional__c = t2.sfid)
	
LEFT JOIN
	bi.order_distribution_temp t3	
ON
	(t1.professional__c = t3.professional__c and t1.year_month = t3.month)
	
LEFT JOIN
	bi.cleaner_ur_temp_temp t4	
ON
	(t1.professional__c = t4.sfid)
	
LEFT JOIN
	bi.Performance_cleaners t5	
ON
	(t1.professional__c = t5.sfid);

	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v4_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'