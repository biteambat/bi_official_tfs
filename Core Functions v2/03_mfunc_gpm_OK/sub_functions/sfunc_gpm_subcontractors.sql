DELIMITER //
CREATE OR REPLACE FUNCTION bi.sfunc_gpm_subcontractors(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_gpm_subcontractors';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN 

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.account_temp;
CREATE TABLE bi.account_temp as 

SELECT  

   t1.name,
   t1.company_name__c,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.delivery_areas__c,
   CASE WHEN t1.company_name__c LIKE '%Ali Kocahal%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Akzent Facility Services GmbH%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Sadettin Akal%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%ATIS Gebäudereinigung de- Cologne%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%blitzgebäudereinigung de-Hamburg%' THEN 'de-hamburg'
		  WHEN t1.company_name__c LIKE '%Kostic%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Crystal Facility Service GmbH de-berlin%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Herr Kovacs%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Jefa Gebäudereinigung%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%KNM Gebäudeservice GmbH%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%Medical Clean Management GmbH de-munich%' THEN 'de-munich'
		  WHEN t1.company_name__c LIKE '%Dordevic%' THEN 'de-munich'
		  WHEN t1.company_name__c LIKE '%Stamm´s Haus & Hof Service%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%TMG Gebäudereinigung und Service de-stuttgart%' THEN 'de-stuttgart'
		  WHEN t1.company_name__c LIKE '%1636583753%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Kovacs%' THEN 'de-berlin'
		  WHEN t1.company_name__c LIKE '%Korkmaz%' THEN 'de-frankfurt'
		  WHEN t1.company_name__c LIKE '%Czerwinski%' THEN 'de-cologne'
		  WHEN t1.company_name__c LIKE '%Kordian%' THEN 'de-frankfurt'
	ELSE 
		CASE WHEN t1.delivery_areas__c IS NULL THEN CASE WHEN t1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN t1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN t1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE t1.delivery_areas__c
											END
										
	END as new_areas,
	t1.longitude__c,
	t1.latitude__c
     
FROM
 
   Salesforce.account t1
WHERE
	t1.type__c = 'cleaning-b2b'
      
GROUP BY
 
   t1.name,
   t1.company_name__c,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.delivery_areas__c,
   t1.longitude__c,
	t1.latitude__c,
   new_areas,
   t1.locale__c;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.cleaner_geoloc_temp;
CREATE TABLE bi.cleaner_geoloc_temp as 

SELECT  

   t1.name,
   t1.sfid,
   t1.parentid,
   t1.new_areas,
   t1.pph__c,
   CASE WHEN t1.new_areas IS NOT NULL THEN t1.new_areas ELSE t2.key__c END as delivery_area,
   ST_Intersects( 
   ST_SetSRID(ST_Point(t1.longitude__c::float, t1.latitude__c::float), 4326), 
   ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) as flag_polygon
     
FROM
 
   bi.account_temp t1,
   salesforce.delivery_area__c t2
      
GROUP BY
 
   t1.name,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.new_areas,
	delivery_area,
	flag_polygon
     
HAVING
 
    ST_Intersects(
    ST_SetSRID(ST_Point(t1.longitude__c::float, t1.latitude__c::float), 4326), 
    ST_SetSRID(ST_GeomFromGeoJSON(t2.polygon__c::json#>>'{features,0,geometry}'), 4326)) = true
    
UNION

	SELECT  

   t1.name,
   t1.sfid,
   t1.parentid,
   t1.new_areas,
   t1.pph__c,
   t1.new_areas,
 	TRUE as flag_polygon
     
FROM
 
   bi.account_temp t1
      
GROUP BY
 
   t1.name,
   t1.sfid,
   t1.parentid,
   t1.pph__c,
   t1.new_areas,
	flag_polygon
     
HAVING
     	t1.New_areas IS NOT NULL;
	 

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


-- CLEANER STATS TEMP

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	t1.name,
	t1.sfid as cleanerid,
	t1.parentid,
	t2.company_name__c,
	t2.parentid as sub_id,
	t2.sfid,
	t2.status__c,
	t1.delivery_area as delivery_areas,
	t2.type__c,
	t2.hr_contract_start__c::date as contract_start,
	t2.hr_contract_end__c::date as contract_end,
	0 as weekly_hours,	
	
	t2.pph__c as pph_subcontractor,
	t1.pph__c as pph_cleaner
FROM
	 bi.cleaner_geoloc_temp t1
LEFT JOIN
	Salesforce.Account t2
ON
	(t1.parentid = t2.sfid)
WHERE 

	(t1.name NOT LIKE '%BAT%' AND t1.name NOT LIKE '%BOOK%') 
	AND t2.test__c = '0' 
	AND t2.name NOT LIKE '%test%' AND t2.name NOT LIKE '%Test%' AND t2.name NOT LIKE '%TEST%'
	AND t2.type__c = 'cleaning-b2b'
	AND t1.name NOT LIKE '%FreshFolds%';
	
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
CREATE TABLE bi.gmp_per_cleaner_temp as 

SELECT

	t1.professional__c,
	t1.polygon,
	Effectivedate::date as date,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN GMV__c
	         WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN GMV_eur_net 
				ELSE 0 END) as GMV,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN GMV__c ELSE 0 END) as b2c_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2b') AND effectivedate::date < current_Date THEN GMV_eur_net*1.19 ELSE 0 END) as b2b_gmv,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date  THEN order_Duration__c ELSE 0 END) + SUM(CASE WHEN (type = 'cleaning-b2b') AND Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status IN ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND (type = 'cleaning-b2c' OR type = '60') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2c_hours,
	SUM(CASE WHEN (type = 'cleaning-b2b') AND Status IN  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') AND effectivedate::date < current_Date THEN order_Duration__c ELSE 0 END) as b2b_hours
		
FROM

	bi.orders t1
	
WHERE

	t1.professional__c IS NOT NULL
	
GROUP BY

	t1.professional__c,
	t1.polygon,
	date;
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GMP PER CLEANER V1 TEMP

DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
CREATE TABLE bi.gmp_per_cleaner_v1_temp as 

SELECT

	t1.name,
	t1.company_name__c,
	t1.cleanerid as sfid,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	type__c,
	date,
	t2.gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours,
	CASE WHEN t1.pph_cleaner IS NOT NULL THEN t1.pph_cleaner ELSE t1.pph_subcontractor END as pph__c
	
FROM

	bi.cleaner_Stats_temp t1
	
LEFT JOIN

	bi.gmp_per_cleaner_temp t2
	
ON

	(t1.cleanerid = t2.professional__c);

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

-- GPM PER CLEANER V2

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
CREATE TABLE bi.gpm_per_cleaner_v2_temp as 

SELECT 

	t2.*, 
	GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
	CASE
	WHEN year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
	WHEN to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
	ELSE calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
	END AS days_worked
	
FROM (

	SELECT	
	
	   to_char(date,'YYYY-MM') as Year_Month,
	   MIN(date) as mindate,
	   t1.name,
	   t1.sfid,
		t1.sfid as professional__c,
		
		CASE WHEN date >= '2018-01-01' THEN t1.pph__c 
		
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.85
			  WHEN (date < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.pph__c
			  
			  END as pph__c,
		delivery_areas as delivery_area,
		DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
		contract_start,
		contract_end,
		type__c,
		SUM(hours) as worked_hours,
		CAST(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
		SUM(GMV) as GMV,
		SUM(GMV) as total_revenue,
		SUM(B2C_GMV) as B2C_Revenue,
		SUM(B2B_GMV) as B2B_Revenue,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2b_share,
		CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(HOurs)) ELSE 0 END as b2c_share,
		MAX(Weekly_hours) as weekly_hours,
		MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL - DATE_TRUNC('month', DATE))/7 )as monthly_hours
		
	FROM
	
		bi.gmp_per_cleaner_v1_temp t1
		
	WHERE
	
		date >= '2016-01-01'
		
	GROUP BY
	
	   Year_Month,
		professional__c,
		t1.sfid,
		t1.date,
		t1.name,
		t1.company_name__c,
		t1.pph__c,
		contract_start,
		days_of_month,
		type__c,
		contract_end,
		delivery_area,
		calc_end
		) as t2;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp_temp as 

SELECT

	year_month,
   professional__c,
   pph__c,
   delivery_area,
   worked_hours,
   type__c,
   contract_start,
   contract_end,
   monthly_hours,
   weekly_hours,
   days_worked,
   days_of_month,
   MIN(mindate) as mindate,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	worked_hours*pph__c as salary_payed,
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV) - SUM(worked_hours*pph__c)  as gp,
    CASE WHEN SUM(GMV) > 0 THEN (SUM(GMV) - SUM(worked_hours*pph__c))/SUM(GMV) ELSE 0 END as gpm_subcontractor

FROM

     bi.gpm_per_cleaner_v2_temp
     
WHERE

    LEFT(delivery_area,2) IN ('de', 'nl', 'ch')
    AND days_worked > '0'
    
GROUP BY
    year_month,
    professional__c,
    pph__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    days_worked,
    worked_hours;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.monitoring_subcontractor;
CREATE TABLE bi.monitoring_subcontractor as

SELECT

	big_table.year_month,
	big_table.mindate,
	big_table.polygon,
	big_table.name_partner,
	big_table.sfid_partner,
	big_table.company_name__c,
	big_table.cost_sub_per_hour,
	big_table.number_employees_tot,
	small_table.cleaners_platform as number_cleaners_platform,
	-- CASE WHEN big_table.number_employees_tot > 0 THEN ROUND(small_table.cleaners_platform/big_table.number_employees_tot::numeric, 2) ELSE 0 END as share_partner,
	MAX(big_table.number_cleaners_active) as number_cleaners_active,
	-- CASE WHEN big_table.number_cleaners_platform > 0 THEN ROUND(big_table.number_cleaners_active/small_table.cleaners_platform::numeric, 2) ELSE 0 END as share_active_platform,
	MAX(big_table.number_customers) as number_customers,
	big_table.hours_executed,
	big_table.avg_hours_cleaner,
	big_table.avg_hours_customer,
	big_table.revenue,
	big_table.cost_sub,
	big_table.gp,
	big_table.gpm


FROM

(SELECT

	finaltable1.year_month,
	finaltable1.mindate,
	finaltable1.polygon,
	finaltable1.name_partner,
	finaltable1.sfid_partner,
	finaltable1.company_name__c,
	finaltable1.cost_sub_per_hour,
	finaltable1.number_employees_tot,
	finaltable1.number_cleaners_platform,
	-- CASE WHEN finaltable1.number_employees_tot > 0 THEN ROUND(finaltable1.number_cleaners_platform/finaltable1.number_employees_tot::numeric, 2) ELSE 0 END as share_partner,
	finaltable2.number_cleaners_platform as number_cleaners_active,
	-- CASE WHEN finaltable1.number_cleaners_platform > 0 THEN ROUND(finaltable2.number_cleaners_platform/finaltable1.number_cleaners_platform::numeric, 2) ELSE 0 END as share_active_platform,
	finaltable1.number_customers,
	finaltable1.hours_executed,
	finaltable1.avg_hours_cleaner,
	finaltable1.avg_hours_customer,
	finaltable1.revenue,
	finaltable1.cost_sub,
	finaltable1.gp,
	finaltable1.gpm

FROM

	(SELECT
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
	
	FROM
	
		(SELECT
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			cost_sub_per_hour,
			number_employees_tot,
			COUNT(DISTINCT name_cleaner) as number_cleaners_platform,
			-- CASE WHEN SUM(order_duration) > 0 THEN 1 ELSE 0 END as number_active_cleaners,
			COUNT(DISTINCT contact_customer) as number_customers,
			SUM(order_duration) as hours_executed,
			CASE WHEN COUNT(DISTINCT sfid_professional) > 0 THEN SUM(order_duration)/COUNT(DISTINCT sfid_professional) ELSE 0 END as avg_hours_cleaner,
			CASE WHEN COUNT(DISTINCT contact_customer) > 0 THEN SUM(order_duration)/COUNT(DISTINCT contact_customer) ELSE 0 END as avg_hours_customer,
			SUM(revenue) as revenue,
			SUM(cost_sub) as cost_sub,
			SUM(gp) as gp,
			CASE WHEN SUM(revenue) > 0 THEN SUM(gp)/SUM(revenue) ELSE 0 END as gpm
		
		FROM	
			
			(SELECT
			
				TO_CHAR(o.effectivedate::date,'YYYY-MM') as year_month,
				-- EXTRACT(YEAR FROM o.effectivedate) as year,
				-- EXTRACT(MONTH FROM o.effectivedate) as month,
				o.effectivedate::date as mindate,
				t1.name_partner,
			   t1.status__c,
				t1.sfid_partner,
				t1.parentid_company,
				t1.company_name__c,
				t1.number_employees_tot,
			
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as hourly_cost_sub,
			
				-- t1.hourly_cost_sub,
				t1.parentid_cleaner,
				t1.sfid_cleaner,
				t1.name_cleaner,
				o.professional__c as sfid_professional,
				o.contact__c as contact_customer,
				o.sfid as order_reference,
				t1.polygon,
				o.pph__c as price_order,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.order_duration__c END as order_duration,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END as revenue,
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as cost_sub_per_hour,
				CASE WHEN o.status LIKE '%NOSHOW PROFESSIONAL%' THEN 0 ELSE (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as cost_sub,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as gp,
				CASE WHEN (o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE (CASE WHEN (o.eff_pph*o.order_duration__c) > 0 THEN (o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c)/(o.eff_pph*o.order_duration__c) ELSE 0 END) END as gpm
			
			FROM
			
				(SELECT
	
					a.name as name_partner,
					a.status__c,
					a.type__c,
					a.hr_contract_start__c as contract_start,
					a.hr_contract_end__c as contract_end,
					a.sfid as sfid_partner,
					a.parentid as parentid_company,
					a.company_name__c,
					b.delivery_areas__c as polygon,
					a.numberofemployees as number_employees_tot,
					a.pph__c as hourly_cost_sub,
					b.parentid as parentid_cleaner,
					b.sfid as sfid_cleaner,
					b.name as name_cleaner
					
				FROM
				
					salesforce.account a
					
				LEFT JOIN
				
					salesforce.account b
					
				ON 
				
					a.sfid = b.parentid
					
				WHERE
				
					-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
					a.type__c = 'partner'
					AND a.type__c NOT LIKE 'master'
					AND a.type__c NOT LIKE 'master'
					AND a.company_name__c NOT LIKE '%BAT%'
					AND a.company_name__c IS NOT NULL
					AND a.company_name__c NOT LIKE ''
					AND LOWER(a.name) not like '%test%'
					AND LOWER(a.company_name__c) not like '%test%'
					AND a.test__c IS FALSE
					) as t1
					
			LEFT JOIN
			
				bi.orders o
				
			ON 
			
				t1.sfid_cleaner = o.professional__c
				
			--- WHERE 
			
				-- o.status NOT LIKE '%CANCELLED%'
				
			ORDER BY
			
				year_month) as t3
				
		GROUP BY
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			number_employees_tot,
			cost_sub_per_hour
			
		ORDER BY
		
			year_month,
			name_partner) as table4
			
	GROUP BY
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
		
	ORDER BY
	
		year_month,
		name_partner) as finaltable1
		
LEFT JOIN

	(SELECT
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
	
	FROM
	
		(SELECT
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			cost_sub_per_hour,
			number_employees_tot,
			COUNT(DISTINCT sfid_professional) as number_cleaners_platform,
			-- CASE WHEN SUM(order_duration) > 0 THEN 1 ELSE 0 END as number_active_cleaners,
			COUNT(DISTINCT contact_customer) as number_customers,
			SUM(order_duration) as hours_executed,
			CASE WHEN COUNT(DISTINCT sfid_professional) > 0 THEN SUM(order_duration)/COUNT(DISTINCT sfid_professional) ELSE 0 END as avg_hours_cleaner,
			CASE WHEN COUNT(DISTINCT contact_customer) > 0 THEN SUM(order_duration)/COUNT(DISTINCT contact_customer) ELSE 0 END as avg_hours_customer,
			SUM(revenue) as revenue,
			SUM(cost_sub) as cost_sub,
			SUM(gp) as gp,
			CASE WHEN SUM(revenue) > 0 THEN SUM(gp)/SUM(revenue) ELSE 0 END as gpm
		
		FROM	
			
			(SELECT
			
				TO_CHAR(o.effectivedate::date,'YYYY-MM') as year_month,
				-- EXTRACT(YEAR FROM o.effectivedate) as year,
				-- EXTRACT(MONTH FROM o.effectivedate) as month,
				o.effectivedate::date as mindate,
				t1.name_partner,
			   t1.status__c,
				t1.sfid_partner,
				t1.parentid_company,
				t1.company_name__c,
				t1.number_employees_tot,
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as hourly_cost_sub,
				t1.parentid_cleaner,
				t1.sfid_cleaner,
				t1.name_cleaner,
				o.professional__c as sfid_professional,
				o.contact__c as contact_customer,
				o.sfid as order_reference,
				o.polygon,
				o.pph__c as price_order,
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.order_duration__c END as order_duration,
				CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.eff_pph*o.order_duration__c END as revenue,
				
				(CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END) as cost_sub_per_hour,
				
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as cost_sub,
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as gp,
				CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE (CASE WHEN (o.eff_pph*o.order_duration__c) > 0 THEN (o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
					  
					  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c)/(o.eff_pph*o.order_duration__c) ELSE 0 END) END as gpm
			
			FROM
			
				(SELECT
	
					a.name as name_partner,
					a.status__c,
					a.type__c,
					a.hr_contract_start__c as contract_start,
					a.hr_contract_end__c as contract_end,
					a.sfid as sfid_partner,
					a.parentid as parentid_company,
					a.company_name__c,
					b.delivery_areas__c as polygon,
					a.numberofemployees as number_employees_tot,
					a.pph__c as hourly_cost_sub,
					b.parentid as parentid_cleaner,
					b.sfid as sfid_cleaner,
					b.name as name_cleaner
					
				FROM
				
					salesforce.account a
					
				LEFT JOIN
				
					salesforce.account b
					
				ON 
				
					a.sfid = b.parentid
					
				WHERE
				
					-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
					a.type__c = 'partner'
					AND a.type__c NOT LIKE 'master'
					AND a.company_name__c NOT LIKE '%BAT%'
					AND a.company_name__c IS NOT NULL
					AND a.company_name__c NOT LIKE ''
					AND LOWER(a.name) not like '%test%'
					AND LOWER(a.company_name__c) not like '%test%'
					AND a.test__c IS FALSE
					) as t1
					
			LEFT JOIN
			
				bi.orders o
				
			ON 
			
				t1.sfid_cleaner = o.professional__c
				
			WHERE 
			
				o.status NOT LIKE '%CANCELLED%'
				
			ORDER BY
			
				year_month) as t3
				
		GROUP BY
		
			year_month,
			mindate,
			polygon,
			name_partner,
			sfid_partner,
			company_name__c,
			number_employees_tot,
			cost_sub_per_hour
			
		ORDER BY
		
			year_month,
			name_partner) as table4
			
	GROUP BY
	
		table4.year_month,
		table4.mindate,
		table4.polygon,
		table4.name_partner,
		table4.sfid_partner,
		table4.company_name__c,
		table4.cost_sub_per_hour,
		table4.number_employees_tot,
		table4.number_cleaners_platform,
		table4.number_customers,
		table4.hours_executed,
		table4.avg_hours_cleaner,
		table4.avg_hours_customer,
		table4.revenue,
		table4.cost_sub,
		table4.gp,
		table4.gpm
		
	ORDER BY
	
		year_month,
		name_partner) as finaltable2
		
ON 

	finaltable1.sfid_partner = finaltable2.sfid_partner
	AND finaltable1.year_month = finaltable2.year_month
	
-- WHERE 

	-- finaltable1.polygon IS NOT NULL
	) as big_table
	
	
LEFT JOIN


	(SELECT

	name_partner,
	COUNT(DISTINCT sfid_cleaner) as cleaners_platform
	
	FROM

	(SELECT
	
		a.name as name_partner,
		a.status__c,
		a.type__c,
		a.hr_contract_start__c as contract_start,
		a.hr_contract_end__c as contract_end,
		a.sfid as sfid_partner,
		a.parentid as parentid_company,
		a.company_name__c,
		b.delivery_areas__c as polygon,
		a.numberofemployees as number_employees_tot,
		a.pph__c as hourly_cost_sub,
		b.parentid as parentid_cleaner,
		b.sfid as sfid_cleaner,
		b.name as name_cleaner
		
	FROM
	
		salesforce.account a
		
	LEFT JOIN
	
		salesforce.account b
		
	ON 
	
		a.sfid = b.parentid
		
	WHERE
	
		-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
		a.type__c = 'partner'
		AND a.type__c NOT LIKE 'master'
		AND a.company_name__c NOT LIKE '%BAT%'
		AND a.company_name__c IS NOT NULL
		AND a.company_name__c NOT LIKE ''
		AND LOWER(a.name) not like '%test%'
		AND LOWER(a.company_name__c) not like '%test%'
		AND a.test__c IS FALSE
					) as table_plus
					
	GROUP BY

		name_partner)	as small_table
		

ON

	big_table.name_partner = small_table.name_partner

GROUP BY

	big_table.year_month,
	big_table.mindate,
	big_table.polygon,
	big_table.name_partner,
	big_table.sfid_partner,
	big_table.company_name__c,
	big_table.cost_sub_per_hour,
	big_table.number_employees_tot,
	number_cleaners_platform,
	-- number_cleaners_active,
	-- big_table.number_cleaners_active,
	big_table.hours_executed,
	big_table.avg_hours_cleaner,
	big_table.avg_hours_customer,
	big_table.revenue,
	big_table.cost_sub,
	big_table.gp,
	big_table.gpm,
	small_table.cleaners_platform
	
ORDER BY

	year_month desc;




------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.gpm_subcontractor;
CREATE TABLE bi.gpm_subcontractor as

SELECT
			
	t1.company_name__c as company_name__c,	
	t1.name_cleaner as name_account,	
	TO_CHAR(o.effectivedate::date,'YYYY-MM') as year_month,
	o.professional__c as sfid_professional,
	
	CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.hourly_cost_sub
			  
			  END as pph__c,
	
	-- t1.hourly_cost_sub as pph__c,
	t1.polygon as delivery_area,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.order_duration__c END) as worked_hours,
	t1.type__c,
	t1.contract_start,
	t1.contract_end,
	1 as monthly_hours,
	1 as weekly_hours,
	1 as days_worked,
	1 as days_of_month,
	min(o.effectivedate)::date as mindate,
	1 as working_hours,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END) as revenue,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END) as gmv,
	0 as B2C_Revenue,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c END) as B2B_Revenue,
	0 as b2c_share,
	1 as b2b_share,
	SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW PROFESSIONAL%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																			  
																																			  ELSE t1.hourly_cost_sub
																																			  
																																			  END)*o.order_duration__c END) as gp,
   CASE WHEN (SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW%') THEN 0 ELSE o.eff_pph*o.order_duration__c END)) > 0 THEN (SUM(CASE WHEN (o.status LIKE '%CANCELLED%' OR o.status LIKE '%NOSHOW%') THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																																																		  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																																																		  
																																																																		  ELSE t1.hourly_cost_sub																																																																  
																																																																		  END)*o.order_duration__c END))/(SUM(CASE WHEN (o.status LIKE '%CANCELLED%') THEN 0 ELSE o.eff_pph*o.order_duration__c END)) ELSE 0 END as gpm_subcontractor
	
FROM

	(SELECT
			
			a.name as name_partner,
			a.status__c,
			a.type__c,
			a.hr_contract_start__c as contract_start,
			a.hr_contract_end__c as contract_end,
			a.sfid as sfid_partner,
			a.parentid as parentid_company,
			a.company_name__c,
			CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
															WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
															WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
															ELSE 'at-other'
															END
													ELSE b.delivery_areas__c
													END as polygon,
			a.numberofemployees as number_employees_tot,
			a.pph__c as hourly_cost_sub,
			b.parentid as parentid_cleaner,
			b.sfid as sfid_cleaner,
			b.name as name_cleaner
			
		FROM
		
			salesforce.account a
			
		LEFT JOIN
		
			salesforce.account b
			
		ON 
		
			a.sfid = b.parentid
			
		WHERE
		
			-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
			a.type__c = 'partner'
			AND a.type__c NOT LIKE 'master'
			AND a.company_name__c NOT LIKE '%BAT%'
			AND a.company_name__c IS NOT NULL
			AND a.company_name__c NOT LIKE ''
			AND LOWER(a.name) not like '%test%'
			AND LOWER(a.company_name__c) not like '%test%'
			AND a.test__c IS FALSE
		) as t1
		
LEFT JOIN

	bi.orders o
	
ON 

	t1.sfid_cleaner = o.professional__c
	
GROUP BY

	year_month,
	t1.name_partner,
    t1.status__c,
    t1.type__c,
	t1.sfid_partner,
	t1.contract_start,
	t1.contract_end,
	t1.parentid_company,
	t1.company_name__c,
	t1.number_employees_tot,
	t1.hourly_cost_sub,
	t1.parentid_cleaner,
	t1.sfid_cleaner,
	t1.name_cleaner,
	sfid_professional,
	t1.polygon,
	o.effectivedate
	
ORDER BY

	year_month;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.improvement_cases_partners;
CREATE TABLE bi.improvement_cases_partners AS

SELECT

	EXTRACT(YEAR FROM c.createddate) as year,
	EXTRACT(MONTH FROM c.createddate) as month,
	c.createddate,
	t2.company_name__c,
	
	COUNT(DISTINCT c.casenumber)

FROM

	(SELECT

	t0.*,
	a.parentid

FROM

	(SELECT
	
		s.company_name__c,
		s.name_account,
		t1.cleaners as sfid_cleaner
	
	FROM
	
		(SELECT
		
			DISTINCT(sfid_professional) as cleaners
			
		FROM
		
			bi.gpm_subcontractor) as t1
		
			LEFT JOIN
			
				bi.gpm_subcontractor s
				
			ON 
			
				t1.cleaners = s.sfid_professional) as t0
				
		LEFT JOIN
		
			salesforce.account a 
			
		ON
		
			t0.sfid_cleaner = a.sfid
			
		WHERE
		
			t0.company_name__c IS NOT NULL
			
		GROUP BY
		
			t0.company_name__c,
			t0.name_account,
			t0.sfid_cleaner,
			parentid) as t2
			
LEFT JOIN

	salesforce.case c
	
ON

	t2.parentid = c.accountid
	
WHERE

	c.reason = 'Professional - Improvement'
	
GROUP BY
	
	year,
	month,
	createddate,
	t2.company_name__c,
	t2.name_account;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.orders_subcontractors;
CREATE TABLE bi.orders_subcontractors AS


SELECT

	*

FROM

	(SELECT
	
		t2.name,
		t2.company_name__c as name_company,
		t1.parentid,
		t2.hr_contract_start__c as start_partner,
		t2.hr_contract_end__c as end_partner
		
	FROM
	
		Salesforce.Account t1
		
	JOIN
	
		Salesforce.Account t2
		
	ON
	
		(t1.parentid = t2.sfid)
		
	WHERE
	
		(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
		
	GROUP BY
	
		t2.name,
		t2.company_name__c,
		t1.parentid,
		t2.hr_contract_start__c,
		t2.hr_contract_end__c) as table1
		
LEFT JOIN


	(SELECT

		-- This table gives us all the orders executed by the partners until today included
		-- It includes also the partners that are not working in collaboration with us
		-- We have the number of their employees, their cost/hour, the price and duration of the orders...
				
		o.effectivedate::date as date_orders,
		t1.name_partner,
		 t1.status__c,
		t1.sfid_partner,
		t1.parentid_company,
		t1.company_name__c,
		t1.number_employees_tot,
		t1.hourly_cost_sub,
		t1.parentid_cleaner,
		t1.sfid_cleaner,
		t1.name_cleaner,
		o.professional__c as sfid_professional,
		o.contact__c as contact_customer,
		o.sfid as order_reference,
		o.status as status_order,
		UPPER(LEFT(t1.polygon, 2)) as country,
		t1.polygon,
		o.pph__c as price_order,
		o.eff_pph,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.order_duration__c END as order_duration,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.eff_pph*o.order_duration__c END as revenue,
		
		CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.hourly_cost_sub
			  
			  END as cost_sub_per_hour,
		
		-- t1.hourly_cost_sub as cost_sub_per_hour,
		
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
			  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE t1.hourly_cost_sub
			  
			  END)*o.order_duration__c END as cost_sub,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																					  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																					  
																																					  ELSE t1.hourly_cost_sub
																																					  
																																					  END)*o.order_duration__c END as gp,
		CASE WHEN ((o.status LIKE '%NOSHOW PROFESSIONAL%') OR (o.status LIKE '%CANCELLED%')) THEN 0 ELSE (CASE WHEN (o.eff_pph*o.order_duration__c) > 0 THEN (o.eff_pph*o.order_duration__c - (CASE WHEN o.effectivedate >= '2018-01-01' THEN t1.hourly_cost_sub
		
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Cleanr%') THEN 18.2
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%DADI%') THEN 17.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Ants&friends%') THEN 19.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Crystal%') THEN 17
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%PureX%') THEN 19.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Sunny%') THEN 18.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%facilitas%') THEN 17.5
																																																	  WHEN (o.effectivedate < '2018-01-01' AND t1.company_name__c LIKE '%Petersdorf%') THEN 17.5
																																																	  
																																																	  ELSE t1.hourly_cost_sub
																																																	  
																																																	  END)*o.order_duration__c)/(o.eff_pph*o.order_duration__c) ELSE 0 END) END as gpm

	FROM

		(SELECT
	
			a.name as name_partner,
			a.status__c,
			a.type__c,
			a.hr_contract_start__c as contract_start,
			a.hr_contract_end__c as contract_end,
			a.sfid as sfid_partner,
			a.parentid as parentid_company,
			a.company_name__c,
			CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
															WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
															WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
															ELSE 'at-other'
															END
													ELSE b.delivery_areas__c
													END as polygon,
			a.numberofemployees as number_employees_tot,
			a.pph__c as hourly_cost_sub,
			b.parentid as parentid_cleaner,
			b.sfid as sfid_cleaner,
			b.name as name_cleaner
			
		FROM
		
			salesforce.account a
			
		LEFT JOIN
		
			salesforce.account b
			
		ON 
		
			a.sfid = b.parentid
			
		WHERE
		
			-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
			a.type__c = 'partner'
			AND a.type__c NOT LIKE 'master'
			AND a.company_name__c NOT LIKE '%BAT%'
			AND a.company_name__c IS NOT NULL
			AND a.company_name__c NOT LIKE ''
			AND LOWER(a.name) not like '%test%'
			AND LOWER(a.company_name__c) not like '%test%'
			AND a.test__c IS FALSE
			) as t1
		
	LEFT JOIN
	
		bi.orders o
	
	ON 
	
		t1.sfid_cleaner = o.professional__c
	
	-- WHERE 
	
		-- o.effectivedate <= current_date
	
	ORDER BY
	
		o.effectivedate desc,
		name_partner) as table2
			
ON

	table1.name_company = table2.company_name__c;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.activity_partners;
CREATE TABLE bi.activity_partners AS

SELECT

	*
	
FROM

(SELECT

	t5.date,
	CASE WHEN t4.partner_start_date <= t5.date AND ((t4.partner_end_date > t5.date) OR (t4.partner_end_date IS NULL)) THEN 'Active' ELSE 'Unactive' END as status_at_time,
	t4.name_partner,
	t4.partner_start_date,
	t4.partner_end_date,
	t4.status__c,
	t4.sfid_partner,
	t4.parentid_company,
	t4.hr_contract_start__c,
	t4.hr_contract_end__c,
	t4.company_name__c,
	t4.number_employees_tot,
	UPPER(t4.country),
	t4.polygon,
	t4.hourly_cost_sub,
	t4.parentid_cleaner,
	t4.sfid_cleaner,
	t4.name_cleaner


FROM

	(SELECT
			
		1 as connecting_key,
		i::date as date 
		
	FROM
	
		generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i) as t5

	FULL JOIN
	
	
	(SELECT
	
		1 as connecting_key,
		t3.hr_contract_start__c as partner_start_date,
		t3.hr_contract_end__c as partner_end_date,
		t1.*
			
	FROM	
	
		(SELECT
  
		    a.name as name_partner,
		    a.status__c,
		    a.type__c,
		    a.hr_contract_start__c,
		    a.hr_contract_end__c,
		    a.sfid as sfid_partner,
		    a.parentid as parentid_company,
		    a.company_name__c,
		    LEFT(b.delivery_areas__c, 2) as country,
		    CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
		                            WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
		                            WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
		                            ELSE 'at-other'
		                            END
		                        ELSE b.delivery_areas__c
		                        END as polygon,
		    a.numberofemployees as number_employees_tot,
		    a.pph__c as hourly_cost_sub,
		    b.parentid as parentid_cleaner,
		    b.sfid as sfid_cleaner,
		    b.name as name_cleaner,
		    b.status__c as status_cleaner
		    
		  FROM
		  
		    salesforce.account a
		    
		  LEFT JOIN
		  
		    salesforce.account b
		    
		  ON 
		  
		    a.sfid = b.parentid
		    
		  WHERE
		  
		    -- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
		    a.type__c = 'partner'
		    AND a.type__c NOT LIKE 'master'
		    AND a.company_name__c NOT LIKE '%BAT%'
		    AND a.company_name__c IS NOT NULL
		    AND a.company_name__c NOT LIKE ''
		    AND LOWER(a.name) not like '%test%'
		    AND LOWER(a.company_name__c) not like '%test%'
		    AND a.test__c IS FALSE) t1
	
	LEFT JOIN
	
		(SELECT
			t2.name,
			t2.company_name__c,
			t1.parentid,
			t2.hr_contract_start__c,
			t2.hr_contract_end__c
			
		FROM
		
			Salesforce.Account t1
			
		JOIN
		
			Salesforce.Account t2
			
		ON
		
			(t1.parentid = t2.sfid)
			
		WHERE
		
			(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
			
		GROUP BY
		
			t2.name,
			t2.company_name__c,
			t1.parentid,
			t2.hr_contract_start__c,
			t2.hr_contract_end__c) as t3
			
	ON
	
		t1.company_name__c = t3.company_name__c) t4
	
ON 

	t5.connecting_key = t4.connecting_key

ORDER BY


	t5.date desc) as t6;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.employees_partners;
CREATE TABLE bi.employees_partners AS

SELECT

	t1.year_month,
	CONCAT(LEFT(t1.year_month,4)::text,'-',RIGHT(t1.year_month,2)::text,'-01')::date as date,
	t1.country,
	t1.polygon,
	t1.company_name__c as company_name,
	t1.number_employees,
	CASE WHEN t1.times_active > 0 THEN 'Active' ELSE 'Inactive' END as activity
	
FROM

	(SELECT
	
		TO_CHAR(o.date,'YYYY-MM') as year_month,
		-- CONCAT(LEFT(TO_CHAR(o.date,'YYYY-MM'), 4)::text,'-',RIGHT(TO_CHAR(o.date,'YYYY-MM'),2)::text,'-01')::date as date,
		-- EXTRACT(YEAR FROM o.date) as year,
		-- EXTRACT(MONTH FROM o.date) as month,
		-- MIN(o.date) as mindate,
		o.upper as country,
		o.polygon,
		o.company_name__c,
		MAX(o.number_employees_tot) number_employees,
		SUM(CASE WHEN o.status_at_time = 'Active' THEN 1 ELSE 0 END) as times_active
		
	FROM
	
		bi.activity_partners o
			
	GROUP BY
	
		-- year,
		-- month,
		year_month,
		-- date,
		o.upper,
		o.polygon,
		company_name__c
	
		
	ORDER BY
	
		year_month desc,
		-- date desc,
		company_name__c) as t1;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.opps_gp;
CREATE TABLE bi.opps_gp AS

SELECT

	table2.date_orders,
	table2.country,
	table2.polygon,
	table2.name,
	table2.name_company,
	table2.parentid,
	table2.sfid_cleaner,
	table2.contact_customer,
	o.contact__c as sfid_opp,
	o.name as name_opp,
	table2.order_reference,
	table2.order_duration,
	table2.revenue,
	table2.eff_pph,
	table2.cost_sub_per_hour,
	table2.cost_sub,
	table2.gp,
	table2.gpm
	
FROM

	bi.orders_subcontractors table2
	
LEFT JOIN

	bi.b2borders o
	
ON

	table2.contact_customer = o.contact__c
	
GROUP BY

	table2.date_orders,
	table2.country,
	table2.polygon,
	table2.name,
	table2.name_company,
	table2.parentid,
	table2.sfid_cleaner,
	table2.contact_customer,
	o.contact__c,
	o.name,
	table2.order_reference,
	table2.order_duration,
	table2.revenue,
	table2.eff_pph,
	table2.cost_sub_per_hour,
	table2.cost_sub,
	table2.gp,
	table2.gpm;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.activity_opps;
CREATE TABLE bi.activity_opps as 

SELECT

	t2.year_month,
	min(t2.effectivedate) as effectivedate,
	t2.polygon,
	COUNT(DISTINCT t2.number_active_opps) as number_active_opps,
	SUM(t2.monthly_hours) as monthly_hours,
	MAX(t2.number_partners) as number_partners,
	MAX(t2.number_active_partners) as number_active_partners,
	MAX(t3.total_prof) as all_professionals,
	MAX(t3.partners_prof) as professionals_partners,
	MAX(t3.total_hours) as total_hours,
	MAX(t3.hours_partners) as hours_partners

FROM

	(SELECT
	
		t1.year_month,
		t1.effectivedate,
		-- t1.week,
		t1.polygon,
		t1.number_active_opps,
		t1.monthly_hours,
		t1.number_partners,
		(CASE WHEN (active_partners.number_active_partners IS NULL) THEN 0 ELSE active_partners.number_active_partners END) as number_active_partners
	
	FROM
	
		(SELECT
		
			number_opps.year_month,
			number_opps.effectivedate,
			number_opps.polygon,
			number_opps.number_active_opps,
			number_opps.monthly_hours,
			(CASE WHEN (number_partners.number_partners IS NULL) THEN 0 ELSE number_partners.number_partners END) as number_partners
		
		FROM
		
			(SELECT
			
				TO_CHAR(oo.effectivedate,'YYYY-MM') as year_month,
				oo.effectivedate,
				-- CONCAT('WK', (EXTRACT(WEEK FROM oo.effectivedate)::text)) as week,
				CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
				oo.contact__c as number_active_opps,
				-- COUNT(DISTINCT oo.contact__c) as number_active_opps,
				SUM(oo.order_duration__c) as monthly_hours
				
			FROM
			
				bi.orders oo
			
			WHERE
			
				oo.type = 'cleaning-b2b'
				AND oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
				AND oo.effectivedate > '2016-01-01'
				AND oo.test__c = FALSE
				
			GROUP BY
			
				year_month,
				oo.effectivedate,
				oo.polygon,
				oo.locale__c,
				oo.contact__c
				
			ORDER BY
			
				year_month desc,
				oo.polygon) as number_opps
				
		LEFT JOIN 
		
			(SELECT
		
				year_month,
				polygon,
				COUNT(DISTINCT company_name) as number_partners
				
			FROM
			
				bi.employees_partners
				
			WHERE
			
				activity = 'Active'
				
			GROUP BY
			
				year_month,
				polygon
				
			ORDER BY
			
				year_month desc,
				polygon desc) as number_partners		
		ON 
		
			number_opps.year_month = number_partners.year_month
			AND number_opps.polygon = number_partners.polygon) as t1
			
	LEFT JOIN
	
		(SELECT
	
			TO_CHAR(date_orders,'YYYY-MM') as year_month,
			polygon,
			COUNT(DISTINCT company_name__c) as number_active_partners
			
		FROM
		
			bi.orders_subcontractors
			
		WHERE
		
			date_orders IS NOT NULL
			
		GROUP BY
		
			year_month,
			polygon
			
		ORDER BY
		
			year_month desc,
			polygon desc) as active_partners
	
	ON
	
		t1.year_month = active_partners.year_month
		AND t1.polygon = active_partners.polygon) as t2
		
LEFT JOIN

	(SELECT
	
		TO_CHAR(t4.effectivedate,'YYYY-MM') as year_month,
		-- CONCAT('WK', (EXTRACT(WEEK FROM t4.effectivedate)::text)) as week,
		t4.polygon,
		COUNT(DISTINCT t4.all_prof) as total_prof,
		COUNT(DISTINCT t4.partners_prof) as partners_prof,
		SUM(t4.order_duration__c) as total_hours,
		SUM(CASE WHEN (t4.partners_prof IS NOT NULL) THEN t4.order_duration__c ELSE 0 END) as hours_partners
	
	FROM
	
		(SELECT
		
			o.effectivedate,
			CASE WHEN o.polygon IS NULL THEN CASE WHEN o.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN o.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN o.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE o.polygon
											END
											as polygon,
			o.sfid,
			o.professional__c as all_prof,
			t3.sfid_cleaner as partners_prof,
			o.order_duration__c
		
		FROM
		
			bi.orders o
			
		LEFT JOIN
		
			(SELECT	
					
					oo.name,
					a.sfid as sfid_cleaner,
					oo.name_company,
					oo.parentid,
					oo.start_partner,
					oo.end_partner	
					
				FROM
				
				
					(SELECT
							
						t2.name,
						t2.sfid,
						t2.company_name__c as name_company,
						t1.parentid,
						t2.hr_contract_start__c as start_partner,
						t2.hr_contract_end__c as end_partner
						
					FROM
					
						Salesforce.Account t1
						
					JOIN
					
						Salesforce.Account t2
						
					ON
					
						(t2.sfid = t1.parentid)
						
					WHERE
					
						(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
						
					GROUP BY
					
						t2.name,
						t2.sfid,
						t2.company_name__c,
						t1.parentid,
						t2.hr_contract_start__c,
						t2.hr_contract_end__c) as oo
						
				LEFT JOIN
				
					salesforce.account a
					
				ON 
				
					oo.sfid = a.parentid) as t3
		
		ON 
		
			o.professional__c = t3.sfid_cleaner
					
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c = FALSE
			AND o.effectivedate > '2016-01-01'
			AND o.effectivedate < current_date + 60
			AND o."status" IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
			
		ORDER BY
		
			o.effectivedate desc) as t4
			
	-- WHERE
	
		-- partners_prof IS NOT NULL
			
	GROUP BY
	
		year_month,
		t4.polygon
		
	ORDER BY
	
		year_month desc,
		t4.polygon desc)as t3	
		
ON 

	t2.year_month = t3.year_month
	AND t2.polygon = t3.polygon

GROUP BY

	t2.year_month,
	t2.polygon
	
ORDER BY

	t2.year_month desc,
	t2.polygon desc;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.occupation_partners;
CREATE TABLE bi.occupation_partners as


SELECT

	table3.year_month,
	table3.effectivedate,
	table3.polygon,
	table3.active_opp,
	table3.name_opp,
	SUM(table3.hours) as hours_on_opp,
	table3.partner as company,
	table3.number_employees_tot,
	-- table3.hourly_cost_sub,
	
	CASE WHEN table3.effectivedate >= '2018-01-01' THEN table3.hourly_cost_sub
		
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%DADI%') THEN 17.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Crystal%') THEN 17
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%PureX%') THEN 19.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Sunny%') THEN 18.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%facilitas%') THEN 17.5
			  WHEN (table3.effectivedate < '2018-01-01' AND table3.partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE table3.hourly_cost_sub
			  
			  END as hourly_cost_sub,
			  
	table3.cleaner,
	SUM(table4.hours_occupied) as hours_occupied_partner,
	MIN(table3.number_employees_tot*3*4.3*5) as total_capacity_partner
	
FROM

	(SELECT
	
		table1.*,
		table2.name as name_opp
		
	FROM
	
		(SELECT
		
			opps.*,
			CASE WHEN partners.company_name__c IS NULL THEN 'BAT' ELSE partners.company_name__c END as partner,
			partners.name_cleaner as cleaner,
			partners.number_employees_tot,
			partners.hourly_cost_sub
		
		FROM
		
			(SELECT
						
				TO_CHAR(oo.effectivedate,'YYYY-MM') as year_month,
				oo.effectivedate,
				-- CONCAT('WK', (EXTRACT(WEEK FROM oo.effectivedate)::text)) as week,
				CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
				oo.contact__c as active_opp,
				oo.professional__c as cleaner_assigned,
				SUM(oo.order_duration__c) as hours
				
			FROM
			
				bi.orders oo
			
			WHERE
			
				oo.type = 'cleaning-b2b'
				AND oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
				AND oo.effectivedate > '2016-01-01'
				AND oo.test__c = FALSE
				
			GROUP BY
			
				year_month,
				effectivedate,
				oo.contact__c,
				oo.professional__c,
				oo.polygon,
				oo.locale__c
				
			ORDER BY
			
				year_month desc,
				oo.polygon) as opps
				
		LEFT JOIN
		
			(SELECT
	
				a.name as name_partner,
				a.status__c,
				a.type__c,
				a.hr_contract_start__c as contract_start,
				a.hr_contract_end__c as contract_end,
				a.sfid as sfid_partner,
				a.parentid as parentid_company,
				a.company_name__c,
				CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
																WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
																WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
																ELSE 'at-other'
																END
														ELSE b.delivery_areas__c
														END as polygon,
				a.numberofemployees as number_employees_tot,
				a.pph__c as hourly_cost_sub,
				b.parentid as parentid_cleaner,
				b.sfid as sfid_cleaner,
				b.name as name_cleaner
				
			FROM
			
				salesforce.account a
				
			LEFT JOIN
			
				salesforce.account b
				
			ON 
			
				a.sfid = b.parentid
				
			WHERE
			
				-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
				a.type__c = 'partner'
				AND a.type__c NOT LIKE 'master'
				AND a.company_name__c NOT LIKE '%BAT%'
				AND a.company_name__c IS NOT NULL
				AND a.company_name__c NOT LIKE ''
				AND LOWER(a.name) not like '%test%'
				AND LOWER(a.company_name__c) not like '%test%'
				AND a.test__c IS FALSE
				) as partners
				
				
		ON 
		
			opps.cleaner_assigned = partners.sfid_cleaner) as table1	
			
	LEFT JOIN
	
		salesforce.contact table2
		
	ON
	
		table1.active_opp = table2.sfid) as table3
		
LEFT JOIN

	(SELECT
	
		occupation_partners.year_month,
		occupation_partners.effectivedate,
		occupation_partners.partner,
		SUM(occupation_partners.hours) as hours_occupied
	
	FROM
		
		(SELECT
		
			table1.*,
			table2.name as name_opp
			
		FROM
		
			(SELECT
			
				opps.*,
				CASE WHEN partners.company_name__c IS NULL THEN 'BAT' ELSE partners.company_name__c END as partner,
				partners.name_cleaner as cleaner,
				partners.number_employees_tot,
				partners.hourly_cost_sub
			
			FROM
			
				(SELECT
							
					TO_CHAR(oo.effectivedate,'YYYY-MM') as year_month,
					oo.effectivedate,
					-- CONCAT('WK', (EXTRACT(WEEK FROM oo.effectivedate)::text)) as week,
					CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
					oo.contact__c as active_opp,
					oo.professional__c as cleaner_assigned,
					SUM(oo.order_duration__c) as hours
					
				FROM
				
					bi.orders oo
				
				WHERE
				
					oo.type = 'cleaning-b2b'
					AND oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER')
					AND oo.effectivedate > '2016-01-01'
					AND oo.test__c = FALSE
					
				GROUP BY
				
					year_month,
					effectivedate,
					oo.contact__c,
					oo.professional__c,
					oo.polygon,
					oo.locale__c
					
				ORDER BY
				
					year_month desc,
					oo.polygon) as opps
					
			LEFT JOIN
			
				(SELECT
	
					a.name as name_partner,
					a.status__c,
					a.type__c,
					a.hr_contract_start__c as contract_start,
					a.hr_contract_end__c as contract_end,
					a.sfid as sfid_partner,
					a.parentid as parentid_company,
					a.company_name__c,
					CASE WHEN b.delivery_areas__c IS NULL THEN CASE WHEN b.locale__c LIKE 'de-%' THEN 'de-other'
																	WHEN b.locale__c LIKE 'ch-%' THEN 'ch-other'
																	WHEN b.locale__c LIKE 'nl-%' THEN 'nl-other'
																	ELSE 'at-other'
																	END
															ELSE b.delivery_areas__c
															END as polygon,
					a.numberofemployees as number_employees_tot,
					a.pph__c as hourly_cost_sub,
					b.parentid as parentid_cleaner,
					b.sfid as sfid_cleaner,
					b.name as name_cleaner
					
				FROM
				
					salesforce.account a
					
				LEFT JOIN
				
					salesforce.account b
					
				ON 
				
					a.sfid = b.parentid
					
				WHERE
				
					-- a.ownerid IN ('00520000003cfbsAAA', '00520000003bJ8EAAU', '00520000003IhraAAC')
					a.type__c = 'partner'
					AND a.type__c NOT LIKE 'master'
					AND a.company_name__c NOT LIKE '%BAT%'
					AND a.company_name__c IS NOT NULL
					AND a.company_name__c NOT LIKE ''
					AND LOWER(a.name) not like '%test%'
					AND LOWER(a.company_name__c) not like '%test%'
					AND a.test__c IS FALSE
					) as partners
					
					
			ON 
			
				opps.cleaner_assigned = partners.sfid_cleaner) as table1	
				
		LEFT JOIN
		
			salesforce.contact table2
			
		ON
		
			table1.active_opp = table2.sfid) occupation_partners
			
	GROUP BY
	
		occupation_partners.year_month,
		occupation_partners.effectivedate,
		occupation_partners.partner
		
	ORDER BY
	
		year_month desc,
		partner ) as table4
		
ON 

	table3.year_month = table4.year_month
	AND table3.partner = table4.partner
		
GROUP BY

	table3.year_month,
	table3.effectivedate,
	table3.polygon,
	table3.active_opp,
	table3.name_opp,		
	table3.partner,
	table3.number_employees_tot,
	table3.hourly_cost_sub,
	table3.cleaner
	
ORDER BY 

	year_month desc,
	polygon;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.distribution_cases;
CREATE TABLE bi.distribution_cases AS

SELECT

	table5.*,
	table4.professional__c,
	table4.number_cases::text,
	table4.year,
	table4.month,
	table4.createddate,
	table4.parentid,
	table4.company_name,
	-- table4.order__c,
	table4.status,
	table4.reason,
	table4.ownerid,
	-- table1.sfid as sfid_case,
	table4.casenumber,
	table4.subject,
	-- table4.description,
	table4.contactid,
	table4.accountid

FROM

	(SELECT
	
		o.sfid as sfid_order,
		COUNT(DISTINCT casenumber) as number_cases,
		o.professional__c,
		table3.*
	
	
	FROM
	
		(SELECT
		
			EXTRACT(YEAR FROM table1.createddate) as year,
			EXTRACT(MONTH FROM table1.createddate) as month,
			table1.createddate,
			table2.parentid,
			CASE WHEN table2.parentid IS NULL THEN table2.name ELSE table2.company_name__c END as company_name,
			table1.order__c,
			table1.status,
			table1.reason,
			table1.ownerid,
			-- table1.sfid as sfid_case,
			table1.casenumber,
			table1.subject,
			table1.description,
			table1.contactid,
			table1.accountid
			
		
		FROM
		
		
			(SELECT
			
				*
				
			FROM
			
				salesforce.case c
				
			WHERE
			
				(c.type IN ('KA', 'TO', 'PM') OR (c.type = 'Pool' AND LOWER(c.origin) = 'partner%'))
				AND (c.reason LIKE '%Improvement%' OR c.reason LIKE '%improvement%')
				AND (c.ownerid LIKE '%00520000003IpXO%' OR c.ownerid LIKE '%00520000003cfbs%' OR c.ownerid LIKE '%00520000003bJ8E%' OR c.ownerid LIKE '%00520000005IuU4%' OR c.ownerid LIKE '%00520000003Ihko%'
				      OR c.ownerid LIKE '%0050J0000074nTT%' OR c.ownerid LIKE '%00520000003InJZ%')) as table1
			      
		LEFT JOIN
		
			(SELECT
		        t1.*,
		        t2.name as subcon
		      FROM
		       Salesforce.Account t1
		        JOIN
		            Salesforce.Account t2
		        ON
		            (t2.sfid = t1.parentid)
		    
		        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
		      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
		      and t2.name NOT LIKE '%BAT Business Services GmbH%') as table2
		      
		ON 
		
			table1.accountid = table2.parentid    
			
		GROUP BY
		
			year,
			month,
			table1.createddate,
			table2.parentid,
			company_name,
			table1.order__c,
			table1.status,
			table1.reason,
			table1.ownerid,
			table1.sfid,
			table1.casenumber,
			table1.subject,
			table1.description,
			table1.contactid,
			table1.accountid
		      
		ORDER BY
		
			year desc,
			month desc,
			table1.createddate,
			table2.parentid,
			company_name,
			table1.order__c,
			table1.status,
			table1.reason,
			table1.ownerid,
			table1.sfid,
			table1.casenumber,
			table1.subject,
			table1.description,
			table1.contactid,
			table1.accountid) as table3
			
	LEFT JOIN
	
		bi.orders o
		
	ON 
	
		table3.order__c = o.sfid
		
	GROUP BY
	
		o.sfid,
		o.professional__c,
		table3.year,
		table3.month,
		table3.createddate,
		table3.parentid,
		table3.company_name,
		table3.order__c,
		table3.status,
		table3.reason,
		table3.ownerid,
		-- table3.sfid,
		table3.casenumber,
		table3.subject,
		table3.description,
		table3.contactid,
		table3.accountid) as table4
		
LEFT JOIN

	(SELECT
	
		t1.*,
		a.name
	
	FROM
	
		(SELECT
		
			EXTRACT(YEAR FROM o.effectivedate) as year2,
			EXTRACT(MONTH FROM o.effectivedate) as month2,
			o.polygon,
			o.professional__c as cleaner_sfid,
			SUM(o.order_duration__c) as hours_performed,
			COUNT(DISTINCT o.sfid) as number_orders
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER')
			AND o.effectivedate < current_date
			AND o.type LIKE '%b2b%'
			
		GROUP BY
		
			year2,
			month2,
			o.polygon,
			cleaner_sfid
			
		ORDER BY
		
			year2 desc,
			month2 desc,
			o.polygon,
			cleaner_sfid) as t1
			
	LEFT JOIN
	
		(SELECT
		
	     t1.*,
	     t2.name as subcon
	     
	   FROM
	   
	    	Salesforce.Account t1
	    	
	   JOIN
	   
	      Salesforce.Account t2
	      
	   ON
	   
	      (t2.sfid = t1.parentid)
	 
	   WHERE 
			
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
			and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') as a
		
	ON
	
		t1.cleaner_sfid = a.sfid
	
	WHERE
	
		a.name IS NOT NULL
		
	
	ORDER BY
	
		year2 desc,
		month2 desc,
		t1.polygon,
		cleaner_sfid
		) as table5

ON

	table4.year = table5.year2
	AND table4.month = table5.month2
	AND table4.professional__c = table5.cleaner_sfid
	
WHERE

	table4.company_name IS NOT NULL
	
ORDER BY

	table4.year desc,
	table4.month desc;



------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.gp_cleaners_sub;
CREATE TABLE bi.gp_cleaners_sub AS

SELECT

	t4.*,
	-- a1.delivery_areas__c,
	CASE WHEN a1.delivery_areas__c IS NULL THEN CASE WHEN a1.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN a1.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN a1.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE a1.delivery_areas__c
											END
											as delivery_areas__c,
											
	CASE WHEN t4.effectivedate >= '2018-01-01' THEN a1.pph__c 
		
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%DADI%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Crystal%') THEN 17
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%PureX%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Sunny%') THEN 18.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%facilitas%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE a1.pph__c
			  
			  END as pph__c,
	a1.pph__c as without_changes,
	
	-- t4.hours*a1.pph__c as cost_sub,
	
	t4.hours*(CASE WHEN t4.effectivedate >= '2018-01-01' THEN a1.pph__c 
		
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%DADI%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Crystal%') THEN 17
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%PureX%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Sunny%') THEN 18.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%facilitas%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE a1.pph__c
			  
			  END) as cost_sub,
	
	t4.gmv_eur_net - t4.hours*(CASE WHEN effectivedate >= '2018-01-01' THEN a1.pph__c 
		
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Cleanr%') THEN 18.2
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%DADI%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Ants&friends%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Crystal%') THEN 17
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%PureX%') THEN 19.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Sunny%') THEN 18.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%facilitas%') THEN 17.5
			  WHEN (t4.effectivedate < '2018-01-01' AND t4.name_partner LIKE '%Petersdorf%') THEN 17.5
			  
			  ELSE a1.pph__c
			  
			  END) as  gp
		
	-- t4.gmv_eur_net - t4.hours*a1.pph__c as  gp

FROM


	(SELECT
	
	
		t3.*,
		a.name
	
	FROM
	
		(SELECT
		
			EXTRACT(YEAR FROM o.effectivedate) as year,
			EXTRACT(MONTH FROM o.effectivedate) as month,
			o.effectivedate,
			o.sfid,
			o.status,
			t4.name as name_partner,
			t4.sfid_cleaner as cleaner_partner,
			MIN(o.order_duration__c) as hours,
			MIN(o.gmv_eur_net) as gmv_eur_net
			
		FROM
		
			(SELECT	
								
				oo.name,
				a.sfid as sfid_cleaner,
				oo.name_company,
				oo.parentid,
				oo.start_partner,
				oo.end_partner,
				oo.pph__c
				
			FROM
			
			
				(SELECT
						
					t2.name,
					t2.sfid,
					t2.company_name__c as name_company,
					t1.parentid,
					t2.hr_contract_start__c as start_partner,
					t2.hr_contract_end__c as end_partner,
					t2.pph__c
					
				FROM
				
					Salesforce.Account t1
					
				JOIN
				
					Salesforce.Account t2
					
				ON
				
					(t2.sfid = t1.parentid)
					
				WHERE
				
					(t2.name not like '%BOOK%' or t2.name not like '%BAT%') and t2.name NOT like '%BAT Business Services GmbH%'
					
				GROUP BY
				
					t2.name,
					t2.sfid,
					t2.company_name__c,
					t1.parentid,
					t2.hr_contract_start__c,
					t2.hr_contract_end__c,
					t2.pph__c) as oo
					
			LEFT JOIN
			
				salesforce.account a
				
			ON 
			
				oo.sfid = a.parentid) as t4
				
		LEFT JOIN
		
			bi.orders o
			
		ON 
		
			t4.sfid_cleaner = o.professional__c
			
		WHERE 
		
			o.effectivedate IS NOT NULL
			-- AND EXTRACT(YEAR FROM o.effectivedate) < 2018
			-- AND EXTRACT(MONTH FROM o.effectivedate) = 9
			-- AND EXTRACT(YEAR FROM o.effectivedate) > 2016
			AND o.status IN ('INVOICED', 'NOSHOW CUSTOMER', 'FULFILLED', 'PENDING TO START')
			
		GROUP BY
		
			year,
			month,
			o.effectivedate,
			o.status,
			o.sfid,
			name_partner,
			cleaner_partner
			
		ORDER BY
		
			o.effectivedate desc,
			o.status,
			name_partner,
			cleaner_partner) as t3
			
	LEFT JOIN
	
		salesforce.account a 
		
	ON
	
		t3.cleaner_partner = a.sfid
		
	ORDER BY
	
		year desc,
		month desc,
		effectivedate desc) as t4
		
LEFT JOIN

	salesforce.account a1
	
ON

	t4.name_partner = a1.name
	
WHERE

	a1.parentid IS NULL
	AND EXTRACT(YEAR FROM effectivedate) = 2017
	AND t4.name_partner LIKE '%Sunny%'
	
ORDER BY
	
	year desc,
	month desc,
	effectivedate desc;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.cluster_opps_won;
CREATE Table bi.cluster_opps_won as 


SELECT

	table1.*,
	o.sfid,
	o.customer__c
	
FROM

	(SELECT
	
		o.first_order_date,
		EXTRACT(year FROM o.first_order_date) as year,
		EXTRACT(MONTH FROM o.first_order_date) as month,
		EXTRACT(WEEK FROM o.first_order_date) as week,
		CASE WHEN o.polygon IS NULL THEN CASE WHEN o.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN o.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN o.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE o.polygon
											END
											as polygon,
		o.opportunity_id,
		o.contact__c,
		o.name as name_opp,
		MIN(o.grand_total__c) as revenue_won,
		CASE WHEN o.grand_total__c < 250 THEN '< 250€'
			  WHEN (o.grand_total__c >= 250 AND o.grand_total__c < 750) THEN '250€ <= AND < 750€'
			  WHEN o.grand_total__c >= 750 THEN '>= 750€'
			  ELSE 'Unknown'
			  END as category
		
	FROM
	
		bi.b2borders o
		
	WHERE
	
		o.date < current_date
		AND o.opportunity_id IS NOT NULL
	
	GROUP BY
	
		o.first_order_date,
		year,
		month,
		week,
		o.polygon,
		o.opportunity_id,
		o.contact__c,
		o.name,
		category,
		o.locale__c
		
	ORDER BY
	
		o.first_order_date desc,
		year desc,
		month desc,
		week desc) as table1
		
LEFT JOIN


	salesforce.opportunity o
	
	
ON 

	table1.opportunity_id = o.sfid;
			

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.cluster_opps_lost;
CREATE Table bi.cluster_opps_lost as 



SELECT

	t3.*,
	oo.name,
	-- oo.polygon,
	CASE WHEN oo.polygon IS NULL THEN CASE WHEN oo.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN oo.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN oo.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE oo.polygon
											END
											as polygon,
	MAX(oo.returning_month) as existence_churn,
	CASE WHEN t3.revenue_lost IS NULL THEN NULL 
					
												 ELSE CASE WHEN t3.revenue_lost < 250 THEN '< 250€'
															  WHEN (t3.revenue_lost >= 250 AND t3.revenue_lost < 750) THEN '250€ <= AND < 750€'
															  WHEN t3.revenue_lost >= 750 THEN '>= 750€'
															  ELSE 'Unknown'
															  END
												 END as category
FROM

	(SELECT
	
		t1.createddate as date_lost,
		EXTRACT(year FROM t1.createddate) as year,
		EXTRACT(MONTH FROM t1.createddate) as month,
		EXTRACT(WEEK FROM t1.createddate) as week,
		COUNT (DISTINCT t1.opportunityid) as lost_opportunities,
		t2.sfid,
		t2.customer__c,
		
		t2.grand_total__c,
		MIN(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END) as revenue_lost
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		
	GROUP BY
	
		oo.createddate,
		oo.opportunityid) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid
		
	GROUP BY
	
		t1.createddate,
		year,
		month,
		week,
		t2.sfid,
		t2.customer__c,
		t2.grand_total__c
		
	ORDER BY
	
		year desc,
		month desc,
		week desc) as t3
	
LEFT JOIN 

	bi.b2borders oo
	
ON

	t3.sfid = oo.opportunity_id
	
GROUP BY

	t3.date_lost,
	t3.year,
	t3.month,
	t3.week,
	t3.lost_opportunities,
	t3.sfid,
	t3.customer__c,
	t3.grand_total__c,
	t3.revenue_lost,
	oo.name,
	oo.polygon,
	oo.locale__c
	
ORDER BY

	year desc,
	month desc,
	week desc;
			

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cluster_type_service;
CREATE Table bi.cluster_type_service as 
	
SELECT

	table5.first_order_date,
	table5.year,
	table5.month,
	table5.week,
	table5.polygon,
	table5.opportunity_id,
	table5.name_opp,
	table5.revenue_won,
	table5.category,
	CASE WHEN table5.sfid_cleaner IS NULL THEN 'Served by BAT' ELSE 'Served by Partner' END as type_service

FROM
	
	(SELECT
	
		table3.*,
		table4.sfid as sfid_cleaner
	
	FROM	
		
		
		(SELECT
		
			table2.*,
			o.professional__c
		
		FROM	
			
			(SELECT
			
				table1.*,
				o.sfid,
				o.customer__c,
				CASE WHEN table1.revenue_won < 250 THEN '< 250€'
					  WHEN (table1.revenue_won >= 250 AND table1.revenue_won < 750) THEN '250€ <= AND < 750€'
					  WHEN table1.revenue_won >= 750 THEN '>= 750€'
					  ELSE 'Unknown'
					  END as category
				
			FROM
			
				(SELECT
				
					o.first_order_date,
					EXTRACT(year FROM o.first_order_date) as year,
					EXTRACT(MONTH FROM o.first_order_date) as month,
					EXTRACT(WEEK FROM o.first_order_date) as week,
					CASE WHEN o.polygon IS NULL THEN CASE WHEN o.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN o.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN o.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE o.polygon
											END
											as polygon,
					o.opportunity_id,
					o.contact__c,
					o.name as name_opp,
					MIN(o.grand_total__c) as revenue_won
					
					
				FROM
				
					bi.b2borders o
					
				WHERE
				
					o.date < current_date
					AND o.opportunity_id IS NOT NULL
				
				GROUP BY
				
					o.first_order_date,
					year,
					month,
					week,
					o.polygon,
					o.opportunity_id,
					o.contact__c,
					o.name,
					o.locale__c
					
				ORDER BY
				
					o.first_order_date desc,
					year desc,
					month desc,
					week desc) as table1
					
			LEFT JOIN
			
			
				salesforce.opportunity o
				
				
			ON 
			
				table1.opportunity_id = o.sfid) as table2
				
		
		LEFT JOIN
		
			bi.orders o
			
		ON 
		
			table2.contact__c = o.contact__c
			AND table2.first_order_date = o.effectivedate
			AND table2.polygon = o.polygon) as table3
			
	LEFT JOIN		
			
		(SELECT
	        t1.*,
	        t2.name as subcon
	      FROM
	       Salesforce.Account t1
	        JOIN
	            Salesforce.Account t2
	        ON
	            (t2.sfid = t1.parentid)
	    
	        WHERE t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	      and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	      and t2.name NOT LIKE '%BAT Business Services GmbH%')	as table4
			
	ON
	
		table3.professional__c = table4.sfid) as table5	
	
GROUP BY

	table5.first_order_date,
	table5.year,
	table5.month,
	table5.week,
	table5.polygon,
	table5.opportunity_id,
	table5.name_opp,
	table5.revenue_won,
	table5.category,
	table5.sfid_cleaner
	
ORDER BY

  table5.first_order_date desc;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.stage_opps;
CREATE Table bi.stage_opps as 

SELECT

	o.createddate,
	EXTRACT(YEAR FROM o.createddate) as year,
	EXTRACT(month FROM o.createddate) as month,
	o.name,
	o.stagename,
	SUM(o.grand_total__c) as revenue
	
FROM

	salesforce.opportunity o
	
GROUP BY

	o.createddate,
	year,
	month,
	name,
	stagename
	
ORDER BY

	year desc,
	month desc;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.balance_opps;
CREATE TABLE bi.balance_opps AS

SELECT

	LEFT(t2.locale__c, 2) as locale,
	t1.createddate,
	t1.opportunityid,
	'Churned'::text as kpi,
	(- COUNT (DISTINCT t1.opportunityid))::integer as count,
	(- SUM(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END))::numeric as total

FROM

	(SELECT

		MAX(oo.createddate) as createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
			AND LEFT(o.polygon, 2) LIKE 'de') t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.createddate > '2016-12-31'
		AND oo.newvalue IN ('RESIGNED', 'CANCELLED')
		
	GROUP BY 
		
		oo.opportunityid) t1
	
LEFT JOIN

	salesforce.opportunity t2

	
ON 

	t1.opportunityid = t2.sfid
	
WHERE

	-- LEFT(t2.locale__C, 2) = 'de'
	t2.name NOT LIKE 'test%'
	
GROUP BY 
	
	locale,
	t1.createddate,
	t1.opportunityid;
	
	
INSERT INTO bi.balance_opps	

SELECT

	LEFT(t2.locale__c, 2) as locale,
	t1.createddate,
	t1.opportunityid,
	'Signed'::text as kpi,
	COUNT (DISTINCT t1.opportunityid),
	SUM(CASE WHEN t2.grand_total__c IS NOT NULL THEN t2.grand_total__c ELSE t2.amount END) as total

FROM

	(SELECT

		MAX(oo.createddate) as createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.effectivedate < current_date
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
			AND LEFT(o.polygon, 2) LIKE 'de') t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		oo.createddate > '2016-12-31'
		AND oo.newvalue IN ('PENDING')
		
	GROUP BY 
		
		oo.opportunityid) t1
	
LEFT JOIN

	salesforce.opportunity t2

	
ON 

	t1.opportunityid = t2.sfid
	
WHERE

	-- LEFT(t2.locale__C, 2) = 'de'
	t2.name NOT LIKE 'test%'
	
GROUP BY 
	
	locale,
	t1.createddate,
	t1.opportunityid;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.hourly_cost_supply;
CREATE TABLE bi.hourly_cost_supply AS

SELECT

	t1.year_month,
	t1.opportunity__c,
	t1.cost_supply,
	t2.hours,
	CASE WHEN t2.hours IS NULL THEN t1.cost_supply ELSE t1.cost_supply/t2.hours END as hourly_cost_supply

FROM

	(SELECT
	
		TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
		o.opportunity__c,
	
		
		SUM(o.total_amount__c) as cost_supply
	
	FROM
	
		salesforce.productlineitem__c o
		
	GROUP BY
	
		year_month,
		o.opportunity__c
		
	ORDER BY
	
		year_month desc) as t1	
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(oo.effectivedate, 'YYYY-MM') as year_month,
		oo.opportunityid,
	
		
		SUM(oo.order_duration__c) as hours
	
	FROM
	
		salesforce.order oo
		
	GROUP BY
	
		year_month,
		oo.opportunityid
		
	ORDER BY
	
		year_month desc) as t2
		
ON

	t1.opportunity__c = t2.opportunityid
	AND t1.year_month = t2.year_month
	
GROUP BY

	t1.year_month,
	t1.opportunity__c,
	t1.cost_supply,
	t2.hours
	
ORDER BY

	t1.year_month desc;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.account_temp;
DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;
DROP TABLE IF EXISTS bi.activity_partners;

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'