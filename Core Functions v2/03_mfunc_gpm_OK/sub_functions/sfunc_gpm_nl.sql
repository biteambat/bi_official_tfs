
DELIMITER //
CREATE OR REPLACE FUNCTION bi.sfunc_gpm_nl(crunchdate date) RETURNS void AS 
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_gpm_nl';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
CREATE TABLE bi.cleaner_Stats_temp as 
SELECT
	a.sfid as professional__c,
	-- a.delivery_areas__c as delivery_areas,
	CASE WHEN a.delivery_areas__c IS NULL THEN CASE WHEN a.locale__c LIKE 'de-%' THEN 'de-other'
													WHEN a.locale__c LIKE 'ch-%' THEN 'ch-other'
													WHEN a.locale__c LIKE 'nl-%' THEN 'nl-other'
													ELSE 'at-other'
													END
											ELSE a.delivery_areas__c
											END as delivery_areas,
	a.hr_contract_start__c::date as contract_start,
	a.hr_contract_end__c::date as contract_end,
	a.hr_contract_weekly_hours_min__c as weekly_hours,
	a.type__c
FROM

   Salesforce.Account a
   
JOIN

   Salesforce.Account t2
   
ON

   (t2.sfid = a.parentid)

WHERE 

	a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%' and a.name not like '%Test%' and a.name not like '%TEST%'
	and (a.type__c like 'cleaning-b2c' or (a.type__c like '%cleaning-b2c;cleaning-b2b%'))
	and  a.test__c = '0'
	and left(a.locale__c,2) = 'nl'
GROUP BY
	a.sfid,
	a.delivery_areas__c,
	a.hr_contract_start__c,
	a.hr_contract_end__c,
	a.hr_contract_weekly_hours_min__c,
	a.type__c,
	a.locale__c;
	
	
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.gmp_per_cleaner;
CREATE TABLE bi.gmp_per_cleaner as 
SELECT
	t1.professional__c,
	Effectivedate::date as date,
SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN GMV__c
	WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date THEN GMV__c*1.06 ELSE 0 END) as GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN GMV__c ELSE 0 END) as B2C_GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2b' or type = '222') and effectivedate::date < current_Date THEN GMV__c*1.06 ELSE 0 END) as 	B2B_GMV,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN Order_Duration__c ELSE 0 END)+SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date THEN Order_Duration__c ELSE 0 END) as Hours,
	SUM(CASE WHEN Status in ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and (type = 'cleaning-b2c' or type = '60') and effectivedate::date < current_Date THEN Order_Duration__c ELSE 0 END) as B2C_Hours,
	SUM(CASE WHEN (type = 'cleaning-b2b' or type = '222') and Status in  ('INVOICED','NOSHOW CUSTOMER','PENDING TO START','FULFILLED') and effectivedate::date < current_Date THEN Order_Duration__c 		ELSE 0 END) as B2B_Hours
FROM
	bi.orders t1
WHERE
	left(locale__c,2) = 'nl'
GROUP BY
	t1.professional__c,
	date;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

	
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1;
CREATE TABLE bi.gmp_per_cleaner_v1 as 
SELECT
	t1.professional__c,
	delivery_areas,
	contract_start,
	contract_end,
	weekly_hours,
	date,
	gmv,
	b2c_gmv,
	b2b_gmv,
	hours,
	b2c_hours,
	b2b_hours,
	type__c
FROM
	bi.cleaner_Stats_temp t1
LEFT JOIn
	bi.gmp_per_cleaner t2
ON
	(t1.professional__c = t2.professional__c);

DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.holidays_cleaner;
CREATE TABLE bi.holidays_cleaner as
SELECT
	account__c,
	sfid,
	status__c,
	type__c,
	start__c,
	end__c,
	days__c
FROM
	salesforce.hr__c
WHERE
	type__c != 'unpaid';


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

	
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
CREATE TABLE bi.holidays_cleaner_2 as 
SELECT
	*,
	EXTRACT(dow from date) weekday,
	CASE WHEN date between start__c::date and end__c::date then 1 else 0 end as a
FROM
	bi.holidays_cleaner,
	bi.orderdate
WHERE
	date > '2016-01-01'
	and date < current_date::date
	and status__c in ('approved');

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.holidays_cleaner_3;
CREATE TABLE bi.holidays_cleaner_3 as 
SELECT
	date,
	account__c,
	MAX(CASE WHEN type__c = 'holidays' and date between start__c and end__c THEN a ELSE 0 END) as holiday_flag,
	MAX(CASE WHEN type__c = 'sickness' and date between start__c and end__c THEN a ELSE 0 END) as sickness_flag
FROM
	bi.holidays_cleaner_2
WHERE
	weekday != '0'
GROUP BY
	date,
	account__c;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.holidays_cleaner_4;
CREATE TABLE bi.holidays_cleaner_4 as 
SELECT
	to_char(date,'YYYY-MM') as Year_Month,
	account__c,
	SUM(sickness_flag) as sickness,
	SUM(holiday_flag) as holiday
FROM
	bi.holidays_cleaner_3
GROUP BY
	Year_Month,
	account__c;

DROP TABLE IF EXISTS bi.holidays_cleaner;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2;
CREATE TABLE bi.gpm_per_cleaner_v2 as 
select xx.*, 
		GREATEST(concat(year_month,'-01')::date,contract_start) as calc_start,
		case
		when year_month = to_char(contract_end,'YYYY-MM') AND (date_trunc('MONTH', concat(year_month,'-01')::date) + INTERVAL '1 MONTH - 1 day')::date <> contract_end THEN 0
		when  to_char(current_date,'YYYY-MM') = to_char(mindate,'YYYY-MM') THEN calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) 
		else calc_end - GREATEST(concat(year_month,'-01')::date,contract_start) + 1
		END AS days_worked
from (
SELECT	
   to_char(date,'YYYY-MM') as Year_Month,
   min(date) as mindate,
	professional__c,
	delivery_areas as delivery_area,
	type__c,
	DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE)) as days_of_month,
	contract_start,
	contract_end,
	SUM(hours) as worked_hours,
	cast(LEAST(now(), contract_end, (date_trunc('MONTH', date::date) + INTERVAL '1 MONTH - 1 day')::date) as date) as calc_end,
	SUM(GMV) as GMV,
	SUM(GMV/1.06) as total_revenue,
	SUM(B2C_GMV/1.06) as B2C_Revenue,
	SUM(B2B_GMV/1.06) as B2B_Revenue,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2B_Hours) as decimal)/SUM(Hours)) ELSE 0 END as b2b_share,
	CASE WHEN SUM(Hours) > 0 THEN (CAST(SUM(B2C_Hours) as decimal)/SUM(Hours)) ELSE 0 END as b2c_share,
	MAX(Weekly_hours) as weekly_hours,
	MAX(weekly_hours)* (DATE_PART('days', DATE_TRUNC('month', DATE) + '1 MONTH'::INTERVAL  - DATE_TRUNC('month', DATE))/7 )as monthly_hours
FROM
	bi.gmp_per_cleaner_v1 t1
WHERE
	date >= '2016-01-01'
GROUP BY
   Year_Month,
   type__c,
	professional__c,
	contract_start,
	days_of_month,
	contract_end,
	delivery_area,
	calc_end
	) xx;

------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
CREATE TABLE bi.gpm_per_cleaner_v2_2 as 
SELECT
	t1.*,
	CASE WHEN sickness IS NULL THEN 0 ELSE sickness END as sickness,
	CASE WHEN holiday IS NULL THEN 0 ELSE holiday END as holiday
FROM
	bi.gpm_per_cleaner_v2 t1
LEFT JOIN
	 bi.holidays_cleaner_4 t2
ON
	(t1.professional__c = t2.account__c and t1.year_month = t2.year_month);


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
CREATE TABLE bi.gpm_per_cleaner_v3_temp as 
SELECT
    Year_month,
    MIN(mindate) as mindate,
    professional__c,
    delivery_area,
    worked_hours,
    type__c,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    -- min(mindate) as mindate,
    holiday,
    (weekly_hours/5)*(holiday) as holiday_hours,
    (weekly_hours/5)*(sickness) as sickness_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness)+worked_hours as total_hours,
	
	CASE WHEN ((weekly_hours/5)*(sickness - holiday))+worked_hours >= (monthly_hours/days_of_month)*days_worked 
		  THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73 
		  ELSE CASE WHEN ((weekly_hours/5)*(sickness - holiday)+worked_hours) > 0 
		  				THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73
						ELSE 0
						END
		  END as salary_payed,
	
	SUM(GMV) as GMV,
	SUM(B2C_Revenue) as B2C_Revenue,
	SUM(B2B_Revenue) as B2B_Revenue,
	SUM(B2C_Share) as b2c_share,
	SUM(B2B_Share) as b2b_share,
	SUM(GMV/1.06) as Revenue,
	SUM(GMV/1.06) -  CASE WHEN ((weekly_hours/5)*(sickness - holiday))+worked_hours >= (monthly_hours/days_of_month)*days_worked 
						  THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73 
						  ELSE CASE WHEN ((weekly_hours/5)*(sickness - holiday)+worked_hours) > 0 
						  				THEN ((weekly_hours/5)*(sickness - holiday)+worked_hours)*14.73
										ELSE 0
										END
						  END as gp

FROM
     bi.gpm_per_cleaner_v2_2
     
WHERE

    LEFT(delivery_area,2) = 'nl'
    AND days_worked > 0
    
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    type__c,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    holiday,
    holiday_hours,
    sickness_hours,
    days_worked,
    worked_hours;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO bi.gpm_per_cleaner_v3 

SELECT
    year_month,
    MIN(mindate) as mindate,
    professional__c,
    delivery_area,
    worked_hours,
    contract_start,
    contract_end,
    monthly_hours,
    weekly_hours,
    days_worked,
    days_of_month,
    sickness,
    -- MIN(mindate) as mindate,
    0 as holidays,
    (weekly_hours/5)*(sickness) as sickness_hours,
   (monthly_hours/days_of_month)*days_worked as working_hours,
	(weekly_hours/5)*(sickness)+worked_hours as total_hours,
	SUM(salary_payed) as salary_payed,
	SUM(B2C_revenue) as b2c_revenue,
	SUM(B2B_revenue) as b2b_revenue,
	SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2b_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2b' THEN (b2b_revenue-salary_payed)
	ELSE b2b_revenue-(salary_payed*b2b_share) END) as b2b_gp,
		SUM(CASE 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c;cleaning-b2b' THEN (b2c_revenue-salary_payed)/2 
	WHEN b2b_share = '0' and b2c_share = '0' and type__c = 'cleaning-b2c' THEN (b2c_revenue-salary_payed)
	ELSE b2c_revenue-(salary_payed*b2c_share) END) as b2c_gp,
	gp,
	GMV,
	Revenue
	
from

     bi.gpm_per_cleaner_v3_temp
where
    LEFT(delivery_area,2) = 'nl'
    and days_worked > 0
    and professional__c != '0012000001bQCsnAAG'
GROUP BY
    year_month,
    mindate,
    professional__c,
    weekly_hours,
    contract_start,
    contract_end,
    days_of_month,
    weekly_hours,
    monthly_hours,
    working_hours,
    delivery_area,
    sickness,
    sickness_hours,
    days_worked,
    worked_hours,
    gp,
	GMV,
	Revenue;


------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner;
DROP TABLE IF EXISTS bi.cleaner_Stats_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_temp;
DROP TABLE IF EXISTS bi.gmp_per_cleaner_v1_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_2;
DROP TABLE IF EXISTS bi.holidays_cleaner_3_temp;
DROP TABLE IF EXISTS bi.holidays_cleaner_4_temp;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v2_2;
DROP TABLE IF EXISTS bi.gpm_per_cleaner_v3_temp;
DROP TABLE IF EXISTS bi.order_distribution_temp;
DROP TABLE IF EXISTS bi.cleaner_ur_temp_temp;




------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;
$BODY$ LANGUAGE 'plpgsql'
