

CREATE OR REPLACE FUNCTION bi.sfunc_KPIs_Master(crunchdate date) RETURNS void AS

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_KPIs_Master';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.kpis_master;
CREATE TABLE bi.kpis_master AS


-- GROSS GMV per status

    SELECT
        o.effectivedate::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'GMV Gross'::text as kpi,
        SUM(o.gmv_eur)::numeric as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, left(o.locale__c,2)::text, o.polygon, time_format, kpi_type, business_type, o.status, o.marketing_channel, breakdown2, kpi

    ORDER BY o.effectivedate desc, left(o.locale__c,2)::text asc, o.polygon asc, o.status asc

;


INSERT INTO bi.kpis_master

-- NET GMV per status


    SELECT
        o.effectivedate::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        o.status::text as breakdown2,
        'GMV Net'::text as kpi,
        SUM(o.gmv_eur_net) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, left(o.locale__c,2)::text, o.polygon, time_format, kpi_type, business_type, o.status, breakdown2, kpi

    ORDER BY o.effectivedate desc, left(o.locale__c,2)::text asc, o.polygon asc, o.status asc

;


INSERT INTO bi.kpis_master

-- Hours per status


    SELECT
        o.effectivedate::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        o.status::text as breakdown2,
        'Hours'::text as kpi,
        SUM(o.order_duration__c) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, left(o.locale__c,2)::text, o.polygon, time_format, kpi_type, business_type, o.status, breakdown2, kpi

    ORDER BY o.effectivedate desc, left(o.locale__c,2)::text asc, o.polygon asc, o.status asc

;


INSERT INTO bi.kpis_master

-- Orders per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Orders'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon, o.status, o.marketing_channel

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon asc, o.status asc, o.marketing_channel asc

;


INSERT INTO bi.kpis_master

-- Orders per ordertype


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        (CASE WHEN o.acquisition_new_customer__c = '1' THEN 'Acquisition' WHEN (o.acquisition_new_customer__c = '0' AND o.acquisition_channel__c = 'recurrent') THEN 'Recurrent' WHEN (o.acquisition_new_customer__c = '0' AND o.acquisition_channel__c = 'web') THEN 'Rebookings' END)::text as breakdown2,
        'Bookings by Acquisition Phase'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.status NOT IN ('CANCELLED FAKED', 'CANCELLED MISTAKE')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon, o.marketing_channel, breakdown2

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon asc, o.marketing_channel asc, breakdown2

;




INSERT INTO bi.kpis_master

-- Acquisitions per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Acquisitions'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '1'

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.status, o.marketing_channel

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.status asc, o.marketing_channel asc

;



INSERT INTO bi.kpis_master

-- Rebookings per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Rebookings'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '0' 
        AND o.acquisition_channel__c in ('web','ios','android')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.status, o.marketing_channel

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.status asc, o.marketing_channel asc

;



INSERT INTO bi.kpis_master

-- AutoRecurrent orders per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Recurrent'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '0' 
        AND o.acquisition_channel__c in ('recurrent')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.status, o.marketing_channel

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.status asc, o.marketing_channel asc

;


-- Same for the GMV

INSERT INTO bi.kpis_master

-- Acquisitions per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'GMV Acquisitions'::text as kpi,
        SUM(gmv_eur) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '1'

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, time_format, kpi_type, business_type, o.status, o.marketing_channel, kpi

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.status asc, o.marketing_channel asc

;



INSERT INTO bi.kpis_master

-- Rebookings per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'GMV Rebookings'::text as kpi,
        SUM(gmv_eur) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '0' 
        AND o.acquisition_channel__c in ('web','ios','android')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, time_format, kpi_type, business_type, o.marketing_channel, o.status, kpi

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.marketing_channel asc, o.status asc

;



INSERT INTO bi.kpis_master

-- AutoRecurrent orders per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'GMV Recurrent'::text as kpi,
        SUM(gmv_eur) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '0' 
        AND o.acquisition_channel__c in ('recurrent')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, time_format, kpi_type, business_type, o.marketing_channel, o.status, kpi

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.marketing_channel asc, o.status asc

;



-- Same for the Distinct Customers

INSERT INTO bi.kpis_master

-- Acquisitions per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Acquired customers'::text as kpi,
        COUNT(DISTINCT o.customer_id__c) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '1'

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.marketing_channel, o.status

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.marketing_channel asc, o.status asc

;



INSERT INTO bi.kpis_master

-- Rebookings per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Rebooking customers'::text as kpi,
        COUNT(DISTINCT o.customer_id__c) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '0' 
        AND o.acquisition_channel__c in ('web','ios','android')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.marketing_channel, o.status

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.marketing_channel asc, o.status asc

;



INSERT INTO bi.kpis_master

-- AutoRecurrent orders per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Recurrent customers'::text as kpi,
        COUNT(DISTINCT o.customer_id__c) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.acquisition_new_customer__c = '0' 
        AND o.acquisition_channel__c in ('recurrent')

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.marketing_channel, o.status

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.marketing_channel asc, o.status asc

;





INSERT INTO bi.kpis_master

-- Acquisitions per status


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2B'::text as business_type,
        o.marketing_channel::text as breakdown1,
        o.status::text as breakdown2,
        'Acquisitions'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '2'
        AND o.acquisition_new_customer__c = '1'

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon::text, o.marketing_channel, o.status

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon::text asc, o.marketing_channel asc, o.status asc

;


INSERT INTO bi.kpis_master

-- Total Onboardings

    SELECT
        a.hr_contract_start__c::date as date,
        left(a.locale__c,2) as locale,
        a.delivery_areas__c::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Onboardings'::text as kpi,
        COUNT(DISTINCT a.sfid) as value
    FROM
        Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED') and (t2.name like '%BAT%' or t2.name like '%BOOK%') and a.test__c = '0' and a.name not like '%test%'
        
    GROUP BY a.hr_contract_start__c, left(a.locale__c,2), a.delivery_areas__c

    ORDER BY date desc, locale asc, city asc, business_type asc
   
;


----------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO bi.kpis_master VALUES ('2016-08-29','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','10');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-08-29','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','2');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','10');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-05','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','9');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-12','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','2');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','9');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-19','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','12');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-09-26','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','12');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-10-03','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','12');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-10-10','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','2');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','2');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','12');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-10-17','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','12');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-10-24','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','8');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','5');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-10-31','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','8');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-11-07','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','8');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-11-14','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','2');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','8');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-11-21','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','at','at-vienna','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-basel','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-bern','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-geneva','Weekly','Absolute','B2C/B2B','','','Onboarding targets','1');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-lausanne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-lucerne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-stgallen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','ch','ch-zurich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','6');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-berlin','Weekly','Absolute','B2C/B2B','','','Onboarding targets','8');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-bonn','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-cologne','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-dusseldorf','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-essen','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-frankfurt','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-hamburg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','4');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-hannover','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-leipzig','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-mainz','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-manheim','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-munich','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-nuremberg','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-potsdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','de','de-stuttgart','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','nl','nl-amsterdam','Weekly','Absolute','B2C/B2B','','','Onboarding targets','3');
INSERT INTO bi.kpis_master VALUES ('2016-11-28','nl','nl-hague','Weekly','Absolute','B2C/B2B','','','Onboarding targets','0');



----------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------




--<<<<<<< HEAD
INSERT INTO bi.kpis_master

-- Total Offboardings

    SELECT
        a.hr_contract_end__c::date as date,
        left(a.locale__c,2) as locale,
        a.delivery_areas__c::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Onboardings'::text as kpi,
        COUNT(DISTINCT a.sfid) as value

    FROM salesforce.account a 

    WHERE a.type__c not in ('cleaning-b2b') 
        AND a.hr_contract_end__c IS NOT NULL

    GROUP BY a.hr_contract_end__c, left(a.locale__c,2), a.delivery_areas__c

    ORDER BY date desc, locale asc, city asc, kpi asc, kpi_type asc, business_type asc
   
;

--=======
-->>>>>>> 643780e5f3f7042580ab467dc3ace5998d4afe2e


INSERT INTO bi.kpis_master

-- Total Leads

    SELECT
        l.date as date,
        l.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Leads'::text as kpi,
        SUM(l.leads) as value

    FROM bi.cleaners_costsperonboarding_polygon l

    GROUP BY l.date, l.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY l.date desc, l.locale asc


;


INSERT INTO bi.kpis_master

-- Salary payed

    SELECT
        m.mindate as date,
        LEFT(m.delivery_area,2) as locale,
        m.delivery_area as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Salary paid'::text as kpi,
        SUM(m.salary_payed) as value

    FROM bi.gpm_weekly_cleaner m

    WHERE m.mindate < current_date

    GROUP BY m.mindate, LEFT(m.delivery_area,2), m.delivery_area, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY m.mindate desc, LEFT(m.delivery_area,2) asc, m.delivery_area asc


;


INSERT INTO bi.kpis_master

-- Revenue earned

    SELECT
        m.mindate as date,
        LEFT(m.delivery_area,2) as locale,
        m.delivery_area as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Invoiced revenue'::text as kpi,
        SUM(m.revenue) as value

    FROM bi.gpm_weekly_cleaner m

    WHERE m.mindate < current_date

    GROUP BY m.mindate, LEFT(m.delivery_area,2), m.delivery_area, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY m.mindate desc, LEFT(m.delivery_area,2) asc, m.delivery_area asc


;


INSERT INTO bi.kpis_master

-- CANCELLATION FEES BY TYPE AND PAYMENT METHOD


    SELECT
        t1.received__c::date as date,
        left(t1.locale__c,2)::text as locale,
        t2.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        t1.type__c::text as breakdown1,
        t1.method__c::text as breakdown2,
        'Fees charged'::text as kpi,
        SUM(amount__c) as value

    FROM salesforce.payment__c t1
        JOIN bi.orders t2 ON t1.order__c = t2.sfid

    WHERE t2.test__c = '0'
        AND t2.order_type = '1'
        AND t1.received__c::date >= '2016-09-01'
        AND t1.type__c IN ('cancellation_fee','reschedule_fee','noshow')

    GROUP BY t1.received__c::date, left(t1.locale__c,2)::text, t2.polygon, time_format, kpi_type, business_type, t1.type__c, t1.method__c, kpi

    ORDER BY t1.received__c::date desc, left(t1.locale__c,2)::text asc, t2.polygon asc, t1.type__c asc

;


INSERT INTO bi.kpis_master

-- Worked hours

    SELECT
        m.mindate as date,
        LEFT(m.delivery_area,2) as locale,
        m.delivery_area as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Worked hours'::text as kpi,
        SUM(m.worked_hours) as value

    FROM bi.gpm_weekly_cleaner m

    WHERE m.mindate < current_date

    GROUP BY m.mindate, LEFT(m.delivery_area,2), m.delivery_area, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY m.mindate desc, LEFT(m.delivery_area,2) asc, m.delivery_area asc


;

INSERT INTO bi.kpis_master

-- Weekly hours

    SELECT
        m.mindate as date,
        LEFT(m.delivery_area,2) as locale,
        m.delivery_area as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Weekly hours'::text as kpi,
        SUM(m.weekly_hours) as value

    FROM bi.gpm_weekly_cleaner m

    WHERE m.mindate < current_date

    GROUP BY m.mindate, LEFT(m.delivery_area,2), m.delivery_area, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY m.mindate desc, LEFT(m.delivery_area,2) asc, m.delivery_area asc

;


INSERT INTO bi.kpis_master

-- Daily # churns

    SELECT
        c.date as date,
        c.locale as locale,
        c.polygon as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        '# total churns'::text as kpi,
        COUNT(DISTINCT c.order_id) as value

    FROM bi.churns_motivations c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, c.polygon

    ORDER BY c.date desc, c.locale asc, c.polygon asc

;

INSERT INTO bi.kpis_master

-- Daily bad rate churns

    SELECT
        c.date as date,
        c.locale as locale,
        c.polygon as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        '# bad rate churns'::text as kpi,
        SUM(CASE WHEN c.count_badrate_l28d > '0' AND c.count_nmp_l28d = '0' THEN 1 ELSE 0 END) as value

    FROM bi.churns_motivations c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, c.polygon, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc, c.polygon asc

;



INSERT INTO bi.kpis_master

-- Daily bad rate churns

    SELECT
        c.date as date,
        c.locale as locale,
        c.polygon as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        '# NMP churns'::text as kpi,
        SUM(CASE WHEN c.count_badrate_l28d = '0' AND c.count_nmp_l28d > '0' THEN 1 ELSE 0 END) as value

    FROM bi.churns_motivations c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, c.polygon, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc, c.polygon asc

;



INSERT INTO bi.kpis_master

-- Daily unattributed rate churns

    SELECT
        c.date as date,
        c.locale as locale,
        c.polygon as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Unattributed churns'::text as kpi,
        (SUM(CASE WHEN c.count_badrate_l28d = 0 AND c.count_nmp_l28d = 0 THEN 1 ELSE 0 END) + SUM(CASE WHEN c.count_badrate_l28d > 0 AND c.count_nmp_l28d > 0 THEN 1 ELSE 0 END)) as value

    FROM bi.churns_motivations c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, c.polygon, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc, c.polygon asc

;



INSERT INTO bi.kpis_master

-- Trial bookings per day


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        o.status::text as breakdown2,
        'Trial bookings'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.recurrency__c = '0'

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon, o.status

    ORDER BY o.order_creation__c::date desc, left(o.locale__c,2)::text asc, o.polygon asc, o.status asc

;




INSERT INTO bi.kpis_master

-- Trial bookings with voucher per day


    SELECT
        o.order_creation__c::date as date,
        left(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        o.status::text as breakdown2,
        'Trial bookings w/ voucher'::text as kpi,
        COUNT(DISTINCT o.sfid) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.order_type = '1'
        AND o.recurrency__c = '0'
        AND (o.voucher__c IS NOT NULL)

    GROUP BY o.order_creation__c::date, left(o.locale__c,2)::text, o.polygon, o.status

    ORDER BY o.order_creation__c::date, left(o.locale__c,2)::text asc, o.polygon asc, o.status asc

;



INSERT INTO bi.kpis_master


-- Invoiced GMV Forecast B2C per polygon/locale

    SELECT
        current_date::date as date,
        i.locale::text as locale,
        CASE WHEN i.city_polygon IS NULL THEN 'Out of polygons' ELSE i.city_polygon END as city,
        ''::text as time_format,
        'Forecast EOM'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Invoiced GMV'::text as kpi,
        i.forecast as value 

    FROM bi.invoiced_gmv_forecast_polygon i
;


INSERT INTO bi.kpis_master


-- Invoiced Acquisitions Forecast B2C per polygon/locale

    SELECT
        current_date::date as date,
        i.locale::text as locale,
        CASE WHEN i.city_polygon IS NULL THEN 'Out of polygons' ELSE i.city_polygon END as city,
        ''::text as time_format,
        'Forecast EOM'::text as kpi_type,
        'B2C'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Invoiced Acquisitions'::text as kpi,
        i.forecast_invoiced_acquisitions as value 

    FROM bi.invoiced_gmv_forecast_polygon i
;


INSERT INTO bi.kpis_master


-- Invoiced GMV Forecast B2B per locale

    SELECT
        current_date::date as date,
        i.locale::text as locale,
        NULL::text as city,
        ''::text as time_format,
        'Forecast EOM'::text as kpi_type,
        'B2B'::text as business_type,
        ''::text as breakdown1,
        ''::text as breakdown2,
        'Invoiced GMV'::text as kpi,
        i.b2b_forecast as value

    FROM bi.invoiced_forecast_b2c_b2b i
;



INSERT INTO bi.kpis_master


-- Likelies costs per locale, daily

    SELECT
        c.date::date as date,
        c.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        c.source_channel::text as breakdown1,
        ''::text as breakdown2,
        'Likelies costs'::text as kpi,
        c.cost as value

    FROM bi.b2c_cost_per_likelies c
;



INSERT INTO bi.kpis_master


-- Likelies nb per locale, daily

    SELECT
        c.date::date as date,
        c.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        c.source_channel::text as breakdown1,
        ''::text as breakdown2,
        'Nb likelies'::text as kpi,
        c.nb_likelies as value

    FROM bi.b2c_cost_per_likelies c
;



INSERT INTO bi.kpis_master


-- Return rates per city

    SELECT
        r.date::date as date,
        ''::text as locale,
        LOWER(r.city)::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        'City level'::text as breakdown1,
        ''::text as breakdown2,
        'Return Rate M1'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p1' AND r.kpi_type = 'City analysis'

    GROUP BY r.date, LOWER(r.city)::text, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY r.date desc, LOWER(r.city)::text asc
;



INSERT INTO bi.kpis_master


-- Return rates per city

    SELECT
        r.date::date as date,
        ''::text as locale,
        LOWER(r.city)::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        'City level'::text as breakdown1,
        ''::text as breakdown2,
        'Return Rate M3'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p3' AND r.kpi_type = 'City analysis'

    GROUP BY r.date, LOWER(r.city)::text, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY r.date desc, LOWER(r.city)::text asc
;





INSERT INTO bi.kpis_master


-- Return rates per locale

    SELECT
        r.date::date as date,
        r.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        'Country level'::text as breakdown1,
        ''::text as breakdown2,
        'Return Rate M1'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p1' AND r.kpi_type = 'Global'

    GROUP BY r.date, r.locale::text, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY r.date desc, r.locale asc
;



INSERT INTO bi.kpis_master


-- Return rates per locale

    SELECT
        r.date::date as date,
        r.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        'Country level'::text as breakdown1,
        ''::text as breakdown2,
        'Return Rate M3'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p3' AND r.kpi_type = 'Global'

    GROUP BY r.date, r.locale::text, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY r.date desc, r.locale asc
;



INSERT INTO bi.kpis_master


-- Return rates per locale

    SELECT
        r.date::date as date,
        r.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        'Country level'::text as breakdown1,
        ''::text as breakdown2,
        'Return Rate M6'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p6' AND r.kpi_type = 'Global'

    GROUP BY r.date, r.locale::text, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY r.date desc, r.locale asc
;


INSERT INTO bi.kpis_master


-- Return rates per channel

    SELECT
        r.date::date as date,
        r.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        r.channel::text as breakdown1,
        'Channel level'::text as breakdown2,
        'Return Rate M1'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p1' AND r.kpi_type = 'Channel analysis'

    GROUP BY r.date, r.locale::text, time_format, kpi_type, business_type, breakdown1, r.channel, kpi

    ORDER BY r.date desc, r.locale asc
;


INSERT INTO bi.kpis_master


-- Return rates per channel

    SELECT
        r.date::date as date,
        r.locale::text as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Ratio'::text as kpi_type,
        'B2C'::text as business_type,
        r.channel::text as breakdown1,
        'Channel level'::text as breakdown2,
        'Return Rate M3'::text as kpi,
        SUM(r.cohort_return_rate) as value

    FROM bi.retention_marketing r

    WHERE r.period = 'p3' AND r.kpi_type = 'Channel analysis'

    GROUP BY r.date, r.locale::text, time_format, kpi_type, business_type, breakdown1, r.channel, kpi

    ORDER BY r.date desc, r.locale asc
;




INSERT INTO bi.kpis_master


-- Facebook leadgen tracking data

    SELECT
        f.order_created::date as date,
        f.locale::text as locale,
        f.city::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        f.acquisition_tracking::text as breakdown1,
        f.voucher::text as breakdown2,
        'Facebook Leadgen Tracking'::text as kpi,
        COUNT(DISTINCT f.order_id) as value

    FROM bi.fb_voucher_acquisitions f

    WHERE (f.locale IS NOT NULL OR f.city IS NOT NULL)

    GROUP BY f.order_created, f.locale, f.city, f.voucher, f.acquisition_tracking

    ORDER BY date desc, locale asc, city asc, breakdown1 asc, breakdown2 asc  
;





INSERT INTO bi.kpis_master


-- Subscriptions by recurrency level

    SELECT
        o.effectivedate::date as date,
        LEFT(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.recurrency__c::text as breakdown1,
        ''::text as breakdown2,
        '# subscriptions per recurrency'::text as kpi,
        COUNT(DISTINCT o.customer_id__c) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.recurrency__c in ('7','14','21','28')
        AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
        AND o.acquisition_channel__c in ('android','ios','web')
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon, o.recurrency__c

    ORDER BY o.effectivedate desc, LEFT(o.locale__c,2) asc, o.polygon asc, o.recurrency__c asc
;





INSERT INTO bi.kpis_master


-- Sources of new subscriptions

    SELECT
        o.effectivedate::date as date,
        LEFT(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        CASE WHEN o.acquisition_new_customer__c = '1' THEN 'New subscribers'::text ELSE 'Existing subscribers'::text END as breakdown1,
        ''::text as breakdown2,
        'Source of new subscribers'::text as kpi,
        COUNT(DISTINCT o.customer_id__c) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.recurrency__c in ('7','14','21','28')
        AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
        AND o.acquisition_channel__c in ('android','ios','web')
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon, breakdown1

    ORDER BY o.effectivedate desc, LEFT(o.locale__c,2) asc, o.polygon asc, breakdown1 asc
;



INSERT INTO bi.kpis_master


-- Invoiced revenue by recurrency level

    SELECT
        o.effectivedate::date as date,
        LEFT(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.recurrency__c::text as breakdown1,
        'Gross'::text as breakdown2,
        'GMV per recurrency level'::text as kpi,
        SUM(gmv_eur) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.recurrency__c in ('7','14','21','28')
        AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
        AND o.acquisition_channel__c in ('android','ios','web')
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon, o.recurrency__c

    ORDER BY o.effectivedate desc, LEFT(o.locale__c,2) asc, o.polygon asc, o.recurrency__c asc
;




INSERT INTO bi.kpis_master


-- Invoiced revenue by recurrency level

    SELECT
        o.effectivedate::date as date,
        LEFT(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        o.recurrency__c::text as breakdown1,
        'Net'::text as breakdown2,
        'GMV per recurrency level'::text as kpi,
        SUM(gmv_eur_net) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.recurrency__c in ('7','14','21','28')
        AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
        AND o.acquisition_channel__c in ('android','ios','web')
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon, time_format, kpi_type, business_type, o.recurrency__c, breakdown2, kpi

    ORDER BY o.effectivedate desc, LEFT(o.locale__c,2) asc, o.polygon asc, o.recurrency__c asc
;





INSERT INTO bi.kpis_master


-- GMV per customer type

    SELECT
        o.effectivedate::date as date,
        LEFT(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        CASE WHEN acquisition_new_customer__c = '1' THEN 'New subscribers'::text ELSE 'Existing subscribers'::text END as breakdown1,
        'Gross'::text as breakdown2,
        'Invoiced GMV per Customer Type'::text as kpi,
        SUM(o.gmv_eur) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.recurrency__c in ('7','14','21','28')
        AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
        AND o.acquisition_channel__c in ('android','ios','web')
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY o.effectivedate desc, LEFT(o.locale__c,2) asc, o.polygon asc, breakdown1 asc
;





INSERT INTO bi.kpis_master


-- GMV per customer type

    SELECT
        o.effectivedate::date as date,
        LEFT(o.locale__c,2)::text as locale,
        o.polygon::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C'::text as business_type,
        CASE WHEN acquisition_new_customer__c = '1' THEN 'New subscribers'::text ELSE 'Existing subscribers'::text END as breakdown1,
        'Net'::text as breakdown2,
        'Invoiced GMV per Customer Type'::text as kpi,
        SUM(o.gmv_eur_net) as value

    FROM bi.orders o

    WHERE o.test__c = '0'
        AND o.recurrency__c in ('7','14','21','28')
        AND o.status in ('INVOICED','NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
        AND o.acquisition_channel__c in ('android','ios','web')
        AND o.order_type = '1'

    GROUP BY o.effectivedate::date, LEFT(o.locale__c,2), o.polygon, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY o.effectivedate desc, LEFT(o.locale__c,2) asc, o.polygon asc, breakdown1 asc
;


-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------

-- Daily Costs per channel

INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'SEM'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.sem_non_brand) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;



INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'SEM Brand'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.sem_brand) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;



INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'SEO'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.seo) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Facebook'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.facebook) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Display'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.criteo + c.sociomantic + c.gdn) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Youtube'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.youtube) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Coops'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.coops) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'TV'::text as breakdown1,
        ''::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.tvcampaign) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;




INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Voucher Campaigns'::text as breakdown1,
        'Acquisition Vouchers'::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.other_voucher) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;



INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Voucher Campaigns'::text as breakdown1,
        'Rebookings Vouchers'::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.other_voucher_reb) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Voucher Campaigns'::text as breakdown1,
        'Deindeal Vouchers'::text as breakdown2,
        'Marketing costs'::text as kpi,
        SUM(c.deindeal_voucher) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'SEM'::text as breakdown1,
        ''::text as breakdown2,
        'Discounts'::text as kpi,
        SUM(c.sem_discount) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'SEM Brand'::text as breakdown1,
        ''::text as breakdown2,
        'Discounts'::text as kpi,
        SUM(c.sembrand_discount) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Display'::text as breakdown1,
        ''::text as breakdown2,
        'Discounts'::text as kpi,
        SUM(c.display_discout) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Facebook'::text as breakdown1,
        ''::text as breakdown2,
        'Discounts'::text as kpi,
        SUM(c.facebook_discount) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Voucher Campaigns'::text as breakdown1,
        ''::text as breakdown2,
        'Discounts'::text as kpi,
        SUM(c.vouchercampaigns_discount) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;


INSERT INTO bi.kpis_master

    SELECT
        c.date as date,
        c.locale as locale,
        NULL::text as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2C/B2B'::text as business_type,
        'Brand Marketing Offline'::text as breakdown1,
        ''::text as breakdown2,
        'Discounts'::text as kpi,
        SUM(c.brandmktgoffline_discount) as value

    FROM bi.cpacalclocale c

    WHERE c.date < current_date

    GROUP BY c.date, c.locale, city, time_format, kpi_type, business_type, breakdown1, breakdown2, kpi

    ORDER BY c.date desc, c.locale asc

;



-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------


-- B2B KPIs

INSERT INTO bi.kpis_master

    SELECT
        op.closedate as date,
        LEFT(op.locale__c,2) as locale,
        LEFT(op.locale__c,2) || '-' || LOWER(op.billingaddress_city__c) as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2B'::text as business_type,
        (CASE WHEN op.acquisition_channel__c::text IN ('web','inbound') THEN 'inbound' ELSE 'outbound' END)::text as breakdown1,
        (CASE WHEN op.stagename = 'IRREGULAR' THEN 'One-off' ELSE 'Recurrent' END)::text as breakdown2,
        'Closed deals B2B' as kpi,
        COUNT(DISTINCT op.id) as value

    FROM salesforce.opportunity op

    WHERE op.test__c = '0'
        AND op.stagename IN ('SIGNED','IRREGULAR','DECLINED','TERMINATED','RUNNING')
        AND op.closedate < current_date
        AND EXTRACT(MONTH FROM(op.closedate)) = EXTRACT(MONTH FROM(current_date))
        AND EXTRACT(YEAR FROM(op.closedate)) = EXTRACT(YEAR FROM(current_date)) 

    GROUP BY op.closedate, LEFT(op.locale__c,2), op.billingaddress_city__c, breakdown1, breakdown2

    ORDER BY op.closedate desc, locale asc, city asc, breakdown1 asc, breakdown2 asc

;

INSERT INTO bi.kpis_master

    SELECT
        op.closedate as date,
        LEFT(op.locale__c,2) as locale,
        LEFT(op.locale__c,2) || '-' || LOWER(op.billingaddress_city__c) as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2B'::text as business_type,
        'Overall'::text as breakdown1,
        NULL::text as breakdown2,
        'Closed deals B2B' as kpi,
        COUNT(DISTINCT op.id) as value

    FROM salesforce.opportunity op

    WHERE op.test__c = '0'
        AND op.stagename IN ('SIGNED','IRREGULAR','DECLINED','TERMINATED','RUNNING')
        AND op.closedate < current_date
        AND EXTRACT(MONTH FROM(op.closedate)) = EXTRACT(MONTH FROM(current_date))
        AND EXTRACT(YEAR FROM(op.closedate)) = EXTRACT(YEAR FROM(current_date)) 

    GROUP BY op.closedate, LEFT(op.locale__c,2), op.billingaddress_city__c

    ORDER BY op.closedate desc, locale asc, city asc, breakdown1 asc, breakdown2 asc

;


INSERT INTO bi.kpis_master

    SELECT
        op.closedate as date,
        LEFT(op.locale__c,2) as locale,
        LEFT(op.locale__c,2) || '-' || LOWER(op.billingaddress_city__c) as city,
        'Daily'::text as time_format,
        'Absolute'::text as kpi_type,
        'B2B'::text as business_type,
        (CASE WHEN op.acquisition_channel__c::text IN ('web','inbound') THEN 'inbound' ELSE 'outbound' END)::text as breakdown1,
        (CASE WHEN op.stagename = 'IRREGULAR' THEN 'One-off' ELSE 'Recurrent' END)::text as breakdown2,
        'Closed revenue B2B' as kpi,
        SUM(CASE WHEN op.amount IS NOT NULL THEN op.amount ELSE 0 END) as value

    FROM salesforce.opportunity op

    WHERE op.test__c = '0'
        AND op.stagename IN ('SIGNED','IRREGULAR','DECLINED','TERMINATED','RUNNING')
        AND op.closedate < current_date
        AND EXTRACT(MONTH FROM(op.closedate)) = EXTRACT(MONTH FROM(current_date))
        AND EXTRACT(YEAR FROM(op.closedate)) = EXTRACT(YEAR FROM(current_date)) 

    GROUP BY op.closedate, LEFT(op.locale__c,2), op.billingaddress_city__c, time_format, kpi_type, business_type, breakdown1, breakdown2

    ORDER BY op.closedate desc, locale asc, city asc, breakdown1 asc, breakdown2 asc

;

INSERT INTO bi.kpis_master
SELECT
	date as date,
	Left(polygon,2) as locale,
	polygon as city,
	'Daily'::text as time_format,
    'Absolute'::text as kpi_type,
    'B2C'::text as business_type,
	'-' as breakdown1,
	'-' as breakdown2,
	'Customer with same cleaner' as kpi,
	CAST(SUM(CASE WHEN cleaners = 1 and orders > 0 THEN 1 ELSE 0 END) as decimal) as customer_with_same_cleaner
FROM(
SELECT
	a.date,
	contact__c,
	polygon,
	COUNT(DISTINCT(order_id__c)) as Orders,
	COUNT(DISTINCT(professional__c)) as Cleaners
FROM
	bi.orders,
	(select i::date as date from generate_series('2016-05-01', 
  current_date - Interval '2 days', '1 day'::interval) as i) as a
WHERE
	effectivedate::date between date::date - Interval '47 Days' and date::date - Interval '2 Days'
	and status = 'INVOICED'
	and order_type = '1'
	and recurrency__c > '0'
GROUP By
	a.date,
	contact__c,
	polygon) as a 
GROUP By
	date,
	polygon,
	locale;

INSERT INTO bi.kpis_master
SELECT
	date as date,
	Left(polygon,2) as locale,
	polygon as city,
	'Daily'::text as time_format,
    'Absolute'::text as kpi_type,
    'B2C'::text as business_type,
	'-' as breakdown1,
	'-' as breakdown2,
	'all customer l30d' as kpi,
    COUNT(DISTINCT(contact__c)) as all_customer 

FROM(
SELECT
	a.date,
	contact__c,
	polygon,
	COUNT(DISTINCT(order_id__c)) as Orders,
	COUNT(DISTINCT(professional__c)) as Cleaners
FROM
	bi.orders,
	(select i::date as date from generate_series('2016-05-01', 
  current_date - Interval '2 days', '1 day'::interval) as i) as a
WHERE
	effectivedate::date between date::date - Interval '47 Days' and date::date - Interval '2 Days'
	and status = 'INVOICED'
	and order_type = '1'
	and recurrency__c > '0'

GROUP By
	a.date,
	contact__c,
	polygon) as a 
GROUP By
	date,
	polygon,
	locale;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);


EXCEPTION WHEN others THEN 

    INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'           
            
                    