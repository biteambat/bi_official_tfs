CREATE OR REPLACE FUNCTION bi.sfunc_KPIs_CLV(crunchdate date) RETURNS void AS 

$BODY$

DECLARE 

function_name varchar := 'bi.sfunc_KPIs_CLV';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.temp_gp_city_monthly;
CREATE TABLE bi.temp_gp_city_monthly AS
	
	SELECT
		year_month,
		delivery_area as polygon,
		SUM(b2c_revenue) as b2c_revenue,
		SUM(b2c_gp) as b2c_gp,
		SUM(b2c_revenue)/NULLIF(SUM(b2c_gp),0) as b2c_gp_pcent

	FROM bi.gpm_city_over_time

	WHERE delivery_area LIKE ('%-%')

	GROUP BY year_month, delivery_area
	ORDER BY delivery_area asc, year_month desc
;


DROP TABLE IF EXISTS bi.temp_acq_city_monthly;
CREATE TABLE bi.temp_acq_city_monthly AS

	SELECT
		to_char(order_creation__c, 'YYYY-MM') as year_month,
		polygon as polygon,
		COUNT(DISTINCT CASE WHEN acquisition_new_customer__c = '1' THEN customer_id__c ELSE NULL END) as nb_acquisitions,
		COUNT(DISTINCT CASE WHEN acquisition_new_customer__c = '0' THEN customer_id__c ELSE NULL END) as nb_rebookings

	FROM bi.orders

	WHERE test__c = '0'
		AND order_type = '1'
		--AND acquisition_new_customer__c = '1'
		AND status = 'INVOICED'
		AND LEFT(locale__c, 2) = 'de'

	GROUP BY to_char(order_creation__c, 'YYYY-MM'), polygon

	ORDER BY polygon asc, year_month desc

;

DROP TABLE IF EXISTS bi.temp_cpa_city_monthly;
CREATE TABLE bi.temp_cpa_city_monthly AS

	SELECT 
		to_char(date, 'YYYY-MM') as year_month,
		polygon as polygon,
		SUM(Sem_Cost+Sem_Brand_Cost+Facebook_Cost+Display_Cost+Seo_Cost+Offline_Cost
			+Display_Discount+Facebook_Discount+Facebook_Organic_Discount
			+Newsletter_Discount+Offline_Discount+Sem_Brand_Discount
			+Sem_Discount+Seo_Brand_Discount+Seo_Discount+Youtube_Discount
			+Display_Discount_Reb+Facebook_Discount_Reb+Facebook_Organic_Discount_Reb+Newsletter_Discount_Reb+Offline_Discount_Reb
			+Sem_Brand_Discount_Reb+Sem_Discount_Reb+Seo_Brand_Discount_Reb+Seo_Discount_Reb+Vouchers_Cost_Reb+Youtube_Discount_Reb)
			/

			NULLIF(SUM(all_acq),0) as cpa

	FROM bi.cpacalcpolygon

	WHERE locale = 'de'

	GROUP BY to_char(date, 'YYYY-MM'), polygon

	ORDER BY polygon asc, year_month desc

;

DROP TABLE IF EXISTS bi.clv_newcalculation;
CREATE TABLE bi.clv_newcalculation AS

	SELECT
		t.*,
		SUM(t.cohort_gp) OVER (PARTITION BY t.cohort, t.polygon ORDER BY t.order_month) as cum_cohort_gp,
		SUM(t.b2c_revenue) OVER (PARTITION BY t.cohort, t.polygon ORDER BY t.order_month) as cum_revenue,
		SUM(t.cohort_gp) OVER (PARTITION BY t.cohort, t.polygon ORDER BY t.order_month) / SUM(t.b2c_revenue) OVER (PARTITION BY t.cohort, t.polygon ORDER BY t.order_month) as cum_cohort_gp_pcent
	FROM
		(SELECT
			t1.cohort,
			t1.order_month,
			t1.returning_month,
			LOWER(t1.city) as polygon,
			SUM(t1.total_cohort) as total_cohort,
			t3.nb_acquisitions as ordermonth_acquisitions,
			SUM(t1.returning_customer) as returning_cust,
			SUM(t1.hours) as hours,
			SUM(t1.revenue) as b2c_revenue,
			(t2.b2c_gp/t2.b2c_revenue)*SUM(t1.revenue) as cohort_gp,
			t4.cpa

		FROM bi.customer_cohorts_recurrent_aggr t1

		LEFT JOIN bi.temp_gp_city_monthly t2 ON t1.order_month = t2.year_month
			AND LOWER(t1.city) = t2.polygon

		LEFT JOIN bi.temp_acq_city_monthly t3 ON t1.cohort = t3.year_month
			AND LOWER(t1.city) = t3.polygon

		LEFT JOIN bi.temp_cpa_city_monthly t4 ON t1.cohort = t4.year_month
			AND LOWER(t1.city) = t4.polygon

		WHERE LEFT(LOWER(t1.city),2) = 'de'

		GROUP BY
			t1.cohort,
			t1.order_month,
			t1.returning_month,
			LOWER(t1.city),
			t2.b2c_gp,
			t2.b2c_revenue,
			t2.b2c_gp_pcent,
			t3.nb_acquisitions,
			t3.nb_rebookings,
			t4.cpa

		ORDER BY polygon asc, 
			cohort desc, 
			order_month desc
		) as t

		ORDER BY polygon asc, 
			cohort desc, 
			order_month desc
;

-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
  RAISE NOTICE 'Error detected: transaction was rolled back.';
  RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;
$BODY$ LANGUAGE 'plpgsql'