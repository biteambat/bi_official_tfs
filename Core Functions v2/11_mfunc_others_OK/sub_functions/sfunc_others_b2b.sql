DELIMITER //

CREATE OR REPLACE FUNCTION bi.sfunc_others_b2b(crunchdate date) RETURNS void AS

$BODY$
DECLARE 

function_name varchar := 'bi.sfunc_others_b2b';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;

BEGIN


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.working_days_monthly;
CREATE TABLE bi.working_days_monthly as 

SELECT

	t3.year_month,
	t3.date,
	t3.weekday,
	-- t3.number_weekday,
	t3.day_type,
	t3.working_day_number,
	t3.non_working_day_number,
	CASE WHEN t3.day_type = 'Working Day'
	     THEN CONCAT(t3.day_type, ' - ', t3.working_day_number)
		  ELSE CONCAT(t3.day_type, ' - ', t3.non_working_day_number)
		  END as day_type_number,
	t3.number_working_days_in_month
	
FROM	
		
	(SELECT
	
		CASE WHEN t0.day_type = 'Working Day' THEN ROW_NUMBER() OVER 
		                 (PARTITION BY t0.year_month
		                  ORDER BY t0.day_type desc,
								         t0.date asc) ELSE 0 END as working_day_number,
		CASE WHEN t0.day_type = 'Non Working Day' THEN ROW_NUMBER() OVER 
		                 (PARTITION BY t0.year_month
		                  ORDER BY t0.day_type asc,
								         t0.date asc) ELSE 0 END as non_working_day_number,
		t0.year_month,
		t0.date,
		t0.weekday,
		t0.number_weekday,
		t0.day_type,
		t2.number_working_days_in_month
	
	FROM
		
		(SELECT 
		
			TO_CHAR(i, 'YYYY-MM') as year_month,
			i::date as date,
			TO_CHAR(i, 'day') as weekday,
			EXTRACT(DOW FROM i) as number_weekday,
			CASE WHEN EXTRACT(DOW FROM i) IN ('1', '2', '3', '4', '5') 
			          AND i NOT IN ('2018-01-01', '2018-03-30', '2018-04-02', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03', '2018-12-25', '2018-12-26',
							            '2019-01-01', '2019-04-19', '2019-04-22', '2019-05-01', '2019-05-30', '2019-06-10', '2019-10-03', '2019-12-25', '2019-12-26',
							            '2020-01-01', '2020-04-10', '2020-04-13', '2020-05-01', '2020-05-21', '2020-06-01', '2020-10-03', '2020-12-25', '2020-12-26',
							            '2021-01-01', '2021-04-02', '2021-04-05', '2021-05-01', '2021-05-13', '2021-05-24', '2021-10-03', '2021-12-25', '2021-12-26')
				  THEN 'Working Day' ELSE 'Non Working Day' END as day_type
			
		FROM 
		
			generate_series('2017-12-31', '2022-01-01', '1 day'::interval) i
			
		ORDER BY
		
			date desc) as t0
			
	LEFT JOIN
	
		(SELECT	
			
			year_month,
			SUM(CASE WHEN day_type = 'Working Day' THEN 1 ELSE 0 END) as number_working_days_in_month
		
		FROM
		
			
			(SELECT 
			
				TO_CHAR(i, 'YYYY-MM') as year_month,
				i::date as date,
				TO_CHAR(i, 'day') as weekday,
				EXTRACT(DOW FROM i) as number_weekday,
				CASE WHEN EXTRACT(DOW FROM i) IN ('1', '2', '3', '4', '5') 
				          AND i NOT IN ('2018-01-01', '2018-03-30', '2018-04-02', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03', '2018-12-25', '2018-12-26',
							               '2019-01-01', '2019-04-19', '2019-04-22', '2019-05-01', '2019-05-30', '2019-06-10', '2019-10-03', '2019-12-25', '2019-12-26',
							               '2020-01-01', '2020-04-10', '2020-04-13', '2020-05-01', '2020-05-21', '2020-06-01', '2020-10-03', '2020-12-25', '2020-12-26',
							               '2021-01-01', '2021-04-02', '2021-04-05', '2021-05-01', '2021-05-13', '2021-05-24', '2021-10-03', '2021-12-25', '2021-12-26')
												
				              
				
					  THEN 'Working Day' ELSE 'Non Working Day' END as day_type
				
			FROM 
			
				generate_series('2017-12-31', '2021-01-01', '1 day'::interval) i
				
			ORDER BY
			
				date desc) as t1
				
		GROUP BY
		
			year_month
			
		ORDER BY
		
			year_month desc) as t2
	
	ON
	
		t0.year_month = t2.year_month
		
	ORDER BY
	
		-- t0.day_type desc,
		t0.date desc) as t3;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 14_b2b_orders

	DROP TABLE IF EXISTS bi.b2borders;
	CREATE TABLE bi.b2borders as 
	SELECT
		t1.contact__c,
		t2.name,
		t2.sfid as opportunity_id,
		t2.delivery_area__c as polygon,
		t2.email__c,
		t2.locale__c,
		t2.contact_name__c,
		CASE 
 		WHEN CAST(EXTRACT(YEAR FROM effectivedate::date) as integer) = CAST(EXTRACT(YEAR FROM first_order_date::date) as integer) THEN CAST(EXTRACT(MONTH FROM effectivedate::date) as integer)- CAST(EXTRACT(MONTH FROM first_order_date::date) as integer) 
  		WHEN CAST(EXTRACT(YEAR FROM effectivedate::date) as integer) != CAST(EXTRACT(YEAR FROM first_order_date::date) as integer) THEN (CAST(EXTRACT(YEAR FROM effectivedate::date) as integer)-CAST(EXTRACT(YEAR FROM first_order_date::date) as integer))*12 + (CAST(EXTRACT(MONTH FROM effectivedate::date) as integer)- CAST(EXTRACT(MONTH FROM first_order_date::date) as integer) )
  		ELSE 0 END as returning_month,
		to_char(Effectivedate::date,'YYYY-MM') as Year_month,
		min(effectivedate::Date) as Date,
		SUM(t1.GMV__c)+MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as total_amount,
		SUM(t1.GMV__c) as Cleaning_Gross_Revenue,
		MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue,
		COUNT(1) as Total_Order_Count, 
		first_order_date,
		TO_CHAR(first_order_date,'YYYY-MM') as cohort,
		SUM(t1.Order_Duration__C) as Total_Invoiced_Hours,
		CASE WHEN SUM(Order_Duration__c) > 0 THEN (SUM(t1.GMV__c))/SUM(Order_Duration__c) ELSE NULL END as PPH,
		COUNT(1),
		SUM(CASE WHEN t3.company_name like '%BAT%' or t3.company_name like '%BOOK%' THEN Order_Duration__c ELSE 0 END) as bat_hour_share,
		SUM(Order_Duration__c) as total_hours,
		grand_total__c,
		CASE WHEN to_char(Effectivedate::date,'YYYY-MM') = to_char(first_order_date,'YYYY-MM') THEN 'New Customer' ELSE 'Existing Customer' End as customer_type,
		t2.closedate::date as sign_date,
		MAX(CASE WHEN to_char(Effectivedate::date,'YYYY-MM') = to_char(first_order_date,'YYYY-MM') THEN (32-EXTRACT(DAY FROM first_order_date))/31*grand_total__c ELSE grand_total__c END) as grand_total_calc
	FROM
		salesforce.order t1
	LEFT JOIN
		Salesforce.Opportunity t2
	ON
		(t2.sfid = t1.opportunityid)
	LEFT JOIN
		(SELECT
 		a.*,
 		t2.name as company_name,
 		t2.pph__c as sub_pph
 	FROM
   Salesforce.Account a
    JOIN
        Salesforce.Account t2
    ON
        (t2.sfid = a.parentid)

    WHERE a.status__c not in ('SUSPENDED')  and a.test__c = '0' and a.name not like '%test%') as t3
   	ON
   		(t1.professional__c = t3.sfid)
   	LEFT JOIN
   		(SELECT
   			opportunityid,
   			min(effectivedate::date) as first_order_Date
   		FROM
   			bi.orders
   		WHERE
   			status not like '%CANCELLED%'
   		GROUP BY
   			opportunityid) as t4
   	ON
   		(t1.opportunityid = t4.opportunityid)
   	
   	
	WHERE
		t1.type = 'cleaning-b2b'
		and t1.test__c = '0'
		and t1.status not like '%CANCELLED%'

	GROUP BY
		year_month,
		t2.delivery_area__c,
		t2.name,
		cohort,
		t2.email__c,
		t2.sfid,
		t2.locale__c,
		contact__c,
		contact_name__c,
		returning_month,
		first_order_date,
		grand_total__c,
		customer_type,
		sign_date;
	
DELETE FROM bi.b2b_pph_eff;	
	
INSERT INTO bi.b2b_pph_eff	
SELECT
	opportunity_id,
	year_month,
	grand_total__c,
	grand_total_calc,
	total_invoiced_hours,
	grand_total_calc/total_invoiced_hours as Eff_PPH
FROM
	bi.b2borders
WHERE
	year_month > '2017-02'
	and grand_total_calc > 0;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 18_b2brunrate

-- Signed Customers


DROP TABLE IF EXISTS bi.b2b_daily_kpis;
CREATE TABLE bi.b2b_daily_kpis as 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	
	locale;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale,
	sub_kpi;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	'Net New Customer' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	'Net New Hours on Platform' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-08-01'::date as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	'Total gross revenue' as sub_kpi,
	CAST('Actual' as text) as type,
	0;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel Inbound' ELSE 'Sales Inbound' END as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
	and acquisition_channel__c in ('inbound','web')
GROUP BY
	date,
	locale,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis 
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Customers' as text) as kpi,
	'New Customer churn' as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(sfid)) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;

-- DE Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'43' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'48' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'35' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer service' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'5' as value;

-- NL Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- CH Targets

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Net New Customer' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'15' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'17' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Inbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'3' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('Outbound' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'14' as value;
		
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Customers' as text) as kpi,
	CAST('New Customer churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2' as value;

-- New Customer End

-- New Hours Start

/*INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('New hours churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	left(locale,2) as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(customers) > 0 THEN SUM(hours)/SUM(customers) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		CASE WHEN SUM (plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a
GROUP BY
	locale,
	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(customers) > 0 THEN SUM(hours)/SUM(customers) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		COUNT(DISTINCT(sfid)) as customers,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(hours) > 0 THEN SUM(amount)/SUM(Hours) ELSE NULL END
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	'All' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(hours) > 0 THEN SUM(amount)/SUM(Hours) ELSE NULL END
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
		SUM(amount) as amount,
		CASE WHEN SUM(plan_pph__c) > 0 THEN SUM((amount-(CASE WHEN pps__c is null THEN 0 ELSE pps__c END))/plan_pph__c) ELSE NULL END as Hours
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date;
*/

-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'889' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'791' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'97' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('19' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'234' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'270' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'13' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('28' as decimal) as value;


-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'306' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('Net New hours on Platform' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'272' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
		CAST('New hours churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'34' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Hours/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('18' as decimal) as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Hours' as text) as kpi,
	CAST('AVG Revenue/hour' as text) as sub_kpi,
	CAST('Target' as text) as type,
	CAST('39' as decimal) as value;


-- New Hours END

-- New Revenue Start

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	date,
	locale;

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING')
GROUP BY
	date,
	locale;	

INSERT INTO bi.b2b_daily_kpis
SELECT
	closedate::date as date,
	left(locale__c,2) as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('DECLINED','TERMINATED')
GROUP BY
	date,
	locale;	


INSERT INTO bi.b2b_daily_kpis
SELECT
	date::date as date,
	locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Actual' as text) as type,
	CASE WHEN SUM(clients) > 0 THEN SUM(amount)/sum(clients) ELSE NULL END as value
FROM
	(SELECT
		EXTRACT(MONTH FROM closedate::date) as Month,
		EXTRACT(YEAR FROM closedate::date) as Year,
		min(closedate::date) as date,
		left(locale__c,2) as locale,
			SUM(CASE WHEN grand_total__c is null then amount ELSE grand_total__c END) as amount,
		COUNT(DISTINCT(sfid)) as clients
	FROM
	Salesforce.Opportunity
WHERE
	stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Month,
	year,
	locale) as a

GROUP BY
	date,
	locale;

-- DE Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'20446' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'18197' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'2249' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'de' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'426' as value;

-- NL Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'7481' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'6481' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'998' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'nl' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'499' as value;

-- CH Target

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue sold' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'11903' as value;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('Total gross revenue' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'10594' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('New cust revenue churn' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'1309' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-12-01' as date,
	'ch' as locale,
	CAST('Signed Revenue' as text) as kpi,
	CAST('AVG Revenue/cust' as text) as sub_kpi,
	CAST('Target' as text) as type,
	'700' as value;

-- marketing costs



INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-05-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-04-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-03-01','de','Marketing Stats','Cost Team','Actual','39393');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','de','Marketing Stats','Cost Team','Actual','38352');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','de','Marketing Stats','Cost Team','Actual','35883');




INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','ch','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','ch','Marketing Stats','Cost Team','Actual','0');


INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-01-01','nl','Marketing Stats','Cost Team','Actual','4149');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-02-01','nl','Marketing Stats','Cost Team','Actual','3567');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-03-01','nl','Marketing Stats','Cost Team','Actual','0');
INSERT INTO  bi.b2b_daily_kpis VALUES ('2018-04-01','nl','Marketing Stats','Cost Team','Actual','0');


-- INSERT INTO bi.b2b_daily_kpis
-- SELECT
-- 	mindate,
-- 	locale,
-- 	'Marketing Stats',
-- 	'Adwords Cost',	
	-- 'Actual',
-- 	costs
-- FROM

	-- (SELECT
	-- 	TO_CHAR(date,'YYYY-MM') as Month,
	-- 	locale,
	-- 	min(date) as mindate,
	-- 	SUM(sem_cost) as costs
	-- FROM
	-- 	bi.b2b_leadgencosts
	-- GROUP BY
	-- 	Month,
	-- 	locale) as a;


INSERT INTO bi.b2b_daily_kpis VALUES ('2018-01-01','de','Marketing Stats','Adwords Cost','Actual','20201');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-02-01','de','Marketing Stats','Adwords Cost','Actual','18805');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-03-01','de','Marketing Stats','Adwords Cost','Actual','9948');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-04-01','de','Marketing Stats','Adwords Cost','Actual','11350');
INSERT INTO bi.b2b_daily_kpis VALUES ('2018-05-01','de','Marketing Stats','Adwords Cost','Actual','12085');


INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-07-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*200*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and sub_kpi = 'Outbound'
	and left(locale,2) in ('de','nl')
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;


INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team',
	'Actual',
	sum(value)*40*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and left(locale,2) in ('de','nl')
	and sub_kpi = 'Inbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	'2017-01-01' as date,
	locale,
	'Marketing Stats',
	'Cost Team Inbound',
	'Actual',
	sum(value)*40*1.23 as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and type = 'Actual'
	and left(locale,2) in ('de','nl')
	and sub_kpi = 'Inbound'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date,'YYYY-MM')
GROUP BY
	locale;
	
	


INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t2.value) > 0 THEN SUM(t1.value)/SUM(t2.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date,
	t1.locale;
	
INSERT INTO  bi.b2b_daily_kpis
SELECT
	t1.date,
	'All' as locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t2.value) > 0 THEN SUM(t1.value)/SUM(t2.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	*
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team') as t1
LEFT JOIN
	
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	sum(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
GROUP BY
	year_month,
	locale) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
WHERE
	TO_CHAR(date,'YYYY-MM') < TO_CHAR(current_date,'YYYY-MM')

GROUP BY
	t1.date;


INSERT INTO  bi.b2b_daily_kpis
	SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'CPA',
	'Actual',
	CASE WHEN SUM(t1.value) > 0 THEN SUM(t2.value)/SUM(t1.value) ELSE NULL END as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	date,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	min(date) as date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team'
GROUP BY
	locale,
	year_month,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;
	
	
DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team' WHERE sub_kpi = 	'Cost Team Est'; 

INSERT INTO  bi.b2b_daily_kpis
SELECT
	t2.date,
	t1.locale,
	'Marketing Stats',
	'Cost Team Inbound Est',
	'Actual',
	SUM(t2.value) as value
FROM
(SELECT
	TO_CHAR(date,'YYYY-MM') as Year_Month,
	locale,
	SUM(value) as value
FROM
	bi.b2b_daily_kpis
WHERE
	kpi = 'Signed Customers'
	and sub_kpi = 'New Customer sold'
	and type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	year_month,
	locale) as t1
LEFT JOIN
(SELECT
	locale,
	TO_CHAR(date,'YYYY-MM') as year_month,
	min(date) as date,
	CASE WHEN (MAX(alldays)*MAX(current_days)) >0 THEN (CAST(SUM(Value) as decimal)/MAX(alldays)*MAX(current_days)) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and kpi = 'Marketing Stats'
	and sub_kpi = 'Cost Team Inbound'
GROUP BY
	locale,
	year_month,
	sub_kpi) as t2
ON
	(t1.year_month = t2.year_month and t1.locale = t2.locale)
GROUP BY
	t2.date,
	t1.locale;

DELETE FROM bi.b2b_daily_kpis WHERE kpi = 'Marketing Stats' and sub_kpi = 'Cost Team Inbound' and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM');

UPDATE bi.b2b_daily_kpis set sub_kpi = 'Cost Team Inbound' WHERE sub_kpi = 	'Cost Team Inbound Est'; 


-- New Revenue END

-- TOtal Customers START

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01'::date as date,
	LEFT(t1.locale__c,2) as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Actual' as text) as type,
	COUNT(DISTINCT(contact__c)) as unique_customer
FROM
	bi.orders t1
JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunityid = t2.sfid)
WHERE
	TO_CHAR(effectivedate::date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and (status not like '%CANCELLED%' and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL'))
	and order_type = '2'
GROUP BY
	Locale;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'185' as value;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'ch' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'27' as value;

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	'de' as locale,
	('Signed Customers') as kpi,
	('Total customers on the platform') as sub_kpi,
	CAST('Target' as text) as type,
	'33' as value;


-- Summary

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Runrate on Target% till now' as text) as type,
	CASE WHEN MAX(alldays) > 0 THEN 

		CASE WHEN (SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays)))*100 > 0 THEN
	
			round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/(SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END)*(CAST(MAX(current_days) as decimal)/MAX(alldays))))*100,0) ELSE 0 END 


	ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
	(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	

INSERT INTO bi.b2b_daily_kpis
SELECT
	'2016-11-01' as date,
	locale,
	kpi,
		sub_kpi,
	CAST('Runrate on Target%' as text) as type,
	CASE WHEN SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END) > 0 THEN round((SUM(CASE WHEN type = 'Actual' THEN value ELSe 0 END)/SUM(CASE WHEN type = 'Target' THEN Value ELSE 0 END))*100,0) ELSE 0 END as value
FROM
	bi.b2b_daily_kpis
WHERE
	TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Achieved' as varchar) as type,
	CASE WHEN MAX(current_days) > 0 THEN CAST(SUM(Value) as decimal)/MAX(current_days) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
	and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	date,
	locale,
	kpi,
	sub_kpi,
	CAST('Daily Target' as varchar) as type,
	CASE WHEN MAX(alldays) > 0 THEN CAST(SUM(Value) as decimal)/MAX(alldays) ELSE NULL END as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Target'
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	date,
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO	bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	locale,
	kpi,
	sub_kpi,
	CAST('Projected Achievement' as varchar) as type,
	(CASE WHEN MAX(current_days) > 0 THEN CAST(SUM(Value) as decimal)/MAX(current_days) ELSE NULL END)*(MAX(alldays)-MAX(current_days))+SUM(Value) as value
FROM
	bi.b2b_daily_kpis,
		(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
WHERE
	type = 'Actual'
	and TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM')
		and sub_kpi != 'Total customers on the platform'
GROUP BY
	locale,
	kpi,
	sub_kpi;
	
INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'Current Days' as type,
	MAX(current_days) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;
 	
 INSERT INTO bi.b2b_daily_kpis
SELECT
	current_date::date as date,
	'-' as locale,
	'Time' as kpi,
	'-' as sub_kpi,
	'All Days' as type,
	MAX(alldays) as value
FROM
			(SELECT SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') THEN 1 ELSE 0 END) as alldays,
		SUM(CASE WHEN TO_CHAR(date,'YYYY-MM') = TO_CHAR(current_date::date,'YYYY-MM') and EXTRACT(DOW FROM date) not in ('0','6') and date not in ('2016-10-03') and date <= current_date::date - interval '1 day' THEN 1 ELSE 0 END) as current_days
 FROM
	(select i::date as date from generate_series('2016-01-01', 
  '2020-12-12', '1 day'::interval) i) b) c
 GROUP BY
 	date;

INSERT INTO bi.b2b_daily_kpis
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Touched Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;

 INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Generated Inbound Leads') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(likelies) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	'All' as locale,
	('CVR Stats') as kpi,
	('CVR') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(CVR) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5)*100 ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'

GROUP BY
	year_month) as a
GROUP BY
	date;		
	
INSERT INTO bi.b2b_daily_kpis	
SELECT
	mindate::date as date,
	locale,
	('CVR Stats') as kpi,
	('Signed Customers') as sub_kpi,
	CAST('Actual' as text) as type,
	SUM(opportunities) as value
FROM(
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	MIN(t1.createddate::date) as mindate,
	left(t1.locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	
GROUP BY
	year_month,
	locale) as a
GROUP BY
	date,
	locale;	


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 53_b2b_kpis

DROP TABLE IF EXISTS bi.b2b_kpis;
CREATE TABLE bi.b2b_kpis AS

	SELECT -- INVOICED GMV

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced GMV'::text as kpi,
		SUM(o.gmv_eur)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- INVOICED HOURS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Invoiced hours'::text as kpi,
		SUM(o.order_duration__c)::numeric as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

/*
INSERT INTO bi.b2b_kpis -- MIN PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Min PPH'::text as kpi,
		ROUND(MIN(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;



INSERT INTO bi.b2b_kpis -- MAX PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Max PPH'::text as kpi,
		ROUND(MAX(o.pph__c)::numeric,1) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;*/

INSERT INTO bi.b2b_kpis -- AVERAGE PPH

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Average PPH'::text as kpi,
		CASE WHEN SUM(o.order_duration__c) > 0 THEN ROUND((SUM(o.gmv_eur)/SUM(o.order_duration__c))::numeric,1) ELSE NULL END as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis -- ACTIVE CLEANERS

	SELECT

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate) as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		o.polygon::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')

	GROUP BY EXTRACT(year from o.effectivedate),
			EXTRACT(month from o.effectivedate),
			EXTRACT(week from o.effectivedate),
			LEFT(o.locale__c,2),
			o.polygon

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;


INSERT INTO bi.b2b_kpis

	SELECT
		year::int as year,
	  	month::int as month,
	  	NULL::int as week,
	  	NULL::date as weekdate,
	  	locale::text as locale,
		city::text as city,
		'Supplies revenue'::text as kpi,
		SUM(Supply_revenue) as value
	FROM(
			SELECT
				 EXTRACT(YEAR from t1.Effectivedate) as year,
				 EXTRACT(MONTH from t1.Effectivedate) as month,
				 left(t1.locale__c,2) as locale,
				 t1.polygon as city,
					t2.sfid,
				 MAX(CASE WHEN t2.pps__c is null THEN 0 ELSE t2.pps__c END) as Supply_Revenue
			FROM
				 bi.orders t1
			LEFT JOIN
				 Salesforce.Opportunity t2
			ON
				 (t2.sfid = t1.opportunityid)
			WHERE
				 type = '222' 
				 AND EXTRACT(YEAR from t1.Effectivedate) <= EXTRACT(YEAR from current_date)
				 AND EXTRACT(MONTH from t1.Effectivedate) <= EXTRACT(MONTH from current_date)
				 AND status NOT LIKE ('%CANCELLED%')
				 AND (t1.effectivedate >= '2018-01-01')
			GROUP BY
				 left(t1.locale__c,2),
				 t1.polygon,
				 t2.sfid,
				 EXTRACT(YEAR from t1.Effectivedate),
				 EXTRACT(MONTH from t1.Effectivedate)) as a
	GROUP BY
		year,
		month,
		locale,
		city
	ORDER BY
		year,
		month,
		locale,
		city

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced GMV'::text as kpi,
		ROUND(SUM(o.gmv_eur)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced GMV'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Invoiced hours'::text as kpi,
		ROUND(SUM(o.order_duration__c)::numeric,2) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Invoiced hours'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

INSERT INTO bi.b2b_kpis

	SELECT -- INVOICED GMV DE TOTAL

		EXTRACT(year from o.effectivedate)::int as year,
		EXTRACT(month from o.effectivedate)::int as month,
		EXTRACT(week from o.effectivedate)::int as week,
		MIN(o.effectivedate)::date as weekdate,
		LEFT(o.locale__c,2)::text as locale,
		'DE Total'::text as city,
		'Active cleaners'::text as kpi,
		COUNT(DISTINCT o.professional__c) as value

	FROM bi.orders o

	WHERE o.test__c = '0'
			AND o.status in ('INVOICED','`NOSHOW CUSTOMER','NOSHOW PROFESSIONAL')
			AND o.order_type = '2'
			AND (o.effectivedate < current_date)
			AND (o.effectivedate >= '2018-01-01')
			AND LEFT(o.locale__c,2)::text = 'de'

	GROUP BY EXTRACT(year from o.effectivedate)::int,
			EXTRACT(month from o.effectivedate)::int,
			EXTRACT(week from o.effectivedate)::int,
			LEFT(o.locale__c,2),
			'DE Total'::text,
			'Active cleaners'::text

	ORDER BY year desc, month desc, week desc, locale asc, city asc

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------- 58_b2blikelies


DROP TABLE IF EXISTS bi.b2b_likelies;
CREATE TABLE bi.b2b_likelies AS

SELECT
		t.*,

		CASE 	


				WHEN LOWER(t.acquisition_channel_ref__c) = 'anygrowth' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'

				WHEN LOWER(t.acquisition_channel_ref__c) = 'reveal' and t.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'

				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
				
				WHEN (t.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
				
				WHEN (t.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'

				WHEN ((lower(t.acquisition_channel__c) = 'inbound') AND lower(t.acquisition_tracking_id__c) = 'classifieds')					
				THEN 'Classifieds'
							
				WHEN (t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text

		            AND t.createddate::date <= '2017-02-08'

		        THEN 'SEM Brand'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goob%')

		        	AND t.createddate::date > '2017-02-08'
		        	AND ((t.acquisition_channel_params__c LIKE '%goob%') AND (t.acquisition_channel_params__c LIKE '%bt:b2b%' OR t.acquisition_channel_params__c LIKE '%bt: b2b%' OR t.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
		        	)

		        THEN 'SEM Brand B2B'::text

		        WHEN (((t.acquisition_channel_params__c::text ~~ '%goob%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goob%')

		        	AND t.createddate::date > '2017-02-08'
					AND ((t.acquisition_channel_params__c LIKE '%goob%') AND (t.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%' ))
		        	
		        THEN 'SEM Brand B2C'::text
		        

		        WHEN (((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date <= '2017-02-08'
		        THEN 'SEM'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date > '2017-02-08'
		        	AND (((t.acquisition_channel_params__c LIKE '%goog%') AND (t.acquisition_channel_params__c LIKE '%bt: b2b%' OR t.acquisition_channel_params__c LIKE '%bt:b2b%' OR t.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
		        	))
		        	 OR
		            (t.acquisition_tracking_id__c LIKE 'unbounce_reinigungdirekt' AND t.createddate::date <= '2017-11-15')
		            OR
		            	acquisition_tracking_id__c LIKE 'rd%'

		            OR
		            	acquisition_channel_params__c LIKE '%ysm%'

		        THEN 'SEM B2B'::text

		        WHEN ((((t.acquisition_channel_params__c::text ~~ '%goog%'::text OR t.acquisition_channel_params__c::text ~~ '%ysm%'::text OR t.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text) or t.acquisition_tracking_id__c like '%goog%')

		        	AND t.createddate::date > '2017-02-08' 
		        	AND ((t.acquisition_channel_params__c LIKE '%goog%') AND (t.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (t.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%')))
		        	OR t.acquisition_channel_params__c::text LIKE '%goog%'
		        THEN 'SEM B2C'::text
		        

		        WHEN (t.acquisition_channel_params__c::text ~~ '%disp%'::text OR t.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
		            AND t.acquisition_channel_params__c::text !~~ '%facebook%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		        THEN 'Display'::text
		                    

		        WHEN t.acquisition_channel_params__c::text ~~ '%ytbe%'::text
		        THEN 'Youtube Paid'::text


		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text
		            AND LOWER(acquisition_tracking_id__c) LIKE '%b2b seo page form&'
		        THEN 'SEO B2B'::text
		        

		        WHEN ((t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_ref__c::text !~~ '%goob%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text
					AND LOWER(acquisition_tracking_id__c) NOT LIKE '%b2b seo page form&')

		        	OR
		        		((acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL AND (acquisition_tracking_id__c LIKE '%b2b%' OR acquisition_tracking_id__c LIKE '%tfs%')))

		        THEN 'SEO'::text
		        

		        WHEN (t.acquisition_channel_ref__c::text ~~ '%google%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%bing%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%naver%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%ask%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
		            AND t.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
		            AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		            AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		            THEN 'SEO Brand'::text
		            

		        WHEN (t.acquisition_channel_params__c::text ~~ '%batfb%'::text  
		                OR t.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%facebook%'::text 
		                OR t.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
		        THEN 'Facebook'::text


		        WHEN t.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%email%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%vero%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%batnl%'::text 
		                OR t.acquisition_channel_params__c::text ~~ '%fullname%'::text
		                OR t.acquisition_channel_params__c::text ~~ '%invoice%'::text 
		        THEN 'Newsletter'::text
		        

		        WHEN (t.acquisition_channel_params__c::text !~~ '%goog%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ysm%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%disp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%dsp%'::text
		                AND t.acquisition_channel_params__c::text !~~ '%batfb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%fb%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%clid%'::text 
		                AND t.acquisition_channel_params__c::text !~~ '%utm%'::text 
		                AND t.acquisition_channel_params__c::text <> ''::text)
		        	OR (acquisition_channel__c IS NULL AND acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL)
		        THEN 'DTI'::text

		        
        

        		
		        
		        -- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'Reinigungsdirekt'

		        ---------------------------------------------------------------------------------------------------------

		        -- WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'b2c home form'
		        -- WHEN acquisition_tracking_id__c like '%b2c home funnel%' THEN 'b2c home funnel'
		        -- WHEN acquisition_tracking_id__c like '%b2c funnel availability request%' THEN 'b2c funnel availability request'

		        -- WHEN acquisition_tracking_id__c like '%b2b funnel%' THEN 'b2b funnel'
		        -- WHEN acquisition_tracking_id__c like '%availability request%' THEN 'availability request'
		        -- WHEN acquisition_tracking_id__c like '%price page funnel%' THEN 'price page funnel'
		        -- WHEN acquisition_tracking_id__c like '%help page funnel%' THEN 'help page funnel'
		        -- WHEN acquisition_tracking_id__c like '%about page funnel%' THEN 'about page funnel'
		        -- WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'b2b home form'
		        -- WHEN acquisition_tracking_id__c like '%b2b home funnel%' THEN 'b2b home funnel'
		        -- WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'chatbot'
		        -- WHEN acquisition_tracking_id__c like '%b2b amp%' THEN 'b2b amp'
		        -- WHEN acquisition_tracking_id__c like '%footer property management cb%' THEN 'footer property management cb'
		        -- WHEN acquisition_tracking_id__c like '%footer newsletter b2b%' THEN 'footer newsletter b2b'
		        -- WHEN acquisition_tracking_id__c like '%price page form%' THEN 'price page form'
		        -- WHEN acquisition_tracking_id__c like '%help page form%' THEN 'help page form'
		        -- WHEN acquisition_tracking_id__c like '%about page form%' THEN 'about page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b contact page form%' THEN 'b2b contact page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b service page form%' THEN 'b2b service page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b about page form%' THEN 'b2b about page form'
		        -- WHEN acquisition_tracking_id__c like '%b2b floating cb%' THEN 'b2b floating cb'
		        -- WHEN acquisition_tracking_id__c like '%b2b floating newsletter%' THEN 'b2b floating newsletter'
		        -- WHEN acquisition_tracking_id__c like '%b2b seo page form%' THEN 'b2b seo page form'
		        -- WHEN acquisition_tracking_id__c like '%unbounce%' THEN 'unbounce'
		        -- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'unbounce_reinigungdirekt'
		        -- WHEN acquisition_tracking_id__c like '%unbound_tfs%' THEN 'unbound_tfs'

		        /*WHEN t.acquisition_channel_params__c::text ~~ '%coop%'
		            OR t.acquisition_channel_params__c::text ~~ '%afnt%'
		            OR t.acquisition_channel_params__c::text ~~ '%putzchecker%'
		        THEN 'Affiliate/Coops'*/
		        
		        ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as source_channel,

		CASE WHEN t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c NOT IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}','{"src":"step1"}') THEN 'B2B Homepage' -- THE FIRST PAGE THE LEAD ACTUALLY VISITED ON THE WEBSITE
				WHEN (t.acquisition_tracking_id__c IS NULL AND t.acquisition_channel_params__c IN ('{"ref":"b2c"}','{"ref":"b2c-home-banner"}')) OR t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Homepage'
				WHEN t.acquisition_channel_params__c IN ('{"src":"step1"}') THEN 'B2C Funnel'
				
				ELSE 'Unknown' END
		as landing_page,

		CASE WHEN t.acquisition_tracking_id__c = 'appbooking' THEN 'B2C Funnel' -- WHERE THE LEAD TYPED-IN ITS INFORMATON
				WHEN t.acquisition_tracking_id__c IS NULL THEN 'B2B Funnel'
				ELSE 'Unknown' END
		as conversion_page,
		CASE
		WHEN acquisition_tracking_id__c like '%b2b funnel%' and direct_relation__c = TRUE THEN 'B2B Funnel Direct'
		WHEN acquisition_tracking_id__c like '%b2b funnel%' and direct_relation__c = FALSE THEN 'B2B Funnel Partner'
		WHEN acquisition_tracking_id__c like '%b2b home cb%' THEN 'B2B Home CB'
		WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'B2B Home Form'
		WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'B2C Home Form'
		WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'Chatbot'
		-- WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'Reinigungdirekt'
		-- WHEN acquisition_tracking_id__c like '%unbounce%' and acquisition_tracking_id__c not like '%unbounce_reinigungdirekt%'  THEN 'Unbounce'
		-- WHEN acquisition_tracking_id__c like '%b2c home form%' THEN 'b2c home form'

		-------------------------------------------------------------------------------------------------------------------  

        WHEN acquisition_tracking_id__c like '%b2c home funnel%' THEN 'b2c home funnel'
        WHEN acquisition_tracking_id__c like '%b2c funnel availability request%' THEN 'b2c funnel availability request'

        -- WHEN acquisition_tracking_id__c like '%b2b funnel%' THEN 'b2b funnel'
        WHEN acquisition_tracking_id__c like '%availability request%' THEN 'availability request'
        WHEN acquisition_tracking_id__c like '%price page funnel%' THEN 'price page funnel'
        WHEN acquisition_tracking_id__c like '%help page funnel%' THEN 'help page funnel'
        WHEN acquisition_tracking_id__c like '%about page funnel%' THEN 'about page funnel'
        -- WHEN acquisition_tracking_id__c like '%b2b home form%' THEN 'b2b home form'
        WHEN acquisition_tracking_id__c like '%b2b home funnel%' THEN 'b2b home funnel'
        WHEN acquisition_tracking_id__c like '%chatbot%' THEN 'chatbot'
        WHEN acquisition_tracking_id__c like '%b2b amp%' THEN 'b2b amp'
        WHEN acquisition_tracking_id__c like '%footer property management cb%' THEN 'footer property management cb'
        WHEN acquisition_tracking_id__c like '%footer newsletter b2b%' THEN 'footer newsletter b2b'
        WHEN acquisition_tracking_id__c like '%price page form%' THEN 'price page form'
        WHEN acquisition_tracking_id__c like '%help page form%' THEN 'help page form'
        WHEN acquisition_tracking_id__c like '%about page form%' THEN 'about page form'
        WHEN acquisition_tracking_id__c like '%b2b contact page form%' THEN 'b2b contact page form'
        WHEN acquisition_tracking_id__c like '%b2b service page form%' THEN 'b2b service page form'
        WHEN acquisition_tracking_id__c like '%b2b about page form%' THEN 'b2b about page form'
        WHEN acquisition_tracking_id__c like '%b2b floating cb%' THEN 'b2b floating cb'
        WHEN acquisition_tracking_id__c like '%b2b floating newsletter%' THEN 'b2b floating newsletter'
        WHEN acquisition_tracking_id__c like '%b2b seo page form%' THEN 'b2b seo page form'
        WHEN acquisition_tracking_id__c like 'unbounce' THEN 'unbounce'
        WHEN acquisition_tracking_id__c like '%unbounce_reinigungdirekt%' THEN 'unbounce reinigungdirekt'
        WHEN acquisition_tracking_id__c like '%unbounce_tfs%' THEN 'unbounce tfs'


        WHEN acquisition_tracking_id__c like '%price_page_funnel_b2b%' THEN 'price page funnel b2b'
        WHEN acquisition_tracking_id__c like '%quality_page_funnel_b2b%' THEN 'quality page funnel b2b'
        WHEN acquisition_tracking_id__c like '%quality_page_funnel_b2c%' THEN 'quality page funnel b2c'
        WHEN acquisition_tracking_id__c like '%how_it_works_page_funnel_b2b%' THEN 'how it works page funnel b2b'
        WHEN acquisition_tracking_id__c like '%how_it_works_page_funnel_b2c%' THEN 'how it works page funnel b2c'
        WHEN acquisition_tracking_id__c like '%about_us_page_funnel_b2b%' THEN 'about us page funnel b2b'
        WHEN acquisition_tracking_id__c like '%about_us_page_funnel_b2c%' THEN 'about us page funnel b2c'
        WHEN acquisition_tracking_id__c like '%about_us_page%' THEN 'about us page'

		ELSE 'Other' END as lead_source


	FROM
	Salesforce.likeli__c t
	WHERE
	createddate::date > '2017-01-01'
	and type__c = 'B2B'
	-- and t.test__c IS FALSE
	AND acquisition_channel__c NOT LIKE 'outbound'
	AND company_name__c NOT LIKE '%test%'
	AND company_name__c NOT LIKE '%bookatiger%'
	AND email__c NOT LIKE '%bookatiger%'
	AND ((t.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR t.lost_reason__c IS NULL)
	-- AND o.acquisition_channel__c IN ('inbound', 'web')
	-- AND LEFT(o.locale__c, 2) = 'de'
	AND t.test__c IS FALSE
	AND t.name NOT LIKE '%test%';


DROP TABLE IF EXISTS bi.b2b_likelie_cvr;
CREATE Table bi.b2b_likelie_cvr as 
SELECT
	TO_CHAR(t1.createddate::date,'YYYY-MM') as Year_Month,
	left(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	min(t1.createddate::date) as date,
	conversion_page,
	lead_source,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	COUNT(DISTINCT(CASE WHEN t2.stagename IN ('LOST') THEN t2.sfid ELSE NULL END)) as lost,
	COUNT(DISTINCT(CASE WHEN t2.stagename IN ('OFFER SENT', 'VERBAL CONFIRMATION') THEN t2.sfid ELSE NULL END)) as offer_sent_verbal_conf,
	COUNT(DISTINCT(CASE WHEN t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING') THEN t2.sfid ELSE NULL END)) as signed_opps,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIn
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c NOT IN ('outbound')
	AND t1.test__c IS FALSE
	AND t1.company_name__c NOT LIKE '%test%'
	AND t1.company_name__c NOT LIKE '%bookatiger%'
	AND (t1.lost_reason__c NOT IN ('invalid - sem duplicate') OR t1.lost_reason__c IS NULL)
	AND LEFT(t1.locale__c, 2) = 'de'
	AND t1.createddate >= '2018-01-01'

	-- AND t2.test__c IS FALSE
	-- and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	year_month,
	locale,
	lead_source,
	Source_Channel,
	landing_page,
	conversion_page;

	
DROP TABLE IF EXISTS bi.b2b_likelie_cvr_weekly;
CREATE TABLE bi.b2b_likelie_cvr_weekly as 
SELECT
	EXTRACT(YEAR FROM t1.createddate) as yearnum,
	EXTRACT(WEEK FROM t1.createddate) as weeknum,
	LEFT(t1.locale__c,2) as locale,
	Source_Channel,
	landing_page,
	min(t1.createddate::date) as date,
	conversion_page,
	COUNT(DISTINCT(t1.sfid)) as likelies,
	COUNT(DISTINCT(t2.sfid)) as opportunities,
	CASE WHEN COUNT(DISTINCT(t1.sfid)) > 0 THEN round(CAST(COUNT(DISTINCT(t2.sfid)) as decimal)/COUNT(DISTINCT(t1.sfid)),5) ELSE NULL END as CVR
FROM
	bi.b2b_likelies t1
LEFT JOIN
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid and t2.stagename in ('DECLINED','IRREGULAR','RUNNING','SIGNED','TERMINATED','WON','PENDING'))
WHERE
	t1.type__c = 'B2B'
	and t1.acquisition_channel__c in ('inbound','web')
	and t2.ownerid != '00520000003IiNCAA0'
GROUP BY
	EXTRACT(YEAR FROM t1.createddate),
	EXTRACT(WEEK FROM t1.createddate),
	LEFT(t1.locale__c,2),
	Source_Channel,
	landing_page,
	conversion_page

ORDER BY 
	yearnum desc,
	weeknum desc,
	locale asc

;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

		
DROP TABLE IF EXISTS bi.b2b_monthly_kpis;
CREATE TABLE bi.b2b_monthly_kpis as 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(t2.sfid)) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid and LOWER(t2.name) not like '%test%')
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL','WAITING FOR RESCHEDULE')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
		
INSERT INTO bi.b2b_monthly_kpis 	
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as revenue
FROM
	bi.b2borders 
GROUP BY
	year_month,
	locale,
	city;
	
	

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total EOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(ORder_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New (Signed Before)' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	TO_CHAR(sign_date,'YYYY-MM') < Year_MOnth
	and customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;
	
INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;

INSERT INTO bi.b2b_monthly_kpis 
SELECT
	year_month,
	min(date) as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New (Signed same month)' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	'None' as customer_type,
	COUNT(DISTINCT(opportunity_id)) as value
FROM
	bi.b2borders
WHERE
	TO_CHAR(sign_date,'YYYY-MM') = Year_MOnth
	and customer_type = 'New Customer'
GROUP BY
	year_month,
	locale,
	city;	
	
INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date,'YYYY-MM') as Year_Month,
	min(effectivedate::date) as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(CASE WHEN TO_CHAR(Effectivedate::date,'YYYY-MM') = TO_CHAR(first_order::date,'YYYY-MM') THEN Order_Duration__c ELSE NULL END) as value
	
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
LEFT JOIN
(SELECT
	t2.sfid,
	min(effectivedate::date) as first_order
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	t2.sfid) as t3
ON
	(t2.sfid = t3.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;	

INSERT INTO bi.b2b_monthly_kpis	
SELECT
	year_month,
	min(date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('New' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN TO_CHAR(first_date::date,'YYYY-MM') = TO_CHAR(date::date,'YYYY-MM') THEN (CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) ELSE 0 END) as revenue

FROM
	bi.b2borders a
LEFT JOIN
	(SELECT
		name,
		min(date) as first_date
	FROM
		bi.b2borders	
	GROUP BY
		name) as b
ON
	(a.name = b.name)
GROUP BY
	year_month,
	locale,
	city;

------------------------------------------------------------------------------------------------------- Revenue Churned Opps 27/03/2018

INSERT INTO bi.b2b_monthly_kpis
SELECT

	to_char(churn_date,'YYYY-MM') as year_month,
	min(churn_date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CAST('-' as varchar) as city,
	CAST('Churn' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'B2B' as customer_type,
	SUM(total) as revenue

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		t3.potential as total
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND (o.effectivedate >= '2018-01-01')
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		-- EXTRACT(WEEK FROM oo.createddate) = 6
		-- AND EXTRACT(year FROM oo.createddate) = 2018
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		-- AND oo.name NOT LIKE '%test%'
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid

	LEFT JOIN

		bi.potential_revenue_per_opp t3

	ON

		t1.opportunityid = t3.opportunityid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t3.potential
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_month,
	locale,
	city
	
ORDER BY

	year_month desc;
		
------------------------------------------------------------------------------------------------------- Revenue Churned Opps 27/03/2018

INSERT INTO bi.b2b_monthly_kpis
SELECT

	to_char(churn_date,'YYYY-MM') as year_month,
	min(churn_date::date)as mindate,
	LEFT(locale__c,2) as locale,
	CAST('-' as varchar) as city,
	CAST('Churned Opps' as varchar) as kpi,
	CAST('Customer' as varchar) as type,
	COUNT(DISTINCT t3.opportunityid) as customers
	

FROM

	
	(SELECT
		
		MIN(t1.createddate) as churn_date,
		t2.locale__c,
		t1.opportunityid,
		t3.potential as total
	
	FROM
	
	(SELECT
	
		oo.createddate,
		oo.opportunityid
	
	FROM
	
		salesforce.opportunityfieldhistory oo
			
	LEFT JOIN
	
		(-- customers with valid order in the past
		SELECT
		
			DISTINCT o.contact__c
			
		FROM
		
			bi.orders o
			
		WHERE
		
			o.type = 'cleaning-b2b'
			AND o.test__c IS FALSE
			AND o.effectivedate < current_date
			AND (o.effectivedate >= '2018-01-01')
			AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')) t1
		
	ON
	
		t1.contact__c = oo.opportunityid 
		
	WHERE
	
		-- EXTRACT(WEEK FROM oo.createddate) = 6
		-- AND EXTRACT(year FROM oo.createddate) = 2018
		oo.newvalue IN ('RESIGNED', 'CANCELLED')
		-- AND oo.name NOT LIKE '%test%'
		) t1
		
	LEFT JOIN
	
		salesforce.opportunity t2
		
	ON 
	
		t1.opportunityid = t2.sfid

	LEFT JOIN

		bi.potential_revenue_per_opp t3

	ON

		t1.opportunityid = t3.opportunityid
		
	WHERE
	
		t2.status__c NOT IN ('REVIEW', 'ONBOARDED', 'RUNNING', 'RENEGOTIATION', 'SIGNED', 'RETENTION')
		
	GROUP BY
		
		t1.opportunityid,
		t2.locale__c,
		t3.potential
		
	ORDER BY
		
		t1.opportunityid) as t3
		
GROUP BY

	year_month,
	locale,
	city
	
ORDER BY

	year_month desc;
		

-------------------------------------------------------------------------------------------------------

INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Customers' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	COUNT(DISTINCT(t2.sfid)) as sfid
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;

INSERT INTO bi.b2b_monthly_kpis 	
SELECT
	TO_CHAR(date::date + Interval '1 Month','YYYY-MM') as Year_Months,
	min(date::date + Interval '1 Month')::date as mindate,
	left(locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Revenue' as varchar) as type,
	'None' as customer_type,
	SUM(CASE WHEN grand_total__c > 0 THEN grand_total_calc else cleaning_gross_revenue+supply_revenue END) as revenue
FROM
	bi.b2borders 
GROUP BY
	year_months,
	locale,
	city;


INSERT INTO bi.b2b_monthly_kpis
SELECT
	TO_CHAR(Effectivedate::date + Interval '1 Month','YYYY-MM') as Year_Month,
	min(effectivedate::date + Interval '1 Month') as mindate,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN polygon is null THEN 'Other' ELSE polygon END as city,
	CAST('Total BOM' as varchar) as kpi,
	CAST('Hours' as varchar) as type,
	CASE WHEN stagename = 'IRREGULAR' THEN 'One-Off' ELSE 'Recurrent' END as customer_type,
	SUM(Order_Duration__c) as value
FROM
	bi.orders t1
LEFT JOIN 
	Salesforce.Opportunity t2
ON
	(t1.Opportunityid = t2.sfid)
WHERE
	(t1.status in ('INVOICED') or status not like '%CANCELLED%') and status not in ('NOSHOW CUSTOMER','NOSHOW PROFESSIONAL','ERROR INTERNAL')
	and t1.order_type = '2'
	AND (t1.effectivedate >= '2018-01-01')
GROUP BY
	Year_month,
	locale,
	polygon,
	customer_type;
	
DROP TABLE IF EXISTS bi.closemonth_stats;
CREATE TABLE bi.closemonth_stats as 
SELECT
	Signing_Month,
	start_month,
	locale,
	min(sign_date) as min_sign_date,
	((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) as Year_Diff,
	CASE WHEN stagename in ('IRREGULAR','RUNNING','SIGNED','WON','PENDING') and status__c in ('RESIGNED','CANCELLED') THEN 'LEFT' ELSE 'PENDING' END as status,
	SUM(unique_customers) as Unique_Customers
FROM(
SELECt
	to_char(closedate::date,'YYYY-MM') as Signing_Month,
	TO_CHAR(Order_Start::date,'YYYY-MM') as Start_Month,
	min(closedate::date) as Sign_Date,
	min(order_Start::Date) as Start_date,
	stagename,
	status__c,
	LEFT(Locale__c,2) as locale,
	COUNT(DISTINCT(t1.sfid)) as unique_customers
FROM
	Salesforce.Opportunity t1
LEFT JOIN
	(SELECT
		t3.opportunityid,
		min(effectivedate::date) as Order_Start
	FROM
		bi.orders t3
	WHERE
		t3.status not like '%CANCELLED%'
		AND (t3.effectivedate >= '2018-01-01')
	GROUP BY
		t3.opportunityid) as t2
ON
	(t1.sfid = t2.opportunityid)
WHERE
	t1.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING')
GROUP BY
	Signing_Month,
	Start_Month,
	status__c,
	locale,
	stagename) as a
GROUP BY
	Signing_Month,
	Start_Month,
	status,
	locale,
	Year_Diff
HAVING
	((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) > -1 or ((CAST(LEFT(start_Month,4) as integer)-CAST(LEFT(Signing_Month,4) as integer))*12)+(CAST(RIGHT(start_Month,2) as integer)-CAST(RIGHT(Signing_Month,2) as integer)) is null;

DROP VIEW IF EXISTS bi.history_view;

DROP TABLE IF EXISTS bi.leadhistory_touched_date;
CREATE TABLE bi.leadhistory_touched_date as 
SELECT
	parentid,
	min(createddate::date) as createddate
FROM
	salesforce.likeli__history t1
WHERE
	field = 'Owner'
	and createddate::date > '2017-02-01'
	and oldvalue in ('API Tiger','MC Tiger','IT Tiger','Alejandra Chavez','Renan Ribeiro','Austin Marcus')
	and newvalue not in ('Alejandra Chavez','Renan Ribeiro','Austin Marcus','API Tiger','MC TIGER','IT TIGER')
GROUP BY
	parentid;
	
DROP TABLE IF EXISTS bi.likeli_to_opp_conversion;
CREATE TABLE bi.likeli_to_opp_conversion as
SELECT
	t2.sfid as opp_id,
	t1.acquisition_tracking_id__c,
	LEFT(t1.locale__c,2) as locale,
	CASE WHEN t1.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t1.createddate::date as lead_created,
	t2.createddate::date as opp_created,
	t2.closedate::date as signed_date,
	extract(day from age(t2.closedate::date,t2.createddate::date)) as time_to_convert,
	t4.potential as Monthly_Amount,
	CASE WHEN t2.stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','WON','PENDING') THEN 1 ELSE 0 END as Conversion_Signed,
	CASE WHEN t2.stagename not in  ('NEGOTIATION','NEW','OFFER SENT','POSITIVE FEEDBACK','PENDING','WON') THEN 1 ELSE 0 END as Conversion_Finished,
	CASE WHEN t3.hours > 0 THEN 1 ELSE 0 END as Conversion_Started
FROM
	salesforce.likeli__c t1
JOIN 
	Salesforce.opportunity t2
ON
	(t1.opportunity__c = t2.sfid)

LEFT JOIN
	bi.potential_revenue_per_opp t4

ON

	t1.opportunity__c = t4.opportunityid

LEFT JOIN
	(SELECT
		t3.opportunity_id,
		sum(total_hours) as hours
	FROM
		bi.b2borders t3
	GROUP BY
		opportunity_id) as t3
ON
	(t2.sfid = t3.opportunity_id);

DROP VIEW IF EXISTS bi.history_view;
CREATE VIEW bi.history_view as 
SELECT
	parentid,
	locale,
	acquisition_tracking_id__c,
	lead_source,
	lead_created,
	times_recycled,
	first_touch_date,
	1 as new_cvr,
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null or not_reached_date is not null or ended_date is not null THEN 1 ELSE 0 END) as touched_cvr,	
	MAX(CASE WHEN qualified_date is not null or reached_date is not null or opp_date is not null or validated_date is not null or dm_reached_date is not null THEN 1 ELSE 0 END) as reached_cvr,
	MAX(CASE WHEN opp_date is not null or qualified_date is not null THEN 1 ELSE 0 END) as qualified_cvr,
	MAX(CASE WHEN opp_date is not null  THEN 1 ELSE 0 END) as opp_cvr,
	MAX(CASE WHEN ended_date is not null THEN 1 ELSE 0 END) as ended_cvr,
	MAX(CASE WHEN not_reached_date is not null THEN 1 ELSE 0 END) as not_reached_cvr,
	MAX(CASE WHEN dm_reached_date is not null THEN 1 ELSE 0 END) as dm_reached_cvr,
	MAX(CASE WHEN signed_date is not null THEN 1 ELSE 0 END) as signed_cvr
FROM(
SELECT
	t1.parentid,
	t2.acquisition_tracking_id__c,
	LEFT(t2.locale__c,2) as locale,
	CASE WHEN t2.acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' end as lead_source,
	t2.createddate::date as lead_created,
	t3.createddate::date as first_touch_date,
	CASE WHEN recycle__c is null THEN 0 ELSE recycle__c END as times_recycled,
	MIN(CASE WHEN newvalue = 'NEW' and field  = 'stage__c' THEN t1.createddate::date else null END) as new_date,
	MIN(CASE WHEN newvalue = 'VALIDATED' and field  = 'stage__c' THEN t1.createddate::date else null END) as validated_date,
	MIN(CASE WHEN newvalue = 'NOT REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as not_reached_date,
	MIN(CASE WHEN newvalue = 'REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as reached_date,
	MIN(CASE WHEN newvalue = 'DM REACHED' and field  = 'stage__c' THEN t1.createddate::date else null END) as dm_reached_date,
	MIN(CASE WHEN newvalue = 'QUALIFIED' and field  = 'stage__c' THEN t1.createddate::date else null END) as qualified_date,
	MIN(CASE WHEN newvalue = 'ENDED' and field  = 'stage__c' THEN t1.createddate::date else null END) as ended_date,
	MIN(CASE WHEN newvalue != ''  and field = 'opportunity__c' THEN t1.createddate::date else null end) as opp_date, 
	MIN(CASE WHEN stagename in ('IRREGULAR','DECLINED','RUNNING','SIGNED','TERMINATED','PENDING','WON') THEN closedate::date ELSE null END) as signed_date
FROM
	salesforce.likeli__history t1
JOIN
	salesforce.likeli__c t2
ON
	(t1.parentid = t2.sfid)
LEFT JOIN
	bi.leadhistory_touched_date t3
ON
	(t1.parentid = t3.parentid)
LEFT JOIn
	Salesforce.opportunity t4
ON
	(t2.opportunity__c = t4.sfid)
WHERE
	field  in ('stage__c','opportunity__c')
GROUP BY
	t1.parentid,
	t2.acquisition_tracking_id__c,
	t2.createddate::date,
	lead_source,
	locale,
	first_touch_date,
	times_recycled) as a
GROUP BY
	parentid,
	lead_source,
	first_touch_date,
	locale,
	acquisition_tracking_id__c,
	lead_created,
	times_recycled;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.opportunity_churn;
CREATE TABLE bi.opportunity_churn as 
	
SELECT

	-- CASE WHEN t4.monthly_revenue_loss > 700 THEN '> 700€'
		  -- WHEN (t4.monthly_revenue_loss > 250 AND t4.monthly_revenue_loss < 700) THEN '250€ - 700€'
		  -- WHEN t4.monthly_revenue_loss < 250 THEN '> 250€'
		  -- ELSE NULL
		  -- END as cluster,
	t4.*

FROM	
	
	
	(SELECT
	
		MIN(t1.date_churn) as date_churn,
		t1.year,
		t1.month,
		t1.opportunityid,
		t1.name,
		t1.locale,
		t2.polygon,
		-- t1.stagename,
		t1.churn_reason,
		t2.hours as monthly_hours_loss,
		grand_total__c,
		t2.monthly_revenue as monthly_revenue_loss,
		t1.acquisition_channel__c as acquisition_channel
		
	FROM
	
		(SELECT

			opphistory.*,
			op.name,
			left(op.locale__c,2) as locale,
			op.churn_reason__c as churn_reason,
			op.grand_total__c,
			op.acquisition_channel__c
		
		FROM
		
			(SELECT
				
				EXTRACT(YEAR FROM o.createddate) as year,
				EXTRACT(MONTH FROM o.createddate) as month,	
				MAX(o.createddate::date) as date_churn,
				o.opportunityid
				-- o.stagename
			
			FROM
			
				salesforce.opportunityfieldhistory o
				
			WHERE
			
				o.newvalue IN ('DECLINED', 'TERMINATED', 'RESIGNED', 'CANCELLED')
				
			GROUP BY
			
				o.opportunityid,
				-- o.stagename,
				year,
				month
				
			ORDER BY
				
				year desc,
				month desc,
				o.opportunityid) as opphistory
			
		LEFT JOIN 
		
			salesforce.opportunity op
			
		ON 
		
			opphistory.opportunityid = op.sfid) as t1
			
	LEFT JOIN
		
		(SELECT

			t3.opportunityid,
			t3.polygon,
			MAX(t3.hours) as hours,
			MAX(t3.monthly_revenue) as monthly_revenue
		
		FROM
		
			(SELECT
		
				EXTRACT(YEAR FROM o.effectivedate) as year,
				EXTRACT(MONTH FROM o.effectivedate) as month,
				o.opportunityid,
				o.polygon,
				SUM(CASE WHEN o.eff_pph IS NULL THEN o.pph__c*o.order_duration__c ELSE o.eff_pph*o.order_duration__c END) as monthly_revenue,
				SUM(o.order_duration__c) as hours
							
			FROM
			
				bi.orders o
				
			WHERE
			
				o.order_type = 2
				AND (o.effectivedate >= '2018-01-01')
								
			GROUP BY
			
				year,
				month,
				o.opportunityid,
				o.polygon) as t3
		
		GROUP BY 
		
			t3.opportunityid,
			t3.polygon
			) as t2
				
	ON 
	
		t1.opportunityid = t2.opportunityid
		
	GROUP BY
	
		t1.year,
		t1.month,
		t1.opportunityid,
		t1.name,
		t1.locale,
		t2.polygon,
		-- t1.stagename,
		t1.churn_reason,
		monthly_hours_loss,
		grand_total__c,
		t2.monthly_revenue,
		t1.acquisition_channel__c
	
	ORDER BY
	
		year desc,
		month desc) as t4;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.forecast_sales;
CREATE TABLE bi.forecast_sales AS


SELECT

	year,
	month,
	country,
	channel,
	SUM(t3.number_opportunities) as actual_opportunities,
	SUM(t3.forecast_opportunities) as forecast_opps,
	CASE WHEN (country = 'DE' AND channel = 'INBOUND') THEN 50 
		  WHEN (country = 'DE' AND channel = 'OUTBOUND') THEN 20
		  ELSE (CASE WHEN (country = 'CH' AND channel = 'INBOUND')  THEN 4 
		             WHEN (country = 'CH' AND channel = 'OUTBOUND')  THEN 4
		  				 ELSE (CASE WHEN (country = 'NL' AND channel = 'INBOUND') THEN 7 
						            WHEN (country = 'NL' AND channel = 'OUTBOUND') THEN 7
						            ELSE NULL END)
						 END)
		  END as target,
	CASE WHEN (country = 'DE' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/50
	     WHEN (country = 'DE' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/20 
		  ELSE (CASE WHEN (country = 'CH' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/4
	     				 WHEN (country = 'CH' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/4
		  				ELSE (CASE WHEN (country = 'NL' AND channel = 'INBOUND') THEN SUM(t3.forecast_opportunities)/7
	     				 			  WHEN (country = 'NL' AND channel = 'OUTBOUND') THEN SUM(t3.forecast_opportunities)/7
	     				 			  ELSE NULL
	     				 			  END)
						END)
		  END as runrate

FROM

	(SELECT
	
		UPPER(LEFT(locale__c, 2)) as country,
		EXTRACT(YEAR FROM t2.closedate) as year,
		EXTRACT(MONTH FROM t2.closedate) as month,
		EXTRACT(DAY FROM t2.closedate) as day,
		DATE_PART('days', DATE_TRUNC('month', t2.closedate) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_in_month,
		CASE WHEN t2.acquisition_channel__c IN ('web', 'inbound') THEN 'INBOUND' ELSE 'OUTBOUND' END as channel,
		SUM(CASE WHEN t2.stagename IN ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING') THEN 1 ELSE 0 END) as number_opportunities,
		ROUND((SUM(CASE WHEN t2.stagename IN ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING') THEN 1 ELSE 0 END)/EXTRACT(DAY FROM current_date))*DATE_PART('days', DATE_TRUNC('month', t2.closedate) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL)) as forecast_opportunities
	
	FROM
	
		(SELECT

			*
			
		FROM
		
			salesforce.opportunity
			
		WHERE
		
			stagename in ('SIGNED', 'DECLINED', 'RUNNING', 'TERMINATED', 'IRREGULAR','WON','PENDING')
			-- AND (closedate::date BETWEEN '2017-06-01' AND '2017-06-30')
			) as t2
	
	GROUP BY
	
		country,
		year,
		month,
		day,
		number_days_in_month,
		channel
		
	ORDER BY
	
		year,
		month) as t3
		
WHERE

	year = EXTRACT(YEAR FROM current_date)
	AND month =EXTRACT(MONTH FROM current_date)
		
GROUP BY

	country,
	year,
	month,
	channel;

-- B2B Sales Dashboard

DROP TABLE IF EXISTS bi.salesreport_daily;
CREATE TABLE bi.salesreport_daily as 

-- Signed Deals - Actual
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Deals' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	ROUND(COUNT(*),2) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
	
-- Signed Revenue - Actual	
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Revenue' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	SUM(grand_total__c) as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;
	
-- Signed Hours - Actual		
	
INSERT INTO  bi.salesreport_daily
SELECT
	LEFT(locale__c,2) as locale,
	TO_CHAR(closedate::date,'YYYY-MM') as Year_Month,
	CAST('Actual' as Varchar) as sub_kpi,
	CAST('Signed Hours' as Varchar) as kpi,
	CASE WHEN grand_total__c > 1100 THEN 'KA' ELSE 'SME' END as customer_type,
	CASE WHEN direct_relation__c = 'TRUE' THEN 'Funnel' ELSE 'Sales' END as channel,
	CASE WHEN acquisition_channel__c in ('inbound','web') THEN 'Inbound' ELSE 'Outbound' END as acquisition_type,
	
	CASE WHEN LEFT(locale__c,2) ='de' THEN SUM(grand_total__c)/25.9 
	WHEN LEFT(locale__c,2) ='nl' THEN SUM(grand_total__c)/27.9 
	ELSE 0 END as value
FROM
	Salesforce.Opportunity
WHERE
	stagename in ('PENDING','WON')
	and test__c = '0'
GROUP BY
	locale,
	year_month,
	customer_type,
	channel,
	acquisition_type;	



-- Signed Revenue - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
sub1.value*sub2.value AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Deals')
	AND sub2.kpi in ('Average Revenue')
		AND sub1.sub_kpi in ('Target');	
	
-- Signed Hours - Target

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Signed Hours' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Average PPH')
	AND sub1.sub_kpi in ('Target');
	
-- Average PPH - Actual

INSERT INTO  bi.salesreport_daily
SELECT
sub1.locale AS locale,
sub1."year_month" AS year_month, 
sub1.sub_kpi AS sub_kpi,
CAST('Average Revenue' as Varchar) as kpi,
sub1.customer_type AS customer_type,
sub1.channel AS channel,
sub1.acquisition_type AS acquisition_type,
(CASE sub2.value WHEN '0' THEN 0 ELSE (sub1.value/sub2.value)END) AS value
FROM
	bi.salesreport_daily as sub1
	
	INNER JOIN bi.salesreport_daily sub2
	ON sub1.locale = sub2.locale AND sub1."year_month" = sub2."year_month" AND sub1.sub_kpi = sub2.sub_kpi AND sub1.customer_type = sub2.customer_type AND sub1.channel = sub2.channel AND sub1.acquisition_type = sub2.acquisition_type
	
WHERE
	sub1.kpi in ('Signed Revenue')
	AND sub2.kpi in ('Signed Deals')
	AND sub1.sub_kpi in ('Actual');

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.click_to_signed;
CREATE TABLE bi.click_to_signed AS

SELECT

	LEFT(created.locale__c, 2) as country,
	-- created.sfid,
	created.created_date,
	-- contract_sent.sent_date,
	EXTRACT(EPOCH FROM (contract_sent.sent_date - created.created_date))/3600::numeric as time_creation_contract_sent,
	(click.clicks_or_reloads) as clicks_or_reloads,
	-- click.first_click,
	EXTRACT(EPOCH FROM (click.first_click - contract_sent.sent_date))/3600::numeric  as time_contract_sent_click,
	-- click.last_click_or_reload,
	-- click.time_first_to_last_click,
	-- signed.signed_date::time,
	EXTRACT(EPOCH FROM (signed.signed_date -  created.created_date))/3600::numeric  as time_creation_signed,
	EXTRACT(EPOCH FROM (signed.signed_date - contract_sent.sent_date))/3600::numeric  as time_contract_sent_signed,
	EXTRACT(EPOCH FROM (signed.signed_date - click.first_click))/3600::numeric  as time_first_click_signed
	
	-- EXTRACT(EPOCH FROM '2 months 3 days 12 hours 65 minutes'::INTERVAL)/60

FROM

	(SELECT
	
		o.createddate as created_date,
		o.sfid,
		o.locale__c
	
	FROM
	
		salesforce.opportunity o

	WHERE

		o.createddate >= '2018-01-22' ) as created

LEFT JOIN

	(SELECT

		t1.opportunityid,
		t1.createddate as signed_date
		
	FROM
	
	(SELECT
	
		DISTINCT oo.opportunityid,
		oo.createddate
	
	FROM
	
		salesforce.opportunityfieldhistory oo
					
	WHERE
	
		oo.newvalue IN ('PENDING')) t1
		
	ORDER BY
	
		t1.createddate desc) as signed
				
ON 

	created.sfid = signed.opportunityid
	
LEFT JOIN

	(SELECT 

	   metadata_json#>>'{opportunity,Id}' as "opp_id",
	   COUNT(id) as "clicks_or_reloads",
	   MIN(created_at) as "first_click",
	   MAX(created_at) as "last_click_or_reload",
		MAX(created_at) - MIN(created_at) as time_first_to_last_click
	            
	FROM       
	
		events.sodium
	
	WHERE 
	
	   event_name = 'opportunity:get_offer'
	   AND id > 3300000
	            
	GROUP BY    
	
		"opp_id") as click
		
ON

	created.sfid = click.opp_id
	
LEFT JOIN

	(SELECT

		t1.opportunityid,
		t1.createddate as sent_date
		
	FROM
	
	(SELECT
	
		DISTINCT oo.opportunityid,
		oo.createddate
	
	FROM
	
		salesforce.opportunityfieldhistory oo
					
	WHERE
	
		oo.newvalue IN ('CONTRACT SEND OUT')) t1
		
	ORDER BY
	
		t1.createddate desc) as contract_sent
		
ON

	created.sfid = contract_sent.opportunityid

GROUP BY

	LEFT(created.locale__c, 2),
	created.created_date,
	time_creation_contract_sent,
	time_contract_sent_click,
	time_creation_signed,
	time_contract_sent_signed,
	time_first_click_signed,
	clicks_or_reloads
	
ORDER BY 

	created.created_date desc;	
			
-- Author: Chandra
-- Short Description: This script calculates the actual mom churn rate and the churn rate before start, it uses bi.b2b_monthly_kpis and bi.closemonth_stats tables
-- Created on: 19/02/2018
DROP TABLE IF EXISTS bi.churn_rate;
CREATE TABLE bi.churn_rate AS
				
				
SELECT 	distinct main.year_month as year_month,
			-- On 27/03/2018 chandra: adding locale to the query to use as filter
			main.locale,
			new,
			total_bom,
			total_eom,
			round(((new.new - ((total_eom.total_eom) - (total_bom.total_bom))) / total_bom.total_bom) * 100, 2) AS churn_percent,
			lbs.left_before_start AS left_before_start_churn_percent
FROM bi.b2b_monthly_kpis main
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS new
		FROM bi.b2b_monthly_kpis _inn
			WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'New'
			GROUP BY year_month, locale
	) new
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS total_bom
		FROM bi.b2b_monthly_kpis _inn
			WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'Total BOM'
			GROUP BY year_month, locale
	) total_bom
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	year_month,
					locale,
					sum(value) AS total_eom
		FROM bi.b2b_monthly_kpis _inn
		WHERE _inn.year_month = main.year_month
				AND _inn.locale = main.locale
				AND type = 'Customers'
				AND kpi = 'Total EOM'
			GROUP BY year_month, locale
	) total_eom
ON TRUE
LEFT JOIN
	LATERAL	(
		SELECT 	to_char(min_sign_date, 'YYYY-MM') AS signing_month,
					locale,
					round((SUM(CASE
							WHEN year_diff IS NULL AND status = 'LEFT' THEN unique_customers
								ELSE 0
							END) / sum(unique_customers)) * 100, 2) AS left_before_start

		FROM bi.closemonth_stats _inn
		WHERE _inn.signing_month = main.year_month
			AND _inn.locale = main.locale
		GROUP BY to_char(min_sign_date, 'YYYY-MM'), locale

	) lbs
ON TRUE
WHERE main."year_month" <= to_char(now(), 'YYYY-MM')
GROUP BY 
			main.year_month,
			main.locale,
			new.new,
			total_bom.total_bom,
			total_eom.total_eom,
			lbs.left_before_start
ORDER BY main.year_month desc;	


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED
-- churn date = last order's date - It's the last day on which they are making money
-- Created on: 02/07/2018

DROP TABLE IF EXISTS bi.churn_details;
CREATE TABLE bi.churn_details as 


SELECT  

		LEFT(oo.locale__c, 2) as country,
		oo.locale__c,
		o.delivery_area__c,
		o.opportunityid,
		oo.name,
		oo.stagename,
		oo.status__c,
		ooo.potential as grand_total,
		oo.contract_duration__c,
		oo.hours_weekly__c,
	 	oo.hours_weekly__c*4.33 AS avg_monthly_hours,	
		oo.churn_reason__c,
		oo.nextstep,
		MAX(o.effectivedate) AS date_churn,
		MIN(o.effectivedate) AS first_order,
		MAX(o.effectivedate) - MIN(o.effectivedate) AS Age_days,
			CASE WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) < 31 THEN 'M0'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 31 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 61 THEN 'M1'
			  WHEN (MAX(o.effectivedate) - MIN(o.effectivedate)) >= 61 AND (MAX(o.effectivedate) - MIN(o.effectivedate)) < 92 THEN 'M2'
			  ELSE '>M3'
			  END as Age_Segment
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	LEFT JOIN

		bi.potential_revenue_per_opp ooo

	ON

		o.opportunityid = ooo.opportunityid
				
	WHERE
	
		o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND oo.status__c IN ('RESIGNED', 'CANCELLED')
		AND oo.test__c IS FALSE
		AND o.createddate >= '2018-01-01'

	
	GROUP BY
	
		LEFT(oo.locale__c, 2),
		oo.locale__c,
		o.delivery_area__c,
		-- type_date,
		o.opportunityid,
		oo.name,
		oo.stagename,
		ooo.potential,
		oo.contract_duration__c,
		oo.hours_weekly__c,
		oo.status__c,
		oo.churn_reason__c,
		oo.nextstep;
		
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: Table containing the information regarding the activities of the partners on the portal (validation of orders, comments...)
-- Created on: 18/07/2018

DELETE FROM bi.monitoring_portal_partners WHERE year_month = TO_CHAR(current_date::date,'YYYY-MM');

INSERT INTO bi.monitoring_portal_partners

SELECT

	t5.*,
	t6.origin as origin_case,
	t6.subject,
	COUNT(DISTINCT t6.sfid_case) as number_cases

FROM

	(SELECT
	
	  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
	  MIN(effectivedate::date) as mindate,
	  MAX(effectivedate::date) as maxdate,
	  t3.delivery_areas__c as polygon,
	  t3.subcon as name_partner,
	  t3.sfid_partner,
	  COUNT(DISTINCT t4.sfid) as total_orders,
	  SUM(t4.order_duration__c) as hours,
	  COUNT(DISTINCT t4.professional__c) as number_prof,
	  COUNT(DISTINCT t4.contact__c) as number_customers,
	  SUM(CASE WHEN t4.status IN ('FULFILLED') THEN 1 ELSE 0 END) as fulfilled,
	  SUM(CASE WHEN t4.status IN ('INVOICED') THEN 1 ELSE 0 END) as invoiced,
	  SUM(CASE WHEN t4.status IN ('PENDING TO START') THEN 1 ELSE 0 END) as pts,
	  SUM(CASE WHEN t4.status IN ('NOSHOW CUSTOMER') THEN 1 ELSE 0 END) as noshow_customer,
	  SUM(CASE WHEN t4.status IN ('NOSHOW PROFESSIONAL') THEN 1 ELSE 0 END) as noshow_professional,
	  SUM(CASE WHEN t4.status IN ('CANCELLED NOMANPOWER') THEN 1 ELSE 0 END) as nomanpower,
	  SUM(CASE WHEN t4.status LIKE '%ERROR%' THEN 1 ELSE 0 END) as error,
	  SUM(CASE WHEN t4.status LIKE '%MISTAKE%' THEN 1 ELSE 0 END) as mistake
  
	FROM
	
	  (SELECT
	  
	     t1.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t1
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t1.parentid)
	     
		WHERE 
		
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	   	and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
	      
	JOIN 
	
	  salesforce.order t4
	  
	ON
	
	  (t3.sfid = t4.professional__c)
	  
	WHERE
	
	  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
	  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
	  and LEFT(t4.locale__c,2) IN ('de')
	  and t4.effectivedate < current_date
	  AND t4.effectivedate >= '2018-01-01'
	  AND t4.type = 'cleaning-b2b'
	  AND t4.test__c IS FALSE
	
	GROUP BY
	
	  year_month,
	  polygon,
	  t3.subcon,
	  t3.sfid_partner
	  
	  
	ORDER BY
		
	  year_month,
	  polygon,
	  t3.subcon	) as t5
	  
LEFT JOIN	

	(SELECT

		TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
		MIN(o.createddate) as mindate,
		a.delivery_areas__c,
		a.company_name__c,
		a.sfid_partner,
		-- a.name,
		-- a.sfid as sfid_partner,
		o.sfid as sfid_case,
		o.accountid as sfid_cleaner,
		COUNT(DISTINCT o.contactid) as number_customers,
		COUNT(DISTINCT o.sfid) as number_cases,
		COUNT(DISTINCT oo.sfid) as number_orders,
		COUNT(DISTINCT oo.contact__c) as number_customers2,
		COUNT(DISTINCT oo.professional__c) as number_prof,
		SUM(oo.order_duration__c) as hours,
		SUM(CASE WHEN oo."status" = 'FULFILLED' THEN 1 ELSE 0 END) as orders_validated,
		o.origin,
		-- o.reason,
		o.subject
		
	FROM
	
		(SELECT
	  
	     t1.*,
	     t2.name as subcon,
	     t2.sfid as sfid_partner
	     
	   FROM
	   
	   	Salesforce.Account t1
	   
		JOIN
	      
			Salesforce.Account t2
	   
		ON
	   
			(t2.sfid = t1.parentid)
	     
		WHERE 
		
			t1.status__c not in ('SUSPENDED') and t1.test__c = '0' and t1.name not like '%test%'
	   	and (t1.type__c like 'cleaning-b2c' or (t1.type__c like '%cleaning-b2c;cleaning-b2b%') or t1.type__c like 'cleaning-b2b')
	   	and t2.name NOT LIKE '%BAT Business Services GmbH%') a
	   	
	LEFT JOIN
	
		salesforce."case" o
	
	ON
	
		 a.sfid = o.accountid
		
	LEFT JOIN
	
		salesforce.order oo
		
	ON
	
		a.sfid = oo.professional__c
		AND TO_CHAR(o.createddate, 'YYYY-MM') = TO_CHAR(oo.effectivedate, 'YYYY-MM') 
		AND oo.effectivedate >= '2018-01-01'
		
	WHERE
	
		(o.origin = 'Partner portal' OR o.origin LIKE 'Partner - portal%')
		AND LOWER(o.description) NOT LIKE '%test%'
		AND LOWER(a.name) NOT LIKE '%test%'
		AND LOWER(a.company_name__c) NOT LIKE '%test%'
		AND LOWER(a.name) NOT LIKE '%book a cat%'
		AND oo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NO MANPOWER')
		
		AND LEFT(oo.locale__C,2) in ('de')
	   AND oo.test__c IS FALSE
		AND LOWER(o.description) NOT LIKE '%test%'
		
	GROUP BY
	
		year_month,
		a.company_name__c,
		a.sfid_partner,
		o.sfid,
		o.accountid,
		o.contactid,
		-- a.name,
		-- a.sfid,
		-- o.sfid,
		a.delivery_areas__c,
		o.origin,
		-- o.reason,
		o.subject
		
	ORDER BY
	
		year_month desc) as t6
		
ON

	t5.sfid_partner = t6.sfid_partner
	AND t5.year_month = t6.year_month
	
GROUP BY

	t5.year_month,
	t5.mindate,
	t5.maxdate,
	t5.polygon,
	t5.name_partner,
	t5.sfid_partner,
	t5.total_orders,
	t5.hours,
	t5.number_prof,
	t5.number_customers,
	t5.fulfilled,
	t5.invoiced,
	t5.pts,
	t5.noshow_customer,
	t5.noshow_professional,
	t5.nomanpower,
	t5.error,
	t5.mistake,
	t6.origin,
	t6.subject

ORDER BY

	year_month desc;


-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
 
 -- Sales First Response Time

DROP TABLE IF EXISTS bi.sales_firstresponsetime;
CREATE TABLE bi.sales_firstresponsetime as

SELECT DISTINCT ON (LikelieID)
*

FROM

(SELECT

t1.*,

-- ---------------------------------------------------------------------------------- difference: owner change -> first contact		

-- -
-- - working h Mo-Fr 09:00:00 - 18:00:00 
-- - summertime timezone difference of 2 h

CASE 
	WHEN t1.ownerchange_date::date IS NULL THEN 'rule 0.1'
	WHEN t1.first_contact::date IS NULL THEN 'rule 0.2'
	WHEN t1.ownerchange_date::timestamp > t1.first_contact::timestamp	THEN 'rule 1.0'
	WHEN t1.ownerchange_date::date = t1.first_contact::date THEN 
		(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' THEN 'rule 2.1' ELSE 'rule 2.2' END)
	ELSE 
		(CASE WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 'rule 3.1' 
				WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 'rule 3.2'
				ELSE (CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' THEN 'rule 3.3' ELSE 'rule 3.4'END)
		END) END AS rule_set,


CASE 
-- if the owner change was after the first contact date just use the value "-1" -> this will be used as a filter in tableau
	WHEN t1.ownerchange_date::timestamp > t1.first_contact::timestamp THEN -1

-- if owner change date and first contact date on the same day, calculate difference between times in minutes
	WHEN t1.ownerchange_date::date = t1.first_contact::date
	THEN 
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
		(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0'
				THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' ) * 60 +
						DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
				ELSE
						DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) * 60 +
						DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) 
				END)
					
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
	ELSE
	
	-- minutes pased on the owner change date UNTIL END OF WORK
				-- owner changed after working day
		(CASE WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 0 
				-- owner change at the weekend
				WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 0
				ELSE
					-- owner changed before working day
					(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
							ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - t1.ownerchange_date::time) * 60 +
									DATE_PART 	('minute', TIME '16:00:00.0' - t1.ownerchange_date::time) END) END)
		+
	
	-- minutes pased on the first contact date beginning at the morning working time
		(CASE WHEN t1.first_contact::time < TIME '07:00:00.0' THEN 0 
				WHEN t1.first_contact::time < TIME '16:00:00.0' 
				THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' )*60 +
						DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
				ELSE (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60 END)

		+
	
	-- create a list of all date incl. owner change date and first contact date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: owner change date, first contact date -> minutes are calculated separatly
		(CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END)
			
END AS First_Response_Time_Minutes,

-- --------------------------------------------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------------------------------------------
-- FRT DETAILS

-- weekday
date_part('dow', t1.ownerchange_date::date) AS weekday,

-- minutes same day
CASE 	WHEN t1.ownerchange_date::date = t1.first_contact::date
		THEN 
		-- if owner change time before start of the working day, calculate difference between 09:00:00 and first contact time
		(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0'
				THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' )*60 +
						DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
				ELSE
						DATE_PART 	('hour', t1.first_contact::timestamp - t1.ownerchange_date::timestamp)*60 +
						DATE_PART 	('minute', t1.first_contact::timestamp - t1.ownerchange_date::timestamp) 
				END) 
		ELSE NULL END AS FRT_sameday,

-- minutes until workday end

CASE  WHEN TIME '16:00:00.0' < t1.ownerchange_date::time THEN 0 
		WHEN date_part('dow', t1.ownerchange_date::date) IN ('6','0') THEN 0
		ELSE
		-- owner changed before working day
			(CASE WHEN t1.ownerchange_date::time < TIME '07:00:00.0' 
					THEN (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
					ELSE	DATE_PART 	('hour', TIME '16:00:00.0' - t1.ownerchange_date::time)*60 +
							DATE_PART 	('minute', TIME '16:00:00.0' - t1.ownerchange_date::time) 
					END)
		END AS FRT_afternoon,
		
-- minutes pased on the first contact date beginning at the morning working time

CASE	WHEN t1.first_contact::time < TIME '07:00:00.0' THEN 0 
		WHEN t1.first_contact::time < TIME '16:00:00.0' 
		THEN 	DATE_PART 	('hour', t1.first_contact::time - TIME '07:00:00.0' )*60 +
				DATE_PART 	('minute', t1.first_contact::time - TIME '07:00:00.0')
		ELSE (DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60 
		END AS FRT_morning,		

-- day difference - weekend
CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 
			END AS FRT_daydif,

-- day difference - weekend
CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.ownerchange_date::date, t1.first_contact::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) 
			ELSE 0 
			END AS FRT_days,


-- ---------------------------------------------------------------------------------- difference: created date -> owner change		
-- -
-- - working h Mo-Fr 09:00:00 - 18:00:00 
-- - summertime timezone difference of 2 h

CASE 

-- if likeli wasn't assigned to an agent use -1 to filter in tableau
	WHEN t1.ownerchange_date::timestamp IS NULL THEN -1

-- if the created date was after the owner change date just use the value "-1" -> this will be used as a filter in tableau
-- WHEN t1.ownerchange_date::timestamp < t1.created_date::timestamp
-- THEN -1

-- if created date and owner change date on the same day, calculate difference between times in minutes
	WHEN t1.ownerchange_date::date = t1.created_date::date
	THEN
		DATE_PART 	('day', t1.ownerchange_date::timestamp - t1.created_date::timestamp) * 24 +
		DATE_PART 	('hour', t1.ownerchange_date::timestamp - t1.created_date::timestamp) * 60 +
		DATE_PART 	('minute', t1.ownerchange_date::timestamp - t1.created_date::timestamp)
	
-- else calculate the difference in seperate parts, which will be summariezed to get the result in minutes	
	ELSE
	
	-- minutes pased on the created date until end of work day
	-- in case likeli was created outside business hours use 0 as value
		(CASE WHEN 
		(TIME '16:00:00.0' < t1.created_date::time )
		THEN 0
		ELSE (DATE_PART 	('hour', TIME '16:00:00.0' - t1.created_date::time) * 60 +
		DATE_PART 	('minute', TIME '16:00:00.0' - t1.created_date::time)) END)
		+
	
	-- minutes pased on the owner change date beginning at the morning working time
		DATE_PART 	('hour', t1.ownerchange_date::time - TIME '07:00:00.0' ) * 60 +
		DATE_PART 	('minute', t1.ownerchange_date::time - TIME '07:00:00.0')
		+
	
	-- create a list of all date incl. created date and owner change date 
	-- COUNT the working days
	-- and SUBTRACT 2 days 
	-- --------------- 2 days: created date, owner change date -> minutes are calculated separatly
		(CASE WHEN
			((SELECT COUNT(*)
			FROM generate_series (t1.created_date::date, t1.ownerchange_date::date, '1 day'::interval) dd
			WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) > 0 
			
			-- in case the working days are > 0 -> workind days * working h per day * 60 minutes = working days in minutes within working h
			THEN  
				((SELECT COUNt(*)
				FROM generate_series (t1.created_date::date, t1.ownerchange_date::date, '1 day'::interval) dd
				WHERE date_part('dow', dd) NOT IN ('6','0')) -2	) *(DATE_PART ('hour', TIME '16:00:00.0' - TIME '07:00:00.0'))*60
			ELSE 0 END)
				
END AS Assign_to_Agent_Minutes,

-- Likeli created outside of working hours
CASE WHEN TIME '16:00:00.0' < t1.created_date::time THEN 1 ELSE 0 END AS created_outside_working_h 


FROM
(SELECT
sub1.parentid AS LikelieID,
likeli.contact_name__c AS Name,
likeli.locale__c AS locale,
likeli.billingaddress_city__c AS city,
likeli.acquisition_channel__c AS aquisition_channel,
likeli.acquisition_tracking_id__c AS aquisition_trackingId,
likeli.stage__c AS stage,
likeli.lost_reason__c AS lost_reason,

CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
		WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
		ELSE 'Agent' END AS createdby,
sub1.createddate AS created_date,
-- CASE WHEN sub1.

CASE 	WHEN t2.ownerchange_date IS NOT NULL THEN t2.ownerchange_date
		WHEN t2.ownerchange_date IS NULL 
			AND (CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
							WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
							ELSE 'Agent' END) <> 'API Tiger' THEN sub1.createddate
		ELSE NULL END AS ownerchange_date,
		
-- t2.ownerchange_date AS ownerchange_date_old,
		
CASE 	WHEN t2.Agent IS NOT NULL THEN t2.Agent
		WHEN t2.Agent IS NULL 
			AND (CASE 	WHEN likeli.createdbyid LIKE '00520000003IiNCAA0' THEN 'API Tiger' 
							WHEN likeli.createdbyid LIKE '00520000004n1OxAAI' THEN 'MC Tiger'
							ELSE 'Agent' END) <> 'API Tiger' THEN likeli.ownername
		ELSE NULL END AS Agent,
		
-- t2.Agent AS Agent_old,

t3.stage_not_reached_date AS stage_not_reached_date,
t4.stage_ended_date AS stage_ended_date,
t5.stage_reached_date AS stage_reached_date,
t6.stage_dm_reached_date AS stage_dm_reached_date,
t7.stage_qualified_date AS stage_qualified_date,


-- ---------------------------------------------------------------------------------- first contacts

LEAST 
		(t3.stage_not_reached_date,
		t4.stage_ended_date,
		t5.stage_reached_date,
		t6.stage_dm_reached_date,
		t7.stage_qualified_date) AS first_contact
		
		


FROM 
salesforce.likeli__history sub1

LEFT JOIN
			(
			SELECT
			sublikeli.sfid AS userid,
			subuser.name AS ownername,
			*
			FROM
			salesforce.likeli__c AS sublikeli
			
			LEFT JOIN	
				salesforce.user AS subuser
				ON (sublikeli.ownerid = subuser.sfid)
			
			) AS likeli
				
				
			ON
				(sub1.parentid = likeli.userid)
			

-- ---------------------------------------------------------------------------------- likelihistory - owner-change

LEFT JOIN
		(SELECT
		sub2.sfid,
		sub2.parentid,
		sub2.createddate AS ownerchange_date,
		CASE WHEN sub3.name IS NULL THEN sub2.newvalue ELSE sub3.name END AS Agent
		
		FROM 
		salesforce.likeli__history sub2
		
			LEFT JOIN
			salesforce.user sub3
			ON
				(sub2.newvalue = sub3.sfid)
		
		WHERE
		sub2.field = 'Owner'
		) AS t2
		
		ON (sub1.parentid = t2.parentid) 
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - NOT REACHED

LEFT JOIN
		(SELECT
		sub4.sfid,
		sub4.parentid,
		sub4.oldvalue,
		sub4.createddate AS stage_not_reached_date
		
		FROM 
		salesforce.likeli__history sub4
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'NOT REACHED'
		) AS t3
		
		ON (sub1.parentid = t3.parentid)
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - ENDED		

LEFT JOIN		
		(SELECT
		sub5.sfid,
		sub5.parentid,
		sub5.createddate AS stage_ended_date
		
		FROM 
		salesforce.likeli__history sub5
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'ENDED'
		) AS t4
		
		ON (sub1.parentid = t4.parentid)		
		
-- ---------------------------------------------------------------------------------- likelihistory - status-change - REACHED		

LEFT JOIN
		(SELECT
		sub6.sfid,
		sub6.parentid,
		sub6.createddate AS stage_reached_date
		
		FROM 
		salesforce.likeli__history sub6
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'REACHED'
		) AS t5

		ON ( sub1.parentid = t5.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - DM REACHED		

LEFT JOIN
		(SELECT
		sub7.sfid,
		sub7.parentid,
		sub7.createddate AS stage_dm_reached_date
		
		FROM 
		salesforce.likeli__history sub7
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'DM REACHED'
		) AS t6

		ON ( sub1.parentid = t6.parentid)

-- ---------------------------------------------------------------------------------- likelihistory - status-change - QUALIFIED		

LEFT JOIN
		(SELECT
		sub8.sfid,
		sub8.parentid,
		sub8.createddate AS stage_qualified_date
		
		FROM 
		salesforce.likeli__history sub8
		
		WHERE
		field = 'stage__c'
		AND oldvalue = 'NEW'
		AND newvalue = 'QUALIFIED'
		) AS t7

		ON ( sub1.parentid = t7.parentid)
		
				
WHERE
sub1.field = 'created'
AND sub1.createddate::date > '2017-06-01'
AND likeli.acquisition_channel__c <> 'outbound'
AND likeli.type__c <> 'B2C'
AND likeli.test__c = 'false'
AND likeli.ownerid NOT LIKE '00520000004n1OxAAI'



GROUP BY
sub1.parentid,
likeli.contact_name__c,
likeli.locale__c,
likeli.billingaddress_city__c,
likeli.acquisition_channel__c,
likeli.acquisition_tracking_id__c,
likeli.stage__c,
likeli.lost_reason__c,
likeli.ownername,

likeli.createdbyid,
sub1.createddate,

t2.ownerchange_date,
t2.Agent,

t3.stage_not_reached_date,
t4.stage_ended_date,
t5.stage_reached_date,
t6.stage_dm_reached_date,
t7.stage_qualified_date
) AS t1

ORDER BY created_date desc
	
) AS FRT_final

ORDER BY LikelieID, ownerchange_date ASC;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.variation_grand_total_monthly;
CREATE TABLE bi.variation_grand_total_monthly as
		
		
SELECT

	t4.year_month,
	MIN(t4.mindate) as mindate,
	SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as oldvalue,
	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) as newvalue,

	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) - SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as variation

FROM
			
	(SELECT
	
		t1.year_month,
		t1.opportunityid,
		t1.mindate,
		t1.maxdate,
		t2.oldvalue,
		t3.newvalue
				
	FROM
	
		
		(SELECT -- Here I create the list of first and last changes, every month, for every opportunity
			
			TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
			MIN(o.createddate) as mindate,
			MAX(o.createddate) as maxdate,
			o.opportunityid
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('1')
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_month,
			o.opportunityid
			
		ORDER BY
		
			year_month desc) as t1
			
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every month
			
			TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_month desc) as t2
		
	ON
	
		t1.mindate = t2.createddate
		AND t1.opportunityid = t2.opportunityid
	
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every month
			
			TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_month,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_month desc) as t3
		
	ON
	
		t1.maxdate = t3.createddate
		AND t1.opportunityid = t3.opportunityid
		
	ORDER BY
	
		t1.year_month desc) as t4
		
GROUP BY

	t4.year_month
	
ORDER BY 

	t4.year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.variation_grand_total;
CREATE TABLE bi.variation_grand_total as
		
		
SELECT

	t4.year_week,
	MIN(t4.mindate) as mindate,
	SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as oldvalue,
	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) as newvalue,

	SUM(COALESCE(NULLIF(t4.newvalue, ''), '0')::decimal) - SUM(COALESCE(NULLIF(t4.oldvalue, ''), '0')::decimal) as variation

FROM
			
	(SELECT
	
		t1.year_week,
		t1.opportunityid,
		t1.mindate,
		t1.maxdate,
		t2.oldvalue,
		t3.newvalue
				
	FROM
	
		
		(SELECT -- Here I create the list of first and last changes, every week, for every opportunity
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			MIN(o.createddate) as mindate,
			MAX(o.createddate) as maxdate,
			o.opportunityid
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('1')
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.opportunityid
			
		ORDER BY
		
			year_week desc) as t1
			
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every week
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_week desc) as t2
		
	ON
	
		t1.mindate = t2.createddate
		AND t1.opportunityid = t2.opportunityid
	
	LEFT JOIN
	
		(SELECT -- Here I'm listing all the changes of grand total for every opportunity, every week
			
			TO_CHAR(o.createddate, 'YYYY-WW') as year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		FROM
		
			salesforce.opportunityfieldhistory o
			
		LEFT JOIN

			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
			
		WHERE
		
			o.field IN ('grand_total__c')
			AND o.oldvalue IS NOT NULL
			AND o.oldvalue NOT IN ('')
			AND o.oldvalue NOT IN ('1') 
			AND ABS(COALESCE(NULLIF(o.newvalue, ''), '0')::decimal - COALESCE(NULLIF(o.oldvalue, ''), '0')::decimal) < 5000
			AND oo.test__c IS FALSE
			
		GROUP BY
		
			year_week,
			o.createddate,
			o.opportunityid,
			o.oldvalue,
			o.newvalue
			
		ORDER BY
		
			year_week desc) as t3
		
	ON
	
		t1.maxdate = t3.createddate
		AND t1.opportunityid = t3.opportunityid
		
	ORDER BY
	
		t1.year_week desc) as t4
		
GROUP BY

	t4.year_week
	
ORDER BY 

	t4.year_week desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------



DELETE FROM public.marketing_spending;

INSERT INTO public.marketing_spending

SELECT

	l."Date" as start_date,
	l."Date" as end_date,	
	LEFT(LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)), 2) as locale,	
	LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)) as languages,	
	LOWER(SUBSTRING(l."CampaignName" from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')) as "city",	
	LEFT(l."CampaignName", 3) as type,	
	'Marketing Costs' as kpi,	
	'Adwords' as sub_kpi_1,
		
	CASE WHEN l."AccountDescriptiveName" = 'Reinigung Direkt' THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services' END as sub_kpi_2,
	
	LOWER(substring(l."AdGroupName" from '[0-9]{3}_[A-Z]{2}_([^\+]+)')) as sub_kpi_3,
	'New' as sub_kpi_4,
	'SEM B2B' as sub_kpi_5,
	'' as sub_kpi_6,
	'' as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	SUM(l."Cost"::numeric/1000000) as value
	
	
	-- CASE WHEN l."AccountDescriptiveName" IN ('unbounce rei') THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services'
		
	-- lower(substring(l.campaignname from 'tpc:(.[^\,]+)')) as "keyword",
	-- lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as "city",

FROM

	public.adwords_adgroup_report l

GROUP BY

	l."Date",
	l."AccountDescriptiveName",
	LOWER(SUBSTRING(l."CampaignName" from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')),
	LEFT(LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)), 2),
	LOWER(RIGHT(LEFT(l."CampaignName", 9), 5)),
	LEFT(l."CampaignName", 3),
	LOWER(substring(l."AdGroupName" from '[0-9]{3}_[A-Z]{2}_([^\+]+)'));

---------------------------------------------------------------------------------------------

INSERT INTO public.marketing_spending	

SELECT

	l.date as start_date,
	l.date as end_date,	
	LEFT(LOWER(RIGHT(LEFT(l.campaign, 9), 5)), 2) as locale,	
	LOWER(RIGHT(LEFT(l.campaign, 9), 5)) as languages,	
	LOWER(SUBSTRING(l.campaign from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')) as "city",	
	LEFT(l.campaign, 3) as type,	
	'Marketing Costs' as kpi,	
	'Bing' as sub_kpi_1,
		
	CASE WHEN l.account = 'Reinigung Direkt DE' THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services' END as sub_kpi_2,
	
	LOWER(substring(l.adgroup from '[0-9]{3}_[A-Z]{2}_([^\+]+)')) as sub_kpi_3,
	'New' as sub_kpi_4,
	'SEM B2B' as sub_kpi_5,
	'' as sub_kpi_6,
	'' as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	SUM(l.cost::numeric) as value
	
	
	-- CASE WHEN l."AccountDescriptiveName" IN ('unbounce rei') THEN 'Reinigungdirekt' ELSE 'Tiger Facility Services'
		
	-- lower(substring(l.campaignname from 'tpc:(.[^\,]+)')) as "keyword",
	-- lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as "city",

FROM

	public.bing_ads_report l

GROUP BY

	l.date,
	l.account,
	LOWER(SUBSTRING(l.campaign from '[0-9A-Z]{3}_[A-Z]{2}-[A-Z]{2}_[A-Z]{2}_[0-9]{3}_([^_]+)')),
	LEFT(LOWER(RIGHT(LEFT(l.campaign, 9), 5)), 2),
	LOWER(RIGHT(LEFT(l.campaign, 9), 5)),
	LEFT(l.campaign, 3),
	LOWER(substring(l.adgroup from '[0-9]{3}_[A-Z]{2}_([^\+]+)'));

---------------------------------------------------------------------------------------------
	
	
INSERT INTO public.marketing_spending	
	
SELECT

	TO_CHAR(l.createddate, 'YYYY-MM-DD')::date as start_date,
	TO_CHAR(l.createddate, 'YYYY-MM-DD')::date as end_date,	
	
	LEFT(l.locale__c, 2) as locale,	
	l.locale__c as languages,	
	
	LOWER(SUBSTRING(REPLACE(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
	
		
	l.type__c as type,	
	'Likelies' as kpi,
	CASE 	
		WHEN LOWER(l.acquisition_channel_ref__c) = 'anygrowth' and l.acquisition_tracking_id__c IS NOT NULL THEN 'Anygrowth'
	
		WHEN LOWER(l.acquisition_channel_ref__c) = 'reveal' and l.acquisition_tracking_id__c IS NOT NULL THEN 'Reveal'
	
		WHEN (l.acquisition_channel_params__c IN ('{"ref":"b2c"}'))	THEN 'B2C Homepage Link'
		
		WHEN (l.acquisition_channel_params__c IN ('{"ref":"b2c-home-banner"}'))	THEN 'B2C Homepage Banner'
		
		WHEN (l.acquisition_channel_params__c IN ('{"src":"step1"}')) THEN 'B2C Funnel Link'
	
		WHEN ((lower(l.acquisition_channel__c) = 'inbound') AND lower(l.acquisition_tracking_id__c) = 'classifieds')					
		THEN 'Classifieds'
					
		WHEN (l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysmb%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goob%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	
	         AND l.createddate::date <= '2017-02-08'
	
	     THEN 'SEM Brand'::text
	
	     WHEN ((((l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goob%')
	
	     	AND l.createddate::date > '2017-02-08'
	     	AND ((l.acquisition_channel_params__c LIKE '%goob%') AND (l.acquisition_channel_params__c LIKE '%bt:b2b%' OR l.acquisition_channel_params__c LIKE '%bt: b2b%' OR l.acquisition_channel_params__c LIKE '%"bt":"b2b"%'))
	     	)
	
	     THEN 'SEM Brand B2B'::text
	
	     WHEN (((l.acquisition_channel_params__c::text ~~ '%goob%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goob%')
	
	     	AND l.createddate::date > '2017-02-08'
			AND ((l.acquisition_channel_params__c LIKE '%goob%') AND (l.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%' ))
	     	
	     THEN 'SEM Brand B2C'::text
	     
	
	     WHEN (((l.acquisition_channel_params__c::text ~~ '%goog%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goog%')
	
	     	AND l.createddate::date <= '2017-02-08'
	     THEN 'SEM'::text
	
	     WHEN (l.acquisition_channel_params__c::text ~~ '%goog%'::text) AND (l.acquisition_channel_params__c LIKE '%bt: b2b%' OR l.acquisition_channel_params__c LIKE '%"bt":"b2b%') THEN 'Adwords'
	     
	     WHEN l.acquisition_channel_params__c::text ~~ '%ysm%'::text THEN 'Bing'
	
	     WHEN ((((l.acquisition_channel_params__c::text ~~ '%goog%'::text OR l.acquisition_channel_params__c::text ~~ '%ysm%'::text OR l.acquisition_channel_ref__c::text ~~ '%clid=goog%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text) or l.acquisition_tracking_id__c like '%goog%')
	
	     	AND l.createddate::date > '2017-02-08' 
	     	AND ((l.acquisition_channel_params__c LIKE '%goog%') AND (l.acquisition_channel_params__c NOT LIKE '%bt: b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%bt:b2b%' ) AND (l.acquisition_channel_params__c NOT LIKE '%"bt":"b2b"%')))
	     	OR l.acquisition_channel_params__c::text LIKE '%goog%'
	     THEN 'SEM B2C'::text
	     
	
	     WHEN (l.acquisition_channel_params__c::text ~~ '%disp%'::text OR l.acquisition_channel_params__c::text ~~ '%dsp%'::text) 
	         AND l.acquisition_channel_params__c::text !~~ '%facebook%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	     THEN 'Display'::text
	                 
	
	     WHEN l.acquisition_channel_params__c::text ~~ '%ytbe%'::text
	     THEN 'Youtube Paid'::text
	
	
	     WHEN (l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_ref__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text
	         AND LOWER(acquisition_tracking_id__c) LIKE '%b2b seo page form&'
	     THEN 'SEO B2B'::text
	     
	
	     WHEN ((l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text !~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_ref__c::text !~~ '%goob%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text
			AND LOWER(acquisition_tracking_id__c) NOT LIKE '%b2b seo page form&')
	
	     	OR
	     		((acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL AND (acquisition_tracking_id__c LIKE '%b2b%' OR acquisition_tracking_id__c LIKE '%tfs%')))
	
	     THEN 'SEO'::text
	     
	     WHEN (l.acquisition_channel_ref__c::text ~~ '%google%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yahoo%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%bing%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%yandex%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%naver%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%baidu%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%ask%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%duckduckgo%'::text) 
	         AND l.acquisition_channel_ref__c::text ~~ '%tiger%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%goog%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%ysmb%'::text 
	         AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	         AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	         THEN 'SEO Brand'::text
	         
	     WHEN (l.acquisition_channel_params__c::text ~~ '%batfb%'::text  
	             OR l.acquisition_channel_ref__c::text ~~ '%batfb%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%facebook%'::text 
	             OR l.acquisition_channel_ref__c::text ~~ '%facebook%'::text) 
	     THEN 'Facebook'::text
	
	
	     WHEN l.acquisition_channel_params__c::text ~~ '%newsletter%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%email%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%vero%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%batnl%'::text 
	             OR l.acquisition_channel_params__c::text ~~ '%fullname%'::text
	             OR l.acquisition_channel_params__c::text ~~ '%invoice%'::text 
	     THEN 'Newsletter'::text
	     
	     WHEN (l.acquisition_channel_params__c::text !~~ '%goog%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%ysm%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%disp%'::text
	             AND l.acquisition_channel_params__c::text !~~ '%dsp%'::text
	             AND l.acquisition_channel_params__c::text !~~ '%batfb%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%ytbe%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%fb%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%clid%'::text 
	             AND l.acquisition_channel_params__c::text !~~ '%utm%'::text 
	             AND l.acquisition_channel_params__c::text <> ''::text)
	     	OR (acquisition_channel__c IS NULL AND acquisition_channel_params__c IS NULL AND acquisition_channel_ref__c IS NULL)
	     THEN 'DTI'::text
	     
	     ELSE 'Unattributed'::text -- Make sure with Alex and Ludo that the acquisition will be attributed as Newsletter by default

		END as sub_kpi_1,	

	CASE WHEN (l.acquisition_tracking_id__c LIKE 'rd %' OR l.acquisition_tracking_id__c LIKE '%unbounce_reinigungdirekt%') THEN 'Reinigungdirekt'
	     WHEN (l.acquisition_tracking_id__c LIKE 'tfs %' OR l.acquisition_tracking_id__c LIKE '%unbounce_tfs%') THEN 'Tiger Facility Services'
		  ELSE 'Other'
		  END as sub_kpi_2,
	l.acquisition_tracking_id__c as sub_kpi_3,
	lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as sub_kpi_4,
	-- LOWER(SUBSTRING(l.acquisition_channel_params__c from 'tpc:(.[^\,]+)')) as sub_kpi_3,
	
	'SEM B2B' as sub_kpi_5,
	
	l.stage__c as sub_kpi_6,
	l.lost_reason__c as sub_kpi_7,
	'' as sub_kpi_8,
	'' as sub_kpi_9,
	'' as sub_kpi_10,
	COUNT(DISTINCT l.sfid) as value

FROM

	salesforce.likeli__c l
	
WHERE

	l.type__c = 'B2B'
	AND l.acquisition_channel__c IN ('web', 'inbound')
	AND l.createddate > '2017-12-31'	
	AND l.company_name__c NOT LIKE '%test%'
	AND l.company_name__c NOT LIKE '%bookatiger%'
	AND l.email__c NOT LIKE '%bookatiger%'
	
	AND lower(substring(replace(regexp_replace(l.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) NOT LIKE '%test%'
	
GROUP BY

	start_date,
	end_date,	
	l.acquisition_channel_params__c,
	LEFT(l.locale__c, 2),	
	l.locale__c,	
	type,	
	kpi,	
	sub_kpi_1,	
	sub_kpi_2,
	sub_kpi_3,
	sub_kpi_4,
	sub_kpi_5,
	l.stage__c,
	l.lost_reason__c,
	sub_kpi_8,
	sub_kpi_9,
	sub_kpi_10
	
ORDER BY

	start_date desc;
	
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: Creates a list of all the opportunities running at the moment and give insights on their current status concerning their notice date and traffic light
-- Created on: 25/10/2018

DROP TABLE IF EXISTS bi.notice_lights;
CREATE TABLE bi.notice_lights AS

SELECT

	table3.year_month,
	table3.date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	o.grand_total__c,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.opportunityid,
	oo.name,
	oo.status__c,
	t1.current_owner,
	t1.operated_by,
	t1.operated_by_detail,
	t1.current_avg_traffic_light,	
	CASE WHEN (current_date > (notice_date__c - 30) AND (current_date <= notice_date__c)) THEN 'Notice approaching (within 30days)'
		 WHEN  current_date > notice_date__c THEN 'Notice expired'
		 ELSE 'Notice far away (more than 30 days)' END as notice_type,	
	CASE WHEN current_date > (notice_date__c - 30) AND t1.current_avg_traffic_light <= 2.5 THEN 'Alert' ELSE 'Low priority' END as status_notice,	
	o.start__c,	
	o.end__c,	
	o.notice_date__c,
	o.notice__c,
	CASE WHEN  o.grand_total__c = '0' THEN 'irregular' ELSE
	(CASE 
		-- duration: unlimited
		WHEN additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
		WHEN additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
		-- duration: 12 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
		-- duration: 6 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
		-- duration: 3 month
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
		-- duration: 1 month
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
		ELSE (CASE WHEN o.duration__c IS NULL THEN '1' ELSE o.duration__c END)END) END as duration2,
	o.additional_agreements__c

FROM	
	
	(SELECT
	
		year_month,
		date,
		-- max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.opportunityid,
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps' as varchar) as kpi,
			CAST('Count' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.category
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				oo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date ELSE t2.date_churn END as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.date_churn, 'YYYY-MM') || '-01' END as year_month_churn
							
						
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								o.delivery_area__c,
								MIN(o.effectivedate) as date_start
							
							FROM
							
								salesforce.order o
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND o.effectivedate >= '2018-01-01'
								
							GROUP BY
							
								o.opportunityid,
								o.locale__c,
								o.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'CANCELLED CUSTOMER', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND oo.test__c IS FALSE
								AND o.effectivedate >= '2018-01-01'
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid

			LEFT JOIN

				bi.potential_revenue_per_opp oo

			ON

				t2.opportunityid = oo.opportunityid
	
			WHERE 
	
				o.test__c IS FALSE
				
			GROUP BY
			
					oo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category
				) as table1
	
		WHERE
	
			table1.opportunityid IS NOT NULL
				
		GROUP BY
		
			table1.year_month,
			table1.ymd_max,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	GROUP BY
	
		year_month,
		date,
		-- max_date,
		locale,
		languages,
		city,
		type,
		kpi,
		table2.opportunityid,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5
		
	ORDER BY 
	
		year_month desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_tableau t1
	
ON

	table3.opportunityid = t1.current_opportunity
	
LEFT JOIN

	salesforce.contract__c o
	
ON

	table3.opportunityid = o.opportunity__c
	
LEFT JOIN	

	salesforce.opportunity oo
	
ON

	table3.opportunityid = oo.sfid
	
WHERE

	table3.year_month = TO_CHAR(current_date, 'YYYY-MM')
	AND table3.value = 1
	AND t1.current_avg_traffic_light IS NOT NULL
	AND o.status__c IN ('ACCEPTED', 'SIGNED')
	AND o.service_type__c LIKE 'maintenance cleaning'
	-- AND o.active__c IS TRUE
	
GROUP BY

	table3.year_month,
	table3.date,
	-- max_date,
	table3.locale,
	table3.languages,
	table3.city,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	o.grand_total__c,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.opportunityid,
	oo.name,
	oo.status__c,
	t1.current_owner,
	t1.operated_by,
	t1.operated_by_detail,
	table3.value,
	t1.current_avg_traffic_light,
	o.start__c,
	o.end__c,
	o.notice_date__c,
	o.notice__c,
	o.duration__c,
	o.additional_agreements__c,
	CASE WHEN  o.grand_total__c = '0' THEN 'irregular' ELSE
	(CASE 
		-- duration: unlimited
		WHEN additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
		WHEN additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
		-- duration: 12 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
		-- duration: 6 month
		WHEN additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
		-- duration: 3 month
		WHEN additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
		-- duration: 1 month
		WHEN additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
		ELSE (CASE WHEN o.duration__c IS NULL THEN '1' ELSE o.duration__c END)END) END;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: new analysis regarding the churn, new filters on the contracts, the providers, the category of revenue, the cohort... (CHURNED OPPS PART)
-- Created on: 25/10/2018


DROP TABLE IF EXISTS bi.deep_churn;
CREATE TABLE bi.deep_churn as

SELECT

	table3.year_month,
	table3.year_month_start,
	table3.mindate_churn as date,
	-- max_date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	
	table3.pricing,
	table3.money,
	table3.churn_reason,
	table3.contract_duration,
	
	table3.value,
	
	CASE WHEN table6.provider IS NULL THEN 'Not matched yet' ELSE table6.provider END as provider,
	table6.company_name,
	CASE WHEN table3.churn_reason LIKE '%one off%' THEN 'One-Off'
		  ELSE 'Recurrent'
		  END as acquisition_type,
	MIN(CASE WHEN (table7.name NOT LIKE '%S' AND table7.name NOT LIKE '%G' AND table7.name NOT LIKE '%N') THEN table7.amount__c ELSE 0 END)/1.19 as amount__c

FROM	
	
	(SELECT
	
		TO_CHAR((table1.confirmed_end)::date,'YYYY-MM') as year_month,
		TO_CHAR(table1.date_start,'YYYY-MM') as year_month_start,
		MIN(table1.confirmed_end) as mindate_churn,
		LEFT(table1.country,2) as locale,
		table1.contract_when_accepted__c,
		table1.closedate,
		table1.locale__c as languages,
		-- CAST('-' as varchar) as city,
		table1.delivery_area__c as city,
		table1.opportunityid,
		table1.name,
		CAST('B2B' as varchar) as type,
		CAST('Churn' as varchar) as kpi,
		CAST('Count opps' as varchar) as sub_kpi_1,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 31 THEN 'M0'
				  WHEN (table1.date_churn - table1.date_start) >= 31 AND (table1.date_churn - table1.date_start) < 62 THEN 'M1'
				  WHEN (table1.date_churn - table1.date_start) >= 62 AND (table1.date_churn - table1.date_start) < 93 THEN 'M2'
				  WHEN (table1.date_churn - table1.date_start) >= 93 AND (table1.date_churn - table1.date_start) < 124 THEN 'M3'
				  WHEN (table1.date_churn - table1.date_start) >= 124 AND (table1.date_churn - table1.date_start) < 155 THEN 'M4'
				  WHEN (table1.date_churn - table1.date_start) >= 155 AND (table1.date_churn - table1.date_start) < 186 THEN 'M5'
				  WHEN (table1.date_churn - table1.date_start) >= 186 AND (table1.date_churn - table1.date_start) < 217 THEN 'M6'
				  WHEN (table1.date_churn - table1.date_start) >= 217 AND (table1.date_churn - table1.date_start) < 248 THEN 'M7'
				  WHEN (table1.date_churn - table1.date_start) >= 248 AND (table1.date_churn - table1.date_start) < 279 THEN 'M8'
				  WHEN (table1.date_churn - table1.date_start) >= 279 AND (table1.date_churn - table1.date_start) < 310 THEN 'M9'
				  WHEN (table1.date_churn - table1.date_start) >= 310 AND (table1.date_churn - table1.date_start) < 341 THEN 'M10'
				  WHEN (table1.date_churn - table1.date_start) >= 341 AND (table1.date_churn - table1.date_start) < 372 THEN 'M11'
				  WHEN (table1.date_churn - table1.date_start) >= 372 AND (table1.date_churn - table1.date_start) < 403 THEN 'M12'
				  WHEN (table1.date_churn - table1.date_start) >= 403 AND (table1.date_churn - table1.date_start) < 434 THEN 'M13'
				  WHEN (table1.date_churn - table1.date_start) >= 434 AND (table1.date_churn - table1.date_start) < 465 THEN 'M14'
				  WHEN (table1.date_churn - table1.date_start) >= 465 AND (table1.date_churn - table1.date_start) < 496 THEN 'M15'
				  WHEN (table1.date_churn - table1.date_start) >= 496 AND (table1.date_churn - table1.date_start) < 527 THEN 'M16'
				  WHEN (table1.date_churn - table1.date_start) >= 527 AND (table1.date_churn - table1.date_start) < 558 THEN 'M17'			  
				  ELSE '>M18'
				  END as sub_kpi_2,
		
		CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			  
		CASE WHEN table1.money < 250 THEN '<250€'
			  WHEN table1.money >= 250 AND table1.money < 500 THEN '250€-500€'
			  WHEN table1.money >= 500 AND table1.money < 1000 THEN '500€-1000€'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
		CASE WHEN table1.money < 250 THEN 'Very Small'
			  WHEN table1.money >= 250 AND table1.money < 500 THEN 'Small'
			  WHEN table1.money >= 500 AND table1.money < 1000 THEN 'Medium'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
		table1.pricing,
		table1.money,
		table1.churn_reason__c as churn_reason,
		MAX(table1.contract_duration) contract_duration,
		COUNT(DISTINCT table1.opportunityid) as value
	
	FROM
		
		(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
		         -- It's the last day on which they are making money
			LEFT(oo.locale__c, 2) as country,
			oo.locale__c,
			o.delivery_area__c,
			o.opportunityid,
			oo.name,
			ooooo.potential as money,
			CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END as pricing,
			'last_order' as type_date,
			MIN(o.effectivedate) as date_start,
			MAX(o.effectivedate) as date_churn,
			oooo.confirmed_end__c as confirmed_end,
			MIN(oo.contract_when_accepted__c) as contract_when_accepted__c,
			MIN(oo.closedate) as closedate,
			oo.churn_reason__c,
			CASE WHEN oooo.grand_total__c = '0' THEN 'irregular' ELSE
			(CASE 
				-- duration: unlimited
				WHEN oooo.additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
				-- duration: 12 month
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
				-- duration: 6 month
				WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
				-- duration: 3 month
				WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
				-- duration: 1 month
				WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
				ELSE (CASE WHEN oooo.duration__c IS NULL THEN '1' ELSE oooo.duration__c END)END) END as contract_duration
			
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON 
		
			o.opportunityid = oo.sfid
			
		LEFT JOIN
		
			bi.b2borders ooo
			
		ON
		
			o.opportunityid = ooo.opportunity_id
			
		LEFT JOIN
		
			salesforce.contract__c oooo
		
		ON
		
			o.opportunityid = oooo.opportunity__c

		LEFT JOIN

			bi.potential_revenue_per_opp ooooo

		ON

			o.opportunityid = ooooo.opportunityid
			
		WHERE
		
			o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
			AND oo.status__c IN ('RESIGNED', 'CANCELLED')
			-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
			AND oo.test__c IS FALSE
			AND o.test__c IS FALSE
			AND o.professional__c IS NOT NULL
			AND oooo.service_type__c LIKE 'maintenance cleaning'
			AND oooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND o.effectivedate >= '2018-01-01'
			-- AND oooo.active__c IS TRUE
		
		GROUP BY
		
			LEFT(oo.locale__c, 2),
			oo.locale__c,
			o.delivery_area__c,
			type_date,
			ooooo.potential,
			CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END,
			oo.churn_reason__c,
			o.opportunityid,
			oo.name,
			oooo.confirmed_end__c,
			contract_duration) as table1
			
	GROUP BY
	
		TO_CHAR((table1.confirmed_end)::date,'YYYY-MM'),
		TO_CHAR(table1.date_start,'YYYY-MM'),
		table1.country,
		table1.locale__c,
		table1.delivery_area__c,
		table1.opportunityid,
		table1.name,
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table1.pricing,
		table1.money,
		-- table2.operated_by,
		-- table2.operated_by_detail,
		table1.contract_when_accepted__c,
		table1.churn_reason__c,
		-- table1.contract_duration,
		table1.closedate
		
	ORDER BY
	
		TO_CHAR(table1.confirmed_end::date,'YYYY-MM') desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_new table4
	
ON
	
	table3.opportunityid = table4.opportunity
	
LEFT JOIN

	bi.b2b_additional_booking table5
	
ON 
	
	table3.opportunityid = table5.opportunity
		
LEFT JOIN

	bi.order_provider table6
	
ON
	
	table3.opportunityid = table6.opportunityid
	
LEFT JOIN

	salesforce.invoice__c table7
	
ON

	table3.opportunityid = table7.opportunity__c
	AND table3.year_month = TO_CHAR(table7.issued__c, 'YYYY-MM')
	
GROUP BY

	table3.year_month,
	table3.year_month_start,
	table3.mindate_churn,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.pricing,
	table3.money,
	table3.churn_reason,
	table3.contract_duration,
	table3.value,
	table6.provider,
	table5.acquisition_type,
	table6.company_name
	
ORDER BY

	table3.year_month desc;
	
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: new analysis regarding the churn, new filters on the contracts, the providers, the category of revenue, the cohort... (RUNNING OPPS PART)
-- Created on: 25/10/2018

INSERT INTO bi.deep_churn


SELECT

	table3.year_month,
	table3.year_month_start,
	table3.date,
	-- max_date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,	
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	
	table3.pricing,
	table3.money,
	table3.churn_reason__c,
	table3.contract_duration,
	
	table3.value,
		
	CASE WHEN table6.provider IS NULL THEN 'Not matched yet' ELSE table6.provider END as provider,
	table6.company_name,
	CASE WHEN table3.churn_reason__c LIKE '%one off%' THEN 'One-Off'
		  ELSE 'Recurrent'
		  END as acquisition_type,
	MIN(CASE WHEN (table7.name NOT LIKE '%S' AND table7.name NOT LIKE '%G' AND table7.name NOT LIKE '%N') THEN table7.amount__c ELSE 0 END)/1.19 as amount__c

FROM	
	
	(SELECT
	
		year_month,
		year_month_start,
		date,
		-- max_date,
		locale,
		contract_when_accepted__c,
		closedate,
		languages,
		city,
		table2.opportunityid,
		table2.name,
		type,
		kpi,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		
		table2.pricing,
		table2.grand_total as money,
		table2.churn_reason__c,
		table2.contract_duration,
		
		SUM(CASE WHEN table2.category = 'RUNNING' THEN 1 ELSE 0 END) as value
	
	FROM	
		
		(SELECT	
		
			table1.year_month as year_month,
			TO_CHAR(table1.date_start,'YYYY-MM') as year_month_start,
			MIN(table1.ymd) as date,
			table1.ymd_max as max_date,
			table1.contract_when_accepted__c,
			table1.closedate,
			table1.churn_reason__c,
			table1.contract_duration,
			
			table1.country as locale,
			table1.locale__c as languages,
			table1.polygon as city,
			CAST('B2B' as varchar) as type,
			CAST('Running Opps' as varchar) as kpi,
			CAST('Count' as varchar) as sub_kpi_1,	
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END as sub_kpi_2,
			CASE WHEN (table1.date_churn - table1.date_start) < 180 THEN '6 months'
			  WHEN (table1.date_churn - table1.date_start) >= 180 AND (table1.date_churn - table1.date_start) < 360 THEN '12 months'
			  ELSE 'Unlimited'
			  END as sub_kpi_3,
			CASE WHEN table1.grand_total < 250 THEN '<250€'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN '250€-500€'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN '500€-1000€'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE '>1000€'
			  END as sub_kpi_4,
			  	  
			CASE WHEN table1.grand_total < 250 THEN 'Very Small'
			  WHEN table1.grand_total >= 250 AND table1.grand_total < 500 THEN 'Small'
			  WHEN table1.grand_total >= 500 AND table1.grand_total < 1000 THEN 'Medium'
			  WHEN table1.money IS NULL THEN 'Unknown'
			  ELSE 'Key Account'
			  END as sub_kpi_5,
			table1.opportunityid,
			table1.name,
			table1.category,
			table1.pricing,
			table1.grand_total
			-- SUM(CASE WHEN table1.category = 'RUNNING' THEN 1 ELSE 0 END) as value
			
		FROM
			
			(SELECT
			
				t2.*,
				o.status__c as status_now,
				ooooo.potential as grand_total
			
			FROM
				
				(SELECT
					
						time_table.*,
						table_dates.*,
						CASE WHEN (time_table.ymd >= table_dates.year_month_start::date) AND (time_table.ymd <= table_dates.date_churn) THEN 'RUNNING' ELSE 'NOT RUNNING' END as category
					
					FROM	
						
						(SELECT
							
							'1'::integer as key,	
							TO_CHAR(i, 'YYYY-MM') as year_month,
							MIN(i) as ymd,
							MAX(i) as ymd_max
							-- i::date as date 
							
						FROM
						
							generate_series('2017-01-01'::date, current_date::date, '1 day'::interval) i
							
						GROUP BY
						
							key,
							year_month
							-- date
							
						ORDER BY 
						
							year_month desc) as time_table
							
					LEFT JOIN
					
						(SELECT
						
							'1'::integer as key_link,
							t1.country,
							t1.locale__c,
							t1.delivery_area__c as polygon,
							t1.opportunityid,
							t1.name,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01' as year_month_start,
							MIN(CASE WHEN t2.date_churn IS NULL THEN '2099-12-31'::date 
							         ELSE t2.confirmed_end__c
								     END) as date_churn,
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.confirmed_end__c, 'YYYY-MM') || '-01' END as year_month_churn,
							t1.contract_when_accepted__c,
							t1.closedate,
							t1.churn_reason__c,
							MAX(t1.money) as money,
							t1.pricing,
							MAX(t1.contract_duration) as contract_duration
																								
						FROM
							
							((SELECT
			
								LEFT(o.locale__c, 2) as country,
								o.locale__c,
								o.opportunityid,
								oo.name,
								o.delivery_area__c,
								MIN(o.effectivedate) as date_start,
								MIN(oo.contract_when_accepted__c) as contract_when_accepted__c,
								MIN(oo.closedate) as closedate,
								oo.churn_reason__c,
								ooooo.potential as money,
								CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END as pricing,
								CASE WHEN oooo.grand_total__c = '0' THEN 'irregular' ELSE
								(CASE 
									-- duration: unlimited
									WHEN oooo.additional_agreements__c LIKE ('%unbestimmte_Zeit_geschlossen%') THEN '1'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_unbegrenzt%') THEN '1'
									-- duration: 12 month
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:__12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%lauftzeit%12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit_:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_12_Monate%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_von_mindestens_12_Monaten%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Dauer:_12%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit%sondern_12_Monate%') THEN '12'
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit_beträgt_zwölf_Monate%') THEN '12'
									-- duration: 6 month
									WHEN oooo.additional_agreements__c LIKE ('%Laufzeit:_6%') THEN '6'
									-- duration: 3 month
									WHEN oooo.additional_agreements__c LIKE ('%laufzeit_beträgt_3_Monate%') THEN '3'
									-- duration: 1 month
									WHEN oooo.additional_agreements__c LIKE ('%Vertragslaufzeit:_1%') THEN '1'
									ELSE (CASE WHEN oooo.duration__c IS NULL THEN '1' ELSE oooo.duration__c END)END) END as contract_duration
							
							FROM
							
								salesforce.order o	
								
							LEFT JOIN
			
								salesforce.opportunity oo
				
							ON 
			
								o.opportunityid = oo.sfid
								
							LEFT JOIN
			
								salesforce.contract__c oooo
							
							ON
							
								o.opportunityid = oooo.opportunity__c

							LEFT JOIN

								bi.potential_revenue_per_opp ooooo

							ON

								o.opportunityid = ooooo.opportunityid
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								-- AND oooo.status__c IN ('ACCEPTED', 'SIGNED')
								AND o.test__c IS FALSE
								AND oooo.service_type__c LIKE 'maintenance cleaning'
								AND oooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
								AND o.effectivedate >= '2017-01-01'
			                    -- AND oooo.active__c IS TRUE
								
							GROUP BY
							
								o.opportunityid,
								CASE WHEN oo.grand_total__c IS NULL THEN 'Pph based' ELSE 'Grand Total' END,
								oooo.confirmed_end__c,
								oo.name,
								ooooo.potential,
								o.locale__c,
								contract_duration,
								oo.churn_reason__c,
								o.delivery_area__c,
								LEFT(o.locale__c, 2))) as t1
								
						LEFT JOIN
						
							(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
							         -- It's the last day on which they are making money
								LEFT(o.locale__c, 2) as country,
								o.opportunityid,
								'last_order' as type_date,
								MAX(o.effectivedate) as date_churn,
								oooo.confirmed_end__c
								
							FROM
							
								salesforce.order o
								
							LEFT JOIN
							
								salesforce.opportunity oo
								
							ON 
							
								o.opportunityid = oo.sfid
								
							LEFT JOIN
			
								salesforce.contract__c oooo
							
							ON
							
								o.opportunityid = oooo.opportunity__c
								
							WHERE
							
								o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
								AND oo.status__c IN ('RESIGNED', 'CANCELLED')
								AND o.type = 'cleaning-b2b'
								AND o.professional__c IS NOT NULL
								AND o.test__c IS FALSE
								AND oo.test__c IS FALSE
								AND o.effectivedate >= '2017-01-01'
								AND oooo.service_type__c LIKE 'maintenance cleaning'
								AND oooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
							
							GROUP BY
							
								LEFT(o.locale__c, 2),
								oooo.confirmed_end__c,
								type_date,
								o.opportunityid) as t2
								
						ON
						
							t1.opportunityid = t2.opportunityid
							
						GROUP BY
						
							
							t1.country,
							t1.locale__c,
							t1.delivery_area__c,
							t1.opportunityid,
							t1.name,
							t1.date_start,
							TO_CHAR(t1.date_start, 'YYYY-MM') || '-01',
							CASE WHEN t2.date_churn IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.confirmed_end__c, 'YYYY-MM') || '-01' END,
							t1.contract_when_accepted__c,
							t1.closedate,
							t1.churn_reason__c,
							-- t1.money,
							t1.pricing) as table_dates
							
					ON 
					
						time_table.key = table_dates.key_link
						
				) as t2
					
			LEFT JOIN
			
				salesforce.opportunity o
				
			ON
			
				t2.opportunityid = o.sfid

			LEFT JOIN

				bi.potential_revenue_per_opp ooooo

			ON

				t2.opportunityid = ooooo.opportunityid
	
			WHERE 
	
				o.test__c IS FALSE
				
			GROUP BY
			
					ooooo.potential,
					o.status__c,
					t2.key,	
					t2.year_month,
					t2.ymd,
					t2.ymd_max,
					t2.key_link,
					t2.country,
					t2.locale__c,
					t2.polygon,
					t2.opportunityid,
					t2.name,
					t2.date_start,
					t2.year_month_start,
					t2.date_churn,
					t2.year_month_churn,
					t2.category,
					t2.contract_when_accepted__C,
					t2.closedate,
					t2.churn_reason__c,
					t2.money,
					t2.pricing,
					t2.contract_duration				
				) as table1
	
		WHERE
	
			table1.opportunityid IS NOT NULL
				
		GROUP BY
		
			table1.year_month,
			TO_CHAR(table1.date_start,'YYYY-MM'),
			table1.ymd_max,
			table1.contract_when_accepted__c,
			table1.closedate,
			table1.churn_reason__c,
			table1.country,
			LEFT(table1.locale__c, 2),
			table1.locale__c,
			table1.polygon,
			CASE WHEN (ymd_max::date - table1.date_start) < 31 THEN 'M0'
				  WHEN (ymd_max::date - table1.date_start) >= 31 AND (ymd_max::date - table1.date_start) < 62 THEN 'M1'
				  WHEN (ymd_max::date - table1.date_start) >= 62 AND (ymd_max::date - table1.date_start) < 93 THEN 'M2'
				  WHEN (ymd_max::date - table1.date_start) >= 93 AND (ymd_max::date - table1.date_start) < 124 THEN 'M3'
				  WHEN (ymd_max::date - table1.date_start) >= 124 AND (ymd_max::date - table1.date_start) < 155 THEN 'M4'
				  WHEN (ymd_max::date - table1.date_start) >= 155 AND (ymd_max::date - table1.date_start) < 186 THEN 'M5'
				  WHEN (ymd_max::date - table1.date_start) >= 186 AND (ymd_max::date - table1.date_start) < 217 THEN 'M6'
				  WHEN (ymd_max::date - table1.date_start) >= 217 AND (ymd_max::date - table1.date_start) < 248 THEN 'M7'
				  WHEN (ymd_max::date - table1.date_start) >= 248 AND (ymd_max::date - table1.date_start) < 279 THEN 'M8'
				  WHEN (ymd_max::date - table1.date_start) >= 279 AND (ymd_max::date - table1.date_start) < 310 THEN 'M9'
				  WHEN (ymd_max::date - table1.date_start) >= 310 AND (ymd_max::date - table1.date_start) < 341 THEN 'M10'
				  WHEN (ymd_max::date - table1.date_start) >= 341 AND (ymd_max::date - table1.date_start) < 372 THEN 'M11'
				  WHEN (ymd_max::date - table1.date_start) >= 372 AND (ymd_max::date - table1.date_start) < 403 THEN 'M12'
				  WHEN (ymd_max::date - table1.date_start) >= 403 AND (ymd_max::date - table1.date_start) < 434 THEN 'M13'
				  WHEN (ymd_max::date - table1.date_start) >= 434 AND (ymd_max::date - table1.date_start) < 465 THEN 'M14'
				  WHEN (ymd_max::date - table1.date_start) >= 465 AND (ymd_max::date - table1.date_start) < 496 THEN 'M15'
				  WHEN (ymd_max::date - table1.date_start) >= 496 AND (ymd_max::date - table1.date_start) < 527 THEN 'M16'
				  WHEN (ymd_max::date - table1.date_start) >= 527 AND (ymd_max::date - table1.date_start) < 558 THEN 'M17'
				  ELSE '>M18'
				  END,
				  sub_kpi_3,
			sub_kpi_4,
			sub_kpi_5,
			table1.category,
			table1.opportunityid,
			table1.name,
			table1.pricing,
			table1.grand_total,
			table1.contract_duration
			
		ORDER BY
		
			table1.year_month desc)	 as table2
			
	GROUP BY
	
		year_month,
		year_month_start,
		date,
		-- max_date,
		locale,
		contract_when_accepted__c,
		closedate,
		churn_reason__c,
		languages,
		city,
		type,
		kpi,
		opportunityid,
		name,
		sub_kpi_1,	
		sub_kpi_2,
		sub_kpi_3,
		sub_kpi_4,
		sub_kpi_5,
		table2.pricing,
		table2.grand_total,
		table2.contract_duration
		
	ORDER BY 
	
		year_month desc) as table3
		
LEFT JOIN

	bi.opportunity_traffic_light_new table4
	
ON
	
	table3.opportunityid = table4.opportunity
	
LEFT JOIN

	bi.b2b_additional_booking table5
	
ON 
	
	table3.opportunityid = table5.opportunity
	
LEFT JOIN

	bi.order_provider table6
	
ON
	
	table3.opportunityid = table6.opportunityid
	
LEFT JOIN

	salesforce.invoice__c table7
	
ON

	table3.opportunityid = table7.opportunity__c
	AND table3.year_month = TO_CHAR(table7.issued__c, 'YYYY-MM')
	
WHERE

	table3.value = 1
	-- AND table3.year_month = '2018-09'
	-- AND table3.locale = 'de'
	
GROUP BY

	table3.year_month,
	table3.year_month_start,
	table3.date,
	table3.locale,
	table3.contract_when_accepted__c,
	table3.closedate,
	table3.languages,
	table3.city,
	table3.opportunityid,
	table3.name,
	table3.type,
	table3.kpi,
	table3.sub_kpi_1,
	table3.sub_kpi_2,
	table3.sub_kpi_3,
	table3.sub_kpi_4,
	table3.sub_kpi_5,
	table3.pricing,
	table3.money,
	table3.churn_reason__c,
	table3.contract_duration,
	table3.value,
	table6.provider,
	table5.acquisition_type,
	table6.company_name
	
ORDER BY

	table3.year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Author: Christina Janson
-- Short Description: latest valid (INVOICED,FULFILLED or CANCELLED CUSTOMER) order and information about provider (BAT or Partner)
-- Created on: 17/10/2018

DROP TABLE IF EXISTS 		bi.order_provider;
CREATE TABLE 				bi.order_provider AS 


SELECT DISTINCT ON 	(orders.opportunityid)
			orders.delivery_area__c
			, orders.sfid AS orderid
			, orders.opportunityid
			, opp.name as opportunity
			, orders.effectivedate
			, orders.professional__c
			, prof.pro_name
			, prof.provider
			, prof.company_name
			, prof.company_id as sfid_partner

FROM 		salesforce.order orders

LEFT JOIN 	salesforce.opportunity opp 					ON ( orders.opportunityid = opp.sfid)
LEFT JOIN 	( SELECT 
					pro.sfid															pro_id
					, pro.name															pro_name
					, pro.company_name__c												pro_company_name
					, company.sfid														company_id
					, company.name														company_name
					, CASE 	WHEN company.sfid 	= '0012000001TDMgGAAX' 	THEN 'BAT'
							WHEN company.sfid 	IS NULL					THEN 'unknown'
							ELSE 'Partner' 								END 			provider
					-- ,*
			FROM 		salesforce.account pro
			LEFT JOIN 	salesforce.account company		ON (pro.parentid = company.sfid)
			) AS prof									ON (prof.pro_id = orders.professional__c)

WHERE 		orders.status IN ('INVOICED','FULFILLED','PENDING TO START','NOSHOW CUSTOMER')

ORDER BY 	orders.opportunityid, 
			orders.effectivedate DESC
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains the data concerning the profitability of the BAT cleaners on each opportunity served
-- Created on: 08/11/2018

DROP TABLE IF EXISTS bi.profitability_BAT_opps;
CREATE TABLE bi.profitability_BAT_opps AS 

SELECT

	TO_CHAR(o.issued__c, 'YYYY-MM') as year_month,
	MIN(o.issued__c) as mindate,
	LEFT(o.locale__c, 2) as locale,
	o.locale__c as languages,
	oo.delivery_area__c as city,
	'B2B' as type,
	'Revenue' as kpi,
	'BAT' as sub_kpi_1,
	oo.company_name as sub_kpi_2,
	oo.opportunity as sub_kpi_3,
	oo.professional__c,
	oo.pro_name,
	SUM(ooo.order_duration__c) as hours,
	SUM(ooo.order_duration__c)*12.9 as cost_cleaner,
	MAX(o.amount__c)/1.19 as revenue,
	(MAX(o.amount__c)/1.19)/SUM(ooo.order_duration__c) revenue_per_hour,
	CASE WHEN MAX(o.amount__c)/1.19 > 0 THEN
				(MAX(o.amount__c)/1.19 - SUM(ooo.order_duration__c)*12.9)/MAX(o.amount__c)/1.19
		  ELSE 0
		  END as gpm
	
FROM

	salesforce.invoice__c o
	
LEFT JOIN

	bi.order_provider oo
	
ON

	o.opportunity__c = oo.opportunityid
	
LEFT JOIN

	salesforce.order ooo
	
ON

	o.opportunity__c = ooo.opportunityid
	AND TO_CHAR(o.issued__c, 'YYYY-MM') = TO_CHAR(ooo.effectivedate, 'YYYY-MM')
	
WHERE

	oo.provider = 'BAT'
	AND ooo.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START')
	AND ooo.effectivedate <= current_date
	AND ooo.type = 'cleaning-b2b'
	AND ooo.effectivedate >= '2018-01-01'
	
GROUP BY

	year_month,
	LEFT(o.locale__c, 2),
	o.locale__c,
	oo.delivery_area__c,
	oo.company_name,
	oo.opportunity,
	oo.professional__c,
	oo.pro_name
	
ORDER BY

	year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains a reduced amount of information necessary to the calculation of the CVR from lead to opportunity (filter on working days possible)
-- Created on: 21/11/2018

DROP TABLE IF EXISTS bi.cvr_leads;
CREATE TABLE bi.cvr_leads AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as date,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.createddate,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN o.opportunity__c IS NULL THEN 'No' ELSE 'Yes' END as converted_in_opportunity,
	ooo.stagename as stage_opp,
	o.lost_reason__c,
	o.locale__C,
	LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) as city,
	o.contact_name__c,
	o.acquisition_channel__c,
	o.acquisition_tracking_id__c,
	lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) as keyword,
	t1.spending,
	o.customer__c,
	o.stage__c,
	o.type__c,
	o.company_name__c,
	o.opportunity__c,
	o.ownerid,
	u.name as owner_name,
	o.total_calls__c,
	o.delivery_area__c,
	o.grand_total__c
	
FROM

	salesforce.likeli__c o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.opportunity__c = ooo.sfid

LEFT JOIN

	salesforce.user u
	
ON

	o.ownerid = u.sfid
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.start_date, 'YYYY-MM') as year_month,
		o.sub_kpi_3 as keyword,
		o.city,
		SUM(o.value) as spending
		
		
	
	FROM
	
		public.marketing_spending o
		
	GROUP BY
	
		year_month,
		city,
		keyword
		
	ORDER BY
	
		year_month desc,
		keyword) as t1

ON

	TO_CHAR(o.createddate, 'YYYY-MM') = t1.year_month
	AND lower(substring(replace(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'tpc:(.[^\,]+)')) = t1.keyword
	AND LOWER(SUBSTRING(REPLACE(regexp_replace(o.acquisition_channel_params__c, '[\"\{\}]','','g'), ' ','') from 'cty:(.[^\,]+)')) = t1.city
	
WHERE

	o.type__c = 'B2B'
	AND o.acquisition_channel__c IN ('inbound', 'web')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.test__c IS FALSE
	AND (o.acquisition_tracking_id__c NOT LIKE '%raffle%' OR o.acquisition_tracking_id__c NOT LIKE '%news%' OR o.acquisition_tracking_id__c IS NULL)
	-- AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
	AND (o.acquisition_channel__c NOT LIKE 'outbound' OR o.acquisition_channel__c IS NULL)
	AND (o.company_name__c NOT LIKE '%test%' OR o.company_name__c IS NULL OR o.company_name__c NOT LIKE '%bookatiger%')
	AND (o.email__c NOT LIKE '%bookatiger%' OR o.email__c IS NULL)
	
ORDER BY

	date desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains the info concerning the opportunities created every working day
-- Created on: 22/11/2018

DROP TABLE IF EXISTS bi.cvr_opps;
CREATE TABLE bi.cvr_opps AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date as createddate,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END as converted_in_opportunity,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c as closed_by_id,
	u.name as owner_name,
	o.delivery_area__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.createddate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.sfid = ooo.sfid
	
LEFT JOIN

	salesforce.likeli__C oooo
	
ON

	o.sfid = oooo.opportunity__c
	
LEFT JOIN

	salesforce.user u
	
ON

	o.closed_by__c = u.sfid
	
WHERE

	o.test__c IS FALSE
	AND oooo.acquisition_channel__c IN ('web', 'inbound')
	AND LEFT(o.locale__c, 2) = 'de'
	
GROUP BY

	year_month,
	o.createddate,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	o.acquisition_channel__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c,
	u.name,
	o.delivery_area__c
	
ORDER BY

	TO_CHAR(o.createddate, 'YYYY-MM-DD') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains the data concerning the opportunities closed on every working day
-- Created on: 26/11/2018

DROP TABLE IF EXISTS bi.closed_opps;
CREATE TABLE bi.closed_opps AS 

SELECT

	TO_CHAR(o.closedate, 'YYYY-MM') as year_month,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END as validity,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END as relation,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END as converted_in_opportunity,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c as closed_by_id,
	u.name as owner_name,
	o.delivery_area__c
	
FROM

	salesforce.opportunity o
	
LEFT JOIN

	bi.working_days_monthly oo
	
ON

	TO_CHAR(o.closedate, 'YYYY-MM-DD')::date = oo.date
	
LEFT JOIN

	salesforce.opportunity ooo
	
ON

	o.sfid = ooo.sfid
	
LEFT JOIN

	salesforce.likeli__C oooo
	
ON

	o.sfid = oooo.opportunity__c
	
LEFT JOIN

	salesforce.user u
	
ON

	o.closed_by__c = u.sfid
	
WHERE

	o.test__c IS FALSE
	AND o.acquisition_channel__c IN ('web', 'inbound')
	AND LEFT(o.locale__c, 2) = 'de'
	AND o.closedate <= current_date
	AND o.stagename NOT LIKE '%LOST%'
	AND o.stagename IN ('WRITTEN CONFIRMATION', 'WON','PENDING')
	AND o.closed_by__c IS NOT NULL
	
GROUP BY

	year_month,
	o.closedate,
	oo.working_day_number,
	oo.non_working_day_number,
	oo.day_type,
	oo.day_type_number,
	oo.number_working_days_in_month,
	o.sfid,
	CASE WHEN o.lost_reason__c LIKE 'invalid%'
	          OR o.lost_reason__c LIKE 'not suitable%' THEN 'Invalid'
	     ELSE 
	     	    'Valid'
	     END,
	CASE WHEN o.direct_relation__c IS FALSE THEN 'Qualified' ELSE 'Not Qualified' END,
	CASE WHEN (o.stagename = 'PENDING' OR o.stagename = 'WON') THEN 'Yes' ELSE 'No' END,
	ooo.stagename,
	o.lost_reason__c,
	o.locale__C,
	o.contact_name__c,
	o.acquisition_channel__c,
	oooo.acquisition_channel__c,
	o.sector__c,
	o.customer__c,
	o.name,
	o.grand_total__c,
	o.closed_by__c,
	u.name,
	o.delivery_area__c
	
ORDER BY

	TO_CHAR(o.closedate, 'YYYY-MM-DD') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table contains gives insights on the efficiency of the keywords used in marketing (CVR%, spendings...)
-- Created on: 27/11/2018

DROP TABLE IF EXISTS bi.keywords_efficiency;
CREATE TABLE bi.keywords_efficiency AS 

SELECT

	o.year_month,
	MIN(o.date) as mindate,
	o.locale__c,
	o.city,
	o.keyword,
	COUNT(DISTINCT o.sfid) as leads_created,
	SUM(CASE WHEN o.converted_in_opportunity = 'Yes' THEN 1 ELSE 0 END) as opps_created,
	SUM(CASE WHEN o.stage__c IN ('ENDED') THEN 1 ELSE 0 END) as lead_ended,
	SUM(CASE WHEN o.stage__c NOT IN ('ENDED', 'WON', 'PENDING') THEN 1 ELSE 0 END) as pipeline,
	SUM(CASE WHEN o.stage_opp IN ('WON', 'PENDING') THEN 1 ELSE 0 END) as customer_converted,
	MAX(o.spending) as spending,
	SUM(o.grand_total__c) as grand_total

FROM

	bi.cvr_leads o
	
GROUP BY

	o.year_month,
	o.locale__c,
	o.city,
	o.keyword
	
ORDER BY
	
	year_month desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: this table gives insights concerning the retention efficiency
-- Created on: 28/11/2018

DROP TABLE IF EXISTS bi.retention_efficiency;
CREATE TABLE bi.retention_efficiency AS 

SELECT
	
	DISTINCT o.opportunityid,
	MAX(o.createddate)::date as retention_date,
	oo.name,
	TRUNC(t1.potential::numeric, 2) as potential,
	CASE WHEN t1.potential < 250 THEN 'Very Small'
			  WHEN t1.potential >= 250 AND t1.potential < 500 THEN 'Small'
			  WHEN t1.potential >= 500 AND t1.potential < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END,
	oo.delivery_area__c,
	ooo.provider,
	ooo.company_name,
	ooo.pro_name,
	oo.status__c,
	oo.churn_reason__c,
	MAX(t2.resignation_date__c)::date as resignation_date,
	MAX(t2.confirmed_end__c)::date as confirmed_end

FROM

	salesforce.opportunityfieldhistory o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunityid = oo.sfid
	
LEFT JOIN

	bi.order_provider ooo
	
ON

	o.opportunityid = ooo.opportunityid
	
LEFT JOIN

	bi.potential_revenue_per_opp t1
	
ON 

	o.opportunityid = t1.opportunityid
	
LEFT JOIN

	salesforce.contract__c t2
	
ON

	o.opportunityid = t2.opportunity__c
	
-- LEFT JOIN

	-- salesforce.natterbox_call_reporting_object__c t3
	
-- ON

	-- o.opportunityid = t3.opportunity__c
	
WHERE

	o.newvalue = 'RETENTION'
	AND oo.test__c IS FALSE
	AND t2.service_type__c LIKE 'maintenance cleaning'
	-- AND t2.active__c IS TRUE
	-- AND t3.calldirection__c = 'Outbound'
	-- AND t3.callerlastname__c = 'Kharoo'
	-- AND t3.createddate >= o.createddate
	
GROUP BY

	o.opportunityid,
	oo.name,
	t1.potential,
	CASE WHEN t1.potential < 250 THEN 'Very Small'
			  WHEN t1.potential >= 250 AND t1.potential < 500 THEN 'Small'
			  WHEN t1.potential >= 500 AND t1.potential < 1000 THEN 'Medium'
			  ELSE 'Key Account'
			  END,
	oo.delivery_area__c,
	ooo.provider,
	ooo.company_name,
	ooo.pro_name,
	oo.status__c,
	oo.churn_reason__c
	
ORDER BY

	retention_date desc;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Christina Janson
-- Short Description: Opportunity invoice forecast details for the invoice check done by sales
-- Created on: 28/11/2018

DELETE 	FROM 		bi.opportunity_invoice_check 		WHERE TO_CHAR(((date - interval '1day')::date), 'YYYY-MM') = TO_CHAR(((CURRENT_DATE - interval '1day')::date), 'YYYY-MM');
INSERT INTO  		bi.opportunity_invoice_check   
  
  SELECT  CURRENT_DATE						AS date
  		  , TO_CHAR(((CURRENT_DATE - interval '1day')::date), 'YYYY-MM')  AS year_month
  		  , o.sfid                          AS OpportunityId
          , o.customer__c                   AS customerId
          , o.Name
          , o.StageName
          , o.status__c                     AS status
          , o.supplies__c                   AS supplies		
          , o.grand_total__c
          , o.CloseDate
          , ooo.first_order_date
          , (CASE   WHEN  EXTRACT (month  	FROM ooo.first_order_date::date) = EXTRACT (month   FROM (CURRENT_DATE - interval '1day')::date) 
                    AND  (EXTRACT (year  	FROM ooo.first_order_date::date) = EXTRACT (year   	FROM (CURRENT_DATE - interval '1day')::date)  ) THEN 1 ELSE 0 END)  AS FirstMonth
          , oo.first_order_date_monthly
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- orders
          , oo.orders AS orders
          , (CASE   WHEN oooo.orders_cancelled                > 0   THEN oooo.orders_cancelled                ELSE 0 END)             AS Orders_Cancelled
          , (CASE   WHEN ooooo.Orders_cancelled_customer      > 0   THEN ooooo.Orders_cancelled_customer      ELSE 0 END)             AS Orders_Cancelled_Customer
          , (CASE   WHEN oooo.Orders_Cancelled_Professional   > 0   THEN oooo.Orders_Cancelled_Professional   ELSE 0 END)             AS Orders_Cancelled_Professional
          , (CASE   WHEN oooooooo.Orders_Noshow_Professional  > 0   THEN oooooooo.Orders_Noshow_Professional  ELSE 0 END)             AS Orders_Noshow_Professional
          , (CASE   WHEN oooo.Orders_Cancelled_Mistake        > 0   THEN oooo.Orders_Cancelled_Mistake        ELSE 0 END)             AS Orders_Cancelled_Mistake
          , (CASE   WHEN ooooooooo.orders_bridging_day        > 0   THEN ooooooooo.orders_bridging_day        ELSE 0 END)             AS Orders_Bridging_Day
          , (CASE   WHEN oooo.Orders_Cancelled_Terminated     > 0   THEN oooo.Orders_Cancelled_Terminated     ELSE 0 END)             AS Orders_Cancelled_Terminated	
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- hours
          , o.hours_weekly__c                 AS weekly_hours
          , oo.executed_hours                 AS executed_hours
          , o.hours_weekly__c*oo.CW           AS max_monthly_hours
          , oooo.cancelled_Professional_hours
          , oooo.cancelled_Mistake_hours
          , oooo.cancelled_Terminated_hours
          , ooooo.cancelled_customer_hours    AS cancelled_customer_hours
          , ooooooo.holiday_mistake_hours     AS holiday_mistake_hours
          , oooooooo.Noshow_Professional_hours
          , (CASE   WHEN o.hours_weekly__c                    > 0   THEN (o.grand_total__c/o.hours_weekly__c/4.33) ELSE 0 END)        	AS PPH
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE 		
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE A - executed hours = max hours	
        , (CASE WHEN oo.executed_hours                    		= (o.hours_weekly__c*oo.CW)   					THEN 1 ELSE 0 END)      AS fine_A
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE B - executed hours + CANCELLED CUSTOMER = max hours	
  		, (CASE WHEN (oo.executed_hours + ooooo.cancelled_customer_hours) = (o.hours_weekly__c*oo.CW) 			THEN 1 ELSE 0 END) 		AS fine_B
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE C - first month	
  		, (CASE WHEN 	EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (month 	FROM ooo.first_order_date::date)
  				AND 	EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (year 	FROM ooo.first_order_date::date) THEN 1 ELSE 0 END) AS fine_C
  	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE D - executed hours < max hours AND no cancelled Orders		
  		, (CASE WHEN ((oo.executed_hours 						< (o.hours_weekly__c*oo.CW)) 
  				AND ((CASE WHEN oooo.orders_cancelled 			> 0 THEN oooo.orders_cancelled ELSE 0 END) = 0)) THEN 1 ELSE 0 END)		 AS fine_D
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE E - executed hours < max hours AND CANCELLED CUSTOMER		
  		, (CASE WHEN ((oo.executed_hours 						< (o.hours_weekly__c*oo.CW)) 
  				AND ((CASE WHEN ooooo.Orders_cancelled_customer > 0 THEN ooooo.Orders_cancelled_customer ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS fine_E
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE G - executed hours = max hours AND Public Holiday = CANCELLED MISTAKE
  		, (CASE WHEN ((oo.executed_hours 						= (o.hours_weekly__c*oo.CW)) 
  				AND ((CASE WHEN ooooooo.orders_holiday_mistake  > 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS fine_G
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- FINE H - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE
  		, (CASE WHEN (o.hours_weekly__c*oo.CW) 					= oo.executed_hours  
  		+ (CASE WHEN ooooooo.holiday_mistake_hours 				> 0 THEN ooooooo.holiday_mistake_hours 	ELSE 0 END) 
  				AND ((CASE WHEN ooooooo.orders_holiday_mistake 	> 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)THEN 1 ELSE 0 END) AS fine_H
  	  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW 		
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW first month	
  		, (CASE WHEN 	EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (month 	FROM ooo.first_order_date::date)
  				AND 	EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date) = EXTRACT (year 	FROM ooo.first_order_date::date) THEN 1 ELSE 0 END) AS Review_firstmonth
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW h
  		, (CASE WHEN o.hours_weekly__c 							> 0 											THEN 0 ELSE 1 END) 		AS Review_h
	    	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW executed hours > max hours
  		, (CASE WHEN oo.executed_hours 							> (o.hours_weekly__c*oo.CW) 					THEN 1 ELSE 0 END) 		AS Review_extrabooking
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW public holidays
  		, (CASE WHEN (CASE WHEN oooooo.orders_holiday 			> 0 THEN oooooo.orders_holiday ELSE 0 END) > 0 	THEN 1 ELSE 0 END) 		AS Review_public_holidays
  	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- REVIEW max hours > executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day		
  		, (CASE WHEN ((o.hours_weekly__c*oo.CW) 				> (oo.executed_hours  
  		+ (CASE WHEN ooooooo.holiday_mistake_hours 				> 0 THEN ooooooo.holiday_mistake_hours 	ELSE 0 END)
  		+ (CASE WHEN ooooooooo.holiday_bridging_day 			> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END))
  			AND (CASE WHEN ooooooooo.holiday_bridging_day 		> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END) > 0 )  THEN 1 ELSE 0 END) AS Review_Bridging_days_h
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - executed hours < max hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day		
  		, (CASE WHEN ((oo.executed_hours 						< (o.hours_weekly__c*oo.CW)) 
  			AND ((CASE WHEN ooooooo.orders_holiday_mistake 		> 0 THEN ooooooo.orders_holiday_mistake ELSE 0 END) > 0)
  			AND ((CASE WHEN ooooooooo.orders_bridging_day 		> 0 THEN ooooooooo.orders_bridging_day 	ELSE 0 END) > 0)) THEN 1 ELSE 0 END) AS REVIEW_Bridging_day
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- Review - max hours = executed hours AND Public Holiday = CANCELLED MISTAKE AND Bridging day		
  		, (CASE WHEN ((o.hours_weekly__c*oo.CW) 				= (oo.executed_hours  
  		+ (CASE WHEN ooooooo.holiday_mistake_hours 				> 0 THEN ooooooo.holiday_mistake_hours 	ELSE 0 END)
  		+ (CASE WHEN ooooooooo.holiday_bridging_day 			> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END))
  			AND (CASE WHEN ooooooooo.holiday_bridging_day 		> 0 THEN ooooooooo.holiday_bridging_day ELSE 0 END) > 0)  THEN 1 ELSE 0 END) AS Review_incl_Bridging_day_h
  	
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  FROM 			salesforce.opportunity o 
  	
  	INNER JOIN 	(SELECT 	basic.opportunityid									AS opportunityid
							, basic.first_order_date_monthly					AS first_order_date_monthly
							, basic.orders										AS orders
							, basic.executed_hours								AS executed_hours
							, SUM (CASE WHEN TO_CHAR(basic.first_order_date_monthly, 'day') = weekday 	THEN 1 
																										ELSE 0 END) AS CW
				
				FROM 	 	(SELECT t1.opportunityid							AS opportunityid
					  				, COUNT 	(sfid) 							AS orders
						  			, SUM 		(order_duration__c) 			AS executed_hours
						  			, MIN 		(effectivedate::date) 			AS first_order_date_monthly
						  			
							FROM 	salesforce."order" 		t1			  		
							
							WHERE 	status 		IN ('PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
									AND EXTRACT (month 	FROM effectivedate::date) 	= EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
									AND EXTRACT (year 	FROM effectivedate::date) 	= EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
								  						
							GROUP BY t1.opportunityid ) 	AS basic
				
				LEFT JOIN 	bi.working_days_monthly 		days 				ON (TO_CHAR(basic.first_order_date_monthly, 'YYYY-MM') = days.year_month)
				
				GROUP BY 	basic.opportunityid
							, basic.first_order_date_monthly
							, basic.orders
							, basic.executed_hours) 		AS oo				ON o.sfid = oo.opportunityid				
  
  
  	INNER JOIN 	bi.b2borders 								ooo 				ON o.sfid = ooo.opportunity_id
  	
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED ORDERS without CANCELLED CUSTOMER	
  	LEFT JOIN 	(SELECT 	opportunityid
				  			, COUNT (sfid) 											AS Orders_cancelled
				  			, SUM(CASE WHEN Status LIKE 'CANCELLED PROFESSIONAL' 	THEN 1 					ELSE 0 END) AS Orders_Cancelled_Professional
				  			, SUM(CASE WHEN Status LIKE 'CANCELLED MISTAKE' 		THEN 1 					ELSE 0 END) AS Orders_Cancelled_Mistake
				  			, SUM(CASE WHEN Status LIKE 'CANCELLED TERMINATED' 		THEN 1 					ELSE 0 END) AS Orders_Cancelled_Terminated
							, SUM(CASE WHEN Status LIKE 'CANCELLED PROFESSIONAL' 	THEN order_duration__c 	ELSE 0 END) AS Cancelled_Professional_hours
							, SUM(CASE WHEN Status LIKE 'CANCELLED MISTAKE' 		THEN order_duration__c 	ELSE 0 END) AS Cancelled_Mistake_hours
							, SUM(CASE WHEN Status LIKE 'CANCELLED TERMINATED' 		THEN order_duration__c 	ELSE 0 END) AS Cancelled_Terminated_hours

  				FROM 		salesforce."order" 			t2
  				
  				WHERE 		status 		LIKE 		'%CANCELLED%'
  							AND status 	NOT LIKE 	'CANCELLED CUSTOMER'
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  							
  				GROUP BY 	opportunityid) 					AS oooo 			ON o.sfid = oooo.opportunityid
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- NOSHOW PROFESSIONAL	
  	LEFT JOIN 	(SELECT 	opportunityid
  							, SUM(CASE WHEN Status LIKE 'NOSHOW PROFESSIONAL' 		THEN 1 					ELSE 0 END) AS Orders_Noshow_Professional
							, SUM(CASE WHEN Status LIKE 'NOSHOW PROFESSIONAL' 		THEN order_duration__c 	ELSE 0 END) AS Noshow_Professional_hours
  			
  				FROM 		salesforce."order" 			t22
  				
  				WHERE 		status 			LIKE 		'NOSHOW PROFESSIONAL'
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  				
  				GROUP BY 	opportunityid) 					AS oooooooo 		ON o.sfid = oooooooo.opportunityid
  		
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- CANCELLED CUSTOMER ORDERS	
  	LEFT JOIN 	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS Orders_cancelled_customer
  							, SUM 	(order_duration__c) 							AS cancelled_customer_hours
  		
  				FROM 		salesforce."order" 			t3
  				
  				WHERE 		status 			LIKE 		'CANCELLED CUSTOMER'
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  				
  				GROUP BY  	opportunityid) 					AS ooooo 			ON o.sfid = ooooo.opportunityid	
   
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- 		
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- 
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAYS	w/o CANCELLED MISTAKE	
  	LEFT JOIN 	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS orders_holiday
  							, SUM 	(order_duration__c) 							AS holiday_hours
  							, MIN 	(effectivedate::date) 							AS first_order_date_monthly
  			
  				FROM 		salesforce."order" t4
  		
  				WHERE 	(	status 			IN (		'PENDING TO START','FULFILLED', 'NOSHOW CUSTOMER', 'INVOICED')
  							OR (status 		LIKE 		'%CANCELLED%' 		
  								AND status 	NOT LIKE 	'CANCELLED MISTAKE'))
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND effectivedate::date IN ('2018-01-01', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03') -- should be replaced by an JOIN with the working day table
  				
  				GROUP BY 	opportunityid) 					AS oooooo			ON o.sfid = oooooo.opportunityid			
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- PUBLIC HOLIDAY is CANCELLED MISTAKE	
  	LEFT JOIN	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS orders_holiday_mistake
  							, SUM 	(order_duration__c) 							AS holiday_mistake_hours
  							, MIN 	(effectivedate::date) 							AS first_order_date_monthly
  			
  				FROM  		salesforce."order" 			t5
  		
  				WHERE 		status 			IN 			('CANCELLED MISTAKE')
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (month 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year 	FROM (CURRENT_DATE - interval '1day')::date)
  							AND effectivedate::date IN ('2018-01-01', '2018-05-01', '2018-05-10', '2018-05-21', '2018-10-03') -- should be replaced by an JOIN with the working day table
  		GROUP BY
  			opportunityid) AS ooooooo	
  		ON o.sfid = ooooooo.opportunityid	
  
  -- ---------------------------------------------------------------------------------------------------------------------------------------------- BRIDGING DAY is CANCELLED CUSTOMER	
  	LEFT JOIN	(SELECT 	opportunityid
  							, COUNT (sfid) 											AS orders_bridging_day
  							, SUM 	(order_duration__c) 							AS holiday_bridging_day
  
  				FROM 		salesforce."order" t6
  		
  				WHERE 		status 			IN 			('CANCELLED CUSTOMER')
  							AND EXTRACT (month 	FROM effectivedate::date) = EXTRACT (year FROM (CURRENT_DATE - interval '1day')::date)
  							AND EXTRACT (year 	FROM effectivedate::date) = EXTRACT (year FROM (CURRENT_DATE - interval '1day')::date)
  							AND effectivedate::date IN ('2018-05-11')
  		
  				GROUP BY 	opportunityid) 					AS ooooooooo		ON o.sfid = ooooooooo.opportunityid
  		
  		
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  -- ----------------------------------------------------------------------------------------------------------------------------------------------
  	
  WHERE 		o.grand_total__c 	IS NOT NULL
  				AND o.test__c 		IS FALSE
  				-- AND o.stagename IN ('WON')
  
  GROUP BY 		o.sfid
 				, o.customer__c
 				, o.Name
 				, o.StageName
 				, o.status__c
 				, o.supplies__c
				, o.grand_total__c
				, o.CloseDate
				, o.hours_weekly__c
  				, oo.first_order_date_monthly
 				, oo.orders
				, oo.executed_hours
        		, oo.CW
				, ooo.first_order_date
  				, oooo.cancelled_Professional_hours
  				, oooo.cancelled_Mistake_hours
  				, oooo.cancelled_Terminated_hours
  				, oooo.orders_cancelled
 				, oooo.Orders_Cancelled_Professional
 				, oooo.Orders_Cancelled_Mistake
 				, oooo.Orders_Cancelled_Terminated
 				, ooooo.Orders_cancelled_customer
 				, ooooo.cancelled_customer_hours
				, oooooo.orders_holiday
  				, ooooooo.orders_holiday_mistake
 				, ooooooo.holiday_mistake_hours
 				, oooooooo.Orders_Noshow_Professional
 				, oooooooo.Noshow_Professional_hours
 				, ooooooooo.orders_bridging_day
 				, ooooooooo.holiday_bridging_day
;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: all the cases created by the partners via the portal
-- Created on: 04/12/2018

DROP TABLE IF EXISTS bi.partner_portal_cases;
CREATE TABLE bi.partner_portal_cases AS 

SELECT

	TO_CHAR(o.createddate, 'YYYY-MM') as year_month,
	o.createddate,
	o.accountid,
	b.delivery_areas__c,
	a.company_name__c,
	CASE WHEN a.type__c = 'partner' THEN 'None' ELSE a.name END as cleaner,
	a.type__c,
	o.sfid as sfid_case,
	o.reason,
	o.origin,
	o.subject,
	o.description,	
	COUNT(DISTINCT o.sfid) as cases
	
FROM

	salesforce.case o
	
LEFT JOIN

	salesforce.account a
	
ON

	o.accountid = a.sfid

LEFT JOIN

	salesforce.account b
	
ON 

	a.company_name__c = b.name
	
WHERE

	(o.origin = 'Partner portal' OR o.origin LIKE 'Partner - portal%')
	AND LOWER(o.description) NOT LIKE '%test%'
	AND LOWER(o.description) NOT LIKE '%test%'
	AND LOWER(a.name) NOT LIKE '%test%'
	AND a.test__c IS FALSE
	AND b.test__c IS FALSE
	AND a.company_name__c NOT IN ('##')
	
GROUP BY

	TO_CHAR(o.createddate, 'YYYY-MM'),
	o.createddate,
	o.accountid,
	b.delivery_areas__c,
	a.name,
	a.company_name__c,
	a.type__c,
	o.sfid,
	o.reason,
	o.origin,
	o.description,
	o.subject
	
ORDER BY

	TO_CHAR(o.createddate, 'YYYY-MM') desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: table containing the activity of the partners on the portal and the use of the application by the cleaners
-- Created on: 05/12/2018

DROP TABLE IF EXISTS bi.partner_portal_activity;
CREATE TABLE bi.partner_portal_activity AS 		

SELECT

	t4.company_name__c,
	t4.delivery_areas__c,
	t4.parentid,
	t4.sfid_cleaner,
	t4.last_order,
	t4.is_active,
	t4.cleaner_w_app,
	-- t4.*,
	MAX(t5.login) as last_login


FROM	
	
	(SELECT
	
		t2.*,
		CASE WHEN (t2.last_order > (current_date -91) AND t2.last_order <= current_date) THEN 'Yes' ELSE 'No' END as is_active,
		CASE WHEN t3.deviceid__c IS NOT NULL THEN 'Yes' ELSE 'No' END as cleaner_w_app,
		t3.last_login__c
	
	FROM
			
		(SELECT
		
			t1.company_name__c,
			t1.delivery_areas__c,
			t1.parentid,
			t1.sfid as sfid_cleaner,
			t1.last_order
		
		FROM	
			
			(SELECT	
				
				a.sfid,
				a.delivery_areas__c,
				a.billingcity,
				a.shippingcity,
				a.parentid,
				a.company_name__c,
				a.status__c,
				MAX(o.effectivedate) as last_order
			
			FROM
				
				salesforce.account a
				
			LEFT JOIN
			
				salesforce.account b
				
			ON 
			
				a.company_name__c = b.name
				
			LEFT JOIN
			
				salesforce.order o
				
			ON
			
				a.sfid = o.professional__c 
				
			WHERE
				
				LOWER(a.name) NOT LIKE '%test%'
				AND a.test__c IS FALSE
				AND b.test__c IS FALSE
				AND LEFT(a.locale__c, 2) = 'de'
				AND a.company_name__c NOT IN ('##')
				AND a.status__c IN ('BETA', 'ACTIVE', 'FROZEN')
				AND a.parentid IS NOT NULL
				AND o.status IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
				AND o.effectivedate <= current_date
				AND o.effectivedate >= '2018-01-01'
				
			GROUP BY
			
				a.sfid,
				a.delivery_areas__c,
				a.billingcity,
				a.shippingcity,
				a.parentid,
				a.company_name__c,
				a.status__c) as t1
				
		GROUP BY
		
			t1.company_name__c,
			t1.delivery_areas__c,
			t1.parentid,
			t1.last_order,
			t1.sfid) as t2
			
	LEFT JOIN
		
		(SELECT 
		
			parent.sfid, 
			parent.name,
			prof.sfid as sfid_cleaner,
			prof.deviceid__c,
			parent.last_login__c,
			COUNT(prof.sfid) as cleaners_w_app
		
		FROM 
		
			salesforce.account prof
		
		JOIN 
		
			salesforce.account parent 
			
		ON 
		
			prof.parentid = parent.sfid
			AND prof.status__c IN ('BETA', 'ACTIVE', 'FROZEN')
			AND prof.type__c LIKE '%cleaning-b2b%' 
			AND prof.locale__c LIKE 'de-%'
			AND prof.test__c = False 
			AND parent.test__c = False
			AND prof.deviceid__c IS NOT NULL
		
		GROUP BY 
		
			parent.sfid, 
			prof.deviceid__c,
			prof.sfid,
			parent.last_login__c,
			parent.name
			
		ORDER BY
		
			parent.name) as t3
			
	ON
	
		t2.company_name__c = t3.name
		AND t2.sfid_cleaner = t3.sfid_cleaner) as t4
		
LEFT JOIN

	(SELECT 

		name, 
		MAX(last_login__c) as login 
		
	FROM 
	
		salesforce.account 
	
	WHERE 
	
		last_login__c > DATE '2019-01-01' 
		
	GROUP BY 
	
		name 
		
	order by 
	
		login) as t5
		
ON

	t4.company_name__c = t5.name	
		
GROUP BY

	t4.company_name__c,
	t4.delivery_areas__c,
	t4.parentid,
	t4.sfid_cleaner,
	t4.last_order,
	t4.is_active,
	t4.cleaner_w_app;		

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Table containing the dates of the first and last orders of each opportunity
-- Created on: 11/12/2018


DROP TABLE IF EXISTS bi.first_last_orders;
CREATE TABLE bi.first_last_orders AS 

SELECT

	'1'::integer as key_link,
	t1.country,
	t1.locale__c,
	t1.delivery_area__c as polygon,
	t1.opportunityid,
	t1.date_first_order,
	TO_CHAR(t1.date_first_order, 'YYYY-MM') || '-01' as year_month_first_order,
	CASE WHEN t2.last_order IS NULL THEN '2099-12-31'::date ELSE t2.last_order END as date_last_order,
	CASE WHEN t2.last_order IS NULL THEN '2099-12-01' ELSE TO_CHAR(t2.last_order, 'YYYY-MM') || '-01' END as year_month_lat_order
	
FROM
	
	((SELECT

		LEFT(o.locale__c, 2) as country,
		o.locale__c,
		o.opportunityid,
		o.delivery_area__c,
		MIN(o.effectivedate) as date_first_order
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND o.effectivedate >= '2018-01-01'
		
	GROUP BY
	
		o.opportunityid,
		o.locale__c,
		o.delivery_area__c,
		LEFT(o.locale__c, 2))) as t1
		
LEFT JOIN

	(SELECT  -- Here we make a list containing all the opportunities having a ('INVOICED', 'CANCELLED CUSTOMER', 'FULFILLED', 'PENDING TO START') and that are RESIGNED OR CANCELLED, we take the lat order's date 
	         -- It's the last day on which they are making money
		LEFT(o.locale__c, 2) as country,
		o.opportunityid,
		'last_order' as type_date,
		MAX(o.effectivedate) as last_order
		
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON 
	
		o.opportunityid = oo.sfid
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING ALLOCATION')
		AND o.type = 'cleaning-b2b'
		AND o.professional__c IS NOT NULL
		AND o.test__c IS FALSE
		AND oo.test__c IS FALSE
	
	GROUP BY
	
		LEFT(o.locale__c, 2),
		type_date,
		o.opportunityid) as t2
		
ON

	t1.opportunityid = t2.opportunityid;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- commented out on 2019/04/02 by Christina -- fields replaced by new object (suggested_partner)
-- commented IN again by Sylvain afte modifying the query on 17/04/2019

-- Author: Sylvain Vanhuysse
-- Short Description: table containing the number of offers received and accepted for each partner
-- Created on: 21/01/2019

DROP TABLE IF EXISTS bi.offers_acceptance;
CREATE TABLE bi.offers_acceptance as

SELECT

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
	MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
	a.sfid,
	a.name,
	a.company_name__c,
	a.delivery_areas__c,
	a.type__c,
	a.status__c,
	a.role__c,
	COUNT(o.suggested_partner__c) as offers_received,
	SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted


FROM

	salesforce.account a


LEFT JOIN

	salesforce.partner_offer_partner__c o
		
ON

	a.sfid = o.suggested_partner__c 
	
WHERE

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
	AND a.test__c IS FALSE
	
GROUP BY

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
	a.sfid,
	a.name,
	a.company_name__c,
	a.delivery_areas__c,
	a.type__c,
	a.status__c,
	a.role__c

ORDER BY

	TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
	a.name asc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: table containing the number of offers received and accepted for each partner
-- Created on: 23/01/2019

DROP TABLE IF EXISTS bi.churn_forecast;
CREATE TABLE bi.churn_forecast as
	
SELECT

	TO_CHAR(t1.confirmed_end__c,'YYYY-MM') as year_month,
	MIN(t1.confirmed_end__c) as confirmed_end,
	CASE WHEN t1.status__c IN ('RESIGNED', 'CANCELLED') THEN 'CHURNED' ELSE t1.status__C END as status,
	t1.sfid,
	t1.name,
	-- 'Offboarding' as status,
	COUNT(DISTINCT t1.sfid) as number_opps,
	SUM(t1.avg_last3_months) as amount_lost
	-- SUM(CASE WHEN t1.grand_total__c > t1.avg_last3_months THEN t1.grand_total__c ELSE t1.avg_last3_months END) as amount_lost
	
FROM	
	
	(SELECT
	
		o.sfid,
		o.name,
		o.status__c,
		o.stagename,
		o.delivery_area__c,
		o.grand_total__c,
		oo.potential as avg_last3_months,
		ooo.confirmed_end__c
		
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
		
	LEFT JOIN
	
		bi.potential_revenue_per_opp oo
		
	ON
	
		o.sfid = oo.opportunityid
		
	WHERE
	
		o.status__c IN ('OFFBOARDING', 'RETENTION', 'RESIGNED', 'CANCELLED')
		AND o.locale__c LIKE 'de%'
		AND ooo.confirmed_end__c IS NOT NULL
		AND o.test__c IS FALSE
		AND ooo.service_type__c LIKE 'maintenance cleaning'
		-- AND ooo.active__c IS TRUE
		-- We consider that the last valid contract can be RESIGNED, CANCELLED, SIGNED or ACCEPTED
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')) as t1
		
WHERE

	CASE WHEN t1.grand_total__c > t1.avg_last3_months THEN t1.grand_total__c ELSE t1.avg_last3_months END IS NOT NULL
	
	
GROUP BY 

	year_month,
	t1.sfid,
	t1.name,
	CASE WHEN t1.status__c IN ('RESIGNED', 'CANCELLED') THEN 'CHURNED' ELSE t1.status__C END;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Author: Sylvain Vanhuysse
-- Short Description: new analysis regarding the churn, new filters on the contracts, the providers, the category of revenue, the cohort... (CHURNED OPPS PART)
-- Created on: 05/02/2019

DROP TABLE IF EXISTS bi.waiting_time_offers_partners;
CREATE TABLE bi.waiting_time_offers_partners as


SELECT

	o.createddate,
	o.sfid as sfid_offer,
	o.status__c,
	o.opportunity__c,
	oo.name,
	oo.grand_total__c,
	CASE WHEN oo.grand_total__c < 250 THEN 'Very Small'
		  WHEN oo.grand_total__c >= 250 AND oo.grand_total__c < 500 THEN 'Small'
		  WHEN oo.grand_total__c >= 500 AND oo.grand_total__c < 1000 THEN 'Medium'
		  WHEN oo.grand_total__c IS NULL THEN 'Unknown'
		  ELSE 'Key Account'
		  END as type_customer,
	oo.hours_weekly__c,
	oo.times_per_week__c,
	oo.office_bathrooms__c,
	oo.office_employees__c,
	oo.office_rooms__c,
	oo.office_kitchens__c,
	oo.office_bathrooms__c + oo.office_rooms__c + oo.office_kitchens__c as all_rooms,
	
	CASE WHEN oo.office_size__c <= 100 THEN '<= 100'
	     WHEN (oo.office_size__c > 100  AND oo.office_size__c <= 200) THEN '100 < x <= 200'
		  WHEN (oo.office_size__c > 200  AND oo.office_size__c <= 300) THEN '200 < x <= 300'
		  WHEN (oo.office_size__c > 300  AND oo.office_size__c <= 400) THEN '300 < x <= 400'
		  ELSE '> 400'
		  END as office_size,
	
	CASE WHEN oo.additional_services__c IS NULL THEN 'No' ELSE 'Yes' END as additional_service,
	oo.recurrency__c,
	o.partner__c as partner_won,
	new_dates.new_date,
	send_dates.send_date,
	pending_dates.pending_date,

	accepted_dates.accepted_date,
	cancelled_dates.cancelled_date,
	CASE WHEN (o.status__c IN ('ACCEPTED', 'SEND', 'CANCELLED', 'PENDING') AND accepted_dates.accepted_date IS NOT NULL) THEN accepted_dates.accepted_date::timestamp - o.createddate::timestamp
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NOT NULL) THEN accepted_dates.accepted_date - o.createddate::timestamp
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NULL) THEN cancelled_dates.cancelled_date - o.createddate::timestamp
		  WHEN o.status__c IN ('NEW', 'SEND', 'PENDING') THEN current_timestamp - o.createddate::timestamp
		  ELSE current_timestamp - o.createddate::timestamp
		  END as waiting_time,
		  
	(CASE WHEN o.status__c = 'ACCEPTED' THEN DATE_PART('day', accepted_dates.accepted_date::timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', accepted_dates.accepted_date::timestamp - o.createddate::timestamp) + DATE_PART('minute', accepted_dates.accepted_date::timestamp - o.createddate::timestamp)/60
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NOT NULL) THEN DATE_PART('day', accepted_dates.accepted_date - o.createddate::timestamp)*24 + DATE_PART('hour', accepted_dates.accepted_date - o.createddate::timestamp) + DATE_PART('minute', accepted_dates.accepted_date - o.createddate::timestamp)/60
		 
		  WHEN (o.status__c = 'CANCELLED' AND accepted_dates.accepted_date IS NULL) THEN DATE_PART('day', cancelled_dates.cancelled_date - o.createddate::timestamp)*24 + DATE_PART('hour', cancelled_dates.cancelled_date - o.createddate::timestamp) + DATE_PART('minute', cancelled_dates.cancelled_date - o.createddate::timestamp)/60
		  
		  WHEN o.status__c IN ('NEW', 'SEND', 'PENDING') THEN DATE_PART('day', current_timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', current_timestamp - o.createddate::timestamp) + DATE_PART('minute', current_timestamp - o.createddate::timestamp)/60
		  ELSE DATE_PART('day', current_timestamp - o.createddate::timestamp)*24 + DATE_PART('hour', current_timestamp - o.createddate::timestamp) + DATE_PART('minute', current_timestamp - o.createddate::timestamp)/60
		  END)/24 as waiting_time_2		  

FROM

	salesforce.partner_offer__c o
	
LEFT JOIN

	salesforce.opportunity oo
	
ON

	o.opportunity__c = oo.sfid
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as new_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.oldvalue = 'NEW'
		
	GROUP BY
	
		o.parentid) as new_dates
		
ON

	o.sfid = new_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as send_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'SEND'
		
	GROUP BY
	
		o.parentid) as send_dates
		
ON

	o.sfid = send_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as pending_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'PENDING'
		
	GROUP BY
	
		o.parentid) as pending_dates
		
ON

	o.sfid = pending_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as accepted_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'ACCEPTED'
		
	GROUP BY
	
		o.parentid) as accepted_dates
		
ON

	o.sfid = accepted_dates.sfid_offer
	
LEFT JOIN

	(SELECT

		o.parentid as sfid_offer,
		MIN(o.createddate) as cancelled_date
	
	FROM
	
		salesforce.partner_offer__history o
		
	WHERE
		
		o.newvalue = 'CANCELLED'
		
	GROUP BY
	
		o.parentid) as cancelled_dates
		
ON

	o.sfid = cancelled_dates.sfid_offer
	
LEFT JOIN

	salesforce.contract__c ct
	
ON

	oo.sfid = ct.opportunity__c
	
WHERE

	LOWER(oo.name) NOT LIKE '%test%'
	AND oo.test__c IS FALSE
	AND ct.status__c IN ('SIGNED', 'ACCEPTED', 'CANCELLED', 'RESIGNED')
	AND new_dates.new_date IS NOT NULL
	AND ct.service_type__c LIKE 'maintenance cleaning'
	-- AND ct.active__c IS TRUE
	

ORDER BY

	createddate desc;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.scorecard_partners1;
CREATE TABLE bi.scorecard_partners1 as 

SELECT

	block4.*,
	block5.total_cost,
	CASE WHEN (block5.total_cost/10000)*20 > 20 THEN 20 ELSE (block5.total_cost/10000)*20 END as score_cost_partner


FROM

	(SELECT
	
		block3.sfid,
		block3.partner,
		block3.city as delivery_areas__c,
		block3.hours_city as total_hours_city,
		0.6*block3.hours_executed_this_month + 0.3*block3.hours_executed_last_month + 0.1*block3.hours_executed_last_month2 as hours_executed,	
		block3.threshold_hours,
		CASE WHEN block3.hours_city > 0 THEN
					 block3.hours_executed_this_month/block3.hours_city
		     ELSE 0 
			  END as share_city,
		CASE WHEN block3.hours_city > 0 THEN
				    CASE WHEN (block3.hours_executed_this_month/block3.hours_city) < (block3.threshold_hours - 0.05) THEN '> 5% under threshold'
				 	 WHEN (block3.hours_executed_this_month/block3.hours_city) > block3.threshold_hours THEN 'Above threshold'
				 	 ELSE '< 5% under threshold'
				 	 END
			  ELSE 'Error' 
		     END as light_city_share,		     
		block3.revenue_last_month,
		block3.revenue_last_month2,
		block3.score_revenue,
		CASE WHEN EXTRACT(DAY FROM current_date) > 7 
		     THEN
					CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation revenue €'
								  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue €'
								  ELSE 'Decreasing revenue €'
								  END
			  ELSE
			      CASE WHEN block3.revenue_last_month2 = block3.revenue_last_month3 THEN 'Stagnation revenue €'
								  WHEN block3.revenue_last_month2 > block3.revenue_last_month3 THEN 'Increasing revenue €'
								  ELSE 'Decreasing revenue €'
								  END
			  END as trend,
		block3.operational_costs_last_month,
		block3.gpm as gpm_last_month,
		
		CASE WHEN (block3.gpm*100/30)*15 > 15 THEN 15
		     WHEN (block3.gpm*100/30)*15 < 0 THEN 0
			  ELSE (block3.gpm*100/30)*15
			  END as score_gpm,		
	
		block3.active_pro_this_month,
		block3.active_pro_last_month,
		block3.active_pro_last_month2,
		
		CASE WHEN (block3.active_pro_last_month/10)*5 > 5 THEN 5
		     WHEN block3.active_pro_last_month IS NULL THEN 0
		     ELSE (block3.active_pro_last_month/10)*5
		     END as score_active_pro,
		
		CASE WHEN block3.active_pro_this_month > 0 
			  THEN block3.hours_executed_this_month/block3.active_pro_this_month
			  ELSE 0
			  END as hours_per_cleaner,
			  
		CASE WHEN ((CASE WHEN block3.active_pro_this_month > 0 
							  THEN block3.hours_executed_this_month/block3.active_pro_this_month
							  ELSE 0
							  END)/20)*5 > 5 THEN 5
			  ELSE 
					 ((CASE WHEN block3.active_pro_this_month > 0 
							  THEN block3.hours_executed_this_month/block3.active_pro_this_month
							  ELSE 0
							  END)/20)*5
			  END as score_hours_cleaners,  
			  			  		     
		CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
			  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
			  ELSE 'Decreasing # workers'
			  END as trend_active_pros,
		block3.churn_this_month,
		block3.churn_last_month,
		block3.churn_last_month2,
		block3.avg_churn as weighted_churn,
		
		CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
		     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
		     ELSE 0 
		     END as score_weighted_churn,
		
		block3.churn_this_month€,
		block3.churn_last_month€,
		block3.churn_last_month2€,  
		block3.avg_churn€ as weighted_churn_€,   
		
		CASE WHEN (block3.avg_churn€/500) = 0 THEN 10
		     WHEN (block3.avg_churn€/500) > 1 THEN 0
		     ELSE 10 - (block3.avg_churn€/500)
		     END as score_weighted_churn_€,
		
		block3.offers_received_this_month,
		block3.offers_received_last_month,
		block3.offers_received_last_month2,
		block3.offers_accepted_this_month,
		block3.offers_accepted_last_month,	     
		block3.offers_accepted_last_month2,		
		block3.avg_offers_accepted as pc_accepted_weighted,
		
		block3.avg_offers_accepted*10 as score_acceptance_weighted,
		
		block3.orders_submitted_this_month,
		block3.orders_submitted_last_month,
		block3.orders_submitted_last_month2,
		block3.orders_checked_this_month,
		block3.orders_checked_last_month,
		block3.orders_checked_last_month2,	
		block3.avg_orders_checked as pc_checked_weighted,
			  
		block3.avg_orders_checked*10 as score_checked_weighted,
		
		block3.cases_this_month,
		block3.cases_last_month,
		block3.cases_last_month2,
		block3.avg_number_cases as number_cases
	
	
	FROM	
		
		(SELECT
		
			block2.sfid,
			block2.partner,
			block2.city,
			hours_city.hours_executed_this_month as hours_city,
			block2.hours_executed_this_month,
			block2.hours_executed_last_month,	
			block2.hours_executed_last_month2,
			CASE WHEN hours_city.hours_executed_this_month >= 10000 THEN 0.17
				  WHEN (hours_city.hours_executed_this_month >= 7000 AND hours_city.hours_executed_this_month < 10000) THEN 0.18
				  WHEN (hours_city.hours_executed_this_month >= 4000 AND hours_city.hours_executed_this_month < 7000) THEN 0.2
				  WHEN (hours_city.hours_executed_this_month >= 2000 AND hours_city.hours_executed_this_month < 4000) THEN 0.25
				  WHEN (hours_city.hours_executed_this_month >= 1000 AND hours_city.hours_executed_this_month < 2000) THEN 0.3
				  WHEN (hours_city.hours_executed_this_month >= 500 AND hours_city.hours_executed_this_month < 1000) THEN 0.35
				  WHEN (hours_city.hours_executed_this_month >= 350 AND hours_city.hours_executed_this_month < 500) THEN 0.4
				  WHEN (hours_city.hours_executed_this_month >= 200 AND hours_city.hours_executed_this_month < 350) THEN 0.5
				  WHEN (hours_city.hours_executed_this_month >= 100 AND hours_city.hours_executed_this_month < 200) THEN 0.6
				  WHEN (hours_city.hours_executed_this_month >= 50 AND hours_city.hours_executed_this_month < 100) THEN 0.75
				  ELSE 1
				  END as threshold_hours,
			block2.revenue_last_month,
			block2.revenue_last_month2,
			block2.revenue_last_month3,
			
			CASE WHEN EXTRACT(DAY FROM current_date) > 7
				  THEN CASE WHEN (block2.revenue_last_month/10000)*20 > 20 THEN 20 ELSE (block2.revenue_last_month/10000)*20 END 
			     ELSE 
				       CASE WHEN (block2.revenue_last_month2/10000)*20 > 20 THEN 20 ELSE (block2.revenue_last_month2/10000)*20 END 
				  END as score_revenue,
				  
			block2.operational_costs_last_month,
			block2.operational_costs_last_month2,
			CASE WHEN EXTRACT(DAY FROM current_date) > 7 
			     THEN 
						CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END
	
				  ELSE 
						CASE WHEN (block2.revenue_last_month2) > 0 THEN (block2.revenue_last_month2 - block2.operational_costs_last_month2)/(block2.revenue_last_month2) ELSE 0 END
				  END as gpm,
		
			block2.active_pro_this_month,
			block2.active_pro_last_month,
			block2.active_pro_last_month2,
			block2.churn_this_month,
			block2.churn_last_month,
			block2.churn_last_month2,
			0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
			block2.churn_this_month€,
			block2.churn_last_month€,
			block2.churn_last_month2€,  
			0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
			block2.offers_received_this_month,
			block2.offers_received_last_month,
			block2.offers_received_last_month2,
			block2.offers_accepted_this_month,
			block2.offers_accepted_last_month,	     
			block2.offers_accepted_last_month2,
				
			0.6*(CASE WHEN block2.offers_received_this_month > 0 
			     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
				  ELSE 0
				  END) +
			0.3*(CASE WHEN block2.offers_received_last_month > 0
				  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
				  ELSE 0
				  END) +
			0.1*(CASE WHEN block2.offers_received_last_month2 > 0
			     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
				  ELSE 0
				  END) as avg_offers_accepted,
			
			block2.orders_submitted_this_month,
			block2.orders_submitted_last_month,
			block2.orders_submitted_last_month2,
			block2.orders_checked_this_month,
			block2.orders_checked_last_month,
			block2.orders_checked_last_month2,
			
			0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
			     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
				  ELSE 0
				  END) +
			0.3*(CASE WHEN block2.orders_submitted_last_month > 0
				  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
				  ELSE 0
				  END) +
			0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
			     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
				  ELSE 0
				  END) as avg_orders_checked,
				  
			block2.cases_this_month,
			block2.cases_last_month,
			block2.cases_last_month2,
			0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
		
		FROM
				
			(SELECT
			
				block1.sfid,
				block1.partner,
				-- block1.year_month,
				block1.city,
				SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
				     ELSE block1.revenue_last_month
				     END) as revenue_last_month,
				SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
				     ELSE block1.revenue_last_month2
				     END) as revenue_last_month2,
				SUM(CASE WHEN block1.revenue_last_month3 IS NULL THEN -0
				     ELSE block1.revenue_last_month3
				     END) as revenue_last_month3,
				SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
				     ELSE block1.operational_costs_last_month
				     END) as operational_costs_last_month,
				SUM(CASE WHEN block1.operational_costs_last_month2 IS NULL THEN -0
				     ELSE block1.operational_costs_last_month2
				     END) as operational_costs_last_month2,
				SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
				     ELSE block1.hours_executed_this_month
				     END) as hours_executed_this_month,
				SUM(CASE WHEN block1.hours_executed_last_month IS NULL THEN -0
				     ELSE block1.hours_executed_last_month
				     END) as hours_executed_last_month,
				SUM(CASE WHEN block1.hours_executed_last_month2 IS NULL THEN -0
				     ELSE block1.hours_executed_last_month2
				     END) as hours_executed_last_month2,
				SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
				     ELSE block1.active_pro_this_month
				     END) as active_pro_this_month,
				SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
				     ELSE block1.active_pro_last_month
				     END) as active_pro_last_month,
				SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
				     ELSE block1.active_pro_last_month2
				     END) as active_pro_last_month2,
				SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
				     ELSE block1.churn_this_month
				     END) as churn_this_month,
				SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
				     ELSE block1.churn_last_month
				     END) as churn_last_month,
				SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
				     ELSE block1.churn_last_month2
				     END) as churn_last_month2,
				SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
				     ELSE block1.churn_this_month€
				     END) as churn_this_month€,
				SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
				     ELSE block1.churn_last_month€
				     END) as churn_last_month€,
				SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
				     ELSE block1.churn_last_month2€
				     END) as churn_last_month2€,     
				SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
				     ELSE block1.offers_received_this_month
				     END) as offers_received_this_month,
				SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
				     ELSE block1.offers_received_last_month
				     END) as offers_received_last_month,
				SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
				     ELSE block1.offers_received_last_month2
				     END) as offers_received_last_month2,
				SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
				     ELSE block1.offers_accepted_this_month
				     END) as offers_accepted_this_month,
				SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
				     ELSE block1.offers_accepted_last_month
				     END) as offers_accepted_last_month,	     
				SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
				     ELSE block1.offers_accepted_last_month2
				     END) as offers_accepted_last_month2,
				SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
				     ELSE block1.orders_submitted_this_month
				     END) as orders_submitted_this_month,
				SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
				     ELSE block1.orders_submitted_last_month
				     END) as orders_submitted_last_month,
				SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
				     ELSE block1.orders_submitted_last_month2
				     END) as orders_submitted_last_month2,
				SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
				     ELSE block1.orders_checked_this_month
				     END) as orders_checked_this_month,
				SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
				     ELSE block1.orders_checked_last_month
				     END) as orders_checked_last_month,
				SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
				     ELSE block1.orders_checked_last_month2
				     END) as orders_checked_last_month2,	
				SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
				     ELSE block1.cases_this_month
				     END) as cases_this_month,
				SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
				     ELSE block1.cases_last_month
				     END) as cases_last_month,
				SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
				     ELSE block1.cases_last_month2
				     END) as cases_last_month2
					
			FROM	
				
				(SELECT
				
					in_kpi_master.sfid,
					in_kpi_master.partner,
					in_kpi_master.year_month,
					in_kpi_master.city,
					MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
					MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
					MAX(in_kpi_master.revenue_last_month3) as revenue_last_month3,
					MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
					MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
					MAX(in_kpi_master.operational_costs_last_month2) as operational_costs_last_month2,
					MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
					MAX(in_kpi_master.hours_executed_last_month) as hours_executed_last_month,
					MAX(in_kpi_master.hours_executed_last_month) as hours_executed_last_month2,
					MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
					MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
					MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
					
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_this_month,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_last_month,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_last_month2,
											
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_this_month€,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_last_month€,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_last_month2€,
					MAX(offers.offers_received_this_month) as offers_received_this_month,
					MAX(offers.offers_received_last_month) as offers_received_last_month,
					MAX(offers.offers_received_last_month2) as offers_received_last_month2,
					MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
					MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
					MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
					
					MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
					MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
					MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
					MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
					MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
					MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
					
					MAX(number_cases.cases_this_month) as cases_this_month,
					MAX(number_cases.cases_last_month) as cases_last_month,
					MAX(number_cases.cases_last_month2) as cases_last_month2
										
				FROM
					
					
					((SELECT -- START PART 1 -- Simple list of all the accounts with the role master (all the partners registered on the platform)
					
						a.sfid,
						a.delivery_areas__c,
						a.name as partner
					
					FROM
					
						salesforce.account a 
							
					WHERE
					
						a.test__c IS FALSE
						AND a.role__c = 'master') as t0 -- END PART 1 --
						
					LEFT JOIN
						
						(SELECT -- START PART 2 -- This part is extracting from the table kpi_master. We create a table containing, for every partner, in every city:
							                                                      -- The revenue of last month
							                                                      -- The revenue of month - 2
							                                                      -- The cost of supply last month
							                                                      -- The operational costs of last month
							                                                      -- The hours executed of this month
							                                                      -- The active cleaners of this month
							                                                      -- The active cleaners of last month
							                                                      -- The active cleaners of month - 2
				
							o.date_part as year_month,
							o.sub_kpi_2 as partner2,
							o.city,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
										THEN o.value 
										ELSE 0 END) as revenue_last_month,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as revenue_last_month2,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as revenue_last_month3,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
											(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
					                        THEN o.value
								               ELSE 0 END) 
											      END) as cost_supply_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as operational_costs_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as operational_costs_last_month2,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_this_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_last_month2,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
										THEN o.value 
										ELSE 0 END) as active_pro_this_month,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
										THEN o.value 
										ELSE 0 END) as active_pro_last_month,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as active_pro_last_month2									
															
						FROM
						
							bi.kpi_master o
							
						WHERE
						
							((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
							OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7) AND o.sub_kpi_5 = 'Scorecard')
							OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7) AND o.sub_kpi_5 = 'Scorecard')
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
											
						GROUP BY
						
							o.date_part,
							partner2,
							o.city
							
						ORDER BY
						
							year_month desc,
							partner2 asc) as t1 -- END PART 2 --
							
					ON
					
						t0.partner = t1.partner2) as in_kpi_master
						
				LEFT JOIN
				
					bi.deep_churn as deep_churn1
					
				ON
				
					in_kpi_master.partner = deep_churn1.company_name
					AND in_kpi_master.city = deep_churn1.city
					AND in_kpi_master.year_month = deep_churn1."year_month"
					
				LEFT JOIN
				
					(SELECT -- START PART 4 -- This part creates a table with the number of offers received and accepted by partners in every city:
						                                              -- This month
						                                              -- Last month
						                                              -- Month - 2
					
						offers1.year_month_offer,
						offers1.sfid,
						offers1.name as company_name__c,
						offers1.delivery_area__c,
						SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '1' month))::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '2' month))::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
						SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '1' month))::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '2' month))::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
						
					
					FROM
					
						(SELECT -- START PART 3 -- This part creates a table with the number of offers received and accepted by partner in every city and every month
										
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
							MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
							a.sfid,
							a.name,
							a.company_name__c,
							ooo.delivery_area__c,
							-- a.delivery_areas__c,
							a.type__c,
							a.status__c,
							a.role__c,
							COUNT(o.suggested_partner__c) as offers_received,
							SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
						
						
						FROM
						
							salesforce.account a
						
						
						LEFT JOIN
						
							salesforce.partner_offer_partner__c o
								
						ON
						
							a.sfid = o.suggested_partner__c 
							
						LEFT JOIN
						
							salesforce.partner_offer__c oo
							
						ON 
						
							o.partner_offer__c = oo.sfid
							
						LEFT JOIN
						
							salesforce.opportunity ooo
							
						ON
						
							oo.opportunity__c = ooo.sfid
							
						WHERE
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
							AND a.test__c IS FALSE
							
						GROUP BY
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
							a.sfid,
							a.name,
							a.company_name__c,
							ooo.delivery_area__c,
							-- a.delivery_areas__c,
							a.type__c,
							a.status__c,
							a.role__c
						
						ORDER BY
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
							a.name asc) as offers1 -- END PART 3 --
							
					GROUP BY
					
						offers1.year_month_offer,
						offers1.sfid,
						offers1.name,
						offers1.delivery_area__c) as offers -- END PART 4 --
						
				ON
				
					in_kpi_master.partner = offers.company_name__c
					AND in_kpi_master.city = offers.delivery_area__c
					AND in_kpi_master.year_month = offers.year_month_offer
					
				LEFT JOIN
				
					(SELECT -- START PART 7 -- This part of thw query creates a table showing for every partner the orders submitted and checked this month, last month and month - 2
					
						orders_checked1.year_month,
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.polygon,
						SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
						SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
					
					FROM	
						
						(SELECT -- START PART 6 -- This part of the query creates a table showing for every partner, in every city and every month:
							 														-- the number of orders executed
							 														-- the number of orders checked on the portal
												
						  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
						  MIN(effectivedate::date) as mindate,
						  MAX(effectivedate::date) as maxdate,
						  t4.delivery_area__c as polygon,
						  t3.subcon as name_partner,
						  t3.sfid_partner,
						  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
						  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
						
						FROM
						
						  (SELECT -- START PART 5 -- This part of the query creates the list of all the cleaners that were registered on our platform as working for a partner
	
							  t5.*,
							  t2.name as subcon,
							  t2.sfid as sfid_partner
							  
							FROM
	
								Salesforce.Account t5
	
							JOIN
							   
								Salesforce.Account t2
	
							ON
	
								(t2.sfid = t5.parentid)
							  
							WHERE 
	
								t5.status__c not in ('SUSPENDED') 
								AND t5.test__c IS FALSE
								AND t2.test__c IS FALSE
								AND t5.name not like '%test%'
								-- AND t5.type__c = 'partner'
								AND LEFT(t5.locale__c, 2) = 'de'
								-- AND t5.role__c = 'master'
								AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
								AND t5.name NOT LIKE '%Handyman Kovacs%'
								AND t5.name NOT LIKE '%BAT Business Services GmbH%'
								and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
								and t2.name NOT LIKE '%BAT Business Services GmbH%') t3 -- END PART 5 --
						      
						JOIN 
						
						  salesforce.order t4
						  
						ON
						
						  (t3.sfid = t4.professional__c)
						  
						WHERE
						
						  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
						  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
						  and LEFT(t4.locale__c,2) IN ('de')
						  and t4.effectivedate < current_date
						  AND t4.type = 'cleaning-b2b'
						  AND t4.test__c IS FALSE
						
						GROUP BY
						
						  year_month,
						  polygon,
						  t3.subcon,
						  t3.sfid_partner
						  
						  
						ORDER BY
							
						  year_month desc,
						  polygon,
						  t3.subcon) as orders_checked1 -- END PART 6 --
						  
					GROUP BY
					
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.year_month,
						orders_checked1.polygon
						
					ORDER BY
					
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.year_month desc) as orders_checked -- END PART 7 --
						
				ON
				
					in_kpi_master.partner = orders_checked.name_partner
					AND in_kpi_master.city = orders_checked.polygon
					AND in_kpi_master.year_month = orders_checked.year_month
					
				LEFT JOIN
				
					(SELECT
					
						t3.year_month,
						t3.partnerid,
						t4.name as name_partner,
						t3.delivery_area__c,
						SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
						SUM(CASE WHEN t3.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
						SUM(CASE WHEN t3.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
						
					FROM
								
						(SELECT -- START PART 9 -- This query creates a table displaying the number of cases created by each partner every month in every city
						
							TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
							CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
							t2.name_partner,
							t2.parentid,
							COUNT(DISTINCT t2.sfid_case) as number_cases,
							t2.delivery_area__c
						
						FROM	
						
							(SELECT -- START PART 8 -- This query creates a list of all the cases created by partners in the last 3 months, with assignement to a delivery area
							
								t1.date_case as date_case,
								CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
								t1.sfid_case,
								a.name as name_partner,
								a.parentid,
								t1.delivery_area__c
								
							FROM	
								
								(SELECT -- This query creates a list of all the cases created by partners in the last 3 months, with assignement to a delivery area
								
									c.createddate as date_case,
									c.CaseNumber, 
									c.sfid as sfid_case, 
									o.sfid, a.sfid, 
									o.delivery_area__c,
									c.origin, 
									c.*
								
								FROM 
								
									salesforce.case c
									
								LEFT JOIN 
								
									salesforce.opportunity o
								
								ON
									o.sfid = c.opportunity__c
									
								LEFT JOIN 
								
									salesforce.account a on a.sfid = c.accountid
									
								WHERE 
								
									c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement')
									AND COALESCE(c.Origin, '') != 'System - Notification'
									AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4')
									AND c.type != 'CM B2C'
									AND c.CreatedDate >= (current_date - (interval '3' month))
									AND c.test__c IS FALSE
									AND a.test__c IS FALSE
									AND COALESCE(o.name, '') NOT LIKE '%test%' 
									-- SFID from ## account
									AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
									-- SFID from BAT Business Services GmbH account
									AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
									AND c.parentid IS NULL) as t1
									
							LEFT JOIN
							
								salesforce.account a
								
							ON
							
								(t1.accountid = a.sfid)
								-- OR t1.partner__c = a.sfid)
								
							WHERE 
							
								a.name IS NOT NULL) as t2 -- END PART 8 -- 
								
						GROUP BY 
						
							TO_CHAR(t2.date_case, 'YYYY-MM'),
							t2.partner,
							t2.name_partner,
							t2.parentid,
							t2.delivery_area__c
							
						ORDER BY
						
							t2.name_partner,
							TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3 -- END PART 9 -- 
							
					LEFT JOIN
					
						salesforce.account t4
						
					ON
					
						t3.partnerid = t4.sfid
						
					GROUP BY
					
						t3.year_month,
						t3.partnerid,
						t3.delivery_area__c,
						t4.name) as number_cases
						
				ON
				
					in_kpi_master.partner = number_cases.name_partner
					AND in_kpi_master.city = number_cases.delivery_area__c
					AND in_kpi_master.year_month = number_cases.year_month	
				
				GROUP BY
				
					in_kpi_master.sfid,
					in_kpi_master.partner,
					in_kpi_master.year_month,
					in_kpi_master.city
					
				ORDER BY
				
					in_kpi_master.sfid,
					in_kpi_master.year_month desc) as block1
		
			WHERE
		
				block1.year_month IS NOT NULL
						
			GROUP BY
			
				block1.sfid,
				block1.partner,
				block1.city) as block2
				
		LEFT JOIN
		
			(SELECT -- START PART 10 -- This query creates a table displaying the hours executed by each partner every month in every city
		
				o.city,
				SUM(CASE WHEN 
					         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
					               THEN o.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
				                  THEN o.value
							         ELSE 0 END) 
										END) as hours_executed_this_month,
				SUM(CASE WHEN 
					         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
					               THEN o.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
				                  THEN o.value
							         ELSE 0 END) 
										END) as hours_executed_last_month,
				SUM(CASE WHEN 
					         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
					               THEN o.value
								      ELSE 0 END) IS NULL THEN 0
							ELSE 
							   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
				                  THEN o.value
							         ELSE 0 END) 
										END) as hours_executed_last_month2							
												
			FROM
			
				bi.kpi_master o
				
			WHERE
			
				(o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)
				 OR o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)
				 OR o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)
				 )
				AND o.sub_kpi_2 NOT LIKE 'BAT Business Services GmbH'
			
			GROUP BY
			
				o.city) as hours_city -- END PART 10 --
				
		ON
		
			block2.city = hours_city.city
		
		WHERE
		
			block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3) as block4
			
LEFT JOIN

	(SELECT
	
		t3.served_by__c as sfid_partner,
		t3.partner,
		t3.delivery_area__c,
		SUM(t3.final_partner_cost) as total_cost
	
	FROM	
		
		(SELECT	
			
			t1.*,
			t2.hours,
			CASE WHEN t1.partner_costs = 0 THEN t2.hours*t1.pph_partner ELSE t1.partner_costs END as final_partner_cost
			
		FROM
		
			(SELECT
			
				o.sfid as sfid_opp,
				o.name as name_opp,
				o.delivery_area__c,
				o.served_by__c,
				a.name as partner,
				a.pph__c as pph_partner,
				o.plan_pph__c,
				SUM(cont.grand_total__c) as grand_total,
				SUM(o.potential_partner_costs__c) as partner_costs
				
			FROM
			
				salesforce.opportunity o
				
			LEFT JOIN
			
				salesforce.contract__c cont
				
			ON
			
				o.sfid = cont.opportunity__c
				
			LEFT JOIN
			
				salesforce.account a
				
			ON
			
				o.served_by__c = a.sfid
				
			WHERE
			
				o.status__c NOT IN ('RESIGNED', 'CANCELLED')
				AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')
				AND o.test__c IS FALSE
				AND cont.test__c IS FALSE
				AND cont.active__c IS TRUE
				AND o.served_by__c IS NOT NULL
				AND a.name NOT LIKE '%BAT %'
				
			GROUP BY
			
				o.sfid,
				o.name,
				o.delivery_area__c,
				o.served_by__c,
				o.plan_pph__c,
				a.pph__c,
				a.name) as t1
				
		LEFT JOIN
		
			(SELECT
		
				o.opportunityid,
				SUM(o.order_duration__c) as hours
			
			
			FROM
			
				salesforce.order o
				
			WHERE
			
				o.test__c IS FALSE
				AND o."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
				AND o.effectivedate BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '4 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
				
			GROUP BY
			
				o.opportunityid) as t2
				
		ON
		
			t1.sfid_opp = t2.opportunityid) as t3
			
	GROUP BY	
	
		t3.served_by__c,
		t3.partner,
		t3.delivery_area__c) as block5
		
ON

	block4.sfid = block5.sfid_partner
	AND block4.delivery_areas__c = block5.delivery_area__c;
				

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.scorecard_partners2;
CREATE TABLE bi.scorecard_partners2 as 
	
SELECT	
		
	block4.*,
	block5.total_cost,
	CASE WHEN (block5.total_cost/10000)*20 > 20 THEN 20 ELSE (block5.total_cost/10000)*20 END as score_cost_partner

FROm
	
	(SELECT
	
		block3.sfid,
		block3.partner,	
		0.6*block3.hours_executed_this_month + 0.3*block3.hours_executed_last_month + 0.1*block3.hours_executed_last_month2 as hours_executed,	
		block3.revenue_last_month,
		block3.revenue_last_month2,
		block3.revenue_last_month3,
		block3.score_revenue,
		CASE WHEN EXTRACT(DAY FROM current_date) > 7 
		     THEN
					CASE WHEN block3.revenue_last_month = block3.revenue_last_month2 THEN 'Stagnation revenue €'
								  WHEN block3.revenue_last_month > block3.revenue_last_month2 THEN 'Increasing revenue €'
								  ELSE 'Decreasing revenue €'
								  END
			  ELSE
			      CASE WHEN block3.revenue_last_month2 = block3.revenue_last_month3 THEN 'Stagnation revenue €'
								  WHEN block3.revenue_last_month2 > block3.revenue_last_month3 THEN 'Increasing revenue €'
								  ELSE 'Decreasing revenue €'
								  END
			  END as trend,
		block3.operational_costs_last_month,
		block3.operational_costs_last_month2,
		block3.gpm as gpm_last_month,
		
		CASE WHEN (block3.gpm*100/30)*15 > 15 THEN 15
		     WHEN (block3.gpm*100/30)*15 < 0 THEN 0
			  ELSE (block3.gpm*100/30)*15
			  END as score_gpm,
	
		block3.active_pro_this_month,
		block3.active_pro_last_month,
		block3.active_pro_last_month2,
		CASE WHEN (block3.active_pro_last_month/10)*5 > 5 THEN 5
		     WHEN block3.active_pro_last_month IS NULL THEN 0
		     ELSE (block3.active_pro_last_month/10)*5
		     END as score_active_pro,
		CASE WHEN block3.active_pro_this_month > 0 
			  THEN block3.hours_executed_this_month/block3.active_pro_this_month
			  ELSE 0
			  END as hours_per_cleaner,
			  
		CASE WHEN ((CASE WHEN block3.active_pro_this_month > 0 
							  THEN block3.hours_executed_this_month/block3.active_pro_this_month
							  ELSE 0
							  END)/20)*5 > 5 THEN 5
			  ELSE 
					 ((CASE WHEN block3.active_pro_this_month > 0 
							  THEN block3.hours_executed_this_month/block3.active_pro_this_month
							  ELSE 0
							  END)/20)*5
			  END as score_hours_cleaners,
		     
		CASE WHEN block3.active_pro_this_month = block3.active_pro_last_month THEN 'Stagnation # workers'
			  WHEN block3.active_pro_this_month > block3.active_pro_last_month THEN 'Increasing # workers'
			  ELSE 'Decreasing # workers'
			  END as trend_active_pros,
		block3.churn_this_month,
		block3.churn_last_month,
		block3.churn_last_month2,
		block3.avg_churn as weighted_churn,
		
		CASE WHEN (block3.avg_churn = 0 OR block3.avg_churn IS NULL) THEN 10
		     WHEN (block3.avg_churn > 0 AND block3.avg_churn < 1) THEN 5
		     ELSE 0 
		     END as score_weighted_churn,
		
		block3.churn_this_month€,
		block3.churn_last_month€,
		block3.churn_last_month2€,  
		block3.avg_churn€ as weighted_churn_€,   
		
		CASE WHEN (block3.avg_churn€/500) = 0 THEN 10
		     WHEN (block3.avg_churn€/500) > 1 THEN 0
		     ELSE 10 - (block3.avg_churn€/500)
		     END as score_weighted_churn_€,
		
		block3.offers_received_this_month,
		block3.offers_received_last_month,
		block3.offers_received_last_month2,
		block3.offers_accepted_this_month,
		block3.offers_accepted_last_month,	     
		block3.offers_accepted_last_month2,		
		block3.avg_offers_accepted as pc_accepted_weighted,
		
		block3.avg_offers_accepted*10 as score_acceptance_weighted,
		
		block3.orders_submitted_this_month,
		block3.orders_submitted_last_month,
		block3.orders_submitted_last_month2,
		block3.orders_checked_this_month,
		block3.orders_checked_last_month,
		block3.orders_checked_last_month2,	
		block3.avg_orders_checked as pc_checked_weighted,
			  
		block3.avg_orders_checked*10 as score_checked_weighted,
		
		block3.cases_this_month,
		block3.cases_last_month,
		block3.cases_last_month2,
		block3.avg_number_cases as number_cases
	
	
	FROM	
		
		(SELECT
		
			block2.sfid,
			block2.partner,
			block2.hours_executed_this_month,
			block2.hours_executed_last_month,
			block2.hours_executed_last_month2,	
			block2.revenue_last_month,
			block2.revenue_last_month2,
			block2.revenue_last_month3,
			CASE WHEN EXTRACT(DAY FROM current_date) > 7
				  THEN CASE WHEN (block2.revenue_last_month/10000)*20 > 20 THEN 20 ELSE (block2.revenue_last_month/10000)*20 END 
			     ELSE 
				       CASE WHEN (block2.revenue_last_month2/10000)*20 > 20 THEN 20 ELSE (block2.revenue_last_month2/10000)*20 END 
				  END as score_revenue,
			block2.operational_costs_last_month,
			block2.operational_costs_last_month2,
			CASE WHEN EXTRACT(DAY FROM current_date) > 7 
			     THEN 
						CASE WHEN (block2.revenue_last_month) > 0 THEN (block2.revenue_last_month - block2.operational_costs_last_month)/(block2.revenue_last_month) ELSE 0 END
	
				  ELSE 
						CASE WHEN (block2.revenue_last_month2) > 0 THEN (block2.revenue_last_month2 - block2.operational_costs_last_month2)/(block2.revenue_last_month2) ELSE 0 END
				  END as gpm,
			block2.active_pro_this_month,
			block2.active_pro_last_month,
			block2.active_pro_last_month2,
			block2.churn_this_month,
			block2.churn_last_month,
			block2.churn_last_month2,
			0.6*block2.churn_this_month + 0.3*block2.churn_last_month + 0.1*block2.churn_last_month2 as avg_churn,
			block2.churn_this_month€,
			block2.churn_last_month€,
			block2.churn_last_month2€,  
			0.6*block2.churn_this_month€ + 0.3*block2.churn_last_month€ + 0.1*block2.churn_last_month2€ as avg_churn€,   
			block2.offers_received_this_month,
			block2.offers_received_last_month,
			block2.offers_received_last_month2,
			block2.offers_accepted_this_month,
			block2.offers_accepted_last_month,	     
			block2.offers_accepted_last_month2,
				
			0.6*(CASE WHEN block2.offers_received_this_month > 0 
			     THEN block2.offers_accepted_this_month/block2.offers_received_this_month
				  ELSE 0
				  END) +
			0.3*(CASE WHEN block2.offers_received_last_month > 0
				  THEN block2.offers_accepted_last_month/block2.offers_received_last_month
				  ELSE 0
				  END) +
			0.1*(CASE WHEN block2.offers_received_last_month2 > 0
			     THEN block2.offers_accepted_last_month2/block2.offers_received_last_month2
				  ELSE 0
				  END) as avg_offers_accepted,
			
			block2.orders_submitted_this_month,
			block2.orders_submitted_last_month,
			block2.orders_submitted_last_month2,
			block2.orders_checked_this_month,
			block2.orders_checked_last_month,
			block2.orders_checked_last_month2,
			
			0.6*(CASE WHEN block2.orders_submitted_this_month > 0 
			     THEN block2.orders_checked_this_month/block2.orders_submitted_this_month
				  ELSE 0
				  END) +
			0.3*(CASE WHEN block2.orders_submitted_last_month > 0
				  THEN block2.orders_checked_last_month/block2.orders_submitted_last_month
				  ELSE 0
				  END) +
			0.1*(CASE WHEN block2.orders_submitted_last_month2 > 0
			     THEN block2.orders_checked_last_month2/block2.orders_submitted_last_month2
				  ELSE 0
				  END) as avg_orders_checked,
				  
			block2.cases_this_month,
			block2.cases_last_month,
			block2.cases_last_month2,
			0.6*block2.cases_this_month + 0.3*block2.cases_last_month + 0.1*block2.cases_last_month2 as avg_number_cases
		
		FROM
				
			(SELECT
			
				block1.sfid,
				block1.partner,
				-- block1.year_month,
				SUM(CASE WHEN block1.revenue_last_month IS NULL THEN -0
				     ELSE block1.revenue_last_month
				     END) as revenue_last_month,
				SUM(CASE WHEN block1.revenue_last_month2 IS NULL THEN -0
				     ELSE block1.revenue_last_month2
				     END) as revenue_last_month2,
				SUM(CASE WHEN block1.revenue_last_month3 IS NULL THEN -0
				     ELSE block1.revenue_last_month3
				     END) as revenue_last_month3,
				SUM(CASE WHEN block1.operational_costs_last_month IS NULL THEN -0
				     ELSE block1.operational_costs_last_month
				     END) as operational_costs_last_month,
				SUM(CASE WHEN block1.operational_costs_last_month2 IS NULL THEN -0
				     ELSE block1.operational_costs_last_month2
				     END) as operational_costs_last_month2,
				SUM(CASE WHEN block1.hours_executed_this_month IS NULL THEN -0
				     ELSE block1.hours_executed_this_month
				     END) as hours_executed_this_month,
				SUM(CASE WHEN block1.hours_executed_last_month IS NULL THEN -0
				     ELSE block1.hours_executed_last_month
				     END) as hours_executed_last_month,
				SUM(CASE WHEN block1.hours_executed_last_month2 IS NULL THEN -0
				     ELSE block1.hours_executed_last_month2
				     END) as hours_executed_last_month2,
				SUM(CASE WHEN block1.active_pro_this_month IS NULL THEN -0
				     ELSE block1.active_pro_this_month
				     END) as active_pro_this_month,
				SUM(CASE WHEN block1.active_pro_last_month IS NULL THEN -0
				     ELSE block1.active_pro_last_month
				     END) as active_pro_last_month,
				SUM(CASE WHEN block1.active_pro_last_month2 IS NULL THEN -0
				     ELSE block1.active_pro_last_month2
				     END) as active_pro_last_month2,
				SUM(CASE WHEN block1.churn_this_month IS NULL THEN -0
				     ELSE block1.churn_this_month
				     END) as churn_this_month,
				SUM(CASE WHEN block1.churn_last_month IS NULL THEN -0 
				     ELSE block1.churn_last_month
				     END) as churn_last_month,
				SUM(CASE WHEN block1.churn_last_month2 IS NULL THEN -0 
				     ELSE block1.churn_last_month2
				     END) as churn_last_month2,
				SUM(CASE WHEN block1.churn_this_month€ IS NULL THEN -0 
				     ELSE block1.churn_this_month€
				     END) as churn_this_month€,
				SUM(CASE WHEN block1.churn_last_month€ IS NULL THEN -0 
				     ELSE block1.churn_last_month€
				     END) as churn_last_month€,
				SUM(CASE WHEN block1.churn_last_month2€ IS NULL THEN -0 
				     ELSE block1.churn_last_month2€
				     END) as churn_last_month2€,     
				SUM(CASE WHEN block1.offers_received_this_month IS NULL THEN -0 
				     ELSE block1.offers_received_this_month
				     END) as offers_received_this_month,
				SUM(CASE WHEN block1.offers_received_last_month IS NULL THEN -0 
				     ELSE block1.offers_received_last_month
				     END) as offers_received_last_month,
				SUM(CASE WHEN block1.offers_received_last_month2 IS NULL THEN -0 
				     ELSE block1.offers_received_last_month2
				     END) as offers_received_last_month2,
				SUM(CASE WHEN block1.offers_accepted_this_month IS NULL THEN -0 
				     ELSE block1.offers_accepted_this_month
				     END) as offers_accepted_this_month,
				SUM(CASE WHEN block1.offers_accepted_last_month IS NULL THEN -0 
				     ELSE block1.offers_accepted_last_month
				     END) as offers_accepted_last_month,	     
				SUM(CASE WHEN block1.offers_accepted_last_month2 IS NULL THEN -0 
				     ELSE block1.offers_accepted_last_month2
				     END) as offers_accepted_last_month2,
				SUM(CASE WHEN block1.orders_submitted_this_month IS NULL THEN -0 
				     ELSE block1.orders_submitted_this_month
				     END) as orders_submitted_this_month,
				SUM(CASE WHEN block1.orders_submitted_last_month IS NULL THEN -0 
				     ELSE block1.orders_submitted_last_month
				     END) as orders_submitted_last_month,
				SUM(CASE WHEN block1.orders_submitted_last_month2 IS NULL THEN -0 
				     ELSE block1.orders_submitted_last_month2
				     END) as orders_submitted_last_month2,
				SUM(CASE WHEN block1.orders_checked_this_month IS NULL THEN -0 
				     ELSE block1.orders_checked_this_month
				     END) as orders_checked_this_month,
				SUM(CASE WHEN block1.orders_checked_last_month IS NULL THEN -0 
				     ELSE block1.orders_checked_last_month
				     END) as orders_checked_last_month,
				SUM(CASE WHEN block1.orders_checked_last_month2 IS NULL THEN -0 
				     ELSE block1.orders_checked_last_month2
				     END) as orders_checked_last_month2,	
				SUM(CASE WHEN block1.cases_this_month IS NULL THEN -0 
				     ELSE block1.cases_this_month
				     END) as cases_this_month,
				SUM(CASE WHEN block1.cases_last_month IS NULL THEN -0 
				     ELSE block1.cases_last_month
				     END) as cases_last_month,
				SUM(CASE WHEN block1.cases_last_month2 IS NULL THEN -0 
				     ELSE block1.cases_last_month2
				     END) as cases_last_month2
					
			FROM	
				
				(SELECT
				
					in_kpi_master.sfid,
					in_kpi_master.partner,
					in_kpi_master.year_month,
					MAX(in_kpi_master.revenue_last_month) as revenue_last_month,
					MAX(in_kpi_master.revenue_last_month2) as revenue_last_month2,
					MAX(in_kpi_master.revenue_last_month3) as revenue_last_month3,
					MAX(in_kpi_master.cost_supply_last_month) as cost_supply_last_month,
					MAX(in_kpi_master.operational_costs_last_month) as operational_costs_last_month,
					MAX(in_kpi_master.operational_costs_last_month2) as operational_costs_last_month2,
					MAX(in_kpi_master.hours_executed_this_month) as hours_executed_this_month,
					MAX(in_kpi_master.hours_executed_last_month) as hours_executed_last_month,
					MAX(in_kpi_master.hours_executed_last_month2) as hours_executed_last_month2,
					MAX(in_kpi_master.active_pro_this_month) as active_pro_this_month,
					MAX(in_kpi_master.active_pro_last_month) as active_pro_last_month,
					MAX(in_kpi_master.active_pro_last_month2) as active_pro_last_month2,
					
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_this_month,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_last_month,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
						               THEN deep_churn1.value
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
					                  THEN deep_churn1.value
								         ELSE 0 END) 
											END) as churn_last_month2,
											
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date)::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_this_month€,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '1' month))::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_last_month€,
					SUM(CASE WHEN 
						         (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
						               THEN deep_churn1.money
									      ELSE 0 END) IS NULL THEN 0
								ELSE 
								   (CASE WHEN (deep_churn1.kpi = 'Churn' AND deep_churn1.sub_kpi_1 = 'Count opps' AND deep_churn1.year_month = LEFT((current_date - (interval '2' month))::text, 7)) 
					                  THEN deep_churn1.money
								         ELSE 0 END) 
											END) as churn_last_month2€,
					MAX(offers.offers_received_this_month) as offers_received_this_month,
					MAX(offers.offers_received_last_month) as offers_received_last_month,
					MAX(offers.offers_received_last_month2) as offers_received_last_month2,
					MAX(offers.offers_accepted_this_month) as offers_accepted_this_month,
					MAX(offers.offers_accepted_last_month) as offers_accepted_last_month,
					MAX(offers.offers_accepted_last_month2) as offers_accepted_last_month2,
					
					MAX(orders_checked.orders_submitted_this_month) as orders_submitted_this_month,
					MAX(orders_checked.orders_submitted_last_month) as orders_submitted_last_month,
					MAX(orders_checked.orders_submitted_last_month2) as orders_submitted_last_month2,
					MAX(orders_checked.orders_checked_this_month) as orders_checked_this_month,
					MAX(orders_checked.orders_checked_last_month) as orders_checked_last_month,
					MAX(orders_checked.orders_checked_last_month2) as orders_checked_last_month2,
					
					MAX(number_cases.cases_this_month) as cases_this_month,
					MAX(number_cases.cases_last_month) as cases_last_month,
					MAX(number_cases.cases_last_month2) as cases_last_month2
										
				FROM
					
					
					((SELECT -- Simple list of all the accounts with the role master
					
						a.sfid,
						a.delivery_areas__c,
						a.name as partner
					
					FROM
					
						salesforce.account a 
							
					WHERE
					
						a.test__c IS FALSE
						AND a.role__c = 'master') as t0
						
					LEFT JOIN
						
						(SELECT -- Query taking data regarding the partners from the table bi.kpi_master: Revenue, Cost Supply, Operational Costs, Hours Executed, Active Professionals
						
							o.date_part as year_month,
							o.sub_kpi_2 as partner2,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
										THEN o.value 
										ELSE 0 END) as revenue_last_month,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as revenue_last_month2,
							SUM(CASE WHEN (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as revenue_last_month3,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
											(CASE WHEN (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
					                        THEN o.value
								               ELSE 0 END) 
											      END) as cost_supply_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as operational_costs_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.sub_kpi_5 = 'Scorecard' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							                  THEN o.value
										         ELSE 0 END) 
													END) as operational_costs_last_month2,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_this_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_last_month,
							SUM(CASE WHEN 
								         (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
								               THEN o.value
											      ELSE 0 END) IS NULL THEN 0
										ELSE 
										   (CASE WHEN (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7)) 
							                  THEN o.value
										         ELSE 0 END) 
													END) as hours_executed_last_month2,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
										THEN o.value 
										ELSE 0 END) as active_pro_this_month,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
										THEN o.value 
										ELSE 0 END) as active_pro_last_month,
							SUM(CASE WHEN (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							         THEN o.value 
										ELSE 0 END) as active_pro_last_month2									
															
						FROM
						
							bi.kpi_master o
							
						WHERE
						
							((o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Revenue' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '3' month))::text, 7))
							OR (o.kpi = 'Cost Supply' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7) AND o.sub_kpi_5 = 'Scorecard')
							OR (o.kpi = 'Operational Costs' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7) AND o.sub_kpi_5 = 'Scorecard')
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7))
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Hours Executed' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '1' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date - (interval '2' month))::text, 7))
							OR (o.kpi = 'Active Professionals' AND o.sub_kpi_1 = 'Partner' AND o.date_part = LEFT((current_date)::text, 7)))
												
						GROUP BY
						
							o.date_part,
							partner2
							
						ORDER BY
						
							year_month desc,
							partner2 asc) as t1
							
					ON
					
						t0.partner = t1.partner2) as in_kpi_master
						
				LEFT JOIN
				
					bi.deep_churn as deep_churn1
					
				ON
				
					in_kpi_master.partner = deep_churn1.company_name
					AND in_kpi_master.year_month = deep_churn1."year_month"
					
				LEFT JOIN
				
					(SELECT  -- Query containing the number of offers sent to partners, and offers accepted by partners every month in the last 3 months
					
						offers1.year_month_offer,
						offers1.sfid,
						offers1.name as company_name__c,
						SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_this_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '1' month))::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '2' month))::text, 7) THEN offers1.offers_received ELSE 0 END) as offers_received_last_month2,
						SUM(CASE WHEN offers1.year_month_offer = LEFT(current_date::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_this_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '1' month))::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month,
						SUM(CASE WHEN offers1.year_month_offer = LEFT((current_date - (interval '2' month))::text, 7) THEN offers1.offers_accepted ELSE 0 END) as offers_accepted_last_month2
						
					
					FROM
					
						(SELECT -- Query containing the number of offers sent to partners, and offers accepted by partners every month in the last 3 months
										
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') as year_month_offer,
							MIN(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END) as mindate,
							a.sfid,
							a.name,
							a.company_name__c,
							-- a.delivery_areas__c,
							a.type__c,
							a.status__c,
							a.role__c,
							COUNT(o.suggested_partner__c) as offers_received,
							SUM(CASE WHEN o.accepted__c IS NULL THEN 0 ELSE 1 END) as offers_accepted
						
						
						FROM
						
							salesforce.account a
						
						
						LEFT JOIN
						
							salesforce.partner_offer_partner__c o
								
						ON
						
							a.sfid = o.suggested_partner__c 
							
						LEFT JOIN
						
							salesforce.partner_offer__c oo
							
						ON 
						
							o.partner_offer__c = oo.sfid
							
						LEFT JOIN
						
							salesforce.opportunity ooo
							
						ON
						
							oo.opportunity__c = ooo.sfid
							
						WHERE
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') IS NOT NULL
							AND a.test__c IS FALSE
							AND o.createddate >= (current_date - '3 MONTHS'::INTERVAL) -- Only offers created in the last 3 months
							
						GROUP BY
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM'),
							a.sfid,
							a.name,
							a.company_name__c,
							-- a.delivery_areas__c,
							a.type__c,
							a.status__c,
							a.role__c
						
						ORDER BY
						
							TO_CHAR(CASE WHEN o.sent__c IS NULL THEN o.createddate ELSE o.sent__c END, 'YYYY-MM') desc,
							a.name asc) as offers1
							
					GROUP BY
					
						offers1.year_month_offer,
						offers1.sfid,
						offers1.name) as offers
						
				ON
				
					in_kpi_master.partner = offers.company_name__c
					AND in_kpi_master.year_month = offers.year_month_offer
					
				LEFT JOIN
				
					(SELECT -- Query extracting the numbers of orders planned for the partners and the orders checked by the partners
					
						orders_checked1.year_month,
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_this_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN orders_checked1.orders_submitted ELSE 0 END) as orders_submitted_last_month2,
						SUM(CASE WHEN orders_checked1.year_month = LEFT(current_date::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_this_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month,
						SUM(CASE WHEN orders_checked1.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN orders_checked1.orders_checked ELSE 0 END) as orders_checked_last_month2
					
					FROM	
						
						(SELECT -- Query extracting the numbers of orders planned for the partners and the orders checked by the partners
												
						  TO_CHAR(effectivedate::date, 'YYYY-MM') as year_month,
						  MIN(effectivedate::date) as mindate,
						  MAX(effectivedate::date) as maxdate,
						  t3.subcon as name_partner,
						  t3.sfid_partner,
						  SUM(CASE WHEN (t4.status NOT LIKE '%ERROR%' OR t4.status NOT LIKE '%MISTAKE%') THEN 1 ELSE 0 END) as orders_submitted,
						  SUM(CASE WHEN t4.quick_note__c LIKE 'Partner Portal: Status changed to%' THEN 1 ELSE 0 END) as orders_checked
						
						FROM
						
						  (SELECT
						  
						     t5.sfid,
						     t5.name,
						     t2.name as subcon,
						     t2.sfid as sfid_partner
						     
						   FROM
						   
						   	Salesforce.Account t5
						   
							JOIN
						      
								Salesforce.Account t2
						   
							ON
						   
								(t2.sfid = t5.parentid)
						     
							WHERE 
							
								t5.status__c not in ('SUSPENDED') and t5.test__c = '0' and t5.name not like '%test%'
								-- AND t5.type__c = 'partner'
								AND LEFT(t5.locale__c, 2) = 'de'
								-- AND t5.role__c = 'master'
								AND t5.company_name__c NOT LIKE '%Handyman Uwe Stamm%' 
								AND t5.name NOT LIKE '%Handyman Kovacs%'
								AND t5.name NOT LIKE '%BAT Business Services GmbH%'
						   	and (t5.type__c like 'cleaning-b2c' or (t5.type__c like '%cleaning-b2c;cleaning-b2b%') or t5.type__c like 'cleaning-b2b')
						   	and t2.name NOT LIKE '%BAT Business Services GmbH%') t3
						      
						JOIN 
						
						  salesforce.order t4
						  
						ON
						
						  (t3.sfid = t4.professional__c)
						  
						WHERE
						
						  (t4.status IN ('INVOICED', 'FULFILLED', 'NOSHOW CUSTOMER', 'PENDING TO START', 'NOSHOW PROFESSIONAL', 'CANCELLED NOMANPOWER')
						  OR status LIKE '%MISTAKE%' OR status LIKE '%ERROR%')
						  and LEFT(t4.locale__c,2) IN ('de')
						  and t4.effectivedate < current_date
						  AND t4.type = 'cleaning-b2b'
						  AND t4.test__c IS FALSE
						
						GROUP BY
						
						  year_month,
						  t3.subcon,
						  t3.sfid_partner
						  
						  
						ORDER BY
							
						  year_month desc,
						  t3.subcon) as orders_checked1
						  
					GROUP BY
					
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.year_month
						
					ORDER BY
					
						orders_checked1.sfid_partner,
						orders_checked1.name_partner,
						orders_checked1.year_month desc) as orders_checked
						
				ON
				
					in_kpi_master.partner = orders_checked.name_partner
					AND in_kpi_master.year_month = orders_checked.year_month
					
				LEFT JOIN
				
					(SELECT -- Query extracting the cases of the partners (improvements, bad feedbacks...) in the last 3 months
					
						t3.year_month,
						t3.partnerid,
						t4.name as name_partner,
						SUM(CASE WHEN t3.year_month = LEFT((current_date)::text, 7) THEN t3.number_cases ELSE 0 END) as cases_this_month,
						SUM(CASE WHEN t3.year_month = LEFT((current_date - (interval '1' month))::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month,
						SUM(CASE WHEN t3.year_month = LEFT((current_date - (interval '2' month))::text, 7) THEN t3.number_cases ELSE 0 END) as cases_last_month2
						
					FROM
								
						(SELECT
						
							TO_CHAR(t2.date_case, 'YYYY-MM') as year_month,
							CASE WHEN t2.parentid IS NULL THEN t2.partner ELSE t2.parentid END as partnerid,
							t2.name_partner,
							t2.parentid,
							COUNT(DISTINCT t2.sfid_case) as number_cases
						
						FROM	
						
							(SELECT
							
								t1.date_case as date_case,
								CASE WHEN t1.accountid IS NULL THEN t1.partner__c ELSE t1.accountid END as partner,
								t1.sfid_case,
								a.name as name_partner,
								a.parentid
								
							FROM	
								
								(SELECT -- Query containing the cases related to partners 
								
									c.createddate as date_case,
									c.CaseNumber, 
									c.sfid as sfid_case, 
									o.sfid, 
									a.sfid,
									c.origin, 
									c.*
								
								FROM 
								
									salesforce.case c
									
								LEFT JOIN 
								
									salesforce.opportunity o
								
								ON
									o.sfid = c.opportunity__c
									
								LEFT JOIN 
								
									salesforce.account a on a.sfid = c.accountid
									
								WHERE 
								
									c.Reason IN ('Feedback / Complaint', 'Order - Feedback / Complaint', 'Partner - Improvement') -- Only bad cases
									AND COALESCE(c.Origin, '') != 'System - Notification'
									AND COALESCE(c.subject, '') NOT IN ('Satisfaction Feedback: 5', 'Satisfaction Feedback: 4') -- We take only bad feedbacks into account
									AND c.type != 'CM B2C' -- Only B2B related
									AND c.CreatedDate >= (current_date - '3 MONTHS'::INTERVAL) -- We consider only the last 3 months
									AND c.test__c IS FALSE
									AND a.test__c IS FALSE
									AND COALESCE(o.name, '') NOT LIKE '%test%' 
									-- SFID from ## account
									AND COALESCE(c.accountid, '') != '0012000001APUlvAAH'
									-- SFID from BAT Business Services GmbH account
									AND COALESCE(a.parentid, '') != '0012000001TDMgGAAX'
									AND c.parentid IS NULL) as t1
									
							LEFT JOIN
							
								salesforce.account a
								
							ON
							
								(t1.accountid = a.sfid)
								-- OR t1.partner__c = a.sfid)
								
							WHERE 
							
								a.name IS NOT NULL) as t2
								
						GROUP BY 
						
							TO_CHAR(t2.date_case, 'YYYY-MM'),
							t2.partner,
							t2.name_partner,
							t2.parentid
							
						ORDER BY
						
							t2.name_partner,
							TO_CHAR(t2.date_case, 'YYYY-MM') desc) as t3
							
					LEFT JOIN
					
						salesforce.account t4
						
					ON
					
						t3.partnerid = t4.sfid
						
					GROUP BY
					
						t3.year_month,
						t3.partnerid,
						t4.name) as number_cases
						
				ON
				
					in_kpi_master.partner = number_cases.name_partner
					AND in_kpi_master.year_month = number_cases.year_month	
				
				GROUP BY
				
					in_kpi_master.sfid,
					in_kpi_master.partner,
					in_kpi_master.year_month
					
				ORDER BY
				
					in_kpi_master.sfid,
					in_kpi_master.year_month desc) as block1
		
			WHERE
		
				block1.year_month IS NOT NULL
						
			GROUP BY
			
				block1.sfid,
				block1.partner) as block2
		
		WHERE
		
			block2.sfid NOT LIKE '0012000001TDMgGAAX') as block3) as block4

LEFT JOIN

	(SELECT
	
		t3.served_by__c as sfid_partner,
		t3.partner,
		SUM(t3.final_partner_cost) as total_cost
	
	FROM	
		
		(SELECT	
			
			t1.*,
			t2.hours,
			CASE WHEN t1.partner_costs = 0 THEN t2.hours*t1.pph_partner ELSE t1.partner_costs END as final_partner_cost
			
		FROM
		
			(SELECT
			
				o.sfid as sfid_opp,
				o.name as name_opp,
				o.delivery_area__c,
				o.served_by__c,
				a.name as partner,
				a.pph__c as pph_partner,
				o.plan_pph__c,
				SUM(cont.grand_total__c) as grand_total,
				SUM(o.potential_partner_costs__c) as partner_costs
				
			FROM
			
				salesforce.opportunity o
				
			LEFT JOIN
			
				salesforce.contract__c cont
				
			ON
			
				o.sfid = cont.opportunity__c
				
			LEFT JOIN
			
				salesforce.account a
				
			ON
			
				o.served_by__c = a.sfid
				
			WHERE
			
				o.status__c NOT IN ('RESIGNED', 'CANCELLED')
				AND cont.status__c NOT IN ('RESIGNED', 'CANCELLED')
				AND o.test__c IS FALSE
				AND cont.test__c IS FALSE
				AND cont.active__c IS TRUE
				AND o.served_by__c IS NOT NULL
				AND a.name NOT LIKE '%BAT %'
				
			GROUP BY
			
				o.sfid,
				o.name,
				o.delivery_area__c,
				o.served_by__c,
				o.plan_pph__c,
				a.pph__c,
				a.name) as t1
				
		LEFT JOIN
		
			(SELECT
		
				o.opportunityid,
				SUM(o.order_duration__c) as hours
			
			
			FROM
			
				salesforce.order o
				
			WHERE
			
				o.test__c IS FALSE
				AND o."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START')
				AND o.effectivedate BETWEEN (CURRENT_DATE - INTERVAL '0 DAY') - INTERVAL '4 Weeks' AND (CURRENT_DATE - INTERVAL '0 DAY')
				
			GROUP BY
			
				o.opportunityid) as t2
				
		ON
		
			t1.sfid_opp = t2.opportunityid) as t3
			
	GROUP BY	
	
		t3.served_by__c,
		t3.partner) as block5
		
ON

	block4.sfid = block5.sfid_partner;
				
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.share_digital_partners;
CREATE TABLE bi.share_digital_partners as 

SELECT

	digital_activity.year_week,
	MIN(digital_activity.mindate) as mindate,
	COUNT(DISTINCT digital_activity.sfid_partner) as partners_digitaly_active,
	MAX(operational_activity2.partners_op_active) - COUNT(DISTINCT digital_activity.sfid_partner) as partners_digitaly_inactive,
	MAX(operational_activity2.partners_op_active) as partners_op_active,
	COUNT(DISTINCT digital_activity.sfid_partner)/MAX(operational_activity2.partners_op_active)::decimal as share_digitaly_active

FROM	
	
	(SELECT
	
		TO_CHAR(portal.created_at, 'YYYY-WW') as year_week,
		MIN(portal.created_at) as mindate,
		list_active_partners.partner as sfid_partner,
		list_active_partners.name as partner_name,
		CASE WHEN portal.is_tfs_user IS TRUE THEN 'TFS Agent' ELSE 'Partner' END as tfs_user,
		
		CASE WHEN portal.event_name = 'job_marked_fulfilled' THEN 'Job Marked Fulfilled'
			  WHEN portal.event_name = 'job_marked_not_fulfilled' THEN 'Job Marked Not Fulfilled'
			  WHEN portal.event_name = 'job_comment_added' THEN 'Job Comment Added'
			  
			  WHEN portal.event_name = 'plan_adjustment_confirmed' THEN 'Plan Adjustment Confirmed'
			  WHEN portal.event_name = 'order_adjustment_confirmed' THEN 'Order Adjustment Confirmed'
			  
			  WHEN portal.event_name = 'professional_create' THEN 'Professional Create'
			  WHEN portal.event_name = 'professional_sms_sent' THEN 'Professional SMS Sent'
			  WHEN portal.event_name = 'professional_profile_changes_saved' THEN 'Professional Profile Changes Saved'
			  
			  WHEN portal.event_name = 'offer_accepted' THEN 'Offer Accepted'
			  WHEN portal.event_name = 'offer_pending_accepted' THEN 'Offer Pending Accepted'
			  
			  ELSE 'Unknown'
			  END as event_name,
		COUNT(DISTINCT portal.id) as number_actions
	
	FROM	
		
		(SELECT
		
			o.served_by__c as partner,
			a.name,
			MIN(o.effectivedate) as first_order
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.served_by__c = a.sfid
			
		WHERE
		
			o.test__c IS FALSE
			AND a.test__c IS FALSE
			AND o."type" = 'cleaning-b2b'
			AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
			AND o.opportunityid IS NOT NULL
			-- AND o.effectivedate > (current_date - 30)
			AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
			
		GROUP BY
		
			o.served_by__c,
			a.name) as list_active_partners
			
	LEFT JOIN
	
		events.tfs portal
		
	ON
	
		list_active_partners.partner = portal.subject
		
	WHERE
	
		portal.app = 'PARTNER_PORTAL'
		AND portal.is_tfs_user IS FALSE
		
	GROUP BY
	
		TO_CHAR(portal.created_at, 'YYYY-WW'),
		list_active_partners.partner,
		list_active_partners.name,
		portal.is_tfs_user,
		portal.event_name) as digital_activity
		
LEFT JOIN

	(SELECT
	
		operational_activity.year_week,
		MIN(operational_activity.mindate) as mindate,
		COUNT(DISTINCT operational_activity.partner) as partners_op_active
		
	FROM
		
		(SELECT
		
			TO_CHAR(o.effectivedate, 'YYYY-WW') as year_week,
			MIN(o.effectivedate) as mindate,
			o.served_by__c as partner,
			a.name,
			COUNT(DISTINCT o.sfid) as number_orders
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.served_by__c = a.sfid
			
		WHERE
		
			o.test__c IS FALSE
			AND a.test__c IS FALSE
			AND o."type" = 'cleaning-b2b'
			AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
			AND o.opportunityid IS NOT NULL
			AND o.effectivedate <= current_date
			AND o.effectivedate > '2019-03-31'
			AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
			
		GROUP BY
		
			TO_CHAR(o.effectivedate, 'YYYY-WW'),
			o.served_by__c,
			a.name
			
		ORDER BY
		
			TO_CHAR(o.effectivedate, 'YYYY-WW') desc) as operational_activity
			
	GROUP BY
	
		operational_activity.year_week
		
	ORDER BY
	
		operational_activity.year_week desc) as operational_activity2
		
ON

	digital_activity.year_week = operational_activity2.year_week

GROUP BY

	digital_activity.year_week;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.digital_behavior;
CREATE TABLE bi.digital_behavior as

SELECT

	operational_activity.year_week,
	MIN(operational_activity.mindate) as mindate,
	operational_activity.sfid_partner,
	operational_activity.name_partner,
	digital_activity.event_name,
	MAX(CASE WHEN digital_activity.number_actions IS NULL THEN 0 ELSE digital_activity.number_actions END) as number_actions,
	MAX(CASE WHEN operational_activity.number_orders IS NULL THEN 0.1 ELSE operational_activity.number_orders END) as number_orders

FROM	


	(SELECT
	
		TO_CHAR(o.effectivedate, 'YYYY-WW') as year_week,
		MIN(o.effectivedate) as mindate,
		o.served_by__c as sfid_partner,
		a.name as name_partner,
		COUNT(DISTINCT o.sfid) as number_orders
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.account a
		
	ON
	
		o.served_by__c = a.sfid
		
	WHERE
	
		o.test__c IS FALSE
		AND a.test__c IS FALSE
		AND o."type" = 'cleaning-b2b'
		AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
		AND o.opportunityid IS NOT NULL
		AND o.effectivedate <= current_date
		AND o.effectivedate > '2019-03-31'
		AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
		
	GROUP BY
	
		TO_CHAR(o.effectivedate, 'YYYY-WW'),
		o.served_by__c,
		a.name
		
	ORDER BY
	
		TO_CHAR(o.effectivedate, 'YYYY-WW') desc) as operational_activity
			
LEFT JOIN
	
	(SELECT
	
		TO_CHAR(portal.created_at, 'YYYY-WW') as year_week,
		MIN(portal.created_at) as mindate,
		list_active_partners.partner as sfid_partner,
		list_active_partners.name as partner_name,
		CASE WHEN portal.is_tfs_user IS TRUE THEN 'TFS Agent' ELSE 'Partner' END as tfs_user,
		
		CASE WHEN portal.event_name = 'job_marked_fulfilled' THEN 'Job Marked Fulfilled'
			  WHEN portal.event_name = 'job_marked_not_fulfilled' THEN 'Job Marked Not Fulfilled'
			  WHEN portal.event_name = 'job_comment_added' THEN 'Job Comment Added'
			  
			  WHEN portal.event_name = 'plan_adjustment_confirmed' THEN 'Plan Adjustment Confirmed'
			  WHEN portal.event_name = 'order_adjustment_confirmed' THEN 'Order Adjustment Confirmed'
			  
			  WHEN portal.event_name = 'professional_create' THEN 'Professional Create'
			  WHEN portal.event_name = 'professional_sms_sent' THEN 'Professional SMS Sent'
			  WHEN portal.event_name = 'professional_profile_changes_saved' THEN 'Professional Profile Changes Saved'
			  
			  WHEN portal.event_name = 'offer_accepted' THEN 'Offer Accepted'
			  WHEN portal.event_name = 'offer_pending_accepted' THEN 'Offer Pending Accepted'
			  
			  ELSE 'Unknown'
			  END as event_name,
		COUNT(DISTINCT portal.id) as number_actions
	
	FROM	
		
		(SELECT
		
			o.served_by__c as partner,
			a.name,
			MIN(o.effectivedate) as first_order
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.account a
			
		ON
		
			o.served_by__c = a.sfid
			
		WHERE
		
			o.test__c IS FALSE
			AND a.test__c IS FALSE
			AND o."type" = 'cleaning-b2b'
			AND o."status" NOT IN ('CANCELLED TERMINATED', 'CANCELLED MISTAKE', 'CANCELLED FAKED')
			AND o.opportunityid IS NOT NULL
			-- AND o.effectivedate > (current_date - 30)
			AND o.served_by__c NOT LIKE '0012000001TDMgGAAX'
			
		GROUP BY
		
			o.served_by__c,
			a.name) as list_active_partners
			
	LEFT JOIN
	
		events.tfs portal
		
	ON
	
		list_active_partners.partner = portal.subject
		
	WHERE
	
		portal.app = 'PARTNER_PORTAL'
		AND portal.is_tfs_user IS FALSE
		
	GROUP BY
	
		TO_CHAR(portal.created_at, 'YYYY-WW'),
		list_active_partners.partner,
		list_active_partners.name,
		portal.is_tfs_user,
		portal.event_name) as digital_activity
				
ON

	digital_activity.year_week = operational_activity.year_week
	AND digital_activity.sfid_partner = operational_activity.sfid_partner
	AND digital_activity.partner_name = operational_activity.name_partner

GROUP BY

	operational_activity.year_week,
	operational_activity.sfid_partner,
	operational_activity.name_partner,
	digital_activity.event_name;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cohorts_starters;
CREATE TABLE bi.cohorts_starters as

SELECT

	t2.sfid,
	t2.potential,
	t2.grand_total__c,
	t2.year_month_closed,
	t2.closedate,
	CASE WHEN 
	         (CASE WHEN t2.valid_orders = 0 THEN 'Never Started' ELSE 'Started' END) = 'Never Started'
	     THEN NULL
	     ELSE first_order
	     END as first_order,
   t2.valid_orders,
   t2.invalid_orders,
   t2.revenue,
	CASE WHEN t2.valid_orders = 0 THEN 'Never Started' ELSE 'Started' END as status_start

FROM	
	
	(SELECT
	
		t1.*,
		CASE WHEN t1.grand_total__c IS NULL THEN t1.potential ELSE t1.grand_total__c END as revenue
	
	FROM	
		
		(SELECT
		
			o.sfid,
			pot.potential,
			o.grand_total__c,
			TO_CHAR(o.closedate,'YYYY-MM') as year_month_closed,
			o.closedate,
			MIN(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN (oo.effectivedate + 10000)
			         ELSE oo.effectivedate 
						END) as first_order,
			SUM(CASE WHEN oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as valid_orders,
		   SUM(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as invalid_orders
		
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp pot
			
		ON
		
			o.sfid = pot.opportunityid
			
		LEFT JOIN
		
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c
			
		WHERE
		
			o.test__c IS FALSE
			AND LEFT(o.locale__C, 2) = 'de'
			-- AND oo."status" NOT IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND o.stagename IN ('WON', 'PENDING')
			AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
			
			o.sfid,
			o.grand_total__c,
			TO_CHAR(o.closedate,'YYYY-MM'),
			o.closedate,
			pot.potential) AS t1) as t2;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.cohorts_starters_stacks;
CREATE TABLE bi.cohorts_starters_stacks as

SELECT

	t2.sfid,
	t2.potential,
	t2.grand_total__c,
	t2.year_month_closed,
	t2.closedate,
	t2.year_month_active,
	t2.mindate_active,
	CASE WHEN 
	         (CASE WHEN t2.valid_orders = 0 THEN 'Never Started' ELSE 'Started' END) = 'Never Started'
	     THEN NULL
	     ELSE first_order
	     END as first_order,
   t2.valid_orders,
   t2.invalid_orders,
   t2.revenue,
	CASE WHEN t2.valid_orders = 0 THEN 'Never Started' ELSE 'Started' END as status_start

FROM	
	
	(SELECT
	
		t1.*,
		CASE WHEN t1.grand_total__c IS NULL THEN t1.potential ELSE t1.grand_total__c END as revenue
	
	FROM	
		
		(SELECT
		
			o.sfid,
			pot.potential,
			o.grand_total__c,
			TO_CHAR(o.closedate,'YYYY-MM') as year_month_closed,
			o.closedate,
			TO_CHAR(oo.effectivedate ,'YYYY-MM') as year_month_active,
			MIN(oo.effectivedate) as mindate_active,
			MIN(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN (oo.effectivedate + 365)
			         ELSE oo.effectivedate 
						END) as first_order,
			SUM(CASE WHEN oo."status" IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as valid_orders,
		   SUM(CASE WHEN oo."status" NOT IN ('INVOICED', 'FULFILLED', 'PENDING TO START') THEN 1 ELSE 0 END) as invalid_orders
		
		FROM
		
			salesforce.opportunity o
			
		LEFT JOIN
		
			salesforce.order oo
			
		ON
		
			o.sfid = oo.opportunityid
			
		LEFT JOIN
		
			bi.potential_revenue_per_opp pot
			
		ON
		
			o.sfid = pot.opportunityid
			
		LEFT JOIN
		
			salesforce.contract__c cont
			
		ON
		
			o.sfid = cont.opportunity__c
			
		WHERE
		
			o.test__c IS FALSE
			AND LEFT(o.locale__C, 2) = 'de'
			-- AND oo."status" NOT IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND o.stagename IN ('WON', 'PENDING')
			AND cont.service_type__c = 'maintenance cleaning'
			
		GROUP BY
			
			o.sfid,
			o.grand_total__c,
			TO_CHAR(o.closedate,'YYYY-MM'),
			TO_CHAR(oo.effectivedate ,'YYYY-MM'),
			o.closedate,
			pot.potential) AS t1) as t2;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DROP TABLE IF EXISTS bi.forecast_revenue;
CREATE TABLE bi.forecast_revenue as 

SELECT

	'Churned Customers Last Month' as kpi,
	o.sfid,
	o.name,
	o.delivery_area__c,
	ooo.service_type__c,
	-- oo.confirmed_end__c,
	o.grand_total__c,
	-- SUM(ooo.amount__c)/1.19 amount_invoiced,

	-SUM(ooo.amount__c)/1.19 as on_top_this_month

FROM

	salesforce.opportunity o
	
LEFT JOIN

	salesforce.contract__c oo
	
ON

	o.sfid = oo.opportunity__c
	
LEFT JOIN

	salesforce.invoice__c ooo
	
ON

	o.sfid = ooo.opportunity__c

WHERE

	o.test__c IS FALSE
	AND o.status__c IN ('RESIGNED', 'CANCELLED')
	AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
	-- AND ooo.issued__c::text IS NULL
	AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND oo.service_type__c = 'maintenance cleaning'
	AND ooo.service_type__c = 'maintenance cleaning'
	
GROUP BY

	o.sfid,
	o.name,
	o.delivery_area__c,
	ooo.service_type__c,
	o.grand_total__c
	-- oo.confirmed_end__c
	;
	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
	
SELECT

	'Churned Customers This month' as kpi,
	t1.sfid,
	t1.name,
	t1.delivery_area__c,
	t1.service_type__c,
	t1.grand_total__c,
	t1.on_top_this_month

FROM	
	
	(SELECT
	
		o.sfid,
		o.name,
		o.delivery_area__c,
		ooo.service_type__c,
		o.grand_total__c,
		oo.pph__c,
		SUM(ooo.amount__c)/1.19 amount_invoiced_last_month,
		oo.confirmed_end__c,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		EXTRACT(DAY FROM oo.confirmed_end__c) as days_to_invoice,
		CASE WHEN o.grand_total__c IS NOT NULL THEN
		          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     END as to_pay_this_month,
		CASE WHEN o.grand_total__c IS NOT NULL THEN
		          (o.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     ELSE ((SUM(ooo.amount__c)/1.19)/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*EXTRACT(DAY FROM oo.confirmed_end__c)
		     END
		- SUM(ooo.amount__c)/1.19 as on_top_this_month
	
	FROM
	
		salesforce.opportunity o
		
	LEFT JOIN
	
		salesforce.contract__c oo
		
	ON
	
		o.sfid = oo.opportunity__c
		
	LEFT JOIN
	
		salesforce.invoice__c ooo
		
	ON
	
		o.sfid = ooo.opportunity__c
		
	WHERE
	
		o.test__c IS FALSE
		AND o.status__c IN ('RESIGNED', 'CANCELLED', 'OFFBOARDING')
		AND oo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		-- AND ooo.issued__c::text IS NULL
		AND LEFT(oo.confirmed_end__c::text, 7) = LEFT((current_date)::text, 7)
		AND LEFT(ooo.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
		AND oo.service_type__c = 'maintenance cleaning'
		AND ooo.service_type__c = 'maintenance cleaning'
		
	GROUP BY
	
		o.sfid,
		o.name,
		o.delivery_area__c,
		ooo.service_type__c,
		o.grand_total__c,
		oo.pph__c,
		oo.confirmed_end__c) as t1;
		
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
		
SELECT

	'New Customers Last Month' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	inv.service_type__c,
	-- t1.confirmed_end__c,
	-- t1.first_order,	
	t1.grand_total__c,
	-- SUM(inv.amount__c)/1.19 as invoiced_last_month,
	t1.grand_total__c - SUM(inv.amount__c)/1.19 as on_top_this_month
	
FROM	
	
	(SELECT
	
		o.opportunityid,
		oo.delivery_area__c,
		oo.name,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c,
		MIN(o.effectivedate) as first_order
	
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN

		salesforce.contract__c ooo
	
	ON

		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		oo.grand_total__c,
		ooo.start__c,
		ooo.duration__c,
		ooo.end__c,
		ooo.resignation_date__c,
		ooo.confirmed_end__c) as t1
		
LEFT JOIN

	salesforce.invoice__c inv
	
ON

	t1.opportunityid = inv.opportunity__c
		
WHERE
	
	LEFT(t1.first_order::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND LEFT(inv.issued__c::text, 7) = LEFT((current_date - interval '1 month')::text, 7)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7)
	     OR LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
	     OR t1.confirmed_end__c IS NULL)
	AND (LEFT(t1.confirmed_end__c::text, 7) != LEFT((current_date - interval '1 month')::text, 7) OR t1.confirmed_end__c IS NULL)
	AND inv.service_type__c = 'maintenance cleaning'
	
GROUP BY
	
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.grand_total__c,
	inv.service_type__c
	-- t1.confirmed_end__c
	;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'New Customers This month' as kpi,
	t2.opportunityid,
	t2.name,
	t2.delivery_area__c,
	t2.service_type__c,
	t2.grand_total__c,
	t2.on_top_this_month

FROM		
	
	(SELECT
	
		t1.opportunityid,
		t1.name,
		t1.delivery_area__c,
		t1.service_type__c,
		t1.confirmed_end__c,
		t1.first_order,
		t1.grand_total__c,
		EXTRACT(DAY FROM t1.first_order) as day_first_order,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
		DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1 as days_to_invoice,
		(t1.grand_total__c/DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL))*(DATE_PART('days', DATE_TRUNC('month', NOW()) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.first_order) + 1) as on_top_this_month
	        	
	FROM	
		
		(SELECT
		
			o.opportunityid,
			oo.name,
			oo.delivery_area__c,
			ooo.service_type__c,
			oo.grand_total__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c,
			MIN(o.effectivedate) as first_order
		
		
		FROM
		
			salesforce.order o
			
		LEFT JOIN
		
			salesforce.opportunity oo
			
		ON
		
			o.opportunityid = oo.sfid
		
		LEFT JOIN
	
			salesforce.contract__c ooo
		
		ON
	
			o.opportunityid = ooo.opportunity__c
			
		WHERE
		
			o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
			AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
			AND LEFT(o.locale__c, 2) = 'de'
			-- AND (ooo.active__c IS TRUE )
			AND ooo.service_type__c = 'maintenance cleaning'
			AND oo.test__c IS FALSE
			AND (LEFT(ooo.confirmed_end__c::text, 7) != LEFT((current_date)::text, 7)
		       OR ooo.confirmed_end__c IS NULL)
			    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
					
		GROUP BY
		
			o.opportunityid,
			oo.name,
			oo.grand_total__c,
			oo.delivery_area__c,
			ooo.service_type__c,
			ooo.start__c,
			ooo.duration__c,
			ooo.end__c,
			ooo.resignation_date__c,
			ooo.confirmed_end__c) as t1
			
	WHERE
		
		LEFT(t1.first_order::text, 7) = LEFT((current_date)::text, 7)
		
	GROUP BY
		
		t1.opportunityid,
		t1.name,
		t1.grand_total__c,
		t1.delivery_area__c,
		t1.service_type__c,
		t1.confirmed_end__c,
		t1.first_order) as t2;
	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
		
SELECT

	'Difference PPH' as kpi,
	t1.opportunityid,
	t1.name,
	t1.delivery_area__c,
	t1.service_type__c,
	t1.pph__c,
	t1.invoiced_oct - t1.invoiced_sept as on_top_this_month
	
FROM

	(SELECT
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		ooo.service_type__c,
		oo.grand_total__c,
		ooo.pph__c,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END) as hours_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-07' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_july,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END) as hours_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-08' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_august,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END) as hours_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-09' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_sept,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END) as hours_oct,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-10' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_oct,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-11' THEN o.order_duration__c ELSE 0 END) as hours_nov,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-11' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_nov,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-12' THEN o.order_duration__c ELSE 0 END) as hours_dec,
		SUM(CASE WHEN LEFT(o.effectivedate::text, 7) = '2019-12' THEN o.order_duration__c ELSE 0 END)*ooo.pph__c as invoiced_dec
	
	FROM
	
		salesforce.order o
		
	LEFT JOIN
	
		salesforce.opportunity oo
		
	ON
	
		o.opportunityid = oo.sfid
	
	LEFT JOIN
	
		salesforce.contract__c ooo
	
	ON
	
		o.opportunityid = ooo.opportunity__c
		
	WHERE
	
		o.status IN ('INVOICED', 'PENDING TO START', 'FULFILLED')
		AND ooo.status__c NOT IN ('CANCELLED MISTAKE', 'EXPIRED', 'DECLINED')
		AND LEFT(o.locale__c, 2) = 'de'
		-- AND (ooo.active__c IS TRUE )
		AND ooo.service_type__c = 'maintenance cleaning'
		AND oo.test__c IS FALSE
		AND oo.grand_total__c IS NULL
		AND ooo.pph__c IS NOT NULL
		    -- OR (ooo.status__c IN ('RESIGNED' or 'CANCELLED') AND ooo.service_type__c = 'maintenance cleaning'))
				
	GROUP BY
	
		o.opportunityid,
		oo.name,
		oo.delivery_area__c,
		ooo.service_type__c,
		oo.grand_total__c,
		ooo.pph__c) as t1;		

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Invoiced Maintenance Cleaning Last Month' as kpi,
	inv.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END as grand_total,
	SUM(inv.amount__c)/1.19 as revenue_last_month


FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND CASE WHEN RIGHT(current_date::text, 2)::integer < 5
	         THEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - '2 MONTH'::INTERVAL)::text, 7) 
	         ELSE LEFT(inv.issued__c::text, 7) = LEFT((current_date - '1 MONTH'::INTERVAL)::text, 7) 
	         END 
	AND inv.parent_invoice__c IS NULL
	AND inv.service_type__c LIKE 'maintenance cleaning'
	
GROUP BY

	inv.opportunity__c,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Cancellations This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	-12039 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Other Discounts This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	-7817 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Extra Orders This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	1666 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Manual Invoices This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	3740 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;


---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Blocked Not Invoiced This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	-1454 as on_top_this_month -- Average of the last 3 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Grand Total Adjustments This Month' as kpi,
	'All' as sfid,
	'All' as name,
	'All' as delivery_area__c,
	'maintenance cleaning' as service_type__c,
	NULL as grand_total__c,
	1143 as on_top_this_month -- Average of the last 6 months. File Invoice Analysis_sep2019
	
FROM

	salesforce.contact
	
LIMIT 1;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue
	
SELECT

	'Extra Services This Month' as kpi,
	t3.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	t3.service_type__c,
	t3.money as grand_total,
	SUM(t3.invoiced_this_month) as on_top_this_month

FROM	
			
	(SELECT
	
		TO_CHAR(NOW(), 'YYYY-MM') as this_month,
		t2.sfid,
		t2.opportunity__c,
		t2.name,
		t2.service_type__c,
		t2.recurrency__c,
		t2.note__c,
		t2.date_start,
		t2.money,
		t2.number_days_this_month,
		t2.days_to_invoice,
		CASE WHEN TO_CHAR(t2.date_start, 'YYYY-MM') = TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('water dispenser', 'coffee machine')
			  THEN t2.days_to_invoice*t2.money/30.4
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('water dispenser', 'coffee machine')
			  THEN t2.money
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('fruits') AND t2.recurrency__c = 'weekly'
			  THEN t2.money*cal.mondays
			  
			  WHEN TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c IN ('fruits') AND t2.recurrency__c = 'biweekly'
			  THEN t2.money*cal.mondays/2
			  
			  WHEN (TO_CHAR(t2.date_start, 'YYYY-MM') != TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c NOT IN ('water dispenser', 'coffee machine', 'fruits') AND t2.note__c LIKE '%' || TO_CHAR(NOW(), 'YYYY-MM') || '%'
			       OR TO_CHAR(t2.date_start, 'YYYY-MM') = TO_CHAR(NOW(), 'YYYY-MM') AND t2.service_type__c NOT IN ('water dispenser', 'coffee machine', 'fruits'))
			  THEN t2.money
			  
			  WHEN t2.sfid = 'a1C0J000008hzECUAY' THEN 8855
			  
			  WHEN t2.sfid = 'a1C0J000008E8mMUAS' THEN 932.4
			  
			  WHEN t2.sfid = 'a1C0J000009ZpvyUAC' THEN 7*16
			  
			  ELSE 0
			  
			  END as invoiced_this_month
	
	FROM	
		
		(SELECT	
			
			t1.sfid,
			t1.opportunity__c,
			t1.name,
			t1.service_type__c,
			t1.recurrency__c,
			t1.note__c,
			t1.date_start,
			t1.money,
			DATE_PART('days', DATE_TRUNC('month', t1.date_start) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) as number_days_this_month,
			DATE_PART('days', DATE_TRUNC('month', t1.date_start) + '1 MONTH'::INTERVAL - '1 DAY'::INTERVAL) - EXTRACT(DAY FROM t1.date_start) + 1 as days_to_invoice		
			
		FROM
			
			(SELECT
			
				o.sfid,
				o.opportunity__c,
				o.name,
				o.service_type__c,
				o.recurrency__c,
				o.note__c,
				CASE WHEN o.effective_start__c IS NULL THEN o.start__c ELSE o.effective_start__c END as date_start,
				SUM(o.grand_total__c) as money
			
			FROM
			
				salesforce.contract__c o
				
			WHERE
			
				o.service_type__c NOT LIKE 'maintenance cleaning'
				AND o.active__c IS TRUE
				AND o.test__c IS FALSE
				
			GROUP BY
			
				o.sfid,
				o.opportunity__c,
				o.name,
				o.service_type__c,
				o.effective_start__c,
				o.start__c,
				o.recurrency__c,
				o.note__c
				
			ORDER BY
			
				o.sfid,
				o.service_type__c,
				o.effective_start__c) as t1) as t2
				
	LEFT JOIN
	
		bi.calendar_matrix cal
		
	ON
	
		TO_CHAR(NOW(), 'YYYY-MM') = cal."year_month") as t3
		
LEFT JOIN

	salesforce.opportunity o
	
ON

	o.sfid = t3.opportunity__c
	
WHERE

	t3.invoiced_this_month > 0
	
GROUP BY

	t3.opportunity__c,
	o.name,
	o.delivery_area__c,
	t3.service_type__c,
	t3.money;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

INSERT INTO bi.forecast_revenue

SELECT

	'Extra Services Last Month' as kpi,
	inv.opportunity__c as sfid,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END as grand_total,
	SUM(inv.amount__c)/1.19 as revenue_last_month



FROM

	salesforce.invoice__c inv
	
LEFT JOIN

	salesforce.opportunity o
	
ON

	inv.opportunity__c = o.sfid
	
WHERE

	inv.test__c IS FALSE
	AND CASE WHEN RIGHT(current_date::text, 2)::integer < 8
	         THEN LEFT(inv.issued__c::text, 7) = LEFT((current_date - '2 MONTH'::INTERVAL)::text, 7) 
	         ELSE LEFT(inv.issued__c::text, 7) = LEFT((current_date - '1 MONTH'::INTERVAL)::text, 7) 
	         END 
	AND inv.parent_invoice__c IS NULL
	AND inv.service_type__c NOT LIKE 'maintenance cleaning'
	
GROUP BY

	inv.opportunity__c,
	o.name,
	o.delivery_area__c,
	inv.service_type__c,
	CASE WHEN o.grand_total__c IS NULL THEN o.plan_pph__c*inv.total_invoiced_hours_count__c ELSE o.grand_total__c END;



-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
 
-- TEST TEST TEST 


DELETE FROM bi.test_kadir WHERE today = current_date;


INSERT INTO bi.test_kadir

-- Example of the revenue loss for the current date

SELECT
    CURRENT_DATE AS today,
    TO_CHAR(CURRENT_DATE, 'YYYY-MM') AS year_month,
    COUNT(CASE WHEN opps.status__c = 'OFFBOARDING' THEN opps.sfid ELSE NULL END) AS no_opps_offboarding,
    COUNT(CASE WHEN opps.status__c = 'RETENTION' THEN opps.sfid ELSE NULL END) AS no_opps_retention,
    COUNT(CASE WHEN opps.status__c = 'RUNNING' THEN opps.sfid ELSE NULL END) AS no_opps_running,
    SUM(CASE WHEN opps.status__c = 'RETENTION' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * t1.invoiced_hours ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS retention_revenue,
    SUM(CASE WHEN opps.status__c = 'OFFBOARDING' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * t1.invoiced_hours ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS offboarding_revenue,
    SUM(CASE WHEN opps.status__c = 'RUNNING' THEN (CASE WHEN opps.grand_total__c IS NULL THEN opps.plan_pph__c * t1.invoiced_hours ELSE opps.grand_total__c END) ELSE NULL END)::DECIMAL AS running_revenue
FROM
    salesforce.opportunity opps
LEFT JOIN( -- Invoiced hours last month for each opportunities 
    SELECT
        TO_CHAR(effectivedate, 'YYYY-MM') AS year_month,
        SUM(o.Order_Duration__c)::DECIMAL AS invoiced_hours,
        o.opportunityid AS opportunities
           
    FROM
        salesforce.order o

    WHERE
        o.status IN ('INVOICED',
        'PENDING TO START',
        'CANCELLED CUSTOMER',
        'FULFILLED')
        AND o.type = 'cleaning-b2b'
        AND o.professional__c IS NOT NULL
        AND o.test__c IS FALSE
        AND LEFT(o.locale__C, 2) = 'de'
        AND o.effectivedate::TIMESTAMP >= DATE_TRUNC('month', CURRENT_DATE - INTERVAL '1 month')
        AND o.effectivedate::TIMESTAMP < DATE_TRUNC('month', CURRENT_DATE)

    GROUP BY
        year_month,
        o.opportunityid) AS t1 ON
    t1.opportunities = opps.sfid
    -- AND t1.year_month = TO_CHAR(CURRENT_DATE, 'YYYY-MM')
WHERE
    opps.test__c IS FALSE
    AND LEFT(opps.locale__c, 2) = 'de'

GROUP BY
    TO_CHAR(CURRENT_DATE, 'YYYY-MM');

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-- Performance of Sales Managers and Account Management in Sales

------------------------------------------------------------------------


DROP TABLE IF EXISTS bi.sales_performance_02;
CREATE TABLE bi.sales_performance_02 AS

-- Inbound Likelies
-- # of leads assigned(received) 
-- # of leads qualified
-- # of leads not reached
-- # of invalid leads
-- % of invalid leads: SUM(Invalid)/Leads Received
-- # of suitable leads(valid)
-- # of leads won(leads to opps)
-- % Lead-to-opp (CVR1): SUM(leads_converted_opps) / SUM(leads_suitable)
-- % Lead-to-customer(CVR2): SUM(leads_won) / SUM(leads_suitable)

WITH leads AS(

SELECT
    TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS leads_created_date,
    u.name AS owner_name,
    COUNT(li.sfid) AS leads_assigned,
    SUM(CASE WHEN li.lost_reason__c NOT LIKE 'invalid%' AND li.lost_reason__c NOT LIKE 'not suitable%' OR li.lost_reason__c IS NULL THEN 1 ELSE 0 END) AS leads_suitable,
    SUM(CASE WHEN li.lost_reason__c LIKE 'invalid%' OR li.lost_reason__c LIKE 'not suitable%' THEN 1 ELSE 0 END) AS leads_invalid,
    SUM(CASE WHEN li.stage__c = 'QUALIFIED' THEN 1 ELSE 0 END) AS leads_qualified,
--    SUM(CASE WHEN li.direct_relation__c IS FALSE THEN 1 ELSE NULL END) AS leads_qualified,
    SUM(CASE WHEN li.stage__c = 'NOT REACHED' THEN 1 ELSE 0 END) AS leads_not_reached,
    SUM(CASE WHEN li.opportunity__c IS NOT NULL THEN 1 ELSE 0 END) AS leads_converted_opps,
    COALESCE(SUM(t1.leads_won), 0) AS leads_won,
    COALESCE(SUM(t1.leads_lost), 0) AS leads_lost

    
FROM
    salesforce.likeli__c li
LEFT JOIN (
    SELECT
        opps.sfid,
        SUM(CASE WHEN opps.stagename IN ('WON', 'PENDING') THEN 1 ELSE 0 END) AS leads_won,
        SUM(CASE WHEN opps.stagename IN ('LOST', 'CONTRACT FAILED') THEN 1 ELSE 0 END) AS leads_lost
    FROM
        salesforce.opportunity opps
    WHERE
        opps.test__c IS FALSE
    GROUP BY
        opps.sfid) t1 ON
    li.opportunity__c = t1.sfid
    
LEFT JOIN salesforce.user u ON
    li.ownerid = u.sfid

WHERE
    li.type__c = 'B2B'
    AND li.test__c IS FALSE
    AND li.acquisition_channel__c IN ('inbound', 'web')
    AND (li.acquisition_tracking_id__c NOT LIKE '%raffle%' OR li.acquisition_tracking_id__c NOT LIKE '%news%' OR li.acquisition_tracking_id__c IS NULL)
-- AND ((o.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR o.lost_reason__c IS NULL)
    AND (li.company_name__c NOT LIKE '%test%' OR li.company_name__c IS NULL OR li.company_name__c NOT LIKE '%bookatiger%')
    AND (li.email__c NOT LIKE '%bookatiger%' OR li.email__c IS NULL)
    
GROUP BY
    TO_CHAR(li.createddate, 'YYYY-MM'),
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
    u.name
ORDER BY
    TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE DESC),

-- CALL STATS for outbound calls    
-- total # of calls
-- # of connected_calls
-- sum of call duration(min)
-- avg of call duration(min)
-- avg ring duration(min)
calls AS(

SELECT 
    TO_CHAR(t2.calldate::DATE, 'YYYY-MM') AS year_month,
    t2.calldate,
    t2.owner_name,
--    t2.calldirection,
    MAX(total_calls) AS total_calls,
    MAX(connected_calls) AS connected_calls,
    t2.sum_call_duration_sec,
    t2.sum_call_duration,
    ROUND(t2.avg_call_duration_sec::NUMERIC, 2) AS avg_call_duration_sec,
    t2.avg_call_duration,
    t2.avg_ring_duration,
    ROUND(t2.avg_ring_duration_sec::NUMERIC, 2) AS avg_ring_duration_sec


FROM salesforce.likeli__c li

LEFT JOIN (
SELECT
    TO_CHAR(t1.calldate::DATE, 'YYYY-MM') AS year_month,
    t1.calldate,
    t1.name AS owner_name,
 --   t1.calldirection__c AS calldirection,
    SUM(t1.total_calls) AS total_calls,
    SUM(t1.connected_calls) AS connected_calls,
    SUM(t1.lost_calls) AS lost_calls,
    t1.sum_call_duration_sec,
    TO_CHAR((t1.sum_call_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS sum_call_duration,
    t1.avg_call_duration_sec,
    TO_CHAR((t1.avg_call_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS avg_call_duration,
    TO_CHAR((t1.avg_ring_duration_sec || 'second')::INTERVAL, 'HH24:MI:SS') AS avg_ring_duration,
    t1.avg_ring_duration_sec
FROM
    (
    SELECT
        u.name,
        nc.call_start_date_time__c::DATE AS calldate,
    --    nc.calldirection__c,
        COUNT(*) AS total_calls,
        SUM(CASE WHEN nc.callconnected__c = 'Yes' THEN 1 ELSE 0 END) AS connected_calls,
        SUM(CASE WHEN nc.callconnected__c = 'No' AND nc.callconnectedcheckbox__c IS FALSE THEN 1 ELSE 0 END) AS lost_calls,
        SUM(nc.calltalkseconds__c) AS sum_call_duration_sec,
        AVG(nc.calltalkseconds__c) AS avg_call_duration_sec,
        AVG(nc.callringseconds__c) AS avg_ring_duration_sec
    FROM
        salesforce.natterbox_call_reporting_object__c nc
    JOIN salesforce.user u ON
        nc.ownerid = u.sfid
    WHERE
        nc.call_start_date_time__c::DATE >= '2018-01-01'
        AND nc.wrapup_string_1__c LIKE '%B2B%'
        AND nc.calldirection__c = 'Outbound'
    GROUP BY
        u.name,
        calldate
    --    nc.calldirection__c 
   ) AS t1
        
GROUP BY
    owner_name,
    year_month,
    calldate,
 --   t1.calldirection__c,
    sum_call_duration,
    avg_call_duration,
    avg_ring_duration,
    t1.sum_call_duration_sec,
    t1.avg_call_duration_sec,
    t1.avg_ring_duration_sec
    
ORDER BY
    calldate DESC ) t2 ON
    
TO_CHAR(li.createddate::DATE, 'YYYY-MM-DD') = TO_CHAR(t2.calldate::DATE, 'YYYY-MM-DD')

WHERE
    li.type__c = 'B2B'
    AND li.test__c IS FALSE
    AND li.acquisition_channel__c IN ('inbound', 'web')
    AND (li.acquisition_tracking_id__c NOT LIKE '%raffle%' OR li.acquisition_tracking_id__c NOT LIKE '%news%' OR li.acquisition_tracking_id__c IS NULL)
-- AND ((li.lost_reason__c NOT LIKE 'invalid - sem duplicate') OR li.lost_reason__c IS NULL)
    AND (li.company_name__c NOT LIKE '%test%' OR li.company_name__c IS NULL OR li.company_name__c NOT LIKE '%bookatiger%')
    AND (li.email__c NOT LIKE '%bookatiger%' OR li.email__c IS NULL)
    AND t2.calldate IS NOT NULL
    
GROUP BY
    year_month,
--    t2.calldirection,
    t2.owner_name,
    t2.calldate,
    t2.sum_call_duration,
    t2.sum_call_duration_sec,
    t2.avg_call_duration,
    t2.avg_call_duration_sec,
    t2.avg_ring_duration,
    t2.avg_ring_duration_sec
ORDER BY
    t2.calldate::DATE DESC
),


-- total # of deals (contracts ACCEPTED or SIGNED)
-- net revenue of all deals
-- # of recurring deals (6m, 12m, 24m, 36m, 48m and unlimited contracts)
-- # of one-off deals (fixed-term contracts, no duration)
-- total revenue of recurring deals and one-off deals

deals AS (

SELECT 
    t5.year_month,
    t5.opps_signed_date::DATE,
    t5.owner_name,
--    t5.service_type__c AS service_type,
    SUM(t5.grand_total__c)::DECIMAL AS total_net_revenue_deals,
    COUNT(t5.recurring_deals) + COUNT(t5.one_off_deals) AS total_deals,
    COUNT(t5.recurring_deals) AS total_recurring_deals,
    SUM(CASE WHEN t5.recurring_deals IS NOT NULL THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS revenue_recurring_deals,
    COUNT(t5.one_off_deals) AS total_one_off_deals,
    SUM(CASE WHEN one_off_deals IS NOT NULL THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS revenue_one_off_deals,
    SUM(CASE WHEN t5.recurring_deals = '6-month-contract' THEN 1 ELSE 0 END) AS "6-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '6-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-6-month-contract",
    SUM(CASE WHEN t5.recurring_deals= '12-month-contract' THEN 1 ELSE 0 END) AS "12-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '12-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-12-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '24-month-contract' THEN 1 ELSE 0 END) AS "24-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '24-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-24-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '36-month-contract' THEN 1 ELSE 0 END) AS "36-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '36-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-36-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '48-month-contract' THEN 1 ELSE 0 END) AS "48-month-contract",
    SUM(CASE WHEN t5.recurring_deals = '48-month-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-48-month-contract",
    SUM(CASE WHEN t5.recurring_deals = 'unlimited-contract' THEN 1 ELSE 0 END) AS "unlimited-contract",
    SUM(CASE WHEN t5.recurring_deals = 'unlimited-contract' THEN t5.grand_total__c::DECIMAL ELSE 0 END) AS "revenue-unlimited-contract"
FROM 
(
SELECT
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM') AS year_month,
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD') AS opps_signed_date,
    u.name AS owner_name,
    c.opportunity__c,
    c.name AS contract_name,
    opps.name AS opp_name,
--    c.service_type__c,
--    c.when_accepted__c,
    c.grand_total__c,
    c.start__c,
    c.end__c,
    c.duration__c::NUMERIC,
    CASE WHEN c.duration__c::NUMERIC = 6 THEN '6-month-contract'
        WHEN c.duration__c::NUMERIC = 12 THEN '12-month-contract'
        WHEN c.duration__c::NUMERIC = 24 THEN '24-month-contract'
        WHEN c.duration__c::NUMERIC = 36 THEN '36-month-contract'
        WHEN c.duration__c::NUMERIC = 48 THEN '48-month-contract'
        WHEN c.start__c IS NOT NULL AND c.end__c IS NULL AND c.duration__c IS NULL THEN 'unlimited-contract'
        END AS recurring_deals,
    CASE WHEN c.start__c IS NOT NULL AND c.end__c IS NOT NULL AND c.duration__c IS NULL THEN 'fixed-term-contract' END AS one_off_deals

FROM
    salesforce.contract__c c
LEFT JOIN salesforce.opportunity opps ON
    c.opportunity__c = opps.sfid
LEFT JOIN salesforce.user u ON
    c.ownerid = u.sfid
WHERE
    c.test__c IS FALSE
    AND c.status__c IN ('ACCEPTED', 'SIGNED')
    AND opps.closedate <= CURRENT_DATE
GROUP BY
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD'),
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM'),
    c.opportunity__c,
    u.name,
--    c.service_type__c,
    c.start__c,
    c.end__c,
    c.duration__c,
    c.name,
    c.grand_total__c,
    opps.name
    
ORDER BY 
    TO_CHAR(opps.closedate::DATE, 'YYYY-MM-DD') DESC ) AS t5
    
GROUP BY 
    t5.year_month,
    t5.opps_signed_date,
    t5.owner_name
--    t5.service_type__c
ORDER BY
     t5.opps_signed_date DESC),
     

-- Inquiries(cases) for Account Management
-- New cases, cases in progress, closed cases, reopened cases
-- Total number of cases    
cases AS (

SELECT 
    t6.year_month,
    t6.casedate,
    t6.owner_name,
    SUM(CASE WHEN t6.status = 'New' THEN 1 ELSE 0 END) AS new_cases,
    SUM(CASE WHEN t6.status = 'In Progress' THEN 1 ELSE 0 END) AS cases_in_progress,
    SUM(CASE WHEN t6.status = 'Closed' THEN 1 ELSE 0 END) AS closed_cases,
    SUM(CASE WHEN t6.status = 'Reopened' THEN 1 ELSE 0 END) AS reopened_cases

FROM
(SELECT 
    ca.createddate::DATE AS casedate,
    TO_CHAR(ca.createddate::DATE, 'YYYY-MM') AS year_month,
    ca.sfid,
    ca.casenumber,
    ca.status,
    u.name AS owner_name
FROM salesforce.CASE ca     
LEFT JOIN salesforce.user u ON ca.ownerid = u.sfid

WHERE
    ca.test__c IS FALSE
    AND ca.createddate::date >= '2018-01-01'
    
GROUP BY 
    ca.createddate::DATE,
    TO_CHAR(ca.createddate::DATE, 'YYYY-MM'),
    ca.sfid,
    ca.casenumber,
    ca.status,
    u.name
ORDER BY 
    TO_CHAR(ca.createddate::DATE, 'YYYY-MM') ) AS t6
    
GROUP BY 
    t6.casedate,
    t6.year_month,
    t6.owner_name
ORDER BY 
    t6.casedate DESC),


-- productive days (number of days worked)
-- % productivity (productive days / working days in the month): WILL BE ADDED in Tableau via two date filter (working days and non-working days)

working_days AS (

SELECT
    t3.year_month,
    t3.leads_created_date,
    t3.owner_name,
    t3.working_days_in_month,
    t3.day_type,
    t3.non_working_day_number,
--    CASE WHEN t3.leads_created_date >= DATE_TRUNC('month', NOW()) THEN MAX(t3.working_day_number) ELSE 0 END AS working_days_till_today,
    t3.working_day
FROM
    (
    SELECT
        u.name AS owner_name,
        TO_CHAR(li.createddate, 'YYYY-MM') AS year_month,
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE AS leads_created_date,
        wd.working_day_number AS working_day,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month AS working_days_in_month
    FROM
        salesforce.likeli__c li
    LEFT JOIN bi.working_days_monthly wd ON
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE = wd.date
    LEFT JOIN salesforce.user u ON
        li.ownerid = u.sfid
    WHERE
        li.type__c = 'B2B'
        AND li.test__c IS FALSE
        AND li.company_name__c NOT LIKE '%test%'
        AND li.name NOT LIKE '%test%'

        GROUP BY u.name,
        TO_CHAR(li.createddate, 'YYYY-MM'),
        TO_CHAR(li.createddate, 'YYYY-MM-DD')::DATE,
        wd.working_day_number,
        wd.non_working_day_number,
        wd.day_type,
        wd.day_type_number,
        wd.number_working_days_in_month
    ORDER BY
        TO_CHAR(li.createddate, 'YYYY-MM') DESC) t3

GROUP BY
    t3.year_month,
    t3.leads_created_date,
    t3.owner_name,
    t3.day_type,
    t3.non_working_day_number,
    t3.working_day,
    t3.working_days_in_month
ORDER BY
    t3.leads_created_date DESC ),
    
    
dates AS(
SELECT
    d::DATE AS DATE,
    TO_CHAR(d, 'YYYY-MM') AS year_month
FROM
    GENERATE_SERIES('2018-01-01', CURRENT_DATE, '1 day'::INTERVAL) d ),

users AS(
        SELECT 
            u.name AS owner_name
        FROM salesforce.USER u
        WHERE u.name IN ('Malte Jetter', 'Marc Heumer', 'Steven Ellison', 'Tommy Haferkorn', 'André Wagner', 'Anshuman Kharoo')
        )


SELECT
    dates.date,
    dates.year_month,
    users.owner_name,
    working_days.working_days_in_month,
    working_days.working_day,
    CASE WHEN calls.total_calls IS NOT NULL THEN COUNT(DISTINCT(dates.date)) ELSE 0 END AS productive_days,
--    ROUND(MAX(CASE WHEN calls.total_calls IS NOT NULL THEN working_days.working_days_till_today ELSE 0 END) / working_days.number_working_days_in_month::NUMERIC, 2) AS productivity,
    leads.leads_assigned,
    leads.leads_suitable,
    leads.leads_invalid,
    leads.leads_qualified,
    leads.leads_not_reached,
    leads.leads_converted_opps,
    ROUND(SUM(leads.leads_converted_opps) / NULLIF(SUM(leads.leads_suitable), 0), 2) AS cvr1,
    ROUND(SUM(leads.leads_won) / NULLIF(SUM(leads.leads_suitable), 0), 2) AS cvr2,
    leads.leads_won,
    leads.leads_lost,
  --  calls.calldirection,
    calls.total_calls,
    calls.connected_calls,
    calls.sum_call_duration,
    calls.sum_call_duration_sec,
    calls.avg_call_duration,
    calls.avg_call_duration_sec,
    calls.avg_ring_duration,
    calls.avg_ring_duration_sec,
    calls.total_calls / NULLIF((deals.total_deals), 0) AS calls_to_deals,
--    deals.service_type,
    COALESCE(deals.total_deals, 0) AS total_deals,
    COALESCE(deals.total_recurring_deals, 0) AS total_recurring_deals,
    COALESCE(deals.total_one_off_deals, 0) AS total_one_off_deals,
    COALESCE(deals.total_net_revenue_deals, 0) AS total_net_revenue_deals,
    COALESCE(deals.revenue_recurring_deals, 0) AS revenue_recurring_deals,
    COALESCE(deals.revenue_one_off_deals, 0) AS revenue_one_off_deals,
    COALESCE(deals."6-month-contract", 0) AS "6-month-contract",
    COALESCE(deals."revenue-6-month-contract", 0) AS "revenue-6-month-contract",
    COALESCE(deals."12-month-contract", 0) AS "12-month-contract",
    COALESCE(deals."revenue-12-month-contract", 0) AS "revenue-12-month-contract",
    COALESCE(deals."24-month-contract", 0) AS "24-month-contract",
    COALESCE(deals."revenue-24-month-contract", 0) AS "revenue-24-month-contract",
    COALESCE(deals."36-month-contract", 0) AS "36-month-contract",
    COALESCE(deals."revenue-36-month-contract", 0) AS "revenue-36-month-contract",
    COALESCE(deals."48-month-contract", 0) AS "48-month-contract",
    COALESCE(deals."revenue-48-month-contract", 0) AS "revenue-48-month-contract",
    COALESCE(deals."unlimited-contract", 0) AS "unlimited-contract",
    COALESCE(deals."revenue-unlimited-contract", 0) AS "revenue-unlimited-contract",
    COALESCE(cases.new_cases, 0) AS new_cases,
    COALESCE(cases.cases_in_progress, 0) AS cases_in_progress,
    COALESCE(cases.closed_cases, 0) AS closed_cases,
    COALESCE(cases.reopened_cases, 0) AS reopened_cases,
    COALESCE((cases.new_cases + cases.cases_in_progress + cases.closed_cases + cases.reopened_cases), 0) AS total_cases
    

FROM dates
CROSS JOIN users

LEFT JOIN calls ON 
calls.calldate = dates.date
AND calls.owner_name = users.owner_name

LEFT JOIN leads ON
leads.leads_created_date = dates.date
AND leads.owner_name = users.owner_name

LEFT JOIN cases ON 
calls.calldate = cases.casedate
AND cases.owner_name = users.owner_name

LEFT JOIN deals ON 
deals.opps_signed_date = dates.date
AND deals.owner_name = users.owner_name

LEFT JOIN working_days ON 
dates.date = working_days.leads_created_date


GROUP BY
    leads.leads_assigned,
    leads.leads_created_date,
    leads.leads_suitable,
    leads.leads_invalid,
    leads.leads_qualified,
    leads.leads_not_reached,
    leads.leads_converted_opps,
    leads.leads_won,
    leads.leads_lost,
    dates.year_month,
    dates.date,
    leads.leads_created_date,
    users.owner_name,
  --  calls.calldirection,
    calls.total_calls,
    calls.connected_calls,
    calls.sum_call_duration,
    calls.sum_call_duration_sec,
    calls.avg_call_duration,
    calls.avg_call_duration_sec,
    calls.avg_ring_duration,
    calls.avg_ring_duration_sec,
    working_days.working_days_in_month,
    working_days.working_day,
--    deals.service_type,
    deals.total_deals,
    deals.total_recurring_deals,
    deals.total_one_off_deals,
    deals.total_net_revenue_deals,
    deals.revenue_recurring_deals,
    deals.revenue_one_off_deals,
    deals."6-month-contract",
    deals."revenue-6-month-contract",
    deals."12-month-contract",
    deals."revenue-12-month-contract",
    deals."24-month-contract",
    deals."revenue-24-month-contract",
    deals."36-month-contract",
    deals."revenue-36-month-contract",
    deals."48-month-contract",
    deals."revenue-48-month-contract",
    deals."unlimited-contract",
    deals."revenue-unlimited-contract",
    cases.new_cases,
    cases.cases_in_progress,
    cases.closed_cases,
    cases.reopened_cases


ORDER BY
    dates.date DESC;
   

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;


$BODY$ LANGUAGE 'plpgsql'
