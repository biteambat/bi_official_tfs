SELECt
	Cohort_month,
	(SUM(total_monthly_amount)-sum(sub_costs))/SUM(total_monthly_amount) as GPM,
	SUM(total_monthly_amount) as sum_monthly_amount
FROM(
SELECT
	name,
	TO_CHAR(first_order_date,'YYYY-MM') as Cohort_Month,
	CASE WHEN grand_total_calc is null THEN total_amount else grand_total_calc END as total_monthly_amount,
	sub_pph*total_invoiced_hours as sub_costs,
	CASE
	WHEN CASE WHEN grand_total_calc is null THEN total_amount else grand_total_calc END > 0 THEN ((CASE WHEN grand_total_calc is null THEN total_amount else grand_total_calc END)-(sub_pph*total_invoiced_hours)) END AS abs_gp
FROM
	bi.b2borders
WHERE
	year_month = '2017-06'
	and bat_hour_share = 0
	and total_hours > 0) as a
GROUP BY
	Cohort_Month