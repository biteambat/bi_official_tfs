CREATE OR REPLACE FUNCTION bi.trig_attrib_modelling() RETURNS SETOF bi.trig_channels_attribution AS
$BODY$

DECLARE
	rows bi.trig_channels_attribution%rowtype;
BEGIN

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

FOR rows in
	(SELECT * FROM bi.trig_channels_attribution t1)
LOOP

	IF rows.n_visits = 1 THEN
		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 1 ELSE 0 END);
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 1 ELSE 0 END);
		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 1 ELSE 0 END);
		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 1 ELSE 0 END);
		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 1 ELSE 0 END);
		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 1 ELSE 0 END);
		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 1 ELSE 0 END);
		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 1 ELSE 0 END);
		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 1 ELSE 0 END);
		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 1 ELSE 0 END);
		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 1 ELSE 0 END);
		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 1 ELSE 0 END);
		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 1 ELSE 0 END);


	ELSEIF rows.n_visits = 2 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.5 ELSE 0 END);
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.5 ELSE 0 END);
		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.5 ELSE 0 END);
		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.5 ELSE 0 END);
		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.5 ELSE 0 END);
		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.5 ELSE 0 END);
		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.5 ELSE 0 END);
		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.5 ELSE 0 END);
		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.5 ELSE 0 END);
		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.5 ELSE 0 END);
		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.5 ELSE 0 END);
		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.5 ELSE 0 END);
		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.5 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.5 ELSE 0 END);

	ELSEIF rows.n_visits = 3 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.4 ELSE 0 END);
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.4 ELSE 0 END);
		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.4 ELSE 0 END);
		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.4 ELSE 0 END);
		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);
		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.4 ELSE 0 END);
		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.4 ELSE 0 END);
		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.4 ELSE 0 END);
		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.4 ELSE 0 END);
		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.4 ELSE 0 END);
		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.4 ELSE 0 END);
		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);
		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) + SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) + SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 4 THEN

			rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.4 ELSE 0 END);
			
			rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.4 ELSE 0 END);

			rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.4 ELSE 0 END);

			rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.4 ELSE 0 END);

			rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

			rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.4 ELSE 0 END);

			rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.4 ELSE 0 END);

			rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.4 ELSE 0 END);

			rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

			rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.4 ELSE 0 END);

			rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.4 ELSE 0 END);

			rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

			rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
			+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 5 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM' THEN 0.4 ELSE 0 END);
		
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM Brand' THEN 0.4 ELSE 0 END);

		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEO' THEN 0.4 ELSE 0 END);

		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'DTI' THEN 0.4 ELSE 0 END);

		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Display' THEN 0.4 ELSE 0 END);

		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook' THEN 0.4 ELSE 0 END);

		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Unattributed' THEN 0.4 ELSE 0 END);

		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Newsletter' THEN 0.4 ELSE 0 END);

		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Referral' THEN 0.4 ELSE 0 END);

		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 6 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM' THEN 0.4 ELSE 0 END);
		
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM Brand' THEN 0.4 ELSE 0 END);

		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEO' THEN 0.4 ELSE 0 END);

		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s6 = 'DTI' THEN 0.4 ELSE 0 END);

		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Display' THEN 0.4 ELSE 0 END);

		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook' THEN 0.4 ELSE 0 END);

		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Unattributed' THEN 0.4 ELSE 0 END);

		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Newsletter' THEN 0.4 ELSE 0 END);

		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Referral' THEN 0.4 ELSE 0 END);

		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 7 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM' THEN 0.4 ELSE 0 END);
		
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM Brand' THEN 0.4 ELSE 0 END);

		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEO' THEN 0.4 ELSE 0 END);

		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'DTI' THEN 0.4 ELSE 0 END);

		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Display' THEN 0.4 ELSE 0 END);

		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook' THEN 0.4 ELSE 0 END);

		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Unattributed' THEN 0.4 ELSE 0 END);

		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Newsletter' THEN 0.4 ELSE 0 END);

		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Referral' THEN 0.4 ELSE 0 END);

		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 8 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEM' THEN 0.4 ELSE 0 END);
		
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEM Brand' THEN 0.4 ELSE 0 END);

		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEO' THEN 0.4 ELSE 0 END);

		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'DTI' THEN 0.4 ELSE 0 END);

		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Display' THEN 0.4 ELSE 0 END);

		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Facebook' THEN 0.4 ELSE 0 END);

		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Unattributed' THEN 0.4 ELSE 0 END);

		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Newsletter' THEN 0.4 ELSE 0 END);

		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Referral' THEN 0.4 ELSE 0 END);

		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 9 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'SEM' THEN 0.4 ELSE 0 END);
		
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'SEM Brand' THEN 0.4 ELSE 0 END);

		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'SEO' THEN 0.4 ELSE 0 END);

		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'DTI' THEN 0.4 ELSE 0 END);

		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Display' THEN 0.4 ELSE 0 END);

		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Facebook' THEN 0.4 ELSE 0 END);

		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Unattributed' THEN 0.4 ELSE 0 END);

		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Newsletter' THEN 0.4 ELSE 0 END);

		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Referral' THEN 0.4 ELSE 0 END);

		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	ELSEIF rows.n_visits = 10 THEN

		rows.w_sem := SUM(CASE WHEN rows.chan_s1 = 'SEM' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'SEM' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'SEM' THEN 0.4 ELSE 0 END);
		
		rows.w_semb := SUM(CASE WHEN rows.chan_s1 = 'SEM Brand' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'SEM Brand' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'SEM Brand' THEN 0.4 ELSE 0 END);

		rows.w_seo := SUM(CASE WHEN rows.chan_s1 = 'SEO' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'SEO' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'SEO' THEN 0.4 ELSE 0 END);

		rows.w_dti := SUM(CASE WHEN rows.chan_s1 = 'DTI' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s4 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s5 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'DTI' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'DTI' THEN 0.4 ELSE 0 END);

		rows.w_aff := SUM(CASE WHEN rows.chan_s1 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Affiliates/Coops' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Affiliates/Coops' THEN 0.4 ELSE 0 END);

		rows.w_dis := SUM(CASE WHEN rows.chan_s1 = 'Display' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Display' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Display' THEN 0.4 ELSE 0 END);

		rows.w_fb := SUM(CASE WHEN rows.chan_s1 = 'Facebook' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Facebook' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Facebook' THEN 0.4 ELSE 0 END);

		rows.w_unatt := SUM(CASE WHEN rows.chan_s1 = 'Unattributed' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Unattributed' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Unattributed' THEN 0.4 ELSE 0 END);

		rows.w_fbo := SUM(CASE WHEN rows.chan_s1 = 'Facebook Organic' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Facebook Organic' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Facebook Organic' THEN 0.4 ELSE 0 END);

		rows.w_newsletter := SUM(CASE WHEN rows.chan_s1 = 'Newsletter' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Newsletter' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Newsletter' THEN 0.4 ELSE 0 END);

		rows.w_ref := SUM(CASE WHEN rows.chan_s1 = 'Referral' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Referral' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Referral' THEN 0.4 ELSE 0 END);

		rows.w_voucher := SUM(CASE WHEN rows.chan_s1 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Voucher Campaigns' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Voucher Campaigns' THEN 0.4 ELSE 0 END);

		rows.w_brandoff := SUM(CASE WHEN rows.chan_s1 = 'Brand marketing offline' THEN 0.4 ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s2 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s3 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END) 
		+ SUM(CASE WHEN rows.chan_s4 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s5 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s6 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s7 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s8 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s9 = 'Brand marketing offline' THEN 0.2/(rows.n_visits - 2)::numeric ELSE 0 END)
		+ SUM(CASE WHEN rows.chan_s10 = 'Brand marketing offline' THEN 0.4 ELSE 0 END);

	END IF;

RETURN NEXT rows;
END LOOP;

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

END;
$BODY$ 
LANGUAGE 'plpgsql'