CREATE OR REPLACE FUNCTION bi.trig_visits_data_cleanup() RETURNS SETOF bi.trig_piwik_datacleanup AS

$BODY$
DECLARE

rows bi.trig_piwik_datacleanup%rowtype;
function_name varchar := ' bi.daily$attribution_modelling';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
last_visitor_id text;
visitnum int;
rownum int;
last_transaction_id text;

BEGIN

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

rownum:= 0;
visitnum:= 1;
last_visitor_id := NULL;
last_transaction_id := NULL;

FOR rows IN
	(SELECT * FROM bi.trig_piwik_datacleanup t1 ORDER BY t1.visitor_id asc, t1.firstaction_unixtime asc)
LOOP

	rownum:= rownum + 1;

	IF rownum = 1 THEN /*If we are at the first row of the table THEN*/

		IF rows.transaction_id IS NOT NULL AND rows.transaction_id != '' THEN /*If the first row of the table is a transaction*/

			last_transaction_id := rows.transaction_id;
			rows.visit_num := visitnum;
			visitnum := 0;

		ELSE /*If the first row of the table is NOT a transaction*/

			rows.visit_num := visitnum;

		END IF;

	ELSEIF rownum > 1 AND rows.visitor_id <> last_visitor_id THEN /*If we already started looping and switch to a new visitor*/

		visitnum := 1;

		IF rows.transaction_id IS NOT NULL AND rows.transaction_id != '' THEN

			last_transaction_id := rows.transaction_id;
			rows.visit_num := visitnum;
			visitnum := 0;

		ELSE 

			rows.visit_num := visitnum;

		END IF;

	ELSEIF rownum > 1 AND rows.visitor_id = last_visitor_id THEN /*If we already started looping and stay with the same visitor*/

		IF rows.transaction_id IS NOT NULL AND rows.transaction_id != '' AND rows.transaction_id <> last_transaction_id THEN
			
			last_transaction_id := rows.transaction_id;
			rows.visit_num := visitnum;
			visitnum := 0;

		ELSE
			rows.transaction_id := NULL;
			rows.visit_num := visitnum;

		END IF;

	END IF;

visitnum:= visitnum + 1;
last_visitor_id := rows.visitor_id;

RETURN NEXT rows;
END LOOP;

---------------------------------------------------------------------------------------------------------


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

END;

$BODY$ LANGUAGE 'plpgsql'