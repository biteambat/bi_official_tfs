CREATE OR REPLACE FUNCTION bi.sfunc_forecasts_clv(crunchdate date) RETURNS void AS

$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_forecasts_clv';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN
--------------------------------------------------------------
