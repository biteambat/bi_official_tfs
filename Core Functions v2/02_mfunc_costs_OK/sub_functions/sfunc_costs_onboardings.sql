CREATE OR REPLACE FUNCTION bi.sfunc_costs_onboardings(crunchdate date)
RETURNS void AS
$BODY$
DECLARE 
function_name varchar := 'bi.sfunc_costs_onboardings';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;
BEGIN


DROP TABLE IF EXISTS bi.temp_dateseries;
CREATE TABLE bi.temp_dateseries AS 

	(SELECT i::date as date FROM generate_series('2016-05-25', 
	  current_date - interval '1 day', '1 day'::interval) i)

;


DROP TABLE IF EXISTS bi.daily_classifiedscosts_polygon;
CREATE TABLE bi.daily_classifiedscosts_polygon AS

	 SELECT
	  t1.date as date,
	  LEFT(t2.polygon,2) as locale,
	  t2.polygon as polygon,
	  sum(CASE WHEN t2.start_date <= t1.date AND t2.end_date >= t1.date THEN daily_costs_eur else 0 end) as daily_costs
	 FROM bi.temp_dateseries t1,
	 external_data.zapier_onboardingcosts t2
	 GROUP BY
	  t1.date,
	  LEFT(t2.polygon,2),
	  t2.polygon
	 ORDER BY 
		t1.date desc,
		t2.polygon asc

;

DROP TABLE IF EXISTS bi.daily_signup_sem_polygon;
CREATE TABLE bi.daily_signup_sem_polygon AS

	SELECT 
		* 
	FROM
		(
		SELECT 
			date,
			locale,
			CASE WHEN polygon = 'nl-other' THEN 'nl-amsterdam' ELSE polygon END as polygon,
			SUM(adwords_cost_signup + display_cost_signup) as cost
		FROM bi.costsgrouping_adwords
		GROUP BY date, locale, CASE WHEN polygon = 'nl-other' THEN 'nl-amsterdam' ELSE polygon END
		ORDER BY date desc, polygon asc, locale asc
		) as t1

	WHERE t1.cost > 0

;

DROP TABLE IF EXISTS bi.temp_dateseries;


DROP TABLE IF EXISTS bi.cleaners_costsperonboarding_polygon;
CREATE TABLE bi.cleaners_costsperonboarding_polygon AS

	SELECT

		t1.date::date,
		t1.locale::text,
		t1.polygon::text,
		t1.daily_costs::numeric as classifieds_costs,
		(CASE WHEN t2.cost IS NULL THEN 0 ELSE t2.cost END)::numeric as sem_costs,
		(CASE WHEN t3.cost IS NULL THEN 0 ELSE t3.cost END)::numeric as fb_costs,
		COUNT(DISTINCT CASE WHEN (((l.acquisition_channel_params__c NOT LIKE ('%{"clid"%')) or l.acquisition_channel_params__c is null) AND ((l.leadsourcecomment__c IS NULL) AND (l.acquisition_channel_params__c != '{}')) OR ((l.leadsourcecomment__c NOT LIKE ('%SEM%')) AND (l.leadsourcecomment__c NOT IN ('facebook')))) then l.sfid else NULL end)::int as classifieds_leads,
		COUNT(DISTINCT CASE WHEN ((l.leadsourcecomment__c LIKE ('%SEM%')) OR (l.acquisition_channel_params__c LIKE ('%{"clid"%'))) then l.sfid else NULL end)::int as google_leads,
		COUNT(DISTINCT CASE WHEN (l.leadsourcecomment__c IN ('facebook'))  then l.sfid else NULL end)::int as facebook_leads,
		COUNT(distinct(l.sfid))::int as leads,
		COUNT(distinct (a.sfid))::int as onboardings
		
	FROM bi.daily_classifiedscosts_polygon t1

		LEFT JOIN bi.daily_signup_sem_polygon t2 ON 
			t1.date = t2.date
			AND t1.locale = t2.locale
			AND t1.polygon = t2.polygon

		LEFT JOIN bi.daily_signup_fb_polygon t3 ON 
			t1.date = t3.date
			AND t1.locale = t3.locale
			AND t1.polygon = t3.polygon

		LEFT JOIN salesforce.account a ON 
			t1.date::date = (a.hr_contract_start__c::date)-- - interval '3 weeks%') -- Check if you shouldnt do minus instead of plus 
			AND lower(left(a.locale__c,2)) = t1.locale
			AND a.delivery_areas__c = t1.polygon

		LEFT JOIN salesforce.lead l ON 
			t1.date::date = l.createddate::date
			AND lower(left(l.locale__c,2)) = t1.locale
			AND l.delivery_area__c = t1.polygon


	GROUP BY 
		t1.date,
		t1.locale,
		t1.polygon,
		t1.daily_costs,
		t2.cost,
		t3.cost

	ORDER BY
		t1.date desc,
		t1.locale asc,
		t1.polygon asc

;


end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

	INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;
$BODY$
LANGUAGE plpgsql VOLATILE