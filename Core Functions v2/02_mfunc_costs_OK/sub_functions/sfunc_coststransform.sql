CREATE OR REPLACE FUNCTION bi.sfunc_costs_transform(crunchdate date) RETURNS void AS 

$BODY$

DECLARE 
function_name varchar := 'bi.sfunc_costs_transform';
start_time timestamp := clock_timestamp() + interval '2 hours';
end_time timestamp;
duration interval;


BEGIN

  DROP TABLE IF EXISTS bi.coststransform;

  CREATE TABLE bi.coststransform AS -- SEM IS OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'SEM'::text as channel,
      SUM(cpa.sem_cost + cpa.sem_discount) as cost,
      SUM(cpa.sem_acq) as acquisitions,
      '0'::numeric as cpa_target_global, --ok
      '21000'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '150'::numeric as cpa_target_de, --ok
      '12000'::numeric as budget_target_de, --ok
      '2200'::numeric as cpa_target_ch, --ok
      '9000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;

  INSERT INTO bi.coststransform -- SEO is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'SEO'::text as channel,
      SUM(cpa.seo_discount + cpa.seo_cost) as cost,
      SUM(cpa.seo_acq) as acquisitions,
      '9'::numeric as cpa_target_global, --ok
      '0'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '7'::numeric as cpa_target_de, --ok
      '0'::numeric as budget_target_de, --ok
      '26'::numeric as cpa_target_ch, --ok
      '2000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;

  INSERT INTO bi.coststransform -- Brand Marketing is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Brand Marketing'::text as channel,
      SUM( cpa.sem_brand_discount + cpa.sem_brand_cost  /* + (cpa.tvcampaign) */ + cpa.offline_cost + cpa.offline_discount + cpa.seo_brand_discount ) as cost,
      SUM( cpa.dti_acq + cpa.sem_brand_acq + cpa.seo_brand_acq + cpa.offline_acq ) as acquisitions,
      '16'::numeric as cpa_target_global, --ok
      '14500'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '16'::numeric as cpa_target_de, --ok
      '5000'::numeric as budget_target_de, --ok
      '96'::numeric as cpa_target_ch, --ok
      '9500'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


  INSERT INTO bi.coststransform -- Facebook is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Facebook'::text as channel,
      SUM(cpa.facebook_cost + cpa.facebook_discount + cpa.facebook_organic_discount) as cost,
      SUM(cpa.facebook_acq + cpa.facebook_organic_acq) as acquisitions,
      '175'::numeric as cpa_target_global, --ok
      '4000'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '110'::numeric as cpa_target_de, --ok
      '2000'::numeric as budget_target_de, --ok 
      '200'::numeric as cpa_target_ch, --ok
      '2000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok


    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


  INSERT INTO bi.coststransform -- Display is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Display'::text as channel,
      SUM(cpa.display_cost + cpa.display_discount) as cost,
      SUM(cpa.display_acq) as acquisitions,
      '156'::numeric as cpa_target_global, --ok
      '1070'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '130'::numeric as cpa_target_de, --ok
      '1500'::numeric as budget_target_de, --ok
      '120'::numeric as cpa_target_ch, --ok
      '1000'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok


    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


  INSERT INTO bi.coststransform -- Vouchers is OK

    SELECT

      cpa.date as dates,
      cpa.locale as locale,
      'Voucher Campaigns'::text as channel,
      SUM(cpa.vouchers_cost) as cost,
      SUM(cpa.vouchers_acq) as acquisitions,
      '28'::numeric as cpa_target_global, --ok
      '1070'::numeric as budget_target_global, --ok
      '0'::numeric as cpa_target_at, --ok
      '0'::numeric as budget_target_at, --ok
      '22'::numeric as cpa_target_de, --ok
      '570'::numeric as budget_target_de, --ok
      '43'::numeric as cpa_target_ch, --ok
      '500'::numeric as budget_target_ch, --ok
      '0'::numeric as cpa_target_nl, --ok
      '0'::numeric as budget_target_nl --ok

    FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;


    INSERT INTO bi.coststransform -- Overall is in progress
      SELECT

        cpa.date as dates,
        cpa.locale as locale,
        'Overall'::text as channel,

        SUM(
        cpa.sem_cost
        +cpa.sem_discount

        +cpa.sem_brand_cost
        +cpa.sem_brand_discount

        +cpa.display_cost
        +cpa.display_discount

        +cpa.facebook_cost
        +cpa.facebook_discount

        +cpa.offline_cost
        +cpa.offline_discount

        +cpa.vouchers_cost

        +cpa.youtube_cost
        +cpa.youtube_discount

        ) as cost,

        SUM(cpa.all_acq) as acquisitions,
        
        '148'::numeric as cpa_target_global, --ok
        '193970'::numeric as budget_target_global, --ok
        '0'::numeric as cpa_target_at, --ok
        '0'::numeric as budget_target_at, --ok
        '62'::numeric as cpa_target_de, --ok
        '143970'::numeric as budget_target_de, --ok
        '126'::numeric as cpa_target_ch, --ok
        '50000'::numeric as budget_target_ch, --ok
        '0'::numeric as cpa_target_nl, --ok
        '0'::numeric as budget_target_nl --ok

      FROM bi.cpacalcpolygon cpa

    GROUP BY dates, locale, channel

    ORDER BY dates desc, locale asc
  ;



end_time := clock_timestamp() + interval '2 hours';
duration := EXTRACT(EPOCH FROM (end_time - start_time));
INSERT INTO main.function_logging values(DEFAULT, function_name, start_time, end_time, duration);

EXCEPTION WHEN others THEN 

  INSERT INTO main.error_logging VALUES (NOW()::timestamp, function_name::text, SQLERRM::text, SQLSTATE::text);
    RAISE NOTICE 'Error detected: transaction was rolled back.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;

END;

$BODY$ LANGUAGE 'plpgsql'