SELECT

	orders.year_month,
	orders.orders_b2b as orders_b2b,
	orders.orders_b2c as orders_b2c,
	cm_b2b_cases.cm_b2b_cases as cm_b2b_cases,		
	cm_b2b_cases_partner.cm_b2b_cases as cm_b2b_cases_partner,		
	cm_b2b_cases_professional_bat.cm_b2b_cases as cm_b2b_cases_professional_bat,
	cm_b2b_cases_professional_partner.cm_b2b_cases as cm_b2b_cases_professional_partner,
	cm_b2b_cases_unknown.cm_b2b_cases as cm_b2b_cases_unknown,
	cm_b2c_cases.cm_b2c_cases as cm_b2c_cases,
	cm_b2c_cases_partner.cm_b2c_cases as cm_b2c_cases_partner,
	cm_b2c_cases_professional_bat.cm_b2c_cases as cm_b2c_cases_professional_bat,
	cm_b2c_cases_professional_partner.cm_b2c_cases as cm_b2c_cases_professional_partner,
	cm_b2c_cases_unknown.cm_b2c_cases as cm_b2c_cases_unknown,
	inbound_b2b_calls_customers_cm.number_calls as inbound_b2b_calls_customers_cm,
	inbound_b2b_calls_customers_pm.number_calls as inbound_b2b_calls_customers_pm, 
	inbound_b2b_calls_cm_others.number_calls as inbound_b2b_calls_cm_others,
	inbound_b2b_calls_pm_others.number_calls as inbound_b2b_calls_pm_others,
	inbound_b2b_calls_professionals_cm.number_calls as inbound_b2b_calls_professionals_cm, -- 446 
	inbound_b2b_calls_professionals_pm.number_calls as inbound_b2b_calls_professionals_pm, 
	inbound_b2c_calls_customers_cm.number_calls as inbound_b2c_calls_customers_cm, 
	inbound_b2c_calls_others.number_calls as inbound_b2c_calls_others_cm, 
	inbound_b2c_calls_professionals_cm.number_calls as inbound_b2c_calls_professionals_cm, -- 605 
	outbound_b2b_calls_customers_cm.number_calls as outbound_b2b_calls_customers_cm, -- 654 
	outbound_b2b_calls_customers_pm.number_calls as outbound_b2b_calls_customers_pm,  
	outbound_b2b_calls_professionals_cm.number_calls as outbound_b2b_calls_professionals_cm, -- 753
	outbound_b2b_calls_professionals_pm.number_calls as outbound_b2b_calls_professionals_pm,
	outbound_b2c_calls_customers_cm.number_calls as outbound_b2c_calls_customers_cm,
	outbound_b2c_calls_customers_pm.number_calls as outbound_b2c_calls_customers_pm,
	outbound_b2c_calls_professionals_pm.number_calls as outbound_b2c_calls_professionals_pm,
	outbound_b2c_calls_professionals_cm.number_calls as outbound_b2c_calls_professionals_cm,
	pm_b2b_cases.b2b_pm_cases as pm_b2b_cases,
	pm_b2b_cases_partner.b2b_pm_cases as pm_b2b_cases_partner,
	pm_b2b_cases_professional_partner.b2b_pm_cases as pm_b2b_cases_professional_partner,
	pm_b2b_cases_unknown.b2b_pm_cases as pm_b2b_cases_unknown
		

FROM

	(SELECT
	
		TO_CHAR(o.effectivedate , 'YYYY-MM') as year_month,
		SUM(CASE WHEN o."type" = 'cleaning-b2b' THEN 1 ELSE 0 END) as orders_b2b,
		SUM(CASE WHEN o."type" = 'cleaning-b2c' THEN 1 ELSE 0 END) as orders_b2c
	
	FROM
	
		salesforce.order o
		
	WHERE
	
		o.test__c IS FALSE
		AND o.effectivedate >= '2018-01-01'
		AND o."status" IN ('PENDING TO START', 'FULFILLED', 'INVOICED', 'NOSHOW CUSTOMER')
		
	GROUP BY
	
		TO_CHAR(o.effectivedate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.effectivedate , 'YYYY-MM') desc) as orders
LEFT JOIN

	(SELECT

		t2.year_month,
		SUM(t2.number_cases) as cm_b2b_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases
			
			FROM
			
				bi.cm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
				
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2b_cases
		
ON

	orders.year_month = cm_b2b_cases.year_month

LEFT JOIN

	(SELECT

		t2.year_month,
		SUM(t2.number_cases) as cm_b2b_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.cm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
				
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'partner'
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2b_cases_partner
		
ON

	orders.year_month = cm_b2b_cases_partner.year_month

LEFT JOIN

	(SELECT

		t2.year_month,
		SUM(t2.number_cases) as cm_b2b_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional,
				a.parentid
			
			FROM
			
				bi.cm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
				
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c,
				a.parentid
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'professional'
			AND (t1.parentid = '0012000001TDMgGAAX' 
				OR t1.parentid IS NULL)
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2b_cases_professional_bat
		
ON

	orders.year_month = cm_b2b_cases_professional_bat.year_month

LEFT JOIN

	(SELECT

		t2.year_month,
		SUM(t2.number_cases) as cm_b2b_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional,
				a.parentid
			
			FROM
			
				bi.cm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
				
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c,
				a.parentid
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'professional'
			AND NOT (t1.parentid = '0012000001TDMgGAAX' 
				OR t1.parentid IS NULL)
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2b_cases_professional_partner
		
ON

	orders.year_month = cm_b2b_cases_professional_partner.year_month

LEFT JOIN

	(SELECT

		t2.year_month,
		SUM(t2.number_cases) as cm_b2b_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.cm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
				
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'unknown'
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2b_cases_unknown
		
ON

	orders.year_month = cm_b2b_cases_unknown.year_month
			
LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as cm_b2c_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2c_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2c_cases
			
			FROM
			
				bi.cm_b2c_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2c_cases
		
ON

	orders.year_month = cm_b2c_cases.year_month

LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as cm_b2c_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2c_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2c_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.cm_b2c_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
							
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
						
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'partner'
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2c_cases_partner
		
ON

	orders.year_month = cm_b2c_cases_partner.year_month

LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as cm_b2c_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2c_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2c_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional,
				a.parentid
			
			FROM
			
				bi.cm_b2c_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
							
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
						
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c,
				a.parentid
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'professional'
			AND (t1.parentid = '0012000001TDMgGAAX' 
				OR t1.parentid IS NULL)
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2c_cases_professional_bat
		
ON

	orders.year_month = cm_b2c_cases_professional_bat.year_month
	
LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as cm_b2c_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2c_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2c_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional,
				a.parentid
			
			FROM
			
				bi.cm_b2c_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
							
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
						
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c,
				a.parentid

				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'professional'
			AND NOT (t1.parentid = '0012000001TDMgGAAX' 
				OR t1.parentid IS NULL) 
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2c_cases_professional_partner
		
ON

	orders.year_month = cm_b2c_cases_professional_partner.year_month

LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as cm_b2c_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2c_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2c_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.cm_b2c_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
							
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
						
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				ca.origin,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'unknown'
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as cm_b2c_cases_unknown
		
ON

	orders.year_month = cm_b2c_cases_unknown.year_month
	
LEFT JOIN
	
	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	   AND o.e164callednumber__c IN ('493030807264', '41435084849') -- for B2B only de|ch
	 --  AND o.e164callednumber__c IN ('493030807403') -- for B2C only
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2b_calls_customers_cm
		
ON

	orders.year_month = inbound_b2b_calls_customers_cm.year_month	
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	   AND o.e164callednumber__c IN ('493070014488') -- PM only
	 --  AND o.e164callednumber__c IN ('493030807403') -- for B2B only
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2b_calls_customers_pm
		
ON

	orders.year_month = inbound_b2b_calls_customers_pm.year_month	
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	   AND o.e164callednumber__c IN ('493030807264', '41435084849') -- for B2B only de|ch
	 --  AND o.e164callednumber__c IN ('493030807403') -- for B2C only
	   AND o.relatedcontact__c IS NULL -- not customer related
	   AND o.account__c IS NULL -- not professional related
	   
	   -- , CASE WHEN n.e164callednumber__c = '493030807403' THEN 'B2C' ELSE 'B2B' END AS call_type
	   
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2b_calls_cm_others
		
ON

	orders.year_month = inbound_b2b_calls_cm_others.year_month
		
LEFT JOIN

	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	   AND o.e164callednumber__c IN ('493070014488') -- PM only
	 --  AND o.e164callednumber__c NOT IN ('493030807403') -- for B2B only
	   AND o.relatedcontact__c IS NULL -- not customer related
	   AND o.account__c IS NULL -- not professional related
 
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2b_calls_pm_others
		
ON

	orders.year_month = inbound_b2b_calls_pm_others.year_month	
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	   AND o.e164callednumber__c IN ('493030807264', '41435084849') -- for B2B only de|ch
	 --  AND o.e164callednumber__c IN ('493030807403') -- for B2C only
	   AND o.account__c IS NOT NULL -- professional related
	   
	   -- , CASE WHEN n.e164callednumber__c = '493030807403' THEN 'B2C' ELSE 'B2B' END AS call_type
	   
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2b_calls_professionals_cm
		
ON
	
	orders.year_month = inbound_b2b_calls_professionals_cm.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	   AND o.e164callednumber__c IN ('493070014488') -- PM only
	 --  AND o.e164callednumber__c IN ('493030807403') -- for B2B only !!!OUT: would be duplicate with inbound_b2b_calls_professionals_related_CM
	   AND o.account__c IS NOT NULL -- professional related
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2b_calls_professionals_pm
		
ON
	
	orders.year_month = inbound_b2b_calls_professionals_pm.year_month	
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	 --  AND o.e164callednumber__c IN ('493030807264', '41435084849', '493030807403') -- CM only
	   AND o.e164callednumber__c NOT IN ('493030807403') -- for B2C only
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2c_calls_customers_cm
		
ON
	
	orders.year_month = inbound_b2c_calls_customers_cm.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	 --  AND o.e164callednumber__c IN ('493030807264', '41435084849', '493030807403') -- CM
	   AND o.e164callednumber__c IN ('493030807403') -- for B2C only
	   AND o.relatedcontact__c IS NULL -- not customer related
	   AND o.account__c IS NULL -- not professional related
	   
	   -- , CASE WHEN n.e164callednumber__c = '493030807403' THEN 'B2C' ELSE 'B2B' END AS call_type
	   
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2c_calls_others
		
ON

	orders.year_month = inbound_b2c_calls_others.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
	
	WHERE 
	
		o.calldirection__c = 'Inbound'
	 --  AND o.e164callednumber__c IN ('493030807264', '41435084849', '493030807403') -- CM only
	   AND o.e164callednumber__c IN ('493030807403') -- for B2C only
	   AND o.account__c IS NOT NULL -- professional related
	   
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as inbound_b2c_calls_professionals_cm
		
ON

	orders.year_month = inbound_b2c_calls_professionals_cm.year_month		
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	   AND o.department__c LIKE '%CM%'
	   AND (a.type__c LIKE '%b2b%' OR oo.type__c LIKE '%b2b%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2b_calls_customers_cm
			
ON

	orders.year_month = outbound_b2b_calls_customers_cm.year_month	
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	   AND o.department__c LIKE '%PM%'
	  AND (a.type__c LIKE '%b2b%' OR oo.type__c LIKE '%b2b%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2b_calls_customers_pm
		
ON

	orders.year_month = outbound_b2b_calls_customers_pm.year_month	
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.account__c IS NOT NULL -- professional related
	   AND o.department__c LIKE '%CM%'
	   AND (a.type__c LIKE '%b2b%' OR oo.type__c LIKE '%b2b%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2b_calls_professionals_cm
		
ON

	orders.year_month = outbound_b2b_calls_professionals_cm.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.account__c IS NOT NULL -- professional related
	   AND o.department__c LIKE '%PM%'
	   AND (a.type__c LIKE '%b2b%' OR oo.type__c LIKE '%b2b%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2b_calls_professionals_pm
		
ON

	orders.year_month = outbound_b2b_calls_professionals_pm.year_month
	
LEFT JOIN

	(SELECT

		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	   AND o.department__c LIKE '%CM%'
	  AND (a.type__c LIKE '%b2c%' OR oo.type__c LIKE '%b2c%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2c_calls_customers_cm
		
ON

	orders.year_month = outbound_b2c_calls_customers_cm.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.relatedcontact__c IS NOT NULL -- customer related
	   AND o.department__c LIKE '%PM%'
	  AND (a.type__c LIKE '%b2c%' OR oo.type__c LIKE '%b2c%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2c_calls_customers_pm
		
ON

	orders.year_month = outbound_b2c_calls_customers_pm.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.account__c IS NOT NULL -- professional related
	   AND o.department__c LIKE '%CM%'
	   AND (a.type__c LIKE '%b2c%' OR oo.type__c LIKE '%b2c%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2c_calls_professionals_cm
		
ON

	orders.year_month = outbound_b2c_calls_professionals_cm.year_month
	
LEFT JOIN

	(SELECT
	
		TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
		COUNT(DISTINCT o.sfid) as number_calls
	
	FROM
	
	
		salesforce.natterbox_call_reporting_object__c o
		
	LEFT JOIN
	
		salesforce.contact oo
		
	ON
	
		o.relatedcontact__c = oo.sfid
		
	LEFT JOIN 
	
		salesforce.account a
		
	ON
	
		o.account__c = a.sfid
	
	WHERE 
	
		o.calldirection__c = 'Outbound'
	   AND o.account__c IS NOT NULL -- professional related
	   AND o.department__c LIKE '%PM%'
	   AND (a.type__c LIKE '%b2c%' OR oo.type__c LIKE '%b2c%')
	   
	
	GROUP BY
	
		TO_CHAR(o.createddate , 'YYYY-MM')
		
	ORDER BY
	
		TO_CHAR(o.createddate , 'YYYY-MM') desc) as outbound_b2c_calls_professionals_pm
		
ON

	orders.year_month = outbound_b2c_calls_professionals_pm.year_month 
	
LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as b2b_pm_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases
			
			FROM
			
				bi.pm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as pm_b2b_cases
		
ON

	orders.year_month = pm_b2b_cases.year_month 

LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as b2b_pm_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.pm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
			
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'partner'

				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as pm_b2b_cases_partner
		
ON

	orders.year_month = pm_b2b_cases_partner.year_month
	
LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as b2b_pm_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.pm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
			
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'professional'
			

				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as pm_b2b_cases_professional_partner
		
ON

	orders.year_month = pm_b2b_cases_professional_partner.year_month
	
LEFT JOIN

	(SELECT
	
		t2.year_month,
		SUM(t2.number_cases) as b2b_pm_cases
	
	FROM
		
		
		(SELECT
		
			t1.year_month,
			t1.case_id,
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) as try,
			SUM(t1.number_b2b_cases) as number_cases
		
		FROM
		
			(SELECT
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') as year_month,
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'closed') THEN 0 ELSE 1 END as number_b2b_cases_1,
				COUNT(DISTINCT o.case_id) as number_b2b_cases,
				CASE 	WHEN ca.reason LIKE 'Partner%' 			THEN 'partner' 
						WHEN ca.origin LIKE 'Partner%' 			THEN 'partner' 
						WHEN a.type__c LIKE 'partner' 			THEN 'partner'
						WHEN ca.reason LIKE 'Professional%' 	THEN 'professional' 
						WHEN ca.origin LIKE 'Professional%' 	THEN 'professional'
						WHEN a.type__c LIKE 'cleaning-b2b' 		THEN 'professional'

																ELSE 'unknown'  END AS partner_professional
			
			FROM
			
				bi.pm_cases_service_level_basis o
				
			LEFT JOIN
			
				salesforce.case ca
				
			ON
			
				o.case_id = ca.sfid
			
			LEFT JOIN
		
				salesforce.account a 
		
			ON 
		
				ca.accountid = a.sfid
					
			WHERE
			
				o.type NOT IN ('# Reopened')
				AND ca.reason NOT LIKE '%Onboarding%'
				AND ca."type" NOT LIKE '%Damage%'
				AND ca.test__c IS NOT TRUE
				-- AND o.case_id = '5000J00001TDAEhQAP'
				
			GROUP BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM'),
				o.case_id,
				o.date1_opened,
				o.date2_closed,
				ca."status",
				o.closed,
				o.type,
				ca.origin,
				ca.reason,
				a.type__c
				-- CASE WHEN (o.type = 'Type Change' AND o.closed = 'open') THEN 0 ELSE 1 END
				
			ORDER BY
			
				TO_CHAR(o.date1_opened, 'YYYY-MM') desc) as t1
				
		WHERE
		
			-- t1.number_b2b_cases_1 > 0
			-- t1.case_id = '5000J00001TCmRsQAL'
			(CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END) > 0
			AND t1.partner_professional = 'unknown'

				
		GROUP BY
		
			t1.year_month,
			t1.case_id,
			CASE WHEN ((t1.status = 'closed' AND t1.closed = 'open') 
						 OR (t1.type = 'New Case' AND t1.origin = 'direct email outbound')
						 OR (t1.type = 'Reopened Case' AND (t1.date2_closed = t1.date1_opened))) THEN 0 ELSE 1 END
			
		ORDER BY
		
			t1.year_month desc) as t2
			
	GROUP BY
	
		t2.year_month
		
	ORDER BY
	
		t2.year_month desc) as pm_b2b_cases_unknown
		
ON

	orders.year_month = pm_b2b_cases_unknown.year_month	
	
;