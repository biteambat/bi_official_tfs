



SELECT

	TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
	COUNT(DISTINCT o.sfid) as number_calls

FROM


	salesforce.natterbox_call_reporting_object__c o
	
LEFT JOIN

	salesforce.contact oo
	
ON

	o.relatedcontact__c = oo.sfid

WHERE 

	o.calldirection__c = 'Inbound'
   AND o.e164callednumber__c IN ('493030807264', '41435084849') -- for B2B only de|ch
 --  AND o.e164callednumber__c IN ('493030807403') -- for B2C only
   AND o.relatedcontact__c IS NOT NULL -- customer related

GROUP BY

	TO_CHAR(o.createddate , 'YYYY-MM')
	
ORDER BY

	TO_CHAR(o.createddate , 'YYYY-MM') desc
   
   
   
