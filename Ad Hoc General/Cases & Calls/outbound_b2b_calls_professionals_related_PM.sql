



SELECT

	TO_CHAR(o.createddate , 'YYYY-MM') as year_month,
	COUNT(DISTINCT o.sfid) as number_calls

FROM


	salesforce.natterbox_call_reporting_object__c o
	
LEFT JOIN

	salesforce.contact oo
	
ON

	o.relatedcontact__c = oo.sfid
	
LEFT JOIN 

	salesforce.account a
	
ON

	o.account__c = a.sfid

WHERE 

	o.calldirection__c = 'Outbound'
   AND o.account__c IS NOT NULL -- professional related
   AND o.department__c LIKE '%PM%'
   AND (a.type__c LIKE '%b2b%' OR oo.type__c LIKE '%b2b%')
   

GROUP BY

	TO_CHAR(o.createddate , 'YYYY-MM')
	
ORDER BY

	TO_CHAR(o.createddate , 'YYYY-MM') desc
   
   
   
